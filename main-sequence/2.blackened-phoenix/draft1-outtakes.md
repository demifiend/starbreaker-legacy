# Blackened Phoenix

**Part Two of Starbreaker**

by Matthew Graybosch

## Chapter One: Addicted to Chaos

Sid knew this table like an old friend. He could trace every scratch in its cherry veneer blindfolded. If asked, he could tell whose hand had wielded the knife that exposed the plywood beneath. He knew the origin of every stain, and could distinguish between between spilled coffee and spilled whiskey at a glance. He had flipped this table a hundred times in a hundred different brawls. He had even sought shelter beneath it when the ground rumbled and shook beneath them. Its drunken wobble was no less familiar than his wife's rhythm late at night when they thought the children were fast asleep. 

That Sid had never been to this particular hole-in-the-wall Chinese restaurant in the ass end of Queens a couple of blocks from the Murdoch Defense Industries headquarters was immaterial. The table felt like home because of who else sat with him. 

There was Edmund, who sat with his rifle leaning against the table sipping the tea that was all he ever ordered. His close-cropped gray hair gleamed from the gel he combed through it, and the angry red scar from the shot that nearly killed him in Boston made an inroad along his temple. 

Next to him sat Morgan, who wielded his chopsticks with the dainty efficiency of a man who had grown up within sight of the Forbidden City. Sid knew better; Morgan had grown up right here in Queens down the street from his own house. He schooled his expression to keep the nostalgia from showing, though Sid remembered how Morgan would insist on being on the same team as Sid when playing pickup basketball games, Morgan never acknowledged that they had been friends as children, or that he had had a younger brother that always tagged along. As far as the other man was concerned, their friendship began in ACS.

Morgan alternated between orange chicken and pork fried rice, pausing only to let Naomi steal a piece of chicken with her chopsticks. She was as pale as Morgan, and snow-blonde with scarlet eyes. Though her appearance was as unsettling in its way as Morgan's, Sid couldn't help but like her as soon as they met. Perhaps, Sid thought, it was the way her smile always went all the way up to her eyes. She was doing it now, delighted as one of Sid's younger daughters as her chopsticks met Morgan's like crossed swords. *They look better together than Morgan and Christabel ever did, that's for damn sure.*

[Christ, why don't you two just get a room already?] That had come from Sarah, the last to join them at their table. Sid had held her hand in a Boston ambulance told told her everything would be all right as a paramedic amputated legs mangled by a dictator's reckless gunfire below the knee and put them on ice in front of her. She had never gotten them back, and an incompatibility with the neural interface in the biomechanical prosthetics with which she had been fitted left her in near-constant pain. She shrugged off the hand Edmund placed in her shoulder to comfort her. [Don't pretend you care, Eddie. We were just scratching each other's itch, remember?]

"Women, right?" 

Though Edmund hadn't neither spoken nor texted, his expression said it all. Sid recalled seeing similar expressions on his uncles' faces, and like them Edmund had brought his woes upon himself. So he ignored the old man and studied Morgan, who was still eating when Naomi wasn't stealing from his plate. [I know you like to chow down before a mission, but this is ridiculous. I hope that's only your second plate.]

Morgan shrugged. One of the upsides of secure relay chat was the ability talk with one's mouth full without seeming ill-mannered. [Sorry to disappoint you, but it's my third.]

Sid hadn't realized it until recently, but there had been a method to Morgan's binge-eating. When he had had time and opportunity to chow down before a fight, the wounds he inevitably took healed faster. Weird shit, like him crossing the room in a flash to protect Sid or Edmund, also tended to happen more often when Morgan was well-fed. [You've never eaten that much before. How much trouble are you expecting at Murdoch's? We're not going to run into Imaginos, are we?]

Despite Morgan and Naomi sharing everthing they knew or suspected after Christabel's funeral, Sid still had trouble believing that Isaac Magnin, the white-haired dandy who ran the AsgarTech Corporation, was the mastermind behind Alexander Liebenthal and an immortal wizard who had somehow inspired an old concept album. *What was it Claire called Magnin? A white-haired bishounen?*

Though Sid knew not to speak of the Devil, it turned out merely thinking of her was sufficient. [If Morgan's that hungry, maybe he expects Imaginos to go one-winged angel?] 

[Dammit, Claire.]

[Come off it, Nims. Morgan's obviously carb-loading so he can deal with heavy wizardry.]

Morgan brought them back to business. [How does it look in there?]

[Like a clusterfuck waiting to happen. Bloody good thing nobody noticed Mordred or his kitty cam, but I can't believe nobody spotted him. If he gets any bigger you won't need a motorcycle. He's on his way back to you now, by the way.]

The door opened, and a long-haired black and white cat the size of a Great Dane sauntered inside as if making himself at home. Despite being soaked through from the rain, he did not attempt to shake himself dry. Instead, he settled down beside Morgan and began washing himself. The old woman at the counter left her post, her face set in a hostile expression.

Before she could say anything, Morgan turned to her and spoke in Mandarin. "My friend Mordred would like a large Seafood Combo without rice, and a large towel if you can spare one."

She blinked twice before replying in English. "Anybody else want anything?"

Naomi smiled. "Another egg roll, please?"
