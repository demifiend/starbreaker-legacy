## Author's Note

This novel contains not only spoken dialogue but dialogue transmitted over text messaging. To distinguish the two, I use guillemets (French quotation marks) for the latter instead of standard English quotation marks, so that a text message will look like this: {This is text dialogue.}

