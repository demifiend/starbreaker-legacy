#+TITLE: Blackened Phoenix Notes/Outline
#+AUTHOR: Matthew Graybosch
#+EMAIL: matthew@matthewgraybosch.com
#+DATE: <2017-05-19 Fri> 

* Logline
The truth sets no one free.
* Synopsis/Blurb
Morgan Stormrider and his friends seek the truth about the Phoenix Society and its corruption at the hands of Isaac Magnin, but what if their search only serves Magnin’s purpose? Every clue tests their resolve and threatens to shatter their beliefs.

While they build their case, a long-imprisoned enemy gathers strength and prepares to shatter its bonds with the help of a traitor to Magnin’s cause and an android that never learned to be human. Once freed, it will rain apocalyptic fury upon humans and asuras alike.

Meanwhile, the Phoenix Society itself is paralyzed by a series of stochastic terrorist attacks inspired by a figure identified only by the handle “Mastema”. While Isaac Magnin pivots to ensure Morgan and his friends continue to serve his ends as they investigate the Mastema case, his fellow Disciples of the Watch conspire against him.

The clandestine war for the future of intelligent life on Earth threatens to erupt anew. The Antarctic domed city of Asgard, testament to both humanity’s pre-Nationfall technological prowess and its second Renaissance, will be the epicenter. Will Morgan and the others comprehend the real threat in time to save the people of Asgard?
* Detailed Outline
** A Cutthroat Audition
:PROPERTIES:
:Viewpoint: Sid Schneider
:Location: Lincoln Center, Manhattan, NYC
:END:
*** Events
1. Sid Schneider's sitting in Lincoln Center with his family and Naomi Bradleigh as he waits for his oldest son Gabriel's turn to audition for the Juilliard Conservatory Young Pianists program for the upcoming year.
2. With only two students left before Gabriel, Sid asks Naomi where Morgan is because Morgan had promised to attend.
3. Naomi assures Sid that Morgan will be there, and explains that Morgan was checking into something.
4. As the last student before Gabriel performs, Morgan finally shows up and sits beside Naomi.
5. Before Sid can give Morgan shit for almost missing Gabriel's audition, his implant reports an unauthorized incoming data transmission, crashes, and reboots.
6. Upon rebooting, Sid's implant tells him that the Phoenix Society lied to him, and shows him evidence that the Phoenix Society knew [[Nathan Schneider][Nathan Schneider's]] employer cut corners, knowingly allowed unsafe working conditions, and thus shafted his father.
7. Just as Sid gets his implant under control, Gabriel appears on stage. However, he is not alone. A stage-hand has Gabriel, and is holding a knife to the boy's throat.
*** Chapter Cast
1. [[Sid Schneider][Sid Schneider]]
2. [[Naomi Bradleigh]]
3. [[Morgan Stormrider]]
4. [[Gabriel Schneider-Michaels]]
5. an unidentified stage-hand
** Tell Me Sweet Little Lies
:PROPERTIES:
:Viewpoint: Naomi Bradleigh
:Location: Lincoln Center, Manhattan, NYC
:END:
*** Events
*** Chapter Cast
* Dramatis Personae
** Central Characters
*** Sid Schneider
    :PROPERTIES:
    :Age:      32
    :Species:  /homo sapiens sapiens/
    :Ancestry: African
    :Height:   6' 11"
    :Weight:   280lbs
    :Job:      Adversary
    :Spouse:   [[Ellen Schneider][Ellen Michaels]]
    :Children: [[Gabriel Schneider-Michaels][Gabriel Schneider-Michaels]]
    :Children+: [[Aja Schneider-Michaels]]
    :Children+: [[Alexandra Schneider-Michaels]]
    :Children+: [[Elijah Schneider-Michaels]]
    :END:
    Sid Schneider is one of the last active-duty Adversaries in the cast, but wants to retire so that he can be a better father to his younger children. As an Adversary, he feels he missed most of the childhoods of his oldest two children ([[Gabriel Schneider-Michaels][Gabriel]] and [[Aja Schneider-Michaels][Aja]]) and regrets it.

    He met his wife [[Ellen Michaels]] while volunteering as a practice partner at a women's Krav Maga. She drove Sid to the floor to demonstrate to the other women that it was possible to use Krav Maga to take down larger and stronger assailants, and won his respect and affection in the process.

    Originally a professional bodybuilder who trained others while competing, Sid became an Adversary after his father, [[Nathan Schneider]] was blinded at work and a Phoenix Society investigation ruled that his father had not followed appropriate safety procedures and thus his employer was not guilty of any wrongdoing in its refusal to do more than pay for Nathan's immediate medical treatment.

    However, thanks to [[Mastema]] Sid will soon learn that Nathan /had/ been following procedure, and that the Phoenix Society swept the evidenc under the rug and blamed the victim because the Adversaries responsible for the investigation did not disclose their conflicts of interest and recuse themselves. 

    Though his role in /Without Bloodshed/ was relatively minor, he will play a much larger part in /Blackened Phoenix/ as he spearheads the Mastema investigation.
*** TODO Morgan Stormrider
*** TODO Naomi Bradleigh
*** TODO Mastema
** Supporting Cast
** Minor Characters
*** TODO Saul Rosenbaum
*** TODO Ellen Michaels
*** TODO Gabriel Schneider-Michaels
*** TODO Aja Schneider-Michaels
*** TODO Alexandra Schneider-Michaels
*** TODO Elijah Schneider-Michaels
*** TODO Nathan Schneider
