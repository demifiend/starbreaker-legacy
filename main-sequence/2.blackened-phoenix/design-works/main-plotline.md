# Starbreaker Design Works: Blackened Phoenix

**by Matthew Graybosch**  
*with assistance from Catherine Gatt*

## Main Plotline

Morgan and Naomi seek evidence against Imaginos, who appears to be using the Phoenix Society for his own nefarious ends, while fending off new threats from anti-Phoenix Society extremists, the new prototype asura emulator Polaris, and the newly unbound and vengeful demon Sabaoth.
