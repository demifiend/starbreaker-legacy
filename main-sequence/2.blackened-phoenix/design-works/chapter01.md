# Starbreaker Design Works: Blackened Phoenix

**by Matthew Graybosch**  
*with assistance from Catherine Gatt*

## Chapter 1

Main action: Raid on Murdoch Defensive Industries factory in east side of Queens (near where the Nassau County line would be in reality).

### Objective

Morgan Stormrider wants evidence against Isaac Magnin that isn't tied to the murder of Christabel Crowley. A murder rap might not be enough to take down a member of the Phoenix Society's executive council, but proof of corruption and abuse of power will make it impossible for him to retain power even if he isn't deposed and put on trial.

Short version: Morgan is going after Imaginos' legitimacy.

### Rationale

* Morgan & Co. know Magnin was behind the manufacture and sale of devil-killer rifles 
* Magnin then uses third parties to distribute the rifles to extremist groups around the world.
* Now armed with weapons capable of piercing conventional armor, the extremists can cause trouble.
* The Phoenix Society's propaganda department uses this terrorism to justify its continued existence.
* Alexander Liebenthal was one of the distributors Magnin used.
* Victoria Murdoch was one of the manufacturers Magnin used.
* Morgan & Co. know this, but need evidence they can bring to a jury.
* Murdoch Defensive Industries does not have a good reputation among workers due to rights violations:
    * union busting
    * disregard for health/safety
    * freedom of speech violations
    * wage theft
    * mandatory unpaid overtime
* Sid Schneider alone recorded two dozen violations during his career as an Adversary.
* Ordinarily, a corporation guilty of this many violations would be dissolved and its officers arrested.
* MDI gets special treatment because of its ties to the Phoenix Society.

### Method

Despite being a preferred vendor, Morgan and Sid persuaded Saul and Iris to approve a warrant. MDI is fucked, and Victoria Murdoch is to be arrested. Despite this, Morgan isn't interested in putting Murdoch on trial, though he's happy to let Sid do it and get credit for the bust since Sid's father was blinded on the job at Murdoch when Sid was a child, and Murdoch wriggled out of compensating John Schneider for his injury and resulting blindness.

The plan is to persuade Murdoch to plead guilty and spill her guts in exchange for a lighter sentence: exile from Earth instead of life imprisonment.

### Personnel

* Sid Schneider:
    * Lead Adversary
    * He'll take point by presenting the warrant and making the arrest.
* Edmund Cohen:
    * Sharpshooter
    * He'll provide fire support with his trusty Dragunov, as usual.
* Munakata Tetsuo
    * Combat Support Lead
    * He'll back Sid up if something goes wrong.
* Sarah Kohlrynn
    * Combat Support
    * Her legs had to be amputated due to infection while in hospital.
    * She's back on the job using electromechanical prosthetics, but bitter about it.
        * The tech to clone parts is available, but prohibitively expensive.
        * The Phoenix Society could pay for them, but won't because prosthetics are "enough".
        * Sarah would need to save for at least 10 years on her salary.
        * She could take loans, but they'd take at least 15 years to pay off.
        * The longer Sarah goes without cloned replacements, the worse off she'll be.
        * Not only does she have to adjust to prosthetics, but would have to readjust to flesh again.
        * This is totally not helping her relationship with Claire.
* Claire Ashecroft
	* Remote Support
	* Signals and electronic intelligence
	* Electronic warfare and countermeasures
	* Also handling SIGINT, ELINT, EW, and EWC for Morgan and Naomi in London.
* City Militia
    * Bayside Volunteers
    * Led by Kymberlyn Reed
    * Secondary Combat Support

### Action

1. Adversaries take positions outside MDI factory office.
2. Eddie's nervous. MDI looks like it's locked down for a seige.
3. Sid takes note, respecting the old soldier's experience, but attempts to present the warrant anyway.
4. Security guards inside open fire.
5. Sid is injured, and retreats long enough to get first aid.
6. Tetsuo, Sarah, and the Bayside Volunteers back Sid as he fights his way to Murdoch's office.
7. They find Isaac Magnin grasping Victoria Murdoch by the throat.
8. Sid commands Magnin to let Murdoch go.
9. A portal into deep space opens, and Magnin shoves Murdoch through. The portal closes again.
10. Despite Tetsuo's warning, Sid attacks Magnin.
11. Magnin conjures a staff of ice and disarms Sid before knocking him across the room with a single blow.
12. Sid tries to return to the fight, but is pinned to the wall as Magnin extends his staff and pierces Sid's shoulder.
13. Magnin threatens the others, telling them that the more Morgan Stormrider delays, the greater the peril his friends face.
14. As Eddie helps patch Sid up, they decide to enlist Naomi and Claire to persuade Morgan that he absolutely *must* seek help from Desdinova and Ashtoreth despite his reluctance to trust them.
15. Lack of evidence on Witness Protocol results in Karen Del Rio going after the whole crew on an abuse of power charge.

### Setting Notes

Victoria Murdoch might have a video camera in her office to record meetings for blackmail purposes, but this shouldn't be connected to the main CCTV system, or network accessible. The camera is most likely hidden, but Magnin's magic use causes massive EM interference due to the level of power he's drawing. This also fucks with everybody's implants, and keeps Claire from being able to contact them.

(Never mind that this sounds like a boss fight from *Persona 3* or *Persona 4*.)

### Snippets

These need to be pasted into the text once I write it, assuming Catherine approves them.

#### Sid's Defiance

Sid grasped at the rod of ice piercing him, only to jerk his hand away as if burned by the cold. That same gelid pain radiated from his shoulder, threatening to leech all heat from his body. "I don't know what the hell you are, but I ain't letting you go after Morgan or the others."
