# Blackened Phoenix

**Part Two of Starbreaker**

by Matthew Graybosch

## Chapter One

Edmund Cohen stared at Mordred. The tuxedo cat stared back after comparing his cards against the four community cards on the table. The notion of a cat the size of a mastiff playing Texas hold 'em no longer fazed Edmund. His inability to figure out when the damned cat was bluffing was more worrisome.

A soft rumble filled the room as the others checked their cards. Edmund fingered his chips and began to sweat a little. In his limited experience, Mordred's purring at the table meant he held a strong paw of cards. If he was doing it now, after the turn, then Edmund had no choice but to suspect that the dead man's hand he had dealt so far, black aces and eights, somehow improved Mordred's position dramatically.

"Fuck this." Sarah tossed her cards onto the table and swiped her forearm across her forehead. She stood, wincing in pain as she put weight on the prosthetic legs she got installed after the fight against Alexander Liebenthal. "I gotta take my meds anyway. 'Scuse me."

Claire glanced up at Sarah. "You want help?"

"I want my demon-ridden legs back." Sarah forced the words through gritted teeth as she left the room. Though Edmund wanted to offer some words of comfort, he knew better. Phantom limbs were an aspect of the soldier's life he had thus far managed to avoid.

Edmund settled his glare on Morgan. Though he had revealed the truth about his nature as an asura emulator and the abilities built into him and his brothers soon after the mission to Boston, the revelation had come too late to do Sarah any good. He rubbed at the scar along his temple where Liebenthal's last shot grazed him. *It didn't save Naomi from a cracked rib or me from damn near getting my head blown off, either.* 

Like his cat, Morgan met Edmund's glare. "What?"

"Nothing." Edmund shook his head. "Anybody else want to fold?"

Morgan tossed in the required number of chips for the big blind. "Have you ever known me to fold?"

"Maybe you should learn." Sid threw in his cards, and Mordred's purring grew deeper and louder.

Claire elbowed the big man as she folded in turn. "Give the man his due, Sid. He kept it up with Christabel for years. He's probably swallowed as much pride as I've swallowed&mdash;"

"Dammit, Claire." Naomi pushed away her cards with an exasperated sigh. "You were doing so well today."

"Nims, you have no idea how long I've been saving that." Claire stretched, and caught Edmund's gaze as her t-shirt rode up. "See something you like?"

"Don't flatter yourself. I've had better."

Claire favored Eddie with a lazy, venomous smile. "Ashtoreth slipping you roofies doesn't count."

"Your jealousy's showing." Edmund turned over a card and discarded it to foil card counters. Though he doubted any of his friends would cheat, old habits died hard. The river was a three of clubs. "Place your bets, kitties."

Mordred chirped, and went all in as his purring threatened to shake loose the fillings in Edmund's teeth. With a shrug, Morgan followed suit and studied Edmund. "Once more into the breach?"

Edmund checked his cards, which amounted to jack shit. Though the community cards gave him two pair, he doubted Mordred would be purring the way he was if he didn't have an ace or an eight under his paw. He shoved his small pile forward with the heel of one hand. "Might as well. You think the moggie's bluffing?" 

Morgan shrugged. "I wonder if he's capable of it."

A deafening silence filled the void as Mordred stopped purring. His whiskers began to droop as he turned over his cards, revealing a deuce and a three. He mewed, looking at Morgan as if apologizing for coughing up a hairball on his bed. 

Edmund found his shock mirrored in the aghast expressions of Naomi, Claire, and Sid. Sarah returned in time for the aftermath. "Holy shit. The cat was bluffing."

"Which one?" Edmund asked as Morgan flashed his first smile since the game had begun. With a flourish he revealed a pair of aces the crimson of a baron in flight, and Edmund's self-control staged a wildcat strike. "You rat bastard. You've been acting like your favorite band died in a maglev crash, and you had four of a fucking kind?"

"What did you expect?" Morgan scratched behind Mordred's ears, and the cat started to purr again. "Mordred had distracted you. You thought he had a strong hand because he's had strong hands before. When he continued to act as if he held strong cards, you focused your play against. You weren't looking at me as I kept quiet and bet as small as I could."

Sid shook his head. "Man, why don't you quote Sun Tzu while you're at it? I guess we're having Chinese tonight."

\*\*\*\*\*

