## Parental Advisory: Explicit Fucking Content

The following is a work of fiction and contains content that may be offensive, triggering, or inappropriate for certain readers. Any views or opinions expressed by the characters in this novel are strictly their own and do not necessarily reflect those of the author or the publisher&mdash;or the author's employers for that matter.

Any resemblance or similarities between the characters depicted within to living or dead persons in this world or any parallel world within the known multiverse are either a coincidence; an allusion to real, alternate, invented, or secret history; or a parody.

The stunts in this work were performed by trained professionals; attempting them at home can result in property damage, civil or criminal liability, personal injury, and premature death. If you find any allegory or applicability in the following novel, please consult a qualified professional for psychiatric evaluation and treatment.
