## Unfinished Business

*In which carefully laid plans shatter on contact with reality, an Adversary playing his cards close must reveal a hole card, an enemy inflicts an embarrassing defeat, and an untrustworthy ally points the way forward...*

