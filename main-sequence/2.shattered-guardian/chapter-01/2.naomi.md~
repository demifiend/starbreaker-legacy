## Queens :: Naomi

Naomi rolled her eyes as Claire unslung a backpack festooned with heavy metal
band patches attached using a mixture of safety pins and sloppy stitching. She
pulled from the bag a laptop computer the size of an old-fashioned dictionary.
Its lid was as festooned with stickers as her backpack had been with patches.
The newest of them bore slogans like *This machine pwns fascists.* and *If it
ain't Unix it's crap!*

Though Naomi had made a valiant attempt at holding her silence while waiting for
Claire to boot up the ponderous device and connect it to the Murdoch building's
internal network by jacking a cable into the security terminal controlling the
door, it was a lost cause. "I can't believe you're mucking about with that
ancient piece of shit while Morgan's probably getting his arse kicked inside."

"It was either this or my Carl Gustav," said Claire as she sat crosslegged
against the wall. A tinny recording of "Balls to the Wall" by Except blasted
from the tiny speakers as it finished its boot sequence and let Claire log in.
"Trust me. The building's AI was expecting an attack over the network, but its
as vulnerable to direct attack as Morgan is to you when you give him that look."

Naomi glared down at the younger woman. "What look?"

Claire looked up from her screen and favored Naomi with a saucy grin. "That one.
He thinks you're sexy when you're angry."

"Well, he's going to find me irresistable when he sees me next. You were
watching his feed before he dropped off the network, weren't you?"

Claire's hands flew over the keyboard, clickety-clacking as she hurled shell
commands like a sorcerer who had returned from an errand to find brooms dancing
around a crestfallen apprentice. "Morgan's be holding out on us, the rat
bastard."

Naomi sighed, and kicked an empty Ebony can across the pavement as she waited
for Claire to get the door open. The younger woman had expressed her own feeling
more a more succinct fashion than Naomi herself could manage. *Then again,
Claire only loves him as a friend. It's only natural that Morgan wouldn't tell
her everything.*

"I'm in," said Claire. "Accessing the building's security feeds. Looks like
Victoria Murdoch doesn't have a cam in her office. I guess she likes her privacy."

"Not sure I blame him for not telling us he could project an absolute terror
field, though," said Claire after a minute of silence save for her fingertips on
the keyboard. "We all knew Morgan was different, but if he rubbed our faces in
it all the time would any of us want to be around him?"

"Claire, you don't get it. He's been letting people use him as a punching bag
for years when he could have protected himself."

Green eyes fading to gray at the center met Naomi's. "Nims, I don't think *you*
get it. If I had to fight somebody that my sword couldn't touch, I'd bring a
gun. If my gun couldn't take him out, I'd get a *bigger* gun. If the biggest gun
I could handle wasn't enough, I'd break out the explosives. I'd be endangering
an ever-increasing number of innocent people, and for what? To prove that I was
the baddest motherfucker around? Morgan has the title locked right the hell
down, and he doesn't need to bother wiring himself to a secondhand Soviet nuke
to do it."

Unable to argue with the substance of Claire's argument, Naomi settled for
attacking her style for form's sake. "Dammit, Claire. How many obscure allusions
are you going to throw at me?"

Claire shrugged. "As many as it takes. It's not like you don't have a computer
in your head that would let you search out their meanings. But if you want plain
talk, here it is: before you get angry with Morgan because he's kept secrets,
you might want to run an inventory on the skeletons in *your* closet."

"If you know so much about how to maintain a successful relationship, why are
you still single?" It was an unforgivably bitchy thing to say, but the words
sprang to Naomi's lips and escaped before she could hold them back. *I must
really be worried about Morgan if I'm snapping at Claire like this."

Before she could apologize, Claire flipped her off. "I'm single by choice, Nims.
I'm a greedy bisexual slut, I think romance is bollocks, and I'd rather die
alone than apologize for what I am. How long are you and Morgan going to
apologize to the world for what *you* are?"

"I don't know what you're talking about."

"Neither do I, but I'll be damned if I'll let that stop me." Putting aside the
laptop, Claire stood and pulled Naomi into a bear hug she would have expected
from one of the guys. With a sniffle that suggested she was holding back tears,
Claire continued. "You're both hiding shit from yourselves and from the people
who love you most. I can't drag you out of whatever demon-ridden closet you've
locked yourselves inside, but you both need to come out of there before you end
up lost in fucking Narnia with no clue how to get home."

Before Naomi could say anything, the lock disengaged with a series of clicks and
whirrs and the door opened behind her. Claire released her, and began putting
away her gear. "I'll take Morgan out to dinner and talk to him when we're done
here. I'll tell him everything and hear him out."

Claire looked up at Naomi, her eyes shimmering with unshed tears. "Morgan
wouldn't stay offline this long unless bad shit was happening. You'll help him,
won't you?"

*Bloody hell, she's actually scared.* Naomi caught Claire by one of her
backpack's straps and pulled her close. Hugging the younger woman who used to
call her 'Aunt Nims', Naomi held her gaze and said, "Trust me. I might not be
the superhero Morgan seems to be, but I've got a few aces in the hole myself."

Claire nodded, and pulled away. Once she was clear, Naomi drew her sword. The
alloy from which Nakajima Chihiro had forged it shimmered as the sun broke
through the cloud cover. It was part of a matched pair whose names Nakajima had
shared with Naomi but not with Morgan. "Morgan thinks it's silly and pretentious
to name swords," Nakajima had said, "So I never tell him the names of his
swords. However, I think you'd appreciate knowing that your swords are named
Sweetness and Light."

Letting Sweetness cleave the gloom ahead of her, Naomi passed the back door and
strode through the disused entrance to the Murdoch offices. Most of the
elevators would not respond when she pressed their call buttons, and Naomi had
resigned herself to taking the stairs as she pressed the last one. The button
rewarded her with a faint chime, and lights flickered above the door as the
elevator descended from the top floor to meet her.
