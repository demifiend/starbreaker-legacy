## Dedication

For my wife, Catherine Gatt. When she agreed to swap stories when I first emailed back in 2000, she had no idea what sort of Pandora's box she was about to open.

Then again, neither did I.