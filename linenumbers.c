/**
 * It's ridiculous that Catherine is wasting time typing 
 * out the line numbers herself. That shit can and should
 * be automated. She should have asked me.
 */

#include <stdio.h>

int main()
{
	const int START_LINE = 4198;
	const int END_LINE = 6728;
	const int INCREMENT = 2;
	int i;

	for (i = START_LINE; i < END_LINE; i = i + INCREMENT)
	{
		printf("%d\n", i);
	}
	
	return 0;
} 
