# Dead Man's Hand

**a Starbreaker novel**

by Matthew Graybosch

## Chapter One

The first time is always the worst. Most people read that somewhere, or hear it at the cinema. Men say it to women to overcome their resistance. Sometimes women say it to men, too. What most people never learn is that the first time you die is always the hardest.

Don't look at me like that, doctor. You've seen the psych eval. You know I'm not delusional. Or, if you prefer, you know I'm not indulging in any sort of delusion that has made it into your diagnostic system.

Listen to the machines you've got me jacked into. Listen for the beep that would mark me a liar with five nines certainty. Listen to the sounds they're not making. All you hear is my voice, telling you the plain facts.

Or do you hear yourself, insisting that mine was merely a near-death experience? Believe what you like. It matters as much as what I saw when I crossed over, which incidentally was stone cold nothing.

We both know why I'm here. I'm here to take the first step toward becoming an Adversary. Tyrannicide for fun and profit, and why not? If I'm going to kill people like the Aces-and-Eights, I might as well do it under the Phoenix Society's aegis and on their payroll.

The Phoenix Society agrees, and they get something out of the deal too. They get a guy who can do their dirty work for them. They get someone who would *enjoy* doing their wet work. Seems the standard program at ACS doesn't do a good job of producing such individuals, which is where I come in.

But first, there are niceties we must observe. The Phoenix Society can't permit any perception on the public's part that they play favorites. The people in charge want the public to think I'm a naive young man like the others. They want to think I'm a nice boy who believes it's a privilege to serve his city and humanity by risking his life as part of the thin red line between freedom and tyranny.

That's where you come in. You're the last rubber stamp the Society needs so that my entering Adversary Candidate School looks kosher.

You're going to listen while I tell you what it's like to die knowing you failed the one person who depended on you to keep them safe. You're going to listen as I tell you what it's like to claw your way out of the black and kill anybody stupid enough to get between you and that one person. You're going to listen as I tell you what it's like to see that one person spit in your face and wish you had let them die.

You're going to listen. You're going to wish you could hate me for what I've done. You won't manage it because you know damn well that in my place you might have done the same. Hell, you might even believe me.

## Chapter Two

That one person I shouldn't have failed? He was my brother Jimmy. Well, he might not have been my real brother, but he sure as hell acted like one. My parents adopted me after years of trying. I was seven, and had already gotten bounced between half a dozen foster homes. Seemed I stopped being cute once I was old enough to figure out that my parents wanted me because the fucking AsgarTech Corporation was paying them to raise me as part of some experiment.

How did I know? Simple. I learned to read, and one of my parents had been dumb enough to leave a printed bank statement where I could see it. Apparently I'm not human at all, but some kind of robot: a one-hundred series asura emulator.

But we'll get to that in due course, doc. I was talking about Jimmy and my family. Yeah, I know I put down my family name as Sturmjaeger. Can't believe people actually fell for that shit when the band had put out a new fucking album the day before I signed up.

A month after I had settled in enough to call my parents Mom and Dad with a straight face, they sat me down and told me I was going to have a baby brother. Seems once they had gotten used to having me around and saw that I was comfortable with them, they relaxed enough so that they hit the jackpot one night when they hired our upstairs neighbor's daughter Katie to babysit.

Oh, Katie? I liked her better than the other babysitters. She'd actually speak to me. She actually seemed to give a shit about me. My parents did, too, which was new to me.
