# Tigress Team

By Matthew Graybosch

With apologies to John Steakley (*Armor* and *Vampire$*), this Naomi Bradleigh is no other Naomi Bradleigh. This Claire Ashecroft is no other Claire Ashecroft

## I

Life as a front-line customs officer reminded Udo Hoffman of his service in the army. Both consisted mainly of tedium punctuated by occasional excitements that metastatized into crisis when left unchecked. Intuition suggested that the two women before him would provide today's excitement. He motioned the shorter auburn-haired woman forward. "Papers, please. Anything to declare?"

She came forward, and flashed a NetMini identifying her as Claire Ashecroft. "Only that I'm the best lay you'll never have."

Hoffman raised an eyebrow; he was unsure if Ms. Ashecroft was trying to be witty, or if she honestly believed she was that good in bed. He took a moment to undress her with his eyes, though her snug "GOT POSIX?" t-shirt and jeans did much of the work for him. *Well, she's certainly got the body for it.*

A thump as Claire lifted her suitcase onto the counter pulled his attention back to reality. "I suppose you'll want to take a look inside. I've just got my clothes, toiletries, and a couple of dildos in there. Oh, and my harness."

The statuesque woman behind Claire sighed, and Hoffman noticed that not only was she snow-blonde, but a full-on Neko to boot. Her tail twitched in annoyance. "Dammit, Claire. Show the man your carry-on and let him do his job."

"Naomi never lets me have any fun." Claire shot back over her shoulder before placing her carry-on atop her suitcase. She withdrew a deck with a feline shape Hoffman recognized because his niece Bettina had a similar model.

Though he knew better, he couldn't resist commenting. "Aren't you a bit old for Nekotek?"

"Nekotek is bloody *awesome*. It's got better stealth tech so nobody can see what kind of porn I'm pulling off the net, and no other deck runs Mewnix."

*Great. A Nekotek fangirl and a Neko. I don't get paid enough for this shit.* "And this is all personal property? Got any food or goods you intend to sell?"

Claire pulled a packet of Mint Slice cookies from her bag, crammed one into her mouth, and offered Hoffman the packet. With her cookie pressed into one cheek like a deranged squirrel, she managed to say, "Have one. They're almost as tasty as my girlfriend."

The snow-blonde Neko facepalmed and her tail swished in earnest. "Goddammit, Claire."

Hoffman resisted the temptation. A snack would have been nice since he missed lunch, but accepting a cookie from a traveler probably counted as taking bribes. "Better not. What's your purpose for visiting?"

"My partner and I are security consultants. The AsgarTech Corporation in Munich hired us to do some penetration testing."

Hoffman scanned Claire's passport, but the computer found nothing untoward. "You're free to go. Welcome to Germany. Next!"

The snow-blonde woman smiled, and removed her sunglasses to reveal scarlet eyes. Her NetMini identified her as Naomi Bradleigh. With Claire out of the way, Hoffman noticed the rapier on her companion's hip. "I think that's the first time I've seen a Neko wearing a sword. Did AsgarTech hire you, too?"

"Indeed, officer." Unlike Claire's brash tone, Naomi spoke in a soft contralto that led Hoffman to wonder if she was a singer. "I don't have any goods, food, or alcohol, but I should probably show you my weapons."

Hoffman stood, peering over the counter as Naomi crouched and withdrew long knives from each her boots and laid them on the counter. She pulled two more from up the sleeves of her leather jacket, which appeared made more of zippers than of leather and looked capable of deflecting swords. She then showed Hoffman her hands. Claws sprang from her fingertips. "Meow."

## II

"Meow." Claire was still giggling as Naomi closed the door to their room at a Japanese-themed hotel behind them. "Did you see the look on that Customs dick's face, Nims?"

"'Bloody priceless', apparently." Naomi schooled her expression, but despite her efforts impatience tinted her voice. "I recall you making that observation three times during the drive from the airport."

"Sorry." After extracting her deck and her open pack of Mint Slices, Claire shoved her bags into a closet. "You gonna unpack?"

"I suppose I should, but are you sure this is the right hotel? I recall making the reservation for a suite at an entirely different establishment."

"Oh yeah." Claire said around a mouthful of Mint Slice from the package she had opened earlier. "I cancelled that and got this instead. I figured it would be easier to secure, and it's *much* cheaper."

"How the hell did you manage that? I didn't even tell you about the deal I got. I thought you'd want to stay somewhere *nice* for once."

"Hey, this *is* nice." Claire spread her arms. "It's roomy, it's clean, there's no clutter, all the fixtures are brand new, and you don't have to sleep anywhere near me if you don't want to."

Naomi turned about, taking in the room as Claire opened a closet and pulled out a folding screen. She thrust it into Naomi's hands. "Here. Unfold this and stand behind it."

"Why?"

"Trust me. You'll love this."

"Fine." For good measure, Naomi retreated to a corner of the room and unfolded the screen so that it cut her off from the rest of the room. "Now what?"

Naomi's corner of the room was utterly silent, though she could see Claire's silhouette moving through the screen. The other woman brought her hands to her mouth as if she wanted to amplify her voice, but Naomi heard nothing until she pushed the screen aside. "Do I even want to know what you were saying?"

Claire chuckled. "Probably not."

Naomi brushed her fingertips across the top of the folding screen. "So if we arrange these properly, nobody will be able to listen to us?"

Claire nodded. "Room's supposed to be a soundproof Faraday cell, but the screens are a little extra. Also, I was thinking of getting these for our flat. This job was a good excuse to try before buying, you know?"

"Good point." Possibilities already sprang to Naomi's imagination. With screens like these, the loft she shared with Claire would more closely resemble a proper home. "Should I put this away?"

"Nah. Help me set up some more."

Once they had surrounded themselves with screens, Claire sat cross-legged on the tatami-covered floor. Naomi joined her, folding her legs beneath her, and glanced around. "This is so bloody weird."

"Says the woman who decided to spend her life in full Asura Profile cosplay. I don't mind texting you if we're apart or working, but it's nice to actually *talk*." Claire leaned forward. "So, let's hear it. What sort of job have you lined up for us? All I know is it involves AsgarTech."

## III



[I still can't fuckin' believe our contact is some dude named Armitage.] 