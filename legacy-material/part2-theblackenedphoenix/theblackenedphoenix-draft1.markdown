#The Blackened Phoenix
a *Starbreaker* novel    
by Matthew Graybosch

##Disclaimer
The following is a work of fiction. Any resemblance or similarities between the characters in this novel to living or dead persons in this world or any parallel world within the known multiverse is either a coincidence; an allusion to real, alternate, invented, or occult history; or a parody. If you find any allegory or applicability, please consult a qualified professional for psychiatric evaluation and treatment.

##Parental Advisory
This book and other *Starbreaker* novels are not appropriate for children under the age of sixteen due to explicit fucking content.

##Dedication
For my wife, Catherine Gatt, because without her might be stuck dedicating this book to my cat.

#Outline
##Phoenix Society Bomb Threat
1. Bomb in Sephiroth AI's reserve hardware
- Morgan and Naomi asked to help
- Device turns out to be a transmitter
- The entire building is rigged to collapse
- The device is built into the component
- It uses a dead man's switch
- Cut power, and the device uses built-in capacitors to transmit detonation signal
- Component cannot be removed without cutting power
- Computer cannot be removed without cutting power
- Capacitors cannot be removed without force that would damage the computer
- Bomb squad not called because threat promised immediate detonation on arrival