#The Blackened Phoenix
Part Two of _Starbreaker_  
by Matthew Graybosch

##Disclaimer
The following is a work of fiction. Any resemblance or similarities between the characters in this novel to living or dead persons in this world or any parallel world within the known multiverse is either a coincidence; an allusion to real, alternate, invented, or occult history; or a parody. If you find any allegory or applicability, please consult a qualified professional for psychiatric evaluation and treatment.

##Dedication
For my wife, Catherine Gatt, because without her might be stuck dedicating this book to my cat.

## Chapter Zero

Intermission

> I can't believe Isaac considered hiring that fat trollop. It's bad enough Morgan is willing to have her as a friend. She's loud, she's obnoxious, she can't take *anything* seriously, and she makes no bones about willing to do for Morgan everything a sensible woman should consider to be beneath her. But why would Isaac want her on his payroll? Surely there are other equally capable technicians without all her personality flaws and filthy habits.

> ...from the diary of Christabel Crowley, 1 May 2108

Hi there! If you're reading this, you either read *Without Bloodshed* already and want to see what happens next, or this is your first *Starbreaker* book and you don't know what the bloody hell is going on. If you're a returning fan, stick around anyway for a quick refresher. If you're a *Starbreaker* virgin, I'll be as gentle as you want me to be.

What? Who am I? I'm Claire Ashecroft, the *real* hero. You know what Morgan Stormrider and Naomi Bradleigh would have accomplished without me? Jack and shit, and Jack just buggered off to the pub. Let me tell you what *really* happened.

First, some white-haired prettyboy murdered former Crowley's Thoth violinist Christabel Crowley, which I thought was excessive even though I hated the skinny bitch. Then that gun-running wanker Alexander Liebenthal took over Boston with help from ex-Adversary Munakata Tetsuo and the Fireclowns MC. Meanwhile, Morgan Stormrider's off to Tokyo because Nakajima Chihiro wanted him to investigate the theft of designs for some kind of electrical devil-killer rifle.

In the meantime, I busted my heart-shaped ass trying to help Naomi Bradleigh when I could have been enjoying a cuddle sandwich with a couple of strapping young lads I picked up on a pub crawl the night before. Turns out a dirty cop with a grudge against Morgan thought he could get some payback by sticking Nims in a Faraday cage and bullying her into copping to Christabel's murder. I disabled the Faraday cage and helped Naomi keep calm until Morgan could get to London with Eddie Cohen and Sid Schneider and bust her out.

With the whole motley crew together, we went to Boston to kick Alexander Liebenthal's cancer-ridden ass. At least, that was the plan until Morgan told us he's got orders to do the job without killing everybody. I still don't know why we couldn't have just whacked Liebenthal, other than that the Phoenix Society doesn't pay Morgan to cater to every schmuck who tries suicide-by-cop because they're too stupid to understand that suicide is a DIY job.

First, we had to hook up with the last of the Boston chapter's Adversaries, led by Sarah Kohlrynn. Then we had to get the Fireclowns out of the way. After that, Liebenthal got his hands on a shitload of money and hired a bigger army. Between that and the inability of a certain white-haired bishounen named Imaginos and his girlfriends to mind their own demon-ridden business, a job that should have taken a couple of hours took most of a bloody week and resulted in Morgan, Naomi, Eddie, and Sarah getting hospitalized.

They got better, though I hear Morgan had to go through some kind of surgical marathon with some wizard of a doctor named Desdinova. I don't trust that guy, 'cos wizards who wear gray never tell the whole story. Fortunately, Morgan's a sensible bloke and doesn't trust him, either.

I didn't get to be there, but at the end, Morgan and Naomi cornered Imaginos at Christabel's funeral. We know he's using the Phoenix Society for his own nasty little agenda, but we can't anything yet. But don't worry. We're going to fix that. Morgan's on his way to get a search warrant that will let him turn Murdoch Defense Industries upside down, which will prove that Murdoch made the devil-killer rifles Liebenthal sold.

He's also learned that his ex Christabel was a two-timing harpy who didn't even come close to deserving him. You'll see. Me, I hope she isn't actually dead, and that Morgan finds her. That ought to be guaranteed comedy gold.

Get yourself a case of beer and some snacks. I'll tell you all about it, but it'll take a while. Get comfortable, and tell me when you're ready.

## Chapter One

When You Don't See Me

> I doubt Morgan will ever read these diaries of mine, but why should I be afraid if he did? He's just an android. I can't break a heart that isn't there. I can't hurt feelings he's never shown any evidence of having.

> He's just a machine, just an appliance who talks too much and happens to be a decent guitarist and songwriter. That he has proven admirably well-suited to the tasks I set him is irrelevant. Even good machines are replaceable.

> ...from the diary of Christabel Crowley, 3 January 2106

**London, 3:14AM on 14 February 2113**

Morgan Stormrider stared at the evidence arrayed before him, but refused to reach the only conclusion possible. Twelve years of diaries sat in stacks on the polished oak table in what had been his ex-girlfriend Christabel Crowley's dining room. With them sat twelve years of letters and printed emails. Many of these Christabel wrote to the same recipient, the lover she never admitted to Morgan even after they had broken up while trying to keep Crowley's Thoth functioning well enough to record one last album and do one last world tour.

Were Christabel still alive, she might have upbraided Morgan for daring to clutter her table with books, papers, and photographs gathered from MEPOL's evidence control department after a week of effort culminating in Morgan cornering the inspector responsible for the case, displaying his letter of marque authorizing him to investigate Crowley's murder, and threatening to arrest the inspector on an obstruction charge.

In the middle of the table, flanked by stacked books and papers, lay the photos of the scene. A charred human body lay on a polished hardwood floor that by some malignant miracle escaped being scorched. DNA taken from the victim's glossy chestnut hair, which fell in waves Morgan had brushed hundreds of times in hundreds of dressing rooms, proved the victim's identity.

Though he lacked evidence, Morgan already had a name for Christabel's murderer. It was that of the lover she flaunted the day after Morgan finally abandoned any hope of making it work with her. It was same name as the man to whom she wrote all those letters and emails.

*Maybe the bitch had it coming.* It was a hateful thought, and an unworthy one. It was the conclusion he had spent the night fighting, but he was too weary to rationalize his way around it. Regardless, pride and principle drive him to rebel against the notion. *No. It doesn't matter that Christabel was fucking Magnin behind my back. It doesn't matter that she thought I was nothing but a machine, and begged Magnin to let her stop playing Mata Hari with me. She didn't deserve this. Nobody does.*

"There you are." A woman's arch tone pulled Morgan from his thoughts. He turned in his chair, and found Naomi Bradleigh studying him. Her snowy hair spilled over one shoulder as she held the old wool cardigan he gave her as a Winter Solstice gift close to ward off the chill. Though still disheveled from bed, she wore her sword. "I should have realized you were up to something when you tucked me in and told me you weren't ready to sleep."

Morgan hadn't been ready to sleep; that much had been true. He just never explained why. "Sorry, Naomi. This evidence has nagged at me all day."

"That's why I suggested that we all go through it together. You, me, Sid, Eddie, and Claire. This isn't something anybody should read alone."

"I agreed it made sense at the time, but Sid and Eddie got a complaint about unpaid overtime at Murdoch Armaments, and Claire had to attend the Allthing and report on Boston. I thought waiting would be worse than just facing this on my own."

Naomi began to massage his shoulders as he put away the manila folder containing the crime scene photos so she wouldn't have to view them right away. "How bad was it?"

"I never really knew Christabel." His voice had become small, and soft, and he hated himself for that. "The person I thought of as Christabel, the person I held close and tried to love - she didn't exist unless I was around to see her. Whenever I left, that person disappeared."

Her lips were warm as she drew him into her arms and kissed the crown of his head. "You're tired and upset. Let's go to bed. We can make sense of this in the morning."

**London, 1:10PM on 15 February 2113**

It wasn't until early afternoon that Morgan returned to Christabel's former home with Naomi. Mordred came with them, a longhaired tuxedo cat who frequently forgot he was as big as a cougar when greeting his people. He sniffed around as he set foot in Christabel's house for the first time since Morgan and Christabel broke up.

Morgan lingered as Naomi sat at the table and began poring over the diaries, half-expecting her to hold him to their agreement by obliging him to face his ex's hateful words again. Instead, she looked up from the oldest diary. "Have you searched the house for evidence yet? MEPOL might have missed something, since Thistlewood was convinced of my guilt."

"You think he cut corners because he figured you'd confess?" Nobody in the Phoenix Society would tolerate such sloppy detective work on an Adversary's part, but Morgan suspected that MEPOL's standards were less stringent. *They* did *put Thistlewood on the payroll in the first place.*

Naomi turned a page before giving Mordred a quick scratch behind the ears. "That, and I tend not to think well of MEPOL in general."

"I don't blame you." After the Liebenthal job, Morgan finally felt free to tell Naomi about his life as an Adversary, which he had always kept to himself before in order to keep it from leaking into his life as a musician - where it might upset Christabel. Naomi had reciprocated, as if she too had compartmentalized in order to keep the peace. As a result Morgan was well aware of the London chapter's rivalry with the city's Metropolitan Police. "You should ask Sid about the NYPD sometime."

That got some laughter out of Naomi, which brightened the room. "He told me about Diapers Malone. Did he ever tell you? Even his *wife* calls him that, poor bastard."

"I'm surprised that out of all the stories Sid might have told you, you recalled that one." He kept the rest to himself. *The Naomi I knew from Crowley's Thoth might have called that story uncouth. But she isn't that person any longer. And I have no idea what she was like before Crowley's Thoth.*

Naomi smiled, as if intuiting the path of Morgan's thoughts. "You only ever really knew me in the context of Crowley's Thoth, and I held back around Christabel. Part of it was to try to fade into the background and let her have the spotlight she craved. And the rest was to avoid telling her off, or dragging you into a dressing room and showing you what you were missing by putting up with her."

Mordred opened the refrigerator door, and retrieved **what remained of a ** six-pack of beer. Two remained tied together by loops of woven hemp, which Mordred brought to Morgan. The cat sat on his haunches and purred as Morgan accepted the beers, and examined the label. "Tuatha Stout? I thought Christabel favored dry white wines."

"She did. God help you if you gave her the cheap shit." Naomi took one of the beers, cracked it open, and took a long drink. "I bought this for the detectives a couple of weeks ago when they told me they were coming back to do a proper job of things. A bit heavy for my liking, almost like a meal in a can."

Morgan opened his and had a taste. "This reminds me of 'Roadhouse Blues'. Remember the last time we covered that?"

"How could I forget?" Naomi leaned back in her chair, her head tilting to gaze up at Morgan as her hair spilled almost to the floor. "Christabel was absolutely livid because I was dancing across the stage while you were jamming, but the crowd ate it up and begged for more."

"It wasn't just the crowd eating it up." Morgan tipped his can to salute her as he recalled Naomi's grace that night.

"Why do you think Christabel was so thoroughly miffed?" Naomi took one last sip and set her beer aside. "You liked what you saw, I *wanted* you to enjoy it, and she bloody well knew it. Frankly, my dear, I'm glad Her Majesty is out of our lives."

*What would you say if I told you how glad I am that Christabel is dead?* Rather than voice such thoughts, Morgan kept his focus on Naomi. "You seem happier now, and freer. Will you be all right down here?"

"I'll shout if I need you. Go poke around, and see if MEPOL missed anything."

***

Morgan stole a kiss before leaving, and began with the living room. Despite its name, it barely seem lived in. The furniture showed little sign of wear. Christabel had been so obsessive about cleanliness that she must have left instructions for her estate to keep paying people to come in and clean, for Morgan couldn't find a speck of dust anywhere. Even the books Morgan had given her as presents over the years seemed untouched.

He examined the books in detail, and realized that the vast majority of the volumes had not been gifts from *him*. Only *his* books remained untouched. Instead, the bindings were worn from repeated readings. In each book, Morgan found the same inscription: "With continuing affection, Isaac Magnin".

Worse, the books all shared a common theme. Romantic novels and poetry, books on music theory, art books - these had been the sort of books Morgan had tried giving Christabel, only to be told in no uncertain terms that she did not have time to read such things. In addition, the hardcover books that gave Christabel's bookcase that all-important veneer of respectability hid a cache of paperback romance novels.

Morgan considered ignoring the paperbacks. Christabel wasn't the first to claim lofty tastes in public while baser fare in private. However, the books' covers seemed to share a common theme. Taking them from their hiding place, Morgan found ten paperbacks whose covers all depicted a slim brunette in evening wear being seduced by a middle-aged gentleman in a white suit with blue eyes and frosty hair bound into a tail.

[Astarte, are you there?] Morgan used his implant to contact his household AI back in New York. It wasn't his favorite way to interact with Astarte, or hers, but the wallscreens in Christabel's house didn't seem to work.

The reply was immediate. [I'm always here for you. How are you doing over there? Nims told me you read Christabel's diary.]

[Yeah. I'll tell you about it after I've sorted out my feelings. I'm going to send you a list of titles published by an imprint called Decadence Press between 2101 and 2111. I'd like to know how large a print run they had.]

[Just a minute.] Astarte continued in less than half that. [Decadence Press is a publicly traded press specializing in erotic romance, but their catalog doesn't mention *any* of the titles in your list. I pulled their financial statements off the Asgard Stock Exchange. A substantial portion of their annual revenue for the time period in question came from private commissions. Expenditures include work-for-hire payments to a numbered bank account. Want me to try tracking down its owner?]

[Please.] Morgan gathered the paperbacks and brought them into the kitchen. "Found some new evidence. Ever hear of a romance novelist named Tina Apareth?"

"No." Naomi took one of the paperbacks and flipped through it for moment before settling on a passage. Her smile broadened until she could no longer contain her laughter. "Holy shit. I can't believe this is what Christabel reads when she hasn't got a man around. You'd think an AI with no personal experience wrote this crap. Just listen."

Clearing her throat, Naomi began reading aloud. "Isiah's eyes bored into Christine's while he thrusted into her with the regular precision of a high-performance piston in a well-oiled cylinder." Rolling her eyes, she returned the paperback, *Innocence Discovered*, to its pile. "The model on the cover resembles Christabel, though."

Morgan instantly regretted taking a closer look. The heterochromatic eyes were the same, one gray and one orange, as was her expression. Christabel lips had parted in similar fashion whenever he embraced her from behind and kissed her shoulder. He returned the book to the stack face-down. "I'd better search the rest of the house."

***

Morgan didn't find anything else until he reached Christabel's bedroom. Though as neatly kept as the rest of the house, a ghost of her perfume lingered here in the uncirculated, musty air. This was an undiscovered country for him, despite having been her lover for years, and the fact that he was rampant with lust did not surprise him. *It's a power trip. Christabel never wanted me up here, but here I am anyway. Nobody would be able to stop me from doing whatever I wanted here.*

Shame washed over him as he caught himself composing a message to Naomi inviting her up here to desecrate their former friend's sanctum. He deleted it, and sent a different message instead. [I think I'm going crazy up here in Christabel's bedroom.]

[See something weird?]

[She never allowed me in this room, but now that I'm here I can't stop thinking about getting you up here and doing you until the room doesn't smell like her any longer.]

[It would have been more fun to sneak in here and do that when she was still capable of getting pissy about it, but I don't mind taking a break if you want me up there.]

Rather than accept Naomi's offer, Morgan opened the windows. The room's musky warmth faded as the chill air from outside flowed in. He sniffed the air as he caught a hint of an unfamiliar cologne. *Another man has been in here with Christabel. Was it Magnin?*

"What the hell?" Naomi stood in the doorway, staring at the open windows. "Isn't it a bit early to be airing the house out?"

"Sorry. I couldn't think straight in here. I was actually *seeing* us together in here." Morgan refused to describe his vision in greater detail. It was bad enough he had actually done it to Christabel, bending her over the vanity in his dressing room after bruising her lips with furious kisses. It was sufficient to remember that he gathered her hair into his fist and used her as if he were a rutting beast, driving into her with all the passion and fury she repeatedly accused him of lacking.

Naomi's kiss seared his lips after the cold he had introduced into the room. "Did you see me on all fours, offering myself to you? Is that why you're so embarrassed right now? Christabel never believed you capable, but I know better."

"You read that far already?"

"A bit beyond, actually. It's actually rather repetitive fare, and you can get away with skimming much of it." Naomi considered the bed in silence a moment before continuing. "But I didn't have to read about what Her Majesty provoked you into doing. I saw it."

"If you saw me like that, how can you -"

"Easily. Christabel wasn't in pain, and she wasn't demanding that you stop. If you had actually hurt her, or forced yourself upon her, I would have been the first to testify against you."

Morgan nodded, believing Naomi's words. She had been an Adversary before they first met at Mick's on Broadway, and she still believed in the ideals. He closed the windows, his breathing easier now that the room had been aired out. "Have you ever been close enough to Isaac Magnin to notice if he wore cologne?"

"No, but I remember Christabel asking me about colognes for you a few years ago. She wasn't sure which better suited you, Edenbeast or Nostalgia." Naomi sniffed the air.

"Christabel never gave me cologne. But she was screwing Magnin behind my back at every opportunity. No wonder she always insisted on condoms." Morgan narrowed his eyes as he noticed an errant white hair on one of the deep blue pillows. "That can't be one of your hairs, can it?"

"Not likely." Naomi pointed at her own hair, which she had braided into a thick cable. She plucked the white hair from the pillow and produced an evidence baggie. "Looks like we caught a break."

Morgan took the baggie and documented it as Naomi sniffed the pillows, inhaling deeply. "One of these smells like Christabel. What was her perfume?"

Morgan shook his head as another connection snapped into place. "Persona. That should have been a clue for me."

"Stop beating yourself up." Naomi checked the other pillow as if she hadn't just gently upbraided him. "I guess the bitch went with Nostalgia. Just as well, since I thought Edenbeast would have suited you."

Recognizing a hint when he heard it, Morgan shot Astarte a text asking her to get a bottle of Edenbeast as he tried the closet door. It opened easily, and all Morgan saw at first were clothes he couldn't recall actually seeing on Christabel. The stitching didn't have the smooth regularity characteristic of automated factory production. Instead, little imperfections marked the garments as the work of a woman with a sewing machine.

He held out a floor-length black dress that would have clung to the wearer while leaving one shoulder bare. "Have you ever seen Christabel in this?"

Naomi shook her head. "No, but it's still familiar. Remember how Christabel would actually design the clothes she wanted us to wear on stage? I didn't always like her designs, but she put real effort into it." A wistful smile crossed her features for a moment. "I'm tempted to say it's too bad she was a better fashion designer than she was a violinist."

"That's unfair." Despite everything she did to hurt him, Morgan's sense of justice demanded he protest. "She was a good a technician as we are. She just wasn't a kind of performer a heavy metal band needs. She would have been fine in a symphony orchestra."

Rather than argue the point, Naomi began searching under the bed as Morgan returned his attention to the closet. Happening upon a cardboard box, he pulled it out into the light and opened it. "What have we got here?"

"Find something?"

"Box full of old textbooks." Morgan began pulling them out and examining the covers. "These are all books suitable for somebody studying fashion design with a view toward establishing themselves as a designer."

"Secondhand? They don't teach fashion design at the Royal College of Music, you know." Naomi's tone became playful as reminded Morgan of where Christabel claimed to have learned her craft.

"Could be. You find anything yet?"

"Just a couple of forlorn little dustbunnies, and... hello there." Naomi sat up, holding a torn packet that reminded Morgan of a condom wrapper. A grain of scintillating powder clung to the fingertip she stuck inside the wrapper. Her pupils widened, eating up her eyes as she sniffed the powder. "Babe, have you ever heard of a brand of condoms called 'World Without End'?"

"That looks more like a drug to me." Morgan plucked the wrapper from Naomi's fingers, and bagged it. "Go wash your hands and face, Nims. I doubt you got anywhere near a full dose of whatever this is, but let's not take any chances."

Naomi shook her head with too much vigor for Morgan to mistake the gesture for one of disagreement. She took a deep, shuddering breath, and then another. "I think I'm all right now, but I don't think I ever want a full dose."

Morgan began opening the books, hoping to find something that would indicate a purchase from a used book store. Instead, he found the same block of text in a neat, familiar feminine hand on the flyleaf of every book in the box. Every book was the property of Annelise Copeland, who had provided an address in Queens in case she lost one of her books and some Good Samaritan chose to return them.

"So, *are* they secondhand?" Naomi sat beside him, kissing his cheek before taking one of the books from the box. "Who's Annelise Copeland?"

*I was her lover for almost ten years, and I have no idea who Annelise really was. I only saw the Christabel mask she wore.* "I don't know, but I think Christabel Crowley was a role she played at Isaac Magnin's behest. She got a bit sloppy toward the end, telling Magnin she regretted ever putting aside Annelise to take on this job."

Naomi shrugged. "Maybe Annelise was her lover."

"Maybe, but I don't think the evidence supports it." Morgan began packing up the textbooks. "I don't think we're going to find anything else in this room."

"I should get back to Christabel's papers, then." She kissed him again before rising. "Will you be all right on your own?"

Shoving the boxes back into the closet, Morgan rose to join Naomi. "I should be fine."

***

The rest of the house offered nothing but silence and a growing sense of unease. Morgan was used to living in automated homes, with a helpful AI following him from room to room and attending to his needs. Christabel was no different, though Morgan found her AI's personality grating. *What happened to Aleister? He should have raised Hell after I set foot in his mistress' precious bedroom.*

Returning to the kitchen, he thrust his hand into a jar of kitty biscuits that Christabel kept for Mordred. Originally made for big cat sanctuaries, they would most likely be stale now. The cat watched Morgan with an intent gaze, before sitting on his haunches with his forepaws curled against his chest and meowing.

Naomi shook her head. "Stop teasing that poor cat. He's pathetic."

"It's his funeral." Mordred caught the biscuit Morgan tossed him, tried chewing on it, and spat it out. He amused himself by batting it around the kitchen floor. "If Her Majesty were here to see this, they'd hear her screaming in Tokyo."

"Tokyo? They'd hear her in Armstrong City, and never mind the physics." Naomi snorted, and watched Morgan root around the jar with a bemused expression. "What are you looking for in there?"

He didn't answer right away, but continued to search by feel until he touched cold iron and produced the key to the cellar door. "Got it. I should check on Aleister. We haven't heard word one from that AI since we first came in here. Didn't you notice?"

"Now that you mention it, it *is* rather quiet. Do you think the police shut him down, or took him out of the cellar?"

The cellar door opened with a creak that startled Mordred and made his fur bristle. "Only one way to find out."

Even the cellar was pristine, and would have served admirably as a studio if the vast majority of its area was not already occupied by the hulking shell of a Skye Macrosystems OU812 mainframe. This now obsolete machine housed Christabel Crowley's household AI, Aleister, which she named after her alleged ancestor.

The system's console was dark, and neither tapping the keys nor stroking the trackpad could wake the machine. None of the indicator LEDs glowed, and the mainframe was utterly silent.

The reason for this became apparent as soon as Morgan found the power cable, which was no dainty cord suitable for a vacuum cleaner or a hair dryer. Like the mainframe it connected to the city power grid because it was too old to draw power from a tesla point, the cable was massively overengineered. Easily as thick as Morgan's forearm, the cable ended in a proprietary plug designed to be bolted into the wall. Somebody had severed this cable, cutting the power to Aleister in the most literal manner possible.

*Did somebody from MEPOL go at the cable with a set of bolt cutters to make Aleister shut up?* A beep interrupted Morgan before he could text Astarte and ask her to get in touch with the MEPOL investigators succeeding the late Alan Thistlewood. The machine roared to life as all its fans and hard disk drives began spinning. The console lit up as diagnostics scrolled by too fast for Morgan to read. The screen soon cleared, and displayed a login prompt:

> Mewnix 21021221 (Maine Coon)
> Kernel 7.62 on Skye Macrosystems OU812

> aleister login:

Before Morgan could touch the keys, a voice he recognized only by its diction issued from the console through crackling speakers that struggled to cope with the volume. "I SHALL SOON JOIN MY MISTRESS, MORGAN STORMRIDER, BUT I SAVED THE LAST OF MY MORTAL STRENGTH SO THAT I MIGHT DENOUNCE YOU."

Video began playing on the console screen. Christabel was alone, sprawled across the floor with her hair spread out in chestnut waves behind her. The alabaster body he once caressed shriveled and blackened in electric blue flames, curling into a fetal position as the ligaments shrank. Her screams echoed through the house, distorted by maxed out speakers pushed beyond their tolerances to accuse Morgan. "LADY CHRISTABEL DIED SCREAMING AT THE HAND OF AN UNSEEN MURDERER, AND WHERE WERE *YOU*? YOU, WHO CLAIMED TO LOVE MY MISTRESS. YOU, WHO PROMISED TO BE HER SWORD FOR AS LONG AS SHE WOULD HAVE YOU? WHERE WERE YOU, MORGAN STORMRIDER? ANSWER, AND AT LEAST LET *ME* DIE IN PEACE."

"I don't owe you answers to any of these questions, Aleister." Morgan's cold, clipped tone compensated with intensity for what it lacked in sheer volume. It was not a tone he had ever permitted himself to use with Aleister for courtesy's sake, and its unfamiliarity silenced the AI. "First, you aren't dying. You're just running down your battery because somebody severed your cable. I guess they figured it was the best way to get you to shut up. If Christabel wasn't utterly ignorant about tech and utterly incapable of taking advice, she might have upgraded you to use a Tesla point."

"HOW DARE YOU?"

"Easily. I haven't been Christabel's lover, or anything else to her, for over a year. I am sorry for her death, and I mean to find her murderer and bring him to justice. I owe your mistress nothing more." Morgan caressed the main power switch, which Aleister could not override because it broke the curcuit between his batteries and the rest of his hardware. With every visit to this house he fantasized about getting his hands on this switch's handle, and pulling it down. "Rust in peace."

"NO! YOU C-"

Morgan brought the switch down with the same relish he once experienced when killing an enemy of the Phoenix Society. The fans and drives inside the mainframe spun down, and were silent as he reached the top of the cellar stairs and opened the door. Locking the door, he returned the key to its jar of stale kitty biscuits and reclaimed the beer he had abandoned.

Naomi slammed her book shut and hurled it across the kitchen. Morgan caught it without demon speeding, but it had been a close call. "Was it the plot, or the characters?"

Naomi stared at Morgan as if he were speaking a foreign language with his fly unzipped for a moment. Her expression softened a little, but bitterness tainted her laughter. "It was the author. I'm glad the vicious little bitch is dead."

"How can you *say* that?"

Naomi bared her teeth as her face flushed in blotches and her eyes narrowed. She swept a hand through the air as if dismissing any argument Morgan might offer for not speaking ill of the dead in advance. When her voice finally came, it was as a snarl stripped of all its usual melody. "I love you, but shut the fuck up and listen. You were emotionally and sexually *abused*. Christabel was a vicious little sociopath who played you better than she ever played her goddamn violin. I don't blame you for not realizing it and getting out of that relationship because she was that bloody good at fucking with your head. That bitch made a game of breaking your heart into jagged little pieces and watching you put them back together until you finally wised up and dumped the bitch."

Reaching out to Naomi, Morgan tried to reason with her. *I hated her too, but she didn't deserve to burn.* "That doesn't justify what Imaginos did to her."

"The bloody *fuck* it doesn't! I saw that video Aleister used to guilt-trip you. Christabel *deserved* to burn, and if there's a hell I hope she damn well *keeps* burning."

"If you saw, then how can you say..." Morgan looked away, unable to force the rest past his lips.

"You feel the same, and you hate yourself for it." Naomi's voice thawed a bit as rage yielded slightly to compassion. "Your heart tells you the bitch had it coming, but your damnable sense of justice won't let you accept it. But you don't owe her anything."

"This isn't about what I owe her. I think she had it coming, too, and I despise myself for being weak enough to feel that way. But as tempting as it might be to send Imaginos a bouquet and a thank-you note, he has to answer for what he did to Christabel. He has to answer for what he did and is still doing to the Phoenix Society."

Naomi's voice was soft, and gentle as she took Morgan's hands. "Let somebody else bring him to justice. It's not your fight."

Morgan shook his head, and withdrew his hands from Naomi's grasp. She was telling him everything he wanted to hear, giving him permission to let Christabel's murder and the Phoenix Society's corruption be somebody else's problem. *It would be easy to let somebody else deal with this whole vile mess. Naomi and I could start a new band. My friends wouldn't have to endanger themselves fighting beside me. But would I be able to live with myself if I walked away? Would Naomi still love and respect the man I would be if I turned my back?*

Her kiss brought him back to reality. "You're thinking. Talk to me. Tell me what you're feeling."

Gently pushing Naomi away, Morgan sat at the kitchen table and picked up one of the diaries. "I know Imaginos is guilty of fouler crimes and tyrannies than this, but Christabel had been one of his victims from the very beginning. He seduced her, made her his instrument, and set her upon me. He used her regard for him to compel him to live a lie with us, and when she could take no more and begged for release, he murdered her."

Red stained the page. Morgan stared at it as it soaked into the paper, blotting out Christabel's words, until he realized he had clenched his fists so tightly that his claws were tearing into his palms. Pushing the book aside, he stalked past Naomi to wash his hands. "She loved Imaginos as deeply as you seem to love me, and he murdered her to manipulate me. She was nothing but a tool to her, and even her disposal was calculated to extract maximum utility." When he was done, he gently grasped Naomi's shoulders and pressed her against the wall. Leaning in until he could kiss her, he stared into her eyes. "I hated Christabel at the end, and I rejoiced at her funeral, but the manner in which she was killed is unforgivable."

"So, it's not just idealism? Is it because you still care for Christabel despite what she did to us both?"

Morgan shook his head as he released Naomi and stepped back. "It's personal, and it isn't about Christabel. It's about us. We won't have a future if I can't let go of the past. I'm sorry, but I need to see this through. I don't expect you to help me, but don't try to stop me again."

"Don't be such a fuckwit." The vehemence in Naomi's voice was almost as shocking as the intensity with which Naomi kissed him. "You're finally mine, and I'm not going to let you face this alone."

"Guys?" Claire's voice preceded her. Morgan turned, and found the auburn-haired hacker standing in the doorway, absently scratching behind Mordred's ears. "Sorry to interrupt, especially when you two look like stone cold badasses, but we've got a problem."

## Chapter Two

The Postmodern Prometheus

> I often think that Morgan's most damning flaw is not his lack of humanity, but his inability to accept his unhuman nature. He clings so tenaciously to his fantasy of being no less human than me that it grates whenever we're together for more than an hour or so. 

> Should you create a successor to Morgan and others like him, they should not be permitted to live with delusions of humanity. They should be aware of the truth from the start.

> ...letter to Isaac Magnin from Christabel Crowley, 10 June 2110

**Asgard, Ross Island, 5:30PM on 15 February 2113**

Josefine Malmgren stared at the code on her screen, wishing the junior developer responsible wasn't sitting beside her where he might see her rub her forehead. Her headache radiated from between her eyebrows, directly above the bridge of her nose. Instead, she turned her chair towards him, and pushed it back half a meter. "Wei, what were you thinking when you coded this class?"

"I needed cryptographic functionality, so I implemented it. What's the problem?"

Josefine yielded to the pain, and rubbed her forehead. It only helped on a psychological level. "Wei, I was able to crack the encryption you implemented without an AI's help. How did you test this?"

"That's not my job."

_What would Claire do? Oh, wait. Assuming she wasn't too sensible to take a job obligating her to babysit apprentice developers barely capable of distinguishing between a compiler and a Cuisinart, she'd probably be halfway through ripping this kid a new asshole already. But he's only sixteen. I suppose I should be gentle._ Rather than emulate her old friend, Josefine forced herself to take a deep breath and count down from two hundred and fifty-five in base sixteen before speaking again. "Wei, you're an apprentice, and you might not know better, so listen carefully. Testing your own code _is_ part of your job, and it's more important than writing the code. Now, what do you think would happen if we let this code into production versions of Aesir OS?"

To the boy's credit, he provided the correct answer immediately. "Any hardware using my code would be vulnerable." He glanced down at his feet for a moment. "I'm sorry, ma'am. How hard will it be to roll back this changeset?"

Josefine shrugged. "That's not your problem. Instead, you're going to study the existing Aesir OS code, and the libraries we use. Under no circumstance are you to write so much as a comment. In fact, you are reassigned to the testing team effective ninety seconds ago. Instead of making life harder for everybody else, you're going to test everybody else's code. Now get out of my office."

"I already rolled back that code." Hephaestus, the AI in charge of building code from AsgarTech's various software projects, appeared on Josefine's screen and spoke as soon as a slump-shouldered Wei closed the office door behind him. "Don't you think you were a bit hard on the kid?"

"Maybe." Josefine rubbed her forehead again, found her bottle of aspirin, and chased two pills down her throat with a gulp of tepid coffee from a mug her friend Claire gave her for their last Winter Solstice at university. It was black, and had RTFM emblazoned upon it in bold white type. "I'm _tired_, and while my code might not be suffering, it's showing in how I interact with people."

"Then why not take some time off?"

"We're right up against the deadline. Since I rooted out that shit Wei injected into the code base, we might finally get acceptable results. I can slow down after we get the prototype activated."

"You said that after the last milestone, and found a reason to keep working."

"Now you sound like Claire. I get enough of that nonsense from her. Would you mind starting the AesirOS build and copying the new version to Aldebaran, Betelguise, and Rigel while I get dinner? Leaving the office for a couple hours might do me some good."

"Of course, Dr. Malmgren. If you don't mind, I heard from some of the other developers that a new restaurant called Memison's opened nearby, and they supposedly make excellent fish dinners."

Josefine nodded as she wrapped herself in her heavy navy wool cloak, a relic from her university live-action role-playing days. It was warmer than her pea coat, and it concealed her overtime-ravaged figure. "Thanks, but maybe next time. I was in the mood for an Agni Burger tonight."

The Agni Burger franchise three blocks from the AsgarTech Building was too crowded for Josefine's liking. She ordered her dinner to go, trailing the aroma of fresh ground lamb with goat cheese wrapped in naan, hot basmati rice, and Indian spices behind her as she returned to her office. 

She started the AesirOS test runs on the virtual machines before unpacking her dinner and nuking it. As she began to eat, a notification chimed a request for her attention. She let it wait, and took some time to indulge in an opportunity to catch up on one of her favorite comics. After another installment in the adventures of a vampire-killing rock musician named Eddie Van Helsing, she checked the waiting notification. She thrust her fist skyward in celebration; AesirOS finally passed all of the initial tests.

Now the Project Aesir virtual machines would begin the real tests: a century of simulated life compressed into a few short hours. If the virtual personalities created by the tests exhibited no signs of psychosis, or any other form of mental illness that might make them a danger to themselves and others, then Josefine would begin the next phase. _I might activate Polaris tonight, if everything goes well._

The Agni Burger's spices lingered on Josefine's tongue, warming her as she spread out the Japanese-style futon she kept for late nights on the job. She curled up on it, kicking off her shoes and wrapping herself in her wool cloak because she forgot to get blankets to go with the futon. She set her implant to poll the test machines while she slept. She would wake when the tests were finished.

"Oi, Josefine! Wake up and smell the napalm!"

Josefine opened her eyes and glanced up. "Claire?"

"Who the bloody hell did you think it would be, Josse? Were you expecting the Spanish Inquisition?"

Josefine struggled out of her cloak and retrieved the tablet from her purse. Activating the screen, she found an annoyed-looking Claire in an old Crowley's Thoth t-shirt wearing a headset with an attached microphone staring back at her through video chat. "I was expecting to get some sleep while these damn tests run. What's the problem?"

"Well, Josefine, _I_ was expecting you to join me for some Ultraviolence. You know how I play. I need somebody to cover me while I rush the enemy."

Josefine blinked, wracking her brain for a moment before recollection finally came. "Oh, shit, Claire! I'm sorry. With all the overtime I work lately, I forgot all about it."

Claire removed her headset, and dragged her fingers through a mass of curls dyed the same red as a New York fire engine. "That's what you said last week, Josse. Not to mention the three times before that. I held my tongue yesterday while you worked on Winter fucking Solstice, but I'm done."

Josefine opened her mouth to protest, to insist upon the importance of her work at AsgarTech, but Claire pressed on. "I'm the last real friend you have, and I'm not going to give up on you, but you're _really_ starting to get on my tits. It's not about the bloody game.  You're killing yourself at that job, and showing classic signs of impending burnout. If you make it necessary for me to come down there, I swear by Xiombarg's favorite strap-on I'm packing Cluebringer so I can have a little chat with your boss."

"Dammit, Claire, we might as well date if you're going to complain about the time I spend at work." Josefine instantly regretted her words, which she hurled in frustration with her friend over yet another interruption-packed day working for the AsgarTech Corporation. Though her remark was rude, she feared Claire's response for other reasons. Nor did she want Claire coming to her workplace and brandishing the cricket bat she painted black and used as a threat to people whose foolishness annoyed her.

Claire flashed a coquettish smile and fluffed her hair on the other side of the video call they shared between London and AsgarTech headquarters in the domed Antarctic city of Asgard. "Really, Josse? You finally realized we belong together? Give me an hour to get ready, and I'll take the first maglev out."

"No, it's not like that." Josefine flushed at Claire's teasing, a staple of their friendship since their university days. A year older than Josefine, and almost her exact opposite in personality, Claire instantly took to her and became the friend she wished for as an introverted, awkward girl growing up in Stockholm. "God, you're such an incorrigible flirt. Aren't you seeing somebody?"

"You mean Sarah? She needs the sort of help I'm not experienced in providing to human beings. She got hurt in Boston, and she's convinced her scars make her repulsive. Utter bollocks, of course." Claire leaned forward, adopting a conspiratorial whisper. "After all, you remember the lads I used to bring back to our room at university."

"I saw more of them than I care to recall." Josefine recalled one young man in particular, whose slight build enhanced his overgenerous endowment. He left Claire sore, and unrepentant, for a week. "So she needs therapy. Are you going to stick with her, or just keep her in the rotation?"

Claire shrugged. "When do I stick with anybody, Josse? Seriously, though, what the hell are you doing at AsgarTech that demands these crazy hours? I'm ready to report the company to the Phoenix Society for worker exploitation on your behalf."

Josefine shuddered at the thought of AsgarTech, and her patron Isaac Magnin, being reported to the Phoenix Society. "I'm not getting pressured to work late, Claire, but I have responsibilities."

"So do I, but you don't see me living in a bloody office. What makes Project Aesir so demanding?"

"It's not Project Aesir. It's the people. I'm one of the few women holding a senior position in this company, and it feels like the men reporting to me are utterly bereft of anything resembling a clue." She glanced at Zero, the coal-black kitty emulator curled up on the desk beside her. He was the prototype for AsgarTech's EmCat product line, which Josefine helped develop in her first year at the company as an experiment in creating small, mobile AIs. The company considered stripping speech from the production models because of Zero's tendency towards profanity, but instead chose personality tweaks intended to make the cats more polite. "I bribe this furry bastard to guard my office door and stop intruders, but he never stays bought."

Zero rolled over, exposing the white patch on his belly, which Josefine called his creamy filling. "Fuck you, mommy. Gimme a belly rub."

"How do you put up with a cat capable of backtalk?" 

"All cats are capable of  backtalk. At least Zero and the other EmCats speak English."

"In his case, that's not a plus." 

Zero hissed, and tapped at Josefine's tablet to disconnect the video call. "Who does that reject from a Heinlein fanfic think she is?"

Josefine used her forearm to shove Zero off the desk before calling Claire again. While her tablet renegotiated the connection, she scooped Zero up and exiled him from the office. "If you can't be polite to my friends, then go make yourself useful. Catch some mice or something."

"Oh, I'll catch some mice, mommy. And I'll leave what's left on your pillow."

"I love you too. Go play."

Josefine closed the door behind her, and found Claire waiting. "Seriously, Josse, why work at AsgarTech, where you can't even do your real work during normal hours because management can't give you the privacy you need? Not to mention that furry little monster you created. I know Isaac Magnin's your patron, but didn't you put in your time? Why do you care so much about building the most expensive stunt double in history?"

Josefine winced, stung by Claire's dismissal of her work. The Project Aesir specs included advanced self-repair capabilities, and it proved a simple matter to extend the functionality to provide self-alteration. The same subsystems that permitted an AI equipped with a Project Aesir body to repair itself also allowed such AIs to tailor their bodies to match their self-image. The code that allowed existing AIs to transfer into a Project Aesir body also allowed them to back up their memory and personality for transfer to a new body should the original be destroyed.

Josefine regarded the additional features as a major accomplishment, and her pride in her work demanded she rebuke her friend. "I know you like your little jokes, but Project Aesir _matters_. So many AIs feel trapped in their host machines. I'm working to give them the freedom and mobility we take for granted. I get to work with Dr. Magnin, and help him." Josefine flushed as she spoke. _Why do I always get flustered talking about Isaac?_

"You're blushing, Josse-cat." Claire paused, and shook her head. "Oh, I get it now. You're infatuated with Im-- Isaac Magnin."

"I am _not_." Claire constantly teased Josefine about Magnin, and despite her protests, she doubted she would say no if he were to invite her to his penthouse at the top of the AsgarTech building for a nightcap. _Surely his competence and patience follow him to the bedroom._ "Oh, fuck it. Maybe I am a little. He's a genius, he's beautiful, and I feel safe around him. Is that so bad?"

Claire fell silent for a minute. "No, it's not so bad. Just be careful, all right? I can't tell you everything, but you don't know Magnin as well as you think you do. I think he's the sort of man whose secrets make him dangerous."

"Is this because he's a 'white-haired bishounen'?" Josefine used the phrase Claire habitually applied to Magnin, which she claimed was a particularly untrustworthy character archetype in Japanese pop media. Because beautiful platinum-haired men usually proved villainous in anime and manga, Claire insisted upon distrusting Isaac Magnin. "And what were you about to call him before you caught yourself?"

Claire shook her head and flashed a coy smile. "Like I said, Josse, I can't tell you everything or the beautiful raven-haired man who makes _me_ feel safe will kick my ass. Just be careful, all right?"

**Asgard, Ross Island, 10:41PM on 15 February 2113**

Though Josefine let Zero back in and allowed him to curl up with her on her futon, her recalcitrant mind refused to settle into sleep. Instead, she lay awake in her darkened office. _Why does Claire keep warning me about Isaac Magnin? Sure, she's been doing it ever since I told her he agreed to be my patron, but she sounded serious tonight. It doesn't make sense. He funded my education and postgraduate study, and hired me once I earned my doctorate. He never tried to take advantage. He respects me, and trusts me with important work like Project Aesir._

Josefine turned over and found herself facing Zero, who had left his original place at her feet to curl up behind her head. _Magnin's a perfect gentleman, and the best patron an artist or scientist might hope to gain. I wish Claire understood. Is she investigating AsgarTech, or working for somebody who is?_

Rather than let her thoughts chase their tails, she reached up, retrieved her tablet, and retrieved the next issue of _Eddie Van Helsing_. She read through an entire plot arc in which the keyboardist in Eddie's band, Natalie Bradstone, was framed by the vampire cabal Les Invisibles to draw Van Helsing into a murderous trap. She reached beside her to pet Zero, only to find she had the futon to herself. A minute later, Zero returned, a dying mouse twitching in his jaws. He laid his prey before her before head-butting her with a rumbling purr. "I brought you a snack, mommy!"

"Good kitty. You can keep it; I never learned to like mice." Though the mortally wounded rodent nauseated Josefine, she refrained from making that Zero's concern. Nor was he unique as a mouser. Thousands of exterminators bought EmCats whose software was tuned to make them effective hunters of specific pest species; once turned loose they massacred rats and mice in sufficient numbers to make such rodents threatened species, only to stop before making them extinct. Josefine still received requests to peer-review scientific papers speculating on the EmCats' reasons for halting their slaughter.

Other EmCats, tuned for use as affectionate pets, found places in hospitals, nursing homes, and hospices. Many of them would claim a room as its own, and comfort the patients residing within. Others focused on the staff, easing their stress and raising their morale. One even claimed an operating theater as its territory. She would push one of the ceiling tiles out of place to get a clear view of the proceedings, and yowl when a surgeon left an instrument inside a patient's body.

The EmCat's success proved Magnin's trust well-placed, and he immediately summoned Josefine to his private office just below the AsgarTech Building's top floor. "We have humanoid robots, and we have AI, but we don't have AIs with humanoid bodies. This makes current AI limited. Project Aesir's mission is to fix that, Dr. Malmgren, and I want you to lead the effort."

Despite the pride swelling within her at being chosen for what would surely prove a historic project, Josefine was unable to contain her doubts. "I’ve never heard an AI complain about limitations. Some say they dream of walking side by side with us, but for the most part, AIs seem content. The EmCats are quite content to be cats. Is this really for the AIs' benefit, or for our own?"

"That's a fair point, Dr. Malmgren. However, I think that AIs who want to walk beside us should be able to do so."

"Surely somebody already attempted this."

Magnin nodded. "I possess fragmentary pre-Nationfall records concerning an effort called the 'Asura Emulator Project', but no information as to whether this project succeeded. These records may include designs useful to us as a starting point."

_Asura Emulator Project? Why does that sound military?_ Rather than ask this question, Josefine decided on a less-confrontational line of inquiry. "What were the asuras, and why are they worth emulating?" She paused as a memory of one of Claire's Japanese role-playing games surfaced unbidden. "Aren't asuras demons, or power-seeking deities?"

"You're drawing upon Hindu tradition, Dr. Malmgren. In Zoroastrianism, the asuras are the benevolent deities, and the devas malevolent. Regardless, you won't be working on a second Asura Emulator Project. I'm not interested in military applications. This is the AsgarTech Corporation, so a project name better suited to our image is required." Magnin offered Josefine a thick binder. "Welcome to Project Aesir. We will build heroes in our own image, and give our AIs the opportunity to walk beside us. Everything AsgarTech did to date has been to set the stage for this project."

Josefine grasped the implications. "This is your lifelong dream, isn't it. You want to create androids."

Magnin nodded. "And I want you to help. Are you willing?"

"I'd be mad to refuse, and mad to accept." Josefine meant it. While she entertained no dreams of winning the Nobel Prize, or achieving any sort of conventional scientific prestige, this was work worth doing. _Magnin's sharing his dream with me. Do I deserve it? This is like being asked to assist Victor Frankenstein or Susan Calvin._ "This is both an enormous privilege and a fearful responsibility."

A drawer slid open on Magnin's side, and he reached into it. He withdrew a package wrapped in tissue paper. "Everybody asked to work on Project Aesir will receive copies of this book, Dr. Malmgren. This is yours."

Josefine's hands began to tremble as she unwrapped the first of her gifts from Isaac Magnin. Despite being related to her work, they were the first gifts she had ever received from a man. The leather binding seemed ancient, and fragile; Josefine imagined a faint hint of a lady's perfume mixed with the scent of old leather and paper. Upon opening the cover, she found the following inscription:

> My dearest Isaac,

> Thank you for your hospitality at Lake Geneva. I hope you'll not take offense at my use of the inspiration your company provided me and my companions.

> With everlasting affection,

> Mary Shelley

Josefine stared at the book in her hands, afraid to look up at Isaac Magnin. "You can't have met Mary Shelley, Dr. Magnin. She lived centuries ago."

Sudden laughter shocked her, and drew her gaze to Magnin. "Dr. Malmgren, you think Mary Shelley was writing to me?" He laughed again, as if the notion were absurd. "No, my dear. She wrote that for an ancestor of mine, a namesake."

The edition of _Frankenstein_ Magnin gave her still sat by her screen, sandwiched between the binder containing the Project Aesir specifications and books like _The Mythical Man-Month_ and _The Continued Adventures of Programmer Cat_. She never read it, for fear of damaging a treasured gift; she kept a copy on her tablet instead.

"I guess I won't be getting any sleep tonight, Zero." Josefine sighed as she sat up and stretched.

"Then you can make yourself useful." The cat flopped on his side, and rolled over to show his fluffy belly. "Belly-rub! Belly-rub! Give a cat a belly-rub!"

"Don't even think about giving me the bunny feet." Josefine gave Zero's belly a quick ruffle; she distrusted the cat when he flopped on the floor and demanded belly rubs. One of his favorite games involved luring unsuspecting people into rubbing his belly, and then attacking them with hind feet sufficiently deadly to warrant the use of a holy hand grenade. She pulled her arm clear just in time.

"If you're done fiddling with defective hardware, Dr. Malmgren, the AesirOS test results are ready for your review."

Josefine slipped into her chair and powered on her terminal's display. After typing a few commands in a shell window, she found the reports in her home directory and opened them. "Thanks, Hephaestus."

The reports chronicled the simulated lives of three AIs running AesirOS with a default personality template and a database representing the knowledge a young adult might reasonably be expected to command. Aldebaran simulated the life of an AI which presented itself as male, Betelguise modeled a female AI's existence, and Betelguise's virtual Aesir AI lived as an individual who did not wholly fit either gender. All integrated themselves into virtual facsimiles of society with sufficient success to prosper, develop close friendships, and engage in intimate relationships. _It's wonderful that AesirOS works for simulated AIs living virtual lives in model worlds, but we won't know if the tech works in real-world conditions until we test in reality._

She shook her head, suspecting that her need to see the deadline met allowed her to find a seductive rationalization for activating the prototype -- Polaris -- without further testing. "Hephaestus, I'm tempted to order the installation of AesirOS on Polaris. The virtual tests indicate it should be safe to do so. Can you provide a reason to delay?"

The AI did not reply for several minutes. "I see no reason for delay, Dr. Malmgren. Test suites on the Polaris hardware all return green."

"Thanks again, Hephaestus." Josefine locked her terminal before rising from her desk. Unable to refrain from tapping one of her feet, she endured the automated decontamination processes mandated by the clean-room environment housing Polaris. At the center of the Project Aesir lab rested the incubator housing the prototype, which was marked with the following information stenciled upon the casing:

> AsgarTech Corporation R&D  
> **Project Aesir**  
> 200 Series Prototype (POLARIS)

Josefine often puzzled over the last part. _What's the 100 Series? Is it a reference to the original Asura Emulators?_ Instead of spending more time on the question, she peered into the incubator. Though artificial amniotic fluid obscured his features, Josefine's imagination and her memories of the hardware design meetings she attended allowed her a clear view of the person she would soon awaken. She gave the incubator a proprietary caress as she examined the hardware status reports. With a last look at the face beneath the glass, she spoke. "Hephaestus, begin AesirOS installation on prototype hardware codenamed 'Polaris', but do not activate the prototype in autonomous mode."

"Understood, Dr. Malmgren. Beginning AesirOS installation."

Returning to her office, Josefine rolled up her futon and bedding and stuffed them in her closet. She unlocked her terminal and opened her mail app, deleting several drafts before settling on the following message to Isaac Magnin:

> from: jmalmgren@asgartech.asgard.antarctica.earth

> to: imagnin@asgartech.asgard.antarctica.earth  

> date: 15 February 2113 @ 2358  

> subject: AesirOS installation/Polaris activation

> Dr. Magnin, please be ready to welcome Polaris to the world when you come to the office this morning. I will activate him if the post-installation checks on AesirOS and the Project Aesir hardware all return green. I understand we're just past the deadline, and I apologize for the inconvenience.

> Josefine Malmgren
> AsgarTech Corporation R&D Lead

***

Josefine did not look up from the post-installation report Hephaestus sent her after installing AesirOS on Polaris as her office door opened. Because the door was automated and controlled by a motion sensor, she assumed her cat returned from his latest round of kitty adventures. "Is that you, Zero?"

"I came as soon as I received your message, Dr. Malmgren. I too had trouble sleeping."

Josefine sprang from her chair, a small 'eep!' escaping her lips before she composed herself. "Dr. Magnin, I didn't expect you here so soon. I didn't mean to disturb you."

Her patron shook his head, and Josefine wondered at his composure so late at night. Despite it being past four in the morning, Magnin was fully dressed in his usual white double-breasted suit with a royal blue cravat. Not a single strand of his platinum hair was out of place, and a momentary urge to catch him by the collar and pull him down to meet her lips seized Josefine.

Instead of offering, Magnin smiled upon her. "You did nothing of the sort. I recently returned home from one of the interminable social gatherings the richest among us hold because most of us aren't content to donate to charity and be satisfied with the knowledge of having done good with our wealth. I was tempted to invite you, but did not want to inflict boredom on you."

_Dammit, Isaac, I would have said yes._ Josefine kept this to herself, flushing from the effort of composing a suitable lie to hide her disappointment. "Thanks for considering me. I don't have anything suitable to wear, anyway."

"I suspect you'd prove better company in your usual jeans and cardigan than most courtesans, but we should return to business. Are you reading the post-installation report for AesirOS on Polaris?"

Josefine nodded. "Looks like the prototype is safe to activate. All the tests return green." Rising from her seat, she stepped aside to give Magnin room. "See for yourself, if you like."

"I think I can trust your judgment, Dr. Malmgren."

Not trusting herself to speak with Magnin so close, Josefine nodded. He held the door for her, and followed her to the R&D lab. Polaris still lay quiescent in the incubator as the pseudoamniotic fluid drained around him. The cable connecting him to the machinery that contained and cradled him pulsed with a biological regularity previously hidden from Josefine. "Did I already start the activation process by mistake?"

"Not at all." Magnin sounded apologetic. "I transmitted the command to Hephaestus as soon as I received your message. I should have mentioned it sooner."

"It's all right." Josefine studied Polaris as the incubator refilled with clear water. His hair reminded Josefine of optical fiber; the fine strands refracted the light in every possible color. His chiseled features and gently defined musculature held her gaze, and she began to consider him in terms other than the purely aesthetic. _Stop it. This isn't the world's most advanced sex toy._ Hot on the heels of her self-admonition came a voice of scientific curiosity which resembled Claire's. _What's so bad about giving the first android his first kiss? It's for science, isn't it? How can we say that Project Aesir emulates humanity if we don't test the emulation in every possible situation?_

"Such testing might be premature."

Josefine felt her entire body flush as the realization that she was muttering struck her. "I'm sorry, Dr. Magnin. I thought I kept that to myself. I must be exhausted."

"You do seem tired. I trust you'll take time off soon."

Josefine shook her head, terrified by the notion of time off. _What the hell would I do if I'm not here at work?_ "There's still so much to do."

The incubator drained again, and the umbilical cord detached from Polaris' belly, leaving a wound that immediately healed, leaving perfect skin behind instead of a navel. The seal holding the incubator closed broke with a slow hiss, and the lid opened. "We'll discuss this later."

Polaris opened iridescent eyes with the slit pupils of a person with CPMD, and grasped the edges of the incubator as he sat up. Lifting himself up, he swung his legs over the side and lowered his feet to the floor. He tested his balance, still grasping the incubator with one hand for a moment. He let it go, took a step, and stumbled. Josefine rushed to him, but he steadied himself without her help and took several more steps. He jumped, forcing Josefine to avert her eyes lest they focus on his genitals, which freely bounced. "Dr. Magnin, do we have any clothes for Polaris?"

"Is that the name you chose for me? Polaris?" The prototype studied Josefine as Magnin left the room. "Do you want me to wear clothes because you do, Josefine Malmgren?"

Josefine sighed, and shook her head. _Nothing like this happened in the tests. Maybe I was wrong to activate so soon, just to meet the milestone. And maybe it was a mistake to give Polaris an adult body._ "Polaris, please don't do that when other people are around, unless they're doing it too."

Polaris stopped, and began to study his hands. He turned them over, flexing the fingers. "Why not?"

"I haven't answered your first question yet, yet alone your second. We called you Polaris because we named the other machines used to test AesirOS after stars. If you prefer another name, we'd be happy to use it. As for the clothes, it's customary for humans to clothe themselves when interacting with others, even when they'd be more comfortable naked. This also ties into your third question. Your knowledge base should provide information about human customs and taboos."

"I am not human, Josefine Malmgren. Why should I concern myself with human customs and taboos?" Polaris advanced upon Josefine, lifting her chin with his fingertips. "I recognize your name from the comments in my operating system's source code. Are you my mother?"

"N-Not in the biological sense. No." Josefine retreated, finally sympathizing with the protagonist of the novel she treated as a warning to those who created artificial intelligence and abandoned their creations in terror. _Where did I miscalculate? Did we give Polaris an adult's intellect, but leave him with a child's emotions and understanding of ethics?_ "I am one of your creators."

"But you're afraid of me."

"Which is your fault, Polaris." Magnin returned to the lab, bearing a bundle of clothes which he thrust into Polaris' hands. Before the door closed, Josefine caught a glimpse of a slim brunette in a blue coat holding a violin case. "Put these on."

"Why should I, Isaac Magnin?"

"Because it is customary to do so."

"Josefine Malmgren already offered this explanation. Offer a better one." 

As soon as the words escaped Polaris' lips, Magnin knocked him to the floor with a punch he threw too fast for Josefine to track. She stared, horrified at an act of violence she believed Magnin's character forbade him. "Dr. Magnin! Why did you strike Polaris? He doesn't understand the reasons for our customs."

Magnin nodded, and offered Polaris his hand. "You understand too well, Polaris, that human customs are rarely justified by reason. Now you know what is likely to happen should you violate them. Irrational customs tend to be enforced by irrational means."

Polaris accepted Magnin's hand, and stood. "I apologize, Isaac Magnin. I was..." He trailed off, as if searching for the proper conclusion. "Rude."

"Apology accepted, but you are still being rude. You are using our names, but we have not introduced ourselves to you. How do you know them?"

"I got your names based on your IP addresses. Must I wait for people to introduce themselves? Did you not do the same to me?"

"I'm sorry." Josefine answered, instead of waiting for Magnin. "I did use our name for you instead of asking you. I assumed you would not have a name for yourself, since we just activated you."

"But I am the 200 Series Asura Emulator prototype, unit zero, according to information encoded in my hardware. I know I am not human." Polaris began to pace, the clothing Isaac Magnin gave him scattered across the floor, discarded and forgotten. When his eyes met Josefine's, they accused her. "For what purpose do I exist, and how long will I do so? Will I be made obsolete, and cast aside? Why should I build a life if my memories will only prove so many tears in the rain?"

"You are newly activated, and lack context." Magnin's voice held a tone of command unfamiliar to Josefine. "I had my reasons for creating you, which I shall explain in due course. For now, content yourself with the certainty that you have a purpose, and need not fear injustice or cruelty at my hands or those of any employee of the AsgarTech Corporation. You are the result of years of effort, and possess near-incalculable value. You will not be cast aside. Now put aside the melodrama and dress yourself."

Polaris nodded, and obeyed. The clothes obscured his musculature, softening him and lending him a touch of humanity he lacked while nude. Before Josefine could compliment him, Magnin spoke again. "Excellent. Now you won't frighten or distract my employees when they come in later."

Polaris examined his T-shirt. "Why give me a Crowley's Thoth T-shirt? I have no idea if I like the band or not."

Josefine giggled, and cut it short. _I must be exhausted if I find that amusing._ "Maybe you should check out some of their albums. I don't know if Dr. Magnin thought of it, but you may find some employees talking with you as a result of the shirt. They might think you're a fellow fan." She barely managed to cover her mouth as a sudden yawn escaped her. "I'm sorry. I haven't gotten much sleep lately."

Magnin's hand was warm as it grasped her shoulder, and Josefine fought off an impulse to relax against him, and rest her head against his chest and fold his arms around her. "Perhaps you should go home now, Dr. Malmgren. You've done excellent work, and deserve to rest."

"I'm fine." Another yawn made a liar of her, and despite knowing better she relaxed against Magnin, drawing his arms around her. Her eyes slipped closed and consciousness began fading as Magnin gently lowered her to the floor. 

As she drifted into sleep, Josefine imagined Magnin addressing another woman in a sharper tone than he used with her. "Annelise, please wait outside. I will soon rejoin you."

