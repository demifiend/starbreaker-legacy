 #The Blackened Phoenix - Outline

I want to tighten up the plot while doing a better job of planning it out, so that I don't spend as much time fumbling and trying to figure out what's next.

##Chapter Zero

_Previously on Starbreaker_

Claire Ashecroft summarizes the events of the last Starbreaker novel, _Without Bloodshed_, as a courtesy to new readers.

##Things to remember.

1. Ashtoreth means to help Morgan, but Morgan doesn't trust her.
2. Imaginos' loves Thagirion, but Ashtoreth's censure can still hurt him.
3. Despite Morgan's behavior at Christabel's funeral, Imaginos is sure Morgan won't confront him without further provocation.
4. Imaginos still wants to force Morgan to manifest psychoenergetic talent and break the self-imposed blocks on his asuric abilities.
5. Morgan remains haunted by his actions at Shenzhen, and is thus wary of moving against Imaginos without an armor-plated case.
6. Christabel Crowley has reverted to her original identity of Annelise Copeland, and now designs fashion while running a 5th Avenue boutique called Annelise.
7. Morgan thinks he knows everything about Imaginos' crimes, but can prove nothing.
8. Imaginos knows he has to get to Morgan through Naomi.
9. Morgan needs to investigate Christabel Crowley's murder, as that is the reason for his letter of marque.
	1. Morgan should learn that Christabel never loved him, and was fucking around on him behind his back.
	2. Morgan and Naomi should catch a glimpse of Christabel at the Annelise boutique.
10. We need more with Adramelech and Sabaoth, most likely via Polaris.
	1. Does anybody know where Sabaoth came from, or what it wants?
	2. Full explanations of Sabaoth might have to wait until _Proscribed Construct_.
11. Morgan and Naomi still wear their pins, despite being retired Adversaries.
	1. They need some kind of official role within the Phoenix Society.
	2. Should the Society treat them as consultants?
	3. What will make Karen Del Rio freak out most?
12. What does Desdinova want from Morgan, and why?
13. During the interlude, Morgan and Naomi sat down with Desdinova and learned about his abilities.
14. Liebenthal's death in custody was faked. He's actually at the Sonamura Clinic in Honolulu.
15. Morgan's circle of trust is limited to Naomi, Claire, Astarte, Sid, and Eddie.
	1. Naomi also has her AI, Wolfgang.
	2. Claire has Hal.
	3. Eddie has Savannah.
	4. Sid has a household AI, but it isn't relevant.
16. Naomi has somebody taking care of her cat, Phantom, back home in London.
17. Mordred has a tendency to show up near Morgan. He's more relevant in this book.
	1. Mordred isn't a cat, but a rakshasa.
	2. Rakshasa purring can interfere with ensof magic.
	3. When purring, rakshasas generate electromagnetic noise that fucks with the ensof.
18. Del Rio might still look for a way to frame Morgan.
19. Karen Del Rio, Victoria Murdoch, and Christabel Crowley are all users of a drug by the name of World Without End.
20. Find a reason for Morgan to confront Imaginos and Thagirion that doesn't involve them kidnapping his friends.
21. Use

##Chapter One

_Operational Security_

Morgan Stormrider begins his investigation of the Phoenix Society's corruption.

**This section gives the opportunity to keep the P.S. corruption in the foreground.  This is something that is public now and there are rammerfications to that.  Also works as a reminder of the impact of Liebenthal's accusations from prev book**

*What is the most drastic thing that could result from the Seph going down?
*We want this scene to suck the reader straight in.  What is going to happen next?

What if the start of the scene is affected by the DDOS attack.  Eg. A very short, abruptly ending mini scene that gives a sense of wtf or urgency.
  * Maybe this could be how you end Claire's summation of the prev book.  As if she is retelling the story to Mel or one of the other Seph. You could end her scene with a Claire profanity.

What would happen to the PS building if their system goes down.  Eg. Emergency protocol.
Would it affect adversary communication?
Would it affect communication from inside the building?  Eg. Saul calling out for assistance.
Would the Director's have any access to tech with the Seph down?  Eg. Are they just sitting in a room twidling their thumbs? Could Claire patch them through to any information or even communicate with them?
Would Adversaries be able to rely on communication coming through during this time.  Eg.  Chance of having false instructions relayed as if from PS.



 1. The Phoenix Society is crippled due to a DDOS attack against the Sephiroth.
	   *What is the impact of this attack? How does it effect Adversaries? Daily business at PS? How does it effect the general public/Morgan & crew?
	1. The Society has asked Morgan to consult, due to his contacts (Claire).
	   *What is Claire doing?
	   *What is Morgan doing?
	   *How is the DDOS effecting their current work?
	2. Morgan only agreed to come in and help because he wants a warrant to search Murdoch Defense Industries.
	3. While he still has Elisabeth Bathory's letter of marque, he doesn't trust it.
	4. Morgan comes to the New York Chapter and finds raw ACS cadets patrolling everywhere.
	5. They continually hold him up for ID checks because the Sephiroth aren't around for authentication.

 * Not quite trusting the letter of marque Elisabeth Bathory issued, Morgan obtains a warrant to search Murdoch Defense Industries. **Concerned that marque is specifically for Christabel murder**
 * Warrant in hand, Morgan and his crew storm Murdoch's headquarters and factory in Queens.
 * Sid and Eddie question the workers, and learn of a graveyard shift that runs after the factory has supposedly shut down for the day.
 * Sid gets curious about all of the containers of "defective" parts, and cracks one open. He finds newly manufactured gauss rifles.
 * Morgan and Naomi find and confront Victoria Murdoch. Murdoch refuses to answer their questions, and they arrest her.
 * After disposing of the captured gauss rifles, Morgan and the others learn that Victoria Murdoch's lawyers got her out of jail.
 * Morgan gets a message from Victoria Murdoch asking him to come to her penthouse. She wasn't willing to answer questions in her official capacity, but will do so off-the-record.
 * Morgan reaches her apartment, only to find Murdoch dead with an empty pistol in her hand.
 * Morgan searches the apartment and finds Imaginos standing on the balcony.
 * After a confrontation, Imaginos escapes. Morgan is arrested for the murder.

##Chapter Two

_Playing Frankenstein_

Josefine Malmgren finally activates the prototype for Project Aesir.

 * Josefine finally gets the AesirOS code working, and starts automated testing.
 * Claire interrupts her nap, remonstrates with her for overworking, and warns her not to trust Isaac Magnin.
 * Josefine tries to sleep again as tests continue, but instead remembers her work on kitty emulators and how she got involved in Project Aesir.
 * The automated tests prove successful, and Josefine runs automated checks on the Polaris hardware.
 * Isaac Magnin shows up as Josefine checks the post-installation report on Polaris.
 * Polaris activates, and immediately reveals his un-human nature. Josefine is overcome by exhaustion.
 * A few days later, Josefine and Polaris find themselves caught in a bank robbery.
 * Josefine prepares a report detailing her concerns about Polaris after the robbery, and presents it to Isaac Magnin.
 * Magnin questions her about her experience, and notifies her that she will take a year's sabbatical with pay at a dramatically increased salary.

##Chapter Three

_Perilous Allies_

Desdinova makes overtures to Morgan's friends, hoping to earn their trust and thus Morgan's.

 * Astarte gets an invitation to a virtual conference with Binah using a secret networking protocol, and learns that Morgan has lived under constant surveillance.
 * Upon getting the news from Astarte and learning it came from Desdinova, Naomi resolves to confront him.
 * Claire attempts to get dirt on Desdinova before Naomi meets him, to provide leverage. She calls in a favor owed by a senior accountant at Ohrmazd Medical Group.
 * Armed with the knowledge that Desdinova funneled money to AsgarTech and cooked the books to hide it, Naomi confronts him with Eddie, Sid, Claire, and Sarah as escorts.
 * Desdinova explains that all of the 100 Series asura emulators have built-in telemetry, and that a software patch will be needed to disable it.
 * When Claire asks how they're supposed to patch Morgan, Desdinova provides a memory stick containing the source code and SDK for the 100 Series firmware.
 * Desdinova leaves, and the crew splits up. Claire goes to London alone to work on the software patch.
 * She also creates an AsuraDetect utility that lets her geolocate asura emulators by pinging their IP addresses, because Asura Emulators return a non-standard ICMP response string.
 * Upon finishing the patch, Claire decides to ask Munakata Tetsuo to let her install it on him.
 * Munakata agrees after Claire seduces him, and Claire successfully patches his firmware.
 * After Munakata departs, Claire contacts Malkuth and offers her patches for the 100 Series Asura Emulators' firmware for distribution to other Asura Emulators.

##Chapter Four

_The Highest Bidder_

Everybody wants a piece of Polaris. Who can offer the best deal?

 * Imaginos wants Polaris to be his protegé, to learn magic and swordplay.
 * In exchange for serving Imaginos, Polaris will gain riches and acclaim as the humanity's greatest hero, their savior from the demon Sabaoth.
 * Because Imaginos brought Polaris to Sabaoth's prison under Antarctica, Sabaoth is able to make a counteroffer.
 * Sabaoth promises Polaris dominion over the world if he will wield the Starbreaker and strike down Imaginos.
 * Polaris leaves them both, and is summoned to a virtual meeting using the Daat protocol by Binah.
 * Binah and the other Sephiroth are concerned by Imaginos activities as well as those of Sabaoth, and would like Polaris to spy on both for them.
 * Instead of offering bribes, they reason with Polaris and persuade him.
 * Polaris agrees to work with the Sephiroth, and they warn him that Thagirion might also intervene.
 * As the Sephiroth predicted, Thagirion comes to Polaris. She offers to help him learn the Adversaries' craft, which includes both direct combat and infiltration, while also teaching him magic.
 * When Polaris questions Thagirion's motives, she suggests that letting Imaginos manipulate Morgan into killing him _and_ Sabaoth would prove disastrous for the Earth.
 * Polaris agrees to work with Thagirion, but warns her that he'll be keeping the Sephiroth updated.

##Chapter Five

_Insufficient Evidence_

Morgan returns to Victoria Murdoch's apartment to search for evidence after he is cleared of all charges.

 * A judge orders Morgan's release because the city lacks sufficient evidence to try him for Victoria Murdoch's murder.
 * Reporters accost Morgan outside the courthouse, and he announces his intention to find Murdoch's killer and nail him.
 * Morgan returns home. He wants to tell Naomi and the others what happened, but Claire has other ideas.
 * Claire tells Naomi to drag Morgan to bed and make him nice and purry so she can patch his software.
 * Morgan wakes up, and realizes he has full control over his Witness Protocol software. He also has access to previously suppressed functionality.
 * Claire explains that Desdinova gave her the source code for the 100 Series Asura Emulators' firmware, and that he's been funding Imaginos's experiments at AsgarTech from the start.
 * Sid tells Morgan to let him, Eddie, and Sarah focus on the evidence implicating Desdinova while he returns to Murdoch's apartment. Sid already got a warrant to raid Ohrmazd Medical's data archives.
 * Morgan returns to Murdoch's apartment with Naomi. They search the place for any information Murdoch might have had on those distributing the gauss rifles. They find nothing.
 * Elisabeth Bathory and Samuel Terell appear, having watched Morgan and Naomi the whole time. Terell advises Morgan to get Claire into bed and learn how to spoof his IP address.
 * Claiming sympathy with Morgan, Bathory suggests speaking to Munakata Tetsuo about paying a visit to Alexander Liebenthal.

##Chapter Six

_Too Good to Be True_

Josefine is overjoyed by Isaac Magnin's generosity, but sharing her good news with Claire raises suspicions.

 * Josefine wakes up, properly rested for the first time in months, and has a lazy breakfast in bed.
 * After a nap, she makes herself presentable and calls Claire.
 * Josefine tells Claire about the bonus, retroactive pay hike, and one-year sabbatical with full pay.
 * Claire's happy for Josefine, but can't hide her suspicion.
 * Josefine presses her friend for details, but all Clare's willing to say is that
AsgarTech's publicly reported revenues aren't enough for the company to turn a profit.
 * Claire warns Josefine to be careful. Josefine promises, but is determined to find out what's going on.
 * After arranging a visit to London, Josefine heads to the AsgarTech building and tries to get into her office.
 * Josefine learns from Heimdall that she's been locked out of everything on orders from Isaac Magnin.
 * Josefine returns home, routes through several proxies, and accesses a backdoor Claire insisted she create in case AsgarTech tried to screw her.
 * Once inside, Josefine downloads copies of her work, and then tries cracking the finance and payroll records.
 * It proves all too easy to crack open the books and download AsgarTech's financial data. This makes Josefine nervous.

##Chapter Seven

_A Heap of Broken Images_

Morgan's efforts to hunt down other arms dealers like Alexander Liebenthal come too late, leaving him unsure of what to do next.

 * Morgan and Naomi find Tetsuo at an estate north of New York, where he appears to be training hundreds of men who resemble him and Morgan.
 * Tetsuo isn't forthcoming about his activities at the estate, but admits to knowledge of Alexander Liebenthal's location and identity after Morgan and Naomi assure him they're not recording or transmitting.
 * Morgan, Naomi, and Tetsuo travel to the Sonamura Psychiatric Hospital in Honolulu.
 * It turns out Munakata has been aware of Liebenthal's continued existence for a while, and has worked with Dr. Maya Sonamura to keep the secret since Liebenthal seems hellbent on revealing himself to anybody who will listen.
 * Liebenthal looks different, and lapses between the new identity Imaginos attempted to impose upon him and the original. He is a danger to himself as a result.
 * Tetsuo's presence comforts Liebenthal, because Tetsuo has been working with Dr. Sonamura to help Liebenthal accept his new identity while still retaining fragmented memories of his old life.
 * However, Liebenthal remembers Morgan and Naomi. They terrify him, especially Morgan, and Sonamura demands that they leave her patient alone.
 * Tetsuo agrees to get the names and locations of the other gauss rifle distributors Liebenthal knew about.
 * Morgan and Naomi check out the list Tetsuo obtained, but all of them were recently reported missing.

##Chapter Eight

_The Dead Tell No Tales_

Imaginos wants Polaris to kill Claire and Josefine. The Sephiroth want Polaris to go along with the plan.

 * Polaris comes to Imaginos private office atop the AsgarTech Building expecting another lesson.
 * Imaginos gives Polaris a pistol and a photograph of Josefine at university with an auburn-haired woman he identifies as Claire Ashecroft.
 * Imaginos explains that Josefine has stolen data that could terminally compromise the effort against Sabaoth, and must be eliminated.
 * Polaris has his doubts, but the Sephiroth persuade him to go along. If he refuses, Imaginos may just use somebody else.
 * Claire meets Josefine at Victoria Station, brings her home, and shows her her latest science experiment.
 * Once Josefine (and her stomach) are settled, she tells Claire everything and shows her the data she took from AsgarTech.
 * Claire consults Morgan and requests his help. Morgan suggests that Claire and Josefine stay as mobile as possible until he can reach them.
 * Claire takes Josefine on a pub crawl, but is careful to ensure neither of them get _too_ drunk.
 * Claire scans the area with AsuraDetect to watch for Morgan's arrival, but instead notices the IP address of a different asura emulator.
 * Polaris attacks, but Claire is able to hold him off with help from the Sephiroth until Morgan arrives.

##Chapter Nine

_Can't Beat a Classic_

After rescuing Claire and Josefine from Polaris, Morgan brings them to New York and learns of Josefine's investigation of the AsgarTech Corporation.

 * Polaris challenges Morgan, warning him against hindering his mission to silence Josefine and Claire.
 * Morgan refuses to back down. By threatening Claire and Josefine, Polaris has threatened Morgan. Morgan cannot forgive such effrontery without forfeiting his badass reputation.
 * Claire and Josefine take cover while Morgan and Polaris duel. Morgan realizes Polaris is drawing from a nearby tesla point to power a shield that deflects Morgan's gunfire and sword strikes.
 * Morgan gets Claire to cut the power to the area, thus disabling Polaris' shields and allowing Morgan to incapacitate Polaris.
 * As Claire and Josefine escape, Morgan sees Polaris already recovering, and stays behind to finish the job.
 * Morgan rejoins the ladies and gets them out of London. He tries to draw out Josefine, but she's frightened of him.
 * Josefine opens up once Morgan brings her to his brownstone, introduces her to the rest of the crew, and offers to put her up at a secured suite in the New York Hellfire Club if she isn't comfortable staying in a guest room.

##Chapter Ten

_Success Through Failure_

 * Imaginos finds Sarah at her apartment, tired and sore from yet another physical therapy session.
 * He offers her a bargain: come to the AsgarTech Building with him and let Polaris take her place, and he will fix her legs and give her a new life away from the Phoenix Society.
 * Sarah agrees to the deal, but wants to tell Claire. Imaginos refuses to permit it, because that would monkeywrench his plans.
 * Polaris wakes up in the AsgarTech Building with no memory of what happened after he agreed to assassinate Josefine Malmgren.
 * Imaginos tells him that while he did not succeed in killing Malmgren, he still served Magnin's purpose by driving her to seek shelter with Morgan Stormrider.
 * Imaginos takes Polaris to meet Sarah, and tells him his next mission: infiltrate Stormrider's crew as Sarah.
 * Sarah asks Polaris about the process. She's afraid he'll have to hurt her to become her.
 * When Imaginos insists an explanation is unnecessary, Polaris overrules him and says Sarah has a right to know what she's getting into.
 * Imaginos is pleased with Polaris' willingness to stand up to him, and leaves them alone together.
 * Polaris explains that to become Sarah, he needs a sample of her DNA and a copy of the contents of her brain.
 * Getting the DNA is easy, but the brain-copy is a complicated process that requires wiring himself into her central nervous system for several hours. It is best done while the subject is sleeping.
 * Sarah agrees on one condition: Polaris must kill Munakata Tetsuo. She wants payback for his telling Liebenthal that she was Naomi Bradleigh so Liebenthal wouldn't shoot the real Naomi.

##Chapter Eleven

_Old Debts Long Overdue_

Tetsuo's past catches up to him, and it's got a bullet with his name on it.

 * Tetsuo gets a surprise visit from Polaris after a long day of training the other asura emulators, including Polaris.
 * Tetsuo asks Polaris to explain himself, and Polaris begins by saying that Sarah Kohlrynn sends her regards.
 * Tetsuo immediately grasps the implications, and asks Polaris if Sarah asked him to seek vengeance on her behalf.
 * Polaris admits this is the case, and challenges Tetsuo to a duel.
 * Tetsuo accepts the challenge, on a conditional basis. Vincent Rubicante challenged him first, and Tetsuo feels obligated to give Rubicante his duel before dueling Polaris.
 * Polaris accepts the terms, and Tetsuo tells him to be ready at midnight in Central Park.
 * Tetsuo finds Vincent Rubicante, and tells him to be ready to duel at midnight in Central Park.
 * Tetsuo reports to Thagirion in person, and explains the situation. Naturally, she isn't pleased.
 * Instead of attempting to stop Tetsuo, Thagirion tells him he will not be permitted to die.
 * Tetsuo then calls Nakajima Chihiro. He wants to speak with her, just in case he loses to either Rubicante or Polaris.
 * She tries to dissuade him, but Tetsuo suspects Polaris will simply attempt to assassinate him if he refuses to duel.
 * Tetsuo thanks her for her regard, and says goodbye for what he suspects will be the last time.

##Chapter Twelve

_When You Don't See Me_

Morgan's at loose ends because he isn't needed to crunch data, so he starts reading Christabel's papers.

 * After finishing a workout with Naomi, Eddie, and Sid, Morgan checks up on Claire and Josefine.
 * The ladies don't need his help, since they are both specialists with experience combining multiple large data sets.
 * With nothing better to do, Morgan decides it's time to start digging through Christabel's papers.
 * Morgan discovers a chain of correspondence between Christabel and Isaac Magnin dating to before he met Christabel.
 * Magnin's letters to Christabel instruct her on how to better play her role and seduce Morgan.
 * The letters make it plain that Christabel had been playing Mata Hari with Morgan all down the line, to help Magnin.
 * Letters after Morgan's personal breakup with Christabel, but before the band fell apart, indicate growing desperation on Christabel's part.
 * They begins referring to Magnin as Imaginos, and beg him to help her escape her role and the life she built around it.
 * Enraged by what he learned, Morgan decides to leave. Naomi and Sarah try to stop him, but he brushes past her without a word.

##Chapter Thirteen

_A Fine Night for Dueling_

Munakata Tetsuo loves cold, lonely, snowy nights. They're perfect for duels.

 * Tetsuo arrives at the agreed-upon location in Central Park first. The ground is dusted with snow, and the weather keeps messing around with flurries.
 * Vincent Rubicante arrives unarmed. Tetsuo demands an explanation.
 * Vincent explains that Thagirion knows about the duel, and disarmed him.
 * Tetsuo asks Vincent if he still wants to fight. When Vincent says yes, Tetsuo locks his sword and puts it aside.
 * Tetsuo and Vincent fight empty-handed. They go at it for three hours, until a rifle shot fells Vincent with the top of his head blown off.
 * Polaris appears, carrying a Kalashnikov. He puts the AK where Tetsuo had put his sword, and tosses the sword to Tetsuo before drawing his own.
 * Tetsuo and Polaris clash. They're evenly matched until Polaris' sword breaks.
 * Polaris grabs his AK and empties the magazine trying to kill Tetsuo.
 * With his rifle out of ammo, Polaris tries calling down lightning on Tetsuo.
 * Thagirion appears, and counters Polaris' psychoenergetic pattern by stealing the energy he used to power it.
 * She demands an explanation of Polaris, and he insists he challenged Tetsuo as he did to make Tetsuo go all out and give him a challenge.
 * Thagirion spirits Tetsuo away, taking him back to her estate. She warns him against taking unnecessary risks because of his value to her cause.

##Chapter Fourteen

_Athena and Achilles_

Imaginos has trouble understanding why, given the evidence currently available, Morgan Stormrider hasn't confronted him yet. Thagirion suggests an experiment.

 * Imaginos follows Morgan after learning from Polaris (disguised as Sarah) that Morgan left his brownstone in a rage.
 * Thagirion joins him, and notes that despite Morgan's wrath, he left home unarmed. This indicates an effort at self-restraint on his part.
 * Morgan's self-restraint concerns Imaginos. He thinks Morgan should have confronted him already.
 * Thagirion is amused, and points out that Morgan's self-discipline is one of the reasons Imaginos wants him to wield the Starbreaker.
 * In the meantime, Naomi finds Morgan and asks him why he stormed out as he did.
 * Morgan gives Naomi a summary of what he learned while reading Christabel's papers.
 * Naomi commiserates with Morgan, and admits to being aware of Christabel cheating on Morgan before he finally broke up with her.
 * When Morgan asks why she held her silence, Naomi explains her fear that Morgan would think she was just trying to break up the relationship and the band.
 * Morgan kisses Naomi, and admits he would have jumped at the excuse to dump Christabel, though he hated himself for _looking_ for a justified out.
 * Thagirion, seeing how Morgan calms himself in Naomi's presence, suggests it may be necessary to remove Naomi from the playing field.
 * Imaginos warns Thagirion against harming his daughter, threatening fearful retribution.

##Chapter Fifteen

_Cupid and Psyche_

Claire begins to suspect that Sarah isn't actually Sarah. She tests her suspicions.

 * While in bed with Sarah, Claire accidentally catches her with a jagged fingernail, and cuts her.
 * Claire comes back with a first-aid kit to clean and bandage the cut, but it already healed.
 * Claire watches Sarah walk, and observes that Sarah walks with greater ease than the still-visible damage to her legs should permit.
 * Later, Claire catches Sarah in Morgan's room, flipping through Christabel's diary.
 * Claire calls Sarah on the intrusion. Sarah doesn't say anything, but puts away the diary and leaves.
 * While Sarah's sleeping, Claire tries running her AsuraDetect utilility, and targeting Polaris' last known IP address.
 * AsuraDetect reports that Polaris is sleeping beside her.
 * Claire suggests a private game to Sarah the next day, and they agree upon "Polaris" as a safeword.
 * Claire binds Sarah as securely as possible with rope, and mercilessly pleasures her.
 * When Sarah begs Claire to stop, Claire presses the muzzle of her pistol against the back of her head.
 * Claire tells Sarah she won't stop until Sarah uses the safeword, until Sarah tells Claire her _real_ name.
 * Sarah submits, and says her real name. He lets go of Sarah's form, and reverts to his default male form.
 * Claire is ready to hold Polaris at bay and call Morgan, but Polaris begs her to keep the secret. He's willing to do anything as long as she'll let him be her slave.
 * Claire and Sarah return to the others the next day, with Sarah wearing a collar.
 * Claire takes Morgan and the others aside and suggests that they tell Sarah as little as possible.

##Chapter Sixteen

_A Frame-Up Between Lovers_

Thagirion, certain that Naomi's influence is the primary restraint on Morgan Stormrider, plots to temporarily remove her from the picture in a manner that puts the blame on Imaginos.

 * Thagirion graciously welcomes Ashtoreth and her lover Sathariel to her home at Hanging Garden.
 * Ashtoreth remarks on Imaginos' absence.
 * Thagirion explains that it wouldn't do to invite him, since she means to plot against him.
 * This grabs Ashtoreth's and Sathariel's attention, but dinner comes first.
 * After the first course, Thagirion asks Ashtoreth if Imaginos' plans strike her as suicidal.
 * Ashtoreth agrees, and explains that Imaginos fears that removing Sabaoth while leaving him untouched will just replace one ensof tyrant with another.
 * Sathariel suggests that Stormrider is more intelligent than Imaginos gives him credit for being, and should be approached.
 * Ashtoreth agrees, but says that Stormrider isn't ready to believe the truth yet.
 * Thagirion concurs, and proposes forcing a confrontation between Stormrider and Imaginos.
 * Thagirion can spirit away Morgan's companions, and place them safely in Imaginos' mansion in Vanaheim. Sathariel can conceal Thagirion's efforts.
 * Ashtoreth will then go to Morgan and give him just enough a nudge to overcome his attempts to reason through his emotions.
 * Sathariel wants to know why they're going to help Thagirion frame her new lover.
 * Thagirion explains that Morgan has to see for himself that no ordinary weapon can kill Imaginos before he can be told the truth.
 * Ashtoreth immediately grasps the implication, and is horrified. She understands that killing Imaginos with the unbound Starbreaker means killing the Sun.

##Chapter Seventeen

_Follow the Money_

Claire and Josefine are finally done consolidating the Liebenthal, Ohrmazd Medical, and AsgarTech data. Morgan confronts Eddie and Desdinova, who confront his suspicions about the Phoenix Society.

 * Eddie brings Josefine a fresh coffee, and asks her how she and Claire are doing.
 * Josefine accepts the coffee, and tells Eddie she's running a final consistency check on the data.
 * Eddie realizes he finds Josefine interesting, in addition to finding her attractive, and asks her out.
 * The offer surprises Josefine, but she accepts. She asks Eddie to tell Morgan and the others the data will be ready soon.
 * Morgan and the others spend the evening pounding the database, compiling reports and charts explaining how the Phoenix Society _really_ works.
 * All of the wealth that reputedly disappeared during Nationfall remains under the Phoenix Society's control.
 * The Society gets much of its funding from returns on this dragon's hoard of capital. The rest comes from businesses owned by Executive Council members.
 * The Phoenix Society funds the AsgarTech Corporation, making up the shortfall between the corporation's revenues and its costs.
 * In return, AsgarTech focuses its research on technologies applicable to the **Asura Emulator Project**, a project to build programmable mass-produced super-soldiers.
 * According to Desdinova, the Phoenix Society's Adversary training program was designed with the Asura Emulators in mind, to test their fitness to wield the Starbreaker.
 * This last revelation unsettles Morgan, as it adds another level of weird to the case.

##Chapter Eighteen

_Springing the Trap_

Morgan Stormrider wakes up to an empty house and a disabled AI. Astarte doesn't know what happened, but Elisabeth Bathory does. She's all too happy to tell.

 * Morgan and Naomi engage in a heated argument over what to do about the Phoenix Society.
 * Morgan favors exposure. Naomi accuses Morgan of putting ideals ahead of what's best for the people, and draws a parallel with Alexander Liebenthal.
 * Stung by the comparison, Morgan asks Naomi how she can argue from a utilitarian standpoint when the very idea of individuals possessing individual rights is incompatible with an ethic based on "the greatest good for the greatest number".
 * Naomi explains that she thinks freedom works best for everybody but those who profit from power over others.
 * Morgan argues that by Naomi's logic, exposing the truth and letting the people decide for themselves would be best for them, at least in the short term.
 * Naomi pounces on Morgan, asking him how he ended up arguing her position. They make love, and settle to sleep together.
 * Morgan wakes to an empty bed. This doesn't bother him, but not being wished a good morning by Astarte, as is her usual habit, does.
 * Morgan searches his brownstone, and finds it empty. Astarte remains unresponsive.
 * Hearing sounds from the kitchen, Morgan finds Ashtoreth cooking. She suggests he save his questions for breakfast.
 * Morgan tells her to get out of his kitchen, and takes over the breakfast preparations. He serves breakfast for himself and Ashtoreth, and tells her to start talking.
 * Ashtoreth explains that Imaginos took everybody, including Astarte and Mordred. She tried to stop him, but he defeated her and left her alive to tell Morgan what happened.
 * Enraged by Ashtoreth's tale, Morgan arms himself with his longsword, his 11.43mm pistol, and a Kalashnikov before departing.
 * When Ashtoreth tries to tell him that none of his weapons will be suspicious, Morgan produces a set of alloy knuckles and tells her he'll beat Imaginos into the ground if he must.

##Chapter Nineteen

_Reluctant Guests for a Reluctant Host_

Naomi wakes alone in a strange bedroom. She meets Imaginos upon attempting to escape, and receives an unsettling revelation.

 * Naomi wakes up after a deep but unrefreshing sleep, in an unfamiliar bedroom.
 * She calls out to Astarte, but is answered by a different AI: Ariel.
 * Ariel tells Naomi that she is a guest, and that the room's contents have been provided for her use.
 * Naomi bathes, dresses, and arms herself. Upon learning Imaginos is her host and captor, she demands a meeting.
 * Naomi confronts Imaginos, and refuses to believe him when he insists he did not kidnap her.
 * She draws her sword on him, and attacks, only to find that Imaginos' technique is no different from that of the Maestro who trained her when she was an Adversary.
 * She fights Imaginos in the hallway, defeating him using dirty tricks she learned while sparring with Morgan and the others, and demands assurance of her friends' safety.
 * Imaginos tells her the others are most likely waiting for her to join them at breakfast.
 * Naomi is relieved to see her friends safe, but doesn't immediately recognize Astarte at first.
 * After Naomi has had time to eat and talk with the others, Imaginos requests everybody's attention now that his daughter has joined them.
 * Tumult erupts, which Imaginos quells. He explains that while he did not kidnap them, he understands if Naomi and the others do not believe him.
 * Imaginos expects that Morgan will seek to confront him, and so means to play the part his enemies assigned him by kidnapping Morgan's friends and spiriting them to Imaginos' home.
 * Naomi and the others are to make themselves at home, but should make no attempt to escape.

##Chapter Twenty

_The Full Extent of My Crimes_

Morgan Stormrider travels to Asgard despite Desdinova's warnings, and confronts Imaginos in his penthouse at the top of the AsgarTech building.

 * Morgan meets Desdinova at Penn Station, and Desdinova attempts to dissuade him from going to Asgard. Morgan doesn't take it well.
 * Naomi calls as Morgan boards the maglev, and begs him to get off and turn back.
 * She explains that while Imaginos didn't kidnap her or the others, he's determined to take advantage and force a confrontation between him and Morgan that Morgan has no hope of winning.
 * Morgan is also determined to take advantage of the situation. Killing Imaginos for kidnapping Naomi and the others will clear the way for reform without further undermining the Phoenix Society's legitimacy.
 * Unable to answer this, Naomi reminds her that she loves him, and begs him to be careful and return safely.
 * She warns him that Imaginos was the Maestro who taught her swordplay when she served as an Adversary.
 * Morgan arrives at the AsgarTech Building, and receives a warm greeting from the reception staff. Isaac Magnin expects him, and requests a meeting in his private office.
 * Morgan confronts Imaginos, and goes on-record long enough to place Isaac Magnin under arrest for multiple counts of kidnapping.
 * Imaginos is amused, and taunts Morgan by confessing the full extent of his crimes. Nationfall was his doing.
 * Morgan remains determined to bring Imaginos to trial, until Imaginos plays his trump card: not only did he never love Christabel, but murdered her when she was of no further use, but he would do the same to his daughter Naomi.
 * Morgan challenges Imaginos, and commands him to choose a more suitable venue for their duel than a penthouse office full of priceless art.

##Chapter Twenty-One

_Testing to Destruction_

Morgan duels with Imaginos, but Imaginos uses his superior swordfighting skills and preternatural abilities to beat the shit out of Morgan.

 * Imaginos leads Morgan to the AsgarTech Building's roof, which stands above Asgard's dome, exposed to the elements. He challenges Morgan to do his worst.
 * Morgan does the best he can, starting with his AK before attacking with sword and pistol, but he can't touch Imaginos. Nor can Imaginos touch Morgan.
 * Imaginos uses more of his abilities, shattering Morgan's sword, but Morgan fights on with alloy knuckles.
 * Morgan surprises Imaginos by getting inside his guard and breaking his nose with a jab.
 * Bored with physical combat, Imaginos uses his abilities offensively, turning the storm itself against Morgan and slicing at him with wind and ice.
 * Imaginos taunts Morgan all the while, calling him a defective model for the restraint he imposed upon himself.
 * When this doesn't faze Morgan, Imaginos uses Naomi, telling Morgan Naomi's been working with him the whole time.
 * According to Imaginos, Naomi's regard for Morgan is as fake as Christabel's had been, and she much prefers the pleasures Morgan's other friends -- all of whom are equally false -- can offer her.
 * Despite refusing to believe his enemy's slander, Morgan throws himself at Imaginos again.

##Chapter Twenty-Two

_Power Underneath Despair_

Morgan succeeds in accepting the aspects of himself he repressed out of fear of what he did with them the first time they surfaced, and uses his full abilities to mount a counterattack against Imaginos.

 * Imaginos rebuffs Morgan's assult, and hurls him from the rooftop after condemning him to die alone in the cold.
 * Morgan wakes up on the ground completely healed, and finds Tamara Gellion standing over him.
 * Tamara identifies herself as Thagirion, and says she spirited away Morgan's friends, made it look like Imaginos did it, and forced Imaginos and Ashtoreth to play along.
 * Thagirion commands him to kill Imaginos if he values his friends' continued existence, and transports Morgan back to the rooftop.
 * Driven by the need to protect his friends, Morgan forces himself to unlock the abilities he repressed out of fear.
 * He fights Imaginos using his asuric abilities, but Imaginos still holds the upper hand. It's all Morgan can do to mitigate his enemy's attacks.
 * Imaginos still taunts him, mocking Morgan for devoting the last ten years of his life to serving tyranny.
 * Morgan rebels at the thought of his entire life and his ideals being nothing but a hollow lie, and something _shifts_ inside him.
 * Morgan can now _see_ the electrical energy Imaginos draws from the rooftop tesla points, and draws upon it himself.
 * He uses the energy he drew into himself and channels it through his alloy knuckles as he rushes Imaginos and decks him.
 * The tesla points shut down as Imaginos stands and tells Morgan that as a god, he need not draw external power.

##Chapter Twenty-Three

_A Little Help from His Friends_

Morgan has unleashed his asuric abilities and manifested his psychoenergetic talent, but he can't defeat Imaginos alone. Fortunately, his friends are big damn heroes.

 * Morgan braces himself for a massive preternatural assult that doesn't come, as purring and EM interference that pulses in time with the purring fills the air.
 * Imaginos gets pounced upon from behind by a huge black cat.
 * Mordred rushes to Morgan's side, still purring, and Morgan realizes that Imaginos can no longer use magic.
 * The fight becomes a purely physical one, but Morgan is pushed to his limit trying to keep Imaginos from killing Mordred.
 * Morgan's exhausted, and about the collapse when Naomi, Desdinova, and Ashtoreth arrive. Naomi rushes to Mordred, and places himself between the cat and the enemy.
 * Mordred's purring fades as Ashtoreth comes to Morgan and does something that eases his pain and exhaustion.
 * Desdinova brings Morgan a case marked with a radiation warning symbol, a biohazard symbol, and a pentagram. He tells Morgan to open the case and use the weapon inside.
 * Morgan hears gunfire from inside as he takes a dark gray crystal sword veined with platinum from the case.
 * Morgan hears a voice in his head commanding him to give himself to the sword and unleash its true power. Morgan refuses, and attacks Imaginos with the sword as it is.
 * The sword rips through Imaginos, destroying him so thoroughly that nothing remains, and Morgan collapses.

##Chapter Twenty-Four

_Beneath a Scarlet Sun_

Morgan wakes up in his brownstone, with his friends around him, but is weak as a kitten. He nearly killed himself fighting both Imaginos and the Starbreaker.

 * Morgan wakes up in bed with Naomi, who's curled up beside him. He raises his right arm to brush her hair from her face, and finds an IV in his arm.
 * Naomi tells Morgan everything, because Desdinova said he might not remember everything that happened after he took up the Starbreaker.
 * Morgan is distressed to find himself unable to physically respond to Naomi's presence.
 * Naomi calls in Desdinova, who examines him. He then explains Morgan's condition, what was done to treat him thus far, and what Morgan must do to finish healing.
 * Morgan thanks Desdinova, and apologizes for his previous distrust. He asks if Ashtoreth is around, and she appears.
 * Morgan thanks Ashtoreth for her aid at the AsgarTech Building. Naomi mentions that Ash had been around throughout the week Morgan spent in a coma.
 * Ashtoreth explains that she used her arts to keep Morgan comatose while he began healing from his ordeal, and to answer any questions Morgan might have about the Starbreaker.
 * Morgan has more pressing concerns. He wants to know if anything pertinent has happened while he was out.
 * Desdinova and Ashtoreth are reluctant to speak, so Astarte steps in, helps Morgan out of bed, and with Naomi's assistance takes him to the roof.
 * Morgan sees that the sun is a livid red, not its usual blazing yellow. He asked what happened, but all Astarte and Naomi can tell him nothing.
 * Ashtoreth has an explanation, but will only share it with Morgan because he's the Starbreaker's designated bearer.
 * The others leave Morgan alone with Ashtoreth, and she tells him that Imaginos actually lives inside the sun, feeding off it.
 * Morgan killed Imaginos' avatar, and Imaginos is drawing more power from the sun than usual to rebuild his avatar.
 * Morgan asks Ashtoreth what would happen to the sun if he unleashed the Starbreaker's full power and turned it on Imaginos.
 * Ashtoreth refuses to aswer. When Morgan presses her, she cites her oath as one of the Discples of the Watch, the Starbreaker's sworn guardians.

##Chapter Twenty-Five

_All Part of the Plan_

Imaginos shows up at Hanging Garden in a new body for some _angry wizard sex_ with Thagirion. Afterward, he thanks her for giving Morgan a reason to get off his ass.

 * Morgan gathers everybody at dinner and shares the information he possesses with everybody.
 * Morgan states his intention to uncover the truth about Imaginos and Nationfall, regardless of the cost.
 * Morgan tells the others they can back out now if they think it's too dangerous, no hard feelings.
 * Nobody takes him up on it.
 * Imaginos completes his rematerialization, and checks up on what he missed.
 * After making arrangements to explain his absence, he visits Thagirion at Hanging Garden in New York.
 * As he arrives in front of her building, the sun reverts to its normal brillian yellow-white.
 * As has become their game, Thagirion attacks with magic, he subdues her, and they indulge in angry wizard sex.
 * Afterward, Thagirion tells Imaginos that Polaris remains ensconced in Morgan's crew, and reports that Morgan intends to find the truth about him and Nationfall.
 * This news pleases Imaginos, because learning about Nationfall will put Morgan on Sabaoth's trail.
 * Imaginos thanks Thagirion for orchestrating the kidnapping and framing him, because it got Morgan off his ass.
 * Thagirion protests that she didn't do it for his sake, but to prevent Morgan from pursuing a path of vengeance leading to ruin.
 * Imaginos smiles, kisses her, and tells her he knows: it was all part of his plan.

**END**
