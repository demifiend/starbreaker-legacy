_If Morgan's boning Naomi in the broom closet, I'm going to kick his ass._ Sid doubted the two would actually duck into a closet for a quickie in the middle of a raid, but they had dropped out of secure relay chat without warning. He caught a staff member as she scuttled out of the ladies room. "Excuse me, ma'am. Have you seen a couple of Adversaries? One's got black hair, the other's snow-blonde."

"Did you try the server room?" The worker ducked past Sid and scurried back to whatever duties she had put aside.

"Hey!" Sid called after her. "Where's the server room?" 

He muttered a curse as she continued without answering, and turned to secure relay chat. [Claire, you busy?]

[Busy thinking of you, big guy. What can I do you for?]

Sid shook his head while wishing Claire did not flirt with absolutely _everybody_. Though she'd never do more without Sid's consent, such attention from women other than his wife made him uncomfortable. After his mother divorced his father, she took to telling him, "Sidney, a man's only as faithful as his options. Your father had options, and he took them."

Though she insisted it was infidelity on his part that killed the marriage, Sid was mature enough to understand she was happy to stick around as long as he made good money, especially since neither was faithful to the other. Without both arms and the ability to provide, an unfaithful husband was nothing but a liability, and Sid's mother was unwilling to hold on to liabilities without a damned good reason. It, and her facility with hypocrisy, was the secret of her success in business.

_Hell, my mother would probably get along with Victoria Murdoch._ Sid shoved the memories back into their closet at the back of his mind, and returned to business. [You got a floor plan for this shithole? I need to find the server room.]

[No problem.] Claire guided him, turn by turn, to an unmarked door. Sid tried it, found it unlocked, and stepped into a room full of obsolete computing machinery. His network connection cut out, and he realized why Morgan and Naomi were unreachable. "Why the fuck didn't Claire tell me you guys were in a Faraday cage?"

Morgan, absorbed in his efforts at one of the consoles, did not reply. Naomi turned to Sid while keeping her eye and sword on a pudgy, pasty little man wearing a t-shirt that read 'I &hearts; SODOMY'. "It's not a Faraday cage. We're not sure what it is yet, because my new friend here has proven most uncooperative."

The pudge in the t-shirt had a small cut across the back of his hand, little more than a scratch, and kept glancing toward a corner of the room into which somebody had kicked a taser. Sid picked it up and checked the charge. Satisfied that it was full, he advanced upon its former owner. "You must have tried using this on Adversary Bradleigh. If you had used this on me, I'd have shoved it up your ass since your t-shirt suggests you're into that sort of thing. Maybe I should do it anyway."

"Don't bother with him." Morgan looked up from the console, and stretched. "I found the problem. He had a jamming device rigged to activate if anybody tried to access this mainframe from outside the company."

Naomi glanced at Morgan. "Did you disable it?"

"Not yet. I've been busy archiving all the data stored on this old monster. I just finished the transfer." Morgan rose, found a stepladder, and reached over the mainframe. 

Sid approached the console, and saw an 'Emergency Data Wipe in Progress' message on the screen. "Hey, Morgan, did you know about this?"

"The wipe? That's why I transferred all the data before I unplugged this." Morgan climbed back down, and tossed Sid a small device. "Hold on to it."

Sid pocketed the device as secure relay chat began working again. A message from Eddie came through. [I'm outside with the prisoner transport. You make any arrests yet?]

Sid smiled at the pudge, and grasped his shoulder. [Yeah, we got one. Obstruction of justice. Let's drop his fat ass off before we go after Victoria Murdoch.]

Once that was done, Sid led the others back inside. All that remained was to arrest the company's executives, but all the elevators but one was out of order. This sole survivor slowly descended to meet them.

He stared into Murdoch's only working elevator as its tarnished maw ground open to admit him and his companions. The mirrors lining its interior were milky with age and neglect. He set a foot inside, and his bootheel adhered to the floor with a faint squelch. Worst of all was the Muzak emanating from a tinny old speaker within. The Crowley's Thoth anthem sanitized for a corporate setting was an insult.

Naomi withdrew, and glanced at Sid. [You can ride that death trap without me, if you like. I'll take the stairs.]

Morgan shook his head. [We all should take the stairs. I don't like the way the car sank when Sid stepped inside.]

[Shit. You noticed that too?] Sid found the stairwell, and recoiled at the stench as he opened the door. A cloud of fruit flies rushed toward them, eager to escape, and Morgan slammed the door shut to contain them. Naomi turned away, covering her mouth as if struggling with nausea, and Eddie bent over a trash bin to retch. 

Sid suppressed his own gorge with an effort. [Christ! They must have been dumping their garbage in there for weeks. It's a fire hazard and a biohazard combined.]

[Sounds like my refrigerator if I go without cleaning it too long.] Sid had forgotten about Claire, for she had been silent since he found the server room with her help. [Morgan, all the data you sent looks good, but I'm sending Astarte a copy to confirm.]

[Thanks. You still have floor plans for this building, right? Is there a safe way up? Elevators and internal stairway are out.]

[I just engaged the fire escape. If you don't mind busting some windows, you can burst in on Murdoch and take her from behind.]

Sid chuckled at Claire's phrasing. [My wife would kill me.]

Morgan glanced at Naomi. [My girlfriend would kill me.]

Naomi joined in as well, as if realizing this was a running joke Sid shared with the guys. [Claire would kill me.]

[Too bloody right. You promised you'd let me be your first --] Somebody booted Claire from the relay chat before she could finish. _Probably Morgan._

They looked at Eddie, who shook his head. [I guess I can take one for the team, Sid, but I thought you wanted to be the one to nail her.]

[To a fuckin' wall.] Sid shook his head and brushed past Morgan and Naomi. [Come on, guys. Let's go arrest some suits.]

The climb up to the top floor of the Murdoch Defense Industries headquarters was a slow one. A film of ice coated every exposed surface of the fire escape's ladders and platforms. Even with boots and gloves engineered for superior grip in winter, Sid and the others often slipped.

Sid stopped at the third floor and caught his breath. He looked through a window, and narrowed his eyes at the sight of dozens of cubicle slaves laboring at their terminals with downcast eyes. [Guys, is this shit creeping any of you out?]

[What do you mean?]

[Think about it, Nims. Eddie and I shut down the factory, but it's business as usual up on the third floor. Do people not talk to each other in this outfit?]

Naomi frowned as she considered Sid's question. [No doubt the management has a bad habit of shooting messengers. Isn't there a name for that sort of thing?]

[Yeah. It's the SNAFU Principle.] Sid cringed, preparing himself for a wall of text. Expounding on this fundamental rule of human interaction was one of Eddie's favorite pastimes when he had gone too long without getting drunk, stoned, high, or laid. It was bad enough when Eddie held forth while speaking; for him to do so over secure talk was a guaranteed eyestrain headache.

Naomi forestalled the lecture. [I've heard of it. Communication is only possible between equals because those with power tend to mistreat people who tell them unpleasant truths.]

[The work atmosphere must be so thoroughly poisoned that Murdoch has no idea what's going on.] Morgan flashed a nasty little smile. [This is going to be fun.]

Minutes later, Sid was able to read Murdoch's email by looking over her shoulder at the screen. She seemed oblivious to his presence and that of his companions. [So, how do we get in?]

Cracks radiated from the window's center as Morgan withdrew his gloved hand. He struck a second time, driving his strength into the glass through the palm of his hand as it struck. The window shattered on the third strike, and Sid was in Victoria Murdoch's office before she could spin to face them. 

She yanked open her drawer, offering Sid a glimpse of a revolver big enough to knock him on his ass if he fired it. Morgan slammed the drawer shut, and Murdoch got her hand out just in time to avoid losing a fingertip. He and Naomi held their swords on her, as Eddie backed them with his Dragunov.

Murdoch glanced around the room before glaring up at Sid. "Who are you, and by what right do you _dare_ force your way into my office?"

Morgan produced the search warrant, and handed it to Sid. "This might help."

"Thanks." Sid unfolded the warrant, and held it for Murdoch to read. "We came to execute this warrant, and search your business for evidence tying Murdoch Defense Industries to the Liebenthal gun-running operation and thus the coup d'état in Boston."

Murdoch glared up at Sid after reading the warrant. "I've heard nothing about this."

"Not surprised." Eddie chuckled. "Ever hear of the SNAFU Principle, Vicky? None of your underlings trust or respect you, so they never tell you a demon-ridden thing. It's basic human rel --"

"Cohen. We're on the job." Naomi forstalled Eddie a second time, for which Sid swore eternal gratitude. She smiled at Sid. "Should we question Ms. Murdoch?"

Murdoch rose and offered her hands for Sid to bind. "I won't answer your questions without an attorney present, so you might as well arrest me. What are the charges?"

Sid nodded to Morgan, and he and Naomi withdrew their swords. Turning Murdoch, he bound her hands behind her back as gently as possible. "Ms. Murdoch, you stand accused of multiple counts of worker exploitation, manufacturer and sale of weapons of mass destruction. Moreover, Murdoch Defense Industries stands accused of the same charges, as well as gross malfeasance."

Murdoch stared over her shoulder at Sid, her eyes wide with shock. "You're going after my business as well? Surely you don't think you can prove those charges before a jury."

Sid smiled for the first time since his arrival here. "Ms. Murdoch, I'm going to make you regret ever giving me cause to try. That much I guaranfuckingtee."
