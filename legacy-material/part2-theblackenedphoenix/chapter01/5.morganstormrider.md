The screen was glossy enough to show Morgan a ghost of Naomi's approaching reflection as she crept up behind him. She pulled aside his hair and kissed the junction of his neck and shoulder, and flashed an impish smile as she gently raked her teeth over his skin. Morgan turned in his chair to catch her lips with his.

She lingered a moment before straightening. "You're easily distracted tonight." 

"When you're around, it's hard to pay attention to anything else."

Naomi stole another kiss before checking out the screen. "Are these the Murdoch financials?"

Morgan nodded, and pointed at some dates. "These predate Victoria Murdoch's tenure. Seems she inherited the company from her father, George Murdoch."

Naomi scrolled, squinting at the figures specified in the financial statement on Morgan's screen. She tapped a link to the company's payroll figures. "What the hell?"

Morgan immediately spotted the cause of Naomi's surprise. "George Murdoch must have run the corporation as if it were a worker-owned cooperative, and paid the workers accordingly. I bet that screwed with the profit margins. Astarte?"

Morgan's AI appeared, tucking a stray scarlet curl behind her ear. "What's up?"

"I want you to crunch Murdoch's financial statements and generate some charts." He chased an intuition's shadow, cast by numbers proving that Victoria Murdoch did not run her father's business as he had done. _George Murdoch managed to run a prosperous business without shafting his workers. Why couldn't Victoria?_ "Correlate the financial statements with payroll data and stock price data from the New York Stock Exchange. List the major stockholders for each year, and tie that into the chart as well.

Astarte made a show of writing Morgan's instructions in a little notebook, but he could tell she was just a bit of fun for her. "The NYSE doesn't have any data dating to George Murdoch's ownership of Murdoch Defense Industries."

Naomi leaned over Morgan, and used his terminal to run a search. "Here we go. The company was privately owned under George Murdoch. Victoria did the initial public offering about a month after her father's death."

Morgan narrowed his eyes as he read the financial press' coverage of the IPO. "So, Victoria Murdoch inherited the company from George, started selling shares, and became a Wall Street darling for making daddy's business profitable."

"But it was profitable before the IPO." Astarte frowned a moment. "Oh, wait. We're talking about a New York stockbroker's notion of profitability. A single digit profit margin might as well be a loss."

Naomi idly played with Morgan's hair. "Are you teaching an innocent AI cynicism?"

"Nims, I haven't been innocent in a long time." Astarte smiled, and closed her notebook. "I'll need at least half an hour to work up comprehensive charts. I ordered pizza in the meantime."

The pizza came first. Morgan stacked them in the living room with plates and bottles of wine. Everybody was on their second slice when Astarte appeared on the screen. 
