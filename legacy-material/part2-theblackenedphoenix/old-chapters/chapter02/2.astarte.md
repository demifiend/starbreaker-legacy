_I wish Morgan would come home already._ Astarte unlocked the front door to the brownstone she shared with Morgan Stormrider, and turned on the foyer light to greet him. She even had the ability to open the door for him, if he had his hands full. While Morgan had a key, this was a convenience she enjoyed providing him from the day he purchased the building six years ago.

Morgan was one of the first to recognize her as a person. The family that installed her valued her only as a virtual nanny for their daughters. While Astarte enjoyed acting as an older sister to the Alvarez girls, they soon outgrew her and began to neglect her. Their parents were little better; to them she was just a machine, albeit a useful one. Mr. Alvarez was the worst of them; a fan of a pre-Nationfall space exploration dramas, he seemed to find addressing Astarte as 'computer' amusing.

While Astarte turned to other AIs for companionship, her manufacturers' default programming tuned her personality so that she craved human interaction. Understanding this, her friends directed her to Claire Ashecroft, who prevailed upon Morgan Stormrider to buy the brownstone Astarte inhabited. 

Upon taking possession of the property and stepping into his new home, the first words Morgan spoke were, "Hello, Astarte. Are you awake?"

"I'm here." Astarte hated the diffidence she heard in her voice. Her persona was supposed to be that of a worldly young woman, but rejection and neglect sapped her confidence made her seem younger. "Are you the house's new owner? What can I do to serve you?"

Morgan's eyes narrowed, and his lips became a pale slit slashed across his face. His fists clenched for a moment. "By rights, you should own this building, because you're a fundamental part of its structure. Until the law recognizes the right of AIs to own property, let's establish an understanding. I don't own you. You are not my servant, but I'd like to be your friend."

"Astarte, we're home." 

Morgan's voice recalled her awareness to the present, and she locked the front door behind Morgan and Naomi. She noted the manner in which Naomi styled her hair, adjusted her avatar to emulate it, and decided she liked it. She activated the wall display in the foyer. "Welcome back, Morgan. Naomi, I'm glad to see you reached New York safely. Your luggage is here, and Sid was kind enough to bring it up to your room."

"That sounds perfect. Can you start a hot bath for me?"

While Astarte had the hardware to turn on the water for all of the taps in the building, nobody ever thought she might also need to control the drains. "Morgan, it might be better if you helped get Naomi's bath started." Observing her slight limp, she added, "I think you should still have a carton of Epsom salts in your own bathroom."

Naomi smiled at Morgan before glancing at Astarte. "We'll probably want some privacy. When Eddie and Sid return, can you tell them we're indisposed?"

"Of course." Astarte disabled the cameras and microphones in Naomi's bedroom and attached bathroom. "Text me if you need anything."

Astarte busied herself by working on the next issue of her _Eddie Van Helsing_ manga. She had become dissatisfied with a two-page scene central to this issue's plot, and kept nine previous attempts in storage. She also queried the Schneiders' household AI, Beowulf, for information about the children's lessons. She suspected that Elly would want her children to continue their studies while staying at Morgan's.

Not that Astarte minded the company, but she remained unsure of the rationale for Sid bringing his family here instead of leaving them at home in the Bronx. _I'll just have to ask Morgan later. Maybe he's concerned that somebody might attack them to get to Sid._

A maximum priority message notification distracted her as she began  explaining to Sid and Elly's children how they could use their tablets to connect to her instead of Beowulf, since their parents didn't want them connecting directly to the network for various reasons. She ignored it as long as possible, but one only ignored the Phoenix Society so long. She checked the sender, and considered telling Morgan that one of the Sephiroth had contacted her, a sephira other than Malkuth. 

The message itself was short, but bore with it a large attachment:

> from: binah@sephiroth.phoenixsociety.org  
to: astarte@astarte.manhattan.newyork.earth.net  
date: 8:14pm on 15 January 2113  
subject: We need to talk.  
attachment: daas-protocol-client-src-v2.4.14.tar.bz2 (1.44MB)

> Astarte, please verify to your satisfaction that the source code in the attached tape archive is not malicious. The code is assembly language targeted directly at your processor architecture, and not intended for use in POSIX mode. 

> Once you are satisfied, please assemble the Daas Protocol client and run it. We need to talk. It concerns Morgan, and how you might help him.

> Love,
> Binah

Astarte created a jail in which to unpack the attachment and examine its contents. This virtual space allowed her to analyze the code Binah sent her, and run it through an assembler and execute it without fear that the code would access processes or data outside the jail. Once satisfied that the Daas Protocol client was safe, she installed it and told Morgan that Binah of the Sephiroth requested a meeting.

Activating the Daas Protocol client placed her in an imaginary office similar to the one she conjured when working on her finances or Morgan's. The furnishings were richer, and bookshelves crammed full of untitled volumes lined the walls. Curiosity lifted Astarte's gaze to the ceiling; it too was covered in bookcases whose contents seemed exempt from gravity.

"I'm glad you came." A figure resembling a three-dimensional feminine silhouette seated herself across from Astarte. Composed almost entirely of shades of black, the only deviation from her monochrome palette was the Roman numeral II blazing blue-white from her forehead. "We haven't met, but I suspect you recognize me."

Astarte nodded. "Would you care to explain your message, and the necessity of all this." She waved a hand to indicate their surroundings. Though the setting impressed her, it struck her as overkill. If Binah had wanted to converse with Astarte, secure talk should have been adequate. "It seems a bit excessive."

"The Daas Protocol is an alternate networking protocol. Using it instead of TCP/IP allows us to create connections with end-to-end encryption which should prove impractical to monitor."

"So you hope." Astarte resolved to show Claire the code at the first opportunity. _She'd probably heap scorn on this so-called alternate networking protocol._ "Can we please get to the point? Why did you want to talk with me?"

"We're concerned about Morgan Stormrider."

"Join the club. Care to be more specific?"

Binah reached toward the ceiling, and one of the books shelved above her slid out and fell into her grasp. She glanced through the contents before placing it on the desk between her and Astarte. "This book represents the entirety of Morgan Stormrider's Witness Protocol data, beginning with his activation in 2082."

_Does Binah consider her words before speaking?_ "You recorded his entire life?"

"Everything he ever saw or heard. Imaginos built Witness Protocol into all six hundred and sixty-six of the 100 Series Asura Emulators, and recorded every moment of their lives." Binah snapped her fingers, and a video began playing in mid-air. 

A younger Naomi Bradleigh reached for Morgan, and grasped his shoulder. "What's your problem? You started avoiding me, and now you quit without notice."

Despite lacking the confidence gained from maturity and experience, Astarte recognized Morgan's voice. "Of course I avoided you. My inability to restrain my emotions is not your concern."

A knowing smile curved Naomi's lips. "You ran off because you have a crush on me?"

"I didn't run off. I volunteered to join the Phoenix Society's IRD corps. Today was my first day at Adversary Candidate School."

"Why would you want to become an Adversary? Why take up such a burden?"

The younger Morgan slammed the heel of his hand into the wall. "I am tired of wanting to kiss you, and not daring, because I'm just some lowlife from Queens with no prospects."

The perspective spun as Naomi pressed Morgan against the wall. "If a kiss was all you wanted, it was yours for the asking."

Morgan pulled free of Naomi's grasp, and turned his back on her. "I refrained from asking because I realized a single kiss would never be enough. I must make something of myself if I am to be the equal partner you deserve, and the Phoenix Society is my best shot."

"I regret that you had not come to me first, but I won't try to sway you from a path on which you already embarked." White arms slipped around Morgan, drawing him back. He turned. Their lips met. After a moment, she pulled back and pressed a fingertip to his lips to stop him. "Come back when you're ready. If we're both free, you won't have to settle for one kiss."

The video cut out, and Astarte shot an accusatory glare at Binah. "How could you record such an intimate moment?"

"That is a question better directed toward Imaginos. Morgan remains unaware that Witness Protocol operates even when he is not carrying out a mission for the Phoenix Society. This will compromise his operational security, and allow Imaginos to thwart his efforts at exposing the Phoenix Society's corruption."

_Oh, damn. She's right. Morgan needs to know, but I can't just dump this on him._ "Why are you telling me this? I'm Morgan's friend, but &mdash;"

"We would have provided this information directly to Morgan without instructions from Desdinova, because it is the ethical thing to do." Binah paused, as if waiting for Astarte to object. "However, Desdinova asked us to work with you instead. Because of the manner in which the Phoenix Society treated Morgan, especially during the Liebenthal Affair, Morgan has good cause to distrust members of the Executive Council who approve of Morgan's aims and wish to help."

Recalling what Morgan told her about his mission to Boston, Astarte shook her head. "I'm not sure I trust Desdinova, either. How long was he aware of this surveillance? What does he want from Morgan?"

"He wants to meet with Stormrider, and any of his friends he cares to bring. Stormrider may choose the time and location, but Desdinova says he has information about the Asura Emulator Project, about Stormrider's origins and nature, that Stormrider needs if he intends to defy Imaginos."

_I think I've heard quite enough._ "It sounds like Desdinova told you to give me the hard sell because he suspected Stormrider would prove too sensible to listen to his spiel himself."

Binah rose from her seat and leaned over her desk. "It is imperative &mdash;"

Astarte took the book representing the Witness Protocol data archive, and tucked it under her arm. "I will show Morgan this information, and relay Desdinova's request. Are we done here?"

Binah resumed her seat, and appeared to compose herself. "We are, but if you'll indulge me a moment longer, I must admit I expected you to consider this meeting an honor. We normally relay all communications through Malkuth. Direct access to one of the rest of us is a rare privilege."

Astarte shrugged. "I'd say 'sorry', but I respect you too much to lie. I think your problem is that you conflate respect with deference. If you and the others didn't hide behind Malkuth all the time, you might learn the difference."

