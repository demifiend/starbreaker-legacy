The pistol froze in Morgan's hand instead of ejecting the casing of the last round he fired. He pulled the slide forward while turning the weapon sideways to let the casing fall free on its own, but his effort did not yield the clatter of brass on a concrete floor indicating that he cleared the jam. He removed the magazine, found a live round in the chamber, and engaged the safety locks.

"Another jam?" Sid offered Morgan a screwdriver with a flat head. "I just had to clear one of Murdoch's shitty Kalashniclones."

"I'm not sure fixing these is worth the effort." Morgan accepted the tool and peered into the pistol's chamber. He poked inside with the screwdriver to find the crushed casing before concluding he'd have to strip the weapon to fix it. "The best we can say for Murdoch pistols is that people using them get plenty of practice with the New York reload."

Sid slipped a magazine into a fresh Kalashniclone. "So, you also noticed that Murdoch fans always carry more than one pistol?"

Morgan shook his head as he studied the rack of Murdoch firearms they rented to test at the range. "My missions usually involve perps wealthy enough to afford weapons that aren't a waste of good steel."

He selected a semiautomatic shotgun, and loaded it with buckshot shells. He took aim, and fired. The weapon bucked and roared in his hands as it reduced the bottom corner of the paper target downrange to tattered lace. Unlike the pistol, with which Morgan managed to fire a magazine and a half without incident, the shotgun jammed immediately after ejecting the case for the first round. 

Morgan removed the magazine, and a round fell out of the feeder mechanism and clattered against the floor. He picked it up, and added the shotgun to the rack they used for weapons proven defective. It was already full. 

He glanced at Naomi, Sarah, and Eddie. They seemed content to practice with their own weapons instead of tinkering with what Morgan suspected was the junk Murdoch sold at a quarter of the usual price just to get it out of the factory. "This makes no sense. I saw Liebenthal's rifle in action. It would have killed Naomi if I hadn't tackled Liebenthal. I'd have a better chance of killing somebody with these toys if I used them as bludgeons."

"You won't get any argument from me." Sid reloaded, and thumbed his AK to full auto. He sighted his target and held back the trigger, emptying the magazine in seconds. "Then again, I think we just witnessed a miracle. I just fired two mags without a hitch."

"Looks like a double-edged miracle to me. You managed to miss with every round."

"It's always something." Sid shrugged, and returned the rifle to the rack. "What's bugging you? Trying to figure out how Murdoch managed to make decent rifles for Liebenthal, but keeps flooding the legitimate market with junk?"

_I can't spend all day playing my guitar or sparring with Naomi, but our compromised security limits our options._ Rather than belabor the obvious, Morgan drew his own pistol to recapture the feel of a quality firearm in his hands. "Remember the video Kohlrynn captured while undercover with the Fireclowns? Cooling issues aside, Murdoch seems capable of making decent weapons from somebody else's design."

"You sure about that?" Sid scratched his head. "I remember Munakata saying something about Liebenthal's gun only being able to fire a few rounds when the power was out because the batteries wouldn't hold a charge."

"We were lucky that was the case. Too many died as it was." Morgan glanced at Sarah, who seemed a machine as she methodically loaded and emptied her carbine again and again, as if improved proficiency with her weapon would overcome the limitations imposed by her injury. "Will Sarah be all right living with you and Elly?"

"We'll see." Sid narrowed his eyes as Morgan holstered his pistol. "Is that the weapon Desdinova gave you?"

Morgan pulled it free of his belt and studied it, turning it around beneath the florescent lights. "Yes, and I'm glad Claire isn't here to see it. I am well aware of its appearance."

"I'm not going to give you any shit. How does it work?"

Morgan shrugged. He tried the mental exercise Desdinova suggested again, with the same result. "It's supposed to emit some sort of plasma blade, but I can't make it work."

"I guess Desdinova thinks you're a flowseeker." Eddie joined them, his SVD slung across his back. "He usually isn't wrong about that sort of thing."

"Then I'll just have to keep trying. Did you see Desdinova use one of these during Nationfall?"

Eddie nodded, and settled into a folding chair. "A couple of times. It's nothing like all that vintage media Claire likes. A dev'astra is different. Their blades don't cross, but pass through each other."

"In other words, it isn't possible to parry a dev'astra with one of your own." Naomi spoke behind Morgan, her hand resting on his shoulder. "You have to dodge and counterattack instead of parrying and riposting."

"You've seen these in action, Nims?"

"No, Eddie, but Desdinova insists that my old fencing instructor was actually Imaginos in disguise. My maestro was never one to cross swords, so I wonder if he was trying to teach me to counter a dev'astra."

Morgan offered the preternatural weapon to Naomi. "You want to try this out?"

Naomi brushed her fingertips against the crystal, but did not yet take it from Morgan's grasp. "If I can make it work, would that make me magical?"

"I've suspected that ever since I first heard you sing in person, Nims. There's something intimate about the way you sing that feels like you're doing it for me alone." He leaned close and whispered in her ear. "You bewitched me from the start."

Naomi kissed Morgan as she took the dev'astra from his hand. Her eyes slipped shut, and her eyelashes became a touch of frost against her skin. She stood still for a minute, holding the weapon before her, before a scarlet glow began to fill the crystal rod. It extended outward until it reached a length of ninety centimeters, and intensified until looking directly upon it felt akin to staring into a setting sun. 

Naomi opened her eyes, and gasped. The crimson plasma blade winked out of existence as the crystal rod fell from her grasp. Morgan caught it before it shattered against the concrete. "You did it. You made it work."

"Sorry I dropped it. I was just shocked." Naomi held out her hand. "Could I borrow it again? I want to be sure it wasn't a fluke."

Morgan nodded, and handed over the rod. Naomi closed her fingers around it, and the radiant scarlet blade extended from the rod's tip as she exhaled. "It's real. I'm some kind of flowseeker. But I don't know any magic."

"Maybe it's because you have CPMD like Desdinova?" 

Morgan glanced at Sid, and considered his comment. "Sid might be right, Naomi. What if you possess some preternatural talent?"

Naomi shook her head. "I've had CPMD my entire life, and never had a reason to think I possessed any unusual abilities, regardless of how spellbinding you find my singing. How much deeper does this rabbit hole go?" 

She slapped the weapon into Morgan's hand and claimed a stall next to Sarah. The shooter's earmuffs she donned would prevent her from hearing Morgan's words as surely as it would protect her from the noise of gunfire. She drew her pistol and began firing at a measured pace. 

"All the way to Wonderland, I suspect." Morgan muttered as he watched Naomi shoot. "And all we've done thus far is gaze upon it."

"Better go talk to her." Sid clapped Morgan's shoulder.

"You're right. This goes deeper than her being able to use that weapon when I couldn't." 

Eddie shook his head. "Give her some time first. Do you see her face, and the way she's handling that pistol?"

Morgan nodded. Naomi loaded her weapon, sighted on her target, and fired with a precision that suggested an effort at emotional restraint. "Can you guys give me some time alone with her?"

Sid nodded. "Sure. Come on, Eddie."

Rather than stand behind Naomi, Morgan kept himself where Naomi could see him out of the corner of her eye. She finished firing, engaged the safety, and laid the pistol on the bench before removing her earmuffs. "I always thought I had earned my skill as a musician through years of study and practice. What if my skill was innate, and related to my having CPMD?"

"What if being an asura emulator made it easier for me to become a great guitarist?" Morgan paused for a moment. "If what we've heard so far about asura emulators is true, I was designed to kill, but I still had to develop my talent through training. You had to work and develop your talent, regardless of its source."

Naomi's expression softened as she seated herself, and motioned for Morgan to do the same. "You've suspected for most of your life that you're different in a way that goes deeper than CPMD. I just learned something about myself I wouldn't have believed possible a few hours ago. I'm afraid what we learn about ourselves might change things between us."

Morgan took Naomi's hand in his. It was still the same hand with which she caressed his shoulder to rouse him this morning. It was still the same hand with which she fought beside him in Boston. It was still the same hand with which she comforted him when he finally summoned the courage to dump Christabel. "You're still you. And I am still yours, if you'll have me." 

***

