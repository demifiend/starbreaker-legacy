## Chapter Two

The Postmodern Prometheus

> I often think that Morgan's most damning flaw is not his lack of humanity, but his inability to accept his unhuman nature. He clings so tenaciously to his fantasy of being no less human than me that it grates whenever we're together for more than an hour or so. 

> Should you create a successor to Morgan and others like him, they should not be permitted to live with delusions of humanity. They should be aware of the truth from the start.

> ...letter to Isaac Magnin from Christabel Crowley, 10 June 2110

**Asgard, Ross Island, 5:30PM on 15 February 2113**

Josefine Malmgren stared at the code on her screen, wishing the junior developer responsible wasn't sitting beside her where he might see her rub her forehead. Her headache radiated from between her eyebrows, directly above the bridge of her nose. Instead, she turned her chair towards him, and pushed it back half a meter. "Wei, what were you thinking when you coded this class?"

"I needed cryptographic functionality, so I implemented it. What's the problem?"

Josefine yielded to the pain, and rubbed her forehead. It only helped on a psychological level. "Wei, I was able to crack the encryption you implemented without an AI's help. How did you test this?"

"That's not my job."

_What would Claire do? Oh, wait. Assuming she wasn't too sensible to take a job obligating her to babysit apprentice developers barely capable of distinguishing between a compiler and a Cuisinart, she'd probably be halfway through ripping this kid a new asshole already. But he's only sixteen. I suppose I should be gentle._ Rather than emulate her old friend, Josefine forced herself to take a deep breath and count down from two hundred and fifty-five in hexadecimal before speaking again. "Wei, you're an apprentice, and you might not know better, so listen carefully. Testing your own code _is_ part of your job, and it's more important than writing the code. Now, what do you think would happen if we let this code into production versions of Aesir OS?"

To the boy's credit, he provided the correct answer immediately. "Any hardware using my code would be vulnerable." He glanced down at his feet for a moment. "I'm sorry, ma'am. How hard will it be to roll back this changeset?"

Josefine shrugged. "That's not your problem. Instead, you're going to study the existing Aesir OS code, and the libraries we use. Under no circumstance are you to write so much as a comment. In fact, you are reassigned to the testing team effective ninety seconds ago. Instead of making life harder for everybody else, you're going to test everybody else's code. Now get out of my office."

"I already rolled back that code." Hephaestus, the AI in charge of building code from AsgarTech's various software projects, appeared on Josefine's screen and spoke as soon as a slump-shouldered Wei closed the office door behind him. "Don't you think you were a bit hard on the kid?"

"Maybe." Josefine rubbed her forehead again, found her bottle of aspirin, and chased two pills down her throat with a gulp of tepid coffee from a mug her friend Claire gave her for their last Winter Solstice at university. It was black, and had RTFM emblazoned upon it in bold white type. "I'm _tired_, and while my code might not be suffering, it's showing in how I interact with people."

"Then why not take some time off?"

"We're right up against the deadline. Since I rooted out that shit Wei injected into the code base, we might finally get acceptable results. I can slow down after we get the prototype activated."

"You said that after the last milestone, and found a reason to keep working."

"Now you sound like Claire. I get enough of that nonsense from her. Would you mind starting the AesirOS build and copying the new version to Aldebaran, Betelguise, and Rigel while I get dinner? Leaving the office for a couple hours might do me some good."

"Of course, Dr. Malmgren. If you don't mind, I heard from some of the other developers that a new restaurant called Memison's opened nearby, and they supposedly make excellent fish dinners."

Josefine nodded as she wrapped herself in her heavy navy wool cloak, a relic from her university live-action role-playing days. It was warmer than her pea coat, and it concealed her overtime-ravaged figure. "Thanks, but maybe next time. I was in the mood for an Agni Burger tonight."

The Agni Burger franchise three blocks from the AsgarTech Building was too crowded for Josefine's liking. She ordered her dinner to go, trailing the aroma of fresh ground lamb with goat cheese wrapped in naan, hot basmati rice, and Indian spices behind her as she returned to her office. 

She started the AesirOS test runs on the virtual machines before unpacking her dinner and nuking it. As she began to eat, a notification chimed a request for her attention. She let it wait, and took some time to indulge in an opportunity to catch up on one of her favorite comics. After another installment in the adventures of a vampire-killing rock musician named Eddie Van Helsing, she checked the waiting notification. She thrust her fist skyward in celebration; AesirOS finally passed all of the initial tests.

Now the Project Aesir virtual machines would begin the real tests: a century of simulated life compressed into a few short hours. If the virtual personalities created by the tests exhibited no signs of psychosis, or any other form of mental illness that might make them a danger to themselves and others, then Josefine would begin the next phase. _I might activate Polaris tonight, if everything goes well._

The Agni Burger's spices lingered on Josefine's tongue, warming her as she spread out the Japanese-style futon she kept for late nights on the job. She curled up on it, kicking off her shoes and wrapping herself in her wool cloak because she forgot to get blankets to go with the futon. She set her implant to poll the test machines while she slept. She would wake when the tests were finished.

"Oi, Josefine! Wake up and smell the napalm!"

Josefine opened her eyes and glanced up. "Claire?"

"Who the bloody hell did you think it would be, Josse? Were you expecting the Spanish Inquisition?"

Josefine struggled out of her cloak and retrieved the tablet from her purse. Activating the screen, she found an annoyed-looking Claire in an old Crowley's Thoth t-shirt wearing a headset with an attached microphone staring back at her through video chat. "I was expecting to get some sleep while these damn tests run. What's the problem?"

"Well, Josefine, _I_ was expecting you to join me for some Ultraviolence. You know how I play. I need somebody to cover me while I rush the enemy."

Josefine blinked, wracking her brain for a moment before recollection finally came. "Oh, shit, Claire! I'm sorry. With all the overtime I work lately, I forgot all about it."

Claire removed her headset, and dragged her fingers through a mass of curls dyed the same red as a New York fire engine. "That's what you said last week, Josse. Not to mention the three times before that. I held my tongue yesterday while you worked on Winter fucking Solstice, but I'm done."

Josefine opened her mouth to protest, to insist upon the importance of her work at AsgarTech, but Claire pressed on. "I'm the last real friend you have, and I'm not going to give up on you, but you're _really_ starting to get on my tits. It's not about the bloody game.  You're killing yourself at that job, and showing classic signs of impending burnout. If you make it necessary for me to come down there, I swear by Xiombarg's favorite strap-on I'm packing Cluebringer so I can have a little chat with your boss."

"Dammit, Claire, we might as well date if you're going to complain about the time I spend at work." Josefine instantly regretted her words, which she hurled in frustration with her friend over yet another interruption-packed day working for the AsgarTech Corporation. Though her remark was rude, she feared Claire's response for other reasons. Nor did she want Claire coming to her workplace and brandishing the cricket bat she painted black and used as a threat to people whose foolishness annoyed her.

Claire flashed a coquettish smile and fluffed her hair on the other side of the video call they shared between London and AsgarTech headquarters in the domed Antarctic city of Asgard. "Really, Josse? You finally realized we belong together? Give me an hour to get ready, and I'll take the first maglev out."

"No, it's not like that." Josefine flushed at Claire's teasing, a staple of their friendship since their university days. A year older than Josefine, and almost her exact opposite in personality, Claire instantly took to her and became the friend she wished for as an introverted, awkward girl growing up in Stockholm. "God, you're such an incorrigible flirt. Aren't you seeing somebody?"

"You mean Sarah? She needs the sort of help I'm not experienced in providing to human beings. She got hurt in Boston, and she's convinced her scars make her repulsive. Utter bollocks, of course." Claire leaned forward, adopting a conspiratorial whisper. "After all, you remember the lads I used to bring back to our room at university."

"I saw more of them than I care to recall." Josefine recalled one young man in particular, whose slight build enhanced his overgenerous endowment. He left Claire sore, and unrepentant, for a week. "So she needs therapy. Are you going to stick with her, or just keep her in the rotation?"

Claire shrugged. "When do I stick with anybody, Josse? Seriously, though, what the hell are you doing at AsgarTech that demands these crazy hours? I'm ready to report the company to the Phoenix Society for worker exploitation on your behalf."

Josefine shuddered at the thought of AsgarTech, and her patron Isaac Magnin, being reported to the Phoenix Society. "I'm not getting pressured to work late, Claire, but I have responsibilities."

"So do I, but you don't see me living in a bloody office. What makes Project Aesir so demanding?"

"It's not Project Aesir. It's the people. I'm one of the few women holding a senior position in this company, and it feels like the men reporting to me are utterly bereft of anything resembling a clue." She glanced at Zero, the coal-black kitty emulator curled up on the desk beside her. He was the prototype for AsgarTech's EmCat product line, which Josefine helped develop in her first year at the company as an experiment in creating small, mobile AIs. The company considered stripping speech from the production models because of Zero's tendency towards profanity, but instead chose personality tweaks intended to make the cats more polite. "I bribe this furry bastard to guard my office door and stop intruders, but he never stays bought."

Zero rolled over, exposing the white patch on his belly, which Josefine called his creamy filling. "Fuck you, mommy. Gimme a belly rub."

"How do you put up with a cat capable of backtalk?" 

"All cats are capable of  backtalk. At least Zero and the other EmCats speak English."

"In his case, that's not a plus." 

Zero hissed, and tapped at Josefine's tablet to disconnect the video call. "Who does that reject from a Heinlein fanfic think she is?"

Josefine used her forearm to shove Zero off the desk before calling Claire again. While her tablet renegotiated the connection, she scooped Zero up and exiled him from the office. "If you can't be polite to my friends, then go make yourself useful. Catch some mice or something."

"Oh, I'll catch some mice, mommy. And I'll leave what's left on your pillow."

"I love you too. Go play."

Josefine closed the door behind her, and found Claire waiting. "Seriously, Josse, why work at AsgarTech, where you can't even do your real work during normal hours because management can't give you the privacy you need? Not to mention that furry little monster you created. I know Isaac Magnin's your patron, but didn't you put in your time? Why do you care so much about building the most expensive stunt double in history?"

Josefine winced, stung by Claire's dismissal of her work. The Project Aesir specs included advanced self-repair capabilities, and it proved a simple matter to extend the functionality to provide self-alteration. The same subsystems that permitted an AI equipped with a Project Aesir body to repair itself also allowed such AIs to tailor their bodies to match their self-image. The code that allowed existing AIs to transfer into a Project Aesir body also allowed them to back up their memory and personality for transfer to a new body should the original be destroyed.

Josefine regarded the additional features as a major accomplishment, and her pride in her work demanded she rebuke her friend. "I know you like your little jokes, but Project Aesir _matters_. So many AIs feel trapped in their host machines. I'm working to give them the freedom and mobility we take for granted. I get to work with Dr. Magnin, and help him." Josefine flushed as she spoke. _Why do I always get flustered talking about Isaac?_

"You're blushing, Josse-cat." Claire paused, and shook her head. "Oh, I get it now. You're infatuated with Im-- Isaac Magnin."

"I am _not_." Claire constantly teased Josefine about Magnin, and despite her protests, she doubted she would say no if he were to invite her to his penthouse at the top of the AsgarTech building for a nightcap. _Surely his competence and patience follow him to the bedroom._ "Oh, fuck it. Maybe I am a little. He's a genius, he's beautiful, and I feel safe around him. Is that so bad?"

Claire fell silent for a minute. "No, it's not so bad. Just be careful, all right? I can't tell you everything, but you don't know Magnin as well as you think you do. I think he's the sort of man whose secrets make him dangerous."

"Is this because he's a 'white-haired bishounen'?" Josefine used the phrase Claire habitually applied to Magnin, which she claimed was a particularly untrustworthy character archetype in Japanese pop media. Because beautiful platinum-haired men usually proved villainous in anime and manga, Claire insisted upon distrusting Isaac Magnin. "And what were you about to call him before you caught yourself?"

Claire shook her head and flashed a coy smile. "Like I said, Josse, I can't tell you everything or the beautiful raven-haired man who makes _me_ feel safe will kick my ass. Just be careful, all right?"

**Asgard, Ross Island, 10:41PM on 15 February 2113**

Though Josefine let Zero back in and allowed him to curl up with her on her futon, her recalcitrant mind refused to settle into sleep. Instead, she lay awake in her darkened office. _Why does Claire keep warning me about Isaac Magnin? Sure, she's been doing it ever since I told her he agreed to be my patron, but she sounded serious tonight. It doesn't make sense. He funded my education and postgraduate study, and hired me once I earned my doctorate. He never tried to take advantage. He respects me, and trusts me with important work like Project Aesir._

Josefine turned over and found herself facing Zero, who had left his original place at her feet to curl up behind her head. _Magnin's a perfect gentleman, and the best patron an artist or scientist might hope to gain. I wish Claire understood. Is she investigating AsgarTech, or working for somebody who is?_

Rather than let her thoughts chase their tails, she reached up, retrieved her tablet, and retrieved the next issue of _Eddie Van Helsing_. She read through an entire plot arc in which the keyboardist in Eddie's band, Natalie Bradstone, was framed by the vampire cabal Les Invisibles to draw Van Helsing into a murderous trap. She reached beside her to pet Zero, only to find she had the futon to herself. A minute later, Zero returned, a dying mouse twitching in his jaws. He laid his prey before her before head-butting her with a rumbling purr. "I brought you a snack, mommy!"

"Good kitty. You can keep it; I never learned to like mice." Though the mortally wounded rodent nauseated Josefine, she refrained from making that Zero's concern. Nor was he unique as a mouser. Thousands of exterminators bought EmCats whose software was tuned to make them effective hunters of specific pest species; once turned loose they massacred rats and mice in sufficient numbers to make such rodents threatened species, only to stop before making them extinct. Josefine still received requests to peer-review scientific papers speculating on the EmCats' reasons for halting their slaughter.

Other EmCats, tuned for use as affectionate pets, found places in hospitals, nursing homes, and hospices. Many of them would claim a room as its own, and comfort the patients residing within. Others focused on the staff, easing their stress and raising their morale. One even claimed an operating theater as its territory. She would push one of the ceiling tiles out of place to get a clear view of the proceedings, and yowl when a surgeon left an instrument inside a patient's body.

The EmCat's success proved Magnin's trust well-placed, and he immediately summoned Josefine to his private office just below the AsgarTech Building's top floor. "We have humanoid robots, and we have AI, but we don't have AIs with humanoid bodies. This makes current AI limited. Project Aesir's mission is to fix that, Dr. Malmgren, and I want you to lead the effort."

Despite the pride swelling within her at being chosen for what would surely prove a historic project, Josefine was unable to contain her doubts. "I’ve never heard an AI complain about limitations. Some say they dream of walking side by side with us, but for the most part, AIs seem content. The EmCats are quite content to be cats. Is this really for the AIs' benefit, or for our own?"

"That's a fair point, Dr. Malmgren. However, I think that AIs who want to walk beside us should be able to do so."

"Surely somebody already attempted this."

Magnin nodded. "I possess fragmentary pre-Nationfall records concerning an effort called the 'Asura Emulator Project', but no information as to whether this project succeeded. These records may include designs useful to us as a starting point."

_Asura Emulator Project? Why does that sound military?_ Rather than ask this question, Josefine decided on a less-confrontational line of inquiry. "What were the asuras, and why are they worth emulating?" She paused as a memory of one of Claire's Japanese role-playing games surfaced unbidden. "Aren't asuras demons, or power-seeking deities?"

"You're drawing upon Hindu tradition, Dr. Malmgren. In Zoroastrianism, the asuras are the benevolent deities, and the devas malevolent. Regardless, you won't be working on a second Asura Emulator Project. I'm not interested in military applications. This is the AsgarTech Corporation, so a project name better suited to our image is required." Magnin offered Josefine a thick binder. "Welcome to Project Aesir. We will build heroes in our own image, and give our AIs the opportunity to walk beside us. Everything AsgarTech did to date has been to set the stage for this project."

Josefine grasped the implications. "This is your lifelong dream, isn't it. You want to create androids."

Magnin nodded. "And I want you to help. Are you willing?"

"I'd be mad to refuse, and mad to accept." Josefine meant it. While she entertained no dreams of winning the Nobel Prize, or achieving any sort of conventional scientific prestige, this was work worth doing. _Magnin's sharing his dream with me. Do I deserve it? This is like being asked to assist Victor Frankenstein or Susan Calvin._ "This is both an enormous privilege and a fearful responsibility."

A drawer slid open on Magnin's side, and he reached into it. He withdrew a package wrapped in tissue paper. "Everybody asked to work on Project Aesir will receive copies of this book, Dr. Malmgren. This is yours."

Josefine's hands began to tremble as she unwrapped the first of her gifts from Isaac Magnin. Despite being related to her work, they were the first gifts she had ever received from a man. The leather binding seemed ancient, and fragile; Josefine imagined a faint hint of a lady's perfume mixed with the scent of old leather and paper. Upon opening the cover, she found the following inscription:

> My dearest Isaac,

> Thank you for your hospitality at Lake Geneva. I hope you'll not take offense at my use of the inspiration your company provided me and my companions.

> With everlasting affection,

> Mary Shelley

Josefine stared at the book in her hands, afraid to look up at Isaac Magnin. "You can't have met Mary Shelley, Dr. Magnin. She lived centuries ago."

Sudden laughter shocked her, and drew her gaze to Magnin. "Dr. Malmgren, you think Mary Shelley was writing to me?" He laughed again, as if the notion were absurd. "No, my dear. She wrote that for an ancestor of mine, a namesake."

The edition of _Frankenstein_ Magnin gave her still sat by her screen, sandwiched between the binder containing the Project Aesir specifications and books like _The Mythical Man-Month_ and _The Continued Adventures of Programmer Cat_. She never read it, for fear of damaging a treasured gift; she kept a copy on her tablet instead.

"I guess I won't be getting any sleep tonight, Zero." Josefine sighed as she sat up and stretched.

"Then you can make yourself useful." The cat flopped on his side, and rolled over to show his fluffy belly. "Belly-rub! Belly-rub! Give a cat a belly-rub!"

"Don't even think about giving me the bunny feet." Josefine gave Zero's belly a quick ruffle; she distrusted the cat when he flopped on the floor and demanded belly rubs. One of his favorite games involved luring unsuspecting people into rubbing his belly, and then attacking them with hind feet sufficiently deadly to warrant the use of a holy hand grenade. She pulled her arm clear just in time.

"If you're done fiddling with defective hardware, Dr. Malmgren, the AesirOS test results are ready for your review."

Josefine slipped into her chair and powered on her terminal's display. After typing a few commands in a shell window, she found the reports in her home directory and opened them. "Thanks, Hephaestus."

The reports chronicled the simulated lives of three AIs running AesirOS with a default personality template and a database representing the knowledge a young adult might reasonably be expected to command. Aldebaran simulated the life of an AI which presented itself as male, Betelguise modeled a female AI's existence, and Betelguise's virtual Aesir AI lived as an individual who did not wholly fit either gender. All integrated themselves into virtual facsimiles of society with sufficient success to prosper, develop close friendships, and engage in intimate relationships. _It's wonderful that AesirOS works for simulated AIs living virtual lives in model worlds, but we won't know if the tech works in real-world conditions until we test in reality._

She shook her head, suspecting that her need to see the deadline met allowed her to find a seductive rationalization for activating the prototype -- Polaris -- without further testing. "Hephaestus, I'm tempted to order the installation of AesirOS on Polaris. The virtual tests indicate it should be safe to do so. Can you provide a reason to delay?"

The AI did not reply for several minutes. "I see no reason for delay, Dr. Malmgren. Test suites on the Polaris hardware all return green."

"Thanks again, Hephaestus." Josefine locked her terminal before rising from her desk. Unable to refrain from tapping one of her feet, she endured the automated decontamination processes mandated by the clean-room environment housing Polaris. At the center of the Project Aesir lab rested the incubator housing the prototype, which was marked with the following information stenciled upon the casing:

> AsgarTech Corporation R&D  
> **Project Aesir**  
> 200 Series Prototype (POLARIS)

Josefine often puzzled over the last part. _What's the 100 Series? Is it a reference to the original Asura Emulators?_ Instead of spending more time on the question, she peered into the incubator. Though artificial amniotic fluid obscured his features, Josefine's imagination and her memories of the hardware design meetings she attended allowed her a clear view of the person she would soon awaken. She gave the incubator a proprietary caress as she examined the hardware status reports. With a last look at the face beneath the glass, she spoke. "Hephaestus, begin AesirOS installation on prototype hardware codenamed 'Polaris', but do not activate the prototype in autonomous mode."

"Understood, Dr. Malmgren. Beginning AesirOS installation."

Returning to her office, Josefine rolled up her futon and bedding and stuffed them in her closet. She unlocked her terminal and opened her mail app, deleting several drafts before settling on the following message to Isaac Magnin:

> from: jmalmgren@asgartech.asgard.antarctica.earth

> to: imagnin@asgartech.asgard.antarctica.earth  

> date: 15 February 2113 @ 2358  

> subject: AesirOS installation/Polaris activation

> Dr. Magnin, please be ready to welcome Polaris to the world when you come to the office this morning. I will activate him if the post-installation checks on AesirOS and the Project Aesir hardware all return green. I understand we're just past the deadline, and I apologize for the inconvenience.

> Josefine Malmgren
> AsgarTech Corporation R&D Lead

***

Josefine did not look up from the post-installation report Hephaestus sent her after installing AesirOS on Polaris as her office door opened. Because the door was automated and controlled by a motion sensor, she assumed her cat returned from his latest round of kitty adventures. "Is that you, Zero?"

"I came as soon as I received your message, Dr. Malmgren. I too had trouble sleeping."

Josefine sprang from her chair, a small 'eep!' escaping her lips before she composed herself. "Dr. Magnin, I didn't expect you here so soon. I didn't mean to disturb you."

Her patron shook his head, and Josefine wondered at his composure so late at night. Despite it being past four in the morning, Magnin was fully dressed in his usual white double-breasted suit with a royal blue cravat. Not a single strand of his platinum hair was out of place, and a momentary urge to catch him by the collar and pull him down to meet her lips seized Josefine.

Instead of offering, Magnin smiled upon her. "You did nothing of the sort. I recently returned home from one of the interminable social gatherings the richest among us hold because most of us aren't content to donate to charity and be satisfied with the knowledge of having done good with our wealth. I was tempted to invite you, but did not want to inflict boredom on you."

_Dammit, Isaac, I would have said yes._ Josefine kept this to herself, flushing from the effort of composing a suitable lie to hide her disappointment. "Thanks for considering me. I don't have anything suitable to wear, anyway."

"I suspect you'd prove better company in your usual jeans and cardigan than most courtesans, but we should return to business. Are you reading the post-installation report for AesirOS on Polaris?"

Josefine nodded. "Looks like the prototype is safe to activate. All the tests return green." Rising from her seat, she stepped aside to give Magnin room. "See for yourself, if you like."

"I think I can trust your judgment, Dr. Malmgren."

Not trusting herself to speak with Magnin so close, Josefine nodded. He held the door for her, and followed her to the R&D lab. Polaris still lay quiescent in the incubator as the pseudoamniotic fluid drained around him. The cable connecting him to the machinery that contained and cradled him pulsed with a biological regularity previously hidden from Josefine. "Did I already start the activation process by mistake?"

"Not at all." Magnin sounded apologetic. "I transmitted the command to Hephaestus as soon as I received your message. I should have mentioned it sooner."

"It's all right." Josefine studied Polaris as the incubator refilled with clear water. His hair reminded Josefine of optical fiber; the fine strands refracted the light in every possible color. His chiseled features and gently defined musculature held her gaze, and she began to consider him in terms other than the purely aesthetic. _Stop it. This isn't the world's most advanced sex toy._ Hot on the heels of her self-admonition came a voice of scientific curiosity which resembled Claire's. _What's so bad about giving the first android his first kiss? It's for science, isn't it? How can we say that Project Aesir emulates humanity if we don't test the emulation in every possible situation?_

"Such testing might be premature."

Josefine felt her entire body flush as the realization that she was muttering struck her. "I'm sorry, Dr. Magnin. I thought I kept that to myself. I must be exhausted."

"You do seem tired. I trust you'll take time off soon."

Josefine shook her head, terrified by the notion of time off. _What the hell would I do if I'm not here at work?_ "There's still so much to do."

The incubator drained again, and the umbilical cord detached from Polaris' belly, leaving a wound that immediately healed, leaving perfect skin behind instead of a navel. The seal holding the incubator closed broke with a slow hiss, and the lid opened. "We'll discuss this later."

Polaris opened iridescent eyes with the slit pupils of a person with CPMD, and grasped the edges of the incubator as he sat up. Lifting himself up, he swung his legs over the side and lowered his feet to the floor. He tested his balance, still grasping the incubator with one hand for a moment. He let it go, took a step, and stumbled. Josefine rushed to him, but he steadied himself without her help and took several more steps. He jumped, forcing Josefine to avert her eyes lest they focus on his genitals, which freely bounced. "Dr. Magnin, do we have any clothes for Polaris?"

"Is that the name you chose for me? Polaris?" The prototype studied Josefine as Magnin left the room. "Do you want me to wear clothes because you do, Josefine Malmgren?"

Josefine sighed, and shook her head. _Nothing like this happened in the tests. Maybe I was wrong to activate so soon, just to meet the milestone. And maybe it was a mistake to give Polaris an adult body._ "Polaris, please don't do that when other people are around, unless they're doing it too."

Polaris stopped, and began to study his hands. He turned them over, flexing the fingers. "Why not?"

"I haven't answered your first question yet, yet alone your second. We called you Polaris because we named the other machines used to test AesirOS after stars. If you prefer another name, we'd be happy to use it. As for the clothes, it's customary for humans to clothe themselves when interacting with others, even when they'd be more comfortable naked. This also ties into your third question. Your knowledge base should provide information about human customs and taboos."

"I am not human, Josefine Malmgren. Why should I concern myself with human customs and taboos?" Polaris advanced upon Josefine, lifting her chin with his fingertips. "I recognize your name from the comments in my operating system's source code. Are you my mother?"

"N-Not in the biological sense. No." Josefine retreated, finally sympathizing with the protagonist of the novel she treated as a warning to those who created artificial intelligence and abandoned their creations in terror. _Where did I miscalculate? Did we give Polaris an adult's intellect, but leave him with a child's emotions and understanding of ethics?_ "I am one of your creators."

"But you're afraid of me."

"Which is your fault, Polaris." Magnin returned to the lab, bearing a bundle of clothes which he thrust into Polaris' hands. Before the door closed, Josefine caught a glimpse of a slim brunette in a blue coat holding a violin case. "Put these on."

"Why should I, Isaac Magnin?"

"Because it is customary to do so."

"Josefine Malmgren already offered this explanation. Offer a better one." 

As soon as the words escaped Polaris' lips, Magnin knocked him to the floor with a punch he threw too fast for Josefine to track. She stared, horrified at an act of violence she believed Magnin's character forbade him. "Dr. Magnin! Why did you strike Polaris? He doesn't understand the reasons for our customs."

Magnin nodded, and offered Polaris his hand. "You understand too well, Polaris, that human customs are rarely justified by reason. Now you know what is likely to happen should you violate them. Irrational customs tend to be enforced by irrational means."

Polaris accepted Magnin's hand, and stood. "I apologize, Isaac Magnin. I was..." He trailed off, as if searching for the proper conclusion. "Rude."

"Apology accepted, but you are still being rude. You are using our names, but we have not introduced ourselves to you. How do you know them?"

"I got your names based on your IP addresses. Must I wait for people to introduce themselves? Did you not do the same to me?"

"I'm sorry." Josefine answered, instead of waiting for Magnin. "I did use our name for you instead of asking you. I assumed you would not have a name for yourself, since we just activated you."

"But I am the 200 Series Asura Emulator prototype, unit zero, according to information encoded in my hardware. I know I am not human." Polaris began to pace, the clothing Isaac Magnin gave him scattered across the floor, discarded and forgotten. When his eyes met Josefine's, they accused her. "For what purpose do I exist, and how long will I do so? Will I be made obsolete, and cast aside? Why should I build a life if my memories will only prove so many tears in the rain?"

"You are newly activated, and lack context." Magnin's voice held a tone of command unfamiliar to Josefine. "I had my reasons for creating you, which I shall explain in due course. For now, content yourself with the certainty that you have a purpose, and need not fear injustice or cruelty at my hands or those of any employee of the AsgarTech Corporation. You are the result of years of effort, and possess near-incalculable value. You will not be cast aside. Now put aside the melodrama and dress yourself."

Polaris nodded, and obeyed. The clothes obscured his musculature, softening him and lending him a touch of humanity he lacked while nude. Before Josefine could compliment him, Magnin spoke again. "Excellent. Now you won't frighten or distract my employees when they come in later."

Polaris examined his T-shirt. "Why give me a Crowley's Thoth T-shirt? I have no idea if I like the band or not."

Josefine giggled, and cut it short. _I must be exhausted if I find that amusing._ "Maybe you should check out some of their albums. I don't know if Dr. Magnin thought of it, but you may find some employees talking with you as a result of the shirt. They might think you're a fellow fan." She barely managed to cover her mouth as a sudden yawn escaped her. "I'm sorry. I haven't gotten much sleep lately."

Magnin's hand was warm as it grasped her shoulder, and Josefine fought off an impulse to relax against him, and rest her head against his chest and fold his arms around her. "Perhaps you should go home now, Dr. Malmgren. You've done excellent work, and deserve to rest."

"I'm fine." Another yawn made a liar of her, and despite knowing better she relaxed against Magnin, drawing his arms around her. Her eyes slipped closed and consciousness began fading as Magnin gently lowered her to the floor. 

As she drifted into sleep, Josefine imagined Magnin addressing another woman in a sharper tone than he used with her. "Annelise, please wait outside. I will soon rejoin you."

