# Starbreaker Design Works: Blackened Phoenix

**by Matthew Graybosch**  
*with assistance from Catherine Gatt*

## Main Plotline

Morgan and Naomi seek evidence against Imaginos, who appears to be using the Phoenix Society for his own nefarious ends, while fending off new threats from anti-Phoenix Society extremists, the new prototype asura emulator Polaris, and the newly unbound and vengeful demon Sabaoth.

## Cast

 * Morgan Stormrider
 * Naomi Bradleigh
 * Edmund Cohen
 * Sid Schneider
 * Claire Ashecroft
 * Josefine Malmgren
 * Sarah Kohlrynn
 * Christabel Crowley (deceased?)
 * Annelise Copeland
 * Astarte
 * Mordred
 * Saul Rosenbaum
 * Iris Deschat
 * Munakata Tetsuo
 * Alexander Liebenthal
 * Nakajima Chihiro
 * Victoria Murdoch
 * Gregory Windsor
 * Malkuth
 * Kether
 * Gevurah
 * Binah
 * Tiphareth
 * Imaginos (Isaac Magnin)
 * Thagiron (Tamara Gellion/Theresa Garibaldi)
 * Ashtoreth (Elisabeth Bathory)
 * Adramelech (Abram Mellech)
 * Sathariel (Samuel Terell)
 * Sabaoth (pretends to be Yahweh/Jehovah/Allah)
 * Polaris
 * Vincent Rubicante
 * Catherine Gatto

## Dramatis Personae

Unlike the **Cast** section, this part arranged characters by faction/organization and may thus contain redundancies.

 * Annelise Copeland - a rising New York fashion designer with her own boutique on Fifth Avenue. Bears a striking resemblance to the late Christabel Crowley.
 * Sabaoth - an ensof imprisoned beneath the Antarctic ice cap by Imaginos. Frequently appears before humans, claims to be God, and demands humans obey his commands to commit genocide.
 * Mordred - A rakshasa who has adopted Morgan Stormrider and his companions. Can understand English, but his spoken vocabulary is limited to "meow".

### Crowley's Thoth

Crowley's Thoth is a neo-Romantic heavy metal band active between 2101 and 2112. Noted for albums like *Prometheus Unbound*, *Glass Earth Falling*, and *Water Margin*; Crowley's Thoth is also famous for its three-hour live shows, which often start without the band's founder and violinist.

* Christabel Crowley (deceased?) - Violin/Viola
* Morgan Stormrider - Guitar/Vocals
* Naomi Bradleigh - Lead Vocals/Keyboards

### The Phoenix Society

The Phoenix Society is nominally a non-government organization dedicated to the defense of free and open societies through active policing of corruption and abuses of power in government, commerce, and religion. The truth is a bit more sinister.

#### Adversaries

Adversaries are agents of the Phoenix Society's enforcement arm, the **Individual Rights Defense** corps. If Solid Snake from *Metal Gear Solid* worked for the ACLU, he'd be similar to what Adversaries are supposed to be.

 * Morgan Stormrider
 * Naomi Bradleigh
 * Edmund Cohen
 * Sid Schneider
 * Sarah Kohlrynn
 * Catherine Gatto

#### IRD Corps Directors

Though the AIs comprising the Sephiroth do most of the work of assigning work to Adversaries, human officers have the final say.

* Saul Rosenbaum
* Iris Deschat
* Karen Del Rio

#### The Sephiroth

Ten artificial intelligences govern most aspects of the Phoenix Society's operations, but serve other purposes and have their own agenda.

* Malkuth
* Kether
* Gevurah
* Binah
* Tiphareth

#### The Executive Council

The identity of the Phoenix Council's executive council is a carefull-guarded secret to keep the Society's enemies from striking at the Society's heart -- and to preserve the secret of the Society's true nature and purpose.

* Isaac Magnin
* Charles Desdinova
* Edmund Cohen
* Tamara Gellion
* Elisabeth Bathory
* Samuel Terell
* Abram Mellech

### Port Royal

A distributed digital republic composed primarily of techies, hackers, crackers, gamers, and the occasional trolls. Port Royal is a frequent source of anti-Phoenix Society sentiment, and occasional attacks against the Society's technical infrastructure.

* Claire Ashecroft - a gray-hat hacker who frequently works with Morgan Stormrider and his crew

### The AsgarTech Corporation

The Asgard Technological Development Corporation (AsgarTech) is one with a long and unsavory history spanning four incarnations. The most recent under Isaac Magnin is responsible for the Asura Emulator Project and primarily funded by the Phoenix Society &mdash; but this is a closely guarded secret.

* Isaac Magnin, CEO
* Dr. Josefine Malmgren, ÆsirOS developer/Project Æsir (Asura Emulator Project)
* Polaris, Project Æsir prototype

### Ohrmazd Medical group

Ostensibly a benevolent organization specializing in providing bleeding-edge medical care to the masses, Ohrmazd Medical is also involved in the Asura Emulator Project.

* Dr. Charles Desdinova, Founder

### Disciples of the Watch

A cabal of ensof sworn to guard the Starbreaker, the Disciples are likewise sworn to destroy any ensof who threatens sapient species under their protection, like devas/asuras and humans.

* Thagirion
* Ashtoreth
* Sathariel
* Adramelech (cast out for treachery)
* Imaginos

### Nakajima Armaments of Kyoto

A manufacturer of arms and armor trusted by Adversaries since its foundation in 2071 by Nakajima Kaoru, it continues to supply Adversaries under the supervision of Kaoru's daughter Chihiro, who took over the company in 2104.

* Nakajima Kaoru (deceased), - Founder
* Nakajima Chihiro, President
* Ms. Yamagishi, administrative assistant to Nakajima Chihiro
* Masamune, Design and Production AI for Nakajima Armaments

### Murdoch Defense Industries

A manufacturer of cheap equipment propped up by the Phoenix Society, Murdoch also makes additional profits by selling black-market arms &mdash; with the Society's tacit approval.

* Victoria Murdoch, CEO
* Don Kissel, factory foreman

### MEPOL

Though Adversaries have had to clamp down on abuses committed by officers of the Metropolitan Police of London, the modern MEPOL is an efficient, professional police force.

* Chief Inspector Gregory Windsor

### Blackened Phoenix

This opposition group sprang into prominence in the wake of the Phoenix Society's counterrevolutionary operations in Boston. Initially demanding "justice for Liebenthal" through social media activity and non-violent demonstrations outside Society installations, Blackened Phoenix has recently resorted to attacking the Society's Technological infrastructure.

* Munakata Tetsuo
* Alexander Liebenthal
* Vincent Rubicante
* Sarah Kohlrynn

### Household AIs

Digital butlers/secretaries serving private individuals are slowly becoming more common as costs decrease, but remain expensive because AIs are considered persons under the law, and are therefore protected by the Universal Declaration of Individual Rights.

* Astarte (works with Morgan)
* Hal (works with Claire)
* Wolfgang (works with Naomi)
* Aleister (worked with Christabel)
* Savannah (works with Eddie)
* Howl (works with Josefine)

## Chapter 1: When You Don't See Me

The following characters are on-stage:

* Morgan Stormrider
* Naomi Bradleigh
* Mordred
* Gregory Windsor

The following characters are off-stage, but mentioned:

* Christabel Crowley
* Annelise Copeland

### Primary Conflict

Morgan Stormrider wants the evidence MEPOL gathered during their investigation into Christabel Crowley's murder. Gregory Windsor isn't convinced that's a good idea, given what he's learned about Christabel.

### Secondary Conflicts
