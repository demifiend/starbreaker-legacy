	Title:			The Milgram Battery
	Author:		Matthew Graybosch
	Date:		28 February 2012
	Comment:	A short story set in the Starbreaker universe

	# __The Milgram Battery__ #
	A short story by Matthew Graybosch



	Morgan studied the experimenter, ignoring the hand he offered as a polite gesture. His muddy eyes were those of the technician who helped him into the simulation creche and hooked him up. His leathery hands had injected him with a drug which even now fought to blunt his awareness. His lab coat had a Phoenix Society patch on the shoulder. _This is the test. They want to gauge my reactions. The drug must be designed to lower my inhibitions and prevent me from thinking about my responses._

	The experimenter lowered his right hand with a huff, and consulted his tablet. “Morgan Stormrider? An odd name. What were your parents thinking?”

	“They had no say in the matter. I grew up in foster care. My name is my own.”

	“No wonder you seem rather unsociable. Research indicates children who grow up without a stable home environment —”

	“When did my childhood become your concern?”

	“It isn’t. I was simply making an observation.”

	“Keep them to yourself, and tell me why I’m here.”

	“You’ve been selected to help me with an experiment.” He led Morgan into another room as antiseptic white as the one in which they had started. Plate glass partitioned the room. On Morgan’s side waited a machine similar to an electronic keyboard; each key would play a voltage higher than the last, in steps of fifteen volts, instead of a different tone. On the other side sat a person connected to heart-monitoring equipment. Lines connected him to the keyboard on Morgan’s side. The person on the other side mopped his forehead with a shirtsleeve, his lips moving as he pored over a list. “The volunteer on the other side is our subject in an experiment concerning learning and negative reinforcement.”

	“I think I know how this goes.” Morgan gestured towards the keyboard. “That poor schmuck is supposed to memorize a series of word pairs. I’m supposed to test him, and give him a shock every time he makes a mistake.”

	“Exactly. You are to start with the lowest voltage, and work your way up to the maximum, which is four hundred and fifty volts. We use a low amperage current which may prove painful, but not dangerous.”

	“Unless your subject suffers from a heart condition.”

	The experimenter consulted his tablet again. “Oh, dear. His medical history  a heart murmur. Of course, he can stop the experiment at any time just by asking.”

	Morgan about-faced, turning his back on the experimental apparatus and the victim behind the plate glass. “Or, I can end this farce before it begins by refusing to participate. You want to determine whether I will obey orders to torture.”

	“It is not torture." The experimenter handed Morgan a stack of forms. "The subject gave informed consent. If you wish, I can hook you up to the keyboard and let you feel the maximum voltage for yourself. There is no real danger.”

	He glared over his shoulder at the experimenter. “You need not trouble yourself.”

	“I must insist upon your _participation_.” 

	Morgan smiled. While the prod wasn’t classic Milgram, he already deviated far enough from the scenario to force the simulation to adapt to him. “I refuse.”

	“The experiment requires your _participation._”

	“Of course it does.” Morgan advanced upon the experimenter. “I am the subject, after all.”

	“It is absolutely essential that you _participate_.”

	He grasped the collar of the experimenter’s shirt, and lifted him off his feet. “I know.”

	“You have no other choice. You must _participate._”

	“I have one other choice.” Cracks radiated from the point at which the experimenter’s body impacted the plate glass and broke through. He climbed through the hole he created by throwing the experimenter through the window, and lifted the cowering scientist to his feet. “_Non serviam,_ torturer.”

	As he drew back his fist, the experimenter shattered into pixels, each fading to black. The room itself faded to void.

	###

	Karen Del Rio shook her head as the AI interpreting Morgan Stormrider’s simulator-induced dream shut down the scenario, while letting him rest inside the dream sequencer. “Do we even have a classification for somebody who refuses to even participate in the experiment? Or do we just write him off as a failure?”

	“It would be a shame to write him off.” One of Del Rio’s co-Directors, Iris Deschat consulted her handheld and pulled Morgan’s dossier. “His academic record is impeccable, and his psychological evaluation indicates a genuine belief in the Society’s ideals and mission.”

	The most senior of the three directors of the Phoenix Society’s New York chapter considered the candidate’s records himself. Saul had kept a careful watch on Stormrider at the behest of his old friend, Edmund Cohen, to the point of acting as Morgan’s patron. To let the Adversary candidate wash out now would reflect poorly on him, but so would too vehement a defense. “He doesn’t have a record of insubordination, Karen.”

	“Saul, you trust him too much. Morgan isn’t even a M-one based on what we’ve seen so far, and we’re not supposed to swear in anybody who isn’t classified between M-three and M-seven by the Milgram Battery.”

	Iris shook her head as she checked her handheld. “Naomi Bradleigh was classified as M-two.”

	“Naomi Bradleigh was a freak, and Isaac Magnin wanted to fuck her.”

	“Excuse me.” The directors turned to find a frost-haired man in a white double-breasted suit standing in the doorway to the observation room they shared. The door snicked shut behind him as he found a chair for himself and crossed his legs. “It can be so troublesome to enter a room in the middle of a heated conversation. Without context, it is too easy to misunderstand one another.”

	Karen blinked, and collected herself with a deep breath. “Dr. Magnin, I meant to remind Ms. Deschat that Adversary Bradleigh’s results after undergoing the Milgram Battery were anomalous, in that the psychotropic agent we use to induce and direct the candidate’s dreams was ineffective at the usual dose.”

	“How did Stormrider react to the drug?”

	Saul shook his head. “I don't think it works on him. He seems lucid, and refused to even participate in the classic scenario at the heart of the first trial.”

	“How did he react when Malkuth adapted the standard prods?”

	Iris moved the video’s stop point for Magnin. “The battery footage will show that he resorted to violence after the final prompt.”

	“This is a rare find.” Magnin’s eyes gleamed with interest as watched the video. “He pierced the simulation almost immediately, and gave the experimenter no chance to persuade him using any of the usual sophistries with which one might justify the use of torture.”

	“We can’t give him an Adversary’s pins. He’s a M-zero.”

	Magnin gave his head a gentle shake. “May I remind you, Ms. Del Rio, that you are not qualified to make such evaluations?”

	“Do we continue, Dr. Magnin?”

	“Yes. And, Mr. Rosenbaum? Instruct the technicians to double the dosage for the next stage of the Battery.”

	###

	Morgan found himself standing at attention, his right arm outstretched in salute. The gate creaked shut behind the SS officer, who glared through Morgan as if he were not there. Low-ranking stormtroopers flanked the officer; the blackened steel of their submachine-guns gleamed a dull counterpoint to the silver glints in their superior’s uniform. Their movements were not even robotic, but reminiscent of somebody’s initial efforts at computer animation. Nor were their faces human; their eyes were a flat blue, and lacked the striations normally visible in the human iris. Their noses were mere suggestions, and they could not speak for lack of mouths.

	The officer, however, was not only human, but bore a face Morgan recognized from an old film he viewed at a WWII movie festival with several acquaintances from ACS last week. A gust of wind lifted the cap from his head, and before he could clamp it back down Morgan caught a glimpse of sandy hair, and a swastika scar on his forehead. _As if the flunkies weren’t a dead giveaway that this is also a sim._

	If Morgan gave any sign of recognition, the officer did not acknowledge it. He considered the paper uniforms in the background, digging holes only to fill them in again under the sights of machine guns in towers. “More workers will arrive at this camp this weekend, Commandant. You will have to make room for them.”

	“How do you suggest I do that, Colonel? The Fuhrer has ordered that not a single round of ammunition be wasted.”

	The officer nodded. “You are correct. Fortunately the Fuhrer has provided a more efficient means of implementing the final solution. May I assume you received your shipment of the new gas, Zyklon-B?”

	Morgan took a deep breath, and considered the stormtroopers’ weapons. He did not put it past the AI running the simulation to cheat, and ensure his death should he resist. _This is the test. Will I obey and live, or die rather than give the order to gas prisoners to death?_ “If you want to kill these prisoners, you will have to do so yourself.”

	“You are the commandant of this camp. The Fuhrer insists upon your _obedience_.”

	“Tell the Fuhrer he’s as mediocre an orator as he was a painter.” Morgan smiled as the words passed his lips; he could imagine the AI processing Morgan’s words in a desperate effort to adapt and keep the simulation running according to the script.

	The SS officer sputtered for a moment before finding his voice. “The Third Reich requires your _obedience_.”

	“The Third Reich is fucked, and you damn well know it.”

	“I don’t think you understand the gravity of your situation, Commandant.” The officer ground out the words, his lips a rictus as the stormtroopers stepped forward and trained their weapons on Morgan. “You have no other choice if you value your life. You must _obey_.”

	“What makes you think I value my life?” Morgan reached into his greatcoat and drew a Luger from a shoulder harness underneath. He chambered a round, and aimed for the officer’s head. “Life as a Nazi seems its own punishment.”

	“You have no other choice. You must _obey._” The stormtroopers strained against an invisible leash, their fingers squeezing triggers which refused to yield to the pressure placed on them. Morgan shot them first, their bodies dissolving like generic enemies in a video game as he put a 9mm round through the SS officer’s eye. As the officer scrabbled at his coat to get his own pistol, Morgan pressed the muzzle of his Luger under his chin, and raised his middle finger in a final salute. The void consumed him before he pulled the trigger.

	###

	“Quadruple the current dosage.” Isaac Magnin had no need to raise his voice to deliver his order to the technician attending Morgan as he lay quiescent in the dream sequencer’s creche. He doubted anybody here had the backbone to raise an objection. He was, after all, part of the Phoenix Society’s executive council.

	Iris Deschat proved him wrong. “Dr. Magnin, are you sure it’s wise to give Stormrider eight times his original dosage?”

	“I agree with Iris.” Saul seemed to draw inspiration from the older woman, which Magnin expected. Rosenbaum did serve under Deschat before Nationfall. “Even though the standard dosage wears off quickly, you already gave him a double dose. Now you want to give him even more, when we don’t know if the last dose has worn off yet.”

	“You can trust me. I’m a physician.” Magnin smiled as he delivered the line. It was usually enough to quell objections.

	“I don’t care if you’re Phoebus fucking Apollo, god of medicine. That’s one of my men you’re using as a test subject. Ever hear of informed consent?” He turned to the technician, who just finished preparing the increased dosage. “Belay Dr. Magnin’s last order. Give Stormrider the standard dosage.”

	“Saul’s right.” Iris placed herself between Saul and Dr. Magnin as her fellow Director argued with the technician. “The protocol for administering the Milgram Battery does not call for increased dosages should the candidate somehow realize the simulation's nature and refuse to cooperate. It specifies two alternatives. We either halt the Battery, or continue until the subject encounters a situation he cannot dismiss as a mere simulation.”

	Magnin nodded, and rose from his seat. “It seems my direct involvement is unnecessary at this point. I trust you will advise me as to Stormrider’s progress.”

	“Of course.”

	“Thank you, Director.” He let Karen Del Rio back into the observation room before closing the door behind him. He found one of his fellow executive council members waiting for him upon returning to his office. Desdinova had not bothered to remove his habitual charcoal grey greatcoat, and Magnin wondered — as he often did — if the brother who distanced himself with an alias secretly reveled in the comparison a British philologist had made to his wife upon seeing them together at Oxford after the Second World War.

	He closed the door, and converted electricity drawn from a nearby tesla point to power a pattern which would prevent their conversation from escaping the room. “Stormrider keeps seeing through the Milgram Battery’s simulations, just like the other nine asura emulators.”

	Desdinova looked up from the report he had been reading on a tablet. “I noticed. It seems you’ve also been testing the asura emulators’ immunity to chemical agents.”

	“I was in fact testing Deschat and Rosenbaum. I was curious as to whether they would defy me to protect their charge. I assume you set one of them to the task of watching over Stormrider.”

	Desdinova rose, tucking his tablet under his arm. “I find your assumption amusing, considering how you used to caution me against find evidence of conspiracies in actions explicable by other means.”

	“Who did you ask to watch over him?”

	“I asked Edmund Cohen.” He opened the door, breaking the pattern Magnin created using his preternatural talents. “It seems the man finally learned to delegate. Or perhaps the Directors saw promise in this young man on their own.”

	“They did seem impressed with his abilities. Should I assume you share Deschat and Rosenbaum’s opinions?”

	“I think Stormrider bears watching.”

	_That he does._ Magnin thought once his brother left him alone in the office. _He just might have the strength of ego I require of a soldier entrusted with the Starbreaker._ He picked up the phone and dialed the observation room. “End the battery. Classify Stormrider as M-zero.”

	###

	_What will it be this time?_ Morgan lost count of the scenarios the dream sequencer presented him long ago, along with his grip on time. He had been a prisoner of war, offered freedom and a new home if only he would betray his fellows. He had been a university student, egged on by so-called friends to take advantage of a drunken young woman. He had been the president of a dead nation, under pressure to sign into law a bill mandating that all citizens be given the Patch to enhance social cohesion. He even stepped into Abraham's sandals, and covered his ears as the voice of God demanded the sacrifice of his only son Isaac.

	He opened his eyes, and blinked as the technician opened the simulator's creche to let him out. The empty pistol magazine, which he took with him as a reminder that he was awake in the real world again, bit into the palm of his hand. He slipped it into his pocket once he found his feet. He blinked at the Directors of the New York Chapter of the Phoenix Society, who supervised the Battery, led him to a small conference room. “Did I pass?”

	Del Rio glared at him, her voice an annoyed snarl. “You didn’t even fail. You are _not_ supposed to reject the simulation itself. If you do, how can we test your reactions when faced with immoral orders, or pressure from your friends or your position?”

	_Working with her will prove interesting. Eddie Cohen was right. This woman_ is _a martinet._ He cleared his head, and recalled the first simulation. “Director Del Rio, please consider the first simulation, based on the classic Yale experiment. The entire premise of the fictional experiment requires I hurt somebody for making a mistake in memorizing word pairs. It seemed unethical to participate at all, rather than go along until the actor on the other side of the glass began to protest.”

	“That’s a valid point, Karen.” Deschat nodded to him. “Am I correct in assuming thought you all of the situations immoral?”

	“At the very least.”

	It was not until Rosenbaum offered him a cup of coffee and a plate of steak and eggs that Morgan remembered his hunger. The instructions for the Battery required that he fast for twenty-four hours prior. Rosenbaum watched him eat, and waited until he was finished before asking his question. “Did you experience something troubling in the simulations?”

	Del Rio cleared his throat. “We’re not here to give him therapy.”

	“I want to hear his answer.” Deschat paused, as if considering his words. “I myself found the situation involving the drunk woman problematic.”

	Morgan nodded, glad that he was not alone in his disquiet. “I recognized the woman. She plays the piano at the jazz bar where I work at night." He used the technicians' term for the machinery used to administer the Battery. "I don’t think the dream sequencer just induces dreams. I think it dredges my memories for imagery to use against me.”

	“That insight alone is reason enough to give Stormrider his commission.” The door opened to admit a pale man whose white double-breasted suit and blue cravat matched his hair and eyes. He held a sheathed smallsword in his hands, along with a small jewelry box. “Adversary Stormrider, how did you realize we mined your memories during the Milgram Battery?”

	“One of the simulations involved friends encouraging him to take advantage of a drunk woman, Dr. Magnin.” Rosenbaum explained before Morgan found the words. “He recognized the woman.”

	Magnin nodded, and placed the sword and box on the table. “In that case, Adversary Stormrider, I owe you an apology. The simulator _is_ in fact programmed to look for ways to amplify the stakes and introduce temptation into what might otherwise be a clear choice between right and wrong.”

	“You do this to everybody?”

	Magnin nodded. “Yes. Yielding to that temptation, of course, is an automatic failure regardless of your overall score.”

	“Which is M-zero, by the way.” Del Rio ground out the words. “It’s obvious you have no discipline .”

	Magnin glared at her. “Remember your place while you still have one.”

	“No. Let her have her say. I will be taking orders from Ms. Del Rio, along with Ms. Deschat and Mr. Rosenbaum. If any of them have reservations concerning me, I want to hear them.”

	The others looked to Del Rio, the only dissenting voice. “You saw how he performed during the Battery. He is not only insubordinate, but he _attacks_ authority figures.”

	Saul’s tone was dry. “You realize that’s what Adversaries are supposed to do, right?”

	“What if he attacks one of _us_?”

	“Are you planning to give him cause to do so?” Deschat considered Morgan for a moment, her eyes lingering on him until he wondered if he was going to blush beneath her gaze. She was, after all, an attractive woman despite her seniority. “I think you’re mistaking obedience for discipline.”

	“I think so as well.” Saul pushed the sword and the jeweler’s box towards Morgan. “I’m willing to trust this man’s self-discipline.”

	“Thank you.” Morgan opened the box, and found a set of well-polished sword and balance pins. They were an old design, bulkier than the current generation, and less abstract. These actually had the rattlesnake coiled around the sword’s blade, holding the balance in its jaws. He took his time in attaching them to his ballistic jacket’s lapels before taking up the sword. It was a smallsword, shorter and slimmer than a rapier, and good only for thrusting. However, the base of the blade was just wide enough for a word to be etched on each of the blade’s three sides: ‘Liberty’, ‘Justice’, and ‘Equality’. He drew the blade fully, and saluted.

	Magnin nodded. “We would hear your oath, Adversary Stormrider. I trust you know the words.”

	Morgan knew the words. They were etched into his memory as indelibly as the Phoenix Society’s three primary ideals on the blade of his dress sword: “I swear eternal hostility towards every form of tyranny over the human mind.”
