#Interview with Rebecca Knight

*Matthew Graybosch*

##How long have you considered yourself a writer?

I started in 1996. Given how we're close to the end of 2013, that's eighteen years and almost half my life. I started in my senior year of high school, when I got stuck with an "elective" creative writing course taught by an instructor who insisted that the only fiction worth writing or reading came from the modernist tradition exemplified by early twentieth-century novelists like Ernest Hemingway, F. Scott Fitzgerald, and Norman Mailer -- the latter of which being the best of the lot, in my own opinion.

She spared little patience with students foolish enough to espouse any affection for "genre" work, and none whatsoever for long-haired metalheads possessed of sufficient nerve to suggest her literary idols were *also* genre writers. It didn't help that I turned in stories about long-haired metalheads aided by gifts from the Lead Bodhisattva, who put the hosts of Heaven and Hell alike to the sword because God and the Devil had the temerity to screw with his weekend plans by starting Armageddon.

The Lead Bodhisattva's gift, by the way, was a black diamond sword called the Starbreaker. "Stormbringer" was already taken -- which surely caused the members of Deep Purple no little trouble when they released an album with that title in the 1970s.

##What inspires and motivates you to write?

At risk of sounding both morbid and melodramatic, I needed a reason to go on living. When I got out of high school, I had no idea what I was going to do with my life. I didn't have a dream, since I realized that I would never be a good enough musician to command stages, or even earn a living. No particular career appealed to me. Since I was neither public-spirited, nor religious, I couldn't have done what other aimless young men did: enter religious life or seek a military career.

I started college because I was supposed to; going to college is what reasonably intelligent white teenagers do in America once they've been released from high school. It's a prerequisite to a career, marriage, children, and all the rest of the nonsense we call the American Dream.

However, I lacked a meaningful alternative until I suffered the serendipitous misfortune of reading Terry Goodkind's first novel, *Wizard's First Rule*. If you haven't read it, here's the deal: an Eagle Scout picks a lousy time to finally notice the existence of women, gets infatuated with the wrong woman, gets drawn into a quest to kill a dictator whose right-hand man is a pedophile, and has one of those awkward "Luke, I am your father." moments with said dictator -- whose name is "Darken Rahl".

And what does Darken Rahl want? He wants to rule the world, not that Terry Goodkind bothers to tell us why, and if he can't rule the world he'll damn well destroy it -- and himself in the bargain. Add in quasi-Satanic child sacrifice, lots of torture porn, and the sort of plot twist M. Night Shyamalan might have rejected as too obvious, and you have the first volume of *The Sword of Truth* -- a series for *Wheel of Time* fans to enjoy while waiting for Robert Jordan to get off his ass and move *his* byzantine plot along.

So, after reading some of that garbage -- and paying good money for the privilege, since I was a working-class college kid cleaning toilets at a supermarket to pay for textbooks and train fare -- I was thoroughly fed up with the fantasy genre. Sure, Michael Moorcock and Roger Zelazny shook things up in the 1970s and 1980s, but it wasn't fair to expect Moorcock to keep cranking out Eternal Champion stories when he had already published *The Revenge of the Rose* (an unsung classic, in my estimation), and Zelazny was dead of cancer. So it goes.

In the meantime, C. J. Cherryh was done with her Morgaine science fantasy series. C. S. Friedman had finished her utterly brilliant *Coldfire Trilogy*. Steven Brust still doesn't get enough respect. George R. R. Martin's *A Song of Ice and Fire* wasn't the juggernaut it is today, neither Scott Lynch nor Joe Abercrombie were publishing yet, and I had not yet read Matthew Stover's brilliantly profane *Acts of Caine*. Brandon Sanderson *might* have been working on *Elantris* at the time (which I still haven't gotten around to reading). Nor had I yet discovered Glen Cook, the Vietnam vet who penned underrated classics like *The Black Company* and the Dread Empire sequence while making cars for GM, and I wouldn't mind buying that guy a beer.

I was pissed off with the fantasy genre as I understood it, particularly with the genre's tolerance for lousy villains -- a tolerance for which blame should be laid squarely on the grave of J. R. R. Tolkien. Sauron was a lousy antagonist, from a literary standpoint. All we know about his motiviation is what his enemies tell their dupes, by which I mean what Gandalf tells Frodo to con him into taking the One Ring and buggering off to Rivendell. And what are we told about Sauron? That he wants to rule the world.

You know what a band called Tears for Fears taught me in the 80s? *Everybody* wants to rule the world. Tolkien, however, never bothered to tell us *why* Sauron wanted to rule the world. Did he want his pick of Elvish virgins? Was it payback for how the Valar bumped off his boss Morgoth back in the First Age? Did he just want to spend eternity punching kittens without any interference? 

We don't know. Tolkien, perhaps because of a belief in evil as diminishment that goes all the way back to Dante Alighieri (if not further), doesn't tell us. So, after drop-kicking my copy of *Wizard's First Rule* across the room, I said to my cat, "You know what, furball? This genre needs a steel-toed boot up the ass. If the best we can do is rehashes of the Campbellian monomyth with some demon-ridden idiot of a farm boy marching off to adventure with his daddy's katana bouncing off his hip, and villains with less characterization than the end boss of a bad JRPG, then we deserve all the contempt we get."

My cat's response was to start licking his nuts. Not exactly supportive.

##Tell me more about your brand of steel-toed fantasy.

I'm surprised you care at this point, and I'd be shocked if your readers got this far, but here goes. The following principles provide the foundation of my brand of heavy metal science fantasy. I'll admit that I didn't come up with these principles right away. It took years of further reading in the fantasy and science fiction genres, as well as outside the genre, to develop some of them.

 * There is no such thing as the supernatural.
 * Everything must make sense.
 * Everybody has a reason for the things they do.
 * The characters should be competent adults.
 * If a sentence sucks, the whole book sucks.

I hope you and your readers will indulge me as I expound on these principles.

###There is no such thing as the supernatural.

I suspect many of your readers will object to the first principle, and insist that a blanket rejection of the supernatural precludes magic. I disagree. I distinguish between the *supernatural* and the *preternatural*. Supernatural phenomena are beyond the scope of human understanding, and always will be. Preternatural phenomena are *currently* unexplained, but may yet be explicable through diligent scientific research.

If you want examples from film, consider *The Exorcist* and *Ghostbusters*. In the former, no remedy or stratagem known to human science can banish Pazuzu from its human host, Regan. Regan's terrorized parents resort to the Catholic Church's rituals of exorcism. Demonic possession is supernatural in *The Exorcist*, and there is nothing humans can do about it on their own. They have to turn to a higher power.

In *Ghostbusters*, the ghosts are preternatural, rather than supernatural. Most people dismiss them as fakery, and the paranormal researchers portrayed by Bill Murray and Harold Ramis as charlatans, but they figure out how to trap ghosts and remove them from their haunts using unlicensed particle accelerators strapped to their backs. Instead of the power of Christ, they use the power of *science*, albeit Hollywood science.

###Everything must make sense.

I don't care much for Tolkien's fiction, for reasons beyond the scope of this interview, but the man not only grasped the concept of worldbuilding, but grasped the importance of keeping most of his worldbuilding in the background. I suspect his only living equal as a worldbuilder is Ursula K. Le Guin, who draws from anthropology as Tolkien did from philology, which predates modern linguistics.

Being a self-taught software developer, rather than a linguist or an anthropologist, I may never match the stature of Tolkien and Le Guin as worldbuilders. However, I grasp the importance of logic in a narrative. If there is anything in my story that I can't justify, then I screwed up. End of story. Coincidences are not permitted. Miracles are right out. Everything that happens in my fiction must have a cause, and that cause should be explained, or at least alluded to, in the text.

###Everybody has a reason for the things they do.

Remember how I griped about villains like Sauron, Lord Foul the Despiser, and Darken Rahl? My primary objection is that they are evil *just because*. They got stuck with the Villain Ball. I'm not naive enough to think there's no such thing as evil -- not when human history is as full of examples of human depravity as I am of self-aggrandizing arrogance -- but the Holocaust happened for a reason, and the reason isn't just, "Because Hitler was a pathetic wanker with a silly mustache."

As an author, it's my responsibility to bring to characterization the *same* exacting effort every halfway competent fantasist brings to their worldbuilding. Create as elaborate a stage as you like, but the play's the thing. What's a play without the players?

Since it's the villain who drives the story, it isn't enough to pick a character and say, "OK, you've got the Villain Ball, now run with it." The first thing I did when I started putting serious effort into *Starbreaker*, the first damn thing, was to sit down with my villain Imaginos. I asked questions:

1. What does Imaginos want?
2. Why does he want what he wants?
3. What gave him his reasons for wanting what he wants?
4. What has he done to get what he wants?
5. What is he willing to do to get what he wants?

These questions are Psychology 101, but a lot of traditionally published fantasy authors (at least when I was starting out) only asked such questions of their *protagonists*, and perhaps their *minor* antagonists. They might psychoanalyze the Dark Lord's flunkies, but not the Dark Lord himself.

To suggest I am unimpressed with such an approach is an understatement of Homeric proportions.

###The characters should be competent adults.

There's a reason it took me almost twenty years to publish my first novel: it's hard to write about mature adults when you're not one yourself, and when I first conceived characters like Morgan Stormrider and Naomi Bradleigh -- experienced adults who think they understand the world and their place in it -- I was an eighteen-year-old punk who had yet to kiss a woman. I had to grow into my cast -- as if it were possible to grow up enough to properly characterize a sorceress who's over fifty thousand years old, and hides behind the persona of a Manhattan socialite devoted to horticulture and the symphony.

Morgan Stormrider isn't just some callow youth from a farming village who agrees to ride off with a sorceress he doesn't know and her bodyguard because monsters burned his house down and almost killed his father. He's a grown man, with adult problems. He's stuck with a job he's come to hate. He wants to start a career better suited to his evolving values. He's in love with a woman with whom he's been friends for years. His ex was recently murdered, and he's a possible suspect. With the possible exception of the last, these are all problems any thirty-year-old man living in a Western country might face. It's the specific nature of his problems that are extraordinary, and the efforts required to solve his problems will force him to grow, learn new skills, and reevaluate his understanding of the world around him.

###If a sentence sucks, the whole book sucks.

I know I'm likely to regret mentioning this last one, because I risk running afoul of an individual reader's tastes, but I flatter myself by thinking that I do the best I can with every word, sentence, paragraph, and scene I write. I omit needless words, and excise irrelevant details. If you see one of my characters sitting in the crapper, it won't be because everybody poops, but because something related to the plot is about to happen while this character's sitting with his pants around his ankles.

In _Without Bloodshed_, I have a scene where Morgan and Naomi fret about contraception not to hammer home the importance of safe sex, but to show the implications of the genetic condition they share, congenital pseudofeline morphological disorder (CPMD). When somebody with CPMD has sex with a normal person, there's no risk of pregnancy. When two people with CPMD get together, pregnancy is a possibility. This is important to the story, for reasons I'm reluctant to explain here.

##What inspired the name of your series "Starbreaker"?

I stole the name from a song by Judas Priest, from their *Sin After Sin* album. It sounded suitably badass for a weapon capable of killing demons from outer space -- instead of just destroying their avatars.

Like I said, "Stormbringer" was taken. I'm already too close to writing "Elric on a Harley" for comfort.

##What's your typical writing session and environment like?

I'm a lunch break novelist. I bring a laptop to work with me, and drive down the street to a nearby pizza parlor to eat a slice of pizza and belt out a scene. On a good day, I manage between 500 and 1500 words of raw text in about an hour. 

I'd love to be able to write at home after work, but I rarely manage it. After a full day as a software developer, my brain shuts down once I come home. I'm more likely to curl up with my wife and read, play video games, or mess around on the net than I am to write.

When I'm writing, I put on headphones and try to find music that suits the scene and character I'm trying to write. My playlist usually includes music by Iron Maiden, Iced Earth, the Blue Oyster Cult, Judas Priest, Queensryche, Bruce Dickinson, Iced Earth, Nightwish, Without Temptation, Delain, Nemesea, Blind Guardian, The Worshyp, The Protomen, Savatage, Black Sabbath, Therion, Joe Satriani, Megadeth, Dream Theater, Coheed and Cambria, Ayreon, Symphony X, Nobuo Uematsu, and Shoji Meguro.

I tend to turn off wifi when writing. I don't think much of Jonathan Franzen's fiction, but I suspect he might be right about the difficulty of writing good fiction while jacked in. 

##Tell me about influences, if any:

I'd have to be especially arrogant to claim complete originality, free of any influences. However, when you ask any novelist to name their influences, you impose upon them a nigh-irresistable temptation to claim a part in the literary traditions of the authors they most admire, while omitting any mention of authors they despise.

I'd love to claim that I draw upon European Romanticism and the daring SF and fantasy for which such authors as Michael Moorcock, Roger Zelazny, Stephen Brust, Robert Heinlein, C. J. Cherryh, C. S. Friedman, and M. John Harrison are justly famous. However, I might not have proved as successful in escaping the shadows of Tolkien, Donaldson, Jordan, and Goodkind as I hoped.

An accurate account of the influences on my work is a task better left to critics, and not to novelists seeking to promote their own work.

##What are your favorite writing tools?

My laptop runs [CrunchBang Linux](http://www.crunchbang.org), and I tend to write my drafts in plain text files formatted with a markup language called Markdown, which I can convert to HTML and other formats using freely available tools like pandoc. When I'm ready to submit a piece for editing and publication, I use LibreOffice, which can cope with Microsoft Word's formats and includes "track changes" functionality. 

I also run a dict server on my laptop, which allows me to get definitions and synonyms by typing a command into a shell prompt, such as "dict bazooka". Because I write my drafts in plain text, I can use any text editor I choose, even heavy duty programmers' editors like vim and emacs. I currently favor an app called PyRoom, an open source clone of Hog Bay Software's *WriteRoom* app for Mac and iOS. It's a full-screen plain text editor, which allows me to focus on my writing without distraction.

Because I run a Unix-based OS, I can use the OS itself to organize my work. I have a documents directory, just like you Windows and Mac users. In it, there's a "starbreaker" directory. In that "starbreaker" directory I have directories for each story using the Starbreaker setting. For novels like *Without Bloodshed*, the directory consists of a file containing the title named "00.title.md" (.md for Markdown files); a "scenebreak" text file consisting of a blank line, a line with three asterisks, and another blank line; and a directory for each chapter, named so that the OS orders it for me: "01.theunforgiven", "02.norefugebutaudacity", etc. In each directory, I have another "00.title.md" for the chapter number and title. I also have a file for each scene whose name is based on the order in which the scene occurs, and the viewpoint character's name, such as "01.morganstormrider.md", "02.naomibradleigh.md", etc.

I can then put it all together using a small shell script written so I can use it to convert any story I write into a single file for conversion to standard formats. It uses the "cat" (concatenate) command. If I want word counts for scenes, chapters, or the entire story, I can use the "wc" (word count) command. If I need to do a find, I use "grep". If I need to do a story-wide find/replace affecting more than one file, then I use "sed".

I started using Linux in 1999, after my first computer (an secondhand IBM PS/ValuePoint running PC-DOS 6, if anybody cares) died by my hand. I was trying to swap out the hard drive when my cat bit my toes to get attention. This startled me, and I ruined the computer by driving my screwdriver straight through the main board. I had to build a new one, I didn't want to keep running DOS and writing, I didn't want to pay a hundred bucks for a copy of Windows 98, and I couldn't afford a Mac. So I bought a copy of Linux on CD (Red Hat 5.2, if anybody cares), installed it, and alternated between writing and tinkering.

Since my day job involves software development on Windows, I trust Microsoft's offerings as far as I can throw them. Macbooks are nice, and I used one from 2006 until 2012, but overpriced for the hardware you get inside the pretty case. And if George R. R. Martin can keep using WordStar 4 on DOS, why shouldn't I use Linux?

##What is the most unexpected reaction you have had to your writing?

My writing is how I've met every woman with whom I've been intimately involved, including my wife of nine years, who I courted for four. Catherine and I met on a Yahoo! forum for aspiring fantasy writers, and started out by reading each other's work. It's a long story, and perhaps beyond the scope of this interview.

##Do you have any regrets pertaining to your writing?

I sometimes suspect I picked the wrong trade for a day job. Software development gave me valuable technical skills, as well as experience I used while writing *Starbreaker*, but it's a bad trade for writers. The demands on one's time and intellect often leave little time or energy for writing, even when I avoided working in Silicon Valley and in start-ups in favor of taking jobs involving government contracts which should only require a forty hour workweek.

Aside from that, I have no regrets. I needed to do *something* with my life, and writing gives me a sense of purpose. It allows me to indulge all of my nasty little control-freak tendencies without actually hurting anybody.

##What plans do you have for future work?

*Without Bloodshed*, the first Starbreaker novel, comes out in about a month or so. [Curiosity Quills Press](http://www.curiosityquills.com) is currently doing a new cover. I'm working on *The Blackened Phoenix*, as well as a short piece called "Tattoo Vampire". For Christmas, I've started kicking around some ideas for a story called "Cardigans" in which we'll see a young Morgan Stormrider knitting. I'm thinking of expanding a novelette, *Steadfast*, into a full-length NA science fantasy novel starring Naomi Bradleigh called *Silent Clarion*. I also have to plot and eventually write the last two main-sequence Starbreaker books: *Proscribed Construct* and *A Tyranny of Demons*. 

After that -- let's just say that chronicling the life and crimes of Imaginos could be a lifetime's work. I could write a Michener-style epic about Nationfall, the social/political/economic collapse that sets the stage for the rise of the society in which Starbreaker is set. I can do with Starbreaker what Tolkien did with Middle-Earth, only I cheat by taking our world and screwing around with history.

##What is the best advice you want to share with aspiring authors?

This first bit should be obvious, but for the love of all the demons ever venerated by humankind, **READ**. Read your chosen genre. Get acquainted with its tropes and cliches. Figure out what readers expect, so you can screw with them if you want to. Once you're done reading within your genre, read *outside* it. You might find ideas and elements you can import into your chosen genre, and exposure to different styles and voices will help you develop a richer style of your own.

Half of what you hear about building an author platform is arrant nonsense, but I can't tell you which half. I've had people tell me Google+ is a waste of time, and that I should use Facebook and Twitter instead. I ignore them, because I've tried Twitter and Facebook. Twitter is the men's room wall of the internet. Facebook is how the Daleks will justify our extermination. Google+ is where I found *my* audience, which is currently about 18,000 followers. Maybe a tenth of them will bother to buy my book, but nobody builds an empire overnight.

Some people will tell you that fanfic is a good way to develop your technique, but I don't agree with them. I think working with an existing setting and existing characters makes it harder for writers to learn how to develop settings and characters of their own. Instead, I recommend the [pastiche](http://en.wikipedia.org/wiki/Pastiche). Instead of taking Kirk and Spock as is, and working around them, use these characters as *templates* for new characters of your own creation if you lack the confidence to start from scratch.

I'd suggest learning a bit about computer programming. You don't have to do it for a living, and I lack sufficient sadism to suggest that aspiring writers take on software development as a day job. It's thankless work, and frequently makes writing unnecessarily difficult. However, learning to code requires learning *logic*, which serves writers as well as it does mathematicians, scientists, and programmers.

Be ruthless in pursuit of your art. Defy everybody who opposes you, and never give them time to discourage you. The converse is also true: *acknowledge treasure everybody who has ever supported you.* If you're lucky enough to have a lover or spouse who's willing to help you, don't screw up that relationship.

##How do you promote your work both on and off the internet?

I've focused the vast majority of my promotional efforts online, especially on Google+. I got my publishing deal by posting bits and pieces as I wrote them. Afterward, I'd talk about the plot as I worked on *Without Bloodshed*. I'd also post dialogue stripped of narrative context using hashtags like #ShitMyCharactersSay.

Since I'm a metalhead, and music is incredibly important to my writing, I also make a habit of posting YouTube videos of songs that helped me develop some aspect *Starbreaker*, and discuss why these songs matter to me.

I worked with artist [Harvey Bunda](http://soulspline.deviantart.com) of [Gunship Revolution](http://www.gunshiprevolution.com), commissioning portraits of several of my major characters. I use this character art in posts about my work and characters.

I help promote other independent writers and musicians, recommending their books and music. Sometimes they ask, and sometimes I come across them, check them out for myself, and decide they're worth mentioning.

I also comment on current events, especially if they apply to the *Starbreaker* setting for some reason. For example, when Google Glass was first announced, I linked it to Witness Protocol, a technology in the Starbreaker setting that allows people equipped with implanted computers and the appropriate software to record everything they see and hear.

##Is there anything else you would like me to write about?

I think you and your readers are thoroughly sick of me by now, but you might also be interested in interviewing some other independent authors whose work I enjoy and recommend: Michael Shean (the Wonderland sequence), Lynda Williams (the Okal Rel saga), K. H. Koehler (the Nick Engelbrecht series), Charity Bradford (author of _The Magic Wakes_ and _Stellar Cloud_), and Michael Reeves-McMillan (author of *Realmgolds*).

However, if I might beg your indulgence a *bit* longer, I'd like to mention that while *Without Bloodshed* is not yet available, I do have a story entitled "The Milgram Battery" available in the [Curiosity Quills Primetime](http://www.amazon.com/Curiosity-Quills-Primetime-Charity-Anthology-ebook/dp/B00FOEYYJU) charity anthology. Five bucks gets you twenty short pieces of weird fiction, and ten percent goes to reputable no-kill animal shelters across the United States.

##How should people find you online...

Author Website: http://www.matthewgraybosch.com  
Starbreaker series Website: http://www.starbreakerseries.com 

Email: [mg@matthewgraybosch.com](mailto:mg@matthewgraybosch.com)

Google+: [Matthew Graybosch on Google+](https://plus.google.com/103251633033550231172/about?rel=author)

Twitter: @MGraybosch

Facebook: https://www.facebook.com/matthew.graybosch

Goodreads: https://www.goodreads.com/matthewgraybosch

Other: http://www.amazon.com/author/matthewgraybosch

You can also try conjuring me, but the last person to try squiggled a line that should have been straight while drawing his summoning circle. The poor schmuck ended up as a chew toy for the Hounds of Tindalos. So it goes.
