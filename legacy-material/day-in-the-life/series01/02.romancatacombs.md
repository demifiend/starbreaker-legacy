# Under the Roman Catacombs, 23 August 2085

"We're going to die down here, Naomi. Nobody's going to find us." Nathan Bradleigh's voice quavered on the edge of tears as he gripped Naomi's arm. He clung to her, trembling in the dark. 

Half an hour had passed since Niall and Norman lost them in a neglected section of Rome's ancient catacombs. Naomi had remained silent, waiting for her eyes to adjust to the subterranean gloom, but Nathan's grip had become painful. "Let go of me."

A sniffle. "But what if I lose you too?"

"Don't worry about that." Naomi took her brother's hand. "See? I've got you."

"But we're stuck down here."

"The hell we are."

"You can't curse down here. It's disrespectful to the dead."

Naomi shook her head. "They're in no position to complain. They're dead, remember?"

"But aren't there ghosts?"

Naomi shook her head, even though Nathan wouldn't be able to see it. "I won't believe in ghosts until I've grabbed one by the scruff of the neck."

That shut Nathan up for a little while, but he eventually began sniffling again. "Are you still there, Naomi?"

She gently squeezed her brother's hand. "I'm still here. Are you worried because I was quiet?"

"Yeah."

"What if I sing for you?"

"But you only sing songs by dead people." 

Naomi smiled at the familiar complaint. If he retained spirit enough to gripe about the old rock ballads she liked to sing, he would probably be fine. "Are you going to Scarborough Fair..."

"Dammit, Nims, not *that* bloody song."