#Outline#

1. Naomi Bradleigh at medical examiner's office.  
	1. She examines the victim herself and sees that his throat as been ripped out.  
	2. Further examination indicates the use of teeth, but the bites do not match any animal local to the area.

2. Naomi Bradleigh at the house of the last gang member.  
	1. The youth is afraid of her.  
	2. Upon questioning, Naomi learns that the killer was also pale with fair hair, and had red eyes.  
	3. The killer was also dressed in some kind of uniform.  
	4. Naomi agrees to guard the youth overnight, in case the killer comes for him.  
	5. Since the previous victims were all killed while alone, Naomi arranges for the last survivor's family to leave town.

3. Naomi Bradleigh standing guard.  
	1. While the last survivor sleeps, Naomi remains awake.  
	2. Though she hears a noise downstairs, she does not leave her charge to investigate.  
	3. The source of the noise eventually comes upstairs, but does not dare Naomi's sword.  
	4. As it retreats, Naomi hears it mutter something about "protecting the installation".

4. Naomi Bradleigh discovers the truth.  
	1.  Naomi checks with the Phoenix Society, and learns that the North American Commonwealth's War Department had an ARPA installation near this village.  
	2. The purpose of this ARPA installation is unknown, but the Society suspects it pertains to biological warfare.  
	3. Naomi wakes the survivor and questions him. She learns that they found and looted an old military base hidden inside a mountain.    
	4. They knew nothing of any research; they were happy to find old uniforms, weapons, ammo, and the like. These things could be sold to collectors, and they all needed money.
	5. Naomi presses the youth, and discovers that they also found a crate of backup drives near a lab inside the installation.

5. Naomi at the ARPA installation.  
	1. Naomi rides up to the installation on a dirt bike, wearing an officer's uniform looted from under the mountain.  
	2. She finds the killer prowling the halls under the mountain.  
	3. To Naomi's surprise, the killer does not attack, but instead comes to attention and salutes.  
	4. Naomi learns that the killer had been a North American Commonwealth soldier named Arthur Morris.

6. Project Harker  
	1. Arthur Morris volunteered to be a test subject for something called Project Harker, an attempt to create vampire-like super-soldiers capable of engaging in night-time guerrilla warfare with minimal support and equipment, during Nationfall.  
	2. Once things began to fall apart, Morris was ordered to guard the Project Harker installation.  
	3. Naomi tells Morris that the North American Commonwealth is long dead, and that he is a soldier without a country.  
	4. Hearing this, Morris attacks Naomi. She cuts him down, and hears his last words: "Thank you, ma'am."
	5. Naomi returns to the village with Morris' body, and explains what had happened to the residents.
	6. She leaves once Arthur Morris' body is buried.