## Chapter Thirty-One

With five hundred irregulars from the town militia behind me, plus some youth volunteers who insisted on coming along because Scott Wilson had been their friend, we made short work of cataloging the equipment stored aboveground at Fort Clarion. The completion of my official mission was an anticlimax of sorts, but I was glad that nobody else died as Scott Wilson did four days ago. 

We found plenty of automatic rifles, submachine guns, pistols, sharpshooters' rifles, and even half a dozen anti-materiel rifles that fired such outrageously powerful rounds that using them against unarmored personnel must have constituted a war crime. In addition to the small arms, Fort Clarion also boasted two armored personnel carriers and four helicopters, all of which were armed with 12.7mm machine guns.

Nor were the barracks and officers' housing without treasures. We found enough government-issued fiat currency to knock the bottom out of the market for Commonwealth dollars among numismatists. One officer's house held a large cache of marijuana; one of the irregulars sniffed it out and used the stock of his rifle to smash through the drywall. Every other footlocker in the barracks held some sort of skin mag.

When not issuing orders for Sheriff Robinson to pass down to his sergeants, I busied myself by using my copy of HermitCrab to crack every computer the irregulars found on the base. I found music by long-dead bands too obscure to merit a revival, pornography depicting kinks Jacqueline never told me about. Unfortunately, the computers held little of professional interest, and bugger-all concerning Project Harker. 

I did learn that one of Dr. Petersen's trusted lieutenants had been shagging Petersen's wife whenever he was off base for more than a day, which I would have preferred not to know. Good thing the doctor wasn't looking over my shoulder when I made *that* sordid little discovery. He would have been unable to find closure by confronting his wife or her paramour; they both died during Nationfall.

After we returned to town, I wanted to make my report to the Phoenix Society immediately. The sooner they sent an arms control team, the sooner all that ordnance would cease to be a threat to anybody. Moreover, with more Adversaries at my side, we could penetrate Fort Clarion's underground and drag whatever lay beneath into the sun.

Both Sheriff Robinson and Dr. Petersen seemed to have other ideas. They had gotten to The Lonely Mountain ahead of me since I dropped behind to guard the rear. Robinson raised a half-empty glass of beer as I walked in. "There you are, Adversary. How about joining us for a drink? Doc's paying."

"Just one, guys. I have to file my report with the New York chapter." And I'll be discreetly checking that one drink for drugs unless it comes directly from the barkeep's hands to mine. 

Sitting with them, I ignored Dr. Petersen's scowl and waved at Dick Halford. No doubt Robinson had already had a few at the doctor's expense, and the old man expected me to follow suit. Maybe I *should* order something outrageously expensive, just to justify the puss on his face.

Halford brought a glass of the house red from behind the bar. "Want me to leave the bottle?"

It was cruel of him to tempt me so. I had been on edge for days now, expecting another attack by altered soldiers traumatized by years of isolation -- or the discovery of another bitten-up corpse. Neither had happened, thank all the gods, but all I wanted right now was to have a drink, make my report, take a long hot bath, and curl up in bed for an early night. "Thanks, but I'd better stick with one glass tonight. Dr. Petersen's paying, and I don't want to take advantage of his generosity. Besides, I still have to report in."

Dr. Petersen seemed to relax as I set my limit. Was he really afraid I'd abuse his generosity? You'd think he'd have more pressing concerns than how much of his money I'd spend when I'm searching for evidence against him. "You worked hard the last few days. How's your side?"

"There's barely a scar. Nurse Thorvaldson does excellent work." Not that a scar would have bothered me. They're an occupational hazard and the only medals of valor any Adversary can expect. At least, that's what my instructors claimed Edmund Cohen would say. It's not like I can wear a bikini, in any case. "I feel fine."

Petersen nodded. "Have you spoken with anybody about your experience? I can recommend a colleague."

I had talked about some of it with Kaylee over drinks a couple of nights ago, and dished with Jacqueline at greater length, but I wasn't going to tell Petersen that. It was none of his business. "I'm fine, thank you."

Robinson changed the subject, for which I was grateful. "We still haven't found anything we can use to close the Wilson murder. I don't think we're going to find a suspect or evidence with which to convict him aboveground."

Petersen shot me an accusing glare. "How much did you tell him?"

"I answered the Sheriff's questions as honestly as I could with the knowledge I possessed." It would take more than an old man's glare to intimidate me. I met his stare, and held it until he turned away. "If you aren't willing to be honest with *me*, perhaps you should have a long, confidential talk with the Sheriff."

Draining my glass before either Petersen or Robinson could react, I rose and bid them goodnight. Now that I was back in my room and alone, the last thing I wanted was to rehash the last few days with Saul. If he cared that much, why couldn't he just ask Malkuth or one of the other Sephiroth to summarize my Witness Protocol feed?

I opened the channel anyway, after double-checking the Fort Clarion inventory and sending it to Malkuth. Saul and Iris wanted personal reports from Adversaries in the field. While Director Chattan was content to get his summaries from Malkuth, I was in Rome now, and it was sensible to emulate the Romans. 

I usually reported to Saul, but it was Iris Deschat who took my call tonight. She was closer to handsome than beautiful, but her face projected a mature strength tested and tempered by many trials. It was a face suited to command. "Good evening, Adversary Bradleigh."

"Good evening, Director. I'm pleased to report that the materiel inventory for Fort Clarion is complete."

Iris nodded. "I have a copy. Were you able to complete your other investigation?"

"Not yet. My Witness Protocol feed will show that Fort Clarion is still inhabited by the remnants of a Commonwealth Army unit called Dusk Patrol. I am confident that some of these individuals are behind the disappearances that have plagued Clarion ever since its resettlement, as well as the recent murder of Scott Wilson of Clarion. However, I cannot name individual suspects, nor can I provide sufficient evidence to convince a jury."

Deschat narrowed her eyes in disapproval. "But you found time to seduce a Dusk Patrol survivor who might be responsible for the disappearances, and have time to drink with the Sheriff and the town doctor? Why have you not yet penetrated Fort Clarion's underground and rooted out the necessary evidence?"

"With all due respect, Director, my coupling with Christopher Renfield yielded invaluable intelligence. Furthermore, Sheriff Robinson has been extremely helpful, and I would have had a great deal of difficulty completing my inventory of Fort Clarion's armament without his cooperation and that of the local militia. Finally, we have not yet found a way into Fort Clarion's underground. Once I've done so, I must then persuade Sheriff Robinson and Mayor Robinson to ask members of the town militia to follow me down there since the Phoenix Society has not provided me any backup." 

As I paused before making my final point, Iris opened her mouth to speak. I cut her off, and didn't bother phrasing my concerns in a manner she might find palatable. "For fuck's sake, you people practically go autistic whenever I bring up Project Harker. If you can give me shit about half an hour of rebound sex, but can't be arsed to provide me intel or backup, you have *no* business questioning my priorities."

"Director Chattan told me you were a spirited young woman. I'm glad to find he wasn't lying." Deschat favored me with the flash of an indulgent smile. "Unfortunately, the Executive Council has bound my hands on this matter. The following comes straight from the top, Adversary Bradleigh. You are to cease your investigation of the Clarion disappearances and Project Harker immediately, and return to London for the remainder of your leave."

So, the Executive Council was happy to take advantage of my zeal when it could be turned to a job they would otherwise have left undone for lack of available personnel, but now that they have an inventory of the equipment stored at Fort Clarion, they want me to just turn my back and let the cancer eating away at this town continue to fester? My fists clenched and began to tremble as my resolve hardened into cold steel. "With all due respect, Director, the Executive Council can sod off. I am going to get to the bottom of this with or without the Society's help. If you don't like it, you can bloody well send Adversaries to arrest me."

Deschat shook her head. This time her smile seemed regretful. "If Adversaries come to arrest you, it won't be on my order or Saul's. I can promise that much, but nothing more."

Was Deschat sympathetic to my cause, but unable to help without jeopardizing her own position? "Thank you, but why are you helping me?"

She remained silent for a long moment before answering. "I don't know how much Director Chattan told you about me, but I was once the captain of a Commonwealth Navy nuclear submarine, the *Thomas Paine*. Just before everything fell apart, we received an order to fire our missiles at New York, on our own fellow citizens."

"Holy shit." The words came involuntarily.

"It wasn't an order I could follow in good conscience. Nor was it an order I could defy without the unanimous support of my crew, because once I refused that order, they'd most likely pay for my defiance alongside me. Fortunately, none of us aboard the *Thomas Paine* were willing to nuke our own people." 

Holding my breath, I waited for Deschat to relate how she and her crew survived the retribution her superiors must have attempted to exact for their defiance. Rather than continue her story, she studied me. "Your conscience won't allow you to let this go, will it?"

"Innocents are suffering for this secret, Director. Your own countrymen are still at war, with nobody to release them from their nightmare. What the hell do you *expect* me to do about it?"

Deschat saluted. "I expect you to uphold your oath, Adversary." She cut off the connection, but seconds later a text came through from an anonymous sender: 『S. and I will try to help, but we can't use official channels.』 
