## Chapter Fifty-One

"Crimes against humanity?" Dr. Petersen still slurred his words, though not as badly as before, and reduced what would have been a cry of protest to a bleat. "Adversary Bradleigh, I have no idea how you could possibly suspect me of any such -"

"Project Harker." The words knocked the wind from Petersen as surely as if I had punched him in the gut. But before I could continue, Dr. Petersen needed to be informed of his rights. "Colonel Henrik Petersen, you are under arrest for crimes against humanity not limited to unethical experimentation upon human beings without informed consent. You have the right to remain silent-"

"We had the Miranda Warning in the Commonwealth, Adversary. I understand my rights."

Favoring Dr. Petersen with my sweetest smile, I patted his uninjured shoulder. "I'm sure you understand your rights under the old regime, doctor, but we've updated the classic warning. You have additional rights, of which you might not be aware, and because you have suffered a concussion, I absolutely must notify you of them and confirm your understanding."

"Yes, yes. Representation by an attorney, network access for preparation of my defense, access to all evidence against me, and humane treatment while in custody." Petersen was done slurring his words, and now spoke in a clipped, impatient tone. "I have a concussion, not Alzheimer's disease."

"Excellent. I shall recall that should you attempt to evade a question by claiming a gap in your memory. Now, do you think you can walk?"

Petersen was a bit wobbly on his feet at first, but waved away the hand I offered to help him. He soon steadied himself, and reached the stairs before I did. "Do you know a safe place where we can talk, Adversary?" He glanced at the soldier glaring up at Mike. "My house is not defensible, and it should be obvious that I'm of no use to you as a hostage."

The Lonely Mountain was tempting, but instead of using one human shield, I'd be using dozens or hundreds. I couldn't arrest them all, and I was already right up against the ethical line as it was. Fortunately, it didn't seem as though Mayor Collins had discovered the secret under Gibson Hacker Supply. "I know a place."

The stocky, dark-haired deputy guarding the entrance favored me with a suspicious glare, and rested a sun-browned and heavily callused hand on the hilt of her service gladius. Her badge identified her as Alvarez. "Sorry, Adversary, but there was another murder on the premises today. Mayor Collins is dead."

Hearing the man was actually dead was a relief. The last thing I needed was an enraged and technologically augmented public official who had survived having a shotgun emptied into him at close range, and now had a legitimate reason to dislike me. Regardless, it simply wouldn't do to tell Deputy Alvarez I knew anything. It was easier to feign ignorance than to pretend I wasn't glad the vicious son of a bitch was dead. "That's terrible! Does he have somebody who can take over until the election and maintain order?"

"Yeah. Gets worse, though. Whoever did the job used a shotgun, and went for the overkill, but cleaned up after themselves. We couldn't find a single shell casing." Deputy Alvarez shook her head. "Damnedest thing. And somebody took a hammer to that big-ass computer in there, too. But no sign of theft."

"Sounds like a professional hit. Mind if I take a look inside? I might spot something."

Alvarez shook her head. "Sorry, Adversary, but I can't let you inside without notifying the Sheriff."

I should have expected this. Fortunately, my implant provided a function that let me get the IP addresses of others around me. Once I had the deputy's, I passed it to Malkuth. [Can you spoof Sheriff Robinson's IP address and tell Deputy Alvarez to let me and my prisoners through?]

[Sure.]

"Who are you talking to, Adversary?"

Alvarez wasn't quite ready to draw, but it wouldn't take much. If I didn't defuse her suspicion, I'd have a fight on my hands. "Sorry, Deputy Alvarez. I just contacted the Sheriff myself. You should hear from him directly."

She took her hand from her sword's hilt and pressed two fingertips to her ear, just as I did to indicate an incoming call. Alvarez shrugged and stepped aside. "Just got word from the Sheriff, Adversary. You and Dr. Petersen are clear."

"Thank you. What about Mr. Brubaker?"

Alvarez glanced at him. "Why do you need the kid?"

"He's my prisoner. I have to keep him with me until the Society sends somebody to pick him up."

"But he could tamper with the-"

"If he tampers with anything, I'll kick his arse so hard, he lands in London. Do you have any other questions, Deputy?" Alvarez might be doing her job, but the sun was setting, and I hadn't fully eliminated Dusk Patrol. The ones I hadn't arrested could still surround me on the street.

Her grip tightened on the hilt of her gladius. "What if I refused to let any of you through?"

"Drawing your blade first will be your last mistake." My hands were already on my sword, ready to take first blood as soon as the blade cleared the sheath. I was taller, and both my arm and blade were longer. Whatever strength Alvarez possessed would be useless if she couldn't reach me. If she rushed me, I'd run her through. "Think it over."

Evidently she did, for after a moment in which she glanced at Brubaker and his shotgun, she let go of her sword's hilt and shrugged. "I don't get paid enough for this shit."

Neither do I, but there was nothing for it but to finish the job. "Thank you, Deputy. We won't be long."

Alvarez nodded as she stepped aside to let us into the shop. Once we had locked the door behind us, Petersen surprised me. Striding directly to the wall that concealed the hidden basement entrance, he pushed aside a framed poster of an angry-looking African man wielding a pistol and saying, "POSIX, motherfucker! Do you implement it?". He pressed the button Cat Tricklebank used to open the door I’d thought a secret.

He glanced over his shoulder. "You kids coming?"

Once we were safely downstairs with the door shut, I asked the obvious question. "How did you know about this entrance?"

Petersen shrugged. "I had it built during Nationfall. Normally we'd use the engineer corps for this sort of work, but the boys in Dusk Patrol proved admirably capable. They dug the tunnel leading here from Fort Clarion to transport Tetragrammaton Zero in secret."

"Why are you telling me this? You realize everything you say will be used to prove your guilt in court, right?" That was assuming his attorney didn't convince the judge that despite my efforts, Petersen wasn't competent to claim he understood his rights because of his injuries.

Brubaker stared at me and texted. [What the fuck are you doing? If he wants to hang himself, just give him more rope!]

"It doesn't matter." A note of weariness entered Petersen's voice as he sat down. "I am guilty, but I probably won't live to stand trial. The Phoenix Society will soon send an arms control unit despite my contact's efforts to delay their arrival, and once they do, they'll finish the work you began, Adversary Bradleigh. And before that happens, Sheriff Robinson will kill me so that I cannot dispute his account of events."

Before I could say anything, Brubaker spoke up. "What about the Mayor?"

"Robinson would eventually have done the same with Mayor Collins. You did his work for him."

The conclusion seemed pretty obvious. "With you and Collins unable to speak up, Robinson would be able to pin everything on you two."

"And escape justice for his own crimes in the process." A weary smile flashed across Petersen's weathered face. "In fact, you already thwarted his first attempt on my life."

Robinson sent that Dusk Patrol soldier? It certainly fit. A significant number of them seem to have thrown their lot in with him via Corporal Seward instead of remaining loyal to Sergeant Renfield and Dr. Petersen. And who did I have arrested? Renfield and many of his loyalists. That bastard Sheriff was probably having a fine laugh at my expense. "He's been manipulating me from the beginning, hasn't he?"

"I don't think Robinson has it in him to consciously manipulate people. But he's a cop, and good cops are like good officers. They know better than to interrupt an enemy while they're making mistakes."

Small comfort, that. Looks like I've exhausted any margin for error I might have had. "I think you had better start from the beginning. Project Harker and Ian Malkin from the AsgarTech Corporation sound like a good spot."

Petersen startled at Malkin's name, as if surprised I knew it. Or was he afraid the devil might pop up at the mere mention? "So, you managed to get into my files after all. How?"

"Friends in high places." No need for further explanation if Petersen wasn't already aware of the ten AIs who served the Phoenix Society. "I know all your nasty little secrets, doctor."

Brubaker scratched his head, the friction of fingernails against scalp loud in the basement. "Naomi, what do you think Robinson will do if he learns you have access to that data? Who's going to protect those computers?"

Questions like that are what I get for not telling Mike everything. He doesn't know that Tetragrammaton is already uploading compressed archives under the system account. A quick check with my implant showed that both transfers were more than halfway done. A couple hours more, and the data would be forever beyond Robinson's control. "The computers themselves don't matter. It's the data that counts, and I've already sorted that out."

"You arranged for the Society to grab the data already?" Petersen studied me a moment. "A wise choice."

"The Society isn't grabbing the data. I'm sending it to them." Not to mention Port Royal, but I kept that bit of information up my sleeve for now. It was my ace in the hole, which I would only play if the Society tried to bury the evidence and cover up what happened here. Likewise, if they tried to make me an unperson.

Instead of turning defeated by this revelation, Petersen's expression brightened. He sat straighter, as if an unseen weight fell from his shoulders. His smile was that of a man who no longer had to fear the worst-case scenario because it had already happened, and he was still alive. "If you already have the data, then Robinson can no longer buy my cooperation with his silence. I might as well tell you everything."