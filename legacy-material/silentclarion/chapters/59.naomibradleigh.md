## Chapter Fifty-Nine

Ten seconds left, and I had received no response from GUNGNIR. There was no sign that the satellite had accepted or rejected my command to cancel Dr. Petersen's previous command to drop its remaining payload on Clarion. There was nothing more I could do. Nothing to do but wait, and hope.

My hopes waned by the second. Only one second remained, then nine hundred and ninety-nine milliseconds as my implant unhelpfully switched to smaller units. With only ten milliseconds left on the clock, a response finally came down from GUNGNIR.

> Launch aborted.

Unable to believe what I was seeing, I blinked even though my implant's display was superimposed over the raw perceptual data coming from my eyes. The message remained. The launch had been aborted. And now I was back to the control menu, which now offered a self-destruct option.

Because of the time of day, there was no way to see GUNGNIR's destruction from the ground. The sky was simply too bright. But I imagined the satellite using the thrusters that allowed it to change its position to reenter Earth's atmosphere and either burn up on entry, or splash down somewhere in the Atlantic Ocean.

Either way, a tsunami of elation crashed over me, leaving me feeling as if I were invincible. Though I wanted nothing more but to jump around and cheer like a crazy person, I simply smiled at Mike and Renfield. "Guys, you can breathe now. I aborted the launch and dropped GUNGNIR into the ocean."

Grabbing Renfield, I stole a searing kiss just to drive my point home. I was about to treat Mike in similar fashion when almost a meter's worth of sharpened steel erupted from his chest before disappearing just as swiftly. Mike fell to his knees, bloody froth bubbling from the mouth I was about to taste mere moments ago. "Renfield! First aid! Get a fucking paramedic!"

It was as if somebody else had shouted the orders using my voice for a moment, but I forced myself back into reality as Dr. Petersen circled around me, the sword he had concealed in his walking stick dripping red. Drawing my own sword, I devoted my full attention to the treacherous doctor. "Are you truly so desperate to die, Dr. Petersen?"

"I told you, Adversary Bradleigh. I will not permit the knowledge within my head to fall into the Phoenix Society's hands. Since you've stopped GUNGNIR, I must resort to other methods."

We circled one another, sizing each other up. Neither of us seemed ready to strike the first blow. Neither of us wanted to be the first to reveal our style, to give the other insight into our strategy. "If you wanted to commit suicide, there are easier ways. You could have done the job with a pistol instead of attempting to murder one of my witnesses."

"That was not an option for me." Petersen seemed almost regretful as he spoke. "Unlike yours, the treatment I administered to myself was the first version, based on modifications to the Patch. It includes inhibitors intended to prevent certain kinds of ideation. I can't even use related euphemisms without blacking out."

So, he could attempt suicide by Adversary as long as he didn't say or think the words? That didn't make any sense, since I said the word, he heard it and understood it, and managed to remain standing. "I think you're bullshitting me, old man. I think you *could* fall on your sword or eat the gun if you really wanted to. You're still playing some kind of game. Did Ian Malkin put you up to this?"

"No." The first genuine, unaffected smile I had seen on Dr. Petersen's face lit his features, casting him in psychopathic relief. "The Devil's honest truth is that the only way to properly test the effect of my treatment on you is to push you to your limit—and I don't think you've reached it yet."

This is definitely some kind of bullshit game, and I'm sick and tired of playing. "Trust me, old man, I've hit my limit. Now shut your gob and *fight me*."

Leading by example, and running counter to years of training, I let my anger at Petersen's cowardice drive me to strike the first blow. Steel clashed as he parried my thrust with his sword while swinging the cane that had concealed his blade at my head. The price of my overconfidence was a ringing blow to the head that left me reeling and vulnerable for a moment.

Recovering before Petersen could follow up, I parried a cut from his sword. This time I knew his cane was coming, so I stepped inside his guard and struck him with my off-hand. His nose crumpled beneath the palm of my hand, and I licked my lips as blood poured forth.

Unperturbed by the damage I inflicted, he knocked me to the ground with a shoulder strike. This forced me to scramble to get back on my feet before he could pin me to the pavement with his sword. It was obvious that I hadn't been taking Petersen seriously. Despite his apparent age, the experimental treatment he administered on himself must have restored his youthful vigor.

"I expected better from you, Adversary Bradleigh." Petersen's voice still carried the nasal tone of a man with a broken nose. It must have healed as is, just as my broken arm had last night.

Circling around him, waiting for him to strike and expose himself, I gave him the finger. "You're not the first man I've disappointed, doctor."

A cruel smile curved Petersen's features. "No doubt you disappoint every man who first sees you from behind before getting a look at your face."

Was that pathetic excuse for a butter-face joke supposed to goad me into dropping my guard? Not bloody likely. "Just keep digging that grave."

"If you could have killed me, you already would have." Petersen lunged, but my initial anger at his stabbing Mike had cooled and it my training had reasserted itself. His thrust was barely worth the effort it took to sidestep it. While he was off-balance, I punished his failure with a slash across his wrist.

We continued to dance in this manner as the ambulance arrived and paramedics attended to Mike. A crowd had gathered, not understanding why Petersen and I fought, but cheering the barbaric spectacle of our duel. I was caught up in the flow now, and all but untouchable. Every time Petersen attacked, his blade sliced air as mine tasted his blood. His suit was soon tattered, his leathery skin showing through dozens of rents in the cloth.

A normal man would have given up by now. He would have thrown down his sword and surrendered. However, Petersen fought with the determination of an enraged bull, all finesse lost. Whether he retained his intent to test me as part of whatever deranged experiment he had concocted, or was now fighting to avenge wounded pride, he would not stop.

However, he had slowed a little. The little cuts I inflicted as punishment had begun to take a toll on his body. Without food, the only way his enhanced flesh could repair itself was by reconfiguring existing tissue in a catabolic process. He would eventually eat himself alive, unless he submitted. "I can do this all day, Adversary. Even if you can keep my blade from tasting your flesh, your endurance isn't—"

Stopping to monologue in the middle of a duel is a bad idea. Sticking my sword in his lung seemed the best way to illustrate this fact. Besides, I had gotten bored with smacking Petersen around as if he were a mouse. But before I could press my advantage and disarm the doctor, strong hands caught my arm. The voice behind me was one of implacable command. "Stand down, Adversary Bradleigh."

Wrenching free of the interloper, I spun and was about to turn my sword on him when another rammed the stock of her Kalashnikov into my ribs before reversing her rifle and pressing its muzzle into my temple. "Adversary Naomi Bradleigh, you are under arrest. The charges are misuse of Phoenix Society property, destruction of Society property, inhumane treatment of a prisoner in custody, and abuse of authority."

"Inhumane treatment?" The charges stunned me, even though I had been aware the whole time I was right up against the line. "Who did I treat inhumanely?"

"We saw you torturing Dr. Henrik Petersen, whom you had previously arrested." The Adversary who had caught my arm and warned me to stand down spoke. Something in his tone reminded him of my father as he swept an arm to indicate the crowd around us. "So did a few hundred locals."

None of this made sense. None of it was in any way fair. But I remained silent as my fellow Adversaries recited my rights as one accused of committing crimes with the solemnity of a priest administering the last rites. I had even withdrawn into myself a bit, so that the Adversary not holding a gun to my head crouched in front of me and snapped his fingers. "Adversary, I repeat. Do you understand your rights?"

"Yes, I understand my rights. Now tell your girlfriend to get that AK out of my face. And tell me what happened to Mike Brubaker. He was under my protection. What's his condition?"

Instead of answering, the Adversary nodded at his partner. She reversed her rifle, so that the last thing I saw before unconsciousness claimed me was the stock of that damned thing rushing toward me and a flare of crimson pain spreading across my vision.