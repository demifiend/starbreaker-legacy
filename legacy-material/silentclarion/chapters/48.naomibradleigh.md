## Chapter Forty-Eight

It's amazing how far a good breakfast and a couple of mugs of hot coffee can go toward substituting for a good night's sleep. By the time I finished my second cup, I actually felt capable of rational thought again. Which was a good thing, since I still had work to do at Gibson Hacker Supply.

My use of the Clarion Sheriff's Department to ambush Dusk Patrol and arrest them *en masse* had a useful side-effect: there wasn't anybody guarding Tricklebank's shop. All I had to do was open the door, and walk right in. One small problem, though. "Bloody door is locked. Figures."

Mike nodded toward the alley. "Probably a good idea to use a back door, anyway."

We ducked down the alley to the narrow street behind Gibson Hacker Supply, giving the door knob a quick turn to ensure our luck hadn't changed, that I got his meaning. Checking his surroundings first, Mike crouched by the back door and produced a small case. He seemed to know something about the locksmith's trade, for he took a moment to study the lock before picking specific tools for the job. "When I was six my grandpa caught me fiddling with these tools. He taught me how to use 'em, and put me to work. Guess he thought it better then having me learning how to use the tools on my own out on the street."

"Could be a handy skill for an Adversary." Though problematic as hell from a evidentiary standpoint. Would any information I gathered as a result of this bit of breaking and entering be admissible in court?

"Yeah, but I was the one who got the others inside Fort Clarion. If I didn't know how to pick locks, they'd all be alive."

"Are you sure you're not blaming yourself so you can make sense of what's been happening?" 

His hands stopped as he looked over his shoulder at me. "The hell does that mean?"

"It means it won't bring any of them back. Your friends are dead. They might have died without your involvement. Let's focus on making sure nobody else dies."

Mike shook his head, tried the latch, and opened the door a crack. He put away his tools in silence before holding the door for me. "Has anybody ever mentioned you sometimes sound like a stuck-up bitch?"

"Maybe it's just the posh accent."

"Whatever." Mike closed the door behind us and flipped on the lights. "Once we saw the soldiers sleeping, I got out of there. I told the others to do the same, but they didn't listen. Renfield told me that my friends tried slitting one guy's throat. When that didn't work, they hacked his head off.

Unable to keep from imagining the scene, I shuddered. It was the sort of thing that made the news ever now and then. Some junkie with purist beliefs catching someone like me alone and unarmed in a dark street. But Clarion was an integrated community. There was no reason to believe there was anti-CPMD sentiment in this small community. It was simply the level of hatred driving the crime that had been similar. "You don't want to believe your friends were capable of such brutality."

"Would you?" Mike spat the words in disgust. "Goddammit, I shouldn't have said anything in the first place. It isn't your problem."

He was right. It wasn't, but I wasn't going to say so. Doing so would indeed make me a bitch. Instead, I had work to do. "Get behind cover and watch the street. Warn me if company comes."

Opening the door to the back room, I froze in the doorway at the sight of Mayor Collins wearing safety glasses and holding a sledgehammer aloft. He brought the hammer down on the keyboard, scattering shattered plastic everywhere, before swinging it in a wide, horizontal arc to smash the display. The bastard was destroying evidence, and had to be stopped. 

The ring of my sword clearing the scabbard caught Collins' attention as he raised his hammer to strike a blow. "Mayor Collins, you are under arrest for destroying evidence pertaining to a Phoenix Society investigation and obstruction of justice. Drop your weapon and put your hands behind your head."

Mayor Collins turned toward me, still grasping his hammer. Realizing he had no intention of complying, I steeled myself for his assault.

He sprinted toward me, holding his hammer before him as if he meant to drive its head through my chest. If he was smart enough to do that instead of taking a swing that would leave him open, then he was smart enough to be dangerous.

Darting aside, I slashed at his forearm, the tip of my blade parting wool and silk before biting into flesh. I had drawn first blood, but my cut wasn't deep enough to weaken his grip on the sledgehammer. His clothing might even have protected him a bit.

Worse, he was fast. Pivoting, he jabbed at me with his weapon and caught my shoulder with a glancing blow. "You couldn't just have your little vacation and leave, could you? Well, Adversary, you're going to become a statistic."

Opening his forearm again, I followed with a cut across his cheek. "It'll be a cold day in Hell before I die at the hands of a flunky like you."

That got a laugh out of Collins. "Well, pretty kitty, I hear they're playing hockey on the Styx right now."

"That was actually a halfway decent rejoinder." But not good enough to keep me from cutting him again. "You really think Renfield will let you get away with killing me? Hell, you think the Phoenix Society will let my death go?"

"Renfield'll mourn, if only because you're the best poontang he's had in decades, but he'll get over it." Collins shrugged, deflecting my thrust with the haft of his weapon. "In the meantime, he'll do his job and keep the men in line. He'll be reliable again, just like he was before you showed up."

"Not bloody likely, given that I persuaded him and a couple dozen of his men to surrender. They're in jail right now." Collins hesitated, and I took the opportunity to run him through. He fell to his knees, holding a hand to his chest as blood leaked from his mouth. "And when they get out, do you really think they'll go back to being your pet killers? I think it's more likely they'll turn on you like wild animals kept too long in too small a cage."

"Too bad you won't live to see it happen." Wait a minute. Collins' voice was loud and clear. He didn't sound anything like a man who had just had a foot of steel driven though his lung. And what happened to the cuts on his face? He rose, and took a swing at me that I barely dodged. "There are arrangements in place."

"Naomi! Down!" That was Mike's voice, and the metallic clunk characteristic of a pump-action shotgun.. Damn right I was getting down.

Mayor Collins staggered backward with each blast of buckshot Mike unloaded into his chest. The Mayor not only remained standing, but smiled as he tore the tattered remains of his jacket and shirt from his shoulders and threw them aside. The ruined flesh of his torso knit together, and his figure grew slimmer, as Mike dropped shells in his frantic haste to reload.

No human should be able to do that, but I didn't have time to fuck around. Without a better idea, I grasped the dull base of my sword's blade to stabilize it. Charging the Mayor, I drove the point through Collins' throat so that it pierced his spine and pinned him to the wall. "Mike!"

He tossed me the pump action, and I squeezed off the first shot as soon as I had the muzzle pressed between Collins' eyebrows. His body began to slacken in death, but rather than take chances I kept firing until I had pulped the Mayor's skull.

Mike dropped the shotgun as soon as I handed it to him. "Naomi, what the *fuck* just happened?"

Good question. Dusk Patrol had been enhanced as part of Project Harker. Among other advantages, they now had regenerative capability. But they were an all CPMD+ unit. Collins was CPMD-, and had not been a victim of Project Harker. If Collins was able to regenerate who else was equipped with this ability? Robinson? The good doctor himself? All I knew was that like those Dusk Patrol soldiers I fought in the woods, you had to attack the brain to kill them. Welcome to my zombie apocalypse. "I think Dr. Petersen continued Project Harker's work in secret. He managed to apply its treatments to Mayor Collins." 

"Is that even possible?"

I pointed at the pulped remains of the Mayor's face. "I'd suggest asking him, but you'd have to find a competent necromancer first. Come on. We need to see if we can still get into Tetragrammton from here before someone comes looking for the esteemed Mayor. Help me find replacement parts for the console."

Mike nodded, and tossed me the thumb drive containing my HermitCrab environment. "If they knew about this console, they probably know about the other. If you download the information, we can use my basement in the woods to review it. I can still get a signal down there."

Cleaning my blade on a scrap of Collins' tattered shirt, I found I couldn't blame Mike for not wanting to stick around. I didn't want to, either. However, the basement wouldn't do. It wasn't even close to being defensible, since we'd be trapped down there with our enemies controlling the only exit. "They might expect us to return to Fort Clarion's underground. We'll take one of the watchtowers, instead."