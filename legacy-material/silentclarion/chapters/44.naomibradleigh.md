## Chapter Forty-Four

"You think I sided with those kids against you and your men?" It was hard to believe Renfield could misunderstand my purpose in coming to Clarion. I was never for or against Dusk Patrol. How could I have been, when I had remained unaware of their existence until after my arrival? "I came here to find out why people visiting Clarion disappear, not to take sides in your little guerrilla war against the local yokels.

"Hey!"

Ignoring Brubaker's protest, I pressed on. "Even if I had known they might be guilty of murdering your fellow soldiers, I would still have tried to protect them. But instead of witnesses, they would have been prisoners awaiting trial for murder."

"What do you think you'd accomplish by putting these kids on trial? What jurisdiction would recognize us as persons under the law?"

"The Phoenix Society would recognize you. You think Fort Clarion is the first isolated pocket of humanity we've found and helped rejoin civilization? You aren't even the first isolated bunch of soldiers we've had to help understand that the war's been over for decades, and it's time to lay down your arms and come back to the world."

Renfield and Fowler's bitter laughter at my words stung, but Renfield's reply cut deeper. 

"You're cute, Bradleigh. You want us to come back to the world and trust your justice? You saw what the world did to us. Those kids killed our brothers in cold blood. We'll make our own justice."

Brubaker shook his head. "Make your own justice? All you're doing is taking revenge, just like my idiot friends were doing."

"I am the only one standing between you and the same fate your friends met." He turned to Fowler. "Tell the others it's finished."

"Yes, sir." Fowler saluted, and left.

Once the door closed behind him, I returned my attention to Renfield. "Care to tell me why Brubaker would deserve to join his friends in the grave?"

He shook his head. "The kid didn't do anything to stop his gang. That makes him an accessory."

"I wasn't even there, dammit."

A door thunked shut outside. Instead of saying anything, Renfield held a finger to his lips. 

Breathing as softly as I could, I listened by the door. There were voices, and slowly approaching footsteps. We waited, breathing as shallowly as we could, until all sounds from outside faded into silence. Looking over my shoulder, I kept my voice at a whisper just in case. "Why aren't they looking for us? They don't seem to realize a couple of outsiders are down here."

A long pause before Renfield answered. "They trust me." He gestured toward some filing cabinets. "Give me a hand with these, kid."

The room filled with the grinding of metal on concrete as Brubaker and Renfield pushed aside some filing cabinets to reveal a door. Renfield unlocked and opened it, revealing a stairwell. I tensed, sure the noise would bring the others down on us, but no one came.

Nobody needed to tell me what to do. Since I was first up the stairs, I listened by the door a moment before opening it to reveal Fort Clarion in the cool, faint light of an early autumn morning. 

The men followed me, and Renfield locked the door behind him before leading us to the Post Exchange. "We'll be safer aboveground, at least during the day, but we can't linger."

Brubaker looked askance at Renfield. "You're coming with us?"

Renfield shrugged. "Might as well. The others will figure out soon enough that I didn't kill you two, and they'll be none too thrilled with me."

"What are you going to do, then?"

Another shrug from Renfield. "Maybe I can help. I don't really know anything about Project Harker other than what I told you, and maybe it's time I stopped living in the dark."

Brubaker snorted. "Gonna make your own justice?"

"I don't know." Renfield fell silent for a long moment, and I was about to prod him when he spoke again. "I thought I was leading the men by believing in the last mission we were given. I clung to that belief, perhaps for too long. It might be too late for me to lead them by showing them a way back to the world, but Naomi, what you said about the Phoenix Society helping others in this situation gives me hope. Do you really think you can help?"

This was the Renfield I remembered from before he tranked me and left me tied up in a meat locker, but was he being honest with me this time? Did he really do it to save my life and Brubaker's? I wasn't willing to trust him, but if he wants rope, I'll give it to him. If he hangs himself, that's his problem. "I'm willing to try, but I need you to answer my questions as honestly as you can. No more bullshit."

Brubaker glanced at Renfield a moment before leaning close to me. 「Are you sure this is a good idea?」

Not that he needed to be close to me if he was using secure talk, but it wasn't worth addressing right now. 「No, but I think Petersen, Collins, and Robinson are using Dusk Patrol. They believe they're protecting themselves by preserving their former superiors' secrets, and we're not going to change that until we expose the truth.」

「Fine. Then ask Renfield about Tetragrammaton. Even I know a computer like that wouldn't just be sitting in an abandoned town like Clarion if it was still in working condition.」

Now there was a good point, and it had been nagging at me for a while. Why did Clarion have a working General Atomic mainframe in a random building's basement?

"You two done with your telepathy, or however the fuck you're talking to each other in secret?" Renfield's tone held a note of disapproval, which was fair enough. We probably were being rude by having a private conversation right in front of the man.

"You know anything about that mainframe we've got in town, Tetragrammaton?" Brubaker blurted the question before I could regain control of the situation.

"Fuck off, kid. I saw you leaning close to Naomi. Is that what you were discussing with her?"

"Actually, Mike was questioning the wisdom of trusting you." And under the maglev he goes. It wasn't personal, of course, but I can't have him making enemies on my behalf. Not when I can do a perfectly adequate job of it on my own. "We aren't necessarily at cross purposes, Sergeant. You want what's best for your men. I'm looking out for the people of Clarion, which includes your men. Leaving things as they are doesn't serve anybody but Petersen, Collins, and Robinson -- and I say bollocks to the lot of 'em."

Renfield did not speak, but the changes in his expression were an eloquent description of his struggle. Petersen and Collins were commissioned officers, part of Renfield's chain of command. As such, they had a responsibility toward the men under them. Robinson was a MP, with duties similar to my own. Facing the fact that they didn't have his best interests at heart, or those of his men, couldn't possibly be easy.

We sat together, waiting for Renfield to decide whether and how to help us as the sun rose and shortened the shadows. Without a word, Mike reached into his pack and offered us a breakfast of protein bars, jerky, and bottled water. I whispered my thanks while accepting my share, but Renfield shook his head. I was halfway through the packet of jerky when he finally spoke. "I can help you, but it will be risky. We have to go back down."

"Back underground?" If Dusk Patrol were smart, they wouldn't all sleep at the same time. Instead, they'd sleep in shifts and keep watch. If they patrolled the underground, instead of simply guarding the barracks, they might find us and wake the rest. "If we're not careful, we're likely to be discovered down there."

"Only if we're noisy. I'll be taking you to a location well away from the barracks. None of us go there, because of the memories. I'd rather not venture down there, but you might be right. Maybe it is time to show the world what our country did to us, even if that country no longer exists. But if the Phoenix Society already has this information, won't they find a way to bury it?"

Good point, and not one for which I had a particularly good answer. But did I actually need one right now? "Let's worry about that later. We should find the truth before we worry about getting it out into the world."

That got a wry smile from Mike. "Keeping your priorities straight?"

"Come on. We don't want to be there long." Renfield spoke with the impatience of a man who had committed himself to an unpleasant task and wanted to see it completed as soon as possible. Without a word, he led us to the infirmary. We followed him into a doctor's private office. 

A dead General Atomic terminal sat forlorn on a desktop cluttered with issues of medical and biological journals like *The Haemostat* and *Organelle*. I remembered the latter term from my studies, and assumed the journal dealt with cellular biology.

Renfield stopped at a door bearing a placard warning that the room beyond contained confidential medical information that should only be accessed by authorized personnel. Whatever was buried down here, it was buried even deeper than the rest of the fort's subterrane. We picked our way down twelve flights of stairs lit only by faint red LEDs, double the flights of stairs we had climbed to reach the surface earlier this morning. At the bottom of the stairs, flanked by empty guard stations, we found a second door bearing the following sign:

> Commonwealth Advanced Research Agency  
> Project Harker  
> Authorized Personnel Only  
> All personnel must submit to search on entry and exit.

Without a word, Renfield opened the door, and stepped back with an ashen face and trembling hands. "I should keep watch outside. There are too many memories in there for me."
