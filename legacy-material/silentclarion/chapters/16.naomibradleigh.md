## Chapter Sixteen

Michael and I got back to Clarion just in time for me to meet Dr. Petersen as promised. When I showed up at his office, the waiting room was empty save for a nurse bustling about. She tended the plants and tidyed up as if she were ready to shut down for the the day. "Excuse me. I was supposed to meet Dr. Petersen."

"You must be Naomi Bradleigh." The nurse spoke in a bored, weary tone as if she had better things to do than deal with me. Hard to blame her after a peek at her workstation; the poor woman must have been on her feet all day. "Dr. Petersen told me to expect you. He had to make a house call, but he'll meet you at the Lonely Mountain when he's done."

Curiosity tempted me to ask where Petersen went for his house call, but I knew better. The nurse would cite confidentiality and tell me to bugger off. Besides, I had a better question in mind. "Who should I ask about the area around Clarion?"

A shrug from the nurse. "Don't know. Don't care. Try Town Hall. They're open till six." She turned her back and lowered her voice, but I still caught the rest. "She's long past due for a husband and some kids to settle down, but city folk recognize no duty to anybody but themselves."

Well, excuse the living shit out of me. I hope for the sake of Dr. Petersen's patients that this nurse was more solicitous on the job, for such surliness could only impede healing. Still, it was up to me to be the better person. "Thanks for your help."

The receptionist at Clarion Town Hall was better; her face lit up with a megawatt smile as soon as the door closed behind me. She was a middle-aged brunette with shoulder-length curls who wore a little cat pin in her lapel. The nameplate on her desk read, C. Gatt. "Excuse me, Ms. Gatt. I was told the Town Hall would be open until six, and I had some questions about the area and its history."

"Of course." She studied me a moment. "Haven't I seen you before? With Kaylee Chambers?"

Her accent surprised me, despite not being especially pronounced. She sounded like she came from an Australian city. Melbourne, perhaps? I offered my hand before she realized I had failed to introduce myself. "You're right. I'm Naomi Bradleigh. I'm on vacation, and arrived yesterday."

"I'm Cat." She stood and shook my hand before stepping out of her cubicle. She turned her nameplate around so that it now read, 'AFK. BRB.' I suspect that meant, "Away from keyboard. Be right back." She saw my interest and smiled. "Present from hubby."

A rock riff emanated from her purse, and Cat retrieved a smartphone. I guess she couldn't afford an implant yet, or didn't want one. Most people were like that, but I like having the tech in my head, running on blood sugar. No batteries, and it's a metabolic boost. "Speak of the devil."

"Go ahead." I checked out a small rack marked 'Visitor Resources' that consisted mainly of pamphlets advertising local businesses while Cat talked with her man. "I'll be home shortly. I just have to help a visitor. Oh, that sounds tasty. No, you can't have me for desert before dinner."

She hung up and joined me. "Sorry about that. Matthew called to ask what I wanted for dinner."

"Sounds like he wanted *you* for dinner." What can I say? I couldn't resist.

Cat gave a throaty chuckle. "He's incorrigible. So, how can I help you?"

I pulled a pamphlet mapping out nearby hiking trails, and pointed at the Old Fort Woods. "I found a pre-Nationfall military installation in these woods. Do you know of anybody who can tell me more about Fort Clarion?"

A frown from Cat as she repeated the name in a mumble wasn't my idea of a good sign. Was there nobody here who knew about that old base? Cat shook her head. "Unfortunately, Ms. Bradleigh, the original town was almost completely destroyed during Nationfall, and most of the information remaining about the old town was targeted at tourists."

She turned her screen toward me to show a pre-Nationfall net archive. She was right; it was all sanitized for visitors. "So, who got the resettlement effort off the ground?"

"The Phoenix Society did, ten years after Nationfall. Our community is composed of families from across the continent. They either wanted to get out of the big cities, or away from villages too small to have a local arts scene like ours. Regionally popular bands like Charn and Keep Firing, Assholes hail from Clarion."

"I remember hearing one of Charn's albums at the Lonely Mountain yesterday. Interesting stuff. So, is there anybody who can tell me more?"

Cat shrugged. "Maybe some of the oldtimers? I think Dr. Petersen would be your best bet."  

Yeah, the one guy I probably shouldn't trust in light of what I heard from Brubaker. I could ask Malkuth, but his answer would probably be that I'm not cleared for that information.

If we ever do go on that date, I'm going to spank him. Three smacks for every time he stonewalls me ought to do, but if he keeps this up I'll buy a whip. Knowing that cheeky bastard of an AI, however, he'd probably smile and say, "Thank you, ma'am. May I have another?"

My amusement at the notion must have shown, because Cat smiled at me. "Something funny?"

"Just thinking of what I might do to a certain cheeky bastard I know."

"Ah." Cat's grin broadened. "Sounds as incorrigible as my husband."

Dr. Petersen beat me to The Lonely Mountain, and waved me over to his table while closing his notebook. "Good evening, Ms. Bradleigh. I trust you had a pleasant hike."

"I did, but now I'm famished." Which was true; the packet of jerky I bought before leaving didn't last all day. "Thanks for asking. How was your house call?"

He shrugged. "Strictly routine. I met the Brubaker boy on the way back. Michael's quite taken with you."

"Don't worry. He isn't my type." Smiling behind the menu, I tried poking at him. "I wouldn't want to break up his arrangement with Jessica Stern."

"That's not on me, Ms. Bradleigh. I only told his parents when he was a child that if he was to marry a local girl, Ms. Stern would be a good genetic match. His parents fixated on the notion, mainly they hope to combine their farm with that of the Sterns."

That didn't square with what Mike told me. Who's lying? Who would benefit most from lying to me, and from my believing the lie? Mike told me his parents are pressuring him to marry Stern because Dr. Petersen suggested it. If they're pressuring him to marry that girl so they can combine the respective families' holdings, that's still an infringement of his rights. Mike has no reason to lie.

What about Petersen? If I believe he's just a helpful country doctor providing genetic counseling to help keep the local population from getting inbred, that leaves me with a weaker case for investigating him in detail. No matter. His involvement with the Commonwealth Army during Nationfall is reason enough to dig deeper.

"-have someone?" That was the only part of Dr. Petersen's remark I caught, but it caught my attention.

"Excuse me?" A glance at the stage, where a band called Keep Firing, Assholes was starting their sound check, gave me a handy excuse. "I didn't quite catch that."

Petersen nodded. "The question was none of my business. Instead, allow me to extend an offer. Clarion has a substantial population of individuals with CPMD. If you like, I can test your genome and suggest individuals with whom you might like to start an acquaintance."

Ballsy, but subtle. Normally I like that in a man, but I'll be damned if I'll consider a man on the basis of genetic compatibility. I'm not livestock. "Thanks, but I'm not looking for a relationship at the moment. I just ended one."

"Fair enough." Petersen rose, dropped some banknotes on the table to cover his tab, and extended a hand. "I must take my leave now, Ms. Bradleigh."

"Not going to stay for the band?" Petersen pressed something into my hand as he shook it. A note? Hmm.

"Rock was never my bag, but I would love to hear you play sometime." With that, Petersen left. Something about his shoes caught my eye as he threaded his way through the crowd gathering for the band. They weren't shoes, but *boots*, and still had forest dirt caked in soles.

Now, what kind of doctor wears combat boots, and goes tramping through the woods? Was he bullshitting me about that housecall? And what did he slip me before we left? Unfolding the paper, I smoothed it on the table:

 > Fort Clarion is dangerous. Keep your distance.

Dangerous? Sure. I can believe that, but if Dr. Petersen thinks I'm going to keep my distance after he slipped me a note that might as well be a threat, he should talk to a colleague about getting his head examined.

Still, how did he know I had been to Fort Clarion? Could *Mike* have said something to Petersen? Time to check. [You there, Mike?]

It was about a minute before I got a reply. [What's up?]

[Did you say anything to Dr. Petersen about our hike? He says he met you on the way back from a house call.]

One drawback to secure talk is the pauses, especially if the guy on the other end uses a handheld. It wasn't so bad between implants, but the lag as Mike typed his responses into his handheld made the conversation interminable. [Sorry, Naomi. I didn't think we had to keep it on the down low. I told Doc we went out for a hike, and that we found this weird old army base in the Old Fort Woods. That's all.]

A modicum of foresight on my part might have prevented this. I might have told Mike to meet me *in* the woods at a predetermined latitude and longitude away from the town. I might have told him ahead of time to keep the details of our hike to himself. Hell, I might have told him to lie and say we had ourselves a shag by the river, or seduced him and made the lie true.

Instead, like a demon-ridden idiot, I acted like a tourist instead of an Adversary and gave no thought to operational security. Well, no more mistakes from here. I can't afford it. [Mike, we have to be more careful from now on. Petersen is not to be trusted.]
