## Chapter Eight

Despite my resolve, I was unable to leave London for several days. Not only was I obliged to wait for the Phoenix Society's official determination that I had acted in self-defense when fighting those off-duty MEPOL officers, but the transit union strike spread globally. To top everything off, my period proved painful enough to prompt a visit to my gynecologist, who removed my IUD for safety's sake. I'm glad she did; it turned out the device began to degrade abnormally early.

I booked tickets for the first maglev to New York, packed my bag, and took it easy for a few days. Jacqueline and some of my artsy friends came to visit, and we put on an impromptu, gender-swapped production of that Scottish play with me as the usurper and Jackie as Macduff. We performed in front of my building, made a hell of a racket, and had too much fun to give a damn.

Jackie came with me to Victoria Station the next morning, since the transit strike was finally over. "You sure you're going to be OK in New York, Nims?"

"I went to school there, remember?" In fact, I went to two schools. I balanced my time between the Juilliard Conservatory and ACS, where I received edited accounts of the exploits of early Adversaries like Iris Deschat for inspiration. Instead of asking Jackie if Director Chattan shared any juicy stories with her, I patted the hilt of my sword. "I'll be fine."

"All right. Just don't do anything I wouldn't do." Jackie winked at me, knowing damn well I was far more restrained.

For example, I caught Jackie and her vicar boyfriend in my kitchen sharing a three-way kiss with the actor who played the role traditionally given to Lady Macbeth in our little production. "Considering what I saw last night, your admonition gives me a great deal of latitude."

"Yeah, sorry about that. We were all a bit drunk."

I shrugged, not about to admit I lay awake imagining two men showering their attentions on me because of the scene I witnessed. "I didn't mind. It's not like I found you three in my bed."

Jackie smiled. "We were tempted, but I figured you wouldn't appreciate it."

"Thanks for being the voice of reason."

"You're welcome." Jackie glanced behind us, and her eyes widened. She grabbed my arm. "Holy shit. Don't look now, Nims."

I ignored her advice, and looked toward the entrance. Oh, damn. John was here with his fiancée, and some slag who seemed to be hounding them. Was she paparazzi? "Jackie, let's leave them alone."

"Hell no!" She pulled harder, dragging me along until we blocked John's path. She gave John a slow, cynical once-over before turning to me. "I can't believe you settled for this."

"I'm sorry, Naomi. I didn't think I'd meet you here." John seemed to be trying to forestall a response to Jacqueline's remark. "I suppose I should introduce you. This is my fiancée, Christine Pennington. Christine, this is --"

I flashed a smile at Jacqueline and offered Christine my hand. "I'm the other woman."

John's expression was priceless, and Christine stared at me as if unsure of what to make of me. "I beg your pardon? Did you just imply that John cheated on me?"

Jacqueline studied Christine a moment, as if deciding whether she deserved an explanation. "John took you for a test drive while still in a relationship with my friend. As far as we're concerned, you're the other woman, but Naomi's trying to be gracious."

John spread his hands, as if appealing for mercy. "Ladies, I hardly think this is appropriate."

"Shut up. I want to hear this." Christine turned back to me, ignoring her fiancé. "Is your friend telling the truth, Ms. --"

"Adversary Naomi Bradleigh." I offered my hand again, and this time Christine shook it. "Unfortunately, Jacqueline told the truth. John and I had been dating for a little over a year when he met you. After deciding you would prove tolerable as a wife, he came to me.

"Unaware of his arrangement with you, I let him into my bed. Afterward, he asked me if I wanted to be his mistress, whereupon I learned he agreed to marry you, and had already been with you."

Christine's head took on a slight tilt as she considered my explanation. For some reason she reminded me of an actress from an Jane Austen adaptation. "So, let me see if I understood. He cheated on me with you, but was also cheating on you with me."

Christine smiled at me before catching John by his collar, kissing him, and using her knee to ram his balls back up into his belly. He crumpled to the floor, his breathless sobs barely audible, as she ripped the engagement ring from her finger and dropped it on him. "I'd be within my rights to keep this, but I want nothing of yours."

"Oh, this is perfect." I turned, and found the woman who had been stalking John and Christine earlier. She was petite, with bleached blonde hair and a black suit. "Here's an opportunistic young Adversary, Naomi Bradleigh, breaking up an engagement between her betters. Tell me, Adversary, do you enjoy this bit of revenge on those with breeding and privilege you can never hope to attain?"

Jackie came to my aid again. "First off, bitch, Nims wanted to leave them alone. I dragged her over here. Second, who the fuck are you?"

"Oh, I'm sorry. I'm Alice Talbot, from the London Social Register. And you must be Adversary Jacqueline Russo. What's it like, shagging a vicar who would rather get it up the arse?"

"Don't knock it till you've tried it."

Talbot flashed a sly smile before turning to Christine. "Ms. Pennington, can you offer some insight into what it's like to realize your husband-to-be kept a mistress with CPMD from you?"

"I've no idea what you're on about." Christine turned to me and Jackie. "You two are Adversaries, are you not? Would you care to remind Ms. Talbot of our right to privacy?"

Jackie glanced at me. "I'm off-duty, and you're supposed to be on leave."

I let my sword-hand hover over the hilt, as if I were ready to draw. "That just means I need not waste time on niceties like due process. Go chase the March Hare, Alice. I hear he and the Mad Hatter take turns servicing the Queen of Hearts. Surely that's the sort of high-society gossip your readers crave."

We let Talbot mull that over, and escaped her by ducking into a café. Christine was kind enough to do the buying. We chatted until the station AI pinged me. "Adversary Bradleigh; the Tradewinds Atlantic Express is now boarding."

As I rose to take my leave, Christine offered Jacqueline and me her card. I glanced at it before slipping it into a pocket. "What manner of antiques are your specialty?"

"Weapons." Christine glanced at my sword while Jackie ducked into the ladies'. "Is that a Nakajima SDL Mark One?"

"I doubt it. It's a custom model." I never thought my weapon might merit her interest, but to humor her I drew the blade to display the maker's mark. The elegantly rendered column of script read, "Forged for Naomi Bradleigh by Nakajima Kaoru".

"It's beautiful." The reverence in Christine's voice surprised me. "Do you use this blade on duty?"

I shrugged. "Of course."

"I suppose I should have expected that answer." The reverence left her voice. I sheathed my blade as Jacqueline returned. "Nobody wears a sword they're not prepared to use in a fight. If you come across another piece, however..."

I flashed back to Maestro's rapier, still hidden in my closet. "I'll be sure to keep you in mind, Christine. Thank you for the coffee, but I should go."

I boarded my maglev after giving Jacqueline a parting hug, and stowed my bag in the semi-private compartment's overhead storage rack with plenty of time to spare. I settled into one of the plush leather seats, and was about to crack open a paperback I grabbed from the station's lending rack when a girl's voice startled me. "Holy shit, mom. It's Cecilia Harvey from Last Reverie!"

I glanced at the aisle, and saw an auburn-haired girl of about eight staring into my compartment. She wore a bomber jacket over a purple dress speckled with white stars and little black ankle boots. She had a plush Programmer Cat nestled in the crook of her arm. Her mother finished stowing the bag in her hands and looked out from the compartment opposite mine. "Claire, it isn't polite to stare."

"It's fine, ma'am." I scooted over to Claire. "My younger brother is a Last Reverie fan. He tells me Cecilia's a brave knight who loves her king, rescues him time and time again, and--"

"No spoilers!" Claire covered her ears and stomped her foot. "It's not fair. I didn't get to play enough of the game to see any of that for myself."

God, she sounded like Nathan when I managed to read an installment of _The Continuing Misadventures of Programmer Cat_ before he could. "I'm sorry, Claire. I didn't realize."

Claire continued to pout until her mother intervened. "Claire, the nice lady apologized. What do we say?"

She sniffled, and looked at her mother before turning back to me. "I'm sorry, too. Fuckdammit, that was rude of me." She brightened a bit. "Oh, bollocks. I didn't even ask your name."

I offered the salty-tongued little fangirl my hand. "I'm Naomi Bradleigh. Keep this to yourself, but I'm actually an Adversary. I snuck out so I could have a holiday." Claire perked up, and tugged her mother's arm. "Holy shitballs, Mom. She really is a knight."

Her mother sighed. "I'm sorry. I keep trying to teach Claire to watch her mouth. I just can't explain where she gets it."

"Your little girl reminds me of a friend of mine."

"Are her tits as big as yours?"

I smiled at Claire's long-suffering mother as she sighed and shook her head. If Claire was this bawdy as a little girl, I doubt her parents looked forward to her adolescence. Even if they could find a nunnery in which to confine her, I suspect she'd corrupt even the most devoted by sheer force of will and personality. "It's fine, ma'am. I'm not offended."

She smiled at me, and offered her hand. "I'm Lucy Ashecroft. I suppose this'll be a long trip."

I shook Lucy's hand before glancing at Claire. She had settled beside me with a laptop to play what appeared to be a game of global thermonuclear war. I hoped it was just a primitive simulation. The last thing I needed was for New York to not be there when we arrived.

