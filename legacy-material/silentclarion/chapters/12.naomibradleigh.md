## Chapter Twelve

Homesickness struck with the force of a fist to the gut as Kaylee and I arrived at The Lonely Mountain on the other side of Clarion's main street. A hand-painted wooden sign swayed in the breeze, depicting a single mountain against a far horizon. Underneath, the sign read "B. & D. Halford, Proprietors".

The building resembled a traditional English pub in almost every detail. A wrought iron fence surrounded a quaint beer garden abuzz with bees competing with a riot of butterflies for nectar. A flagstone path led from the open gates to a round door. There was even a sign in the window nearest the door advertising rooms to let.

I wondered if the phrasing confused any of the locals as the door closed behind Kaylee and me in a soft clangor of bells. A stereo played some kind of mellow progressive rock. The barkeep reached for a pint and began filling it as we approached the bar. I guess she texted ahead. "Here's your usual, Kaylee. What can I get your friend?"

"We just met, so I have no idea what Naomi drinks. I don't even know how long she's going to be in town." Kaylee drank a third of her pint in one go, and smacked her lips. "Damn, Bruce, I needed that."

I claimed a stool next to Kaylee. The singer was singing something about gold everywhere he turned. "A glass of your house red, please. What do you have playing?"

"The band's called Charn." The bartender opened a fresh bottle, and put out a dish of mixed nuts. "Kaylee said you're new to Clarion."

I sipped my wine before answering. "I just rode into town for a holiday, Mr. Halford. I understand you've rooms available."

Halford nodded, and quoted the rate. "How long did you plan to stay?"

I pulled my wallet, and placed a quantity of banknotes sufficient to cover a week's lodging on the bar. "I think this will cover me for a week. Can we talk extensions if I need to stay longer?"

"Of course." Halford counted the cash, and nodded. "Just a moment, please."

He soon returned with a receipt and a key on a numbered fob, which I promptly pocketed. "Anything else I can do for you ladies?"

Kaylee nodded. "How about dinner?"

I scanned the common room, and found a table by the window that afforded a good view of the street while also letting me observe the patrons. "Mind if we sit by the window?"

"Go ahead." Halford grabbed a couple of menus and followed us. A huge Irish wolfhound lifted his head, giving us a forlorn look as we passed the hearth. He whined softly, begging to be rescued from the two black kittens draped contentedly across his back.

As we ate, Kaylee regaled me with stories about the townspeople, starting with our host. It seems Bruce Halford referreed a weekly Catacombs & Chimeras game every Sunday here in the common room while his husband took a turn behind the bar. In return, I told her about London and life as an Adversary. By the time Bruce came by with the dessert menu, I was convinced I had made a friend here in town.

Kaylee studied me a moment, her fork poised over her slice of steaming apple pie as I sipped my coffee. "You sure you don't want a piece? Dick Halford makes a great apple pie."

"I really shouldn't. I'll only regret it later."

"Raw deal." Kaylee pointed with her fork at a gaunt gentleman wearing a white lab coat over his shirt and waistcoat. While he might have been a scientist or some sort of technician, his almost military bearing led me to peg him as a physician. "That's Dr. Petersen. I heard he was one of the first to move back to Clarion after Nationfall. If anybody knows where the bodies are buried, it's probably him."

"Why do you say that?"

Kaylee leaned close. "I think he dug a lot of the graves. He used to be in the Commonwealth Army. Now he runs a family practice when not serving as coroner and medical examiner."

Now *that* was odd. Why would he return to an empty town to practice medicine? Did he serve nearby during Nationfall? I can understand a former North American Commonwealth soldier, possibly a medic, who runs a family practice. But in London, medical examiners must possess specialized qualifications in forensic pathology. Would that be the case here? Either way, Dr. Petersen was number one with a bullet on my list of people to chat up. "What else can you tell me about him?"

"He goes bow-hunting with Sheriff Robinson and Mayor Cooper every fall." Kaylee gave me a suspicious look. "Are you on the job?"

Shit. That's what I get for not quitting while I'm ahead. I've got to be more careful if I don't want a jury wondering why I overstepped my currently non-existent authority. "I'm on leave, but while I was in New York, I overheard a woman who had been here. She mentioned disappearances, and I got curious. Do you know anything?"

"There was that lady who was all over the news, but she eventually turned up. Her boyfriend didn't, though. Fuckin' shame, that. They were going to get married." Kaylee's face scrunched as she tried to think of something else. "And every now and then some dumbass kid ignores warnings to stay out of the Fort Woods, and doesn't come back when he said he would. We send out a search party, and find 'em half the time. You're a city girl, so you should understand that sometimes people just disappear."

Kaylee's right. Sometimes people do just disappear, but there's usually a reason: somebody *made* them disappear. Did somebody make the kids that couldn't be found disappear? If so, then who? How? Why?

I didn't quite catch what Kaylee said. "What was that?"

"I said, there goes Dr. Petersen now."

I put some banknotes on the table. "Sorry to run out, but this is too good an opportunity. This should cover the check, with a tip. Is there a back door?"

"You're going after Petersen?" Kaylee pointed the way instead of waiting for me to answer. I ignored a drunken catcall and plunged into cool autumn night. An alley ran parallel to Main Street, allowing me to keep pace with Dr. Petersen without getting too close.

We walked across town before the alley curved and brought me back to Main Street. Now seemed as good a time to cross Dr. Petersen's path as any, so I approached him. Because I tend to walk silently and thus sneak up on people without intending to do so, I sang softly to alert Petersen to my presence. I just wanted to talk to the man, not put a shank in his back.

"You've a lovely voice, young lady." Petersen turned to me with a confident smile. "I saw you with Ms. Chambers at the Lonely Mountain. Have you been following me?"

"Kaylee told me you were the man to meet if I was curious about Clarion's history."

"Are you curious, Miss --?"

"Bradleigh. Naomi Bradleigh." No point in denying him my name when my appearance precludes anonymity. Now that I had a good look at his face, I used my implant to search for records. Turns out Kaylee was right about him serving in the North American Commonwealth's army, but his service record is sealed by order of the Phoenix Society. All I got was name, rank, and serial number. This shit just keeps getting weirder. "And you're Dr. Henrik Petersen. Or should I address you as Colonel Petersen?"

"'Doctor' is fine, Adversary Bradleigh." He flashed a knowing smile. "I couldn't help but run a search on your name and face. No doubt you did the same with me. Am I the subject of an investigation?"

I shook my head. Dammit, I should have expected he'd search me. And if he's Sheriff Robinson's buddy, I'll probably get to meet him soon, too. "You aren't. I just heard some odd rumors about Clarion, and got curious enough to visit."

"Hmm." Dr. Petersen glanced northward, as if thinking of something in the forest beyond. "What manner of odd rumors?"

"Disappearances. Apparently a couple got lost recently, and only the woman got out safely. Nobody knows what became of her fiance."

"It was quite the tragedy. I treated the young lady in question for exposure and malnutrition." Petersen pushed his glasses up his nose as he spoke. "I'm sorry we weren't able to find her young man. Do you have a young man, Ms. Bradleigh?"

"A few." I lied, because hearing such a question at night on an empty street creeped me out. Let Dr. Petersen think what he will, as long as he doesn't think me easy prey who might vanish without notice. I ran a fingertip down his chest to further disconcert him. "But there's much to be said for experience and maturity, is there not?"

"At my age I think you'd be the death of me, Ms. Bradleigh." His eyes crinkled as he smiled at my flirtation. He checked his watch before withdrawing the keys to his practice. Did he live above his office? "Would you come by tomorrow afternoon for coffee? I think I'd enjoy your company."

Sure. Why not? I'll drink the old man's coffee and pick his brain. Maybe poke around his files while he's in the loo if I can get away with it. A high-ranking soldier with a service record sealed by order of the Phoenix Society had to have a few skeletons in his closet. "Is five o'clock convenient?"

"Perfectly." The foyer light came to life as he opened the door. "Good night, Ms. Bradleigh."