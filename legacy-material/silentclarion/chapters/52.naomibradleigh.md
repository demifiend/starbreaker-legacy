## Chapter Fifty-Two

Stunned by Petersen's words, I couldn't help but stare at him a moment. "Are you absolutely sure you want to confess? **You wont be doing yourself any favors and if you had an attorney present they would be telling you to keep your gob shut.** If you had an attorney present, and they were at all competent, they'd be yelling at you to shut your bloody gob right now. The Society might use your words as evidence against Robinson if you implicate him, but they won't seek a more lenient sentence for you because you did their work for them."

Mike gave me an exasperated look. "**Why are you giving him advise? He doesn't....lawyer. Don't...**He doesn't have a fuckin' lawyer, so why are you giving him advice? Don't you *want* him to prove his own guilt?"

Before I could answer, Mike got up and took a position by the stairs. If somebody came down, he'd be at risk, but it was also a good place to ambush intruders. **Line is a little oxy-moronic may just need tweaking. eg. ...stairs. Despite the dangers of such an open position, he prepared to deal a lethal blow against whoever discovered our position first.**

Petersen chuckled. "**playing devil's advocate, adversary? Do you doubt the system you serve that much? Or do you wish to ease any guilt you might feel?** It seems, Adversary, that you don't trust your own superiors to do right any more than I trusted mine at the end."

The doctor was right. **Devil's Advocate indeed. In my duty to prevent the abuse of power, wasn't it right that I start by questioning my own. As to doubt....** My superiors do appear to have mistaken me for some exotic species of mushroom **of late**, given their **continued efforts to keep me in the dark. Not to mention the sheer plethora of b/s being dealt out by all those around me.**recent tendency to keep me in the dark and feed me bullshit. And if Petersen had contacts in the Society who have made it their business to hinder me, I had all the more cause for distrust. Unfortunately, Mike's objection wasn't wholly unreasonable. Duty demanded that I prove Petersen guilty **remove next 3 words** if I could. In the meantime Petersen was innocent until proven guilty **remove to [and] I'd be remiss....(however make that internal - see further changes below)**in a court of law, and I was obligated to honor *his* rights in the interim.

My position was hardly enviable, but I had nobody to blame but myself. Nothing for it but to do my job. "Dr. Petersen, are you *sure*? **Move prev question to end of next sentance**Mike's right, but I'd be remiss if I didn't offer a final warning. **Start dialog here. "Dr P. a y s?**Everything you tell me will be recorded and used against you in court."

"And there's nothing I can say to save myself, **remove remaining dialog. Doubt the 5th would save him with all the other evidence** except to take the Fifth." Petersen gave a small, bitter laugh. "Good thing for you I'm not interested in saving my own hide. What I want is a deal **for my men**. Let me take full responsibility for everything that has happened in Clarion. My men were only following orders."

"I think that's the first time I've seen somebody invoke the Nuremberg Defense on their subordinates' behalf."

Petersen shook his head. "It's called command responsibility. If I was merely aware of atrocities committed by my men, and did nothing, I would still be criminally liable under the Medina standard. But I ordered my men to survive, conceal their existence, and protect Fort Clarion by any means necessary. Moreover, I may be the most senior officer involved in Project Harker who's still alive. Somebody has to be held accountable. I'm an old man, so it might as well be me."

Because I agreed with his reasoning, **Remove prev line**I refrained from mentioning the fate the Phoenix Society meted out to other war criminals who escaped justice until old age. If Petersen knew he might be condemned to involuntary rejuvenation *before* serving a life sentence, and face a century in prison instead of the five or ten years he expects to serve before dying, it might break his resolve. **Justice could be a right bitch and **Despite my duty to respect the rights of an individual not yet proven guilty **in a court of law**, the last thing I wanted was for Petersen to reconsider confession. **Remove remaining line.**In fact, I said nothing at all.

Petersen did not speak again for several minutes, and we sat in silence as he composed his thoughts. "When Dr. Ian Malkin of the AsgarTech Corporation first approached me about testing Dusk Patrol for what he called the 'asura potential', I was delighted. Dusk Patrol was a joint innovation on my part and that of Sergeant Major Renfield, a unit composed entirely of CPMD-positive soldiers who would train to enhance the capabilities unique to them and develop tactics that would exploit their strengths to achieve decisive victories.

"Despite the unit's proven effectiveness, my superiors were concerned because the root of their *espirit de corps* was what separated them from the rest of humanity. The brass was concerned about the potential for separatist sentiment to take root in the Army, beginning with the unit, but couldn't order it disbanded because news of their successes had already made it to the President. So they tried to do the next best thing and get the unit's members killed"

"You figured Dusk Patrol's luck would eventually run out, and wanted to improve their chances?" It seemed a reasonable question given what Petersen had told me thus far. 

Petersen nodded. "Exactly. When Dr. Malkin showed up and started talking about the men having some kind of 'asura potential', I recognized the opportunity before us."

Recalling Petersen's suggestion that Malkin approach the upper brass first, so that the idea would seem to come above the colonel's pay grade, I gave him a gentle prod. "But you must have known that **if** it all went wrong, the men would turn against you along with the **use diff term since brass used above - your superiors** brass. You'd be caught in the middle."

"Which was why I suggested Malkin pitch it to General Quinn. The men already disliked her, and I thought to use that to further harden them."

"So, Project Harker happened, but you weren't part of the research team. Why was that?"

Petersen shrugged. "I began my medical training once Project Harker got rolling, but did not complete it until after Nationfall. By that point, I might argue that the damage was done."

So he might, only the damage continued afterward. "Why order the men to remain in hiding after Nationfall?"

"Dr. Malkin ultimately viewed Project Harker as a failure, for reasons he never shared with me, and I was afraid the men would not be able to rejoin society unless I could find a way to reverse the changes wrought upon them."

Unable to help myself, I shook my head in disbelief. "You thought you could undo an experimental treatment designed to evoke some kind of asura potential that you've yet to describe or explain? How?"

"I had to start by understanding the asura potential. To do so, I persuaded my contacts in the Phoenix Society to back the resettlement of Clarion. I soon determined by comparing the men of Dusk Patrol with the CPMD-negative majority of the new population that the asura potential is tied to CPMD."

 "Are you telling me that everybody who's CPMD-positive has this asura potential?"

"Everybody I've tested. Even you. It varies between individuals, however, and follows a normal distribution. It's a heritable trait, and a child of two high-potential parents will most likely have a high potential themselves. Yours is two standard deviations above the mean, if you care. I was able to test you when you came in to have that wound to **on - too many to's** your side treated."

Between this and my red eyes marking me as some kind of half-demonic hybrid **see suggestion. Don't think she knows anything about her eyes meaning**, I was really starting to feel like some kind of freak. **Wasn't that just freaking great. As if my unique looks didn't already make dating a challenge. At this point I wouldn't have been surprised if he claimed my red kitty eyes marked me as a demonic hybrid. ** "I haven't had time to fully examine your research. What does having a high asura potential mean?"

"Good question. Those with the highest potentials were best suited to the treatments developed by Project Harker. However, they were also prone to certain side effects and exhibited unusual phenomena."

"Such as?"

Petersen glanced around, as if checking for additional observers, and leaned forward. "One subject broke free of his restraints **not sure if remaining sentence is needed it almost distracts from the true outcome** while repeatedly screaming, '*laissez-faire*'. When orderlies tried to subdue the subject, they couldn't get within a meter of him. They insisted some kind of barrier kept them from getting closer. The subject died soon afterward of starvation. His body had somehow consumed itself while producing the barrier phenomenon. Furthermore, cellular analysis revealed anomalies in the subject's mitochondrial DNA."

So, the test subjects had wonky mitochondria? **Science lesson feels wrong. How about.. The animation shown in ACS?? biology/anatomy class to explain the how mitochondria were the little critters....into**That didn't sound good. Mitochondria are the little critters inside our cells that convert nutrients into **skip to - the basic** adenosine triphosphate, the basic fuel on which our cells depend **came to mind**. Without them and other features of eukaryotic cells, complex multicellular life probably wouldn't exist. "What sort of anomalies? Did the subjects suffer from some sort of metabolic disorder?"

Petersen raised an eyebrow at my question, but also smiled. "Excellent question, but the subjects' medical records contained no indication of metabolic problems. Moreover**however**, the **their**mDNA contained genes for neurotransmitter receptors, which were activated in the mitochrondria within the subjects brain and nerve cells."

"Are you telling me these people **they**had brains capable of direct mitochondrial control?" **Following line doesn't make sense. Can possibly skip to Suppose..**And would be the benefit if this were the case? Suppose I could command the mitochondria in my brain cells to double or triple their normal energy production. Would that alone let me think faster? "What good is that?"

"I don't know, but there was some connection between the neural-mitochondrial link and the psychokinetic phenomena some of the Project Harker subjects exhibited." Petersen fell silent for a long moment, and seemed to study me. "When I tested you, I not only checked for asura potential, but for this other trait. You possess both. Under the proper circumstances, you too could exhibit paranormal abilities."

That's a bloody cheerful thought. If pushed hard enough, I might go full Carrie and then die once my body has consumed itself to power whatever ability I end up manifesting. What good is *that*? "I appreciate the warning, doctor, but we're off on a tangent again. This isn't about me. This is about your involvement in Project Harker and subsequent unethical research. I take it you engaged in breeding experiments involving the local CPMD-positive population to further your understanding of the asura potential, and longitudinal studies without obtaining informed consent."

"I did. I performed the same experiments on the CPMD-negative population as both a control and a cover. Since I did it to everybody, I could sell it as free genetic counseling."

A snarl and a sudden thud against a table pulled my attention to Mike, who had slammed down the book he had been reading. **Thought he was watching the door?** pointed an accusing finger at Dr. Petersen. "I knew it. I fucking knew it. Do you have any fucking idea how much misery you caused, you lying sack of shit?"

Despite Mike's angry display, Dr. Petersen remained calm, his voice **typo??**drily amused. "Considering that the people of Clarion tend to come to me for mental health referrals, I know full well how much misery I caused. It is one of the reasons I am here, explaining myself to your new friend."

Thank God he didn't say 'new girlfriend'. It probably isn't easy to maintain control of an interrogation when you're blushing as deep a red as your eyes.

As if guessing at my thoughts, Petersen flashed a smile at me before continuing. "However, I experimented on regular humans for another reason. Sheriff Robinson was blackmailing me, as I mentioned before. In exchange for his silence, I had to find a way to transfer the asura potential to humans and safely activate it. I needed to ensure the gene therapy didn't cross the Weismann barrier and affect the germ-line as well as somatic DNA." He paused, and pride brightened his expression. "I succeeded, as you no doubt learned from your encounter with the late Mayor Collins."

If Mike and I hadn't already faced the augmented Mayor Collins,**Remove start of line** if not for the crazy shit we had already seen, I would dismissed Dr. Petersen's claims as the posturing of an old failure. It was tempting to do it anyway, because of the implications. If Petersen had done it to Collins, then he could have done it to others. He might even have done it to himself.

Before I could question the old doctor further, a booming thud echoed from the door at the top of the stairs. Mike turned **toward the CCTV displays (since he was watching the door)**away from the CCTV displays. "Naomi, we got a problem."