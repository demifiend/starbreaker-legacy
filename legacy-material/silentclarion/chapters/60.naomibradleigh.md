## Chapter Sixty

The last time I woke up with a headache like this, there was a foul taste best left undefined in my mouth and a certainty in my mind that Jacqueline was somehow to blame. This time, I wasn't quite sure what had happened. There were hazy memories of a duel of attrition, a wounded young man I should have done a better job of protecting, and the almost overwhelming certainty that my death and that of thousands of others would rain from beyond the sky. 

Had I taken a blow to the head? It scared me that I couldn't quite remember, nor understand why I was here—wherever here was.

A soft white light flared to life above me, and showed that my world consisted of gleaming black. The walls, ceiling, furnishings, fixtures, and even the bedding were an unremitting ebony. Somebody had even taken my clothes and weapons, and left me glad in black pajamas.

At least the floor wasn't too cold against my bare feet; somebody had thought to provide pajamas, but no socks or slippers. Furthermore, the cell wasn't as cramped as it could be at five by five meters, with a ceiling three meters from the floor. There was room for me to exercise, so that was how I started. I still wasn't clear on how I'd escape, but I knew I needed to be strong if I hoped to make the attempt.

Once I had finished my PT, it was time to take further stock of my surroundings Despite my implant, I couldn't reach the network. There wasn't even a GPS reading that would give me a clue as to my location—save that I was either underground or in a Faraday cell. Quite possibly both.

All I knew was that several days had passed since my arrest. Was Mike Brubaker safe? What about Christopher Renfield and the remnants of Dusk Patrol? Were they here with me, in other cells? What did the Phoenix Society gain from keeping me here? All I had were questions, solitude, and entirely too much time on my hands. Time to reach out.

"Hello? Is anybody out there?" It would have reassured me to hear a guard outside, even if their sole response was a barked order to shut the fuck up.

Instead, there was only silence. Even my own voice seemed strangely muted, as if my cell's acoustics had been designed to dampen all sound. If I screamed, would anybody outside hear me?

Not realizing it, I had begun to pace as if trying to outrun my thoughts. That wouldn't do at all. PT was one thing, but wearing myself out would be counterproductive.

The question was, what should I do? My captors had not even thought to provide a selection of books I might read to while away the time. Such much for humane treatment while in custody. Oh, wait. That had been one of the charges against me: that I had subjected Dr. Petersen to inhumane treatment.

Never mind that he wasn't my prisoner at the time, or that he had come within a fine hair of wiping Clarion of the map. Because I had placed him under arrest, I was responsible for the bastard. I should have just blown his head off, and then worried about stopping GUNGNIR. At least then I wouldn't have to worry about him being outside, free, and laughing his sick fucking arse off at me.

Just thinking about it frustrated me. Without realizing it, I had hit the wall with the heel of my hand. The impact sent waves radiating outward through the material from where I had struck my blow.

Now, *that* was strange. I've never heard of a solid material that showed the force of an impact radiating out from where it had been struck. With a shout I tried the wall again, lashing out with a kick. Same effect, only the waves spread out farther this time.

Sitting in the black chair that constituted one of the three pieces of furniture my cell contained—the third being a table, naturally—I tried to blank my mind, breathe, and just *be*. It was a technique we Adversaries were taught, to stop trying to direct one's reason and just let it join intuition and go where it would. It sometimes led in directions one might not consciously consider because they seemed too ridiculous.

And what I was thinking now is whatever property of the material lining the walls and ceiling of my cell that allowed it to visibly react to the force of my blows *also* gave it some kind acoustic damping ability. Sound was force, after all, force expressed as vibration conveyed through a medium.

Taking a breath, I stood and let my voice fill the cell at concert pitch. The walls and ceiling became a rippling gray they absorbed the power I projected into a clear steady tone with which one could tune just about any instrument. Letting the note fade, I watched the walls settle back into their original gleaming black. 

Speaking hadn't produced the same effect as singing a sustained concert A, which was strange. Stranger still was the idea born of this result. If I worked my way through my entire range, might I find a note capable of shattering the walls hemming me in if sung long enough with sufficient power?

Lacking anything more interesting to do, I ventured the experiment, starting with the B just below middle C and working my way up in semitones as if practicing a chromatic scale. The effect I had seen before become more pronounced as I progressed toward the upper limit of my performance range, until I was sure that my cell would crumble around me as I hit the highest note I had ever managed while performing.

But as I let my voice fade into silence, I remained a prisoner. It was obvious that it would take a note higher than any I had sung before to shatter these walls. If I wanted out, I would have to break free of my own limitations first.

Beginning to sing again, I catapulted myself to the top of my range as if trying to  ram a locked door with my shoulder to break it down. Every time I failed, anger grew hotter within me as I readied myself for another attempt. My rage fed upon itself, a wrathful chain reaction that drove me to push despite my voice growing hoarse from overuse.

Even as my voice threatened to crack I continued to try. It was insane, *I* was insane, but something about this cell felt so profoundly wrong that I couldn't bear to spend a single night here. It was bad enough I had woken up here after God alone knows how long. My need to get out had eclipsed reason, and would consume me if it went unmet any longer.

Frightened that I might gain my freedom only by losing my voice, I forced myself to stop. Running the tap, I tasted the barest sip of cold water from a cupped hand and waited. 

If there was something in the water—a sedative, perhaps—I hoped my precaution would result in me getting too small a dose to put me out. When nothing happened after fifteen minutes, I began drinking from the tap in greedy double handfuls that cooled my parched throat.

Once I was full I settled back into my chair to think. Perhaps with training and hours of practice I might extend my range by a few notes, but I wasn't back at Juilliard and didn't have the time. It wasn't rational, but I felt like my very life depended on me getting out of here as soon as possible.

For lack of anything better to do, I tried my voice. Singing softly, without projecting, I settled into one of my favorite arias from *The Witches' Flute*. Though it wasn't necessarily appropriate to the setting in which I found myself, I loved the Queen of the Night's role, which sounded all the more aggressive in the original German.

> The vengeance of Hell burns my heart
> Death and despair blaze before me!
> If Sarastro feels not at your hands
> The agony of death
> Then you shall be my daughter nevermore
> Disowned may you be forever
> Abandoned may you be forever
> Destroyed be forever
> All the bonds of nature
> If not by your sword
> Sarastro breathes his last!
> Hear me, Gods of Vengeance
> Hear this mother's oath!

The walls surrounding me had one use. The rippling they produced as they dampened my voice showed that it wasn't just my imagination. They proved I still had my voice. Perhaps, if I could put enough power into my voice, I might be audible outside. Surely a guard would eventually pass by and hear me.

Resolved to reach through my prison and touch somebody, I renewed my song. Working my way through every aria I ever memorized, I poured myself into a succession of roles. I was Titania, Queen Elizabeth, Cleopatra, Lady Macbeth, Lucia Lammermoor, and dozens of other operatic heroines.

Lost in song, I lost all awareness of anything but the music and my desperation to be heard by just one person. To be heard, to reach just one person, to pierce the armor of reason and habit and social convention, and strike directly at their emotions—that was all any artist wanted. But unlike many artists, my life depended on it. That much was certain.