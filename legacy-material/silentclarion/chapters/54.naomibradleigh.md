## Chapter Fifty-Four

It would have been nice to have ridden the rest of the way to Fort Clarion in comfort, but that's not how matters worked out. With just over a kilometer left to go if my estimate of the distance had been correct, the jeep ran out of fuel. The engine sputtered, running on the last of the fumes in the tank as Dr. Petersen shifted to neutral. We coasted to a dead stop, rolling farther than I expected.

Straining my ears in the sudden silence, I heard nothing, but that didn't mean that Sheriff Robinson and his men weren't still behind us. ⎡Think you can manage the rest of the way on foot, Doc?⎦

⎡Doesn't look like I've got a choice. Got a plan for when we get there?⎦

⎡Aside from keeping us all alive?⎦ It didn't seem wise to plan in any greater detail than that. Besides, the less I told Petersen, the harder it would be for him to betray me if he had the opportunity and inclination. ⎡Leave the headlights on, and let's go.⎦

The jeep lit our path as we resumed our journey on foot. The white radiance of its headlights slowly attenuated with each step, allowing us to gently re-acclimate to the dark. It was better than being plunged into darkness and having to waste time standing around blinking like grotesque flightless owls. With any luck, the jeep would also slow down Robinson and the others by causing them to suspect an ambush where none existed. Why should I be the only one looking over my shoulder?

We soon reached the other end, which was what I assumed the cold steel shutters guarded. I crouched down to grasp the handle, so I could try lifting the shutters, but they began to rise of their own accord. They rose quickly, and lights from the other side blinded me for a moment. Even as the sudden brightness overwhelmed me, I raised my rifle.

"Damn, Naomi. It's just me."

I knew that voice, but what the hell was *he* doing here? "Renfield? Is that you?"

"Sorry about the sudden light. Let's see if this helps." 

Before I could say anything, his lips were on mine. He certainly *kissed* like Renfield, but this was neither the time nor the place. I took a step back, and raised my rifle again. "How the hell did you get down here ahead of us? Why aren't you with Robinson?"

"I broke out of jail. Saw the commotion in town, but didn't see you, and figured you found our tunnel. Nice of you to look out for the old man, by the way." He saluted Dr. Petersen. "I was against having you eliminated, Colonel, but Robinson insisted, and most of the guys would rather follow him and Corporal Seward. They're tired of living underground."

Petersen returned the salute. "Thanks, Sergeant." He looked backward, into the dark distance whence we came. "Hard to blame the others. I should have helped you come back to the world sooner, but I -"

He was still talking, or at least trying to, but no words came out. Instead, blood poured from between his fingers as clawed at his own throat. The whipcrack of a gunshot soon followed. Shielding Mike with my own body, I yelled at Renfield. "Move your fucking arse!"

He moved, but carried Petersen's corpse with him. Not that I could fault him. I wouldn't leave a fallen Adversary behind, even getting them off the battlefield cost me my own life. 

The shutter rolled back down as Renfield pounded a button set into the wall. Despite his burden, he led us up the stairs and into Fort Clarion's underground. Encouraging Mike to stay close to Renfield, I trailed behind to guard their backs. At each corner we turned, I stayed behind a moment to ambush any pursuit, but none followed. 

Had the shutter stopped them? How long would it remain an obstacle to Robinson and his followers? Whatever the answer, it wouldn't be long enough.

When I caught up with the guys, they had placed Petersen's body in the meat locker where Renfield had left me hanging no more than a couple of nights ago. Somewhere along the way they found a body bag and stuffed him inside. Mike waited by the body, but Renfield was nowhere to be found. "How long have you been alone?"

Mike shrugged. "Only a couple of minutes. Renfield said something about finding some stuff he took from you."

Renfield soon returned with a bag over his shoulder and two swords stuck in his belt. Lowering the bag to the floor, he pulled the long sword from his belt and held it out to me. It was one of the blades Nakajima lent me. "I found the gear I took off you before. If you're going to fight Dusk Patrol, I figured you'd want it back."

"Not going to stop me? I might have to kill some of them."

Renfield shook his head. "Fuck 'em. This is war, and when they killed Petersen they killed our best shot at going back to the world. Did you know he was going to confess, and take the fall for everything?"

Though I could have told Renfield that Project Harker had practically been Petersen's idea from the start, and that Dusk Patrol's subsequent isolation had also been a further betrayal on their leader's part, I couldn't see the point in doing so. What would Renfield have done with the knowledge, since his former commanding officer was beyond all confrontation? It seemed better to let him believe the colonel had been looking out for his men to the end. "He did confess, and claimed command responsibility."

Renfield nodded, and prodded the bag. "Got your armor in here."

"Thanks for keeping it safe." Not sure what good it would do me, though. Though the turtleneck I wore under my jacket was thin, the gauntlet I tried on wouldn't lock into place. The catches wouldn't engage. "Shit. I probably need that damn undersuit Nakajima included."

Mike tried on the gauntlet himself; it wasn't a perfect fit because his hand was a bit too small, but it wouldn't snap shut for him, either. "Must be a security feature."

"The hell with it." After packing the useless armor away and securing it for retrieval later, I tried one of the Japanese swords Renfield had returned. The draw was as smooth as my usual sword, so I slung my sidesword across my back and belted the katana at my hip along with its shorter companion sword. I'd be at a disadvantage because I didn't regularly train with these blades, but if we could find a safer place I might be able to get in a few minutes of practice. It would be better than nothing. "Renfield, what's the most defensible location on the base?"

Seeing him take time to think it over made me nervous. A soldier like him should be ready to answer this question before I was finished asking it. Shouldn't he? After another moment, he shrugged. "Your best bet is one of the watchtowers. It won't give you much of an advantage, but you'll have elevation, a wide view, and the main approach is a narrow stairwell."

Seriously? We were up there only a few hours ago. But if we had stayed, I wouldn't have gotten Dr. Petersen's confession. Regardless, I might have acquiesced for lack of a better idea if Mike had not voiced his own objection. "Wouldn't they expect us to use conventional tactics and take refuge in the tower?"

Renfield glared at him. "You got a better idea, kid?"

"Yeah. We're outnumbered, and can't afford to face Robinson and your buddies head-on. If we stay in one place, they'll surround us. We have to stay mobile."

"How?" Mike had a good point, but even if I called my motorcycle back to me, I wasn't trained for mounted combat, and my swords probably weren't intended for use in such conditions.

Renfield scratched his head a moment. "Shit. We don't have fuel for any of the vehicles."

Nor did we have time to piss about. If Robinson and his men hadn't gotten past that shutter in the tunnel, I suspected they would soon. "Mike, I want you in that tower with a rifle and plenty of ammo. You can cover me while I get in Dusk Patrol's faces and keep the bastards busy. Aim for center mass, though. We're just trying to hold out until the morning."

Mike nodded. "What about Renfield?"

Good question. If I pitted him directly against his brothers, would he fight, or would brotherhood outweigh his anger over Dr. Petersen's death? Better to avoid the question if I can. "Sergeant, I won't ask you to fight Dusk Patrol beside me. Instead, can I count on you to watch Mike's back?"

Renfield gave me a long, calculating look. "You realize you're leaving your witness alone with me, right? I could take him out, and then shoot you from the tower the way Petersen wanted from the beginning."

He could, but I don't think his pride as a soldier would permit it. Right now, we share a common mission. Renfield won't do anything to fuck it up, but all bets are off afterward. "You won't do anything of the sort until after we've nailed Robinson. It would be unprofessional."

While I waited for him to think over what I said, I followed Mike's lead and checked my rifle. Since the magazine only had a few rounds left, I swapped it out for a fresh one. Last thing I needed was to run out of ammo right after the shooting starts. It would be embarrassing. "Well, Sergeant? Can Mike and I depend on you for now?"

Renfield nodded. "Yeah, but when this is over we're going to talk. "

Before I could tell Renfield it was a date, a rattle of steel came from the stairway down the hall that we had used to escape the tunnel from Clarion. Shouted orders to advance in pairs told me everything I needed to know. "They're here."