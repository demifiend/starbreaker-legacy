## Chapter Twenty-Two

"I saw how you handled Mayor Collins." Saul Rosenbaum chewed his cigar a moment before continuing. If he was going to chew me out at any point during my daily report, this would be the time. "The word 'imperious' comes to mind."

"You should be used to it, since you served with Iris Deschat."

The old Director puffed his cigar . "Iris *was* just like that back in the Navy, especially if someone denied her personnel or materiel she needed to carry out her mission."

A proud-looking woman Saul's age glided into the office behind him and rested a hand on his shoulder. She still wore an Adversary's smallsword on her hip. "So, Saul, you finally decided to throw me over for a younger woman?"

"Ma'am, it's not like that." Why did I have to say that? Their relationship was none of *my* business, was it?

An indulgent smile made her grandmotherly instead of commanding for just a moment. "Don't worry, Adversary Bradleigh. It's how Saul and I flirt. You'll keep that to yourself, of course."

"Of course. Director Rosenbaum, do you have any further questions?"

"No. Be careful out there. We need more evidence before we can justify sending more Adversaries to Clarion." The screen in my room went dark as Saul disconnected.

With my report to the New York chapter complete, I was done for the night. The sensible thing to do would have been to get out of these clothes and into bed.

Unfortunately, I was too restless to be sensible. Where were Fort Clarion's inhabitants? Do they hide during the day? Did they hide to avoid meeting me and the crew I brought with me today? Was Chris Renfield among them? The questions chased their tails as I sat at my desk.

Fuck it. It's not like I don't know the way to Fort Clarion. With my implant to guide me, finding the place in the dark should be no problem.

Equipping myself with my sword and a small emergency kit, I slipped past a small crowd that had gathered to watch a rather spirited Catacombs & Chimeras campaign. Kaylee busily refereed the game, gleefully using imagination and dice rolls to challenge the players gathered around her.

It was a clear, starry night with a waxing gibbous moon to light my way to the Old Fort Woods. My eyes soon adjusted, though I checked the documentation for my implant's low-light enhancement functions anyway. It would be darker in the forest.

Whipporwills advertised their presence all around me as I found a small clearing in the forest. A rabbit stood on its hind feet, staring at me. I ducked as I heard a hoot and a soft flutter behind me, and a great horned owl swooped down over my head. It plucked the rabbit from the ground, its talons digging deep into the fur to secure its grip. Dinner to go. **fine**
 
A coyote howled as I continued through the forest, and I regretted lacking sufficient woodcraft to determine if the beast was alone and far off. The howl went unanswered, which presented its own problems; a lone coyote might be rabid. However, it wasn't the coyote I needed to fear tonight. **fine**

Had he been with me, Mike might have noticed the signs and guided me away from the cougar's den. Instead, I stumbled upon it. Worse, the den contained a litter of spotted kittens waiting for their mother to bring back a kill.

Backing away from the den to avoid disturbing it, I attempted to skirt it. Unfortunately, it was too late. I had already drawn the mother's attention, though she didn't immediately attack.  **fine**

Instead, she must have stalked me. I lowered my guard a couple of kilometers from the den, which I marked on my virtual map so I could avoid it on the way back. That was when she pounced. **fine**

She would have had me if not for sheer dumbass luck. I heard leaves rustle behind me, and turned while unclipping my sword from its belt so I could defend myself with the sheathed weapon. I saw the cougar then, her sleek body gathered for the spring that would have let her slam into my back and drive me into the dirt while she fastened her jaws on my neck and snapped my spine. **fine**

I overrode the training that told me to make myself a smaller target. That training was for human opponents wielding weapons, not for big cats protecting their young. Instead, I wanted to make myself as huge and threatening as possible if the information I pulled with my implant was reliable. Instead of shouting, however, I used my training as a dramatic coloratura soprano and projected the full force of my voice into a sustained high F that I hoped would drive off the puma.

Instead, she sprang at me. I stilled my voice and spun aside, hitting the cougar with my reinforced sheath as I did so. The improvised weapon struck with a sharp crack, and the cat shook her head as she landed and turned to face me.

"You pussy! Is that all you've got?" This time I shouted at the cat, advancing upon it with my arms spread wide. "You already took your best shot, and you blew it. Bugger off!" **fine**

The cougar shrank back again, snarling at me, and sprang a second time. I dodged again, my sheath striking as I did so. "Bad kitty!" **fine**

She sprang again, but instead of leaping for my throat she swiped at my legs with one of her massive forepaws. She caught me just above the top of my boot, one of her claws tearing my jeans and ripping my shin. Enraged by the pain, I rained blows on the animal with my sheathed sword.

A strike across the cougar's nose drove her back as she snarled in pain. I hurled my voice at her again, shrilling a high, clear tone to frighten her. Raising my weapon again I made to advance. She backed away several steps before bolting into the underbrush. I waited a minute, only to tense again as I heard someone clapping.

"I'm not going to pounce on you." Christopher Renfield approached slowly, showing me his empty hands. **fine**

Slipping my sword into my belt, I took several deep breaths, regaining my calm. "You startled me. Who the hell expects applause after fighting off a cougar?"

"Sorry. I was just impressed by how you handled the situation." **fine**

At least I was able to defend myself without killing that cat. Not that I'd admit it to Renfield. "Hello again, Sergeant. How did you find me?" Whether he's friend or foe remains unclear.

His teeth flashed in the starlight. "I followed a high, clear note that pierced the night. A woman's song made a battle cry. Was it yours, Naomi?" He had drawn closer to me as he spoke, and whispered my name in my ear.

The surge of energy that had filled me faded away with the adrenaline from the fight. The pain of my wound and relief all but turned my legs to jelly. Was I really wired enough to flee my bed for a midnight walk before? It's hard to believe, because right now I wanted nothing more than to clean the wound on my leg, bind it, and curl up in bed, but I didn't dare let myself pass out here. It wasn't safe. "That was me. Can you give me a little space? The cat got in a good swipe at my legs."

Renfield nodded, and pointed towards a large boulder just the right height to sit on. "Would you like some help?"

My teeth began to chatter as I tried to answer, and I couldn't get any words out. Renfield led me to the rock, and sat down with me. "Was that your first time fighting, Ms. Bradleigh?"

The question and Renfield's arm around me helped me focus. "Believe it or not, it isn't."

Renfield's arm tightened around my shoulders. "You did fine. You're alive, and so's the cougar. But let's get a little fire going."

Some primal instinct of mine agreed that a fire would be nice, but I couldn't help but be curious. "Why a fire, Sergeant? I just want to check my leg, and continue on my way."

"And where are you going?" Before I could answer, he brushed a soft kiss against my cheek. "We'll talk more in a bit."

As he left to gather brush, I drew my sword just in case. Its edge gleamed in the moonlight, and it wasn't long before my hand was strong and steady again.

Renfield returned first with deadwood, and put it aside to dig a firepit. Once he was satisfied with his digging, he arranged the firewood and ringed the pit with stones before starting the fire. "That's better."

"Thanks." Sheathing my sword, I took off my boot. I tried rolling up the leg of my jeans, but the cut was too close to my knee. If I had a knife, I could just cut the leg off these jeans, but all I've got is my sword. "I'm sorry, Sergeant, but I need to take off my jeans."

Renfield turned his back. "Just give the word when you're done."

"Thanks." I shimmied out of my jeans, and had a look at my shin. The cougar scratch was wider than it was deep. Blood seeped from the wound, and the skin around it was swollen and inflamed. It's a bloody good thing for that damn cougar that she has kittens.

"How bad is it?"

"It's inflammed, but not deep. Bloody sore, though, and the muscle's already a bit stiff." I cleaned the wound before applying an analgesic salve containing medical nanotech that cleared out pathogens and sealed wounds. "Damn, this stuff works fast."

"What is it?"

"Just a standard-issue first aid gel. I've never had to use it before." Unfortunately, the wound was too big for any of the bandaids in the kit. "Can you help me wrap this up?"

Renfield glanced over his shoulder. "You sure?"

"Quite." I began wrapping the gauze around my leg, and held it in place once I was done. "I need an extra pair of hands."

Renfield knelt before me, resolutely keeping his eyes on my face. "What do you need me to do?"

"Cut the gauze, and tape it up. I'm not sure I can do it myself without it all unravelling."

Drawing a knife from his belt, Renfield carefully sliced the gauze where I indicated. He then taped me up, using just enough to keep my leg wrapped. Once he was done, he gazed up at me with a small, playful smile curving his lips. "Want me to kiss it better?"

Nice of him to ask first. Remembering how his lips seared mine the first time we met, I reached down and ran my fingers through his crew cut. "You're welcome to try, Sergeant."