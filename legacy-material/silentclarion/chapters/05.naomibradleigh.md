## Chapter Five

My steps felt lighter as I left Director Chattan's office. I checked the time, found it was after one in the afternoon, and decided to finish out the day. Though I had no outstanding cases, I was confident I'd find some reason to stick around. Perhaps Jacqueline was free to spar with me.

She found me first, and sounded breathless. I had to glare at a few of the men as they leered at Jacqueline in her sweat-soaked training clothes. She opened her mouth to speak, but I shook my head, and led her to a bench to make her sit down. "Get your breath back, first."

Her breathing soon eased. "I just sparred with your Maestro. Bulsara, Kilminster, and Langton were with me, and he kicked our arses four against one."

"That sounds like par for the course." I ducked into the kitchen and poured a glass of water for Jacqueline. She gulped down half. "Whose idea was it to gang up on him?"

"His." Jacqueline took another sip. "He wants you now, Nims. Says you're the only one here who's worth a damn. Probably because you handle a sword just like he does."

"I wanted a reason to stick around and finish my shift, but one of Maestro's fencing lessons wasn't what I had in mind."

A sudden impish smile curved Jacqueline's lips as she punched my shoulder. "Well, given how well he handles a sword, maybe you could take him somewhere private and see how he handles his gun."

"Jacqueline!" I pretended to be shocked. She'd teased me about Maestro all through ACS. She suggest he might be my father, because of our snow-blonde hair and a slight facial resemblance, when not suggesting I seduce him. I normally enjoy competent men, but taking on Maestro felt like a bad idea. "Given he can show up at will and disrupt schedules without repercussions, he probably answers directly to somebody on the Executive Council."

"Assuming he's not XC himself." Jacqueline kept her voice low. Nobody knew who sat on the Executive Council, and speculation as to their identities was a game our immediate superiors discouraged. God itself could hold a seat, and our mission would still be the same Jeffersonian quest: eternal hostility against every form of tyranny over the human mind.

Nor did it particularly matter who Maestro really was. He showed up when he felt like it, and taught me technique I couldn't learn elsewhere. Though his appearance today was most likely a coincidence, I couldn't shake the intuition that it wasn't. "Will you be all right, Jackie?"

"Yeah." She sounded much better already. "I just need a shower. Did you get leave?"

I nodded. "Two months off. I could have left, but it was already one in the afternoon."

"Right. Pretend Maestro's your ex and beat his ass into the ground. Come see me after. I'll get some of the lads together and we'll have ourselves a pub crawl to see you off."

I tried to refrain from groaning, and failed. "The last time I let you take me on a pub crawl, I ended up in bed with one of those people who insist people with CPMD are a different species from humanity and should do their best to outbreed homo sapiens."

"Yeah, but wasn't he good in bed?"

I shrugged. I did rave about him to Jacqueline, but that was before I learned about his separatist politics. "He was fine as long as he used his mouth for something besides talking."

Jacqueline got up and clapped my back. "See? Nothing wrong with a bit of meaningless, drunken sex. Go see what Maestro wants, and I'll get you hooked up tonight."

I think I managed to refrain from rolling my eyes. Jacqueline's a good friend, and I trust her to watch my back. Regardless, her teasing about my love life isn't what I need right now.

I quickly changed into my training clothes, but didn't bother to trade my sword for a practice weapon. Maestro favors live steel. He thinks people learn faster when a mistake means hospitalization.

He saluted me with his own blade as I entered the salle. He was as I remembered him: slightly taller than me, with oceanic blue eyes, and long snow-blond hair bound into a tail with a blue ribbon. Instead of training clothes, he wore a white double-breasted suit with a shirt open at the throat and a blue ascot. I've never seen him sweat. "What kept you, Adversary Bradleigh?"

I returned his salute, and rolled my shoulders to loosen up. "You play rough with my friends when I'm not around, Maestro."

"Your partner has a head for tactics." Maestro's sword flashed beneath the florescent lights with each practice cut. "She let the men grab my attention, and tried to strike from behind."

I began to circle around him, keeping my body behind my sword to offer as small a target as possible. "Did she succeed?"

"You wound me, young lady." He lashed out with his blade, his slash flowing into a lunge meant to pierce my breast.

I was already elsewhere, responding to his sally with a slash to distract him while I danced inside his guard. I tried a left hook, but he ducked it while forcing me to leap backward to avoid an ankle sweep that would have taken my legs out from under me. "I've yet to do anything of the kind, sir."

"You disappoint me, but less so than in the beginning." The point of Maestro's blade caught my vision for a second, stealing my focus.

Had I remained distracted a moment longer he would have had me. Instead, I side-stepped and took the offensive. I led with my sword, hoping to trap him, but he did not oblige me. My training before him left me accustomed to a minuet of ringing blades.

Maestro's way is to deny my steel the touch of his own. If his blade threatened to touch mine, he would withdraw his sword or flow around mine. He fought as if we held liquid swords, blades too insubstantial to be parried.

Tension of a sort held us together in this martial dance. He led, always half a step beyond me. I followed, forever confident that this time I would catch up with him and land a blow. I never let him touch me, but he always remained beyond my grasp in turn. It was thus whenever we met on a practice floor. This was the manner in which he taught me the sword. He did not simply impart his knowledge to me; he forced me to take it from him at swordpoint to preserve my own life.

"I almost had you."

I must have been lost in thought, for I never became aware of the blow. I must have seen Maestro's sword regardless, and danced beyond its reach without conscious intent. I let the flow embrace me, rather than redoubling my effort. Our tempo grew ever faster, until I felt the cold bite of steel against my throat. Maestro's grip on his blade as such that it did not draw blood as I spoke. "You defeated me."

"A Pyrrhic victory at best, my dear." I blinked, and realized I held my sword at his throat. We withdrew together, and sheathed our blades before he spoke again. "I can teach you nothing more."

I imagined mastery would feel less anticlimactic. "Are you sure? I only managed to fight you to a draw. Wouldn't a clear victory be better proof that I learned all you can offer?"

Maestro shook his head, and to my surprise came to me and tousled my hair as if I were his daughter. "I taught you everything I ever learned myself, Naomi. If you defeat me, it will be with knowledge I do not yet possess." His lips were dry and warm against my forehead. "I have done all I can for you. You need not fear those possessed of sufficient temerity to defy you."

Perhaps it was his archaic phrasing, but I believed him. "Will we see each other again?"

Maestro smiled then, but only with his mouth. His eyes remained cool and remote as deep ocean. "We might. I dare not say more than that for your sake."

The words seemed to pain him. I wanted to say something, perhaps ask him what he meant, but the sight of him unbuckling his sword-belt halted my tongue. He offered me the weapon, cradling it in outstretched hands. I reached for it, and hesitated. "I shouldn't."

"I insist." His tone was far more commanding than his words, and I lifted the weapon from his hands. "Examine the blade."

I drew enough of it to get a good look, and nearly dropped it. "Is this what I think it is?"

Maestro nodded. "True Damascus steel. I cannot prove that famous hands ever wielded it, but you might someday change that."

I stared at the rippling bands frozen within the steel, all but hypnotized by the history in my hands. "Even if I dared accept such a valuable gift, I wouldn't dare fight with such a sword. It belongs in a museum, where its beauty can be appreciated."

Rather than take back the sword, Maestro guided my hands until it was again sheathed. "It's yours now. Use it as you will, either to defend others' liberties or reclaim your own."

Before I could protest, he was gone. I left the training room rather than stand there alone, and found Jacqueline waiting outside. "Jackie, did you see Maestro leave?"

She looked at me as if I'd gone round the bend. "You didn't leave him inside, Nims?"

"No." I shook my head. "Let me drop this off at home, and then it's time for that pub crawl. I think I need to get thoroughly pissed."

