## Chapter Thirty-Seven

**I uncurled myself from the chair on which I had spent the night and stretched. Kaylee was exactly as I had left her, spread across my bed like a drunken sailor, snoring softly. - Remove next 2 lines** I had to shove Kaylee aside to get into bed last night. She was there the next morning, snoring softly. She was *still* there when I came out of the bathroom after my shower **Remove remaining as this is a logical choice**, but fortunately I thought to bring a fresh camisole and panties into the loo with me. Once I had finished dressing, I tried giving Kaylee a poke to wake her.

She curled up in a fetal position, hugging her pillow close, and snarling in drunken English. "Fuggoff."

"It's eight thirty in the morning. I have to get moving. Shouldn't you be opening your shop?"

Her reply was less intelligible, but I managed. **Having retrieved Jacqueline from her fair share of epicly excessive pub crawls gave me plenty of ....."drunklish"**My obligation as Jacqueline's friend and partner to retrieve her from several excessive pub crawls and get her safely home gave me plenty of practice in understanding Drunklish. "You fuggin' nutsh? Ish fuggin' **remove fuggin'** Shundy. Fuggoff arreddy."

"Fine, but I'd better not come back and find you dead because you choked on your own puke. You hear?" Trust me; that was the *last* thing I needed. **Remove remaining line, not needed**Not that she was likely to do a Bon Scott lying on her side.

"Yeah yeah yeah. Fuggoff an' lemme shleep."

Before I fucked off, I stopped at the bar for a quick word with Dick Halford. "Dick, Kaylee **remove last name pretty sure he knows her** Chambers is sleeping it off in my room. Can you please check on her later?"

"Sure. Think she might want breakfast?"

Never mind Kaylee, *I* wanted breakfast. "Probably **No rush. You may want to take up some asprin and coffee while your at it. Any chance I can get my usual?** something light for her, but you know what I like."

I took my time eating. It was Sunday, after all, and I doubted Ernest Yoder was likely to go anywhere, whether he was still holed up at home or had in fact disappeared. Hopefully he hadn't suffered a similar fate to Scott Wilson or Charles Foster.

In any case, I would need a warrant **or two before tackling that particular mystery, not to mention** before confronting Matt Tricklebank, Cat's Unix-guru husband. Opening a secure talk session with Malkuth, I sent him an IP address. 『Malkuth, I need a warrant to search the machine at this IP.』

『Can't you just use HermitCrab to penetrate it?』

『The sysadmin is one of the HermitCrab developers. He detected my penetration attempt and clamped down.』

『You should have bought him a few drinks first to loosen him up. Maybe light some candles and put on some soft music. Gotta set the mood, you know?』

Oh, great. Now he's joking about buggery. Where does he get this shit? Jacqueline, I suppose. She's a pernicious influence. 『Malkuth, please at least try to pretend to take this seriously. **It's possible we have a third victim, Ernest Yoder. It is also possible that he may have been the first to die. Nobody looked into it sooner because of his reputation for reclusiveness. How the hell did these kids come to the attention of Dusk Patrol? Why would they go after somebody that rarely left his fortress of solitude?** I've reported two murders thus far with the same method. Dusk Patrol survivors from Fort Clarion are involved. The victims have a friend, Ernest Yoder, who may have disappeared prior to their murders. Nobody has remarked on Yoder's disappearance because of his reputation for reclusiveness.』

『And you figure he's more sociable on the network than in person? He isn't there. Neither are Wilson, Foster, Brubaker, or your new girlfriend.』**fine**

My new *what*? **remove next line**He'd better not mean Kaylee. 『How do you know? Did you access the forum's database?』

『Hell no. I crawled the forum's pages and pulled the user list and everybody's profiles. User IDs are standard first initial and last name. There aren't any of the more fanciful handles you'd find on a board frequented by unsupervised young people. In fact, nobody under thirty uses this board.』**fine**

『Even though it's supposed to be open to everybody living in Clarion?』**fine**

『Would you frequent the same forums as your parents if you were a kid?』**fine**

『Of course not. I'd want somewhere more private, away from prying eyes and bots.』 **Is the web so different from what it is today? Why aren't they just on Fark or some other forum site? Next line has to go.** But where would they put it? Would they have done what we did at ACS? Would they have possessed the technical skill to do so? **Really? This should be a deliberate point back to Matthew. Especially the next one regarding sys admin**
**Next paragraph is like a techy wanking session. While I am all for some tech talk this may be a little much for non-tech readers. Last line should go at least as it is unnecessary**
Network forums are normally served over hypertext transfer protocol, which is sent over port 80 by default. The official forum for ACS cadets was no different, but the sysadmin running the forum also ran a separate, unmonitored server on one of the high-numbered ports allocated for custom and ephemeral connections. If memory serves, the alternate forum listened on port 65535. 

It was like one of those fight clubs mentioned in urban legends. Nobody talked about it, but if you were smart enough to run a port scan and find it on your own, you were welcome. 『Malkuth, can you run a port scan on that server and see if there's another HTTP daemon listening on a non-standard port?』

『Found one. Want me to crawl it?』

『No. Do I have sufficient probable cause for a warrant giving me authority to examine that forum with administrative privileges along with the underlying server and filesystem?』

『No dice, Nims, but I can get you an entry warrant for Ernest Yoder's digs.』 True to Malkuth's word, the access code for my warrant came through. If I wanted, I could print it myself. Otherwise, I'd just give the code to **think "anybody" needs to go. Who would she give it to if he didn't have a landlord?** anybody the landlord and let them download it themselves. 『Anything else I can do for you?』

『I know better than to answer that. Thanks, Mal.』

As a courtesy to Sheriff Robinson, I sent the code for my warrant to his office along with a quick note **remove remaining line** on my way to his apartment on the other side. He had a right to know I meant to kick down Yoder's door and poke around.

Most of the shops were closed, and the streets were empty; I suppose most of the townspeople were in one of the **since it is a small town maybe half a dozen or more generic "one of the"** dozen churches lining Main Street. **The doors opened on the closest church, idicating that service had just finished**Services had just finished at the **closest**last of these, and **Start next line here**the pastor **milled through her flock giving them a final blessing.....** was out front, giving her flock a final blessing before they returned to their secular lives. She sighted on me and pressed a booklet into my hands. "Good morning, young lady. **May God bless you on this beautiful day and may he send you our way next Sunday to join us for mass. - Get rid of the next line.**Won't you please consider visiting next week to learn the truth about Jesus of Nazareth?"

**Remove line. I think you already provided Nim's views in a prev chapter.** A thousand objections sprang to mind, foremost among them the fact that a person as important as Jesus somehow managed to escape the notice of just about every major Roman historian at the time. However, **Keeping my objections to myself, I smiled at the grandmotherly pastor and .... **I kept them to myself and checked out the booklet I had received. The grandmotherly pastor was only trying to be friendly, and **Remove start of line**a church that handed out Jefferson Bibles would most likely go easy on the hellfire and brimstone. "Thank you for inviting me, Reverend. I'll consider it."

Since she was holding an offering plate, and the church looked like it could use a fresh coat of paint, I found a crumpled five-milligram banknote in my pocket and forked it over. The pastor seemed shocked by my generosity, and I immediately regretted the gesture. It revealed me as a soft touch. **I don't like the last line and a half but I will let it go. But I think it likely paints Naomi in more of a Morgan personna.**

Reaching Yoder's home without further incident, I knocked on the door to the first-floor apartment on the assumption that the owner would have kept it for themselves while making tenants take the stairs. Am unshaved middle-aged man in stained overalls and a white t-shirt opened the door. "I don't have any places for rent." 

"I suppose you're the owner."

"Yeah." Glancing at my hands, his expression hardened. "**I don't need whatever your selling but I've got a message you can pass onto the Almighty**Then God must have sent you to tell me about he loves me and wants me to be happy. I got a message you can pass him."

It must be that booklet I still held in my hand. **Remove prev. Thrusting the small bible into my pocket, ....**Thrusting it in my pocket, I decided against a hard approach. His reaction suggested I wasn't the first to offer the cold consolation of religion. "**I can assure you that I am not here as an envoy of God. But if your problem is earthbound I may be able to provide assistance. -- Remove remaining**I'm not sure he'll listen to me, but I'll take your message."

"**Can you bring my wife back? - Having her approach this as an adversary will provide a stronger chapter and shouldn't need too much changing**'I want my wife back, you son of a bitch.' That's my message." Unshed tears glistened in his hard eyes as he spat the words at me.

His barely-restrained anger wasn't specifically for me; I was just handy, somebody who had made the mistake of offering to listen. But there had to be a reason for his pain, and talking him through it might simplify my mission. "Do you want to tell me what happened to your wife, sir? Would it help if I listened to you? No judgment, no Bible talk. I promise."

**Thomas Wesker**He was sobbing by the time he had finished his story, which had a familiar ring. Three years ago he and his wife, Thomas Wesker and **Remove last 3 words**Emily Yount, moved to Clarion. They were a pair of artistic newlyweds who had scraped together enough capital to buy property in town instead of spending the money on a fancy wedding or an extravagant honeymoon. Thomas and Emily were happy together for six months, until they set out with a basket full of food and some blankets for an afternoon of lovemaking in the woods. Thomas dozed after loving his Emily, and woke up alone.

He finished his story with his hands in mine. "**There was no trace of her. The cops couldn't....**We couldn't even find Emily's body. Three years later, here I am. **I know nothing more than I did that day.**I suppose you think I'm pathetic."

Though I was unused to sight of men weeping, I think Thomas had earned the right to a good cry. Something about his manner suggested he never properly grieved for his loss, and his pain had become cancerous. "Not at all, Mr. Wesker."

His tone sharpened a bit. "Not going to tell me to man up and get over it?"

"I'm not a therapist making a housecall, but I still know better than to impugn your masculinity. It would just put you on the defensive, which is counterproductive."

"You certainly *sound* like a shrink."

"It's the training**.** I'm Adversary Naomi Bradleigh, and I've got a warrant authorizing me to enter Ernest Yoder's apartment to verify the occupant's safety or disappearance. **Look I can't guarentee anything but I will see what I can find out about your wife's disappearance. But first I really do need to check in on Mr Yoder.**"

Wesker nodded. "Is this one of those new-fangled digital warrants where you give me a code to download?"

Looks like he would have preferred paper. "It is. I apologize for the inconvenience. Can you provide a network address?"

"Yeah." Once I had it, I sent Thomas the warrant ID. "Just give me a minute, Adversary. Yoder's late paying his rent anyway."

"Is he habitually late with his rent?" If not, then that was a bad sign. **Remove last line. A bit obvious**

Thomas shook his head. "No. That's what I don't get. He's usually a model tenant. But he's late with his rent, and there's this *smell* coming from upstairs. I should have gone up there sooner, but Ernest isn't quite right because of what happened with his parents." He cocked his head and studied me a moment. "You know what happened to him?"

He was right. There *was* a stink of rot in the air here, but I couldn't pinpoint its source. "Enough to realize that if he's still alive, he isn't going to appreciate my presence in what had previously been his one refuge from the half of the human race that scares him as much as he scares himself."

Thomas glanced at me, a smirk curving his thin lips. "You sure you're not a shrink?"

"Quite." Stopping at the third-floor landing, I began taking shallow breaths through my mouth. The moist stink of corruption was stronger here than it had been in front of Wesker's apartment. "Is this Yoder's place?"

"Yeah." He cycled through the keys on his ring until he found the right one. "Do you want me to go in first, ma'am? In case it's bad in there?"

**I think this could be less cynical. Thomas' offer was apprehensive but well intended. The look in his eyes let me know that the situation had atleast presently pulled him away from his own pain. For this reason alone it was unfortunate that I would have to turn him down.**Thomas' offer was no surprise. He had cried in front of me, and now he was trying to reclaim his manhood through. It was charming, but now I had to turn him down without hurting his pride. "I appreciate it, Mr. Wesker, but you're a civilian. If it's as bad as the smell suggests, you might unwittingly compromise the crime scene."

Inserting the key, he unlocked the door and backed away. "There you go, Adversary. Good luck."
