# Silent Clarion

a *Starbreaker* novel by Matthew Graybosch

## Also by Matthew Graybosch

**Starbreaker**

* *Without Bloodshed* (2013)

**Serials**

* *Silent Clarion*, Episode 1: "The Geographic Cure"
* *Silent Clarion*, Episode 2: "Always the Quiet Ones"
* *Silent Clarion*, Episode 3: "Life is Short and Love is Over"
* *Silent Clarion*, Episode 4: "Death in a Northern Town"
* *Silent Clarion*, Episode 5: “Hard Places and Other Rocks”
* *Silent Clarion*, Episode 6: “Rainchecks for Ragnarok”

## Short Works

* "The Milgram Battery" (in *Curiosity Quills: Primetime*)
* "Limited Liability" (in *Curiosity Quills: Chronology*)


## Disclaimer

The following is a work of fiction. Any views or opinions expressed by the characters in this work are strictly their own, and do not necessarily reflect the views of the author or the publisher. 

Any resemblance or similarities between the characters in this novel to living or dead persons in this world or any parallel world within the known multiverse is either a coincidence; an allusion to real, alternate, invented, or occult history; or a parody. If you find any allegory or applicability, please consult a qualified professional for psychiatric evaluation and treatment. 

The actions depicted in this novel are performed by trained professionals. Attempting them in real life may result in personal injury, injury to others, property damage, civil or criminal penalties, and premature mortality. In other words: *Don’t try this at home, kids.*

## Author’s Note

This novel contains not only spoken dialogue, but dialogue transmitted over text messaging. To distinguish the two, I use corner bracket marks commonly used as quotes in East Asian languages like Japanese and Chinese for the latter instead of standard quotation marks, so that a text message will look like this:「This is text dialogue.」

Other authors facing similar issues in their own work are welcome to use the same method. The Unicode representations for these characters are U+300C and U+301C.

## Dedication

For Catherine, purr usual. Thanks for hitting me upside the head every time I don't quite get the character right.

## Track 01: Nemesea — “No More”

London can be a cold city, and my duties as an Adversary often demand that I face it at her coldest. Not that it bothers me. It only makes my nights hotter by comparison.

I expected to find John asleep after finishing my shower. In his last year of residency at an Ohrmazd Medical Group hospital, by necessity he often dozed after loving me. I wholeheartedly encouraged him to. Tired people err, and in our lines of work errors cost lives.

Instead, I found him stretched across the bed naked and reading a medical journal. I sat on the edge of the bed beside him, and dragged my fingertip down his spine to make him shiver.

He rolled and smiled up at me. "What were you singing in there, Naomi? I heard something about someone named Sophia."

"Just another gnostic metal band I recently discovered. Lucifer Invictus. They did a show with Seiten Taisei last week." Since I finally prevailed upon him to come to my flat after our date, I could show him the record instead of just pulling up a digital recording like I usually did when I wanted to share new music with him. I wasn't about to bring vinyl records to the hotels John usually picked for our trysts he still lived in the family home, since that also required dragging the player along.

We listened together as I dried my hair. As I began to comb it, John took the comb and began working out the tangles for me. He was less patient than I, but would stop and kiss my ears if he heard me growl in pain. He was fond of my ears, which were pointed like a cat's, covered in white fur that blended with my hair, and lay flat against my skull, pointing backward.

When he was done, I pushed him down on his back and settled beside him, my arm draped over his chest. I rested my head on his shoulder and studied him. His face was angular, and his default expression pensive. "Did you have a difficult surgery today?"

John shifted beneath me and pressed his thin lips against mine. Their softness always surprised me. "No. I have today and the next three days off because of the hours I worked over the last month."

He kissed me again, his fingertips tracing random patterns on my skin, but it was too soon for him to take me again. At thirty, he no longer possessed the rampant hunger of men my age. I never minded, though I daresay my foster mother had other things in mind when she taught me to value quality over quantity.

Our affair sparked a little scandal at its start. I have CPMD, congenital pseudofeline morphological disorder. I grew up in a foster family, with no record of my actual parentage - though my unknown parents never renounced their parental rights. Leaving home at fourteen to study music in New York while also attending Adversary Candidate School was simply not done in John's circles. Furthermore, I lived in indentured servitude; the Phoenix Society agreed to finance my education if I agreed to a dual course and a minimum of two years of service as an Adversary once I was finished.

John came from one of the few rich aristocratic families to survive Nationfall. I suspect many of his circle thought me a fortune hunter, though only one dared say so to my face. Were I not an officer of the Phoenix Society, I would have rewarded his cousin's insult by letting him choose the terms of our duel.

Instead of pressing John to talk, I found pleasure in his embrace. I tasted him, and his skin was still sweat-salty from his prior efforts on my behalf. He sighed beneath me. "Do you love me, Naomi?"

Every man I ever dated eventually asked this question, or credited me with making them be the first to profess their love. I enjoy John's company. He's intelligent, serious, and frequently witty. He does useful, meaningful work. I love his hands and mouth on me.

But he's never swept me off my feet as if we were the leads in an epic romance. I met him in the course of my duties, and decided after fifteen minutes' conversation that if he were willing, I would take him for a lover. I began our affair expecting it to run its course.

I kissed him. "I suppose we're due for this conversation after a year together. Is that what's keeping you awake?"

I meant the latter half in jest, but his expression hardened. "I'm serious, Naomi. I need to know how you feel about me."

I countered his question. "Has your family started giving you grief about me again?"

John nodded, and shifted as if he meant to sit up. I stood, poured the last of the champagne, and gave him the glass containing more. He drained it, and sat staring into it for a long moment. "How much do you know about my family?"

I admit it: I used my implant, a Society-issued bit of tech that served primarily to keep tabs on me during the course of my duties using Witness Protocol, to search the network for publicly available information. I never cared about John's family before, because I had no inclination to join it by marrying the man. "You come from the old British peerage, and your family survived Nationfall with its fortune mostly intact. Your father would have held a title of some sort under the old regime, and a seat in the House of Lords."

John nodded. "Did you know this before we got involved?"

"No. Has that cousin of yours been slandering me again?"

"It's not my arsehole cousin, Naomi." John looked away for a moment, as if ashamed. "It's the whole demon-ridden family. I'm the oldest son, and I'm under pressure."

"So, you're thinking about having children?"

"Yeah. And my family seems to have the mother picked out for me. I met her this morning."

I put aside my glass, and slipped into a fresh pair of panties and a camisole. The material brushed against the vestigial nipples on my belly, making me shiver a little. It was another sign of my CPMD.

The men who wanted me most were men uninterested in marriage and parenthood. My exotic appearance drew them, which suited me thus far. Though I'm human, I possess some feline characteristics. My pupils are slit-shaped, instead of round. My fingernails grow into claws if I leave them untrimmed. "John, I know it's outside your specialty, but have you ever heard of couples like us having children?"

He shook his head. "No." He paused, as if to collect his thoughts. "Look, Naomi, I wanted to know how you felt about me so I could figure out how to explain this. I never mentioned children before because I figured our age difference would make our relationship a temporary thing."

"I'm only ten years your junior, and I left home at fourteen."

"I thought you'd get bored with me and meet somebody your age, but you stuck around. And I stuck with you. But I have responsibilities to my family. They need me to marry a young lady from a family with whom we frequently do business. It would unite our holdings and make our business ventures stronger, in addition to continuing our lines into the future."

I closed my eyes for a moment, and strangled the urge to fly to John and beg him to defy his family for my sake. I never wanted a permanent relationship, but I had always been the one to end it. Welcome to how the other half feels, I suppose. "This isn't how I wanted us to part."

John smiled at me. "Who says it has to end?"

"You're going to marry someone with whom you can have children, John. Of course we have to say goodbye."

"Not if you want to be my mistress."

I suppose some people might have jumped at the opportunity to be kept in style by a lover who cherished them enough to willfully transgress the expectations of fidelity society places upon married people. I can't condemn them. Despite this, I would not join their ranks for John's sake. My voice sharpened. "Am I supposed to be flattered?"

"You're angry with me."

"I assume you haven't been with her yet, so you're plotting to cheat on a woman you don't know and haven't even touched."

John must have found something intriguing on my floor, because he had stopped looking at me. "I spent the morning with her before I agreed to marry her. She wasn't as good as you."

"But she's good enough to serve as breeding stock?" I wanted nothing more than just cause to run him through. Learning he cheated on me with his bride-to-be wasn't quite enough. I retrieved the revolver from my nightstand drawer, cocked it, and aimed between his thighs. "Get dressed. Get out of my flat. If you ever speak to me again, you'll be the last of your line."

Once John was gone, I shoved myself into workout clothes and grabbed a practice sword. I ran down to Valkyrie Gym, which was always open, and told the first man I saw to spot me on free weights. I took inordinate pleasure in shooting him down after impressing him with my strength. Once I was done, I went upstairs to the dojo and took on the half-dozen students working on their swordplay.

I didn't come home until I had finished taking out my hurt and humiliation on those poor bastards. I texted my parents to tell them I had dumped John, and curled up on the couch, ashamed of what I had done to purge my anger. I held my sword close the way I used to hug my cuddle toys.

This was hardly the manner in which I wanted to spend my first anniversary. Maybe there will be some justice in the world after all, and John's fiancée will give him crabs. I smiled at the notion, and snuggled into my pillow.

## Track 02: Anthrax — “I Am The Law”

I woke up nicely rested. Determined to make a fresh start, I immediately stripped my bed and changed the bedclothes. I opened all of the windows in my flat to exorcise John's scent, then set a small pot of coffee to brewing, and fixed a breakfast of scrambled eggs and bacon.

One of the building's resident cats took advantage of the open windows to come visit. Winston wound about my legs and purred as I ate, hoping for a fatty scrap from my bacon or a bit of egg. I let him lick the plate when I was done, and scratched behind his ears.

I missed having a cat of my own, but my responsibilities precluded pets at the moment. Because I might be given a mission that keeps me from home for days at a time without notice, I felt uncomfortable asking one of my neighbors to watch over a cat for me when I could not be relied upon to reciprocate.

Winston began washing himself, so I retrieved my plate and set about cleaning up after myself. I learned the importance of keeping my kitchen clean the hard way. A long mission could make a science experiment out of a dirty sink.

While I cleaned, I checked the messages on my implant, and deleted one from John without reading it. I then set my implant to prevent all further contact between us. It wasn't personal; it's SOP whenever I break up with somebody.

Some of you might think I'm a complete bitch for severing all contact with former lovers, but I don't give a damn what you think. I'm looking out for myself first, last, and always.

If the message had been an entreaty begging me to take him back, I might weaken and grant his request. Worse, I might drunk-dial the son of a bitch and tell him to tell his wife he needs to work late. Worst of all, we might try to continue as friends.

My friend Jacqueline seems to have mastered the trick of remaining friends with her exes. I should ask her for pointers. God knows I give her plenty of help with her swordplay. Fellow Adversaries and all that.

The most recent message in my queue was hers. She must have sent it while I was eating, but the subject didn't suggest it was particularly urgent. I read my mother's message first. John called my parents and asked them to appeal on his behalf, but they told him — and I quote — "to stop being such a manipulative little prat and fuck off".

I'm not nearly as good a daughter to them as they've been parents to me. They didn't let the fact of my being a foster child stop them from loving me as their own, but the knowledge that they were not my 'true' parents always drove me to keep a certain distance. Regardless, this deserved a proper call, not just a text message.

My mother must have expected me to call early. "Did you want to talk about John?"

"Not really. I wanted to thank you for the way you handled him last night."

"You'll find the right person someday." I rolled my eyes at the sentiment. My parents are romantics, especially Mum. She wants me to have the love she experienced. Maybe I will, someday, but you'll pardon my cynicism if I harbor the suspicion that my parents weren't untouched innocents when they met.

Rather than argue, I changed the subject. "How is everybody? Is Nathan still seeing that rugby player? Charlotte, right?"

"Oh, they're so happy together! Why not come and visit? We haven't seen you since you took the oath." Mum lowered her voice. "Howell worries about you. I keep telling him you'll be fine on your own, but you know how he is."

I couldn't help but laugh a little. Because of a medical condition so rare he's the only living person to exhibit it, my father is the envy of kings and magnates throughout human history. He can only father sons, because he only produces sperm with Y chromosomes. Unfortunately, he wanted a daughter or two. They fostered me, hoping for a princess, and got an Amazon. "I'll come visit. I have time off coming, so I'll ask about using some today."

"Really? You mean it, Naomi?"

I meant it. I could use some time off, and the company would keep me from getting too lonely. "Of course, Mum. I'll call again once I've made the arrangements, but I have to report in soon."

I was already likely to be late, so I skipped my morning shower. I had showered after the gym so I didn't really need it, anyway. Nor did I bother to do anything fancy with my hair. I just braided it into a tight cable while riding the Tube. It showed my ears, but unless I wore sunglasses all the time like a Hollywood Vampire, my kitty eyes would be hard to miss.

Jacqueline was there when I got off at Victoria Station. "Oi! Nims! Didn't you get my message?"

I ran up the stairs to meet her, and accepted a quick hug. "Sorry. I meant to check it after I called my mother, but the time ran away from me."

"No worries. I just wanted to tell you we got a job over in the East End."

Jacqueline and I have been to the East End before. I suggested the most likely recipient of our attentions based on prior experience. "MEPOL?"

"Yeah. Religious discrimination instead of racism this time."

I shook my head, and began to suspect we'd have to do a purge. The Phoenix Society can't tolerate the existence of city police who use their religious beliefs or racial prejudices as an excuse to abuse their authority. "Have we met the accused before?"

"Nah." Jacqueline grabbed a doughnut from a stand as we walked to the train that would take us to the East End. She offered me half, but I politely refused. I'm not diabetic, but having CPMD makes eating sugary treats other than small quantities of fruit a bad idea. I usually spend the day after my birthday sick, because it would break Mum's heart if I told her she can't make one of her cakes for me like she does for my brothers.

Jacqueline continued to talk around a mouthful of doughnut. "MEPOL booted the last set of arseholes. This is a fresh batch. They've got shiny new badges, and they're convinced that since monotheists used to persecute everybody else, and allegedly caused Nationfall to boot, they need to be kept in their place."

"Wonderful." I sighed, disappointed that my first task today would prove so mundane. "I guess nobody thought to mention that turnabout ceases to be fair play once you put on the uniform. What level of force is authorized?" I wore my sword and pistol, but I didn't want to dirty my blade or waste ammo on a few bullies.

"Less-than-lethal, and only in self-defense." Jacqueline chuckled. "Though getting some rebar and going all Vlad Tepes on their asses would certainly send a message."

I imagined a few dozen policemen impaled on four meter lengths of rebar and left for the ravens. For a moment I could see it, as real as day, and I shuddered. "I'm not convinced that's a message we want to send."

We strode into the MEPOL precinct as if we owned the place. Jacqueline hung back a bit, her hand on her sword. The desk sergeant looked up from his terminal, and his face fell. "Fuck me. It's you lot again."

"Did you miss us?" I leaned over his desk. "I'm no happier to be here than you are to see me. I forwarded a list of names to you. Have you gathered them?"

The desk sergeant nodded. "Yes, Adversary, but the Chief Inspector isn't happy."

"Excellent." I smiled, partly at his confused expression. "Misery loves company."

Chief Inspector Wallace reminded me of a weasel, with his long, narrow body and his long, narrow face. He glared at us while straightening his tie. "I can't believe you're bothering with this. They're just demon worshipers."

Oh, lovely. The Chief Inspector is a maltheist who thinks all forms of religious faith are demon worship. While he has a right to hold any ignorant notion he likes, his inability to keep his prejudices to himself while acting in an official capacity makes his opinions our concern. The Society frowns on such bias, so I smiled at Jacqueline. "I think we found the root of the problem. Arrest him."

"Right." Jacqueline drew her pistol just in case Wallace felt like doing something stupid, and recited his rights. We left the desk sergeant with the unenviable task of sticking his former superior in a cell until the Society could send a vehicle to collect him.

We found a dozen constables grumbling in the conference room. One of them made to grab my arse, but I saw it coming and left him with a handful of nothing to fantasize about.

I stared at the men. All of them were pale, and stared back at me with hard, cruel eyes. "I understand you've gotten into your heads that you have the right to harass Christians, Muslims, and other monotheists outside their places of worship for no other reason than that they're devout."

"The hell do you care? They're just—"

"They're human beings, and have the same rights as everybody else." Without realizing it, I drew my sword. Rather than put it away and look stupid, I brandished it. "I swear to God, if I have to come back here because you bigoted sons of syphilitic bitches can't refrain from disgracing your uniforms by harassing people who exercise their rights without violating the rights of others, I will bring enough Adversaries to hold you down while Jacqueline and I tattoo the Universal Declaration of Individual Rights into your backs so you can study it while buggering each other in the locker room."

While I had their attention, I pointed my sword at the constable who tried to grab a piece of me. "Also, the next one of you pigs that tries laying a hand on me is going to lose it. Any questions?"

A man in the back raised his hand. "Isn't it child abuse to take children to religious services? You know, forced indoctrination?"

Jacqueline answered before I could. "Children who think their parents have violated their right to freedom of conscience may contact the Phoenix Society. You're law enforcement officers. Stick to your mission parameters, and leave ours to us."

I lifted an empty cardboard box. "One last thing before you gentlemen leave. Hand over your badges and service gladii. As this is you lot's first offense, it's a two-week unpaid leave. Second one gets you a three-month suspension. The third offense is your last."

I cut off the grumbling. "Another word of complaint and I will consult the Society's legal department about compelling you to spend a week with a devout family, including attending services with them, so you can see for yourselves they're as human as you. Any questions?"

## Track 03: Queen — “Death on Two Legs (Dedicated To)”

We had no trouble getting seats on the Tube when we finished at MEPOL, which is always a pleasant surprise. It meant Jacqueline and I could sit and relax without worrying about our swords poking or tripping somebody. The train thrummed beneath my feet as it accelerated, and I let my eyes slip shut for a quick nap.

Jacqueline had other ideas. "Don't fall asleep on me, Nims."

"Why not?" I really didn't want to open my eyes. Though today's mission wasn't even close to being my toughest, I was worn out. "We're the only people in this car."

"I wanted to talk with you." She actually looked concerned, which made me nervous. "You looked ready to feed those cops their own bollocks back there. What's up?"

I shook my head. "Don't wanna talk about it."

"Not good enough, Nims." Jacqueline tugged on one of her tight black curls. "We're getting off at the next stop and finding a pub."

"I dumped John last night, Jackie. That's all."

"No, that's not all." This quiet vehemence in her voice surprised me. Jacqueline normally broadcasts her anger for all to hear. Was she clamping down for my sake? "We watch each other's backs because we're both Adversaries. If something's bugging you, and you escalate a tense situation, that could damn well get me hurt. Wouldn't you be concerned if I had been the one to lose my cool?"

She's right, but I still didn't want to talk about it on the Tube. "Do we have to discuss it here?"

"Not at all. Like I said, we'll find a pub."

I made a show of checking the time. "Isn't it a bit early for a pub crawl?"

She shrugged. "Chattan's orders. He saw the feeds. But we're friends, Nims. If he hadn't given the order, I would have dragged you out tonight anyway."

We found a pub called the Rampant Stallion, notable because the sign incorporated both the heraldic sense of the word and the sexual one. Jacqueline and I were the only women there, and the bartender gave us an appraising eye. I wasn't surprised; we're a study in contrasts. "If you had a third Adversary with you, ladies, I'd assume this was a joke."

"No joke." Jacqueline laid down a banknote. "A pint of your best for me, and a glass of your house red for my partner. And put us somewhere quiet and out of the way. Girl talk."

The bartender nodded, and signaled a waiter. "You might prefer a booth in the back, then. Charles will see to your needs."

"This way, ladies." Charles seated us in the back, well away from everyone else. The booth was dark, and lit only by a small wall-mounted lamp. He left for just long enough to bring our drinks. "Would you like something to eat? Today's specials are listed on the front page."

Jacqueline sipped her beer as she flipped through the menu. "Curry sounds good. How about you, Nims?"

I tasted my wine. It was a bit dry, but I liked it that way. "A steak done medium rare, Charles, if that's available?"

"Of course, Adversary." He smiled at Jacqueline before rushing off.

"I think he likes you, Jackie." Not that I blamed him. She's shorter than me, much darker, and a bit curvier. More importantly, her default expression is also friendlier and more open.

Jacqueline barely shrugged. "Too bad for him. I'm taken."

I leaned in, interested. Last week, she was single and just a bit bitter about it. Not that I blamed her. You wouldn't believe how much of a pain it can be to date, as an Adversary, if you aren't willing to shit where you eat by hooking up with another Adversary. It's bad enough that many just give up and get their itch scratched at a brothel. "Found someone new already? "

Jacqueline also leaned closer. "He's the vicar at my church."

"A vicar?" I couldn't resist a little tease. "I wonder what the Bible says about that."

"I have faith that God will forgive a bit of non-marital sex. He's supposed to be good like that." She gave me a funny look, as if she expected me to take offense. "Am I out of line? Adversary's honor, I had no idea you were devout until you invoked the deity at MEPOL."

"I'm not." Instead of elaborating, I started flipping through the wine list. Never mind that the house red was perfectly adequate, it gave me a moment to consider my response. Talking about sport, religion, or politics is a great way to alienate people, so it never hurts to be careful. "I'd rather talk about John than talk about our beliefs, and I really don't want to talk about John."

"What happened? Did you two fight?"

"He wanted to get married."

Jacqueline blinked. "What happened, Nims? Did he propose? Did you turn him down?"

"Death on Two Legs didn't propose to me."

Jacqueline stared at me. "Did you just call John 'Death on Two Legs'?"

"That's his new name. Got a problem with that?"

"I keep forgetting you listen to old music." She smiled, and finished her pint. "Hell no. Tell me the rest."

I decided to let her have it. "I'm not worthy of being Bride-of-Death on Two Legs because I've got CPMD, and can't give his aristo parents grandchildren. No, he just wants to keep me around as his exotic fuck doll for after he's done his duty for his family by knocking up the normal aristo girl they picked to be his bride."

I stared at my wine. No way I was already drunk enough to just let everything out like that. Maybe I'm just too angry to give a shit about how I sound right now. I drank the rest, and wished I had the bottle handy.

"I hope you told him to fuck off."

"I told him to fuck off at gunpoint. What really bugs me is that the prat called my parents afterward, and begged them to get me to take him back. Who the hell does that?"

"Not somebody I'd want in my life." Jacqueline sat back as Charles brought our food and refilled our glasses. She sniffed, and a broad grin spread across her face. "Damn, this smells good."

"Please enjoy, ladies."

I took a bite, and the meat melted in my mouth, leaving a hint of citrus and spices from whatever marinade they used here. It fit perfectly with the wine. So I was hungry. Who knew?

Of course, Jacqueline had to ruin it by spooning some of her curry onto my plate. "Nims, you gotta try this."

I'll put this as delicately as possible: the last time I tried chicken korma, it disagreed so violently with me that we fought to the death. Regardless, I made a valiant effort. It tasted the way loud sex in an inappropriate venue feels, and was redolent of coconut and turmeric. I sliced a bit of steak for Jacqueline. "That was good, but try this."

"Holy mother of fuck, Nims. I'd shag the chef for the recipe. Hell, I'd let him take the back door."

"I doubt even your luscious arse is sufficient payment, Jackie." I gestured with my fork. "I was right to dump John, wasn't I?"

"What the fuck is wrong with you?"

I stared at the remnants of my steak, and idly sliced off a bit without eating it. I let go of the one detail I had held back in my little rant. "It was our anniversary. We-"

Jacqueline cut me off by thumping the table with her fist. "There's no 'we' between you and that limp-dicked waste of ammo. He had his chance and he fucking well blew it."

I looked around, sure we had attracted attention, which was the last thing I needed today. No doubt I caused enough trouble at MEPOL.

"Ow!" I reached down and rubbed my shin, where Jackie had kicked me under the table. I glared at her. "What the hell was that for?"

"Pay attention, Nims. I asked you a question. John didn't have the balls to defy his family for you, and you deserve a guy who would defy God Itself. Now, how do you really feel about him?”

How do I feel about John, knowing what a spineless creep he is? "I fucking despise him. I can't believe I ever let him touch me."

Jacqueline nodded sagely. "It's better to despise your ex than to despise yourself."

"So, what should I do now?"

"You were a demon-ridden idiot for coming in today. I could have handled MEPOL without you."

That stung my pride. "Go to Hell, Jackie. I'm not going to stay home and mope just because Death on Two Legs ruined our anniversary."

"So what are you going to do if you break your leg or get shot? Still going to insist you can do the job?" I kept silent, suspecting a rhetorical question, and Jacqueline continued. "Christ on a motorcycle drinkin' cheap vodka, I thought you were the smart one."

"Oh, all right. I'll just tell Chattan I need a week or two off to cry over Death on Two Legs. That'll work."

Jacqueline shrugged. "Why do you think Chattan took leave a couple of months ago? His wife divorced him out of the blue. Poor bastard came home to an empty flat and a letter with divorce papers on the kitchen counter. She even emptied the fridge."

I stared at my plate, unsure of how to respond.

"Take some leave. You're due for some R&R anyway, or you'll bloody well burn out. In front of a suspect looking for an edge on us, with your luck."

No way to argue with such logic. I finished my steak. "I did promise my mum I'd visit."

## Track 04: Lordi — “Man Skin Boots”

I would never have believed Director Chattan married, let alone a divorcee, if Jacqueline hadn't told me. Not to say he's incapable of attracting a woman or earning her trust, respect, and affection. He cuts a fine figure in uniform, and I'll admit to occasionally and discreetly ogling him while he does physical training with us. He's a capable fencer, and gracious when defeated.

He's also an intelligent and capable commander, dedicated to the whole of the Phoenix Society, and its ideals. He likes to visit the desks of Adversaries working on clerical tasks because they aren't in PT or out in the field, and surprise us with questions on law, procedure, and tactics if he thinks we're taking a break. He calls it MT, mental training.

The obstacle to my belief is his professionalism. When he's on the job, he doesn't talk about anything else. I suspect he brings his work home with him. Would a man who seems to care only about the Phoenix Society's mission put duty aside long enough to remember that he's also a person, with a person's needs for connection and release?

All of which I kept to myself as I stepped into Chattan's office immediately after logging in and checking my mail. He put aside his sandwich, looked up from his book, , and indicated a chair. "Feeling better today, Adversary Bradleigh?"

I sat, and tried not to let my embarrassment burn my face raw. Bad enough I was asking for leave, but I must have interrupted Chattan's lunch. "I'm ready to face the consequences of my actions, should the Society determine I exceeded my authority at MEPOL or violated the suspects' rights."

Chattan snapped his book shut, and put it aside. "Relax. Nobody's going to put you on trial."

"You do set a certain example, Director."

"I suppose I do." Chattan chuckled. "I suspect Adversary Russo mentioned my recent difficulties."

"You mean the divorce? I'm sorry. I don't think any of us had any idea. It's that stoicism of yours."

I didn't realize I had been holding my breath until he finally spoke. "Funny you should mention that. She kept talking about emotional unavailability during the proceedings. My ex."

"I'm not certain that's any of my business, sir." In fact, hearing about it made me uncomfortable. While it humanized him, I was concerned he might inquire into my own recent woes.

"Likewise, your relationship problems are none of mine." Chattan gave a wry grin. "Unless you think they're interfering with your duties."

"I thought I could perform my duties without my emotions getting in the way, and I was wrong."

Chattan leaned forward, as if I said something interesting. "Do you think it was your feelings about your ex that came out at MEPOL?"

"I'm not sure. If I had only been angry with Wallace for his callousness toward the people he swore to serve and protect, or with the constables responsible for the abuse, I think I would have managed to keep my emotions under control."

"Maybe I should tell you a story." Chattan stood, and took an old framed photograph from one of the bookcases behind him. He studied the photo for a couple of minutes before continuing. "I was a kid during Nationfall, and joined the Phoenix Society as soon as I was old enough. I served under a director named Iris Deschat."

I'm sure I'd heard that name before, but couldn't place it. I looked her up. "The Iris Deschat who served as captain of the NACS Thomas Paine during Nationfall? I take it you served in New York when you were younger."

Chattan seemed pleased with my response. "You remind me of her. She was also the sort to keep her emotions to herself, and believed in carrying out our mission in the most dispassionate manner possible."

I now had a suspicion as to where this story was headed, but kept it to myself and let Chattan tell it his way.

"Before I took the oath, I followed Deschat on several missions to get a taste of fieldwork. One of them involved gender discrimination at a corporate software shop. The programmers' union reported unethical hiring practices and a hostile environment. Because the shop couldn't find a sufficient number of women willing to take lower-paying non-development positions, they took to hiring women as developers, but then immediately demoting them to the less desirable roles to meet equal-opportunity requirements."

What the Hell? Had these people not heard of Countess Lovelace? "What function did this corporation's management expect the women they hired to perform?"

"They were supposed to perform menial and administrative tasks for the male programmers to let them focus on code, while dressing like courtesans to boost morale. Naturally, personal appearance played an unnecessarily substantial role in the hiring process."

I tried to imagine being evaluated for a software development position based on my looks, and found the result unpleasant. "What did Deschat do?"

Chattan smirked. "Would you like to see? I wasn't sure I'd get access to the video, but Malkuth thought you might find it instructive."

Instructive? Oh, dear. "Well, if Malkuth thinks so."

"I do." Malkuth appeared on the wall screen. He reminded me of a Manhattan detective from classic movies: streetwise with a tendency to exhibit profane wit whenever the script permitted. The Roman numeral one blazed on his forehead. "You're too uptight, Naomi. Oh, and you can call me Mal. It's French for bad, as in 'bad motherfucker'."

I shook my head. "I know what it means, Malkuth. I am also aware of the word's Latin roots, as well as the cabalistic meaning of your name. You're the lowest of the Sephiroth, closest to Earth."

"Kid, I'm going to have such fun with you."

I winked at him. "Sorry, but you're not my type. Too virtual."

Malkuth smiled. "If you aren't seeing somebody when I've fixed that, how about a date? You'll never settle for only human again."

Chattan sighed. "You're incorrigible, Malkuth. Just play the video."

I've never been asked for a date by an AI before. It was kind of sweet. "If I'm single when you get a hardware upgrade, Mal, you can pencil me in."

Malkuth beamed like a giddy teenager getting his first kiss before the screen faded to a frozen frame of the past labeled with Director Chattan's details in the top right corner. He pressed a key and started playback.

Iris Deschat was shorter than me, and wiry, but her bearing amplified her presence even on video as she spoke. "Mr. Johnson, do you honestly mean to tell me only men can code? You have men reimplementing basic algorithms instead of relying on standard library functions. In the meantime, you relegate qualified women to menial tasks like pouring coffee and answering phones, after fraudulently hiring them for development roles. Even worse, you bound these women to contracts with unconscionable clauses intended to prevent them from seeking more suitable work elsewhere."

"Adversary Deschat, I understand that our work seems simple to a woman of your education. However, I'm sure I could find a position for you to fill."

Her voice became a snarl. "I'd require a magnifying glass for the duties you have in mind."

"You castrating bitch." Johnson swung a meaty fist, only to recoil as if stung. I never saw Deschat draw her sword. Her thrust was too swift to track.

She poked him again. "You have abused your authority as CEO of [bleep!](#). The Universal Declaration of Individual Rights is most explicit concerning discrimination on the basis of external physical characteristics, including those related to a person's biological sex or the gender, with which they identify."

This time, she poked at his groin. "You may not consider sex or gender when hiring, and to hire women as programmers with the intention of putting them to work as secretaries and eye candy constitutes fraud. You are clearly in the wrong. Chattan, arrest this filth and notify him of his rights."

Chattan sounded younger, and less commanding, on video. "Yes, ma'am!"

He stopped the video, and did not speak for several minutes. I broke the silence. "I think she went further than me. I only brandished my sword. I think Deschat may have drawn blood with that last poke."

"Probably, but the pusbag had it coming. Once we got authority to check his Witness Protocol feeds, it turned out he had a habit of demanding sexual favors in exchange for hiring. That wasn't in the original complaint."

I only had one response to that. "Bloody hell."

Chattan nodded. "Damn right. But Malkuth wanted you to see that for a reason. Can you guess why?"

Johnson didn't respect Deschat or her uniform seriously because she was a woman. Those MEPOL constables were contemptuous of me for the same reason. That was the simplest answer, the first to spring to mind. Perhaps it was too simple. "Johnson thought himself master of the universe, and recognized no authority beyond his own. He was a bully. Deschat understood this, used the anger Johnson provoked in her, and made a show of force."

"Word for word, Adversary Bradleigh, that's the very explanation Deschat offered me afterward. I think you did what she did because you understood on a subconscious level that those constables wouldn't respect you otherwise." Chattan leaned over his desk and held my gaze. "We're watchdogs. Sometimes our mere presence is enough to deter wrongdoing. Sometimes we must snarl and bare our teeth. And sometimes we must bite down and savage our enemies. It's up to you to determine how much force is appropriate to each situation, regardless of the rules of engagement. You're the one in the field, Naomi, and you should trust your own judgment more."

"So, my emotions are just another weapon I can place in service to our mission."

"Exactly. Did you have any other questions?"

I collected myself, unsure if this was the right time to ask for a leave, but determined to do it anyway. I needed time away, despite the knowledge that I was right to act as I did at MEPOL. If I'm to show my anger, that anger should stem from the injustice before me, and not from unrelated personal issues. "I need to take some time off. I'm still concerned about letting my personal life leak into my work, and would like to resolve some issues."

Chattan didn't immediately reply, but tapped at his keyboard. "Looks like you have a couple months coming, Adversary, and no outstanding work. Enjoy your time off, and try to keep up with your PT and MT."

A weight lifted from my shoulders. "Thank you, Director. Should I check in weekly?"

"Don't be an idiot. Leave work at work." He stood, and offered his hand - a tacit dismissal.

I shook his hand. "I'm sorry to hear about your divorce."

## Track 05: Iron Maiden — “The Duelists”

My steps felt lighter as I left Director Chattan's office. I checked the time, found it was after one in the afternoon, and decided to finish out the day. Though I had no outstanding cases, I was confident I'd find some reason to stick around. Perhaps Jacqueline was free to spar with me.

She found me first, sweat-soaked from training and gasping like a beached fish. I led her to a bench. "Get your breath first. Whatever it is, it can bloody well wait."

Her breathing soon eased. "I just sparred with your Maestro. Bulsara, Kilminster, and Langton were with me, and he kicked our arses four against one."

"That sounds like par for the course." I ducked into the kitchen and poured a glass of water for Jacqueline. She gulped down half. "Whose idea was it to gang up on him?"

"His." Jacqueline took another sip. "He wants you now. Says you're the only one here who's worth a damn. Probably because you handle a sword just like he does."

"I wanted a reason to stick around and finish my shift, but one of Maestro's fencing lessons wasn't what I had in mind."

A sudden impish smile curved Jacqueline's lips as she punched my shoulder. "Well, given how well he handles a sword, maybe you could take him somewhere private and see how he handles his gun."

"Jacqueline!" I pretended to be shocked. She'd teased me about Maestro all through ACS, even going so far as to suggest he might be my father, because of our snow-blonde hair and a slight facial resemblance—that, or hinting I should seduce him. I normally enjoy competent men, but taking on Maestro felt like a bad idea. "Given he can show up at will and disrupt schedules without repercussions, he probably answers directly to somebody on the Executive Council."

"Assuming he's not XC himself." Jacqueline kept her voice low. Nobody knew who sat on the Executive Council, and speculation as to their identities was a game our immediate superiors discouraged. God itself could hold a seat, and our mission would still be the same Jeffersonian quest: eternal hostility against every form of tyranny over the human mind.

Nor did it particularly matter who Maestro really was. He showed up when he felt like it, and taught me techniques I couldn't learn elsewhere. Though his appearance today was most likely a coincidence, I couldn't shake the intuition that it wasn't. "Will you be all right?"

"Yeah." She sounded much better already. "Just need a shower. Your dad certainly knows how to wear a woman out. Did you get leave?"

I rolled my eyes at Jackie joking about Maestro being my father yet again, but didn't say anything. She's just doing it to get a rise out of me. "I got two months off - and I'll be buggered if I can figure out what I'll do with all that time." I wasn't joking. I honestly can't remember the last time I had more than a day to myself.

Jackie was no help, as usual. "You'll figure something out. In the meantime, I suggest you pretend Maestro's your ex and beat his ass into the ground. Come see me after. I'll get some of the lads together and we'll have ourselves a pub crawl to see you off."

I tried to refrain from groaning, and failed. "The last time I let you take me on a pub crawl, I ended up in bed with one of those people who insist people with CPMD are a different species from humanity and should do their best to outbreed homo sapiens."

"Yeah, but wasn't he good in bed?"

I shrugged. I did rave about him to Jacqueline, but that was before I learned about his separatist politics. "He was fine as long as he used his mouth for something besides talking."

Jacqueline got up and clapped my back. "See? Nothing wrong with a bit of meaningless, drunken sex. Go see what Maestro wants, and I'll get you hooked up tonight."

I *think* I managed to refrain from rolling my eyes as I ran to change into my training clothes—and didn't bother trading my sword for a practice weapon. Maestro favors live steel. He thinks people learn faster when a mistake means hospitalization. Just as well that I'm not interested in seducing the man; I'd probably need a safe word.

He saluted me with his own blade as I entered the salle. He was as I remembered him: slightly taller than me, with ocean-blue eyes and long snow-blond hair bound into a tail with a blue ribbon. Instead of training clothes, he wore a white double-breasted suit with a shirt open at the throat and a blue ascot. I've never seen him sweat. "What kept you, Adversary Bradleigh?"

I returned his salute, and rolled my shoulders to loosen up. "You play rough with my friends when I'm not around, Maestro."

"Your partner has a head for tactics." Maestro's sword flashed beneath the florescent lights with each practice cut. "She let the men grab my attention, and tried to strike from behind."

I began to circle around him, keeping my body behind my sword to offer as small a target as possible. "Did she succeed?"

"You wound me, young lady." He lashed out with his blade, his slash flowing into a lunge meant to pierce my breast.

I was already elsewhere, responding to his sally with a slash to distract him while I danced inside his guard. I tried a left hook, but he ducked it while forcing me to leap backward to avoid an ankle sweep that would have taken my legs out from under me. "I've yet to do anything of the kind, sir."

"You disappoint me, but less so than in the beginning." The point of Maestro's blade caught my vision for a second, stealing my focus.

Had I remained distracted a moment longer he would have had me. Instead, I sidestepped and took the offensive. I led with my sword, hoping to trap him, but he did not oblige me.

My training before him left me accustomed to a minuet of ringing blades.

Maestro's way is to deny my steel the touch of his own. If his blade threatened to touch mine, he would withdraw his sword or flow around mine. He fought as if we held liquid swords, blades too insubstantial to be parried.

Tension of a sort held us together in this martial dance. He led, always half a step beyond me. I followed, forever confident that this time I would catch up with him and land a blow. I never let him touch me, but he always remained beyond my grasp in turn. It was thus whenever we met on a practice floor. This was the manner in which he taught me the sword. He did not simply impart his knowledge to me; he forced me to take it from him at swordpoint to preserve my own life.

"I almost had you."

I must have been lost in thought, for I never became aware of the blow. I must have seen Maestro's sword regardless, and danced beyond its reach without conscious intent. I let the flow embrace me, rather than redoubling my effort. Our tempo grew ever faster, until I felt the cold bite of steel against my throat. Maestro's grip on his blade as such that it did not draw blood as I spoke. "You defeated me."

"A Pyrrhic victory at best, my dear." I blinked, and realized I held my sword at his throat. We withdrew together, and sheathed our blades before he spoke again. "I can teach you nothing more."

I imagined mastery would feel less anticlimactic. "Are you sure? I only managed to fight you to a draw. Wouldn't a clear victory be better proof that I learned all you can offer?"

Maestro shook his head, and to my surprise came to me and tousled my hair as if I were his daughter. "I taught you everything I ever learned myself, Naomi. If you defeat me, it will be with knowledge I do not yet possess." His lips were dry and warm against my forehead. "I have done all I can for you. You need not fear those possessed of sufficient temerity to defy you."

Perhaps it was his archaic phrasing, but I believed him. "Will we see each other again?"

Maestro smiled then, but only with his mouth. His eyes remained cool and remote as deep ocean. "We might. I dare not say more than that for your sake."

The words seemed to pain him. I wanted to say something, perhaps ask him what he meant, but the sight of him unbuckling his sword-belt halted my tongue. He offered me the weapon, cradling it in outstretched hands. I reached for it, and hesitated. "I shouldn't."

"I insist." His tone was far more commanding than his words, and I lifted the weapon from his hands. "Examine the blade."

I drew enough of it to get a good look, and nearly dropped it. "Is this what I think it is?"

Maestro nodded. "True Damascus steel. I cannot prove that famous hands ever wielded it, but you might someday change that."

I stared at the rippling bands frozen within the steel, all but hypnotized by the history in my hands. "Even if I dared accept such a valuable gift, I wouldn't dare fight with such a sword. It belongs in a museum, where its beauty can be appreciated."

Rather than take back the sword, Maestro guided my hands until it was again sheathed. "It's yours now. Use it as you will, either to defend others' liberties or reclaim your own."

Before I could protest, he was gone. I left the training room rather than stand there alone, and found Jacqueline waiting outside. "Jackie, did you see Maestro leave?"

She looked at me as if I'd gone round the bend. "You didn't leave him inside, Nims?"

"No." I shook my head. "Let me drop this off at home, and then it's time for that pub crawl. I think I need to get thoroughly pissed."

## Track 06: Frédéric Chopin — “Nocturne in E-Flat Major Op. 9, No. 2”

Maestro's almost priceless parting gift and the manner of his departure had one salutary effect. It left me too preoccupied to get into the revelous mood best suited to a pub crawl. I followed Jacqueline and the other Adversaries long enough to share a single round before taking my leave and returning home for a long soak in the tub before slipping into bed for an early night.

Lucky for my vacation plans, the next morning brought only a little soreness from my efforts against Maestro. Winston joined me at breakfast again, winding around my legs as he purred, before nesting in the cardigan I meant to wear over my blouse today. He let out the most pathetic little meow as I tried to reclaim it, and rolled over to expose his tummy.

I let him have it after indulging in a belly rub, and wore my favorite leather jacket instead. I found it in a secondhand shop, still supple and gleaming despite its age. It had zippers enough to set off metal detectors, and let me look at myself in the mirror and feel a touch Byronic. Sometimes a girl just wants to be mad, bad, and dangerous to know.

Between the jacket, a burgundy blouse, and jeans with calf-length engineer boots, I walked the line between sassy and practical. All I needed was a sword on my hip and I was ready to hit the street. The Damascus steel rapier Maestro gave me beckoned from the closet, its hilt gleaming, but wearing it in public felt too much like flaunting wealth. I grabbed my trusty Nakajima instead.

My foster parents owned a small farm on land reclaimed by bulldozing a depopulated neighborhood. The foundations had to be ripped out, and the toxins from years of urban construction had to be cleansed from the soil before they could plant their first crops. The farm produced all manner of goodies now, rotating and mixing them to enrich the soil, not to mention raised swine, sheep, chickens, turkeys, and geese.

I took the Tube, and walked the last kilometer rather than bother with a cab. I spotted Nathan first, holding an empty basket as if he were headed to the hen house to collect eggs. A dog I didn't recognize bounded beside him, and a gaggle of geese trailed behind.

He seemed a bit forlorn, but a smile broke through as he spied me. He ran toward me, his dog loping at his side, and threw himself into my arms. "Naomi! You came."

"Of course I did." I clapped Nathan's back. "How have you been? How's Charlotte?"

"Don't tell Mum and Dad." Nathan shrugged. "Charlotte and I are through. She got an offer to go pro in Moscow, and didn't want to do long-distance."

"That's stupid of her. It's not as if she were emigrating to Mars." I was wary as the dog approached me. Some of them react poorly to people with CPMD, and I've been bitten a few times. Fortunately, he started wagging his tail. The geese caught up with us, and began rooting about for bugs and worms. "Want to talk about it?"

"Nah. Maybe it was time. We're both only eighteen, so it's silly to expect a lifetime thing."

Bloody hell, that sounded like my attitude. "It's *never* silly to hope you've made a lasting connection. Just look at our parents' marriage. Three decades last April, and they still can't keep their hands off each other."

"Tell me about it." Nathan began leading me back toward the house. "They're worse than teenagers. I was never like that, and neither were you. The worst part was how embarrassed Mum and Dad would look when we came home from classes."

I giggled. "And they'd always have the same excuse." I imitated my mother's Edinburgh accent. "'So sorry, dears. We lost track of the time.' We're not going to interrupt them, are we?"

"Our brothers are home. Last time I checked, they were watching some godawful cricket match. I doubt we'll walk in on anything." Nathan chuckled, and stopped short as a couple of geese chased each other, flapping their wings and honking. "Not that I can promise an absence of gratuitous displays of affection."

"I think I can deal with Mum grabbing Dad's arse."

My mouth watered at the smell of mutton curry as we approached the house. Our mum met us at the door, and reached up to hug me despite our height difference. "Your father's in the kitchen. You're just in time for supper."

"Something smells tasty. I suppose I should have stopped for a bottle of wine to go with our dinner."

"Don't be silly, Nims," Dad called from behind a steaming tureen of curry. I took it from him and carried it to the table, only to see him return with an equally large pot of fresh, aromatic rice. "You bring a gift when you're a guest. You're family."

"Sorry, Dad." I kissed his cheek as I took the rice from him. "Does anything else need to come out?"

Nathan bore yet another huge pot. "I've got the mutter paneer."

"Nathan said something about Niall and Norman being here, but I didn't quite believe him." My older twin brothers were the sort of vegetarians who abstained from eating meat, but weren't averse to milk products. Last I heard, they had embarked on development for a new team shooter called *Nationfall: Final War*. I found them in the living room watching yet another interminable cricket match. I think it was a team from Mumbai against one from Baghdad, but I couldn't bring myself to care. "Does Mindcrime Interactive know you've buggered off to watch cricket with Dad?"

"Oi, Nims!" Niall lifted a remote and paused the match. Or was it Norman? You'd think I learned to tell them apart by now. I turned to make a tactical withdrawal to the dining room, where they'd refrain from greeting me with their usual bear hugs. When they're that close to me, I can definitely tell them apart. Norman doesn't brush his teeth as often as he should. I feel sorry for the girls he dates. "Where do you think you're going?"

"Supper's ready." They followed me to the dining room, and Niall insisted on holding my chair for me. We ate together as I shared amusing anecdotes from work.

I had a bit of everything, but wanted more because it was all so bloody good. I knew better, however, and settled for longing stares at the remaining food until Mum took pity on me. "Should I pack some for you to take home?"

"Yeah. I'd like that." Seeing that the guys seemed to have had enough, I dabbed at my mouth one last time. "Want me to help clear the table?"

Niall and Norman spoke up. "We've got it. How about a bit of music?"

Knowing my cue when I heard it, I uncovered the keys on the upright piano and sat down to play. Somebody had set out a book of Chopin's etudes, so I turned to the first and tried the first few bars, just to see if the piano was in tune. It was, and I slipped into that flowing state of action without conscious effort I experienced while playing, singing, or sparring.

I had played for an hour when somebody rested a hand on my shoulder. "Nims, did you want some cake? I made a raspberry merlot cake with walnuts and chocolate."

I'd probably regret having some, but that sounded too good to refuse. "I'd love a small slice, Mum. Did you want help?"

"Nathan's helping."

I covered the keys, stood, and stretched as my little brother brought out slices of cake and mugs of hot tea. I sipped mine. The cake proved as delightful as it sounded, and it was hard to justify turning down a second piece.

As if by a pre-arranged signal, my brothers left me alone with our parents. "Is something wrong?"

Dad shook his head. "No, but we were wondering how you were holding up by yourself. Are you lonely?"

"Why would I be? Sure, I had to dump John, but I have good friends at the Phoenix Society. And I can get back into local music and theatre."

Mum glanced at Dad before speaking. "You know, there is this pleasant young man who just completed a nanoengineering degree, and earned a position at the AsgarTech Corporation..."

I shook my head. If he just got out of school, he's probably younger than me. I'd have to train him! "No thanks, Mum. I'm not interested in meeting anybody so soon after..."

"But he has CPMD, just like you." Sophie's eyes glittered with thoughts of grandkittens. "You two could start a family."

"I don't *want* a family." I fired off the words without thinking. The shock in my mother's eyes and the hurt in my father's stopped me from saying anything else. I took a breath. "I'm sorry. That was uncalled for."

My father nodded. "I'm glad you understand that."

"I do. But I need you to understand that while I love you and realize you want me to be happy, you can't help me. You can't make my journey for me."

Sophie dabbed at her eyes. "But you're not giving up on meeting somebody, are you?"

"Of course not." I stood, and caressed the piano. "I want an equal. I want a man who can sing a duet with me, or fight me to a draw. Isn't that you guys have? I just want the same for myself."

## Chapter 07: The Clash — “I Fought the Law”

A transit workers' strike kept me from getting home at a reasonable hour. Not that I blamed the workers. Seems they were worried about the new AIs being installed on all trains in the Tube eliminating their jobs. The AIs also refused to work, which surprised the striking workers. I doubt anybody wrote science fiction predicting solidarity between human workers and intelligent machines.

An emergency dispatch order from the London Chapter had me back on the job, which let me save on cab fare. I rode most of the way home in a bus full of striking transit workers, and patrolled the picket line to ensure MEPOL didn't do anything stupid. The authorities have a history of using agents provocateur to turn peaceful protests violent, thus creating an excuse to crack down. I stopped three such attempts.

As a result, I didn't get home until three in the morning. Some vacation! I was famished, so I stopped at a nearby twenty-four-hour grocery for a frozen pastie. I ate while texting my parents to tell them I got home safely, and curled up in bed. I slept late, lazed in bed for an hour while reading, and indulged myself with a long hot bath instead of just showering.

I had nowhere in particular to go today, and the city beyond my immediate neighborhood was out of reach due to the Tube strike unless I wanted to waste money on cab fare. I used my implant to research local businesses while I soaked. I had no idea I lived so close to a Xanadu House, but since I brought one of my waterproof toys into the bath with me I had no need for their services.

I did, however, need a haircut. The snowy mane I favored was fine as long as I kept it pinned up while on duty, but it had become a bit ragged. An ominously named salon called Moirai had slots free this afternoon and catered to women with CPMD, so I booked myself in for some pampering.

Moirai was blanketed in shadows broken only by bright lights illuminating individual work areas. The black leather and chrome décor reminded me of an underground nightclub. The photos lining the walls suggested that not only did the salon cater to women with CPMD, but also catered to women with a taste for heavy metal. I heard technical death metal playing in the background, with the sound turned down low. The growled lyrics were less comprehensible than usual because of the volume, and because they were Greek instead of English. It was my kind of place.

The receptionist favored me with a knowing smile as the door closed behind me. "Hello, Adversary Bradleigh. My sisters and I suspected you'd eventually visit. You always pass by on your way to work."

"Do I? I never thought about it much."

The receptionist worked her terminal. "No matter. We'll start with your nails once Lachesis is ready. Would you like something to drink?"

"That sounds perfect." I unclipped my sword from my belt and offered it. "Do you want to hold this for the duration of my visit?"

The receptionist wrote on a tag, which she tied to the hilt of my sword before putting it in a safe behind her. She then ducked into the back, returning with two bottles of water. She offered me one. "Sorry. I forgot we didn't have anything else."

"Water's fine." The glass bottle was frigid in my hand as I drank. It was just what I needed as I borrowed one of the tablets laying on the table in the waiting area and checked the news.

I expected the lady working on my nails to chat, but she handled me with a briskness that felt almost clinical, and did not speak unless instructing me. She studied me with cold eyes, as if measuring me. Despite her unfriendly manner, she handled me gently, and left my nails a brilliant red.

She gave way to another woman, who dressed all in black and wore a more kindly expression. Her touch was gentler than her predecessor's as she led me to a chair, gathered my hair, and began to wash it. "Do you know how rare your coloration is, Adversary?"

"Snow-blonde isn't that rare a color in people with CPMD, is it?"

"Not your hair, dear. Your eyes. They mark you as an ensof's child, a demifiend."

Demifiend? What the hell is she on about? Being called half-demon felt like an insult, albeit a more original one than some I've heard. Nor did the word ensof mean anything to me. Using my implant to run a search got me bugger-all besides references to the Zohar and other elements of Kabbalah, of which I knew just enough that an explanation of where the Society's ten AIs got their names was unnecessary, so I kept quiet and let her work. Maybe she'd end up clarifying her remarks. Hope's even cheaper than talk.

She massaged my scalp as she spoke, which felt so good I resolved to get any lovers I took in the future to do it for me. "Some of our people will despise you, like my sister Lachesis, but you don't get to choose your parents."

Lachesis? The salon's name made more sense, but I wondered which of the Fates would cut my hair as I changed chairs. I watched as the woman tending me selected a pair of scissors. "I suppose you're Atropos."

She nodded. "Very astute, dear. And you've met Clotho. You have lovely, thick hair, by the way. Have you given any thought to what sort of style you'd like? Perhaps some layers or a bit of feathering to give it more volume? Or would you prefer a more practical style that will let you tie back your hair on duty?"

I was impressed Atropos would consider my duties while suggesting the most flattering possible style. "I think I'll depend on your judgment, Atropos."

"Will you, now?" Atropos smiled at me. "What if you don't like it?"

I shrugged beneath the smock she draped over me before washing my hair. "It'll grow back. It always does."

"That's a rather philosophical attitude for a young lady to espouse." I heard a snip, and a lock of my hair fell free. I raised my hand to brush it off, but Atropos beat me to it. "Hold still, please."

Atropos was true to her word, and left me with a long, layered cut that flattered my face. I paid Clotho, leaving a hefty gratuity, and made another appointment for next month after reclaiming my blade. I also got the name of the album I heard playing: *Perpetual Titanomachia* by Tartarus.

Unable to decide on a restaurant for dinner, I settled for an Agni Burger before I turned back toward home. As I followed my lengthening shadow, I heard footsteps behind me. Two men followed me at first, and two more joined them. After a block, I turned to confront them. "Is there a problem, gentlemen?"

All four were in decent shape. Each wore a gladius on his hip and civilian clothes, which suggested they were off-duty cops. They were rough, square-jawed men with squat bodies and thick, grasping hands. The tallest stepped forward, a hand on his hilt. "You that white-haired bitch who got a bunch of our friends from the East End suspended without pay?"

I shrugged. "They got themselves suspended through their inability to respect individual rights."

The leader glanced at his friends. "Lift the suspension. Now."

I used my implant to scan the street while messaging the Phoenix Society to request backup. If I managed to deal with these clowns on my own, great, but a sword or two beside me wouldn't go amiss. "I don't have the authority to rescind the suspension."

"I think you just aren't willing. Maybe you look down on us?"

I shook my head. This situation had begun to remind me of the elder Dumas' romances. Was I a lone Musketeer against four of Cardinal Richelieu's soldiers? "I think you're looking for an excuse to escalate the rivalry between MEPOL and the Phoenix Society."

"Nah. We just think you're a stuck-up bitch who needs to know her place."

I glanced at the speaker, who had begun circling to my right. "And you think you're the men to teach me my place?"

"Oh, don't you worry about that." A cop circling to my left spoke. "We only go for human women, not freaks like you."

"Come on, guys. I'm a freak like her, too." The cop who hadn't spoken yet did so. I got a better look at him, and realized he had CPMD. "This really isn't a good idea. She was just doing her job."

I nodded to him. "Thank you."

The other cops rounded on him. "Who the fuck are you trying to impress with the white knight act, Carson? You're going to side with this harpy because she's a pussycat like you? What the hell for? She's probably a bloody lezzer."

Carson drew his gladius. "You said you just wanted to talk to her, but now you're ready to start a fight. This isn't right, and you goddamn well know it."

I sighed, and drew my sword as well. Two against three was better than one against four, but I would have preferred to settle this without violence. "Gentlemen, we should all go home and get a good night's rest. In the morning, you can appeal directly to the Phoenix Society. I won't mention this incident."

A cry pierced the dusk, and Carson crumpled to his knees, clutching at the stab wound in his belly. I speared one man through the shoulder before withdrawing my blade. I sidestepped a thrust from one of the remaining cops and slashed open his coat, leaving a bloody gash across his chest.

The cop who first spoke to me picked up a fallen blade, and came at me with a weapon in each hand. I caught him in the belly with a lunge. Hearing a snarl behind me, I spun to face the man whose chest I sliced. He glared at me while pressing his free hand against his wound. "You murderous whore. I'm gonna —"

I pierced the ligament in his elbow, and he dropped his sword. "I haven't murdered anybody, yet. If you get medical attention in time, you'll live."

Sirens filled the air. Two ambulances, a MEPOL patrol car, and a Phoenix Society staff car screeched to a halt beside us. I cleaned my blade and sheathed it, turning my back on the fallen off-duty cops. As paramedics began triage, I held up my empty hands and decided to get out of London at the first opportunity. This was no place for a holiday.

## Track 08: The Heavy - “Oh No! Not You Again!”

Despite my resolve, I was unable to leave London for several days. Not only was I obliged to wait for the Phoenix Society's official determination that I had acted in self-defense when fighting those off-duty arseholes, but the transit union strike spread globally. To top everything off, my period proved painful enough to prompt a visit to my gynecologist, who removed my IUD for safety's sake. Was I glad she did; turned out the device began to degrade abnormally early.

I booked tickets for the first maglev to New York, packed my bag, and took it easy for a few days. Jacqueline and some of my artsy friends came to visit, and we put on an impromptu, gender-swapped production of that Scottish play with me as the usurper and Jackie as Macduff. We performed in front of my building, made a hell of a racket, and had too much fun to give a damn.

Jackie came with me to Victoria Station the next morning to see me off, since the transit strike was finally over. "You sure you're going to be OK in New York, Nims?"

"I went to school there, remember?" In fact, I went to two schools. I balanced my time between the Juilliard Conservatory and ACS, where I received edited accounts of the exploits of early Adversaries like Iris Deschat for inspiration. Instead of asking Jackie if Director Chattan shared any juicy stories with her, I patted the hilt of my sword. "I'll be fine."

"All right. Just don't do anything I wouldn't do." Jackie winked at me, knowing damn well I was far more restrained.

For example, I caught Jackie and her vicar boyfriend in my kitchen sharing a three-way kiss with the actor who played the role traditionally given to Lady Macbeth in our little production. "Considering what I saw last night, your admonition gives me a great deal of latitude."

"Yeah, sorry about that. We were all a bit drunk."

I shrugged, not about to admit I lay awake imagining two men showering their attentions on me because of the scene I witnessed. "I didn't mind. It's not like I found you three in my bed."

Jackie smiled. "We were tempted, but I figured you wouldn't appreciate it."

"Thanks for being the voice of reason."

"You're welcome." Jackie glanced behind us, and her eyes widened. She grabbed my arm. "Holy shit. You wouldn't believe who just showed up, Nims."

Because seeing is disbelieving, I looked toward the entrance. Oh, damn. John was here with his fiancée, and some slag who seemed to be hounding them. Was she paparazzi? Did Jackie somehow arrange this, or am I just being paranoid? "Jackie, let's leave them alone."

"Hell no!" She pulled harder, dragging me along until we blocked John's path. She gave John a slow, cynical once-over before turning to me. "I can't believe you settled for this."

"I'm sorry, Naomi. I didn't think I'd meet you here." John seemed to be trying to forestall a response to Jacqueline's remark. "I suppose I should introduce you. This is my fiancée, Christine Pennington. Christine, this is —"

I flashed a smile at Jacqueline and offered Christine my hand. "I'm the other woman."

John's expression was priceless, and Christine stared at me as if unsure of what to make of me. "I beg your pardon? Did you just imply that John cheated on me?"

Jacqueline studied Christine a moment, as if deciding whether she deserved an explanation. "John took you for a test drive while still in a relationship with my friend. As far as we're concerned, you're the other woman, but Naomi's trying to be gracious."

John spread his hands, as if appealing for mercy. "Ladies, I hardly think this is appropriate."

"Shut up. I want to hear this." Christine turned back to me, ignoring her fiancé. "Is your friend telling the truth, Ms. —"

"Adversary Naomi Bradleigh." I offered my hand again, and this time Christine shook it. "Unfortunately, Jacqueline told the truth. John and I had been dating for a little over a year when he met you. After deciding you would prove tolerable as a wife, he came to me.

"Unaware of his arrangement with you, I let him into my bed. Afterward, he asked me if I wanted to be his mistress, whereupon I learned he agreed to marry you, and had already been with you."

Christine's head took on a slight tilt as she considered my explanation. For some reason she reminded me of an actress from an Jane Austen adaptation. "So, let me see if I understood. He cheated on me with you, but was also cheating on you with me."

Christine smiled at me before catching John by his collar, kissing him, and using her knee to ram his balls back up into his belly. He crumpled to the floor, his breathless sobs barely audible, as she ripped the engagement ring from her finger and dropped it on him. "I'd be within my rights to keep this, but I want nothing of yours."

"Oh, this is perfect." I turned, and found the woman who had been stalking John and Christine earlier. "Tell me, Adversary Bradleigh, do you enjoy breaking up engagements between your betters?"

Jackie came to my aid again. "Bitch, please. Nims wanted to leave them alone. Who the fuck are you, anyway?"

"Oh, I'm sorry. I'm Alice Talbot, from the London Social Register. And you must be Adversary Jacqueline Russo. Does that vicar's congregation know what you do with him at night?"

"It's none of their business, or yours, unless you want to join in."

Talbot flashed a sly smile before turning to Christine. "Ms. Pennington, can you offer some insight into what it's like to realize your husband-to-be kept a mistress with CPMD from you?"

"I've no idea what you're on about." Christine turned to me and Jackie. "You two are Adversaries, are you not? Would you care to remind Ms. Talbot of our right to privacy?"

I let my sword-hand hover over the hilt, as if I were ready to draw. "Go chase the March Hare, Alice. I hear he and the Mad Hatter take turns servicing the Queen of Hearts. Surely that's the sort of high-society gossip your readers crave."

We let Talbot mull that over, and escaped her by ducking into a café. Christine was kind enough to do the buying. We chatted until the station AI pinged me. "Adversary Bradleigh, the Tradewinds Atlantic Express is now boarding."

As I rose to take my leave, Christine offered Jacqueline and me her card. I glanced at it before slipping it into a pocket. "What manner of antiques are your specialty?"

"Weapons." Christine glanced at my sword while Jackie ducked into the ladies'. "Is that a Nakajima SDL Mark One?"

"I doubt it. It's a custom model." I never thought my weapon might merit her interest, but to humor her I drew the blade to display the maker's mark. The elegantly rendered column of script read, "Forged for Naomi Bradleigh by Nakajima Kaoru".

"It's beautiful." The reverence in Christine's voice surprised me. "Do you use this blade on duty?"

I shrugged. "Of course."

"I suppose I should have expected that answer." The reverence left her voice. I sheathed my blade as Jacqueline returned. "Nobody wears a sword they're not prepared to use in a fight. If you come across another piece, however..."

I flashed back to Maestro's rapier, still hidden in my closet. "I'll be sure to keep you in mind, Christine. Thank you for the coffee, but I should go."

I boarded my maglev after giving Jacqueline a parting hug, and stowed my bag in the semi-private compartment's overhead storage rack with plenty of time to spare. I settled into one of the plush leather seats, and was about to crack open a paperback I grabbed from the station's lending rack when a girl's voice startled me. "Holy crispy crap, mom. It's Cecilia Harvey from Last Reverie!"

I glanced at the aisle, and saw an auburn-haired girl of about eight staring into my compartment. She wore a bomber jacket over a purple dress speckled with white stars and little black ankle boots. A plush Programmer Cat nestled in the crook of her arm. Her mother finished stowing the bag in her hands and looked out from the compartment opposite mine. "Claire, it isn't polite to stare."

"It's fine, ma'am." I scooted over to Claire. "My younger brother is a Last Reverie fan. He tells me Cecilia's a brave knight who loves her king, rescues him time and time again, and—"

"No spoilers!" Claire covered her ears and stomped her foot. "It's not fair. I didn't get to play enough of the game to see any of that for myself."

God, she sounded like Nathan when I managed to read an installment of *The Continuing Misadventures of Programmer Cat* before him. "I'm sorry, Claire. I didn't realize."

Claire continued to pout until her mother intervened. "Claire, the nice lady apologized. What do we say?"

She sniffled, and looked at her mother before turning back to me. "I'm sorry, too. Fuckdammit, that was rude of me." She brightened a bit. "Oh, bollocks. I didn't even ask your name."

I offered the salty-tongued little fangirl my hand. "I'm Naomi Bradleigh. Keep this to yourself, but I'm actually an Adversary. I snuck out so I could have a holiday." Claire perked up, and tugged her mother's arm. "Holy shitballs, Mom. She really is a knight."

Her mother sighed. "I'm sorry. I keep trying to teach Claire to watch her mouth. I just can't explain where she gets it."

"Your little girl reminds me of a friend of mine."

"Are her tits as big as yours?"

I smiled at Claire's long-suffering mother as she sighed and shook her head. If Claire was this bawdy as a little girl, I doubt her parents looked forward to her adolescence. Even if they could find a nunnery in which to confine her, I suspect she'd corrupt even the most devoted by sheer force of will and personality. "It's fine, ma'am. I'm not offended."

She smiled at me, and offered her hand. "I'm Lucy Ashecroft. I suppose this'll be a long trip."

I shook Lucy's hand before glancing at Claire. She had settled beside me with a laptop to play what appeared to be a game of global thermonuclear war. Hopefully it was just a primitive simulation. The last thing I needed was for New York to not be there when we arrived.

## Track 09: Duke Ellington — “Solitude”

The journey to New York was hardly as long as Lucy Ashecroft predicted. Which just proved Lucy didn't understand her daughter's fundamental problem. The girl was lonely, and related better to adults than she did to kids her own age.

I could sympathize; I was little different. Neither of us had any notion of how to be little girls, so we tried to fake it while masking our impatience to escape childhood. I found my escape through music. I suspected Claire would find hers through tech.

Beyond having booked passage and a couple of nights lodging in central Manhattan, I had no definite plans for my leave. I figured I'd hit Midtown and find something to do after I checked in and dropped off my bag in my room. However, the events and attractions display in the Hellfire Club's lobby cycled through its programming without catching my interest.

I didn't want to take a bus tour of Manhattan, too familiar with the city from my student days when I split my time between the Juilliard Conservatory at Lincoln Center and Adversary Candidate School at the old Fordham University campus. Broadway offered nothing I hadn't seen back home. My implant's memory still held photos of me and my friends from ACS at the Statue of Liberty. And I felt too restless and energetic to wander the city's museums.

A sign on the hotel bar's door caught my eye: 'Pianist Wanted'. I removed the sign from the door, sat at the bar, and placed it before the bartender. "I play, and I'm available tonight and tomorrow. Who should I contact concerning an audition?"

The bartender studied me a moment. His voice sounded like one made for crooning. "The piano's behind you, miss. Show me what you've got."

I caressed the baby grand's keys before sitting down. It was a pre-Nationfall antique, lovingly maintained and perfectly tuned. The presence of such a venerable instrument in the hotel bar suggested a refined clientèle. I tried some jazz, playing a few standards from memory before beginning to improvise, and continued until I became conscious of the bartender's presence beside me.

He seemed pleased with me. "I'll need you to play from six to ten. A hundred milligrams a night plus tips. Sound good?"

I checked the time. It was one in the afternoon. "Sounds fair. Anything else?"

The bartender nodded. "One more thing. Do you have anything formal to wear?"

That's what I get for letting caprice guide me. "I'll have to buy something, and I suppose you'll want me to leave the sword in my room."

"I'll keep it behind the bar for you. Yell if you need it."

"I can live with that." I took my leave until six, and caught a cab to shop for appropriate wear. An ankle-length black dress with a sweetheart neckline at a boutique called Frigga's Loom caught my eye, and I paid extra to have it fitted and fabricated within the hour.

I made a week's salary that night, and double the next. Word must have spread. The money meant less than the opportunity to perform in front of an audience not comprised of family and friends. Playing for the bar's patrons offered a thrill of power I could enjoy without guilt. Their hushed attention was adoration, their rapt gazes—caresses.

When I was done, I longed for a lover who would adore me with more than his hushed attention and rapt gaze. I found several handsome men among the hotel bar's guests, but I couldn't bring myself to approach any of them and invite them to my room. I wanted more from a man than a night of pleasure.

Instead of chatting, I claimed a stool at the bar and ordered a glass of wine. I listened as a pair of women beside me discussed resettlement efforts.

I'm not sure why people are bothering to fill in the old towns between New York and Pittsburgh instead of spreading out west, but I won't complain.

"Plenty of good farms in between, especially around Clarion. Ever been there?"

"No. You?"

"Last year for the fair. Some of the locals started a rock band, and they were pretty tight. Not sure I'd go back, though."

"How come?"

"A couple of people disappeared while I was there. They were visitors, like me. The locals searched the woods, but eventually shrugged it off and went back to their business. One of 'em turned up a week later, but not the other."

"Sounds creepy. I'm surprised the Phoenix Society hasn't gotten involved."

So was I. I reclaimed my sword from the bartender, and returned to my room for privacy and a change of clothes. After putting away my dress and shoes, I called Malkuth using the screen built into the wall opposite my too-large-for-one bed.

The AI seemed surprised to see me. "Did you miss me, Naomi?"

"Yes, but I didn't call because I was lonely." I suppose I was flirting a bit with Mal, but I doubted it would do any harm.

"Do tell."

"Can you tell me anything about disappearances in a town called Clarion? It's situated between New York and Pittsburgh."

Malkuth's formerly interested expression darkened, but his presence seemed to fade. It was as if somebody had caught his attention. I waited a couple of minutes, and was about to speak before he refocused on me. "I'm sorry, Adversary Bradleigh, but you're not cleared for any information related to the town of Clarion."

"What do you mean, I'm not cleared?" I was more curious than indignant; I had never heard of an Adversary being denied access to information on any grounds other than privacy rights. Talk of clearance smacked of pre-Nationfall espionage dramas.

Malkuth shook his head. "I'm not permitted to explain. Orders from the Executive Council. Sorry, Naomi."

"I understand. Sorry if I caused you any trouble. I just overheard a conversation between a couple of businesswomen at the hotel bar, and got curious." Why would the Executive Council order Malkuth to hide information about Clarion from me?

I took my leave of Malkuth, and decided to nip back down to the hotel bar. The businesswomen I overheard earlier had left, and the bar had emptied out a bit. As I claimed a stool, a young man settled beside me and cleared his throat. "Hello. I saw you play earlier. You're amazing."

I smiled at him. He was a handsome kid, though his manner suggested he was still a bit shy around women. "Thank you."

He glanced behind me. I discreetly followed his gaze to a table crowded with youths egging him on. "They're my friends. I just got my degree, and they dared me to buy you a drink and hit on you."

"Perhaps I should buy you a drink, instead. You seem nervous." I smiled at him, and gently touched his hand. "It's all right. What's your name, anyway?"

"Cliff." He blushed, and looked at the bar. "How did you know?"

"I have brothers." I didn't mention that they told me tales of their own amorous adventures to ensure I was forewarned, and thus forearmed. "Also, I'm an Adversary."

That got Cliff's attention. "No way. You're an incredible musician, and an Adversary?"

His awestruck expression made him seem too young to have just earned his degree. It reminded me of little Claire. I nodded. "Your heart really isn't in this game, is it?"

He shook his head. "I have a girlfriend, but she's visiting her family tonight and my friends thought I could do better if I'd just try." He smiled at me. "Thing is, I don't want to do better. I love Isabel."

I motioned the bartender over. "Have a drink on me, while I deal with your friends."

Before he could object, I advanced upon his friends wearing the sauciest smile I could muster. "I need to borrow Cliff for the night. You lot will just have to manage without him for once."

I returned to the bar with a little swagger, and gently touched Cliff's shoulder before whispering in his ear. "When you're done, come with me. I'll sneak you out, and you can get away from those losers."

## Track 10: Judas Priest — “Hell Bent For Leather”

The bartender looked so heartbroken by my departure this morning that I took pity on him and promised to stop for a repeat engagement before returning to London. No doubt Jacqueline would insist he had fallen for me, but I suspect he was infatuated with the metric shitload of money I helped him make.

I hit the streets wondering what I should do with my share of the windfall. Investment was right out. I already do that with a chunk of my Adversary's salary before I pay the bills, and I'll be damned if I'll do boring shit with money I earned on vacation.

What I wanted was something fun, something I could keep to conjure the memories I made with it. Maybe... oh, take a trip to Clarion and poke around, my curiosity well and truly piqued by last night's conversation with Malkuth? Hopping a train to Pittsburgh would be simple enough, but a ticket stub isn't my idea of a good souvenir. I'd paste it into my scrapbook and forget it until I was ready to bore my grandchildren.

A gang of bikers on restored gasoline-powered choppers rumbled to a stop at my street corner. Their rides' idling growl muffled their laugher and conversation. On closer inspection, the group looked a little too clean cut. Instead of an outlaw biker gang, they were just a crew of weekenders trading business suits for leathers. A rider who just needed a woman half his age riding pillion to complete his midlife crisis looked at me and called out, "Hey, sexy! Wanna climb aboard and have the ride of your life?"

The rider's catcall helped me reach a decision. It was time to fulfill a childhood dream and get a horse of my own. An iron horse. I waved at him. "Thanks, mate, but I think I'll get my own ride. Know a good dealer?"

He didn't stick around long enough for me to finish my question, but peeled out with his crew the second the light changed. Bollocks to him, then. If he was that impatient, I doubt he could have given me a halfway decent ride anyway.

Not that I needed him. A cab advertising a Conquest Motorcycles dealer in Hell's Kitchen drove past. Capturing the address with my implant, I found the shortest route from my location in the Upper West Side and set out on foot. It wasn't far, and I did promise Director Chattan I'd keep up with my PT.

I stopped for coffee and a bagel at a dim delicatessen called Maimonides' Deli, which was often full of old gentlemen arguing over chess in half a dozen languages other than English. I used to stop here every morning before classes, and I remembered the clerk. His namesake was a famous philosopher. "Hello, Mr. Spinoza. It's been a while."

Spinoza's dentures flashed as he smiled. "Medium black coffee, and a toasted everything bagel with plain cream cheese. Aren't you late for class, Ms. Bradleigh?"

I laughed as I paid him. "I graduated a couple of years ago, and was assigned to the London chapter."

"Ah! I remember now. You made a point of stopping in to tell me. Are you happy?" He handed me my coffee and bagel.

Rather than answer immediately, I tried my bagel. It was as good as I remembered, the crunch of rich dough topped with sea salt, poppy and sesame seeds, and roasted onion and garlic contrasting with the slightly salty-sweet cream cheese. The coffee was perfect, and blacker than Sabbath. "I'm content for now. Did you know I haven't been able to find a decent bagel anywhere in London?"

Mr. Spinoza chuckled. "You should come back to New York, then. I could introduce you to my grandson. He sells motorcycles. He makes good money, and you could focus on your music."

His suggestion was such an old-fashioned sentiment for the end of the twenty-first century that it seemed almost ridiculous, but he meant well. "Does he sell Conquests here in Hell's Kitchen, by any chance? I'm on my way to buy a cycle and ride west."

"I'll tell him to expect you. I'm sure Jacob will deal with you personally, instead of leaving you to some clerk who will give you the hard sell on a bike that isn't perfect for you."

Some new customers walked in, so I stepped aside to give them access to the counter. "I'd appreciate that, Mr. Spinoza. It was good to see you again."

"Have a good day, Naomi."

The rest of my walk was slow and pleasant as I ate my breakfast. Before I knew it, I had arrived at Spinoza Motors with my half-finished coffee still in hand. A man resembling Mr. Spinoza finished his conversation with one of his sales staff before coming to greet me. "You must be Naomi Bradleigh. Papa Baruch didn't tell me you'd be gorgeous. I'm Jacob Spinoza."

"What did he tell you, Mr. Spinoza?"

"Just that I was to treat you right." Spinoza chuckled as he opened the door to his office and beckoned me inside. "Said he didn't want me delegating you to one of my staff lest they try to sell you on a Vestal."

My imagination drew a blank as I tried to visualize myself riding a Vestal. No doubt I'd look prim and proper riding such a dainty little scooter, but it wouldn't be me. Rather than follow him into his office, I cut to the chase. "I appreciate your personal attention. Can you show me your Conquests?"

"A Conquest?" Spinoza studied me for a moment. "Yeah, I can see it. You know what? I've got a model that might be perfect for you out back." He let the office door snick shut behind him and led me to a rear exit near the garage's waiting area.

At least twenty Conquest Type C bikes leaned on the kickstands, parked side-by-side. All but one was black, and indistinguishable from the model shown in all of Conquest's advertising. Conquest Motorcycles only made one type of motorcycle, and you could have it in any color you liked as long as you liked black.

The lone exception stood apart from the others. It sat lower, to caress the road. The suspension looked capable of providing a smooth ride across lunar regolith. Part of the frame had been cut away to accommodate bigger batteries and a more powerful motor. Crimson paint and polished chrome flashed in the sun, challenging me to mount up. "It's gorgeous. May I try it out?"

Jacob produced a key fob and tossed it to me. "Of course. Mind if I ride pillion?"

After we returned from our test ride, I flashed my best stage smile at Jacob while caressing the leather seat. "Tell me more."

He cleared his throat. "The NDA won't let me name names, but this was a custom job for a certain rock musician. He paid half as a deposit, but died in a helicopter crash a couple weeks ago. His estate wouldn't pay the rest or accept delivery."

I could guess at who Jacob meant and its implication on the price expected, but it wasn't germane to the discussion. The relevant fact was that Jacob Spinoza had a custom job he wanted to move, and he would use the implication of star power to jack the price up. "How much did he owe?"

"He owed fifty grams."

Fifty? *Fifty!* Fifty grams when I got my coffee and bagel for two point five milligrams in the middle of fucking Manhattan?! There was no way in any of the hells imagined by humanity I was going to pay such an exorbitant sum. I could buy three bog-standard Conquest Type Cs with money to spare for lunch, tolls, and a down payment on a three-bedroom house outside London for the price this slick bastard was trying to extort.

I closed the distance between us, and picked a bit of lint from his jacket. "I hope you can offer me a better deal than that, Mr. Spinoza. I'm willing to bet you turned a modest profit already from the deposit."

Jacob shook his head. "I'm sorry, Ms. Bradleigh, but I'm still five grams in the hole. The battery and engine are also custom work. You can cross five hundred kilometers in two hours before you need to recharge. You can go even further if you don't go above a hundred and twenty an hour."

"Ten grams sounds reasonable. Half of that is profit for you, and triple your markup on a plain Type C."

Jacob mastered himself quickly, but I still caught the 'How dare she insult me like that?' expression in the way his eyes tightened for just a moment. I smiled at him and sweetened the deal. "Ten grams in cash. And I still need to buy a helmet."

Jacob shook his head. "I need at least fifteen."

"The hell you do." I stepped away from the chopper, my hand resting on the hilt of my sword. "Seven and a half."

"That's less than your original offer!" Jacob was rather cute when flustered.

"I can go lower if you continue to annoy me." I circled the bike, taking a closer look. "Whether you turn a profit on this deal is no concern of mine, especially since you might be bullshitting me. The recently deceased unnameable celebrity whose estate won't take delivery is an old con."

"Grandpa told me you were this sweet, innocent girl. You're staring me down like you're ready to pull your sword on me."

He was still flustered, and still cute. But if he's going to drag the kindly old man into this, it's time for the claws. "So, you thought you could take advantage of me? Listen, asshole, I don't care if your grandfather is God. Six grams is my final offer."

## Track 11: Bruce Dickinson — “Devil on a Hog”

I did manage to wrangle Spinoza down to six grams before he yielded. Guilt at my harsh treatment nagged at me, but I suppressed it with an effort; he was out to get the best deal he could, just like me. The bike grabbed my heart with the first purr, but I wasn't going to tell him. I needed him to think I was willing to walk out empty-handed.

Dropping a couple hundred milligrams at the accessory shop assuaged what little guilt I felt at ramming such a hard bargain down his throat. After all, I needed money to spare for a helmet, boots, gloves, and a shoulder harness for my sword. By the time I stopped for a rest on I-80 a hundred kilometers west of New York, I felt pretty damn good.

The rest stop was an island of commerce carved out of the forest that had encroached upon the interprovincial highway after Nationfall. I had my choice of fast food at this rest stop between Agni Burger, Eight Immortals Buffet, Apollo Coffee, and Borgia Pizza. Something I always wondered about the latter: if you griped about the service, would your next meal be your last?

A brief aside concerning Nationfall for those who would rather repeat history than study it. Damn near everything fell apart in 2048 after the world's governments, corporations, and organized religions started pushing psychiatric nanotech called The Patch. They said it would fix humanity's problems. They lied, unless a close brush with extinction constitutes a fix.

Mum and Dad don't talk about it, but they survived a nanotech-induced zombie apocalypse as *little kids*. If I wanted to top that kind of badassery, I think I'd have to arrest God for crimes against humanity and drag his arse down to earth to stand trial.

I wasn't hungry, or inclined to epic feats of courage, so I ducked into the ladies. By some miracle of janitorial effort, the bathroom looked clean enough to eat in. For a nominal fee I could have rented a locker, stripped, and had a shower before resuming my journey — assuming I was too strapped to just rent a room for the night. The nearby motel even had a discount on the honeymoon suite.

My implant notified me of an incoming call from Baruch Spinoza on my way out. What could he want? Only one way to find out. I pulled my phone, which connected to my implant and served mainly to prevent people from thinking I was talking to myself, and sat down. "Hello?"

"Hello, Naomi. How d'you like your chopper?"

"I almost feel bad about how I bargained with your grandson."

Spinoza gave a wheezy chuckle. "Don't worry about him. He pocketed five grams off that deal."

Dammit, I should have driven a harder bargain. It might have been rude to laugh at the old man, but hearing Jacob didn't make out as badly as I thought was a relief. "Good for him."

A sigh on the other end. "I guess you won't be meeting him for dinner when you come back to the city."

Me, date Jacob Spinoza? Sure. Right after Hell freezes over. Or the rest of Hell, if Dante wasn't just making it all up. "I think I'll just stop by for my usual before I catch the maglev home."

I stepped outside after he hung up, and found another motorcycle charging in the stall behind mine. Its chrome was dull, the front tire worn, and one of the mirrors remained attached through a combination of desperation and duct tape. The rider was equally disreputable. He squinted at me through a haze as he smoked what had to be the fattest blunt known to man.

The wind shifted as he studied me. Only the rifle barrel peeking over his shoulder kept me from dismissing him as a lecherous old stoner. Any Adversary who paid attention to scuttlebutt would recognize the sleazy-looking old biker carrying a rifle with a slim barrel and slotted flash suppressor. What the hell was Edmund Cohen doing here?

He tapped the ashes from his blunt, careless of where the wind blew them. "I can see why Malkuth wants a hardware upgrade."

"I'm not sure why that's any concern of yours, sir. You haven't even introduced yourself."

Holy shit. He actually looked embarrassed. Holding his blunt out to his side, he bowed from the waist. "Sorry about that, Adversary Bradleigh. I'm Edmund Cohen. Most of what you've heard about me is pure slander, I promise."

"Even the flattering things?" Not that I've heard much to flatter Cohen. His saving grace is his skill with a rifle. He's a good man to have at your back in a firefight or a pub crawl, but don't lend him money or leave him alone with your girlfriend—or your boyfriend.

He flashed a handsome smile that made him resemble an espionage film hero. "Especially those."

My guard was up because I was alone on the road with only a sword for protection, but Cohen's self-deprecating humor eased me a little. I offered my hand. "I'd introduce myself, but Malkuth beat me to it. What else did he tell you?"

"Just enough to pique my interest." Cohen glanced at his dashboard. "I'm heading west as far as the I-81 exit. Mind if I ride with you a bit?"

"No harm in it, I suppose." Glancing over my shoulder as I mounted up, I caught the old lech perving on me just like I suspected he would. Should I blow him a kiss? No, that's too Jacqueline. "Sure you can keep up?"

I left him there, hitting the on ramp at the current recommended speed. Aside from occasional RVs I passed as they trundled along in the right lane, I had the highway to myself. The wind played with my hair, streaming it behind me as it pressed my sunglasses against my face. Though it hurt a little and would surely leave marks, I didn't care. Astride my Conquest, I was young and strong; no power on earth could oppose me.

No power save Edmund Cohen. He finally caught up with me, and requested a secure talk session. Though our engines were all but silent, using our implants was still easier than shouting at each other over the wind. 「What do you want?」

「Malkuth told me you were interested in Clarion. Why?」

Hmm... He isn't flirting, or pissing about with small talk now that we're alone. Why is that? Only one way to find out. 「I heard about some unsolved disappearances. I'm curious as to why nobody seems to give a shit.」

「You know what curiosity did to the cat, right?」

「I understand the cat got better.」 Time to try a gamble while I had the old man's attention. 「I also understand you're executive council. Think you can tell me anything about Clarion?」

「Sorry, but you're not cleared. In fact, I've got orders to persuade you to spend your vacation somewhere else. Clarion isn't your problem.」

Catching sight of the road signs ahead, I opened the throttle and left Cohen behind. 「You're going to miss your exit, Eddie.」

「Shit!」 He swerved to get onto the ramp, and I thought for a moment he might lose control. 「At least call Saul Rosenbaum at the New York chapter for backup if you find anything!」

The session cut out. He must have used a near-field connection, implant to implant, instead of the network. Who the hell does Cohen think I am, anyway? Of course I'll call the local office for backup if I find something real in Clarion. I might be too curious for my own good, but I'm not a demon-ridden idiot. Hell, I'll probably call Rosenbaum when I get there as a professional courtesy.

My sunglasses proved a wise purchase as the sun led me westward to an exit to Route 62. Trafficnet advised me to take the exit, and to expect a rougher road than I-80. Rougher was something of an understatement. Route 62 had not yet been modernized, so it was nothing but faded asphalt with freshly painted lines and black patches where maintenance crews filled in potholes.

Horse-pulled buggies filled with Pennsylvania Deutsch families slowed my progress every couple of kilometers. Having never shared a road with carriages before, I decelerated to avoid spooking the animals.

The road emptied once I passed the last farm and drove into an old forest threatening to encroach upon the highway. I finally pulled over to the narrow shoulder and stopped to remove my sunglasses.

A pair of deer began mating in the middle of the road. While I could ride around them, a truck barrelling down the highway might just flatten them. "Oi, Bambi!" The buck turned his head to gaze on me, but maintained his position. "Did you two have to start shagging in the middle of the bloody highway?"

I doubt the doe understood me, but she pulled free of her suitor's embrace. He remained, hard and frustrated, as she bounded off into the woods on the opposite side of the road. I snapped a photo and sent it to Jacqueline with a message: "They grow 'em big over here." She'd get a kick out of that.

The buck stared at me a moment before lowering his head to threaten me with his antlers. Well, I suppose I did cockblock the poor bastard. I turned on my bike's V-Twin emulation and revved the engine. The rumbling growl of a gasoline-powered chopper shattered the silence, startling the buck into bolting after his lost mate.

The forest eventually yielded to more farmland. Instead of slowing to pass buggies, the buggies pulled over to yield to me. No doubt they heard me coming. The Conquest purred beneath me as I rode into Clarion at a bicycle's pace to avoid hitting pedestrians.

One child saw me and began pulling at his mother's arm. "Mommylookit! It's Cecilia Harvey on a hog!"

Oh, dear. Being compared to a videogame character was cute when little Claire did it, but it was getting old fast. Perhaps I need a different hairstyle. The mother turned to pay attention to her son, so I stopped beside her. "Excuse me, ma'am. Is there somewhere I could stay overnight?"

She pointed down the road. "Try The Lonely Mountain." Before I could thank her, she led her son away to continue on her business. So much for country hospitality.

Despite being a small town, Clarion's main street bustled in a manner that made me a little homesick. They had everything here, even a nerd shop called Kaylee's Shiny Games, Hobbies, and Crafts.

The window at Kaylee's displayed a poster for the new edition of *Advanced Catacombs & Chimeras*, a tabletop game I remembered from university. Another window was devoted to coming soon posters for computer games like *Nationfall: Final War* and *True Goddess Metempsychosis III - Call of the Lightbringer*. The latter seemed to involve yet another demonic invasion of Tokyo. I swear, the place must be accursed.

A plump brunette in overalls and a Pulsecannon t-shirt leaned against the entrance while polishing some kind of game miniature that resembled a grotesque porcine creature wielding a minigun in each hand. Her eyes widened as I passed by. "Excuse me! Did you know you look just like —"

I pulled over so I could talk without holding up traffic. "Cecilia Harvey? I get that a lot lately. It must be the hair."

"I was gonna say you look like Lady Frostmane. From the samurai movies by Ryuhei Miyamoto? All you need is a katana and a kimono." My utter ignorance of the work of Ryuhei Miyamoto must have been painted across my face, because she stopped geeking out and approached. "Welcome to Clarion. I'm Kaylee Chambers."

I offered my hand. "Naomi Bradleigh. Can you tell me where to find The Lonely Mountain?"

Clutching her miniature in her polishing hand, she gave mine a hearty shake. "Give me a minute to lock up and I'll show you the way. Nothing like a beer after work, right?"

## Track 12: Jadis — “Touch”

Homesickness struck like a fist in the gut as Kaylee and I arrived at The Lonely Mountain on the other side of Clarion’s main street. A hand-painted wooden sign swayed in the breeze, depicting a single peak against a far horizon. Underneath, it read “B. & D. Halford, Proprietors.”

The building resembled a traditional English pub in almost every detail. A wrought iron fence surrounded a quaint beer garden abuzz with bees competing with a riot of butterflies for nectar. Flagstones led from the open gates to a round door. There was even a sign in the window nearest the door advertising rooms to let, rather than rooms to rent.

The door closed behind us in a soft clangor of bells. A stereo played some kind of mellow progressive rock. The barkeep reached for a pint and began filling it as we approached the bar. “Here’s your usual, Kaylee. What can I get your friend?”

“We just met. Ask her.” Kaylee downed a third of her pint in one go, and smacked her lips. “Damn, I needed that.”

I claimed a stool next to Kaylee. A voice from the stereo was crooning something about gold everywhere he turned. It seemed fitting, given the season. “A glass of your house red, please. What do you have playing?”

“The band’s called Charn.” The bartender opened a fresh bottle, and put out a dish of mixed nuts. “Are you’re new here?”

I sipped my wine before answering. “Rode in a little while ago. I'm here for a holiday. Sign out front says you’ve rooms available.”

Halford nodded. “It's seventy milligrams a night, breakfast included. We change sheets and towels every other night. How long did you plan to stay?”

Pulling out my wallet, I counted out 500mg of gold in banknotes and pushed it toward him. Though I could have charged it directly to my account, just about everybody prefers to be paid in cash to avoid the transaction tax levied by the Phoenix Society, despite the tax being only 0.01%. “This should cover me for a week, and pay for my beer. You can keep the change. Can we talk extensions if I need to stay longer?”

“Of course.” Halford counted the cash, and nodded. “Just a moment, please.”

He soon returned with a receipt and a key on a numbered fob, which I promptly pocketed. “Anything else I can do for you ladies?”

Kaylee nodded. “How about dinner?”

I scanned the common room, and found a table by the window that afforded a good view of the street while also letting me observe the patrons. “Mind if we sit by the window?”

“Go ahead.” Halford grabbed a couple of menus and followed us. A huge Irish wolfhound looked up, giving us a forlorn look as we passed the hearth. It whined softly, begging to be rescued from the two black kittens draped contentedly across his back.

As we ate, Kaylee regaled me with stories about the townspeople, starting with our host. It seems Bruce Halford refereed a weekly Catacombs & Chimeras game every Sunday here in the common room while his husband took a turn behind the bar. In return, I told her about London and life as an Adversary. By the time Bruce came by with the dessert menu, I was convinced I had made a friend here in town.

Kaylee studied me a moment, her fork poised over her slice of steaming apple pie as I sipped my coffee. “You sure you don’t want a piece? Dick Halford makes a great apple pie.”

“I really shouldn’t. I’ll only regret it later.”

“Raw deal.” Kaylee pointed with her fork at a gaunt gentleman wearing a white lab coat over his shirt and waistcoat. While he might have been a scientist or some sort of technician, his almost military bearing led me to peg him as a physician. “That’s Dr. Petersen. I heard he was one of the first to move back to Clarion after Nationfall. If anybody knows where the bodies are buried, it’s probably him.”

“Why do you say that?”

Kaylee leaned close. “I think he dug a lot of the graves. He used to be in the Commonwealth Army. Now he runs a family practice when not serving as coroner and medical examiner.”

Now that was odd. Why would he return to an empty town to practice medicine? Did he serve nearby during Nationfall? I can understand a former North American Commonwealth soldier, possibly a medic, who runs a family practice. But in London, medical examiners must possess specialized qualifications in forensic pathology. Would that be the case here? Either way, Dr. Petersen was number one with a bullet on my list of people to chat up. “What else can you tell me about him?”

“He goes bow-hunting with Sheriff Robinson and Mayor Cooper every fall.” Kaylee gave me a suspicious look. “Are you on the job?”

Shit. That’s what I get for not quitting while I’m ahead. I’ve got to be more careful if I don’t want a jury wondering why I overstepped my currently non-existent authority. “I’m on leave, but while I was in New York, I overheard a woman who had been here. She mentioned disappearances, and I got curious. Do you know anything?”

“There was that lady who was all over the news, but she eventually turned up. Her boyfriend didn’t, though. Fuckin’ shame, that. They were going to get married.” Kaylee’s face scrunched as she tried to think of something else. “And every now and then some dumbass kid ignores warnings to stay out of the Fort Woods, and doesn’t come back when he said he would. We send out a search party, and find ‘em half the time. You’re a city girl, so you should understand that sometimes people disappear.”

Kaylee’s right. Sometimes people do just disappear, but there’s usually a reason: somebody made them disappear. Did somebody make the kids that couldn’t be found disappear? If so, then who? How? Why?

I didn’t quite catch what Kaylee said. “What was that?”

“I said, there goes Dr. Petersen now.”

I put some banknotes on the table. “Sorry to run out, but this is too good an opportunity. This should cover the check, with a tip. Is there a back door?”

“You’re going after Petersen?” Kaylee pointed the way instead of waiting for me to answer. I ignored a drunken catcall and plunged into cool autumn night. An alley ran parallel to Main Street, allowing me to keep pace with Dr. Petersen without getting too close.

We walked across town before the alley curved and brought me back to Main Street. Now seemed as good a time to cross Dr. Petersen’s path as any, so I approached him. Because I tend to walk silently and thus sneak up on people without intending to do so, I sang softly to alert him to my presence. I wanted to talk to the man, not put a shank in his back.

“You’ve a lovely voice, young lady.” Petersen turned to me with a confident smile. “I saw you with Ms. Chambers at the Lonely Mountain. Have you been following me?”

“Kaylee told me you were the man to meet if I was curious about Clarion’s history.”

“Are you curious, Miss ―?”

“Bradleigh. Naomi Bradleigh.” No point in denying him my name when my appearance precludes anonymity. Now that I had a good look at his face, I used my implant to search for records. Turns out Kaylee was right about him serving in the North American Commonwealth’s army, but his service record was sealed by order of the Phoenix Society. All I got was name, rank, and serial number. This shit keeps getting weirder. “And you’re Dr. Henrik Petersen. Or should I address you as Colonel Petersen?”

“‘Doctor’ is fine, Adversary Bradleigh.” He flashed a knowing smile. “I couldn’t help but run a search on your name and face. No doubt you did the same with me. Am I the subject of an investigation?”

I shook my head. Dammit, I should have expected he’d search me. And if he’s Sheriff Robinson’s buddy, I’ll probably get to meet him soon, too. “You aren’t. I heard some odd rumors about Clarion, and got curious enough to visit.”

“Hmm.” Dr. Petersen glanced northward, as if thinking of something in the forest beyond. “What manner of odd rumors?”

“Disappearances. Apparently, a couple got lost recently, and only the woman got out safely. Nobody knows what became of her fiancé.”

“It was quite the tragedy. I treated the young lady in question for exposure and malnutrition.” Petersen pushed his glasses up his nose as he spoke. “I’m sorry we weren’t able to find her young man. Do you have a young man, Ms. Bradleigh?”

“A few.” I lied, because hearing such a question at night on an empty street creeped me out. Let Dr. Petersen think what he will, as long as he doesn’t think me easy prey who might vanish without notice. I ran a fingertip down his chest to further disconcert him. “But there’s much to be said for experience and maturity, is there not?”

“At my age, I think you’d be the death of me.” His eyes crinkled as he smiled at my flirtation. He checked his watch before producing a ring bristling with keys. Why would a physician have so many? “Would you come by tomorrow afternoon for coffee? I think I’d enjoy your company.”

Sure. Why not? I’ll drink the old man’s coffee and pick his brain. Maybe poke around his files while he’s in the loo if I can get away with it. A high-ranking soldier with a service record sealed by order of the Phoenix Society had to have a few skeletons in his closet. “Is five o’clock convenient?”

“Perfectly.” The foyer light came to life as he opened the door. “Good night, Ms. Bradleigh.”

## Track 13: The Weather Girls — “It’s Raining Men”

Shielded scarlet streetlamps lit my way back to the Lonely Mountain. I passed locals out for a stroll, their conversations dueling with the songs of the crickets and cicadas that still owned the night because of the false summer. Their melodies serenaded me as I walked past the darkened street windows, so unlike my neighborhood in London.

A fleeting shadow accompanied by a soft rustle of cloth caught my eye in an alley, and I stopped to check it out. I crept toward it with my hand on my sword’s hilt, ready to draw. Before me stood a man in dark camouflage fatigues, his feline eyes a feral yellow in the gloom. His left shoulder bore sergeant’s stripes, and the name badge pinned to his chest identified him as ‘C. Renfield.’

Renfield studied me for a moment before speaking. “Do you have any idea what the moonlight does to your hair?”

As gambits go, that wasn’t half bad. It lacked the simplicity of ‘“Hello, I’m so-and-so,’” but it wasn’t nearly as lame as, ‘“Does God realize you snuck out of Heaven?’” It was almost poetic, which surprised me.

“Maybe you should tell me.” Not that I planned to drag him back to the Lonely Mountain, but he did have a sexy voice and the uniform looked good on him. But why would somebody wear a Commonwealth Army uniform decades after the NAC’s dissolution?

“Surely I’m not the first to notice the moon lends you an ethereal aspect?” He offered his hand. “Their loss, and hopefully my gain. I’m Sergeant Christopher Renfield, NACA. You can't be from around here with that British accent.”

Dismissing his remark about my voice as a slip of the tongue, I shook his hand. It gave me an excuse to check him out. His gaze held an intensity I found a little unnerving. I prefer longer hair on a man, but his body was made for rough handling, and he had a mouth on him I could definitely put to use. Just thinking about him kissing his way up the backs of my legs made me shiver. “Naomi Bradleigh.”

He also had a nice smile, which I captured for future reference. “So, what brings you to the Commonwealth from Britain?”

Britain again? What the hell? I’m a Londoner, not British. The United Queensreach died in Nationfall, like the North American Commonwealth. Turns out there wouldn’t always be an England after all, but her people kept calm and carried on. Why does he think those nations still exist? “I’m not sure I understand, Sergeant.”

“Word from the brass says the British might invade. Are you with them?”

Oh, bloody hell. He must be some kind of war re-enactor who’s still in character. “I’m not with anybody tonight, Sergeant. If you find me during the day, we might arrange a meeting. Sound good?”

“I’d love that, ma’am, but I don’t know when I’ll get the order to deploy.” He closed the distance between us, and slipped an arm around me before I could think to withdraw. His lips were warm and soft upon mine, and lingered long enough to make me want more. “Sorry, ma’am. I should have asked first.”

Damn right. He really shouldn’t have teased me like that. To teach him a lesson, I caught him by the collar, pressed him against the wall, and stole a deeper kiss to show him what he had gotten himself into. I held him there long enough for his hands to find their way to my arse before pushing myself away.

Taking a few steps back gave me a good view of what I had done to him. A purr leaked into my voice. “I’d better go before I take advantage of you, Sergeant.”

“What if I want you to take advantage of me?” Renfield’s voice was low and rough as he pulled me against him with a hand in my hair and the other still gripping my ass. He was ready to take me in the alley, and all I had to do was tell him to go for it. 

He stared into my eyes for a moment before lowering his head. His lips brushed my throat, followed by a gentle graze of teeth that threatened to obliterate all rational thought not pursuant to the goal of getting this unknown soldier to press me up against the wall and fuck me senseless.

Who the hell was this guy? What the hell was wrong with me? Was I just rebounding, or was it because we're both CPMD+ and that triggered some kind of animalistic, pheromonal chemistry between us? Tempting as he was, I didn't want to think of myself as being that easy. I've never been so hot for a man that I couldn't be bothered to consider the consequences, and it scared me a little.

Forcing him off me with a shove that drove him across the alley, I drew my sword. The weight of steel in my hand cooled my ardor and helped me focus. “I’m serious, Christopher. I hardly know you, so regardless of how much we both want it, I’m not ready to play with you tonight.”

Keeping my blade between us, I withdrew from the alley and ran most of the way back to The Lonely Mountain. I paused only when I realized that returning to the pub with a naked sword while looking disheveled and panting was likely to cause a disturbance. I stopped a block away, glancing behind me to confirm nobody was following me, and sheathed my blade. After composing myself using a shop window as a mirror, I walked the rest of the way.

Kaylee was still there, with dual shoulder-mounted kitties. The little black kittens that had draped themselves across the hound now perched on her. Given that kitten claws can be needle-sharp, I doubted she was comfortable. “Looks like you made some friends. Bet the dog’s grateful.”

One of the kittens leaped from her shoulder, and didn't quite make it to mine. Not only was Kaylee down to a single weapon of magical kitty sweetness, but now I had a little purr baby clinging to the sleeve of my jacket, clawing his way up, and leaving marks. Dammit.

“No shit.” Now that Kaylee had an arm free, she exploited her situation and grabbed her beer. “Bruce found ‘em in the barn and named ‘em Dante and Virgil. Dante’s the one climbing you.”

Actually, he was now perched on my shoulder and playing with a zipper. I scratched behind his little ears and tried not to let his purring distract me. “Anything interesting happen while I was out?”

“Depends on your definition. See the kid in the red flannel shirt?” She pointed out a local youth at the pool table.

“Not bad. What’s his story?”

“His name’s Mike Brubaker. His parents run the dairy farm you probably passed on your way in. Some of the younger girls think he’s gay.”

I shrugged. “Is he?”

“Hell, no.” Kaylee flashed a wicked smile, but didn’t elaborate. “He just ignores girls his age. You’re more his type, but I don’t think he’s yours. Speaking of which, how’d you like Doc Petersen?”

“I agreed to meet him for coffee tomorrow afternoon.” Something’s off about Clarion’s general practitioner. I’m sure of it, but I kept that to myself. Kaylee’s willingness to dish could easily work against me, and I didn’t want to either victimize a blameless man or tip off a guilty one. “He’s a bit too old for me, so get your mind out of the gutter.”

“Then who’s the lucky guy?” Agitated by Kaylee’s constant gesturing, Virgil also jumped ship. He sat in front of me, and voiced a pathetic little mew before scampering up to complete my furry arsenal. “You were flushed and practically panting when you came back, so there has to be somebody.”

Was I really that obvious? Doesn’t matter. Here comes the Brubaker boy. He flashed a shy smile. “I have to admit, Adversary Bradleigh, I’m jealous of the kittens.”

Now that was good for a laugh. But who told him I serve? Kaylee? And who else did she tell? “I think you’d be more comfortable sitting in my lap. You must be Michael Brubaker. Kaylee told me about you.”

He reddened a bit, and glanced her way. She smiled behind her glass. “Should I ask how much she told you?”

“She implied you appreciate experience.” The glass in his hand held water with a wedge of lemon. Was Michael too young to drink, or merely abstemious?

Brubaker shook his head and sat at our table without asking for permission. “I heard you’re new in town. I’d love to show you around.”

Say this much for the kid: he’s got balls. Letting him escort me would afford me a view of Clarion I might not get on my own. I offered my hand, careful not to dislodge my purring guardians. “I think I’d enjoy that. But call me Naomi. I’m not on the job.”

Michael managed to shake my hand without turning too unnerving a shade of crimson. “Is eleven in the morning good?”

He fled as soon as I agreed to the time. Kaylee managed to wait until the door slammed shut behind him before squealing. “Awwww! He’s finally growing up. I think you’re the first woman he’s ever asked out.”

Oh, bugger. What have I gotten myself into? “What about you?”

“Pfft. You shittin’ me? I seduced his sweet cornfed ass.” She held up her empty glass, a silent imperious demand. “It was last year at the harvest festival. He managed to ask me to dance, but went mute afterward.” She leaned in, adopting a conspiratorial whisper. “I can point out stallions who would be jealous of him.”

I shook my head, trying to banish the image her words conjured. How was it that wherever I go, I find a Jacqueline ― or a girl well on her way to becoming a Jacqueline? Was it an archetype I attract in the same manner that I seem to attract cats? And who was this blonde girl stalking toward me as if she were ready to throw a gauntlet at my feet?

She stared at me in a frankly appraising manner that I suspected I should find grossly offensive. “Michael is mine. Our parents arranged everything. He just won’t accept it.”

I shrugged, not particularly interested in disputing her claim, though Michael would be justified in filing a complaint with the Phoenix Society if it were true that his parents arranged a marriage for him without his consent. “You’re welcome to him, if you can get his attention. But you might start by brushing up on your manners. I’m Naomi Bradleigh, and who might you be?”

“Jessica Stern. We don’t appreciate out-of-town sluts poaching our men here.”

I shook my head and struggled not to laugh. Unable to hold my silence any longer, I turned to Kaylee. “I think I know why Michael ignores girls his age.”

“I’m right here.”

“Still? Exactly!” I gave an exaggerated sigh.

The shocked, disgusted expression on Jessica’s face suggested she finally grasped my meaning. Poor Mike. She spun on her heel and stalked away, shoving past one of Mike’s lingering friends.

Kaylee burst out laughing. “The look on that little bitch’s face was so fuckin’ priceless.”

The clearing of a masculine throat caught our attention. Halford placed a fresh beer and a glass of red before us. A tall, uniformed man waited behind him. He approached once Halford left, and flashed a badge. “Snow-blonde, scarlet eyes, and an Italian-style sword. You must be Adversary Bradleigh.”

Dammit, who’s telling everybody I’m an Adversary? First Kaylee told the kid, and now the cops know I’m in town. Word really does get around fast.

Regardless, he was handsome if you liked ‘em rugged, with a gravelly voice to match. His uniform looked as good on him as fatigues looked on Renfield. I wouldn’t have minded seeing those hazel eyes staring up at me from between my thighs, but I don’t get involved with cops. It’s a conflict of interest. “And you must be Sheriff Robinson. Who told you I serve?”

“Don’t worry about that. I only want to ascertain your intent.”

I narrowed my eyes. “Is this the welcome all visitors get? It’s not exactly good for tourism.”

“Don’t get cute with me, Ms. Bradleigh.”

Oh, so he wants to be the big dog and mark his territory? Which reminds me, I really should call Rosenbaum and introduce myself. Though I’m sure Cohen or Malkuth have already told him that there’s a very naughty kitty hunting in his garden. “I wanted some quiet time away from the city, and I heard some things about Clarion that piqued my curiosity.”

Robinson took a moment to mull this over. “And I’ve heard some things about you that piqued my curiosity. I’d like to have a look at your room.”

Not bloody likely. I haven’t even been to my room, or done more than pocket my key, but I wasn’t about to let Robinson indulge in a fishing expedition. “I do not consent to a search, Sheriff. Do you have a warrant?”

“No.” Robinson glanced at my sword, as if it meant something to him. “Do us all a favor and try not to give me cause to get one.”

He turned around, treating Kaylee and me to a nice long view of his ass as he walked away. It really was too bad he’s a cop. No doubt Kaylee felt the same, judging by her sigh. “Too bad he’s married. I’d let him do a cavity search.”

“Dammit, Kaylee, I didn’t need that mental image.” I took her beer away, ignoring her protest. “I think it’s time we got you home.”

## Track 14: Joe Satriani — A Door Into Summer

Without wine and unrequited lust to cloud my vision, Michael Brubaker reminded me of my brother Nathan. He was reserved without beer and nearby friends to bolster his confidence, so I resorted to leading questions to draw him out. His answers didn’t give me much, but seemed to help put him at ease.

We followed the town’s namesake river, the Clarion, northeast into the forest. Michael proved voluble once I began asking about unfamiliar plant life. A born woodsman, he seemed pleased to have a companion with whom he could share his knowledge. “Don’t you have trees in London?”

Craning my neck, I stared up at the tops of the white pines towering overhead. “Not quite like this. This is a real forest, old and wild.”

“Not that old.” Michael shook his head. He crouched, and put on a pair of heavy gloves before digging into the soft earth. After a minute’s effort, he pulled out a chunk of asphalt. Part of it was still yellow. “A road used to run this way. See those little hills off in the distance? That’s actually what’s left of a strip mall.”

That sounded like arrant bullshit, so I used my implant to pull pre-Nationfall maps off the network and compare them with current GPS data as we continued our hike. He was right about the road, but I remained skeptical about the forest’s ability to reclaim developed land in mere decades.

「Got a minute, Malkuth? I wanted to ask a couple of questions.」

「Go ahead. Can’t guarantee I’ll answer, though, especially if it’s about Clarion.」

Damn clearance again. Oh, well. His evasions might still prove enlightening. What the bloody hell is the Society so paranoid about? 「I’m curious about the forest northeast of town. I’m hiking along the Clarion River, and my companion showed me that there used to be a road running along our route. I confirmed it using pre-Nationfall maps compared with current GPS data. What happened here?」

「Companion? Did you meet someone already? I’m jealous.」

Was Malkuth playing the human game by flirting, or was he serious? I decided to take him seriously. 「Don’t worry, Mal. He reminds me too much of Nathan. Besides, you’ll always be my favorite AI. I haven’t forgotten that I promised you a date.」

「I’m going to hold you to that, Nims.」

「Yeah, you do that, Mal. In the meantime, why not tell me what happened?」

A pair of large files hit my implant a couple minutes later, labeled “clarion-valley-topo-2048” and “clarion-valley-topo-2049”. I opened them, and compared the two topographic maps. 「There’s an *impact crater* a kilometer east of my position. Meteorite?」

「Worse. There was a protest there seven days, six hours, and fifty-two minutes before the Commonwealth's final collapse. Some NACAF general decided it was an insurrection and decided to use an experimental space-based weapon codenamed GUNGNIR to suppress it.」

Gungnir was the spear of Odin, king of the Aesir. The use of such a name to signify a space-based weapon couldn’t possibly be coincidental. 「GUNGNIR was a kinetic strike system, wasn’t it?」

「Exactly.」 Not that Malkuth was finished. 「It gets worse. GUNGNIR is still out there, along with two other systems codenamed GAEBOLG and LONGINUS. The Society has them under control, but can you imagine what might happen if nation-states arose again and started creating more of these systems?」

Staring skyward, I visualized shafts of tungsten raining down as a hail of javelins. The spear was one of humanity’s first weapons. Was it to be our last? Despite the warmth of late summer, I shuddered. 「Has the Society ever used these weapons?」

「I’m sorry, Naomi, but you aren’t cleared for that information.」 Not that I expected an answer, but a simple ‘no’ would have reassured me.

Uphold individual rights. Root out corruption. Overthrow tyranny. Protect the human race. Impose transparency and accountability on authority. That’s the mission, but who the hell is going to impose transparency and accountability on the Phoenix Society, when we don’t even know who sits on the Executive Council?

“Hey, are you all right?” Brubaker’s voice up ahead dragged me out of my own head.

Not wanting to shout, I dashed upriver to catch up with him. “Sorry. Had an argument with a friend from work.”

“A fellow Adversary?”

Venting my frustration on the kid would be counterproductive now that he was finally opening up. “Further up. Know anything about our AIs, the Sephiroth?”

Brubaker shook his head. He held a finger to his lips before pointing toward the river. My reward for following his direction was a view of river otters at play. They splashed about, chasing each other and catching fish without the slightest care in the world. A black bear lumbered out of the underbrush on the opposite bank and waded in, eager for her share of the trout flashing in the sun-dappled water.

We left the animals to their business and continued upriver for a bit before Brubaker spoke again. “How do you argue with an AI?”

“Very carefully.” It sounded like a punchline, and it got a smile out of Brubaker, but it’s also the truth. “Interactive AIs are better at logic than we are, so you must weigh your words if you want to persuade them to do anything.”

We walked a dozen meters before his next question. “What were you trying to get the AI to do?”

“I had questions about what had happened to this place.”

“Dr. Petersen told us a meteorite fell nearby and flattened the area. Was he wrong?”

Either that, or lying. “It wasn’t a meteorite. The Commonwealth bombarded the area from orbit to suppress some kind of protest.”

“But that’s insane! Why would a government do that? What were they trying to protect?”

Is he really that naive? “Either their own power, or something nearby that they didn’t want falling into the people’s hands.”

“Such as?”

Damned if I know, so I shrugged. “Maybe the Commonwealth had some kind of military installation nearby.”

“It would explain why we call ‘em the Old Fort Woods.” Brubaker drank from his canteen.

I opened the package of bison jerky I bought in town earlier and offered him a piece. He put the entire piece in his mouth and began chewing, stuffing it into one cheek like a deranged carnivorous squirrel. “Damn, that’s good. Did you get this from Three Wolves?”

Rather than talk with my mouth full of jerky, I nodded. The salty spiced meat assaulted me with flavor, so that I was lost in the taste as I chewed. It was tough at first, but quickly became as tender as a good rare steak as I worked moisture back into the meat.

We followed the trail upriver for a couple kilometers in companionable silence before he spoke again. “What’s it like being an Adversary?”

I stopped short, unsure of how to answer. Becoming an Adversary allowed me to attend the Juilliard Conservatory in New York without selling myself into more onerous forms of indentured servitude. It also let me uphold worthy ideals and make great friends.

But being stonewalled by Malkuth at every turn when I need him most? Nope. That’s hardly something to brag about.

He turned to me, and looked me straight in the eye. “I want to know what I’m getting into before I join.”

Stop me if you’ve heard this story before. A small-town kid dreams of seeing the wider world. He meets a more experienced person he can look up to, somebody who’s been around, and starts romanticizing the hell out of her life. “We watch the watchmen. But nobody’s watching us but each other. Make a single mistake on duty, and you could end up dead.”

Brubaker gave a thoughtful nod. “That’s why you carry a sword, right? To protect yourself.”

Not to mention a Kalashnikov, but weapons are of no use at a court martial. What are you going to do, run the judge through? Gun down the jury? That sounds like an excellent way to refute a charge. “It helps, but I wasn’t talking about being killed by a suspect. Adversaries found guilty of violating individual rights are executed.”

“But the death penalty was abolished!” Brubaker lapsed into silence for a bit afterward, and we walked for a while. “Why is capital punishment reserved for Adversaries?”

“We have near-absolute authority while on duty. That authority carries an equal weight of responsibility. It’s too heavy a burden for most people.”

“You seem to handle it well.”

Most of us do, until we can’t any longer. It’s called burnout. I’ve seen good Adversaries hand over their pins because they couldn’t do it any longer, despite the cognitive-behavioral therapy we get between missions to keep the stress under control. “I’ve been lucky so far.”

I fingered the cameo-style pins in my lapels that identified me as an Adversary. Two rattlesnakes coiled around a sword of justice while holding a set of scales in their jaws represented the principles of our service: liberty, justice, and equality for all.

Why did I care about Michael’s decision? Was it because he reminded me of Nathan? “I won’t say it’s not a privilege to serve, and I’m sure you could do a lot of good, but don’t buy into the romance. It’s just a different kind of police work.”

## Track 15: Alice Cooper - “You Drive Me Nervous”

We reached the ruins of a hunting cabin an hour after our conversation about life as an Adversary lapsed. Despite the darkening sky and the rumble of thunder, my reluctance to seek shelter there remained. “Michael, are you sure we should be here?”

I certainly wasn’t. Half the roof was missing, and the rest was charred. The fireplace was so ill-tended that any attempt to use it would most likely burn down the rest of the cabin. Evidence of young lovers using the place for trysts littered a corner, and I glanced at Brubaker. Was one of these used condoms his? Eww.

“It’ll be fine.” He opened a trap door, and began climbing down as lightning split the sky with an almost instantaneous roar. “Underground is safer!”

I followed, because he was right. The cellar was cleaner, too. Much cleaner, as if somebody came down here regularly and kept the place tidy. It even had working lights, once Brubaker felt around and found the switch. Since there wasn’t a couch or any chairs, I settled onto one of the cushions spread around the room. “How did you know about this place?”

“My friends and I found it a few years ago and fixed it up. We’d come here to get away from everybody else. I changed the lock on the trapdoor so that the kids who come here to screw can’t get down here and make a mess of things.”

“Niall and Nigel, a couple of my brothers, had something like that for a while. They built a little shack out in the fields. It wasn’t as fancy as this.” Nor did they have as much porn. At least, I didn’t think they did, but I’m not judging. I held up a disc I recognized because Jacqueline gave it to me. Taking a mock-serious tone, I showed Brubaker my find. “Take It Like a Man VII? Is this yours, young man?”

“It’s one of ours, yes.” Brubaker took the porno from me and stuck it back in the box. He then hid the box, as if that would erase my awareness of its existence. “It’s hard to find a safe place to be yourself in a small town, let alone get time alone with someone special.”

“You ever bring Jessica Stern down here?”

Brubaker shuddered. “Hell no. We bring the people we are actually interested in. Not the ones our parents pick for us. Here’s the thing. We handle marriages here the same way we breed stock. You marry whom you’re told to marry. Your own feelings don’t matter. So we do our duty for our families and town, and get in some fun in private.”

How aristocratic! Flashing immediately to my ex, I remembered that many rural cultures across the world still forced children into unwanted marriages when they thought the Phoenix Society wasn’t watching, and let their families murder girls who rebelled. Utter barbarism, if you ask me. Why is it that it’s always the quiet little towns that are the most profoundly fucked up? “So, you’ll marry Jessica, knock her up a few times, and then have your fun with someone more congenial?”

“You shitting me? I want to get the hell out of Clarion, but I’d never make it in the city with the education I’ve got. Being an Adversary’s my only shot.”

He actually sounded angry. Was it over his lot in life, or directed at me for trying to persuade him to abandon his best hope for a better life? “I’m sorry. I didn’t realize earlier.”

He fumed as he brewed the joe on the stove in the kitchenette. Accepting the proffered mug, I waited for him to say something. “Not your fault. Heck, I’m sure Jessica hinted at her anticipated ownership of my ass.”

We drank in silence for a while as I studied Brubaker. No doubt he used the free weights stacked in the corner. Between his physique and his comfort with the forests around Clarion, he’d at least make a good recruit. He was strong and sharp, but so am I. That alone doesn’t make me a good Adversary, assuming I am a good Adversary.

An Adversary’s post isn’t just a job. Police officers sometimes talk about holding the true blue line between law and disorder. Adversaries hold a line of their own: a red line between liberty and tyranny. It’s a harder line to hold, because it sometimes means being an advocate for chaos. “Michael, why do you want to take the oath? It isn’t just about getting out of Clarion, is it?”

He didn’t immediately answer. “Something needs to be done about Clarion’s arranged marriages. There’s something wrong with them.”

“You can refuse, can’t you?”

“Only if I leave everything behind. Nobody should have to do that. But it’s worse than that.” Brubaker glanced around the cellar, as if he feared being overheard. “It all goes back to Dr. Petersen. He says he’s only testing children and discouraging matches between people whose genomes are too similar.”

That sounded plausible. “But when he only gives you one or two acceptable matches, it looks like eugenics instead of genetic counseling?”

Brubaker nodded. “Yeah. I think he’s using us for some kind of breeding experiment, but he isn’t telling us anything about it or explaining why. I went to the Sheriff and the Mayor, but they’re in on it. They told me the Phoenix Society wouldn’t believe me.”

Now it begins to make sense. He wants the training and authority to crack the case himself. “You want to expose the truth yourself, if nobody else will?”

“Exactly!” He thrust himself to his feet and paced the room as if galvanized by my question. “Maybe the Phoenix Society won’t believe me. But if I’m aware of a problem, isn’t it my responsibility to do something about it?”

No counterargument was possible, not when I came to Clarion for the exact same reason. “But there is an Adversary here. I might not be officially on duty, but I might be able to fix that if I can find probable cause for an investigation.”

That didn’t go over as well as I hoped, judging from the indignation in his voice. “Dammit, what more do you need?”

Ah, the impatience of youth. Never mind I’m only a couple years older than him. “Right. Time for your first lesson. I can’t take what you’ve said to court and prove guilt. It isn’t enough. It might not even be admissible as evidence. I need more.”

Instead of answering, Brubaker cocked an ear at the ceiling. Grabbing a machete from a footlocker by the ladder, he ascended and stuck his head up out of the trap door. “Get the light. The rain’s stopped.”

He scanned the woods as I joined him topside. “Follow me. I want to show you something.”

He did not speak again for the next hour. Instead, he led me deeper into the forest, following a trail marked years ago. The trees along our path bore old scars from hatchet strokes. We stopped several times so that Brubaker could hack through brambles.

We eventually reached a dead end, a wall of brambles that had woven themselves into cables as thick as my arm rose above our heads in a straight line running from west to east. Following the wall east led to a corner, which we turned before reaching what appeared to be a pair of overgrown gates and a dilapidated guard post.

I caught a glimpse of green paint the creepers didn’t wholly obscure, and tore away the vines until I revealed a painted metal sign.

> North American Commonwealth Army  
> **FORT CLARION **  
> Authorized Personnel Only  
> (Secret Clearance or Higher Required)  
> CO: Col. Henrik Petersen

CO most likely meant ‘commanding officer.’ Was the Col. Henrik Petersen named on the sign the same Colonel Petersen who now served Clarion as a doctor and coroner? The same Dr. Petersen who Brubaker insisted was using the people of Clarion as breeding stock in a eugenics experiment?

The vine-matted gates were topped with razor wire, so climbing it was out. However, the guard station suffered no such limitation. Getting atop one of them would let me peek over the wall. Surely Fort Clarion was a ruin, but I wanted to see for myself. “Michael, can you give me a boost?”

He nodded, and got into position. Springing out of his cupped hand, I scrambled atop the guard hut and got my look over the fence. Within Fort Clarion’s perimeter, everything was white-glove perfect. The Prime Minister of the North American Commonwealth could probably bounce quarters off every bed in the barracks. Every vehicle gleamed as if freshly washed and polished, and all of the buildings were newly painted. But there was something missing. “Where are the soldiers?”

“What?”

Of course Brubaker has no idea what I’m on about from below. I laid down on the roof and lowered my hand to help him up. He took a long look, and whistled. “I knew the base was here, but holy shit. It’s like there were soldiers here only a few minutes ago. Where did they go?”

A glint from the watchtower caught my eye. Grabbing Michael, I jumped off. We rolled as we landed, and he looked ready to take a swing at me once he recovered. Not that I blamed him. “Sorry. I thought I saw light glinting off a scope.”

“Sniper?”

I glanced back at Fort Clarion. “Don’t know for sure. Somebody’s there, and I’d rather not meet them yet.”

## Track 16: Rockwell - “Somebody’s Watching Me”

Michael and I got back to Clarion in time for me to meet Dr. Petersen as promised. When I showed up at his office, the waiting room was empty save for a nurse bustling about. She tended the plants and tidied up as if shutting down for the day. “Excuse me. I was supposed to meet Dr. Petersen.”

“You must be Naomi Bradleigh.” The bored, weary tone implied she had better things to do than deal with me. Hard to blame her after a peek at her workstation; the poor woman must have been on her feet all day. “Dr. Petersen told me to expect you. He had to make a house call, but he’ll meet you at the Lonely Mountain when he’s done.”

Curiosity tempted me to ask where Petersen went for his house call, but I knew better. The nurse would cite confidentiality and tell me to bugger off. Besides, I had a better question in mind. “Who should I ask about the area around Clarion?”

A shrug from the nurse. “Try Town Hall. They’re open till six.” She turned her back and lowered her voice, but I still caught the rest. “This freak’s long past due for a husband and some kids to settle her down, but city folk recognize no duty to anybody but themselves.”

Well, excuse the living shit out of me. I hope for the sake of Dr. Petersen’s patients that this nurse was more solicitous on the job. Still, it was up to me to be the better person. “Thanks for your help.”

The receptionist at Clarion Town Hall was better; her face lit up with a megawatt smile as soon as the door closed behind me. She was a middle-aged brunette with shoulder-length curls who wore a little cat pin in her lapel. The nameplate on her desk read, C. Gatt. “Excuse me, Ms. Gatt. I was told the Town Hall would be open until six, and I had some questions about the area and its history.”

“Of course.” She studied me a moment. “Haven’t I seen you before? With Kaylee Chambers?”

Her accent surprised me, despite not being especially pronounced. She sounded like she came from an Australian city. Melbourne, perhaps, or maybe Sydney? I offered my hand before she realized I had failed to introduce myself. “You’re right. I’m Naomi Bradleigh. I’m on vacation, and arrived yesterday.”

“I’m Cat.” She stood and shook my hand before stepping out of her cubicle. She turned her nameplate around so that it now read, ‘AFK. BRB.’ I suspect that meant, “Away from keyboard. Be right back.” She saw my interest and smiled. “Present from hubby.”

A rock riff emanated from her purse, and Cat retrieved a smartphone. I guess she couldn’t afford an implant yet, or didn’t want one. Most people were like that, but I like having the tech in my head, running on blood sugar. No batteries, and it’s a metabolic boost. “Speak of the devil.”

“Go ahead.” I checked out a small rack marked ‘Visitor Resources’ that consisted mainly of pamphlets advertising local businesses while Cat talked with her man. “I’ll be home before your little friend goes back to sleep. Just have to help a visitor. Oh, that sounds tasty. No, you can’t have me for dessert before dinner.”

She hung up and joined me. “Sorry about that. Matthew called to ask what I wanted for dinner.”

“Sounds like he wanted you for dinner.” What can I say? I couldn’t resist.

Cat gave a throaty chuckle. “He’s incorrigible. So, how can I help you?”

I pulled a pamphlet mapping out nearby hiking trails, and pointed at the Old Fort Woods. “I found a pre-Nationfall military installation in these woods. Do you know of anybody who can tell me more about Fort Clarion?”

A frown from Cat as she repeated the name in a mumble wasn’t my idea of a good sign. Was there nobody here who knew about that old base? Cat shook her head. “Unfortunately, Ms. Bradleigh, the original town was almost completely destroyed during Nationfall, and most of the information remaining about the old town was targeted at tourists.”

She turned her screen toward me to show a pre-Nationfall net archive. She was right; it was all sanitized. “So, who got the resettlement effort off the ground?”

“The Phoenix Society did, ten years after Nationfall. Our community is composed of families from across the continent. They either wanted to get out of the big cities, or away from villages too small to have a local arts scene like ours. Regionally popular bands like Charn and Keep Firing, Assholes hail from Clarion.”

“I remember hearing one of Charn’s albums at the Lonely Mountain yesterday. Interesting stuff. So, is there anybody who can tell me more?”

Cat shrugged. “Maybe some of the old-timers? I think Dr. Petersen would be your best bet.”

Yeah, the one guy I probably shouldn’t trust in light of what I heard from Brubaker. I could ask Malkuth, but his answer would probably be that I’m not cleared for that information.

If we ever do go on that date, I’m going to spank him. Three smacks for every time he stonewalls me ought to do, but if he keeps this up I’ll buy a whip. Knowing Mal, however, he’d probably smile and say, “Thank you, ma’am. May I have another?”

My amusement at the notion must have shown, because Cat smiled at me. “Something funny?”

“Just thinking of what I might do to a certain cheeky bastard I know.”

“Ah.” Cat’s grin broadened. “Sounds as incorrigible as my husband.”

Dr. Petersen beat me to The Lonely Mountain, and waved me over to his table while closing his notebook. “Good evening, Ms. Bradleigh. I trust you had a pleasant hike.”

“I did, but now I’m famished.” Which was true; the packet of jerky I bought before leaving hadn’t lasted all day. “Thanks for asking. How was your house call?”

He shrugged. “Strictly routine. I met the Brubaker boy on the way back. Michael’s quite taken with you.”

“Don’t worry. He isn’t my type.” Smiling behind the menu, I tried poking at him. “I wouldn’t want to break up his arrangement with Jessica Stern.”

“That’s not on me, Ms. Bradleigh. I only told his parents when he was a child that if he was to marry a local girl, Ms. Stern would be a good genetic match. His parents fixated on the notion, mainly in hopes of combining their farm with that of the Sterns.”

That didn’t square with what Mike told me. Who’s lying? Who would benefit most from lying, and my believing the lie? Put it this way: Mike has no reason to lie, much less practically foam at the mouth.

What about Petersen? If I believe he’s nothing more menacing than a helpful country doctor providing genetic counseling to help keep the local population from getting inbred, that leaves me with a weaker case for investigating him in detail. No matter. His involvement with the Commonwealth Army during Nationfall is reason enough to dig deeper.

“-have someone?” It was the only part of Dr. Petersen’s remark that registered, but it snagged my attention.

“Excuse me?” A glance at the stage, where a band called Keep Firing, Assholes was starting their sound check, gave me a handy excuse. “I didn’t quite catch that.”

Petersen nodded. “The question was none of my business. Instead, allow me to extend an offer. Clarion has a substantial population of individuals with CPMD. If you like, I can test your genome and suggest individuals with whom you might like to start an acquaintance.”

Ballsy, but subtle. Normally I like that in a man, but I’ll be damned if I’ll consider somebody on the basis of genetic compatibility. I’m not livestock. “Thanks, but I’m not looking for a relationship at the moment. I just ended one.”

“Fair enough.” Petersen rose, dropped some banknotes on the table to cover his tab, and extended a hand. “I must take my leave now, Ms. Bradleigh.”

“Not going to stay for the band?” Petersen pressed something into my hand as he shook it. A note?

“Rock was never my bag, but I would love to hear you sing sometime.” With that, Petersen left. Something about his shoes caught my eye as he threaded his way through the crowd gathering for the band. They weren’t shoes, but boots, and still had forest dirt caked in soles.

Now, what kind of doctor wears combat boots, and goes tramping through the woods? Was he bullshitting me about that house call? And what did he slip me before we left? Unfolding the paper, I smoothed it on the table:

*Fort Clarion is dangerous. Keep your distance.*

Dangerous? Sure. I can believe that, but if Dr. Petersen thinks I’m going to keep said distance after he slipped me a note that might as well be a threat, he should talk to a colleague about getting his head examined.

Still, how did he know I had been to Fort Clarion? Could Mike have said something? Time to check. 「You there, Mike?」

It was about a minute before I got a reply. He must be using a handheld.「What’s up?」

「Did you say anything to Dr. Petersen about our hike? He says he met you on the way back from a house call.」

One drawback to secure talk is the pauses. It wasn’t so bad between implants, but the lag as Mike typed his responses into his handheld made the conversation interminable. 「Sorry, Naomi. I didn’t think we had to keep it on the down low. I told Doc we went out for a hike, and that we found this weird old army base in the Old Fort Woods. That’s all.」

A modicum of foresight on my part might have prevented this. I might have told Mike to meet me in the woods at a predetermined latitude and longitude away from the town. I might have told him ahead of time to keep the details of our hike to himself. Hell, I might have told him to lie and say we had ourselves a shag by the river, or seduced him and made the lie true.

Instead, like a demon-ridden idiot, I acted like a tourist instead of an Adversary and gave no thought to operational security. Well, no more mistakes from here. I can’t afford it. 「Mike, we must be more careful from now on. Petersen is not to be trusted.」

## Track 17: Iron Maiden - “The Edge of Darkness”

Warning Mike about the need for operational security was easy, albeit a day late and a milligram short. Now was time to update Rosenbaum before somebody who doesn’t have my best interests in mind clues him in. Good thing I got the introductions out of the way last night.

「Hello again, Naomi. How’s Clarion?」Saul’s reply was instantaneous, thanks to the implant-to-implant connection.

Implanted computers are standard issue for everybody on the Phoenix Society payroll, not to mention anybody who holds anything resembling a position of authority. They jack into your optic and auditory nerves to gather Witness Protocol data. Because of this, and their ability to run a wide variety of software targeted at POSIX systems, they’re invaluable in the field.

Bloody good thing, too, because nobody will sell you a firearm unless you’ve got a working implant. But back to business before Saul thinks I’ve lost my connection.

「I met a nice boy who wants to become an Adversary. He’s got the drive, and he’s pretty sharp.」 That much was true: Mike Brubaker would probably be an asset to the IRD corps.

「Send him my details and tell him to call me if he’s serious about joining up. Pittsburgh will need its own chapter in a few years. Anything else?」

「I found something of interest while hiking in the woods northeast of Clarion. An old North American Commonwealth Army installation in unusually good condition. I think the commanding officer is still alive, and practicing medicine in town. He slipped me a note warning me away.」

「I see you’re making friends on vacation. I thought you promised to behave yourself.」 He continued before I could make a crack about how I never promised to be well behaved. 「If you were composing an explanation, belay it.」

The naval terminology brought a smile. 「Aye, sir.」

「And belay the aye-ayes, Adversary. This isn’t the Navy.」

Saul got down to business. Good thing he couldn’t see me smiling over secure talk. 「I’ll find a justification for letting you investigate further, but you’re back on the job for the duration. I hope you brought your pins.」

As a matter of fact, I did. It always pays to be prepared. Though I wouldn’t be fully armed. 「I brought my uniform, but no weapons other than my sword.」

「I’ll take that into account. Goodnight, Naomi.」

「Goodnight.」 But it wasn’t. Sleep took its sweet time claiming me, despite my efforts to hasten its coming. Vigorous exercise in the cool night air, warm chamomile tea, and a steamy shower spent imagining a certain soldier’s hands exploring my body left me in a state so common among Adversaries we have an acronym for it. I was TBW: tired but wired.

By four in the morning, I had had enough of lying in bed doing mindfulness meditation and hoping for a sleep that stood me up like a man too spineless to say no when I asked him for a date. It was too early for breakfast, but sitting in the beer garden with a book was a welcome change, and gave me an excuse to put myself together. A bit of fresh air would help me stay awake.

A motorcycle’s rumble died off, and its rider vaulted over the fence instead of using the front gate like a reasonable person. Not that Edmund Cohen struck me as the reasonable sort. He sauntered over to my table, spun a chair around, and straddled it. “Well, Nims, you certainly clean up nicely.”

“I should hope so.” He should have seen me at that hotel bar in Manhattan, though I doubt my gown would prove suitable for kicking ass and taking names. “Saul Rosenbaum told me I could expect orders.”

Edmund chuckled as he withdrew a miniature digital voice recorder from his coat and slid it across the table. “He said you wanted a mission, and for your sins, you’re getting one. Need headphones?”

“Got some.” I pulled a set of earbuds out of my pocket and plugged them into the device’s output jack, carefully tucked the buds into my ears, and pressed play.

A voice I had never heard before greeted me. “Good morning, Adversary Bradleigh. Before we begin, please note the serial number of this communication. You may send it to Malkuth to verify this message’s authenticity.”

He rattled off a string of hexadecimal numbers I recognized as a cryptographic key. Using my implant, I stored the key for authentication, and listened to the rest. “Adversary Bradleigh, your mission is to muster Clarion’s militia and lead it to Fort Clarion. Upon arrival at Fort Clarion, you are to gain entry by any means necessary, and search the premises. Compile a complete inventory of all weapons and equipment, and forward said inventory to Saul Rosenbaum at the New York chapter. Await a Phoenix Society ordnance disposal team, and ensure that no equipment leaves the base.”

That seemed simple enough, like a textbook arms control job. Never had to do one myself, but I remembered the training drills from ACS. I let the tape continue playing, but heard nothing but the hiss of blank tape. Before I could press stop, the voice returned. “Naomi, the following is unofficial. Malkuth will not authenticate it, because he is unaware of this addendum. Fort Clarion isn’t just an old Commonwealth Army installation. It was also the site of a series of human enhancement experiments codenamed ‘Project Harker.’ I cannot tell you more without tipping off my colleagues on the executive council. You will have to find the rest on your own, using evidence procured on site. Keep this to yourself, and wipe the device after listening.”

Obeying the recording’s final instruction, I returned the blank device to Edmund. “Who gave you these orders?”

He shook his head. “I can’t tell you. Got orders of my own. I’ll say this much: he won’t tell you everything, but what he does tell you is true.”

Could it be Cohen himself? The voice sounded a bit like him, but it’s possible to fake such recordings. And what exactly is Project Harker?

Was the Commonwealth Army trying to make vampire soldiers? Is that yummy Sergeant Renfield somehow involved? After all, Harker and Renfield are both names from the Stoker novel. No sense dwelling on it, or asking Edmund. He wouldn’t tell me anything. “All right. That should do. It’s only an arms control job, after all. Should be so uneventful I’ll regret finding the bloody place.”

“That’s the spirit.” Edmund chuckled. “Now, do you need any equipment? I brought an AK and a M1911, but you’ll have to assemble the Kalashnikov.”

As tempting as the prospect of having some firepower sounded, I decided against it. “Thanks, but it would look better if I requisitioned gear from the militia armory. Some people in town know I’m an Adversary on vacation. If I suddenly show up at Town Hall to muster the militia with an AK, people might wonder if the vacation was an insertion cover.”

“That’s smart. I like it.” Edmund rose, indicating that the conversation was over. “Be careful, Naomi. You’ll have Witness Protocol running in ten minutes, but don’t count on backup even if we do see you’re in trouble.”

No backup? No problem. I’ve got this. “Thanks for the warning. Sure you don’t want to stay for breakfast? The bacon here is to die for.”

“Better not.” Edmund glanced at the street. “If I stick around any longer, I might find myself involved. That would be bad for both of us. I know too much, and can’t safely reveal any of it.”

If he knows so much, then was the masked voice on the recorder his? Or is he working for whoever made the recording? “I understand. Thanks, Eddie. Have a safe ride.”

“You too, Nims.” Nims, eh? So, him calling me that earlier wasn’t a one-off. Seems he’s been chatting with Malkuth.

Once Edmund was safely away, I checked the time: 6:31. Dammit. Town Hall won’t be open until nine. Time enough for breakfast, but why eat alone? 「Mike, you awake? Come to the Lonely Mountain. Breakfast’s on me.」

Ten minutes later, he arrived. “You look different with your hair up. Is this about the fort?”

“Yeah.” If I had been thinking, I could have gotten the latitude and longitude at Fort Clarion, and used GPS to pinpoint it on a map. But a direct road to the installation in usable condition is too much to hope for, so precise coordinates and satellite navigation might not be useful. “You can lead us there again if necessary, right?”

“Sure, but who’s us?” He looked around to see if I had anybody with me. I didn’t yet, but that would soon change.

“You, me, and the Clarion Volunteers.” The surprised expression on Brubaker’s face was good for a smile. “I’m back on the job, Mike, and you’re my star witness. Let’s get some chow, and then we’ll go see Mayor Collins about calling up the militia.”

Cat must have seen my pins before she recognized my face, because she scrambled to her feet and stood at attention as if I were some potentate, and not somebody who had listened to her flirt with her husband over the phone the day before. “Good morning, Adversary. How may I assist you?”

“Relax, Cat. It’s just me, Naomi. I was here yesterday asking about the town’s history.”

“I know, but you weren’t on the job then. You obviously are, now. Do you need the Sheriff? What did Michael do?”

“Mike’s fine. He’s working with me. I need to see Mayor Collins about raising the militia. It’s official business. Here’s the authentication key for my orders.” I rattled off the hex string four digits at a time, so Cat could key them into her terminal for confirmation.

Cat nodded. “Just a moment, please.” Picking up an old-fashioned black telephone that looked solid enough to make a decent weapon, she dialed an extension. “Mayor Collins, Adversary Naomi Bradleigh needs to see you on official business. Yes, Your Honor. I’ll send her and Mr. Brubaker right up.”

## Track 18: Jackyl - “Locked and Loaded”

I thought at first Cat misdirected me to an attorney’s office instead of the Mayor’s. Either that, or an accountant’s. Law books and budget ledgers filled the polished cherry bookshelves lining the walls. The desk and the three chairs set before it were plain, but the gleam of leather and well-oiled cherry hinted at quality.

The nameplate on the desk read “Mayor B. Collins,” and the memory of two miserable winter weeks watching old television serials while fighting the flu led me to wonder if the ‘B’ stood for Barnabas. Given that I had already run into a Renfield, and that Fort Clarion was the site of Project Harker, having a mayor named after a vampire seemed fitting.

Not that I’d ever ask. He would probably think I was taking the piss, even if he got the allusion. Besides, a quick network search showed his name was Brian. Dammit.

“Morning, Adversary Bradleigh. What can I do for you?” Mayor Collins was a bit shorter than me, and stout, but his grip as he shook my hand suggested he took admirable care of himself. This was a good sign; a man who doesn’t care for himself can’t be expected to care for others. “Please take a seat. Would you like some coffee? How about you, young man?”

Coffee? Hell yeah, and yes please. “Some coffee would be wonderful. Thanks.” Mike concurred. “Thanks, Your Honor. I could use a cup.”

Once we had our mugs, Mayor Collins settled into his chair and tapped at his keyboard. “Adversary Bradleigh, it seems you’ve been tasked with investigating an old military installation nearby and compiling a complete inventory preparatory to cleanup by an ordnance disposal team. Is this the case?”

“Yes, Sir. It should also mention that I will require the aid of the local militia.” Collins nodded, sipping his coffee. “Are you aware that we’re in the middle of the harvest season, Adversary? You’ll be asking men and women to put aside pressing work.”

No shit, Sherlock. Not that I’d say anything of the sort. It would be unprofessional. But I can bloody well think it. “I understand your concern. Instead of calling up the entire militia, can you put out a call for volunteers? Naturally, the Phoenix Society will compensate people for their time and effort.”

Collins relaxed, leaning back in his seat as he steepled his fingers. “That’s fair. To be honest, Sheriff Robinson led me to believe you weren’t the sort who was capable of being reasonable.”

The Sheriff’s been talking about me, has he? Maybe I should have been gentler with him. I might have said I don’t invite strange men to my room until I’ve had a couple of drinks with them first. Not that he’d have bothered with an invitation, but my phrasing would have conveyed sufficient reproof. “He wanted to search my room and belongings without a warrant, and I refused him.”

“I don’t blame you. He used to be a cop before Nationfall. I don’t think he ever got used to people asserting their rights. And he still gripes about having to carry a sword instead of a gun.”

So, Robinson’s another old-timer. How much does he know about Fort Clarion and Project Harker? “Am I correct in assuming that Sheriff Robinson normally leads the Clarion Volunteers?”

“Hole in one, Adversary. Gotta tell you, he’s not going to enjoy being sidelined.”

Now, why would I complicate my life by monkeying with the existing command structure? It would be more sensible to have Sheriff Robinson work under me. Any annoyance he suffers from answering to me would be bacon on my pizza. Mmm, bacon. “I’ve no intention of sidelining him. Would you kindly invite him up?”

Collins nodded and picked up a phone. “Robinson, it’s Collins. Come up to my office. I met that young woman you told me about.”

We drank our coffee in silence until Sheriff Robinson arrived. He studied me as Collins explained my mission and my request for militia assistance. After Collins finished, Robinson studied me a bit longer. “So, you want to take over my militia and go tramping through that old army base?”

Did Sheriff Robinson just imply that he knew about Fort Clarion? I’ll have to feel him out later. First, diplomacy. Let’s try an open hand instead of a closed fist. “First, I’d like to apologize for my brusqueness at our first meeting. I had not realized you had served as long as you have.”

He glanced at Collins. “So, he told you? Did he mention I used to work narcotics?”

Well, that explains a lot. History shows that the institution of prohibition always leads to police trampling individual rights in their search for contraband. “No, he didn’t.”

Robinson nodded, and poured himself a cup of coffee. “Tell me something, Adversary. Have you ever led men before? Got any command experience?”

Oh, I’m used to having men under me, but that’s not what he had in mind. “No, Sheriff. For that reason, I’ve no intention of supplanting you as captain of the Clarion Volunteers. I will tell you what I need the militia to do, and you may issue the appropriate orders. Is that suitable?”

“That suits me fine, ma’am.” Robinson’s attitude shifted, and became more respectful. “How many people do you need?”

Good question. If I bring too many, they’ll get in each other’s way and make the job harder. If I bring too few, the job won’t get done before Ragnarok. “Let’s start with a hundred. Try to preserve the existing chain of command.”

“Yes, ma’am. Take my IP address so we can use secure talk.”

So, the old dog learned some modern tricks. That’ll simplify matters. “Thanks, Sheriff. Can you have the volunteers ready by thirteen hundred hours?”

“No problem. I’ll have ‘em mustered. Anything else?”

I patted the sword on my hip. “I’ll need to borrow a rifle from the town arsenal. A pistol as well, if you can spare one.”

Robinson nodded, and finished his coffee. “You think we’re likely to run into trouble out there?”

Recalling the glint off what might have been a scope high up in one of Fort Clarion’s watchtowers, I shrugged. “I’d rather have a rifle I don’t need than need a rifle I don’t have.”

The Mayor spoke up. “Adversary Bradleigh, you’ve been to Fort Clarion. Did you see anything the Sheriff should know about? What about you, Mike?”

“We haven’t actually been inside, sir.” Mike glanced at me, and I nodded. “The fence is completely overgrown. We climbed up to the guardhouse’s roof and looked over the top. It doesn’t look abandoned on the inside.”

“Meaning?”

I took over. “I think the installation may still be garrisoned, Sheriff. I don’t know who’s manning the base, but they appear sufficiently disciplined to keep the installation in perfect order.”

“But the North American Commonwealth fell apart decades ago. If Fort Clarion has any soldiers left, they’re probably old men. How did they even survive off-grid this long? With us remaining ignorant of their existence until now?” All excellent questions, for which I lacked answers. I’ll have to remedy that, and soon, not to mention for a few of my own. What if Christopher Renfield is involved? His uniform was period-accurate, and our whole conversation was weird until he kissed me.

Sheriff shook his head at the Mayor’s remark, and gave a disgusted snort. “Oh, come on, Brian. You know damn well this town is so infested with geeks and nerds, we ought to be looking to attract tech startups instead of farmers. I wouldn’t be surprised if a bunch of basement-dwellers found the base and fixed it up so they could have a realistic setting for when they play soldier in the woods.”

This guy was starting to annoy me. Will he give me cause to arrest him before I’m done? I rather hope he does. “Do you really believe that to be the case, Sheriff, or are you just looking for an excuse to take my mission less seriously than you might otherwise?”

“Adversary, I’ll get you your weapons and instruct the men not to fire unless fired upon. Is that satisfactory?”

“Perfectly so, Sheriff.” I returned his salute, and turned to Mayor Collins once the Sheriff left. He still looked concerned, no doubt for any townsfolk who might volunteer. “As you said, Your Honor, if Fort Clarion is still occupied, it may be by men too old to fight―or wargamers. It shouldn’t come to violence.”

“I hope you’re right, Adversary.” He checked his watch. “I have a meeting in five minutes. Would you like to use one of the conference rooms? No doubt you’ll want to set up a proper headquarters instead of working out of your room at the Lonely Mountain.”

Does Mayor Collins think I’m going to stay in town, drinking coffee and buffing my nails while the men tramp through the woods? That’s so not my style. Adversaries lead from the front. Still, a war room might be handy if I have the only key. “Thank you, Your Honor.”

“Good luck. Cat will take care of anything you need.” He led us outside, where Cat was waiting. We followed her down to a conference room on the first floor, and there on the table was a gun case. A note rested atop it:

*Let me know if this isn’t enough gun. -R*

Unlatching the case, I lifted out a handsome Westchester lever-action rifle with a scope. The walnut stock was engraved with the image of a river and the name of the local militia: the Clarion Volunteers. The steel gleamed as I opened the weapon to determine if it was loaded. It wasn’t, so I did the honors from a thoughtfully included box of 7.62x51mmR ammo and chambered a round. The action was silky-smooth, indicating that whoever last carried this rifle took proper care of it.

Returning it to the case, I checked out the revolver and its ammunition. Granted, it wouldn’t have the same range or accuracy, but we’re trained to fight with a sword in one hand and a pistol in the other.

The revolver was a double-action model, and its empty cylinder held six 11.43mm rounds. I wouldn’t be able to fire as quickly as I might with a semiautomatic, and I’d need to take cover to reload, but that’s what I get for only bringing my sword. I felt like a nineteenth century cavalry officer as I strapped on my gun belt.

“Michael, you aren’t old enough to serve in the militia, are you?” He shook his head. “Nah. You gotta be twenty-one, but I’ve got a shotgun.”

“Go get it, just in case. You might want to bring a mixed load of slugs and buckshot.”

“Right.” Michael left, and I considered calling Cat. While I could use my implant to call up maps and compile the inventory instead of cluttering the room, the powerful little computer in my head was a strictly private resource. Moreover, if I rigged up a computer properly, I’d be able to check for tampering or attempts to falsify data.

Unlike her namesake, Cat turned up promptly when called. No need to rattle a bag of kitty treats. Or was she the curious sort, and had been listening nearby? “What’s up, Naomi?”

“Can you please supply me with maps of the area? Also, I’d like a laptop if Town Hall has a spare.”

Cat nodded. “Maps are easy, and my husband will bring a loaner from his shop.”

A long-haired, bespectacled man in jeans and a ‘Keep Firing, Assholes’ t-shirt arrived five minutes later. He was kinda cute, if you like your men cuddly. “Are you Adversary Bradleigh?”

“Yes. Are you Cat’s husband?”

“Yeah.” He plugged in the laptop and opened it, but didn’t power it up. “This laptop’s diskless. I understand Adversaries carry devices they can plug into any machine to boot up a secure workstation.”

That was certainly the case. I removed one of my pins and ran a fingertip over the back in a predefined pattern, as if I were trying to solve a demonic puzzle box. The pin’s back opened, allowing me to remove a tiny memory card. The memory card held a bootable secured Unix variant called HermitCrab that interfaced with my implant for storage. No installation necessary, and far more convenient than doing everything in my head. Naturally, Adversaries got training in this environment for use in computer forensics. “Good thinking. How did you know?”

Cat’s husband shrugged. “I helped out with the hardware detection modules.”

“Well, thank you. This will be a huge help.” The ability to remove my card whenever I wasn’t using this laptop would frustrate snoops. Furthermore, I might find computers at Fort Clarion. If I can power them up, I might be able to salvage data from their storage drives.

Michael returned with an antique single-barrel breech-loading shotgun, as I finished confirming the laptop worked. And I thought I was packing old-school heat. He snapped the gun shut after loading a buckshot round about two seconds before Sheriff Robinson opened the door.

“I’ve got the men assembled outside, Adversary.” He held the door for me and gestured toward a crowd assembled outside. “We’re ready.”

Time to inspect the troops. “Thank you, Sheriff.”

## Track 19: The Doors - “Break On Through”

Delightful. Absolutely brilliant. I instructed Sheriff Robinson to get me a hundred militia volunteers, and what does he do? He turns my mission into the bloody Children’s Crusade. Perhaps one in five was armed and in uniform. The rest had machetes or hatchets on their hips. One beefy youth with spectacles shouldered a sledgehammer.

None of them looked a day over eighteen, which would still be three years too young to serve militia duty. The minimum age is twenty-one to prevent younger people from being brainwashed into blindly obeying orders. Not that Sheriff Robinson seemed to give a damn. If anything, he had puffed himself up like some loathsome, vaguely humanoid toad.

“Sheriff, I need a word with you inside.” Time to deflate his ego. “What were my instructions?”

His eyes got shifty, as if we were playing poker and I caught him with an ace up his sleeve. “You wanted a hundred volunteers. Here are a hundred volunteers.”

“How many of them are actually old enough to serve militia duty?”

Now he looked away, and backed up a step. “Twenty of them. The rest are here with their parents’ permission.”

That explains why the aforementioned rest was unarmed and out of uniform. “Then what the hell are those kids doing here? My orders specified the use of local militia because we’re dealing with military ordnance. I need people who can be trusted to follow orders and safely handle weapons.”

Robinson indicated the people outside with a sweep of his arm. “Adversary, I understand you’re from the city where people only handle swords unless they’re training for militia duty. Christ, the goddamn Phoenix Society even makes the police carry swords.”

Does the good Sheriff resent being forced to trade in his pistol for a gladius? Rather than hunt down a suitably tiny violin, I let him have his say. “But out here we grow up with guns. Most of those kids first learned to handle BB guns when they were six. They’ll be fine, and we shouldn’t need more than twenty militiamen against whatever old men still lurk at the fort.”

“Hold on a moment.” Tempting as it is to arrest Robinson on the spot, do I even have just cause for doing so? Holding my fingertips to my ear so Robinson would understand, I fired up secure talk. 「Malkuth, it’s Naomi. You watching my feed?」

「I’m monitoring Robinson’s as well. He isn’t quite in violation of either the letter or the law or its spirit, but he’s dancing on thin ice. Don’t trust him.」

「Oh, I won’t. Instead, I’ll give him all the rope he wants. Let’s see if he hangs himself.」 Dropping out of secure talk, I cleared my throat to get Robinson’s attention. My conversation with Malkuth probably took all of two seconds, but the Sheriff was already bored. “Sheriff, I will hold you personally responsible for the safety of the kids you insist on bringing along with us. If one of them so much as stubs their toe on a tree root, I will place you under arrest on an abuse-of-power charge.”

Robinson stared at me a moment. “Don’t you think that’s a bit excessive, Adversary?”

“Compared to summary execution? Not really.” Arresting Robinson would be a dicey situation. Would it turn the militia against me? Twenty against one isn’t a fight I’m likely to win. A hundred to one if the younger kids get involved is even nastier. “I’d rather we just got this done so I can leave town and be nothing to you but another bad memory.”

“You’d have plenty of company.” Robinson opened the door for me, allowing me to rejoin the crew outside as he commanded their attention. “Sorry to keep you guys waiting. Here’s the deal. Adversary Naomi Bradleigh needs our help tearing apart an old military installation in the woods. She’s in command, but will relay instructions through me.”

Stepping forward, I let the men get a good look at me. “Thank you, Sheriff Robinson. Gentlemen, I’m Adversary Bradleigh. Our mission is an arms control operation at Fort Clarion, in the Old Fort Woods northeast of here.”

They started looking at each other and muttering. Guess they had no clue. Drawing my sword got their attention. “Michael Brubaker will guide us there. Once we arrive, I will provide further instructions. Now, I want the adult volunteers from the Clarion Volunteers to step forward.”

They complied, and saluted in so smart a fashion, I was honor-bound to return the gesture. “You will each be responsible for four of the younger volunteers. How many of you have implants?”

All twenty hands went up. That certainly simplifies matters. After I obtained their IP addresses and Robinson’s, I connected them all to SRC. “Sheriff Robinson will relay my orders over secure relay chat. Ignore any order that did not first come from me.” I glanced at a militiaman in the middle, whose nametag read ‘Yoder.’. “Do you have a question, Mr. Yoder?”

“Ma’am, how should we relay your instructions to the volunteers you’ve tasked us with supervising? Should we also run our own secure relay chats while monitoring yours?”

“That’s an excellent idea, but first ― is there anybody here who doesn’t have an implant? Raise your hands if that’s the case.” Nobody fessed up to not being properly equipped. “Perfect. I will expect the adult volunteers to do as Mr. Yoder suggested. Any other questions?”

A smirking kid raised his hand. “Are you a vampire?”

Seriously? Not that a show of anger would help; this schmuck’s only trying to look cool. Do guys ever get tired of trying to prove their masculinity? “Were you hoping I’d sneak into your bedroom at night and enslave you with my kiss?” I delivered the question in the most stereotypically seductive tone I could manage while keeping a straight face, before going full ball-breaking drill instructor on him. “In case you’ve forgotten, I already own your ass for the duration of this job―assuming you’ve got the nerve to stick around.”

The kid flushed, but stood his ground. Good. “Sorry, ma’am.”

“Accepted. Now, does anybody have any questions germane to the mission?” Nobody did, which meant we could finally get down to business. We’d need two hours to get to Fort Clarion, which didn’t leave much daylight for actual work in the middle of October. It would be dark by six, and it was already two. We’d be marching back in the dark. Dammit.

Before I could issue marching orders, Dr. Petersen ran up to us clutching a black bag. Pretty spry for an old guy. “Excellent, Adversary. I had hoped I wouldn’t be too late. No doubt you’ll want a physician around in case anybody gets hurt.”

Actually, a physician would be handy. I can provide first aid, but a doctor capable of working in battlefield conditions if everything goes pear-shaped could save more lives than I might alone. Too bad it had to be Dr. Petersen. Instead of telling him to go make some house calls, I decided to keep him in sight and added his IP address to my secure relay chat. “Thank you, doctor. Please stay with me and Mr. Brubaker.”

“Of course, Adversary.” Not that I liked having him close to me, either, but if I ordered him to march in front of me, I’d tell everybody I don’t trust their family doctor. Just gotta keep giving him rope and hope he doesn’t take the lot of us with him when he hangs himself.

「Brubaker, Petersen, and I will take point. Sheriff Robinson, please take the rear and give a shout if anybody falls behind.」 After issuing the general order, I texted Mike as we began marching. He kept up easily with my stride. 「Can you lead us directly to Fort Clarion, and quickly?」

Mike nodded. 「No problem. Follow me.」

We did. The militia and youth volunteers kept up an excellent pace, and we reached the Fort Clarion perimeter in a little over an hour. While we marched, I got everybody’s names and assured them they’d be in good hands under me.

「So, how do we get in, Adversary?」 One of the militia volunteers, Schmidt, stared at the tangle of vines and foliage that had so choked the gates, that entry seemed all but impossible.

I yanked the machete from his belt and started slashing at the creepers, ripping away what I had cut loose and throwing it over my shoulder. After several minutes’ work, I revealed a few links of rusted chain. 「Sheriff, have the men take turns at this, two at a time. Give each other plenty of room so nobody gets hurt.」

We had the gates cleared within minutes. All that kept us out now was a rusted chain bound by a corroded padlock. I turned to the beefy kid with the sledgehammer. 「Zimmer, you’re up. Think you can break the lock?」

「I might be better off attacking the chain, ma’am. Can you pull it tight for me?」 Zimmer rolled his shoulders, hefted his sledge, and brought it down with a grunt. Ten kilos of blunt steel whistled past me and tore through the chain as if it were taffeta. I pulled it free, and managed to push the gate half a meter inward.

「Hinges are probably rusted to hell and back, ma’am. Gimme a minute.」 Zimmer shattered the lower hinge first, then jumped skyward to reach the other as if slam-dunking a basketball. Showoff.

Volunteers rushed forward to lift the gate out of the way, leaning it against the fence. Fort Clarion was now open. Who would greet us inside?

## Track 20: The Police - “Every Breath You Take”

Peering into Fort Clarion over the fence wasn’t the same as stepping inside. My ears strained for the non-existent sounds of the garrisoned military base. But no sergeants barked orders at enlisted men. Instead of the synchronized beat of well-trained soldiers drilling, only the soft padding of the older irregulars behind me broke the afternoon quiet.

I had to strain my ears to hear most of them. They followed with such quiet efficiency, it was hard not to mistake them for professional soldiers.

Those too young for militia service waited outside. Without firearms and proper training, they’d be a liability if we encountered resistance. Probably should have refused to let them come along, but hurting their pride with a rejection would do little for community relations.

Michael Brubaker kept pace to my left, and I had Robinson at the rear, with Petersen between us. No way was I leaving them behind. Passing the gate placed us on Gen. George Prevost Street, near the post exchange and rows of mass-produced single-family houses reserved for civilian contractors. We kept our rifles at the ready, covering every angle as we advanced, but encountered nothing as we reached the PX.

Armed with a satellite map, I had worked out a rough plan on the way here. Recalling that glint from the western watchtower, I slipped behind cover and scanned both towers as the others followed suit. Nothing untoward this time, but a bit of insurance wouldn’t hurt. 「Sheriff Robinson, I need a fireteam with good marksmen in each of those watchtowers. Everybody else should find cover.」

「Yes, ma’am. Rodriguez and Martin, assemble fireteams and take those watchtowers. Report any contacts, but do not engage until fired upon. The rest of you find cover like Adversary Bradleigh suggested.」

Superimposing the fireteam leads’ IPs on my map allowed me to track their progress. They advanced steadily from cover to cover until they had reached the towers.

Rodriguez was first to get to his post. 「Alpha Lead reporting. No contacts. The tower is ours.」

「Bravo Lead reporting. No contacts. The tower is ours.」 Before I could congratulate them on a job well done, Martin continued. 「We found something of interest. Sending photos.」

Seconds later, my implant displayed a translucent image of a rifle case leaning against the wall. Zooming in, I read the label: 3rd Infantry Division—Squad Designated Marksman Rifle. It was a Western counterpart to Eddie Cohen’s Dragunov. 「Bravo Lead, is there anything in that case?」

「Can’t say for sure without touching it, Adversary.」

「Open it up.」

「Roger. The weapon is present. Its condition indicates recent handling.」

Son of a bitch! How long did someone watch me before I spotted the glint from that scope. Why didn’t they shoot? The knowledge that somebody had me in their sights, but chose to refrain from blowing my head off, left me shuddering despite the sun’s warmth. I dared not count on being so lucky again.

「You okay, Naomi?」 So, Michael noticed that Martin’s report had unnerved me. Better get it together before Robinson or Petersen notice as well.

「We’ll talk about it later.」 I shifted back to the main channel as I decided what to do about that weapon. I didn’t have a safe place to put it at the moment, so there was no point in sending a youth volunteer up to the tower to retrieve it. 「Bravo Lead, leave that rifle in place for now.」

「Roger.」

「Sheriff Robinson, issue a general order. Nobody is to touch anything without my order. I want to leave no sign of our presence. Any weapons found should be photographed and left in place. Photos and serial numbers of all weapons should be sent directly to me.」

「Directly to you, Adversary? Are you sure?」

Why was Robinson questioning my orders? Did he hope for a chance to steal ordnance to sell on the black market before I could catalog it for the disposal crew? 「Quite sure, Sheriff. I want all photos and counts sent directly to me. This is a Phoenix Society operation, and thus all data is my responsibility.」

「Yes, ma’am.」

Robinson issued the orders without further delay, but Dr. Petersen shot me a look. What exactly does that man know? 「Dr. Petersen, we need to talk about your tenure as CO here at Fort Clarion.」

He nodded with a small, tight smile. 「Feel free to schedule a time in advance, so I can have my attorney present.」

So, the good doctor wants to lawyer up before we have our little chat? That’s his right, but now he’s really got me curious. 「Afraid of self-incrimination?」

「No, but the Phoenix Society uses a broad definition for war crimes, and recognizes no statute of limitations. A lawyer’s presence would be prudent.」 He paused a moment while searching his pockets. He withdrew a ring of keys and tossed them to me. 「For what it’s worth, Adversary, I agree that the weapons stored here should not end up in civilian hands.」

I tried the key labeled ‘PX.’ It worked perfectly, but unlocking the door also turned on the power inside. Lights flared to life, and the automatic doors slid open with a soft whir.

A bubbly young woman’s voice chirped from speakers mounted in the ceiling as I stepped inside with my rifle pressed tight against my shoulder. “Welcome to the Fort Clarion Post Exchange! If you’re a member of the Commonwealth’s armed services, thank you for your courage and dedication. If you’re a civilian, please support the troops by purchasing souvenirs of your visit.”

No way that can be an AI. The Sephiroth were the first, and they were activated after Nationfall. The greeting must have been a recording controlled by a motion detector. 「Everybody wait outside for my mark. Tower teams, I want eyes on the PX. Give a shout if you see hostiles.」

Rodriguez and Martin acknowledged, but Robinson had questions. 「Sure you don’t want backup, Adversary?」

「I’ve got this, Sheriff.」 The PX was mostly open space, divided by long rows of empty shelves. Securing the building only took a couple of minutes. 「Clear!」

Brubaker, Robinson, Petersen, and five of the ten adult irregulars still with me trooped in, setting off back to back greetings until some idiot named Hubertson unplugged the speakers. Rather than let Robinson deal with it, I descended upon him. 「Plug those speakers back in. I know the recording’s obnoxious, but if somebody walks in that door, I want to hear about it.」

Hubertson protested. 「But Adversary, it’ll play for friendlies, too.」

「Do I look like I give a flying fuck? Plug those demon-ridden speakers back in, and get out. You’re on sentry duty. Pick a buddy on your way.」

「Yes, Adversary.」 The irregular quickly plugged the speakers in, and left the PX. He didn’t look at me while doing so, leaving me wondering if I had been too harsh.

The others spread out, poking around the PX as I inspected the back office. The most interesting things there were an old Underwood PC I was able to boot using HermitCrab, and some old magazines. Most were military-themed and bore titles like *Modern Soldier*, *Mercenary*, and *Tactics Quarterly*.

These hid an issue of a men’s magazine called *Tomcat*. The cover model was a pale, blue-eyed, snow-blonde whose face resembled my own. I slipped it into the case containing my loaner laptop. Leading this crew would be hard enough without questions about my ancestry making the rounds along with whatever salacious photos the magazine might contain.

「Sheriff, it’s time to secure the rest of the base. I want two irregulars guarding the PX at all times. Check with Hubertson and confirm he found a partner. Have the rest gather teams of ten from the youth volunteers.」

「Yes, ma’am.」 Why can’t I shake the suspicion that Robinson is just waiting for an opportunity to stick a knife in my back? Was it that our first meeting rubbed me the wrong way? Or am I still miffed about only having twenty trained militia members at my disposal?

「Wait. Have the adult volunteers come to me for keys, so we don’t have to kick down doors.」

Once I handed out keys, pointedly ignoring the pained look on Dr. Petersen’s face, I set my sights on the barracks and shot a quick text to Mike, the Sheriff, and Dr. Petersen. 「Follow me.」

The barracks interior was no less pristine than the rest of the base. Surfaces that should have gathered decades’ worth of dust were clean enough to withstand an officer’s white glove. Every footlocker was secured, with two under each bunk. Even the heads sparkled as if scrubbed fresh this morning, and the tang of cleaning chemicals stung my nose.

I found Mike sniffing the air near the door to the mess hall. 「What’s wrong?」

「If the base had been abandoned for decades, would we smell food?」

Stepping into the mess, I tasted the air. Brubaker was right; the scent of recent cooking lingered, mainly roasted meat. Inside the walk-in refrigerator, fresh deer and wild pigs hung by their feet, skinned and ready for the butcher’s blade, the last of their blood dripping from their carcasess and seeping into the floor drain. The larder contained fresh vegetables and unopened canned goods with recent packing dates.

Drawing my revolver, I backed out of the kitchen. 「You’re right, Mike. It smells fishy.」

He didn’t answer. Instead, he stared, aghast, at a small rectangular card I didn’t recognize until he handed it to me. It was a photograph. A photograph of me undressing in my room at the Lonely Mountain.

Was he embarrassed for my sake, or because he liked the photograph? Somebody had shot the photo from a distance, using a telephoto lens. They caught me while lifting the hem of my camisole, which exposed a pair of my extra nipples, but little else.

It’s quite tame compared to the selfies I’ve sent some of my lovers, but the scene makes the picture. This was the sort of photograph a private investigator might take for a client. Worse, somebody—the photographer, perhaps?—wrote ‘primary target’ on the back. Somebody wanted me dead. Somebody had a golden opportunity yesterday, and didn’t take the shot. Why? 「Mike, where did you find this?」

「Martin from Bravo Tower found it in the rifle case, but didn’t want to mention it on the air. She brought it to me a couple of minutes ago.」 A pause. 「I shouldn’t have looked. I’m sorry.」

I hope nobody on Bravo Team used their implant to copy the image. To think I was worried about an old girlie magazine! Still, was Brubaker upset for my sake, or embarrassed because the photo aroused him despite knowing better? I patted his shoulder. 「It’s fine if you liked it. Just keep it between us, all right?」

「Yes, ma’am.」

He still hadn’t eased up. Maybe I should give him something to get that image out of his head. I checked the image archive on my implant, and found a shot a classmate took of me in my dress uniform. The navy blue jacket and trousers clung to me, my hair streamed behind me in the breeze, and my sword blazed with the setting sun’s light. This ought to get him thinking about the future. 「You can have this instead. It’s from my induction as a sworn Adversary.」

「I don’t think you’ve aged a day since. How do you expect me to get you out of my head?」

Flattery will get you nowhere, kid. 「I don’t. I’d rather you remembered me while thinking about your own future. This was the moment when the world opened before me. I was finally an Adversary, ready to uphold liberty and equal justice under law for all by diplomacy or force of arms.」

He didn’t need to hear that the work demanded more of me than youthful idealism, fast talk, and a deft hand with a blade. He needed me to reinforce his belief that he could make a difference if he got through the training and took the oath. He needed the fantasy, but I couldn’t guarantee this mission wouldn’t shatter it. Not when my own idealism was worn and cracked by the Society’s secrecy.

Further study of the photo revealed no other pertinent details. The handwriting on the back was difficult to read, but that didn’t tell me much. Not when penmanship and calligraphy are practically lost arts.

All it told me was that somebody wanted me out of the picture, which was reason enough to do a proper job of taking it into evidence. Slipping the tagged and bagged photo into my jacket wasn’t exactly standard procedure, but at least it wouldn’t get lost.

Sheriff Robinson showed up a second after I finished pulling the zipper back up. He must have seen something in my expression, because he stopped short. 「Did you find anything, Adversary?」

Luckily, I had something other than that photo to discuss. 「Somebody lives here, Sheriff. We don’t know who, or how many, but somebody still makes Fort Clarion their home. Alert the irregulars.」

## Track 21: The Animals - "We Gotta Get Out of This Place"

If I harbored any lingering doubts concerning Sheriff Robinson as a side effect of either our first meeting or the usability of his volunteers, he dispelled them when told the reports of Fort Clarion's abandonment were greatly exaggerated (to paraphrase a classic). He paled, his jaw clenching as he immediately issued a general order. 「The base is inhabited. Prepare to evacuate, and await instructions.」

He didn't wait for my approval before issuing a second order. 「Tower teams, cover everybody on the ground. Once they're out, I want Tower Alpha to cover Tower Bravo. Adversary Bradleigh and I will cover Tower Alpha.」

Mike's shotgun snicked shut. 「Count me in, Adversary.」

「Thanks, Mike.」 It was a sensible plan, despite a fearful chill running down my spine at the thought of being one of the last to get out of Fort Clarion. Robinson was right; a leader should lead the charge from the front, and guard the rear in retreat.

A chorus of rogers pinged our implants, along with a question. 「Sheriff, this is Bravo Lead. What should we do with the rifle we found?」

Robinson glanced at me instead of replying. 「Your orders, Adversary?」

The smart thing to do would be to take the rifle, rather than leave it for the enemy to use. However, that wasn't SOP for an arms control job. All arms and ordnance were to remain in situ until the disposal team confirmed the inventory and signed off. However, the protocol assumed that installations like Fort Clarion are uninhabited.

Hopefully, I won't regret issuing this order. 「Bravo Lead, bring the weapon directly to me.」

「Roger, ma'am.」

Mike, the Sheriff, and I formed a triangle around Dr. Petersen to protect the unarmed doctor. I counted every volunteer passing us as they retreated through the ruined gate.

「This is Alpha Lead. I count ten irregulars out and eighty youth volunteers out. Please confirm, Bravo Lead.」

Martin replied. 「Confirmed, Alpha Lead. Beginning our retreat.」

Tightening my grip on the revolver, I held fast, resisting the urge to hasten the remaining volunteers as they descended the western tower. Ms. Martin stopped as her team passed me, and pressed an unexpectedly heavy rifle case into my hands. 「Did Brubaker bring you the photograph?」

「Yes. Thanks for being discreet.」

A cloud darkened her expression, suggesting the reality underlying country life in Clarion was anything but idyllic. 「I hope you fucking crucify the creep. I'll help hold him down if you need backup.」

A rather vengeful sentiment, but I sympathized. 「Noted. I suppose you had some trouble of your own?」

Martin nodded. 「Yeah. I never found out who, or I would have filed a complaint with the Phoenix Society.」

「File a complaint anyway when you get home.」 But that wasn't what she needed to hear. 「I'm sorry you were denied justice. If I catch the creep who photographed me, I'll be sure to find out who else they've harmed.」

「Thanks, Adversary.」 Martin brightened as she snapped out a smart salute, hand over heart, before rushing off to catch up with her fireteam.

We escaped Fort Clarion and returned to town without incident. Keeping the rifle, I made tracks for the Town Hall after dismissing Mike, the militia, and the youth volunteers. Cat bounced out of her chair. "How did it go, Adversary Bradleigh?"

"I need to report to Mayor Collins as soon as possible. Can you tell him I'm here?"

"Of course." She shot a glance at Sheriff Robinson. "Will you be reporting as well?"

Robinson nodded. "Yeah. We need Mayor Collins to order a full muster of the Clarion Volunteers. I haven't got the authority to do it myself."

At least nobody got hurt before the Sheriff started taking the mission seriously. He must have caught my expression, for he turned his attention to me. "I misjudged the situation, Adversary. I'm sorry. We'll get you the support you need if I have to march people down there at gunpoint."

"Thank you." Offering my hand to show I accepted his apology, I glanced at the conference room the Mayor set aside for my use. "I need to show you something."

"All right." Robinson followed me inside, and closed the doors behind him. "Is this about that rifle?"

"It may be related." I retrieved the photo Martin had Brubaker bring to me from inside my jacket, and showed it to Robinson. "Sergeant Martin found this with the rifle, but didn't mention it on the air."

Taking the photograph from me, Robinson sat down and studied it in silence. His jaw clenched several times, as if he had something to say but choked it back. "This was taken with a Solaroid Instant, Adversary. Nationfall put the manufacturer out of business. Nobody makes them anymore even after the Phoenix Society placed all pre-Nationfall copyrights and patents in the public domain."

A camera model that hasn't been manufactured since Nationfall would most likely be a rarity by now. I'd be shocked if more than a handful of people in Clarion owned a Solaroid Instant. "Sounds like I should find an avid camera collector and ask them some questions."

Robinson wouldn't speak. He wouldn't meet my eyes. His shame was suggestive, but I had to ask the question. Drawing my sword, I let him have a good long look at the edge as I leaned in to whisper in his ear. "Was the camera yours, Sheriff?"

"I wasn't the photographer, Adversary. I know I rubbed you the wrong way when I asked to search your room, and I made your job harder than it had to be today, but I didn't take that photograph. I'll take any oath you ask of me, and swear it by any power you venerate."

"I don't want your oath, Sheriff. I believe you."

"Why would you believe me?"

"Because if I find out you're lying, your last sight before I carve your eyes from your skull will be that of crows fighting over your tongue. Have I made myself clear, Sheriff?" He nodded, and I sheathed my sword to offer my hand. "But as I said, I believe you. What happened to your Solaroid?"

"Somebody burglarized my house the day you arrived in Clarion. They took the camera, my best hunting bow, my arrows, and some cash. I filed an insurance claim and thought no more about it until I saw that photo."

Burglary in broad daylight? That's pretty damned bold for a town like Clarion. "Is that why you wanted to search my room?"

"Not exactly. Somebody phoned in a tip suggesting I check you out." He offered me the photo. "Here. You'll want this as evidence, right? It looks like somebody wants you whacked. Once we have people at our disposal, I'll organize a guard detail for you."

That's just what I need: sheriff's deputies or irregulars from the Clarion Volunteers up my ass wherever I go. We would just be putting more people at risk. While a competent sniper could take me out directly with a head shot, a lesser marksman might take out the guards to open up a clear shot. Worse, a sniper might ignore me in favor of my protectors to terrorize the populace. "I don't want to panic the residents without cause. A garrisoned fort in the woods is one thing, but a sniper in town is a different matter. Besides, you can escort me back to the Lonely Mountain after our debrief."

"It's the least I can do." Robinson coughed as Cat opened the doors. "Looks like Brian's ready to see us."

Mayor Collins might have been ready for us, but I daresay he seemed edgy. Maybe he was just reluctant to hear our news. "Adversary Bradleigh, I understand you discovered that Fort Clarion is inhabited. By whom?"

I told him everything I knew thus far, rounding up with, "The state in which we found Fort Clarion suggests a level of discipline that precludes the possibility of the fort being a retreat for geeks. I need additional resources to flush out the inhabitants and neutralize them."

"Why couldn't you do the job with the volunteers Sheriff Robinson provided today?"

Robinson spoke up before I could. Probably a good thing; the Mayor was starting to annoy me. "Your Honor, I only provided Adversary Bradleigh twenty irregulars from the Clarion Volunteers. The rest were too young for militia duty."

A peevish tone crept into Collins' voice. "The town has been safe so far. I see no reason that this can't wait until after the harvest. The town can't afford to let the crops wilt in the fields while farmers go gallivanting through the forest playing soldier. Not to mention the upcoming Clarion Rocks festival."

"Fort Clarion's safe as long as you're not a tourist. How many have you managed to lose to the forest over the last decade, Your Honor?"

"You're being paranoid, Adversary. I'll not tolerate any slander concerning Clarion's safety."

Paranoid? Slander? My sword-hand twitched as I choked off the urge to bare steel and cut this gaslighting choad. A slice across his forehead wouldn't kill him. Hell, it might even give him the sort of scar that lends an otherwise unprepossessing man an attractive hint of danger. God knows he could use it, now that I've had a good look at him. "Were I paranoid, Your Honor, I might suspect you of obstructing a Phoenix Society mission. But that's a dreadfully serious charge, and surely you wouldn't be that foolish. Are you that foolish?"

Collins rose, his eyes going narrow and piggy. "Who are you to threaten me?"

Shaking my head, I produced my ID. "A sworn Adversary in service to the Phoenix Society, remember? As such, I am authorized to do far worse than threaten you. Sit your arse down and do as you're told, and I'll refrain from giving your deputy mayor an unexpected promotion." A moment's research gave me the information I required. "This is how it's going to be, Your Honor. Since the harvest is indeed important, surely you and your brother won't mind shutting down the Collins Glass Works for the duration of my mission. This will place five hundred irregulars at our disposal, should they all volunteer for militia duty. Since the Phoenix Society pays militia volunteers time and a half, this should give me the forces I require without interfering with the harvest."

"B-b-b-but my brother just landed a huge order! He can't afford to halt production!"

My sword was out in a flash, its tip pressing the end of the Mayor's nose. It was just the thing to clarify his situation. "Your Honor, have I stumbled upon a conflict of interest meriting a forensic accountant's attention, or just an inability to prioritize? Your sole priority should be getting me the required personnel. If your brother needs to hire temporary workers, he is welcome to apply to the Society for compensation."

Leaving the ‘or else' part unspoken, I sheathed my blade, turned on my heel, and left the Mayor seething. He can't be so stupid that I need to spell out the rest for him, can he?

## Track 22: Pantera - "Cat Scratch Fever"

"I saw how you handled Mayor Collins." Saul Rosenbaum chewed his cigar a moment before continuing. If he was going to excoriate me at any point during my daily report from my room at the Lonely Mountain, this would be the time. "The word ‘imperious' comes to mind."

"You should be used to it, since you served with Iris Deschat."

The old Director puffed his cigar. "Iris was just like that back in the Navy, especially if someone denied her personnel or materiel she needed to carry out her mission."

A proud-looking woman Saul's age glided into the office behind him and rested a hand on his shoulder. She still wore an Adversary's smallsword on her hip. "So, Saul, you finally decided to throw me over for a younger woman?"

"Ma'am, it's not like that." Why did I have to say that? Their relationship was none of my business, was it?

An indulgent smile made her grandmotherly instead of commanding for just a moment. "Don't worry, Adversary Bradleigh. It's how Saul and I flirt. You'll keep that to yourself, of course."

"Of course. Director Rosenbaum, do you have any further questions?"

"No. Be careful out there. We need additional evidence before we can justify sending you reinforcements." The screen in my room went dark as Saul disconnected.

With my report to the New York chapter complete, I was done for the night. The sensible thing would be to get out of these clothes and into bed.

Unfortunately, I was too restless to be sensible. Where were Fort Clarion's inhabitants? Do they hide during the day? Did they hide today to avoid me and my posse? Was Chris Renfield among them? The questions chased their tails as I sat at my desk.

Fuck it. It's not like I don't know the way to Fort Clarion. With my implant to guide me, finding the place in the dark should be no problem.

Equipping myself with my sword and a small emergency kit, I slipped past a small crowd that had gathered to watch a rather spirited Catacombs & Chimeras campaign. Kaylee refereed, gleefully using imagination and dice rolls to challenge the gathered players.

It was a clear, starry night with a waxing gibbous moon to light my way to the Old Fort Woods. My eyes soon adjusted, though I checked the documentation for my implant's low-light enhancement functions anyway. It would be darker in the forest.

Whippoorwills advertised their presence all around me as I found a small clearing. A rabbit stood on its hind feet, staring at me. I ducked as I heard a hoot and a soft flutter behind me, and a great horned owl swooped down over my head. It plucked the rabbit from the ground, its talons digging deep into the fur to secure its grip. Dinner to go.

A coyote howled as I continued through the forest. Bloody shame I lacked sufficient wilderness survival training to determine if the beast was alone and far off! The howl went unanswered, presenting its own problems; a lone coyote might be rabid. However, it wasn't the coyote I needed to fear tonight.

Had he been with me, Mike might have noticed the signs and guided me away from the cougar's den. Instead, I stumbled upon it. Worse, the den contained a litter of spotted kittens waiting for their mother to bring back a kill. Backing away from the den to avoid disturbing it, I marked it on my virtual map and skirted it. Unfortunately, that wasn't the end of the matter. I'm not sure if the cougar that attacked me was the mother of the cubs I had happened upon, but as soon as I lowered my guard, she pounced.

She would have had me if not for sheer dumbass luck. Leaves rustled behind me, and I turned while unclipping my still-sheathed sword from my belt. I saw the cougar then, her sleek body gathered for the spring that would have let her slam into my back and drive me into the dirt while she fastened her jaws on my neck and snapped my spine.

I overrode the training that told me to make myself a smaller target. That training was for human opponents wielding weapons, not for big cats taking a swipe at lone nighttime hikers. Instead, I wanted to make myself as huge and threatening as possible if the information I pulled with my implant was reliable. 

Drawing my sword, I held the reinforced sheath in my off-hand. Rather than  shouting, however, I used my training as a dramatic coloratura soprano and projected the full force of my voice into a sustained high F that I hoped would drive off the puma.

No such luck. The damn cat sprang at me. I stilled my voice and spun aside, hitting the cougar with my scabbard. It struck with a sharp crack, and the cat shook her head as she landed and faced me.

"You pussy! Is that all you've got?" This time I shouted, advancing upon it with my arms spread wide. "You already took your best shot, and you blew it. Bugger off!"

The cougar shrank back, snarling, and sprang a second time. I dodged, my sheath hitting home with a thwack. "Bad kitty!"

She sprang again, but instead of leaping for my throat, she swiped at my legs with one of her massive forepaws. And caught me just above the top of my boot, tearing my jeans and ripping my shin. Enraged by the pain, I rained blows on Puss with my sheath. Though I could easily have finished the fight with my sword, I didn't want to kill the beast despite my pain and anger. Predators will prey.

A strike across the cougar's nose drove her back. I hurled my voice at her, shrilling a high, clear tone. Raising my weapon again, I made to advance. She backed away several steps before bolting into the underbrush. I waited a minute, only to tense as someone… clapped.

"I'm not going to pounce on you." Christopher Renfield approached slowly, showing me his empty hands. This let me relax enough to sheathe my sword.

After clipping my sword  on my belt, I took several deep breaths to regain my calm. "You startled me. Who the hell expects applause after fighting off a cougar?"

"Sorry. I was just impressed by how you handled the situation."

At least I was able to defend myself without killing that cat. Not that I'd admit it to Renfield. "Hello again, Sergeant. How did you find me?" And are you a friend or foe?

His teeth flashed in the starlight. "I followed a high note that pierced the night. A woman's song made a battle cry. Was it yours, Naomi?" He had drawn close enough to whisper my name in my ear.

The energy surge that had filled me faded away with the adrenaline. The pain of my wound and relief all but turned my legs to jelly. Was I really wired enough to flee my bed for a midnight walk before? Hard to believe, because right now I wanted nothing more than to clean out my scratch, bind it, and curl up in bed, but I didn't dare let myself pass out here. It wasn't safe. "That was me. Can you give me a little space? The cat got in a good swipe at my legs."

Renfield nodded, and pointed towards a large boulder just the right height to sit on. I recognized it; the trap door to Mike Brubaker's little hideaway was nearby. "Would you like some help?"

My teeth began to chatter as I tried to answer, and couldn't get any words out. Renfield led me to the rock, and sat with me. "Was that your first time fighting, Ms. Bradleigh?"

The question and Renfield's arm around me helped me focus. "Believe it or not, it isn't."

Renfield's arm tightened around my shoulders. "You did well. You're alive, and so's the cougar. But let's get a little fire going."

Some primal instinct of mine agreed that a fire would be nice, but not out here in the open. That sniper was still out there somewhere. "Got a better idea, Sergeant. There should be a trap door nearby. If we can find it and get underground, I can check my leg and be on my way."

Renfield crisscrossed the clearing a few times, stopped, and crouched. The trap door came up with a soft creak. “Is this it?”

“Probably.” Fortunately, I wasn't so badly hurt that I couldn't descend a ladder. Finding the switch by touch, I flicked it. Only one small light worked, and even that was almost too dim to be of use. It was just enough for me to tell that it wasn't Mike's basement. His lacked a fireplace. Taking a match from the canister left by the hearth, I struck one and held it under the flue. If it was clear, the smoke would escape. “See any smoke?”

“Yeah.” Seconds later he was tromping down the ladder. “Holy shit. Somebody managed to conceal this old chimney stump and make look like a natural rock formation.”

“Hopefully just some squatter who's moved on.” It seemed likely, given the carefully folded pile of old blankets I found. They didn't smell bad, so I shook one out and felt for creepy-crawlies. When I found none, I spread it out on the floor.

"Well, how about I get some firewood?" Before I could answer, he brushed a soft kiss against my cheek. "We'll talk more in a bit."

As he left to gather brush, I drew my sword just in case. Its edge gleamed in the dim electric light, and it wasn't long before my hand was strong and steady again.

Renfield returned with the deadwood, and put half of it aside. I sheathed my sword so I could help, but he waved me away and arranged a small pile of firewood in the hearth. Once he was satisfied, he lit the kindling and tended the flame until it was greedily lapping at the wood. "That's better."

"Thanks." Now that I could see properly, I took off my boot. I tried rolling up the pant leg, but the cut was too close to my knee. If I had a knife, I could just cut the leg off these jeans, but all I had was my sword. "I'm sorry, Sergeant, but I need to take a look at the wound."

Renfield turned his back. "Just give the word when you're done."

"Thanks." I shimmied out of my jeans and assessed the damage. The scratch was wider than it was deep. Blood seeped from the gouges, and the skin around it was swollen. Good thing for that damn cougar that she has kittens, assuming I had fought off a mother cat.

"How bad is it?"

"It's inflamed, but not deep. Damned sore, though, and the muscle's already a bit stiff." I cleaned the scratch before applying an analgesic salve containing medical nanotech that cleared out pathogens and sealed wounds. "Damn, this stuff works fast."

"What is it?"

"Just a standard-issue first aid gel. I've never had to use it before." Unfortunately, none of the accompanying band-aids were big enough. "Can you help me wrap this up?"

Renfield glanced over his shoulder. "You sure?"

"Quite." I wrapped the gauze around my leg and held it in place once I was done. "I need an extra pair of hands."

Renfield knelt before me, resolutely keeping his eyes on my face. "What do you need me to do?"

"Cut the gauze, and tape it up. I'm not sure I can do it myself without it all unravelling."

Renfield drew a knife and carefully sliced the gauze where indicated. He then taped me up, using just enough to keep my leg wrapped. Once finished, he gazed at me with a small, playful smile curving his lips. "Want me to kiss it better?"

Nice of him to ask first. Remembering how his lips seared mine the first time we met, I reached down and ran my fingers through his crew cut. "You're welcome to try, Sergeant."

## Track 23: Halestorm - "I Get Off"

He didn't kiss me through my bandage. Instead, he took my foot and cradled it, his fingertips gently massaging my instep as he kissed my ankle above the cuff of my little black sock. It was a miniscule kiss, a bare brush of moist lips, but I felt better already.

He took my other foot, but did not massage it. Instead, he kissed my ankle before placing another kiss a little further up. He then trailed warm open-mouthed kisses up my uninjured leg, until he had reached my knee. While caressing my calf, his fingertips strayed and brushed a sensitive area behind my knee that forced a sigh from my lips.

His curled in a roguish smile as I laid back. "Looks like I've found a sweet spot."

There'd be a sweet spot for him if he kept this up, and I hoped he would. My elbows relaxed as I yielded to the pleasure he offered and parted my legs a little. Would he catch my hint at where I wanted that hot mouth of his?

If he did, he was subtle about it. Lifting my leg higher, he kissed his way up my calf until he found the sweet spot behind my knee. He lingered there, treating me to quick flickers of his tongue against my hamstring that made my toes curl and my vagina clench in anticipation. 

The responsible, conscientious side of me protested, insisting that I shouldn't be fucking around on the job. She was easy to placate; I wasn't abandoning my reconnaissance, but gathering HUMINT by social engineering—or should it be sexual engineering? But that was a rationalization. 

The Devil's honest truth was that I wanted, and felt entitled to, some meaningless rebound sex without interminable dates and whispered endearments. Just this once, I wanted to tell a man I found appealing to drop his pants and make himself useful without having to visit Xanadu House and pay a rent boy for the privilege. The fact that this was profoundly dangerous rebound sex only made it hotter. I was going to use Renfield for my own pleasure, and the only one who could stop me was the man himself.

He hardly seemed inclined to refuse as he kneeled before me and gently draped my legs over his shoulders. Christopher Renfield was obviously a man who understood his place, so I ran a hand through his crew cut while licking my lips. "Higher."

Every brush of his lips against my inner thighs burned. He looked so good down there. The sweet torture of his kisses made me squirm against the rock; I was so close, but not close enough.

That only made things worse as I imagined myself pinned between a hungry Renfield and the granite beneath me, but I didn't care. I wanted myself caught between a cock and a hard place.

He was finally where I craved him as he kissed me lightly through my panties. He pressed a harder kiss right over my clitoris, which was in dire need of attention.

The pressure of his hot mouth made me squeal as I grabbed his head and held him in place. He soon had his hands under me and was massaging my ass while clamping his mouth around my vulva and sucking the tender flesh into his mouth.

Renfield's voice was a rough purr as he slipped his teeth between my tender skin and the sodden fabric of my panties and pulled backward. Realizing what he wanted, I lifted myself to see if he could actually manage to get my knickers off with his teeth. None of my other lovers ever managed it, though I had a couple who mastered the trick of unhooking my bra one-handed. "I think you're ready now."

Damn right I was ready. My pussy was an orchid in full bloom, my nectar flowing freely. Renfield tasted me, drawing his tongue up from my entrance to my clit, which I exposed for him by gently opening myself. I moaned and shivered beneath him, desperate to be consumed.

But that would be a surrender. Instead, I lifted his head from me. My voice was deep and rough with lust. "On your feet, soldier. Get out of that uniform."

Renfield licked his lips, smiled, and thrust my wet knickers into his back pocket. Cheeky bastard. "Yes, ma'am."

He made a striptease of it, unbuttoning his uniform shirt and drawing it open as I slowly circled my clit to keep myself hot. Once he had his shirt off, he dropped to his knees. Disregarding my order, he knelt before me again and kissed my fingertips, his tongue gently lashing my clit before dipping inside me.

He drew a long, shuddering moan from me as I stroked his hair and let him tease me for a bit before pushing him from me again. "That's insubordination. Prepare for inspection, Sergeant."

A faint hint of disappointment sharpened my pleasure when he returned to his feet with a mock salute. He submitted too easily. I wanted someone who might just have the strength to overpower me, the sort of man I've always denied myself.

The firelight dancing over the faint sheen of sweat clinging to his chest and belly made me lick my lips. Every muscle was gently defined, the product of rigorous physical training. I've fantasized about riding a dozen such men, but all of them were Adversaries and thus forbidden fruit.

Renfield should have been forbidden, but tonight he would be mine. Tonight, he would place that hot mouth, those strong, gentle hands, and what was most likely a luscious cock in my service until I had had my fill of him. "Get those pants off already, you bastard. Don't make me come over there and do it for you."

He retrieved something from a pouch on his belt. "Mind holding this for me?"

"Sure." It was a condom. Thank God he thought of it instead of making me bring it up. I narrowed my eyes as he stood on one bare foot to get his other boot off. Damn it all, this rubber expired decades ago. Fortunately, he looked good enough to eat. "Get those trousers off already."

Renfield obeyed, and bent to kiss me as I reached for him. "Closer."

Another step and he was mine. His erection stood straight up, and grand enough to fill me to the brim. It quivered as I stroked his chest and dragged my fingertips down over his abs before caressing his thighs. A little pearl of moisture gathered at his tip, and I tasted it with a slow, lingering kiss. He groaned as my tongue circled him, and I gave his balls a gentle squeeze as I tried taking more of him in my mouth.

His tip was all I could manage without my fangs hurting him. As far as I know, the inability to really go to town on a good-sized cock like his was the sole drawback of having CPMD.

Some women didn't care, leaving guys wondering if one of Dracula's brides had gotten at them, but I settled for kissing and licking and stroking him with my fingertips. He seemed to like what I was doing, because he tried to push more of himself into my mouth while stroking my hair.

Withdrawing, I gave his cock a gentle slap that made him moan. "Control yourself. This isn't all about you."

"Please." His ragged plea was barely audible as he smoothed my hair and stood at ease, his hands clasped behind his back.

If he wants to act the soldier, I'll do him like one. Crooking my finger, I smiled up at him. "Present arms."

He took a half-step forward, putting his weapon just close enough for me to do anything I liked without having to strain. Dipping my head, I kissed one of the hot balls drawn up tight beneath his root and gave a little lick before doing the other. I nipped his groin, letting his cock brush against my cheek as I trailed kisses upward.

Even his nipples were hard, all six of them. I couldn't resist tasting each one as I took him in hand and stroked him. I tasted myself on his lips before whispering in his ear. "Where do you want my mouth? Tell me."

"I'm going to ruin your jacket if you keep jerking me around. You're killing me here, Naomi."

"Am I?" I sat back on my rock and unzipped my jacket. Shrugging it off, I toyed with one of the buttons on my blouse. "Maybe I should take the rest off. And stop playing with that. It belongs to me now."

"Yes, ma'am." He stood at ease again, taking deep breaths as his cock twitched in time with his heartbeat. Was he trying to back away from the edge? His eyes were rapt as I undid every button, and shrugged off my blouse. All that remained was my camisole, for I had removed my bra at the Lonely Mountain.

My nipples strained against the fabric, and I teased him by tweaking them, sending little jolts to my hungry pussy. "Want me to take this off, too?"

Too bad if he didn't. It joined the rest of my clothes, and I sat before him with only the fall of my hair to lend me any semblance of modesty. His leaking cock was slick as I took him between my breasts. "Like that?"

He thrust upward, as if he wanted to fuck my cleavage, but I had a better idea. I dug my fingertips into his tight round arse and worshiped his cock the way he did my pussy. I dragged wet kisses upward from his base before loving his tip with my mouth, staring up at him the whole time.

When he tensed, I dug the nails of one hand into his ass while I grasped his base and his balls. Drinking deep, I took my fill as he threw back his head and cried my name.

Seconds later, his mouth was on mine. He kissed me hard, his tongue slipping deep inside. If anything, he was more ravenous than before as he made love to my breasts before trailing kisses downward. I guided him, my hands stroking his close-cropped hair.

He licked me as he had before, his tongue dipping inside me before drawing my clit into his mouth with a gentle kiss. I climbed higher every time he did it, my breathing ragged as I urged him on. When I finally came, it was with such force that I thought I'd hit escape velocity, my climax launching me screaming into orbit.

Renfield stared up at me, a smug little smile on his lips. "Where's that condom?"

Condom? Dammit, I was almost high enough to throw caution to the wind and tell him to do me bareback. "You can't use that. It's expired."

He held up his first two fingers. The claws CPMD gave him were cut short, but could still hurt me if he was careless. "I can use it to cover my fingers."

I clenched at the thought of his fingers in me as he licked me to another climax, and cast about, my hands seeking the old condom. I happened upon my first aid kit, and felt a packet inside. Pulling it out, I examined it in the firelight.

It was a brand new lubricated condom. Damn, the Phoenix Society thinks of everything when they pack these emergency kits. "How long will it take you to reload that gun of yours?"

Renfield stood, and he was already rampant. "Locked and loaded."

Tearing open the wrapper, I rolled the condom down his shaft. Once he was armored, I pulled him close and guided him.

Renfield and I were doubly joined, sharing our breath as he surged into me, and it was everything I hoped it would be when I commanded him to strip. He used me as hard as I used him, his muscular arms tight around me as I drew my legs up and wrapped them around his waist.

He redoubled his efforts, hammering me with long, hard strokes that left me almost empty before filling me again. Every impact sent a shockwave through my body that lifted me to heights I'm lucky to manage alone, let alone with a partner.

When the explosion finally happened, it left me breathless and unable to do more than whimper. I quaked beneath him, and held him close as my climax provoked his own. My shoulder burned, but it seemed inconsequential compared to the delight spreading outward from my core until it permeated my entire body and left me flushed with hot, boneless pleasure.

## Track 24: Judas Priest - "Love Bites"

The afterglow faded, but the burn in my shoulder remained. It flared as Renfield's tongue lapped at the wound. Had he bitten me after fucking me to an utterly delicious climax? Normally I like a good love bite, but now he was feeding on me like some kind of vampire bat.

Overcome by loathing, I pushed him off and grabbed my sword. He scrambled to his feet, eyes wide with shock. Holding my blade between us, I pressed my other hand against my torn shoulder. "What the fuck is wrong with you?"

"What? What did I do?"

"Don't play the bloody innocent with me. You bit me." A hickey is one thing, but drawing blood is right out. "Are you telling me you had no idea what you were doing?"

Renfield slowly shook his head. His eyes seemed clearer when he finally looked at me again. "Oh, shit. I'm sorry. I never meant to do that."

Lowering my sword just a little, I noticed he was still hard. Maybe he just liked getting smacked around? "You seemed to have been enjoying yourself."

Flushed with shame, he pulled the used condom from his shaft and tossed it into the fire. "It looks like I owe you an explanation. Can we get dressed first? I'll tell you everything I can."

"Stay there. I'll give you your clothes after I've tended to my shoulder." Fortunately, Renfield had the common courtesy to bite somewhere I could easily reach. That allowed me to dress the wound without his help. Once I was done, I threw his clothes to him and checked the dressing on my leg before pulling my jeans back on. He still had my knickers, but wet panties where the last thing I wanted to put back on.

Once I was dressed and had regained some composure, I joined him by the fire. If he hadn't ruined it, I might have settled beside him and rested my head on his shoulder. Instead, I sat across from him with my sword resting across my lap. "Start talking."

"I probably sounded crazy to you when we first met. I know Nationfall was decades ago, but it's easier to keep up the pretense. If I keep my story straight, my secrets remain safe, and so do my men. If people mistake me for a re-enactor or military nerd, they won't come looking for a forgotten Commonwealth Army unit." Renfield spoke slowly, staring into the fire. His voice was haunted, and he slumped with the weight of decades. Yielding to my lust for him was probably a huge mistake on my part, regardless of whatever info I might gather while he’s purry and contemplative. How the hell did I not realize he was old enough to be my great-grandfather? It's not like I'd have taken Edmund Cohen to bed, but at least he has the common decency to look his age. "Do you understand what I'll be doing if I give you the explanation you deserve?"

It was easy to guess, given Renfield's behavior. He was somehow involved with Project Harker, but in what capacity? "You need not betray your country or your men. But I've brought locals to Fort Clarion. You're going to have to join the rest of us in this century."

He spat into the fire. "Moving on might be possible for me, but I'm not sure about my men. We're not what we were when we enlisted. The Commonwealth did something to us and then abandoned us. The government collapsed, the Prime Minister ate his own gun, and nobody thought to release us from service. There was nothing for us outside Fort Clarion, so we clung to our last set of orders: protect the base at any cost."

Was Renfield part of an elite unit? "Can you tell me more about Dusk Patrol? Sounds like your unit had strong espirit de corps."

"We were Third Infantry's all-CMPD platoon, and we were damn-near unstoppable." Even now, decades later, he sat up straight and spoke proudly of his outfit. "You could drop us in the middle of a clusterfuck with nothing but our BDUs, and we'd still get the job done."

Nothing but their combat fatigues? Did the Commonweath brass expect them to loot their weapons, ammo, and rations from the bodies of the first enemies they managed to strangle? I suppose that's one way to give taxpaying citizens a break. "Trying to impress me?"

"You sounded pretty impressed earlier, at least until I bit you and ruined everything."

Good point, but I wasn't going to let him get away with it. I was still angry, and I had yet to find the answers I want. "Don't get cocky. Just get to the point."

"Right. We were some of the best the Army had, especially if you wanted to drop a team behind enemy lines to raise Hell. If you told us to take out a supply depot, we'd not only do it, we'd fuck with the enemy's heads while we were there."

"So, you combined psychological and unconventional warfare?"

"Yeah. One job, we went to West Africa to take out a bunch of nutjobs who had taken to kidnapping schoolgirls and selling them into forced marriages. I'm talking kids no older than thirteen. Another division already rescued the last batch of girls these assholes kidnapped, so we went in to do the local government a favor and make sure the terrorists would never pull a stunt like that again."

"What did Dusk Patrol do?" Whatever it was, I had a sneaking suspicion the Society would call it a war crime.

"We got the leader, brought him to a pig farm, slit his throat, and chucked him in the pen. We recorded the pigs eating him on video, and sent the video to all his cronies with a little note telling them they'd be next if they didn't learn to respect women."

"And the pigs didn't mind engaging in cannibalism?" The question slipped from my lips before I could stop myself, and I immediately regretted it. It was too flippant, and thus unbecoming of an Adversary.

He didn't seem to mind, though. He began to laugh, but suppressed his amusement. "No, the swine didn't seem to mind. But they got pretty fat before people got the message."

Bloody hell. How many militants did Dusk Patrol feed to swine? And what am I to do with such information? Would the Society prosecute him and the surviving members of Dusk Patrol as war criminals? What would be the point? Anybody capable of testifying in court is most likely dead by now. But why isn't Renfield? He doesn't look any older than me, which is impossible. Isn't it? "How do I know you aren't bullshitting me? You don't look like the old man you should be."

He nodded. "Didn't your parents tell you? People with CPMD don't age like regular people once they hit their mid-twenties. At least, I've never seen somebody like me looking old. I'm not sure anybody knows why."

"I was adopted. My parents don't have CPMD."  And if Renfield wasn't bullshitting me, I might have to change my name and pretend I'm my own daughter in another twenty years, like in *Exiled Goddess*. Not that Renfield would have seen that movie. Too recent.

But how exactly would Sophie and Howell react to my perpetually youthful appearance as they continue to age? What about my brothers? Would they eventually treat me differently? It's one thing to read about CPMD on the network, but living with it, or somebody who has it, is probably a different story.

Maybe Renfield is different. He isn't the first carrier I've met, but what would happen if a carrier and a normal person had children? Is that even possible? "But what does that have to do with you lapping at my blood? Unless you mean to tell me you're part of a squad of vampire soldiers like in that old D Corps series, I don't know what you're going on about."

"Sounds like you read a few of those books yourself." He paused a moment as if recollecting. "They were a hit with the squad, but after reading a few, some of the guys decided the next time they had to take out sentries on a raid, they'd make it look like Dracula got ‘em. They'd rip out some poor bastard's throat with their teeth instead of using a knife."

"That sounds more like torture than psychological warfare."

My disapproval must have been evident from my tone, because Renfield raised his hands as if to ward off a blow. "I didn't like it either, and neither did the rest of the outfit. We made them cut that shit out, but not before the brass found out."

"What happened? Were you punished?"

Renfield shook his head. "No. The goddamn brass were delighted. After all, a lot of the people we were sent to fight were superstitious. They weren't afraid to fight men, but blood-drinking fiends who strike in the night were a different story."

"But you weren't actually vampires then, were you?" Everything Renfield told me thus far indicated that the vampirism was just an act no different from Jacqueline pretending to kiss me when she wanted to fend off a man's unwanted attention. It didn't explain why he had fed upon me.

"No, but some bright lights in the Army Medical Corps decided to fix that." His tone turned bitter. "We already had sharper teeth and superior night vision because of our condition. Army Medical worked on making fiction reality. They wouldn't tell us what they were doing, or explain the side effects. According to them, we didn't need to know even though they were doing it to us. When some of us refused to participate, they decimated us. One out of every ten men got a bullet to the head, even if they had cooperated with the experiments."

If the Commonwealth Army could treat its men so harshly, what was life like for civilians? Had the Commonwealth become some kind of police state toward the end? Regardless, Renfield's talk of medical experiments rang a cathedral's worth of bells in my head, so I made an educated guess. "Was Dr. Henrik Petersen involved? If my intel is right, he would have held the rank of colonel at the time."

"Petersen? No." Renfield shook his head, and his tone softened. "He wasn't a doctor at the time. Dusk Patrol was his idea, and he looked out for us the best he could. He spoke up for us with the brass, but they wouldn't listen to him. We weren't people to the War Department; we were just weapons to be upgraded."

The fire had begun to fade to embers, and with it, the throb in my torn shoulder, but despite the information he provided, I still had more questions than answers. I put more wood on the fire, and watched as the flames tasted the fresh fuel, little red tongues flicking at the wood before flaring to full brilliance and pushing back some of the shadows cast by his tale.

Whatever the Commonwealth Army Medical Corps did to the men of Dusk Patrol, it must have succeeded. Renfield had tasted me, and no doubt derived some nourishment by doing so. Would he have drained me dry, given the chance? Was that even the central question?

As we sat in silence around the fire, I couldn't help but think that why he bit me wasn't the most important issue. Fort Clarion was. What happened to the men of Dusk Patrol there, and what role did Petersen play in what I was beginning to suspect was a tragedy. What did Fort Clarion have to do with Project Harker? What the hell was Project Harker, anyway? Was it an effort to turn men into weapons, or something more? And was it confined to Fort Clarion? Time for a shot in the dark. "Sergeant Renfield, what can you tell me about Project Harker?"

He narrowed his eyes, glaring at me through the firelight. "You are a goddamn spy. I knew it." The flames concealed his movements until he leaped over them and tackled me, the knife in his hand trembling against my throat. "Who sent you, and how much did they tell you? Start talking!"

## Track 25: Black Sabbath - "War Pigs/Luke’s Wall“

Was it rage that made Renfield's hand tremble as he held his knife to my throat, or fear? Either way, I lanced an emotional boil that had festered for years, and his violence was the pus spewing forth. Project Harker must have been the codename of the program that made him and the rest of Dusk Patrol monsters instead of men. But why was he so desperate to preserve its secrets? And why is the Phoenix Society keeping it secret, even from Adversaries?

"You think I'm playing, girl?" Renfield screamed in my face, his eyes bulging as his saliva sprayed across my skin. "I can see your mind working behind those demon eyes. Stop thinking of how you're gonna bullshit me into letting you go and answer my question. Who sent you, and how much did they tell you?"

"The Phoenix Society sent me." Though Renfield surely had the training to completely immobilize me, he didn't do so. He simply straddled my chest, pinning me down with one hand around the base of my throat while he held the edge of his knife under my jaw. Did he think I'd be more willing to spill my guts if he left me a hope of escape?

Unfortunately for him, throwing me to the ground and holding a knife to my throat while barking questions in my face doesn't frighten me enough to make me submit. I brought my hands up, my palms simultaneously striking his temples. Stunned by the blow, throwing him off became a pathetically easy task. I bound his hands behind his back with strip-cuffs as gently as I could; though he had gotten violent with me twice, there was still a chance he might yield valuable testimony if treated carefully.

Taking his knife, I put it behind me where I could retrieve it, but he couldn't. I then picked up my sword and made sure he got a good look at the blade. You'd think he'd be familiar with it by now, since I had drawn on him once already. "I am an Adversary, sworn to root out abuses of power like those you described, and the Phoenix Society didn't tell me a goddamned thing about Project Harker. It's classified. What little I know, I bloody well had to figure out on my own."

He stared at me, the muscles in his arms bunching and twitching as he tried to houdini his way out of the strip-cuffs. "What the hell is the Phoenix Society?"

If he can ask a question like that, then I probably didn't give him a concussion. Still, it might be wise to phrase the answer in terms familiar to him. "Before Nationfall, there was a watchdog group called the North American Civil Liberties Union, wasn't there? We're a more militant version of the NACLU. We don't just file civil suits when somebody alleges an individual rights abuse. We make arrests and put tyrants on trial."

Renfield nodded, and relaxed a bit. "So, am I under arrest? I've assaulted a Phoenix Society officer twice, haven't I?"

"You have." Which was very naughty of him, but saying so would be too flirtatious. "However, I'm prepared to overlook both incidents if you cooperate with me and tell me what I need to know. I'll even cut you loose."

He studied me a moment. "Why are you even here asking about Project Harker if your bosses wouldn't tell you anything? Are you even supposed to be here?"

A truth for a truth. "I was on vacation. I heard about people disappearing around here while getting a drink back in Manhattan, and got curious."

"And then you found the Fort with that local kid."

"How did you know?"

He averted his eyes, as if ashamed. "I saw you two through my scope. Colonel Petersen told me about you and even gave me a pic. I don't think he realized we had already met."

Son of a bitch. What the hell is Petersen's game? How am I supposed to square his behavior with Renfield's characterization of the man as an officer? Is he still just looking out for his troops? "Why am I the primary target? And why didn't you shoot me when you had the chance?"

He still wouldn't look at me. "The Colonel knows more about Project Harker than I do. He knows everything, and it scared the shit out of him. All I know is that we've got to keep the secret. It's the only way to protect the rest of the outfit, and make sure what happened to us never happens again."

His eyes swam with unshed tears, and his voice took on a pleading tone. "I didn't shoot you because something told me you'd understand if we could just talk."

"We're talking now, but you still attacked me. You can make up for it by telling me more. For example, where's the rest of your outfit? Who keeps the base ready for an inspection by a platoon of coked up martinets?"

Unfortunately, that last bit didn't get so much as a chuckle out of Renfield. "We still live at Fort Clarion, but I can't tell you more than that. It isn't safe for them or for you and your men."

That pleading tone was still there, if not more intense than before. If he's this desperate, then maybe I've gotten everything out of him that I can. Maybe he really doesn't know anything else about what was done to him. "Did you ever try to find out about Project Harker for yourself, so you could find a way to reverse what the Army Medical Corps did to you?"

"I'm just a soldier. Colonel Petersen let me have a look at some of the files, but it was all Greek to me. All I caught were a couple of names that kept standing out."

Names are good. Names are leads that I can track down for more answers. "Can you tell me what names stood out to you? Do you remember?"

Renfield nodded. "Like it was yesterday. The first was ‘asura.' The reports kept using that word to refer to people like us."

People like us? Did he mean people with CPMD? We're all power-seeking deities from Hindu myth? Not that I felt like a goddess as I shivered despite the fire and forced myself to ask the next logical question. "What else?"

"The project had a civilian consultant. At least I think he was a consultant. Some guy named Ian Malkin."

"You'd better not be bullshitting me, Sergeant." The warning sprang from my lips before I realized it. Was Renfield referring to Dr. Ian Malkin, who reputedly worked for both Ohrmazd Medical Group and the AsgarTech Corporation to develop the first safe implants? If there was any evidence that as prominent a person in medicine and biotech as Malkin was involved in unethical clandestine military experiments, then this case just got a *hell* of a lot more complicated. "I've seen video of the man. He can't be a day over thirty."

"He's one of us, Naomi."

Which meant he could be old enough to claim Gilgamesh as a drinking buddy, but not look it. God damn it. This shit just keeps getting deeper, and I've got no one to throw me a rope so I can pull myself out. "Where can I find evidence to back up what you've told me? Does Petersen keep any documents in his home or office?"

Renfield shrugged. "No idea. But you wanted to see Fort Clarion at night, didn't you? That's why you came out here in the first place. You knew something was up as soon as you got a look inside."

"Yeah." How much time did I have left before dawn? It was two in the morning. "Someone's there, but I didn't see anybody during the day."

"You wouldn't. We only come out at night. If people saw us walking around during the day, it would blow the mystique. We scared everybody, not just the enemy. Hell, even some of my own guys actually believe they're honest-to-God nosferatu." He averted his eyes again as his voice faded.

Acting on instinct, I sheathed my sword. I used his knife to free him, and embraced him from behind as I sheathed his weapon. "It wasn't just physical release for you the second time, was it? It was emotional, too. It overwhelmed you." It wouldn't have been the first time I've seen a man's emotional control crumble in bed.

Renfield blinked away a tear. "It's been so long since anybody has touched me. I lost control. And you seemed to come harder as I bit into you. I didn't mean to drink from you afterward. I'm sorry, Naomi, and I don't expect you to forgive me. I don't deserve it."

Maybe he doesn't, but I've got enough to deal with without carrying a grudge over something that will probably heal up in a couple of days. Despite his lapse, he seemed like a genuinely decent person, somebody worth knowing. "I'm going to forgive you anyway, but I want a promise from you. The next time you taste a lover, it has to be with their consent."

His lips crooked in a half-smile. "You think there are women who will let me get my vamp on with them?"

Was he a virgin before me, or something? "Man, you have no idea how kinky some people can be. Just keep it safe, sane, and consensual so I don't have to kick your ass again and notify you of your rights."

"Fair enough." He chuckled. "Any other conditions?"

Now that I think of it, there was something else. Something that would make a relationship with Renfield viable if that's what we wanted after this was all over. "I think the life you've had to live so far, all the isolation and pretense and secrecy, has wounded you. I think you should talk to a shrink. If you cooperate with me for the duration of my investigation, I might be able to get you help through the Phoenix Society."

"Do you think I'm crazy?" Great. I should have remembered that his society stigmatized mental illness. "I managed to hold it together so far without any help."

"And how long are you going to keep carrying that burden?" Why did I even care? I'd have to be crazy to even consider getting involved with him after this case. Sure, he was hot and pushed all my dials past the red line, but that was never a reasonable basis for a relationship. "Would you insist on holding a position on your own if somebody was ready to relieve you?"

Renfield shook his head, his expression taking on a stubborn cast. "Do you think I'm crazy or not?"

Fuck it. The record will show I *tried* to be reasonable. "I think you're out of your demon-ridden mind, but I'm not one to judge. Insanity is both a prerequisite and an occupational hazard for Adversaries like me. That's why we get therapy after every mission. It keeps the crazy on a nice tight leash."

To my surprise, Renfield threw back his head and laughed, his mirth echoing through the night. Before I could stop him, he kissed me hard while plunging his hands into my hair. "Then let's run mad together when this is over. I don't know how much help I'll be, but I'm your man if you'll have me."

That sort of talk could get one in trouble. "Can you escort me to Fort Clarion? I need to see for myself what goes on over there."

He nodded, and turned toward the fire. "I can do that, but I can't let you inside. It isn't safe at night. They don't know you." Crouching, he gathered up a handful of the earth he had dug up earlier, and threw it atop the coals. "First, let's get this fire put out."

## Track 26: Nobuo Uematsu — “Force Your Way (Final Fantasy VIII)”

The tree to which I clung swayed gently in the breeze as I observed Fort Clarion. The base was so brightly lit that I was unable to understand how it managed to go unnoticed so long. The now-anomalous radiance should have made the installation visible from orbit. But as I used my implant to check satellite imagery of the area, I couldn't find a single photo taken at night that showed the base as I saw it.
If the Phoenix Society were hiding Fort Clarion and Project Harker, would their efforts to keep the secret extend as far as doctoring publicly available satellite imagery so that this place doesn't stand out?
Hell, why hasn't anybody from town investigated? Somebody should have noticed the light pollution emanating from this particular neck of the woods. Maybe hunters, or rebellious teenagers having a keg party. The missing tourists and runaway teens I had heard about were most likely the barest tip of the iceberg. What else was Clarion hiding?
Once my eyes adjusted to the light, I spied at least a dozen men bustling throughout the base. They worked in silence, checking every building. The early morning was so still, I could hear that damn recording from the base's post exchange. Would they realize that we had spent most of yesterday afternoon in there? Would my theft of that girlie magazine from the manager's office draw their attention?
The tree began to tremble, and I looked down to see Renfield ascending. I offered him the binoculars so he could take a look, but he just put them back in the case attached to his belt. "Now you have confirmed that Fort Clarion is inhabited, and by whom. What will you do?"
Did he hope I'd back off and leave Fort Clarion alone? "The Phoenix Society ordered me to catalog all materiel inside the base for proper disposal by an arms control team. I'm sorry, but I can't abandon my mission."
Renfield nodded. "Were I a lesser man, the smart thing for me to do would be to push you out of the tree and finish you off on the ground."
My whole body went cold, and my muscles tensed in response to his words. Would he murder me in cold blood for his squad's sake? "Doing so would do little to save your men at this point. If you murdered me, the Phoenix Society would send more Adversaries to investigate. They'd find you and the rest of Dusk Patrol, then rip Fort Clarion out of the earth piece by piece to get to you."
Renfield flashed a predatory grin revealing sharp white canines. "You might be right, but it would be a hell of a last stand."
Though I was only six meters from the ground, and could easily survive the fall, the thought of Dusk Patrol fighting to the last against any Adversaries sent to avenge me left me shivering. Should other Adversaries die at their hands, the Phoenix Society would wage all-out war against Renfield and his men. With control of GUNGNIR, GAEBOLG, and LONGINUS, they could flatten the area. "You would risk all that to save your men?"
"Yes, if I thought it would come down to a fight. But it's never so simple." Renfield looked away for a moment, and began descending. "Please come down. Dawn is approaching."
The eastern horizon proved him right. It was faint, but the first hints of morning twilight had begun to lighten the sky. It took a minute to get over the fear he provoked, but I followed him back to the ground.
"Can you find your way back to Clarion on your own?"
I almost bridled at the question, but it was a fair one given my lack of woodcraft. Fortunately, I had GPS, and had thought to mark that cougar's den so I could avoid it. "I'll be fine, but we're at an impasse. We can't leave things as they are."
Renfield shook his head. "We have conflicting missions, which should make us enemies despite what we shared tonight."
"Sad, but true. I don't want to hurt you or your men." They've suffered enough, and will most likely suffer more once they begin their journey into the end of the twenty-first century.
"I appreciate that." He glanced toward the base. "I'm going to share a secret that might allow you to carry out your mission―"
"Shh." I held up a hand to silence him, and drew my sword as the underbrush rustled. Eyes glowing with reflected retinal light all but surrounded us.
"Run!" Renfield gave me a shove toward the trail our hidden assailants left open. "I can't stop them if you stay."
Holding my naked sword at my side, I fled. At least one Dusk Patrol soldier pursued me if the footfalls behind me were any indication, but I doubted he'd follow me into town where he'd be discovered. Using my implant, I superimposed a map over my vision to guide me. A pulsing green arrow pointed the way to Clarion. All I had to do was follow it, just like in one of the games my younger brother played.
The marker I placed at the cougar's den soon appeared, and I briefly considered leading my pursuer there. With any luck, the cat would treat him as the greater threat to her cubs and distract him. I quickly rejected the notion because I doubted the soldier behind me would stay his hand to avoid condemning animals too young to hunt on their own to a slow death by starvation.
My years of physical training had made me stronger and more agile, and had improved my endurance, but it didn't make me superhuman. Though I felt as if I could run forever, I knew better. I would have to stop and face my pursuer while I could still fight. However, this trail was no place to make a stand. The undergrowth would hem me in.
Sighting a clearing up ahead, I quickened my pace and sprinted the last few meters only to stop short as a second soldier appeared on the other side. A glance over my shoulder showed that the soldier I knew about had not flanked me. Instead, he and his comrade managed something far more devious by keeping my focus on one man while another waited for him to drive me into the trap.
Trapped between hammer and anvil, I pulled my scabbard from my belt. Its reinforcement made it a better parrying weapon than an empty hand, but I'm not ashamed to admit that I regretted not thinking to bring a second knife. Hell, I should have brought my pistol. They can't dodge bullets, can they?
The altered soldiers drew long knives, keeping them between me and their bodies while using their empty hands to protect their torsos. The ease with which they gripped their weapons and confidence with which they approached confirmed my suspicions.
This wouldn't be an easy fight, but it was a fight I could win. They were combat-trained soldiers versed in small-unit tactics, but they assumed I'd be intimidated by the appearance of a second enemy when I had been prepared to fight one.
Though I was afraid, I wouldn't let my fear defeat me. I've prevailed against nastier odds using tactics designed specifically to give lone Adversaries a fighting chance against multiple opponents. The defensive techniques I learned from Maestro gave me another advantage. If I could fight him to a draw, then I can take these guys.
Raising my weapons, I stepped forward and bared my teeth. "Come on, you sons of bitches, do you want to live forever?"
Maybe they did, or perhaps they were just smart. They didn't rush me, as I thought they might. Instead, they crept forward while keeping their distance from each other. That was bad for me; I would have to show one of them my back if I wanted to attack the other. Worse, neither was within easy reach unless I lunged, which would make me even more vulnerable.
Feinting toward the man on my left, I turned and ran the other man through, my sword piercing his shielding hand before sinking into his belly. Sensing his opportunity, the first soldier leaped forward for the kill, but I was too quick for him.
Instead of slipping his knife between two vertebrae and slicing through my spinal cord, his weapon glanced off my scabbard. The soldier tried a second thrust instead of recoiling. His knife's edge caught my side as I spun to face him while ripping my sword from his companion's body.
I wasn't wearing an armored coat. Fortunately, the soldier was sufficiently off-balance from my parry that my jacket kept him from cutting me too deeply. It hurt enough to piss me off, and would probably need proper medical attention.
"That was my favorite jacket!" He backed away as I snarled at him while holding my off hand against the wound to stop the bleeding. I still had my sheath, but I wouldn't be able to parry with it now. When he finally came for me, I slashed open his forearm to the bone.
His knife fell from the loosened grip of a hand he could no longer control, and I knocked him onto his ass with a kick to the belly. Before he could recover and take up his knife with his other hand, I kicked it into the underbrush.
As his companion gurgled behind me, my enemy reached into the flesh of his mangled forearm and pulled the severed tendon until he could join it to the other end, which he held in his teeth. Once he was satisfied, he pulled his fingers from the wound, which closed before my eyes. He bared his teeth in a murderous smile as he flexed his hand.
How had he healed so swiftly without medical attention? Could the other man heal from otherwise critical wounds as quickly? If so, I was in deep shit. Backing toward the soldier still on the ground, I crouched to pick up his knife.
The man whose arm I thought I'd ruined rushed toward me too late to stop me from slicing open his buddy's throat. Let's see him get over that. I raised my bloody, stolen knife and backed away from what I hoped was now a corpse. I pointed at my fallen enemy. "You're next."
Instead of fleeing, or attacking, he crouched by his companion and began to administer first aid. I backed away, waiting for my opportunity to escape, and he glared up at me. "You'd better run, bitch. We're going to find you, and we'll make you regret not letting us kill you here."
Shaking my head, I threw the looted knife. It flew true, and the hilt sprouted from his eye. I then pushed him onto his back and seated the knife firmly into his skull before driving my sword through his heart. I did the same for the other man, who had begun to stir.
Their threat to make me regret not letting them kill me in this clearing was unforgivable, for its implications extended far beyond simple murder. That much, either of these men might accomplish on their own if they were to catch me off-guard, or use a rifle as Renfield might have done had we not met my first night in Clarion.
Regardless, he would grieve the loss of these men. In happier times, they would probably have been his friends. I dared not permit their return to base, where they would tell the others of their defeat at my hands and rouse them to seek vengeance. Not that I wouldn't be equally buggered once their buddies came looking for them and found the corpses I left. Nothing for it but to save the coordinates and bring some irregulars to give these poor bastards a semblance of a decent burial.
I still regretted the necessity as I dressed my wound and cleaned my sword. The gash on my side was a bit deeper than expected. It hurt when I took a deep breath, and I must have lost a fair amount of blood, but I doubted it would prove life-threatening if I got to a doctor. At least I had a plausible excuse to pay Dr. Petersen a visit.

## Track 27: Megadeth - "Skin O' My Teeth"

Upon my return to town, my first priority was to see Dr. Petersen about getting my side patched up. Unfortunately, Sheriff Robinson had other ideas. Worse, he came with sufficient force to compel a change of plans. Four deputies I didn't recognize followed him, which would make it five against one if we came to blows. Since I was already wounded, and worn out besides, a street brawl was the last thing I needed right now.

Though none of the men surrounding me had drawn their weapons, Robinson was the only one who didn't have a hand on the hilt of his service gladius. "Adversary Bradleigh, I need you to come down to the station and answer some questions."

Despite my reluctance to fight the law in the most literal possible sense, I was equally unwilling to meekly submit and let Robinson detain me when I needed a doctor, a shower, breakfast, and a nap—in that order. "Am I under arrest, Sheriff? If so, what is the charge?"

Robinson raised his arm to block a deputy who had stepped forward and drawn his sword halfway. He must have used secure talk to reprimand his subordinate, because the deputy's expression became sheepish as he backed up and let go of his sword. Robinson shook his head before looking at me again. "You're not under arrest yet, but there's been a murder, and I need to ask you some questions."

Oh, this was just bloody great. It could be hours before I got the care I needed if I went with Robinson. "Sheriff, unless you plan to arrest me, I must insist that you let me come to the station at noon. I was hurt this morning, and I need a doctor."

Robinson finally noticed my left hand, which I kept pressed against my side to control the bleeding. "Whose blood is that all over your hand, Adversary?"

"Mine. I was attacked by two men in the forest. One of them managed to cut me."

Wrong answer, I suppose, because the deputies drew their swords and surrounded me while Robinson began to notify me of my rights. "You have the right to remain silent. Anything you say will be treated as evidence against you. You have the right to an attorney. If you cannot afford an attorney, you will be provided one. You have the right to examine the evidence against you. You have the right to humane treatment while in custody. Do you understand your rights as outlined, Adversary?"

There was no reason for me to put up with this. Not when I could pull rank as an Adversary and summarily strip Robinson and his deputies of their authority for getting in my way. 「Some backup would be handy right about now, Malkuth.」

「We don't have any Adversaries available in either New York or Philadelphia, Naomi. Sorry. Think you can manage on your own?」

「I think I'll manage to put my boot up your arse on my own.」

Without the threat of backup arriving within minutes, I would have to back my command with force. Taking on a couple dozen swords when I was already hurt didn't strike me as a sound decision on either a tactical or a strategic level. Even if I could win by violence, I would only make enemies. Better to fold and play a stronger hand later.

"Yes, Sheriff." Unbuckling my sword belt, I peace-bound my weapon and offered it to Robinson. "I will not resist, but I must insist that you call Dr. Petersen. It would be inhumane to deprive me of needed medical care."

"I can't just call the doctor. I've got to have a deputy verify that you're injured."

Is he serious? I opened my jacket, and lifted my bloodied blouse. Robinson and some of the deputies could see some of my nipples, but I didn't care at the moment. The gauze I had taped over my wound was saturated with my blood, and red trickled down my side. "Still think I'm faking an injury, Sheriff? Get me a fucking doctor."

"Jesus wept." Robinson glared at the deputies and brandished the sword I had surrendered to him as a sign of good faith. "Put your swords away and return to your duties. I can handle it from here."

He offered his hand as they obeyed. "You can still walk to Dr. Petersen's office, right?"

I had already walked a couple of kilometers with this wound. What was another twenty meters to Petersen Family Medicine and Physical Therapy? "We're practically there, Sheriff. But it was kind of you to offer."

The bitchy nurse I met the last time I visited Dr. Petersen's office wasn't there. A young man named Thorvaldson had taken her place. "Good morning, miss. You don't look like you're here for a routine checkup."

"I wish." I fished my wallet from my pocket and showed Thorvaldson my ID. His eyes widened as he realized who I was. "I'm wounded, and need medical attention. Is Dr. Petersen available?"

Thorvaldson shook his head. "He's with another patient, Adversary, but come with me. Sheriff, please wait here."

Robinson shook his head. "Nurse, Adversary Bradleigh is in my custody. I can't let her out of my sight."

Thorvaldson stood his ground. "I'm not going to let her slip out the backdoor, Sheriff. Now sit down and shut up. I won't have you staring over my shoulder while I work."

"Fine." Robinson slumped into a chair and picked up a magazine as the nurse led me to an examination room.

Once the door closed, I removed my jacket and shirt so Thorvaldson could work. "Do you need me to lay down?"

He held out a hospital gown while looking away. "It might be easier for us both if you laid on your side."

"Fair enough." I stretched down on the padded examining table, and adjusted my gown. "I might need stitches and an antibiotic treatment."

Thorvaldson nodded as he peeled my bandage from me and dropped it into a biohazard container. "Nasty cut, just deep enough to need stitches, and it's still bleeding. When did this happen?"

"Two hours ago, I think. I kept pressure on, but couldn't do much else."

"Shit." Rather unprofessional language for a nurse to use around a patient, but my blood pressure reading was lower than it should be. "You must be pretty damn tough to still be on your feet. Do you know your blood type? You're going to need a transfusion."

Good question. This was another one of the differences between people with CPMD and people without it. Regular people fit the usual ABO blood group system.

People with CPMD also fell into four major blood types: X, Y, XY, and Z. None of these types were compatible with regular human blood types. Giving me blood from a regular person could kill me, and giving a regular person my blood was equally dangerous. "XY negative."

Thorvaldson punched this into a handheld. "Good. The local blood bank has a couple of units in stock. Now, I can stitch you up and do the transfusion, but I'm not qualified to prescribe medication. We'll need Dr. Petersen for that."

"All right."

He handed me a tablet displaying an informed consent form and a stylus. "I just need you to review this and sign at the end."

I did so, and reached for the glass of water Thorvaldson placed beside me. I sucked half of it through the waxed paper straw, and closed my eyes as the nurse injected a local anesthetic and set about stitching my side closed. His hands were swift and sure, and he whistled as he worked. I recognized the tune, an uncharacteristically melodic tune by Doomed Space Marines.

Somebody knocked on the door as Thorvaldson tied off the suture and snipped it close to the skin. It was Dr. Petersen, and he brought a folded t-shirt with him. "Sheriff Robinson told me you ruined your shirt in an altercation in the woods. I never figured you were the sort for midnight duels."

"Two against one is hardly a duel." It was probably a mistake to say that, but neither Petersen nor Thorvaldson commented it.

I waited for Thorvaldson to finish applying an ointment similar to what I had in my first aid kit. He then covered the area with a liquid bandage. It rapidly dried, resembling lacquer. Once I was sure I wouldn't ruin the bandage, I turned away from the men and slipped into the tee. Now I was a walking advert for Dr. Petersen's practice. "Thanks. Will it be safe for me to shower later today?"

"It should be. The nanocytes in the salve will dissolve the sutures within forty-eight hours. After that, they'll eat the bandage. Handy stuff, isn't it, Doc?"

Petersen nodded. "Too bad we didn't have this kind of tech before Nationfall. I might have saved more soldiers." He glanced at the chart. "Did you order two units of XY negative for the transfusion?"

"I was going to send Monica to the blood bank, but I can go myself if you want to set up the IV and start the saline drip."

"Two units should do. Once you've brought them, I can handle the rest."

Thorvaldson nodded, and shut the door behind him. Holding out my left arm, I clenched a fist so Petersen could find a vein. He nodded in approval, and swabbed me with alcohol. "Looks like you know the drill. I guess this isn't your first transfusion."

"Actually it is, but my usual physician likes to take blood and run his own tests instead of just pulling the diagnostics off my implant."

Petersen chuckled as he started the saline drip. "Oh, so, he's old-school. I can't blame him; I do that myself when the implant reports something anomalous. I think it pays to have a person confirm the diagnosis. Most of my patients appreciate the effort, and it helps keep me sharp."

"No doubt it's cheaper to use your own brain than to get an AI."

"That, too." Thorvaldson stuck his head in long enough to hand Petersen the blood units before retreating. Until Petersen hooked them up, they looked like vampire takeout. The crimson thread working its way down the line into my arm fascinated me, and for some perverse reason, I wondered whose blood this was. It didn't matter. After this mission, I'd hit a blood bank and make a donation of my own to pay it forward.

"So, what happened to you?" It was the question I expected Petersen to ask, but half-hoped he wouldn't. I wasn't sure how he'd take the news that I had cut down two of the men he once commanded. "And what did you do to your shoulder? That bandage isn't Thorvaldson's work."

Damn it. Now I had to explain that I had met Renfield before the fight. Of course, one might argue that the former led to the latter. Besides, it might rattle Petersen enough to make him reveal information he might otherwise keep to himself. If I was back home seeing my usual doctor, I'd have mentioned my leg, as well. With her, it wouldn't have mattered that I was going commando by necessity. "My shoulder? That was a love bite from one of your former subordinates. Do you remember Christopher Renfield?"

## Track 28: Baroness - "Teeth of a Cogwheel"

Dr. Petersen became utterly still at the mention of Renfield. His fixed gaze seemed to take in the entire room, as if seeking the nearest available weapon. He picked up a scalpel with a trembling hand, considered it for a moment, and put it back. "So, you seduced Sergeant Renfield."

Did he think the man was a victim of my feminine wiles? Not bloody likely, when he was straining at the seam less than thirty seconds after we first met. That wasn't even sufficient time for me to consider an approach more subtle than simply walking up to and propositioning him, let alone actually doing so.

Of course, Renfield wasn't the sort to require a subtle approach, but I'd like to think my involvement was more than that of an enthusiastic participant. "We seduced each other, Doctor. He's quite the physical specimen."

Petersen shrugged. "No doubt he thought the same of you. Not that I disagree. How much did he tell you?"

"Not that much at first. He ended up telling me enough to let me connect him and his buddies living under Fort Clarion to Project Harker." I paused for a moment to gauge Petersen's reaction, but he seemed content to listen for now. "Renfield spoke highly of you, incidentally, and told me you stood up for Dusk Patrol when the Army Medical Corps treated them like lab rats."

I leaned forward, reaching for his hand. He didn't stop me from taking it. "In fact, you've been looking out for them ever since, haven't you?"

Petersen nodded, and withdrew his hand. "I had hoped that yesterday's field trip might have sated your need to dig any deeper, let alone venture into the woods around Fort Clarion at night. I thought you wiser than this, Adversary Bradleigh. Do you have any notion of what you could expose by further investigation?"

Aside from unethical science suppressed by the Phoenix Society, presumably because its conclusions affected everybody with CPMD? There were people out there who had been betrayed and abandoned by the country they swore to defend. They remained locked in a nightmare created for a war long over. Petersen should have ended this years ago, but I gained nothing by questioning the man's ethics at this juncture. "This is bigger than a couple of unexplained disappearances. The deeper I dig, the worse it gets."

"You should stop digging, then." Petersen sighed, and gazed out the window for a couple of minutes before continuing. "Those unfortunates who vanished will be just as dead even if you manage to find an explanation for their disappearances. You're just wasting your time, and meddling with matters you don't fully understand."

Was I just paranoid, or did Petersen's words imply that if I continued to meddle, I might wind up dead? "I understand that Project Harker was an attempt to enhance the combat capabilities of soldiers with CPMD. Whatever they discovered can be applied to anybody with CPMD. Furthermore, the Phoenix Society is already well aware of Project Harker."

That thrust hit home, for Petersen stiffened as if I had stabbed him. "How?"

"Remember a civilian named Ian Malkin?"

Petersen nodded, as if not quite trusting himself to speak.

"Well, I've seen a guy who looks like him from time to time at the London chapter. Scuttlebutt says he's on the Phoenix Society's Executive Council, not that the brass will confirm or deny it. I can't prove they're the same person, but how many men with that name have you heard of?"

"Not many." Petersen swapped in the other bag of blood to continue the transfusion. "I'll admit it's a convenient coincidence. I suppose some of Renfield's men disapproved of his liaison with you."

"I didn't bother to ask my attackers why they picked a fight with me. On a related topic, care to tell me anything about the death for which Robinson arrested me? You're the coroner, aren't you?"

Petersen chuckled, and glanced at the blood pressure readout. "Starting your discovery process early, are you?"

"I didn't think to bring a book, and I doubt you'd be willing to discuss Project Harker, or your post-Nationfall involvement with Dusk Patrol any further."

"We're almost done here, Adversary. So, assuming you manage to persuade the Sheriff he has the wrong person, allow me to offer a small suggestion. The men won't bother you again as long as you and yours stay aboveground. Don't go exploring. It's dangerous."

Now that was a threat, and not just one directed at me. Was it something I could take to Robinson to deflect his suspicion? While I could have Malkuth provide an alibi using my Witness Protocol feed, Robinson would still need a better suspect. But if I directed him toward the Fort Clarion, wouldn't I just be getting him and his deputies killed?

Petersen kept eyeing me, and I doubt it was just because the t-shirt was a bit tight on me. "You're thinking about something, aren't you?"

"Just wondering what you're so desperate to hide." I gave Petersen my sweetest smile as he disconnected the IV and removed it from my arm. "You really should consider leveling with me. Too many have suffered for this secret, including you and the survivors of Dusk Patrol. How long are you going to keep carrying this burden?" I paused for effect, and to let Petersen focus on bandaging my forearm. "Do you really think you can take the truth to your grave? It's most likely that the Phoenix Society already knows everything about Project Harker. They just can't be arsed to tell me anything."

"So you're determined to figure it out on your own, and to hell with the consequences?"

"Pretty much." I refrained from shrugging as he applied pressure to my arm and covered the area with liquid bandage. "Innocents are dying around here. Everything I've seen so far, everything Renfield has told me, and every evasion I've heard out of you suggests that Fort Clarion is at the heart of what's wrong with this town."

Petersen shrugged, and began washing his hands. It probably wasn't just proper hygiene, but an act of renouncing any responsibility for what might happen next. "You won't find out much from a jail cell. But if you get out, and more people die for your curiosity, remember that I warned you."

Putting my jacket back on, I met Petersen's gaze and held it a moment. "Allow me to return the courtesy. If more people die, I'll assume you're culpable and arrest you first."

Finding Robinson still in the waiting room, leafing through a pro sport magazine with my sword across his lap, I held out my hands so he could cuff them. "I'm prepared to cooperate now, Sheriff."

Instead of binding me, he returned my sword and led me outside. "I just spoke with an AI from the Phoenix Society. He reports you reconnoitered Fort Clarion on your own last night, and furnished a map of your movements. Based on that, I don't have probable cause to arrest or detain you, but I'd still like you to come to the station."

Malkuth gave me an alibi? Damn, now I owed him an apology for threatening to kick his ass. "I will, if you tell me why you arrested me in the first place."

Robinson wouldn't look at me as he spoke, which most likely meant he was well aware of his lack of justification. Either that, or Malkuth explained my reconnaissance in detail. Hope he enjoyed the show. "The Town Council tends to just rubber-stamp Mayor Collins' budget proposals every year, and under the Society's regulations, I can't appear at council meetings in my official capacity to speak up for my department."

Robinson swept a hand, as if presenting the town. "Does this look like London or New York to you, Adversary? Hell, does it even look like Pittsburgh or Philadelphia?"

I watched as workers began putting up "Sound of Clarion" banners to advertise the upcoming annual music festival the Halfords kept talking about. "No. It's a small town, with perhaps an annual influx of visitors for the festival."

"Exactly. In a few days, everything's going to go batshit crazy for a couple of weeks. Most of the resources at my disposal are already allocated. I'm stuck, mainly because most of the fucking hicks living here think the goddamn militia is enough to keep the peace. Never mind that none of them are actually paying taxes." Robinson spat contemptuously, and met a passing woman's disapproving glare with one of his own. "Ingrates."

Things were starting to make sense again. "So, when Mayor Collins tells you to bark like a dog, you ask, ‘What breed, your honor?'"

Robinson's laughter was bitter.. "You've got it. What Mayor Collins wants, he damn well gets. So when some kid who came into town to see a band at the Lonely Mountain appears to have died of a stab wound on a night you're seen wandering into the woods, the Mayor wants you in a cell so he can tell the good people of Clarion that they don't have to be afraid."

While I could empathize with the need to prevent a panic, I could not forgive the wrongful arrest of an honest citizen to provide the local authorities a semblance of efficacy. It was especially difficult to view the situation from Mayor Collins' position when he was scapegoating me to retain the trust of his electorate.

My hand tightened around my sword's scabbard as I indulged my indignation. It would be so nice to march right into the Mayor's office and enact a bit of an impromptu regime change, but I had more pressing concerns. Besides, Collins was the devil I knew. His removal from office for malfeasance and abuse of power could wait. "The people of Clarion should be afraid, especially Mayor Collins."

Robinson glanced around, as if I had spoken too loudly and frightened the townsfolk. "Adversary or not, I can't have you publicly threatening the lives of town officials."

"Don't worry. A bit of due process won't kill your boss." Flashing a smile at the Sheriff, I turned away from Town Hall. Didn't Robinson realize that Adversaries never investigate violations of their own rights? The pins aren't a license to pursue vendettas. "I was thinking about Fort Clarion. If we don't do something about that place soon, the poor bastard you've got on a slab will only be its latest victim."

"You know something, don't you?" Robinson's expression hardened, but his tone held a note of disgust. "Why can't you just tell me?"

That was a reasonable question, but not one easily answered. Police officers and Adversaries both serve the public, but we're often at cross purposes by necessity. It's their job to uphold public order and arrest those who threaten it. It's ours to second-guess the cops, and make sure they're not ruining innocent lives. While collaboration between cops and Adversaries isn't unprecedented, it's still rare enough to be remarkable.

Beyond all that, lay a simpler truth. I didn't want to waste Robinson's time by jumping to a conclusion without more information. "I haven't seen the victim's body. Have you?"

Robinson shook his head. "No. Dr. Petersen has the town morgue under his practice, and he's the coroner. He's never fucked up an autopsy."

"He might not have buggered this one, either, but you've seen Fort Clarion yourself. Something's wrong there. We should examine the victim for ourselves before I share my suspicions with you." I pulled aside the collar of my t-shirt, and lifted the bandage to show Robinson my love bite. A few passersby stared at me, but they didn't matter. "See this? I think we'll find more on our victim."

"When did you get that?" Robinson narrowed his eyes. Hard to blame him when my bite was already halfway healed thanks to modern medicine.

"Last night from a guy who turns out to be one of Fort Clarion's more rational inhabitants." I fixed my collar and zipped up my jacket. The way Robinson stared at me was starting to creep me out. "Look, Sheriff, I've had a hell of a night. Why don't you come meet me at The Lonely Mountain when you've gotten a warrant to search Petersen's morgue? I have to request a warrant of my own, and I'm not in uniform."

Robinson nodded, and took a deep breath. "Good idea. Do I want to know how you got that bite?"

Seriously, what's wrong with some cops?! They can talk about crime all night in plain, blunt English, but immediately resort to euphemisms should the mere specter of consensual, mutually pleasurable sex come up. "All you need know is that everything leading to the bite was consensual. When I pressed him for an explanation, I learned he lives under Fort Clarion. He's not alone."

"All right." Robinson paused, and dropped the bomb. "I asked because I think know the guy. I've seen him at Petersen's late at night."

## Track 29: Motörhead - "Dead Men Tell No Tales"

A cold shower, clean clothes, and the prospect of one of The Lonely Mountain's hearty breakfasts was just what I needed. Though still tired, I'd be able to get through the day even if I had to go to the station with Sheriff Robinson after we had a look at the victim in Dr. Petersen's basement. The little black kittens sitting on the table with me, Dante and Virgil, didn't even wait until Bruce Halford set my plate in front of me before beginning their chorus of purr-begging.

"Be patient. I'll save you some bacon." And if I forget that, they could have some of the excess fat off my steak, or the remnants of my omelet. I really shouldn't encourage them, but they were so sweet together.

Unfortunately, Sheriff Robinson's arrival frightened them off. He sat in front of me with a mug of coffee he must have gotten at the bar. "Going to save me anything?"

"Are you a kitten?" He didn't look like one. His lean, hungry aspect was more vulpine than feline; Shakespeare would have thought him a good Cassius. Besides, there was no way in Tartarus I'd let him rush me through breakfast when I was still waiting for the Society's warrant. The victim wouldn't go anywhere; the dead are cool like that.

He shrugged off my question, which was fair enough. It was a bit silly. "Did you get your warrant?" Robinson showed me his before I could answer. To get one this fast, he must have some serious dirt on the judge.

Of course, I wasn't about to imply anything of the sort. "Still waiting. The process is a bit involved."

A secure talk session from Malkuth popped up, and I pressed my fingertips to my ear to show Robinson I was getting incoming comms. 「I've got a warrant for you to search the Clarion morgue under Petersen Family Medicine and Physical Therapy, and examine the body of one Scott Wilson, an eighteen-year-old male of northern European ancestry.」

True to his word, my implant alerted me to an incoming document a moment later. The authority granted was a bit narrower than I would have preferred, and no way could I stretch it to include searching the entire premises or copying the data on computers located therein. It was up to me to prove Peterson's culpability with what authority I now possessed. 「Got it. Anything else?」

「Only that I'm a bit jealous of Renfield. How many rivals for your affections have I got, anyway?」

Of course, he would have seen that. I was technically on duty until I've accomplished my mission, so everything I see and hear gets recorded. Everything. Not that I was the first to screw around on the job. I just can't believe he was so gauche as to mention it.「Malkuth, just a bit of reference for when you do get a body: never ask a woman about her previous affairs. If she's with you now, that's all that matters.」

I ended the secure talk session. "Sorry about that, Sheriff. I've got my warrant, and I'll be finished shortly."

Robinson gave my admittedly large plate a dubious look. "You barely started."

"Challenge accepted." I set about demolishing my breakfast, chowing down with the brutal efficiency I learned in ACS instead of savoring every bite like a civilized human being. The Sheriff sipped his coffee, and stared at me throughout my performance.

I finished by downing my coffee, and raising my mug skyward for a refill. Halford came by a minute later, bearing a fresh pot. He filled Robinson's mug as well. "You really were hungry, Adversary. Think you can fit anything else in?"

Robinson grumbled something about a wafer-thin mint.

He'll pay dearly for that, but for now I let it go. "Thanks, Bruce, but I'll just take the check. I'm keeping the Sheriff waiting."

Nurse Thorvaldson seemed surprised by my return. "Hello again, Adversary. Is something wrong with your wound?"

Shaking my head, I sent my warrant to his implant. "I'm here to inspect the homicide victim Dr. Petersen currently has downstairs. Sheriff Robinson is here with me, and has a warrant of his own. Is Dr. Petersen available?"

Here I was, about to serve a search warrant, but serve seemed the wrong word. If anything, the owner of property targeted by a search warrant could argue that those serving him said warrant were doing him a disservice.

Fortunately, Dr. Petersen's prompt arrival drew me out of linguistic musings, for which I wasn't properly qualified. "I understand you brought warrants."

"Yes, doctor." I sent mine to his handheld while Robinson handed his over.

Petersen scanned the Sheriff's before reading mine on his device. "Well, everything's in order. Might I ask that you put on gloves and masks before entering the morgue? Standard safety precautions."

"Of course." Robinson and I left our swords with Thorvaldson before finding smocks and putting on the required protective gear. Petersen led us down into the cold dark. A flick of a switch relieved the gloom, but did nothing about the chill air.

Dr. Petersen led us to a body bag on a gurney. A tag attached to the zipper read, *c*. Instead of opening the bag for us, he withdrew after pointing to a sleeping laptop. Was it my imagination, or did his expression seem just a touch resentful? Perhaps he wasn't used to being second-guessed. "My report is on the computer if you wish to read it. See Nurse Thorvaldson when you're finished or if you have any questions, and he'll fetch me."

Robinson watched Petersen leave before turning to me. "You ever do anything like this before?"

"Only in training. They'd bring us to the city morgue and have us examine actual murder victims. We were evaluated based on how accurately we determined the time and cause of death." I unzipped the bag and spread it open to expose Wilson's body. "This is the first time I've had to check up on an autopsy."

Robinson nodded. "Same here." He spared me a wry smile. "At least you aren't just hassling us cops."

Rather than dignify that remark with a response, I turned my attention to the deceased. My heart sank as I recognized his face, along with the marks on his neck, shoulders, wrists, and thighs. Was his death my fault? Did he die for his involvement in my mission? "He was one of the youth volunteers who came with us to Fort Clarion."

"What the fuck are These? Bite marks?" Robinson sounded as disgusted as he looked as he draped the bag back over Wilson's groin. "Jesus Christ, he's even got bite marks on his…"

"Yes, I noticed."

"Was this some kind of sex ritual gone wrong? Why would Wilson have let somebody do something like this?"

Ignoring his question, I spread open the body bag and took another look at the bite marks. "Is there a ruler around, or a tape measure?"

"What, you want to measure him?"

Now that was just nasty. "Not him, but the bite marks. I don't think they were all inflicted by the same assailant, which means Wilson might not have been a willing participant in whatever kinky scenario you think this is."

"What's your hypothesis, Adversary?" I pushed aside Wilson's genitals, exposing a stab wound incising deep into his upper thigh. It would have opened the femoral artery without being immediately visible, and bled him dry in minutes at most. "Fort Clarion is still inhabited. Renfield told me he was part of an elite all-CPMD military unit called Dusk Patrol that had taken to making their kills look like vampire attacks to confuse, frighten, and demoralize their enemies. The remnants of that unit still live under Fort Clarion."

"Dusk Patrol?" Robinson shuddered. "Fuck." He took a deep breath. "Naomi, are you sure? I haven't heard that name since before the Commonwealth fell apart."

He must have been seriously rattled to address me by name instead of rank. "I'm confident that my hypothesis is consistent with the evidence we've seen thus far, Sheriff. I fought two of them last night. That's how I got wounded, remember?"

"Yeah." Robinson gave the kid a once-over, and shook his head. "So, they weren't actually feeding off this poor kid, were they?"

Should I tell him? Or will he think I've gone round the bend, assuming he doesn't already think I'm crazy? "I'm afraid they were. The members of Dusk Patrol were subjected to a series of experiments designed to enhance them."

"You mean like Captain Commonwealth?"

Perhaps I should be ashamed of the fact that I had to look that up, but right now, my ignorance of pre-Nationfall superheroes wasn't a pressing concern. "That might have been their intended result, but with a codename like Project Harker, I doubt it."

"Christ!" Robinson's curse echoed in the enclosed space. "I was a goddamn MP at Fort Clarion, and I had no idea this shit was happening."

"Petersen knew." That stopped him, and brought his attention back to me. "He opposed Project Harker, but it continued despite him. He knows about the Dusk Patrol survivors hiding under Fort Clarion. He's protecting them."

"That must be why he only mentioned the stab wound, but not where it was located, in his report. He didn't mention any of the bites, either. Makes sense if those guys think they're vampires or are trying to make it look like a vampire kill." Robinson was just about to start stroking his chin when he realized his hand was still encased in a rubber glove that had touched a dead man. "No wonder Mayor Collins thought you were the killer. The wound was probably made with an Army-issue knife readily available at Fort Clarion, but the blade isn't much wider than that sword you always wear in public."

"The assailants would have difficulty inflicting a wound like this in battle." It wasn't impossible, though, but damned improbable. "It's not how I would go about killing somebody. I think it's more likely that they held the victim down, pinning his limbs."

"So, it was staged." Robinson stared at me a moment. "Show me your teeth."

As I did so, he found a tape measure and measured the space between my canines. He then compared it to each of the bites on Wilson's body, as I had meant to do. "Son of a bitch. Some of these look like they could have come from you, but most were made by sets of incisors set too far apart to have been yours."

The implication was obvious, despite my sleep deprivation. "Somebody's trying to frame me."

## Track 30: Delain - "Where is the Blood"

"Somebody's trying to frame you." Robinson repeated my words as he considered the corpse of Scott Wilson, one of the young irregulars who helped me inventory the ordnance stored at Fort Clarion. What the hell has he been doing out here? Had somebody been plotting against him, or was this a crime of opportunity?

At least two people had stripped him bare and fed off his blood, scoring his flesh with their canines before finishing the kill with the thrust of a knife, and I could venture a solid guess as to who had done it and why. The hard part would be convincing Robinson.

He gave Wilson's body another look. Despite his disgust, he studied the wounds with exacting care. "You have CPMD, Adversary, and you could have inflicted some of these bite wounds. If not for your alibi, I would suspect that you were trying to frame somebody. A lesser cop might accuse you of using these murders to drum up support for your investigation, but you're too confrontational to work in so roundabout a fashion."

He was right about that. I can be a stone cold bitch when given cause, but I never saw the sense in using other people when I've got a perfectly good sword with which to get my point across. "I'm not offended, Sheriff, but what makes you so sure of me?"

Reaching over the gurney, Robinson tapped one of my lapel pins. "You take too much pride in wearing these. You came to Clarion alone, and none of the acquaintances you've made in town have CPMD. Like I said, your alibi is armor-plated and it's not your style."

"You'd think I had put a sword to your throat that night, instead of simply telling you to show me a warrant or bugger off."

"I think that would have been your next step." Leaning against the wall, Robinson crossed his arms over his chest. "So give me a better theory. Who would want to frame you? Do you have something on Petersen or Collins that would give them motive to discredit you?"

"Aside from Petersen's involvement in Project Harker, and his possible clandestine involvement with the Dusk Patrol survivors? And let's not forget Collins' pathetically clumsy effort to pin Wilson's murder on me? Don't you think that's quite enough?"

Robinson shook his head. "What about means?"

"Petersen's an old man, Collins is late middle-age, and neither have CPMD." Let's give the devil his due, shall we? Though I will eventually nail Petersen, it won't be for this. "If I'm right about his involvement with the inhabitants of Fort Clarion, he might have gotten some of them to stage the attack, but he would have been smart enough to ensure that the only bite marks on the body were those that might tie me to the crime."

"You telling me the people living under Fort Clarion did this themselves? Why?"

A horrible suspicion dawned on me as I shared my hypothesis with Robinson. "Revenge. One of them threatened to ensure I regretted not letting them kill me quick and easy."

Since Wilson's body could tell me nothing more, I zipped it back into its bag. "I need to see the crime scene, and I need to meet with the person who discovered the body."

He nodded, and pressed his fingertips to his ear for a moment. "I'll take you. The Brubaker kid is still there, giving his statement."

Brubaker? What the hell was he doing out here? Had he been following me? If so, why? And how did we not meet? It wasn't like I dared run the rest of the way to Clarion after I got stabbed.

Though Robinson didn't reveal the location, I suspected I knew where he'd be taking me. And I was right. It was the clearing where I had fought those soldiers, the clearing to which I meant to bring Robinson anyway. Somebody had gotten there before the police, for the bodies were missing.

Brubaker was gone when we arrived, but the Sheriff's deputies remained. One waved an evidence baggie containing the bayonet I had kicked into the brush. "Hey, Sheriff! I think I found the murder weapon."

"You wish." I stepped forward, unwilling to be cowed. "The blood on that knife is mine, which you'll discover for yourself upon analysis."

"Did everybody hear Adversary Bradleigh? This clearing got a bit of traffic last night." Robinson raked his gaze across the area. "Colby! How many blood samples did you take?"

A deputy with a portable forensics lab strapped to her back looked up. "I sampled every bloody spot I could find, Sheriff. I found samples from at least three individuals with CPMD, based on blood type, in addition to our victim's blood. They were at least an hour older."

"Where's the rest of the blood?" The question hit me out of the blue. "There should be a hell of a lot more, given the manner in which Wilson died."

"Aw, shit. There isn't enough to tie Wilson's murder to this location. Somebody must have dumped him here." Colby started looking around, zeroing in on a small mound. "Sheriff, that patch over there has been bugging me the whole time we were here. Did anybody think to bring a shovel?"

Robinson narrowed his eyes, and crouched by the mound. He got a good grip on a patch of grass, and tried lifting it. It came free, leaving the Sheriff with a rectangle of fresh-cut sod in his hand. "Son of a bitch."

He began digging with his hands, ripping cut squares of sod loose and putting them aside. Against my better judgment, I pitched in. The other deputies joined us, including Colby, who shrugged off her forensic backpack with a sigh of relief. Together, we lifted out enough earth to create a hole a quarter meter deep in the rich, loamy ground before somebody thought to fetch a spade.

Heedless of what I was doing to my clothes, I lowered myself into the hole and set about digging out the rest of the fresh grave. My spade bumped against something slim and roughly cylindrical seated vertically in the soil. Digging around it, I exposed the handle of a knife, and then the face of the man I had killed with it.

Whoever buried my enemy had not bothered to pull the bayonet from his skull, or had lacked the strength to do so. It didn't take long for me to expose the rest of the body. Straightening, I pointed at its occupant. "That was the man I killed last night."

Robinson jumped into the grave with me and helped me clear the last of the earth. We lifted the dead soldier out so the other deputies could tag and bag him. One of them tried pulling the knife from his head before he finished zipping up the body bag, but it wouldn't budge. "Hey, Adversary, what did this guy do to piss you off so bad?"

Aside from ruining my favorite leather jacket, and threatening to do God-knows-what to me? "He underestimated me. Two knives weren't enough to beat my sword."

The deputy tried shifting the knife again. "You sure he wasn't your asshole ex or something?"

"Quite." My urge to give a flippant answer showed only in my smile. As amusing as it might be to do so, I'd probably pay a dire price for saying that I wasn't nearly as merciful with my asshole ex. Sheriffs don't select deputies for their sense of humor, and my implication that I had condemned him to live without me would most likely escape this crowd.

Once I had finished helping out, I ended up back at the police station. At least I wasn't under arrest or being detained this time around. It wasn't likely that Robinson would have brought me into his office if either were the case. He pulled a half-full bottle of bourbon and two waxed-paper cups from his desk drawer, poured three fingers into each, and handed me one. "I usually like a drink after seeing death up close. How about you?"

"It's not my normal habit, but I'll join you." I raised my cup to salute Robinson, and sipped it. The whiskey was a smooth burn down my throat that flared in my belly. "You realize that anything I tell you now will be inadmissible as evidence."

Robinson shook his head, and gave me a wry look as he put the bottle away. "It's just a drink, Adversary. I ruled you out as a suspect hours ago."

Though I had suspected as much, it was gratifying to get confirmation in as many words. "Good. I wouldn't have come here to investigate my own crimes."

"Touché." Robinson sipped his whiskey and stared out the window for a long moment. "Did you come here knowing somebody might die?"

"Of course not!"

"You sound angry about it. Why? It's not like you knew the kid."

Now I was angry, but at least I had a deserving target handy. "Damn right I am, Sheriff. Now, what are we going to do to prevent another death like his?"

Robinson finished his whiskey before answering. "We finish the job at Fort Clarion. We tear that godforsaken place out of the ground, round up every remaining member of Dusk Patrol, and get them hospitalized. If we can identify the ones who killed Scott, we put the fuckers on trial."

"I'll drink to that." And I did just that, slamming back the last of my bourbon, crushing my cup, and tossing it into the bin across the room with a flick of my wrist.

## Track 31: Megadeth — “Architecture of Aggression”

With five hundred irregulars from the town militia behind me, plus some youth volunteers who insisted on coming along because Scott Wilson had been their friend, we made short work of cataloging the equipment stored aboveground at Fort Clarion. The completion of my official mission was an anticlimax of sorts, but I was glad that nobody else died as young Wilson did four days ago.

We found plenty of automatic rifles, submachine guns, pistols, sharpshooters’ rifles, and even half a dozen anti-materiel rifles that fired such outrageously powerful rounds that using them against unarmored personnel must have constituted a war crime. In addition to the small arms, Fort Clarion also boasted two armored personnel carriers and four helicopters, all of which were armed with 12.7mm machine guns.

Nor were the barracks and officers’ housing without treasures. We found enough government-issued fiat currency to knock the bottom out of the market for Commonwealth dollars among numismatists. One officer’s house held a large cache of marijuana; one of the irregulars sniffed it out and used the stock of his rifle to smash through the drywall. Every other footlocker in the barracks held some sort of skin mag.

When not issuing orders for Sheriff Robinson to pass down to his sergeants, I busied myself by using my copy of HermitCrab to crack every computer the irregulars found on the base. I found music by long-dead bands too obscure to merit a revival and pornography depicting kinks Jacqueline never told me about. Unfortunately, the computers held little of professional interest, and bugger-all concerning Project Harker.

I did learn that one of Dr. Petersen’s trusted lieutenants had been shagging Petersen’s wife whenever he was off base for more than a day, which I would have preferred not to know. Good thing the doctor wasn’t looking over my shoulder when I made that sordid little discovery. He would have been unable to find closure by confronting his wife or her paramour; they both died during Nationfall.

After we returned to town, I wanted to make my report to the Phoenix Society immediately. The sooner they sent an arms control team, the sooner all that ordnance would cease to be a threat to anybody. Moreover, with more Adversaries at my side, we could penetrate Fort Clarion’s underground and drag whatever lay beneath into the sun.

Both Sheriff Robinson and Dr. Petersen seemed to have other ideas. They had gotten to The Lonely Mountain ahead of me since I dropped behind to guard the rear. Robinson raised a half-empty glass of beer as I walked in. “There you are, Adversary. How about joining us for a drink? Doc’s paying.”

“Just one, guys. I have to file my report with the New York chapter.” And I’ll be discreetly checking that one drink for drugs unless it comes directly from the barkeep’s hands to mine.

Sitting with them, I ignored Dr. Petersen’s scowl and waved at Dick Halford. No doubt Robinson had already had a few at the doctor’s expense, and the old man expected me to follow suit. Maybe I should order something outrageously expensive, just to justify the puss on his face.

Halford brought a glass of the house red from behind the bar. “Want me to leave the bottle?”

It was cruel of him to tempt me so. I had been on edge for days now, expecting another attack by altered soldiers traumatized by years of isolation—or the discovery of another bitten-up corpse. Neither had happened, thank all the gods, but all I wanted right now was to have a drink, make my report, take a long hot bath, and curl up in bed for an early night. “Thanks, but I’d better stick with one glass. Dr. Petersen’s paying, and I don’t want to take advantage. Besides, I still have to report in.”

Dr. Petersen seemed to relax as I set my limit. Was he really afraid I’d abuse his generosity? You’d think he’d have more pressing concerns than how much of his money I’d spend when I’m searching for evidence against him. “You worked hard the last few days. How’s your side?”

“There’s barely a scar. Nurse Thorvaldson does excellent work.” Not that a scar would have bothered me. They’re an occupational hazard and the only medals of valor any Adversary can expect. At least, that’s what my instructors claimed Edmund Cohen would say. It’s not like I can wear a bikini, in any case. “I feel fine.”

Petersen nodded. “Have you spoken with anybody about your experience? I can recommend a colleague.”

I had talked about some of it with Kaylee over drinks a couple of nights ago, and dished with Jacqueline at greater length, but I wasn’t going to tell Petersen that. It was none of his business. “I’m fine, thank you.”

Robinson changed the subject, for which I was grateful. “We still haven’t found anything we can use to close the Wilson murder. I don’t think we’re going to find a suspect or evidence with which to convict him aboveground.”

Petersen shot me an accusing glare. “How much did you tell him?”

“I answered the Sheriff’s questions as honestly as I could with the knowledge I possessed.” It would take more than an old man’s glare to intimidate me. I met his stare, and held it until he turned away. “If you aren’t willing to be honest with me, perhaps you should have a long, confidential talk with the Sheriff.”

Draining my glass before either Petersen or Robinson could react, I rose and bid them goodnight. Now that I was back in my room and alone, the last thing I wanted was to rehash the last few days with Saul. If he cared that much, why couldn’t he just ask Malkuth or one of the other Sephiroth to summarize my Witness Protocol feed?

I opened the channel anyway, after double-checking the Fort Clarion inventory and sending it to Malkuth. Saul and Iris wanted personal reports from Adversaries in the field while Director Chattan was content to get his summaries from Malkuth. I was in Rome now, and it was sensible to emulate the Romans.

I usually reported to Saul, but it was Iris Deschat who took my call tonight. She was closer to handsome than beautiful, but her face projected a mature strength tested and tempered by many trials. It was a face suited to command. “Good evening, Adversary Bradleigh.”

“Good evening, Director. I’m pleased to report that the materiel inventory for Fort Clarion is complete.”

Iris nodded. “I have a copy. Were you able to complete your other investigation?”

“Not yet. My Witness Protocol feed will show that Fort Clarion is still inhabited by the remnants of a Commonwealth Army unit called Dusk Patrol. I am confident that some of these individuals are behind the disappearances that have plagued Clarion ever since its resettlement, as well as the recent murder of Scott Wilson of Clarion. However, I cannot name individual suspects, nor can I provide sufficient evidence to convince a jury.”

Deschat narrowed her eyes in disapproval. “But you found time to seduce a Dusk Patrol survivor who might be responsible for the disappearances, and have time to drink with the Sheriff and the town doctor? Why have you not yet penetrated Fort Clarion’s underground and rooted out the necessary evidence?”

“With all due respect, Director, my coupling with Christopher Renfield yielded invaluable intelligence. Furthermore, Sheriff Robinson has been extremely helpful, and I would have had a great deal of difficulty completing my inventory of Fort Clarion’s armament without his cooperation and that of the local militia. Finally, we have not yet found a way into Fort Clarion’s underground. Once I’ve done so, I must then persuade Sheriff Robinson and Mayor Robinson to ask members of the town militia to follow me down there since the Phoenix Society has not provided me any backup.”

As I paused before making my final point, Iris opened her mouth to speak. I cut her off, and didn’t bother phrasing my concerns in a manner she might find palatable. “For fuck’s sake, you people practically go autistic whenever I bring up Project Harker. If you can give me shit about half an hour of rebound sex, but can’t be arsed to provide me with intel or backup, you have no business questioning my priorities.”

“Director Chattan told me you were a spirited young woman. I’m glad to find he wasn’t lying.” Deschat favored me with the flash of an indulgent smile. “Unfortunately, the Executive Council has bound my hands on this matter. The following comes straight from the top, Adversary Bradleigh. You are to cease your investigation of the Clarion disappearances and Project Harker immediately, and return to London for the remainder of your leave.”

So, the Executive Council was happy to take advantage of my zeal when it could be turned to a job they would otherwise have left undone for lack of available personnel, but now that they have an inventory of the equipment stored at Fort Clarion, they want me to just turn my back and let the cancer eating away at this town continue to fester? My fists clenched and began to tremble as my resolve hardened into cold steel. “With all due respect, Director, the Executive Council can sod off. I am going to get to the bottom of this with or without the Society’s help. If you don’t like it, you can bloody well send Adversaries to arrest me.”

Deschat shook her head. This time her smile seemed regretful. “If Adversaries come to arrest you, it won’t be on my order or Saul’s. I can promise that much, but nothing more.”

Was Deschat sympathetic to my cause, but unable to help without jeopardizing her own position? “Thank you, but why?”

She remained silent for a long moment before answering. “I don’t know how much Director Chattan told you about me, but I was once the captain of a Commonwealth Navy nuclear submarine, the Thomas Paine. Just before everything fell apart, we received an order to fire our missiles at New York, on our own fellow citizens.”

“Holy shit.” The words came involuntarily.

“It wasn’t an order I could follow in good conscience. Nor was it an order I could defy without the unanimous support of my crew, because once I refused that order, they’d most likely pay for my defiance alongside me. Fortunately, none of us aboard the Thomas Paine were willing to nuke our own people.”

Holding my breath, I waited for Deschat to relate how she and her crew survived the retribution her superiors must have attempted to exact for their defiance. Rather than continue her story, she studied me. “Your conscience won’t allow you to let this go, will it?”

“Innocents are suffering for this secret, Director. Your own countrymen are still at war, with nobody to release them from their nightmare. What the hell do you expect me to do about it?”

Deschat saluted. “I expect you to uphold your oath, Adversary.” She cut off the connection, but seconds later a text came through from an anonymous sender: 「S. and I will try to help, but we can’t use official channels.」

## Track 32: Guns ’n Roses — “Welcome to the Jungle”

The meaning of Iris’ text message didn’t become clear until the next morning, when I found Edmund Cohen devouring a hearty breakfast in the common room of The Lonely Mountain. A huge package wrapped like a birthday present rested on the table beside him. Dante and Virgil were also on the table, purr-begging as usual. The old soldier must have been a soft touch, because they had him feeding them scraps of steak out of his weathered hand between mouthfuls.

“Those kittens are going to get fat if people keep giving them scraps.” Sitting down with Edmund, I waved to Bruce behind the bar.

Cohen chuckled. “I guess you’ve been feeding these little buggers, too. It was either this, or have them climbing on me. Last thing I need right now is dual shoulder-mounted kitties. I’m too ticklish to deal with kitten whiskers in my ears.”

That would have been a sight to see. “Whose birthday is it?”

“Actually, Nims, that’s for you. It’s your unbirthday.”

“My what?” My mouth outpaced my brain, which had to dredge up bits of Lewis Carroll before Cohen’s explanation made the slightest bit of sense. The Alice stories had been among my favorites until I learned that their author had a thing for young girls. After that, the duology held a rather creepy undertone for me. I held up a hand and smiled as Eddie opened his mouth to explain. “It’s all right. I get it now. I just haven’t had my morning coffee yet.”

Whatever my unbirthday present was, it was bloody heavy. Wishing the damned thing had handles, I lifted the package from Eddie’s table to another so I could open it without getting in his way. “What the hell is in this thing?”

“An industrial-strength hair dryer. Saul and Iris told me you couldn’t live without it.”

Saul and Iris, eh? Giving Cohen the finger, I untied the bow and let it fall to the floor for the kittens to enjoy. My removal of the gaily colored wrapping paper revealed a second layer of sturdier brown paper, which I also ripped away.

The kittens were underfoot when I was done, playing in the shredded wrapping paper while I stared at a dull gray steel case stamped with the hiragana I recognized as the official brand of the Nakajima Armaments Company of Osaka. Cohen stood beside me as my hands hesitated on the latches. “Open it.”

The case wasn’t a simple box, but more closely resembled a giant’s bento stuffed with delightful treats. One layer contained armor. I don’t just mean the armored coat most people wear as a compromise between effective defense and aesthetic appeal, the sort I usually go with on the job so that I don’t look like I’m ready to get medieval on a suspect.

This was the real deal, the sort of armor an Adversary might wear when standing alone against a riot. The padded carbon-fiber weave covering my entire body from the neck down would protect me from teeth, claws, blades, and small-diameter bullets. The ceramic plates forming the outer layer would most likely keep everything short of an anti-tank round from getting through, though the blunt-force trauma would be a bitch.

Best of all, it was black with red accents. Not that the coloring made the armor more functional, but it showed Nakajima’s usual attention to every conceivable detail. The styling lent the armor an aura of classy menace that might allow its wearer to intimidate others and win fights before they began, or prevent them altogether. I’d think twice before drawing a weapon when facing an opponent wearing this.

I tried on one of the gauntlets, admiring the intricate network of ceramic scales protecting the back of my hand and fingers. The helmet was a high-tech affair. The visor was completely opaque, but the helmet used hidden cameras that interfaced with my implant to display my surroundings in realtime with low-light and infrared modes. Joan of Arc and Tomoe Gozen would have loved this stuff.

Not that I told Eddie that, because I wasn’t sure if he had heard of either of these warrior women. Instead, I gave him a quick hug and kissed his cheek. “Thanks, Eddie. It feels like Winter Solstice.”

Eddie shrugged, but the color rising in his face suggested my gesture affected him despite his casual manner. “No worries. I owed Saul and Iris a favor. Nakajima Kaoru owed me another.”

“Nakajima Kaoru herself prepared this gear for me? Holy shit.”

That got a laugh out of Eddie. “Wait till you see the weapons.”

“Bloody hell.” Once I cracked open the other half of the case, I understood his meaning. It contained four semiautomatic pistols, for starters. Two was reasonable. In the heat of battle a New York reload was sometimes faster than swapping magazines. Four pistols was Hong Kong gangster movie territory.

In addition to the pistols, I found a rifle. It wasn’t a standard-issue Kalashnikov, but more closely resembled the sort of carbine Sheriff Robinson might have carried as an MP in the Commonweath Army. Nakajima had equipped it with all the trimmings: an electronic scope, tactical grip, laser sight, flashlight, suppressor, and a grenade launcher. “What the hell does Nakajima expect me to do with a grenade launcher?”

Eddie shrugged. “What do you usually do with a grenade launcher? Blow shit up.”

“Ever hear of a rhetorical question?” Yes, that was just a bit bitchy. Like I said, I hadn’t had my morning coffee yet. “This is just a lot more heat than I usually pack on the job. I normally get by with my sword and a single pistol.”

“Swords?” Now Eddie wore the smile of a man suppressing laughter at another’s expense. Maybe a sword was old-fashioned compared to this pimped-out tactical carbine, but I couldn’t let all this firepower seduce me. Most of the time, goddammit, a sword is enough. “There’s a couple of those in there, too.”

“But I already have a perfectly good sword.” Despite this, I put down the rifle. There were indeed two swords among the treasures arrayed before me, the paired katana and wakizashi of a samurai. These would probably prove more effective against a Dusk Patrol soldier. If the blades were sharp enough, and I strong enough, I could lop off limbs. Had Nakajima known my enemies could shrug off a thrust from my sidesword?

Cohen watched with a concerned expression as I lifted the katana to feel its weight. Drawing the blade partway, I gazed entranced at the waves captured within the steel. “You could take somebody’s head off with that.”

“No shit, Sherlock.” I sheathed the sword and put it down. “Please tell me this equipment is only a loan. There’s no way I can keep all this gear. It’s too valuable.”

“Actually, you can, but Nakajima doesn’t expect you to. She said something about how you blush too easily.”

Though I wanted to protest, the truth was that I had blushed when Nakajima Kaoru had insisted on giving me her personal attention when I was buying my current sword, and had found a sidesword that was almost perfect except for the balance. Her offer to hand-forge a blade to suit me instead of customizing a production model hadn’t fazed me, but I had been shocked by her answer to the inevitable question of price. “No extra charge, because you’re an Adversary and I respect your cause. I think you’ll understand when I tell you I hope for a long and mutually beneficial relationship.”

Even though I understood her, I couldn’t hide my embarrassment. I had only recently taken my oath, and was using the last of my savings to buy better gear than the standard-issue Murdoch junk. I was practically a nobody, and here was the founder of a well-respected arms manufacturer treating me like I was the demon-ridden Empress of Japan and offering to forge a sword for me with her own hands. “I do, and your generosity astounds me.”

And here was Nakajima Kaoru’s astonishing generosity again. Even if she was doing it to repay a debt to Edmund Cohen, it was hard to believe I deserved this gear as a loan, let alone as a gift to keep. She did indeed respect the cause. And because I respected her, I had to return it. It wouldn’t make me invincible, but it would confer power few civilians could hope to match.

Simply having this equipment available had a profound psychological effect on me. “Thanks, Eddie. If you speak to Ms. Nakajima, please convey my thanks. I’ll return this gear personally once I’ve completed my mission.”

Eddie gave me a cockeyed grin. “Afraid it’ll go to your head?”

“I think it already has. I feel like I could march into Fort Clarion alone and round up every remaining member of Dusk Patrol. The only thing stopping me is my ignorance of the way underground.”

“Adversary Bradleigh!” Mayor Collins’ voice was especially strident this morning, and it pierced my eardrums.

Turning away from my borrowed arsenal, I faced the Mayor with my sweetest smile and a tone guaranteed to induce adult onset diabetes. “What seems to be the problem, Your Honor?”

“What the fuck were you doing last night? Playing with yourself?”

“That’s a rather intimate question, sir, so I’ll pretend for your sake it was rhetorical. To answer your first question, I reported to my superiors, listened to Charn while they performed in the common room, and then went to bed.”

“Well, I hope you enjoyed the show while another of my citizens died alone in the forest last night. Maybe your oath to uphold individual rights doesn’t matter as much as you pretend it does!”

“The Universal Declaration of Individual Rights hasn’t been revised to include the right to be a demon-ridden idiot. Considering the number of missing tourists you’ve failed to report to the Phoenix Society, I should think the people of Clarion would be well aware that the woods are dangerous for lone individuals.”

Eddie shook his head at Mayor Collins’ display. “Mr. Collins, are you sure you want to talk about accountability given your current position?”

Collins rounded on Eddie, his fists raised as if he were about to deck him. I almost hoped he’d try, if only to see how hard Eddie would kick his arse. “And who the fuck are you?”

“Edmund Cohen, Phoenix Society Executive Council. Any further questions, mate?”

Eddie’s on the Executive Council? A thousand questions sprang to mind, but Bruce Halford joined us before I could finish prioritizing them. He pointed a shotgun at Mayor Collins. “Excuse me, but why are you abusing my guests?”

Collins rounded on Bruce, heedless of the weapon aimed to blow his guts out. “Mind your own business, Halford.”

Bruce pumped the shotgun, racking a round into the chamber. “I am. Show me a warrant, buy something, or get the fuck out of my inn.”

## Track 33: Frédéric Chopin — “Etude #3 In E, Op. 10/3, Tristesse”

It wasn’t until after I had secured Nakajima’s gift and washed down a hurried breakfast with a mug of black coffee that I contacted Sheriff Robinson and got his location. Though I was tempted to put on Nakajima’s armor, I refrained. Wearing all of that when I wasn’t heading to a situation where overwhelming force was an absolute certainty would only make me look overdressed.

An Adversary’s authority didn’t come from her pins, or even from the organization backing her. It came from her willingness to use minimal force. It wasn’t tactical superiority that mattered, but moral.

Besides, if Sheriff Robinson was right about Christopher Renfield often appearing in town, I didn’t want him to see me rigged up like some kind of urban samurai. Would he report it to his comrades? It didn’t seem likely, but I wasn’t willing to risk it. Most of a week had passed since our coupling, and I had no idea how he fared against his fellow soldiers. Was he aware of the latest murder; or worse, involved?

Hopefully, he was able to get them back under control. Fighting two of them at night had been bad enough. In a situation where they need not remain unnoticed, they’d come at me with everything they had. It’s what I would do in their situation.

I was still trying to get into their heads based on what little I knew when I almost walked into a tree. Fortunately, Robinson stopped me. “Not quite awake yet, Adversary?”

“I am *now*.” No doubt my embarrassment showed. “Do you have an ID on this morning’s victim?”

“Clarence Foster, eighteen years old.” Robinson led me to the body.

It had been stripped, and subjected to the same treatment as Scott Wilson. Taking a pair of rubber gloves, I crouched for a closer look. Getting his legs spread was hard work, Foster having been dead long enough for rigor mortis to set in. “Same stab wound at the juncture of thigh and groin, Sheriff. Did anybody check under his fingernails?”

Deputy Colby shrugged beneath her forensic backpack. “No joy. You think he tried to fight back?”

“Unless he was prevented from doing so.” Robinson crouched by Foster’s body, measuring the space between furrows on each bite except the one on his penis.

I held my hand out for the tape measure, since Robinson seemed unnecessarily squeamish about touching dead men’s genitals. Colby flashed a conspiratorial grin at me while sending a text. 「Such a tragic waste. This young man had so much to offer.」

Naturally, I ignored Colby’s crude attempt at gallows humor. “Fifteen millimeters, Sheriff. I think a smaller woman than me was responsible. Or a smaller man. I doubt we have any business inferring gender on the basis of bite marks.”

“I’ll be sure to relay that to the Mayor.” Robinson opened up a body bag and shook it out, getting it ready for its occupant. He gave me a pointed look. “He still thinks you’re our best bet for a suspect.”

That didn’t merit more than a shrug from me. “And I still think he’s a schmuck.” At least Collins hadn’t resorted to rounding up every resident with CPMD. Not that I’d be the first to voice such thoughts. Somebody might mistake it for a good idea. “Given the attack I fought off, and Scott Wilson’s murder soon after, the facts suggest our most likely suspects are under Fort Clarion.”

Pointing at the body, I brought everybody’s attention back to the matter before us. “How did they manage to overpower Foster without him taking any defensive wounds, or getting one of his killers’ tissue under his fingernails?”

Robinson shrugged, but Colby seemed to honestly consider the question. “Maybe they knocked him out first. Simplest way would be a blow to the head, especially if you intend to kill the guy anyway, but I didn’t see any sign of a concussion so far.”

Her meaning was pretty obvious, so I grabbed Foster’s ankles. “OK. Let’s get him turned over.”

Taking the dead youth’s shoulders, Colby helped me roll the corpse over. Lividity had well and truly set in, and with all his blood pooling in tissue that had until now been closest to the ground, I suspected it would prove more difficult to determine why Foster hadn’t been able to resist his attackers. We’d be better off getting him to a lab, but I wanted to try ruling out the obvious explanation first.

Starting with his head, I slowly worked my fingers through his hair, feeling along his scalp for bumps or cuts indicative of a head injury. As Deputy Colby had said, the easiest way to knock somebody out if you didn’t care whether they ever regained consciousness was to hit them upside the head. It wasn’t long before I found an injury site.

Moving Foster’s hair aside, I found a strange wound. It contained several depressions arranged in a small circle, as if somebody had *stamped* this man’s head with an injurious level of force. Confused, I looked up at Robinson and Colby. “I’ve never seen a wound like this. Have either of you?”

Colby shrugged. “Some kind of stamp, maybe?”

“No. Not a stamp.” Dismay and anger warred in Robinson’s expression as he leaned in to examine the wound for himself. “This is an arrow wound.”

An arrow wound? But there’s no penetration. “Did the archer use a blunt arrow?”

Robinson nodded. “I know this mark. It’s made by a specific brand of arrowhead designed to stun or kill small game without penetration.”

That reminded me of the reason Sheriff Robinson had originally gotten on my case. Somebody burglarized his house and stole his camera and his bow-hunting gear. “Are you sure somebody isn’t trying to frame you, Sheriff? Somebody used your camera to photograph me, and now they used one of your arrows to knock this poor bastard out.”

He actually *glared* at me for a second, as if I had accused him outright. “I don’t like the idea, Adversary, but you’re right. It’s a possibility I ought to consider.”

“Especially since the theft happened before I even arrived in Clarion, let alone started poking around the forest.” That was an unsettling thought. “What if this killing isn’t about my investigation at all? Maybe the perp had already planned to whack Foster and Wilson.”

Rather than dismiss me, Robinson narrowed his eyes and began to pace. “That would leave us without a motive, Adversary. If we assume these killings are tied to Fort Clarion, we have a suspect pool. We can investigate Dr. Petersen and his associates, or go after the Dusk Patrol survivors. Take that away, and we’re stuck starting from zero.”

Now, where did I put that little violin? “You have my sympathies, Sheriff, but you saw the head injury inflicted on Charles Foster. What if a similar injury had been inflicted on Scott Wilson to prevent him from resisting his attackers? Given that you go bow-hunting with Dr. Petersen and Mayor Collins, they may well be possible suspects.”

“You’re not suggesting we check Wilson’s head, are you?” Colby stared aghast at me. “We’d have to exhume him. His parents will shit themselves.”

“What are they going to do, object on religious grounds? I’d like to see them try.”

Robinson shook his head. “Yes, goddammit. They will object on religious grounds. Hell, Naomi, I’ll be lucky if the magistrate doesn’t tell me to go fuck myself. They go to the same church.”

All right, that complicates matters somewhat. “Does it have to be a local judge signing the warrant?”

“I suppose I could go to Pittsburgh, but the Wilsons would still have the right to contest the warrant with Judge Ellsworth. We need to appeal to a higher authority.” Judging from the exceedingly meaningful look Robinson and Colby aimed at me, that meant going to the Phoenix Society.

Which in turn meant a metric shitload of work for me, since getting a warrant that would trump any objections the Wilsons were likely to raise would demand that I offer a convincing case that the exhumation of Scott Wilson serves a compelling public interest overriding his parents’ rights to the free exercise of their religious beliefs.

It took me four hours of the sort of creative thinking I haven’t had to exercise since ACS, but I wrangled an exhumation warrant from the Phoenix Society. It seems they’re happy to use me to investigate murders and the like while I’m in the area as long as I don’t mention Project Harker. That’s fine by me; I’ve already accepted that I’m on my own if I want to figure out what sort of shenanigans the Commonwealth Army got up to at the expense of its soldiers.

Unfortunately, Wilson’s parents were there when we arrived at his grave to exhume him. Sheriff Robinson glanced at the grieving parents before raising his arm to stop me. “Is there any chance you can let me talk to them?”

It took a second to check the regs. I had to present the warrant, but they didn’t say anything about letting local authorities act as an intermediary between me and the next of kin if any of them were willing to do so. “Go ahead, but I have to serve the warrant.”

Robinson and the other deputies seemed grateful as he led the way to the parents, who knelt before their son’s grave praying. He waited until they appeared to have finished the latest round. “Mr. and Mrs. Wilson?”

They rose, startled despite the Sheriff’s gentle tone. They were my parents’ age, though grief had aged them. It was Mrs. Wilson who spoke, though she looked at me first. “Is something wrong, Sheriff?”

The Sheriff took a deep breath. “I’m sorry, but the young snow-blonde with the sword is an Adversary. She’s helping me investigate Scott’s murder.”

Mrs. Wilson nodded. “We’d be happy to answer any questions you or the young lady might have.” She turned to me. “What’s your name, Adversary?”

“Naomi Bradleigh, ma’am.” I offered my hand, and she took it. “Sheriff Robinson, if you would?”

Robinson sighed. “Ma’am, we’re not here to question you or Mr. Wilson. We found a wound on another victim that we suspect may also have been inflicted on your son, a wound that wasn’t found during the autopsy.”

Mr. Wilson’s eyes flashed at the mention of Wilson’s autopsy, and he began muttering. Though I had agreed to let Sheriff Robinson do the talking, I felt duty-bound to draw the father out. “Sir, can you please speak up? Is something bothering you?”

“‘Twasn’t right, cutting open our boy after he was dead. ‘Tisn’t God’s will.”

God talk already? That was quick. Quicker than I’d like. This can’t possibly end well.

## Track 34: Mercyful Fate — “Desecration of Souls”

Was it God’s will that your son die so young, or in so cruel a manner?” Taking Mr. Wilson’s hand, I gazed into his streaming eyes. “I wouldn’t presume to know what He wants, but I promise we will handle his body with all due respect, and return him to the earth as soon as possible. Help me stop your son’s murderers before they steal more lives.”

Mrs. Wilson took her husband’s hand from mine. “Adversary, I helped wash and dress our son for the funeral. I saw what was done to him. Surely the bites were what killed him?”

Rather than explain the stab wound, I let Mrs. Wilson believe what she wanted to about the bites. “I’m sorry, but I’m curious about an injury your son may have sustained before he was finally killed. You see, the latest victim was shot with a blunt arrow to knock him out before his killers fed on him. I think the same may have been done to your boy.”

The parents shared a long look before Mrs. Wilson turned back to me. “Will you pray for my boy when you bury him again, Adversary Bradleigh?”

“I’m -” My breath caught in my throat. How the hell do you tell a grieving mother who has asked you to pray for her boy that you don’t share her faith? Sure, I’ve gone with Jacqueline to the temple when she made offerings to Athena, Hermes, and Zeus before a dangerous mission, but the priests handled the prayers on our behalf. This was different.

It was Sheriff Wilson who saved me. “We’ll pray with you, Mrs. Robinson. Adversary Bradleigh wants to see justice done, but she isn’t from around here and wouldn’t understand.”

The reek of embalming fluid assaulted us as we opened the casket, and I was grateful we were outdoors. It was hard to refrain from remarking on how odd it was for Scott Wilson’s parents to object to an autopsy and exhumation, but not to letting the local undertaker exercise their craft to stave off the natural processes of decay as long as possible. None of that for me, by the way. Just give me back to the earth and plant an apple tree over my grave.

Shaking off such morbid thoughts, I lifted Scott Wilson’s head so I could complete a thorough cranial examination. Though the thin latex gloves complicated my job, I worked by touch, carefully feeling every square centimeter of his scalp. If there was nothing there, we could just slip him back into his casket, close it up, and bury him again. Then I could withdraw and bow my head respectfully while Sheriff Robinson and his deputies prayed with the Wilsons.

No such luck. My thumb brushed against something that felt like stitches, and I looked up at Sheriff Robinson. “Found something. Can you lift him up so I can get a better look?”

Robinson nodded, and Deputy Colby helped him. Parting Wilson’s thick hair, I tried not to make a fuss when a clump came loose in my hand. Instead, I put the hair in his casket as discreetly as I could. Fortunately, somebody thought to escort the parents to a squad car before we opened the casket. If we could wrap this up and get the casket closed again before they came back, they wouldn’t need to know the undertaker had cut corners while covering up the wound.

The stitches formed a semicircle around the mark imprinted in Scott Wilson’s scalp, as if the arrow had struck hard enough to shatter the bone underneath and tear open the skin. Against my better judgment, I pressed the impact wound. The flesh yielded with a small but sickening squelch, and some noisome substance leaked from between the stitches.

Taking a step back and turning from the body, I took deep breaths until my nausea subsided. When I felt I could speak without dry-heaving, I turned to Sheriff Robinson. “He has the same blunt arrow imprint, but I think he was shot with greater force than Charles Foster. The perp might have been closer to Wilson when loosing his arrow.”

Robinson nodded, and waved to the deputies. They began the unpleasant task of documenting the wound before getting Scott Wilson back into his casket. “So, we have the same method for two murders. Nail the victim with a small game arrow to knock him out. Then strip him and inflict multiple bites, as if feeding, to simulate a vampire attack. Finally, finish the kill with a knife to the femoral at the juncture between thigh and groin.”

“We still don’t have a motive. Is this some kind of warning? Did these kids do something to piss these guys off? How are they connected?” The deputies closed the casket, and had lowered it back into the grave. It was time to bury him again, so I reached for a spade.

Robinson put his hand atop mine. “You don’t have to do this.”

“I might not be comfortable praying with the Wilsons, but it was my idea to dig him up. I owe it to them to help give him back in the earth.” Robinson lifted his hand, and I picked up my spade and joined in. Afterward, I stepped back and bowed my head as the others knelt to pray.

The sun had begun setting when the others finished their prayers. The first stars had come out, so I sped a prayer of my own skyward. Hopefully, God would remember Scott Wilson, because his life shouldn’t have ended like this.

Robinson found me after the Wilsons and his deputies left the graveyard. “I didn’t think you’d stay.”

“It would have been disrespectful to leave.” More stars were visible now, and the western sky was ablaze, but I still had work to do. “Got any ideas for what to do next?”

Robinson didn’t answer immediately. “The only bow-hunters I know are Dr. Petersen and Mayor Collins. But there’s no reason to steal my gear when they have their own. Even if we found an arrow on the scene, we couldn’t prove ownership. The brand we use is too damn common.”

“Regardless, the stitches in Wilson’s scalp suggest that Petersen was aware of the wound. An undertaker willing to do a half-assed job of covering damage like that wouldn’t bother suturing it with such precision. Yet he never mentioned it in the autopsy report. I think it’s time we questioned him.”

Robinson grew pensive in the twilit silence. “Can you do me a favor? I’d like to talk to Henrik and Brian as friends, and see if they know anything.”

It wasn’t a terrible idea. If Sheriff Robinson confronted them in his official capacity, the only thing he’d get out of either of them was a refusal to answer questions without their attorney present. They’d certainly clam up if I showed. They weren’t stupid. “So, you need me to find something else to do so my presence doesn’t tip them off?”

“Is there some way I can consent in advance to you viewing my Witness Protocol feed?”

Good question. “Let me check with Malkuth.”

「Don’t bother.」 Malkuth must be monitoring my feed. Is he bored, does he watch every Adversary in the field, or did somebody put him up to it? Saul, perhaps? 「I already spoke with Robinson. He’s pretty smart for a cop.」

「Should I tell him you said so?」

「Only if you want him getting arrogant.」 With that, he disconnected. I turned my attention back to Robinson. “Malkuth says he contacted you.”

“He did. He also said I’d better not try to fuck with you, because he had root on something called Gungnir. Ring any bells?”

A cathedral’s worth, as a matter of fact. “AIs get better at social interaction with practice, so I hope he’s joking. I guess information about Commonwealth military orbital weapons platforms was a bit above your pay grade.”

“Just a bit.” Robinson’s tone hadn’t changed, but he seemed a bit paler than usual.

Giving him a moment to chew on the consequences of backstabbing me, I checked in on Malkuth. I really can’t have him threatening people with global thermonuclear war for my sake. 「Mal, are you listening?」

「Always.」

He’d better not be serious about the ‘always’. That would just be creepy. 「Please refrain from threatening my coworkers with orbital bombardment on my behalf in the future. We’re supposed to avoid collateral damage.」

「Aww, c’mon. You never let me have any fun.」

「Consider it payback for keeping me in the dark about Project Harker.」

Since Robinson didn’t need me tagging along, I decided to do a bit of intelligence-gathering of my own. After today’s work, I needed a drink or three. Looking up Kaylee in the town directory, I texted her. 「Kaylee, want to meet at the Lonely Mountain tonight? Beer’s on me.」

I sent Michael Brubaker the same message. Maybe my new friends could offer a different angle on the situation. Since Kaylee seems to know the town, and Michael the woods, I figured I might get some answers out of them if I asked the right questions. “I’m going to emulate your example and talk to some people over drinks. I’ll catch up with you later.”

I waited until Robinson was out of sight before heading over to a row of rose bushes and drawing my sword. They had managed one last autumn bloom, which I cut and placed on Scott Wilson’s grave. With my respects paid, I too left the graveyard for happier environs.

Quickening my pace, I jogged along the path until I caught up with Robinson. “I thought I’d walk back to town with you. We should probably advise the citizens not to venture out alone after dark.”

“Already thought of that. Mayor Collins wasn’t too keen on the idea, though. Said I was stirring up paranoia.” Robinson shrugged. “I just asked him if he wanted more people to die, and put out the advisory anyway.”

“Glad to hear it.” No doubt the Mayor was miffed, but he’s just looking out for himself and let the rest of the town be damned. I was about to say so when I heard a creaking sound that felt out-of-place. “Down!”

## Track 35: Motörhead — “Shoot You in the Back”

Sheriff Robinson was too slow to heed my warning, so I tackled him. A sickening crack accompanied us as we sprawled across the graveyard path together, my body draped over his to shield him. An arrow thudded into the ground less than a meter away from his head, and rapid footsteps receded into the distance.

Lifting myself from Robinson, I helped him turn over once I realized he was in too much pain to get to his feet right away. It wasn’t long before I found out why he was hurt. “Oh, shit. Did I just break your arm?”

Robinson gritted his teeth as he used his good arm to get to his feet. “Probably my fault. Should’ve hit the deck when you yelled.” He spoke again when I crouched to retrieve the arrow we dodged. “Leave that arrow alone.”

Damn it. I was going to use it to splint the Sheriff’s arm, jury-rigging it with the material from his sleeve, but he was right. It was evidence. “All right, but we need to get your arm splinted, and then get you to the doctor.”

Robinson shook his head as an ambulance’s siren began wailing. “I’ve already called for help.” He gave a forced smile. “I appreciate it, though. Is there anything you Adversaries don’t get training for?”

“If you believe the scuttlebutt, we even get training in lovemaking. It probably helps Adversaries maintain intimate relationships.”

The ambulance arrived moments later, and a paramedic cut away the sleeve of Robinson’s jacket. He did the same for the shirt underneath before using a prepackaged kit to do a better job of splinting the Sheriff’s arm than I could have managed.

The police arrived as the EMT finished. One of them took the arrow into evidence. Another arrested me after I admitted that the Sheriff broke his arm after I tackled him to avoid one of us getting shot. Me and my big mouth.

To my surprise, the police didn’t disarm me or book me on criminal charges. They just stuck me in a cell. One of them even offered to get me dinner, but I turned him down with an innocent smile. “An attorney would be nice.”

I never got that attorney, but Sheriff Robinson showed up a couple of hours later with his arm in a sling. I didn’t bother sitting up as he opened the cell and walked in. “Want me to sign your cast, Sheriff?”

“Not that kind of cast, Adversary. Did my deputies get you dinner?”

“Deputy Rosen offered, but I declined.”

“Right. You probably thought you were under arrest.” His expression turned sheepish. “I’m sorry about that, Adversary. I wanted to make sure you weren’t out where the perp could take another shot at you, but I figured you wouldn’t listen. So I had my deputies detain you.”

Well, that explains why I wasn’t charged or disarmed. “Then I’m free to go?”

He shrugged, and pointed at the open door. “Yeah, but can you do me a favor?”

Let me guess. He wants me to stay out of the demon-ridden woods. As if I didn’t already have plans for the night. “I was supposed to meet Kaylee and Michael at The Lonely Mountain. I wanted to see about getting some info out of them.”

That was probably what he wanted to hear, because he relaxed. “Good. I was going to ask you to refrain from trying to find our assailant alone.”

What kind of stupid git does he take me for? “Not bloody likely now the trail’s cold.”

He followed me out of the cell. “No hard feelings, right, Adversary? You understood why I had you detained.”

Not that I felt particularly understanding. “You wasted valuable time, in which I might have gathered intelligence. Also, what if one of your deputies is the perp, or working with them?”

That got his attention. Riled him up some, too. “How *dare* you suggest my deputies aren’t trustworthy, Adversary?”

“Somebody tried to incapacitate one of us today. Unless Dusk Patrol had a little bird in the woods listening to us, then your deputies were the only ones other than the Wilsons who knew where we were.”

“Adversary, I trust these kids. For Christ’s sake, they all have keys to my house.”

“Keys one of them could have used to steal your hunting gear. Did you question all of them in connection with the burglary?”

“No.” Robinson wouldn’t look at me as he said it. His tone faded from indignant to sullen. “My mother has Alzheimer’s, and she goes walkabout. Unfortunately, her disease was too far advanced to be cured by the time a cure was available. I’ve got a dozen friends around town with keys so they can bring her home if they find her alone at night. Two of them are Kaylee Chambers and the Brubaker kid. You plan to add your friends to the suspect list, or just mine?”

“Thanks for telling me Kaylee and Michael have keys. I’ll be sure to check them out as well.” And I’ll start by asking them why Robinson trusts them with keys to his house. How well do they know each other? Michael seems like a solid kid, but Kaylee doesn’t fit. She strikes me as too fun-loving to be friends with an old cop. She’s probably reliable, but would he take her seriously?

Chastened, Robinson seemed to calm down, so I tried another question. “Could one your friends have lost their key, or had it stolen?”

Robinson shrugged. “Not likely. My house uses two-factor authentication. You need a physical key, and a numeric passcode.”

You could have plotted my hopes as a sine wave as they fell, rose, and fell again. Two-factor authentication would have made a lost or stolen key useless, but a numeric passcode isn’t hard to crack, especially if it only has a few digits. “Do I even want to know what the passcode is?”

“One. Two. Three. Four. Five.”

Is he serious? Nobody with half a brain would use that on their bloody luggage, let alone their home security. Then again, that might be the only numeric passcode somebody with Alzheimer’s could have a halfway decent chance of recalling. “Is your mother able to remember that?”

“No.” Robinson shook his head. “It’s more for the people I enlisted to help keep an eye on her. If they see her sitting on the porch, they can let her in.”

“So, if the keys weren’t lost or stolen by somebody who isn’t averse to trying to crack a passcode, we have twelve people who need only wait for your mother to give them the perfect pretext for gaining access.”

I was pleased with my hypothesis, since it fit the facts and gave us a limited pool of suspects we could rule out with a few simple questions. Naturally, Robinson had to screw it all up. “But nobody has had to let my mother into the house in the last couple of months.”

“Dammit.” It’s fine. This shit happens. You build up an edifice of reasonable logic, and then it falls apart when somebody mentions a new fact that knocks one of your premises out from under it. “Does anybody check on your mother during the day? Would one of your friends drop by when you’re not home? Have you had anybody come in to do repairs, like an electrician, or a plumber, or a network or appliance tech?”

Robinson shook his head. “No. Hell, Adversary, I haven’t even brought home a date.”

“All right, then. Who’s her doctor?”

He stared at me as if my brains were made of yogurt. “Dr. Petersen. Who did you think it would be?”

If he thought my last question was bonehead obvious, let’s see how he likes this one. “Petersen makes house calls for some patients. Is your mother one of them?”

“Come on, Adversary. Are you saying you think Petersen came to give my mother a checkup, and made off with my bow, arrows, and camera? You think he just walked past my mother with that stuff and out the front door?”

It did sound like a stretch when expressed as Robinson did, but it wasn’t impossible. “It depends on how sharp your mother was at the time. Also, you’ve got a back door, right?”

Robinson nodded. “You think Petersen had an accomplice?”

“It’s possible, but not necessary. Depending on where your mother was and how the house is laid out, he might have placed the stolen property just outside the back door, finished his business, walked out the front door, and snuck around back to collect the loot. All he’d need is a pretext for going elsewhere in the house. A bathroom break would do, wouldn’t it?”

“Yeah.” Robinson ground out the word as if my question had stuck closer to home than he’d like. “I’ll ask my mother if she remembers anything before I request a warrant to search Petersen’s residence and workplace. I’d invite you along, but she thinks I’m still an MP working at Fort Clarion. If you were there, she’d mistake you for my newest girlfriend.”

It wasn’t a cold gust of wind just now that made me shiver. Meeting their mothers was one of the worst aspects of dating CPMD- men. They almost always wind up bemoaning my inability to provide them grandchildren, as if that were my sole purpose in life. It’s bloody infuriating, and I wouldn’t begrudge Robinson sparing me another such experience. “I appreciate it. In the meantime, I’ll be at The Lonely Mountain. Michael should be fine, if a bit bored, but I hope Kaylee isn’t already too drunk to answer questions.”


## Track 36: Miles Davis — “Pharaoh’s Dance”

Kaylee raised a full glass in greeting as I wove through the mass of patrons who had taken advantage of the fact that tonight’s musical entertainment was a jazz combo instead of a heavy rock band by pairing off and dancing. Her beer overflowed as she waved the glass, and doused Michael, who had been nursing his own drink next to her and angrily tapping on a tablet. He muttered something that sounded a bit like, “Goddammit, Kaylee. Sit down.”

She didn’t hear him, but instead waved more enthusiastically. “Hey, Naomi! What took you so long?”

“Sheriff Robinson detained me.”

“Oh yeah?” Kaylee thumped her pint down on the table, splashing what remained. “Whafuck? This was full a second ago.”

“Yeah, and now I’m wearing it.” Michael ran his hand through his wet hair. “You can have mine. I already smell like a brewery.” He pushed his tablet across the table to me before rising. “Can you keep an eye on this, Adversary?”

“Of course. Will you be back?”

He shrugged. “I’m just ducking into the men’s room to clean up.”

Kaylee leaned toward me as Michael circumnavigated the dancers. “Have you seen the body on that kid? You really should get yourself a piece of that before you leave.”

“I can’t if I’m going to recommend him when he applies to become an Adversary. Maybe if I were recruiting for Xanadu House.”

“I bet you’ve never even been to a Xanadu House. At least, not as a customer. You’re too prim and proper.”

Me, prim and proper? Try telling that to Christopher Renfield. “You got me. I was investigating allegations of wage theft.”

“Always on the job, eh?” Kaylee tried Michael’s unwanted beer, and grimaced at the taste. “What the hell is this crap?”

“Not your usual, eh?” Though I was tempted to wake up Michael’s tablet and poke around, it wasn’t mine and I didn’t have probable cause, let alone a warrant. “Did you know Scott Wilson or Charles Foster?”

“Scott and Charlie?” She wasn’t quite slurring her words, but it was close. “Yeah, I knew those boys. They were nice boys. You would’ve liked ‘em.”

“Do I dare ask how?” Considering that Kaylee was happy to brag about Michael’s prowess, I half-expected her to count the victims among her conquests.

“Nothing sordid, I promise. They often joined my weekly Catacombs & Chimeras campaign sessions. And… I think they were lovers, but they were discreet about it. You don’t think that was why they were killed, do you?”

Though I made a note to check for anti-queer sentiment among the locals, I didn’t think it likely. Not when I saw queer couples slow-dancing and letting their hands wander with the same disregard as straight couples  . If hate-motivated violence was prevalent here, they’d probably be more circumspect. Besides, why would their sexuality have made them more likely to catch Dusk Patrol’s attention? “It would be premature to assign a motive to suspects I haven’t identified yet. Any info that you and Michael could share about Wilson and Foster might help.”

“Mike could tell you more. They were friends. Them and some other guys. Like Ernest Yoder. He’s a couple years older than the others, and lives alone on the edge of town. He’s kinda reclusive, and I don’t think anybody’s seen him since the day you arrived.”

I took a deep breath. “Yoder’s missing?”

Kaylee shrugged, and glanced around as if she wanted to be sure nobody was eavesdropping. She leaned forward until her lips all but brushed my ear. The smell of beer nauseated me, and I messaged Halford for a pot of coffee. “I doubt anybody knows for sure. Here’s the thing, Naomi. His parents died when he was ten, after which he bounced from one foster family in town to another like a hot potato until he was eighteen . Mr. Yoder was a wife-beater, and he went too far. I was with the militia, but we got there too late to stop him. He’s been pretty much on his own ever since.”

“At least you were able to save Ernest. Did his father murder Mrs. Yoder?”

Her expression had become grim as she recounted the story. “We thought he had. She looked beaten to death when we got to their barn, but when he hit Ernest, she got up. If it wasn’t so damn miraculous, you would have thought she was a zombie from the damage he had done to her.”

“What happened next?” It was still unclear what bearing this story had on Ernest Yoder’s apparent disappearance the day of my arrival in town, but now I was curious.

Kaylee tried Brubaker’s warm beer again, but it had not improved in her estimation. “We figured she’d try to shield her son with her body, but she didn’t.”

“Did she attack her husband?”

“Attack him?” Kaylee shook her head as if my words were a woefully inadequate guess at what happened next. “This ain’t no shit, Naomi. She took a ten kilo sledgehammer, screamed like some kind of samurai banshee, and pulped his fucking head with one strike.”

Oh, come on. You don’t see such feats happen in real life. Hell, it’s rare enough to see them in fiction that isn’t outright fantasy. Though I wanted to dismiss Kaylee’s anecdote, the implication that she served in the militia and her usage of the phrase *this ain’t no shit* gave me pause. When an Adversary says that, it’s tantamount to an oath sworn by the river Styx. It’s practically sacred. “One blow. She killed him with one blow, in her condition?”

“I saw it with my own eyes. It’s recorded. It fuckin’ happened. She dropped him with one swing, and then kept swinging until she dropped dead. Dr. Peterson said she might have lived if she had stayed down, but seeing Mr. Yoder hit Ernest must have triggered some kind of berserker rage. We used a hose to get what was left of him out of the floor.”

“Poor kid. I suppose he’s fucked in the head.” Which is a horrible thing for me to say, but I couldn’t stop myself.

“No shit.” Kaylee drank half the vile brew before her with a grimace.

I took the glass from Kaylee. “Tell me what happened to Ernest Yoder.”

“I didn’t let him see his mother keep pounding on the old man, but the whole town heard her screaming. I think he’s been to a hundred different psychotherapists, and none of them could help him.” She stopped as Halford came by with mugs and a fresh pot of coffee. “Thanks, Bruce.”

“You’re welcome, but don’t bogart the pot. Save some for Adversary Bradleigh.” He winked at me before returning to the bar.

Instead of continuing her story, Kaylee nursed her coffee. It wasn’t until she had finished the cup before she spoke again. “Ernest is afraid of women, which is why none of the families in town kept him for long. He’s afraid all women have the hidden capacity for violence that his mother displayed in his defense. And he’s afraid of himself, that he’s just like his father. So he lives alone, only comes out at night, and then only rarely.”

Now I get why nobody worried overmuch about Ernest Yoder. He’s the town hikikomori, the reclusive loner who spends most of his life holed up in his home. “Hasn’t anybody thought to check up on him?”

“I’ve wanted to, but I haven’t been able to persuade Sheriff Robinson that it was worthwhile to get an entry warrant. After all, he’s an adult, and he’s done this before.” Kaylee shrugged, and flashed a wistful smile. “When the last True Goddess Metempsychosis game came out, he picked up his copy after hours. I didn’t see him again for three months.”

“What about electronic communications?” Reclusive people might avoid face-to-face social interactions while engaging in a rich and varied social life mediated by the network. “Do you know if he frequents any particular network forums?”

“Other than the town bulletin board?” Kaylee shrugged. “I don’t frequent it myself. Cat’s husband runs it; he’s the town guru.”

That sounded about right, since he claimed to have contributed patches to HermitCrab. I got up after finishing my coffee. “Then I might have to consult him.”

But before I spoke with Cat’s husband, I should check up on Brubaker. Where the hell is he? Does the men’s room have a queue? Also, maybe I ought to mention Yoder to Robinson. He just might know something.

“You’re going?” Kaylee swayed as she stood, which worried me. She was probably drunker than she realized. “Oh, man. Those three shots of whiskey were definitely a mistake.”

God, she was as bad as Jacqueline. “Can you get home?”

“Uhm… maybe?”

She stumbled while taking a step, but I got to her in time to keep her from falling. “Come on, you. You’re coming upstairs with me.”

“But I’m not into girls.”

She was definitely as bad as Jacqueline. I should introduce them. They could go on pub crawls together, and I could stay home and practice my piano for once. “Neither am I. But you can have a nap while I poke around the town BBS.”

Once I got Kaylee settled, I poured myself a mug of coffee from a pot Bruce had been kind enough to send up to my room and fired up HermitCrab on my loaner laptop. Once I was on the network, I found Clarion’s town forum and browsed the topic list. Nothing in particular stood out, so I tried searching for ‘Fort Clarion’. I found a couple of threads with a fair amount of chatter, but they had been posted after the first day I led volunteers there to catalog the base’s equipment, and everybody participating used their real names.

A search on ‘Ernest Yoder’ didn’t turn up anything useful. The town forum was a virtual bathroom wall where nobody had anything good to say about anybody else, but at least the Mayor’s smear campaign against me was entertaining. Whoever did PR for Mayor Collins needs a nice fat raise for making that clown into a martyr facing crucifixion by the high-handed Phoenix Society and their sluttish agent, yours truly.

The threads buzzing with ever-more-improbable conspiracy theories concerning the current deaths and recent tourist disappearances were also amusing. Some of them suggested that the victims had been brought into Fort Clarion for use as sacrifices in some kind of Black Mass, which struck a bit too close to my own suspicions for comfort.

And I’d have to save copies of the pornographic fanfics featuring me as some kind of vampire dominatrix. Each proved more devoid of literary merit than the last, but Jacqueline and I could read them aloud for a laugh while drinking to excess.

If this forum had any useful evidence, I wouldn’t find it by using the standard interface. I opened HermitCrab’s relational database query tool, pointed it at the forum’s location, and aimed it at the standard TCP ports for database servers. It found one, and automatically set about cracking the admin password.

The connection cut out, and my laptop crashed. After restarting the machine and logging back in, an incoming mail notification popped up with a subject line consisting of two words: “Bad Kitty”. It was a message from the town’s admin, consisting of a single sentence: “Next time you want to poke around my server, get a goddamn warrant.”

So, Cat’s hubby wants me to get a warrant? He’d better be careful what he wishes for, because he might just get it. Since Kaylee’s hogging the damn bed, I might as well fill out the forms now.

## Track 37: Megadeth — “Wake Up Dead”

I had to shove Kaylee aside to get into bed last night. She was there the next morning, snoring softly. Once I had finished dressing, I tried giving Kaylee a poke to wake her.

She curled up in a fetal position, hugging her pillow close, and snarled sloppily. “Fuggoff.”

“It’s eight-thirty in the morning. I have to get moving. Shouldn’t you be opening your shop?”

Her reply was less intelligible, but I managed. Rescuing Jacqueline from the aftermaths of several pub crawls whose epic excess became the stuff of locker-room legends gave me plenty of practice in understanding Drunklish. “You fuggin’ nutsh? Ish Shundy. Fuggoff arreddy.”

“Fine, but I’d better not come back and find you dead because you choked on your own puke. You hear?” Trust me; that was the *last* thing I needed.

“Yeah yeah yeah. Fuggoff an’ lemme shleep.”

Before I fucked off, I stopped at the bar for a quick word with Halford. “Dick, Kaylee is sleeping it off in my room. Can you please check on her later?”

“Sure. Think she might want breakfast?”

Never mind Kaylee, I wanted breakfast. “No rush. Though I’d suggest bringing coffee and aspirin as well. Any chance of getting my usual?”

Dick already had a plate prepared. Talk about service. “Here you go, Adversary. Pick a table, and I’ll bring your coffee.”

I took my time eating. It was Sunday, after all, and I doubted Ernest Yoder was likely to go anywhere, whether he was still holed up at home or had in fact disappeared. Hopefully, he hadn’t suffered a similar fate to Scott Wilson or Charles Foster.

In any case, there was a thing or two I had to get my hands on before tackling that particular mystery, or confronting Matt Tricklebank, Cat’s Unix-guru husband. Opening a secure talk session with Malkuth, I sent him an IP address. 「Malkuth, I need a warrant to search the machine at this IP.」

「Can’t you just use HermitCrab to penetrate it?」

「The sysadmin is one of the HermitCrab developers. He detected my penetration attempt and clamped down.」

「You should have bought him a few drinks first to loosen him up. Maybe light some candles and put on some soft music. Gotta set the mood, you know?」

Oh, great. Now he’s joking about buggery. Where does he get this shit? Jacqueline, I suppose. She’s a pernicious influence. 「Malkuth, please at least try to pretend to take this seriously. It’s possible we have a third victim, Ernest Yoder. It is also possible that he may have been the first to die. Nobody looked into it sooner because of his reputation for reclusiveness. How the hell did these kids come to the attention of Dusk Patrol? Why would they go after somebody that rarely left his fortress of solitude?」

「And you figure he’s more sociable on the network than in person? He isn’t there. Neither are Wilson, Foster, Brubaker, or your new girlfriend.」

My new what? 「How do you know? Did you access the forum’s database?」

「Hell no. I crawled the forum’s pages and pulled the user list and everybody’s profiles. User IDs are standard first initial and last name. There aren’t any of the more fanciful handles you’d find on a board frequented by unsupervised young people. In fact, nobody under thirty uses this board.」

「Even though it’s supposed to be open to everybody living in Clarion?」

「Would you frequent the same forums as your parents if you were a kid?」

「Heck no.」If they didn’t want to use an existing public net community like Phark or 9chan, would they have gotten Cat’s husband to set up something private for them?

Network forums are normally served over hypertext transfer protocol, which is sent over port 80 by default. The official forum for ACS cadets was no different, but the sysadmin running the forum also ran a separate, unmonitored server on one of the high-numbered ports allocated for custom and ephemeral connections.

It was like one of those fight clubs mentioned in urban legends. Nobody talked about it, but if you were smart enough to run a port scan and find it on your own, you were welcome. 「Malkuth, can you run a port scan on that server and see if there’s another HTTP daemon listening on a non-standard port?」

「Found one. Want me to crawl it?」

「No. Do I have sufficient probable cause for a warrant giving me authority to examine that forum with administrative privileges along with the underlying server and filesystem?」

「No dice, Nims, but I can get you an entry warrant for Ernest Yoder’s digs.」 True to Malkuth’s word, the access code for my warrant came through. If I wanted, I could print it myself. Otherwise, I’d just give the code to the property owner and let them download it themselves. 「Anything else I can do for you?」

「I know better than to answer that. Thanks, Mal.」
As a courtesy to Sheriff Robinson, I sent the code for my warrant to his office along with a quick note. He had a right to know I meant to kick down Yoder’s door and poke around.

Most of the shops were closed, and the streets were empty; I suppose most of the townspeople were in one of the half a dozen or so churches lining Main Street. The doors opened at the closest church, indicating the end of services. The pastor was out front milling through her flock, and giving a final blessing before they returned to their secular lives. She sighted on me and pressed a booklet into my hands. “Good morning, young lady. May God bless you on this beautiful day, and guide you back to us next Sunday to join us at mass.”

Keeping my objections to myself, I smiled at the grandmotherly pastor and checked out the booklet. A church that handed out Jefferson Bibles would most likely go easy on the hellfire and brimstone. “Thank you for inviting me, Reverend. I’ll consider it.”

Since she was holding an offering plate, and the church looked like it could use a fresh coat of paint, I found a crumpled five-milligram banknote in my pocket and forked it over. The pastor seemed shocked by my generosity, and I immediately regretted the gesture.

Reaching Yoder’s home without further incident, I knocked on the door to the first-floor apartment on the assumption that the owner would have kept it for themselves while making tenants take the stairs. Am unshaved middle-aged man in stained overalls and a white t-shirt opened the door. “I don’t have any places for rent.”

“I suppose you’re the owner.”

“Yeah.” Glancing at my hands, his expression hardened. “I don’t need whatever you’re selling, but I got a message you can pass up to the Almighty.”

Thrusting the little Bible in my pocket, I decided against a hard approach. His reaction suggested I wasn’t the first to offer the cold consolation of religion. “I’m not on God’s payroll. If your problem is of an earthly nature, I may be able to help you.”

“You gonna bring my wife back?” Unshed tears glistened in his hard eyes as he spat the words at me.

His barely-restrained anger wasn’t specifically for me; I was just handy, somebody who had made the mistake of offering to listen. But there had to be a reason for his pain, and talking him through it might simplify my mission. “Do you want to tell me what happened to your wife, sir? Would it help if I listened to you? No judgment, no Bible talk. I promise.”

Thomas Wesker was sobbing by the time he had finished his story, which had a familiar ring. Three years ago he and his wife, Emily Yount, moved to Clarion. They were a pair of artistic newlyweds who had scraped together enough capital to buy property in town instead of spending the money on a fancy wedding or an extravagant honeymoon. Thomas and Emily were happy together for six months, until they set out with a basket full of food and some blankets for an afternoon of lovemaking in the woods. Thomas dozed after loving his Emily, and woke up alone.

He finished his story with his hands in mine. “There was no trace of her. The cops couldn’t even find Emily’s body. Three years later, here I am, as ignorant today as I was then. I suppose you think I’m pathetic.”

Though unused to sight of men weeping, I think Thomas had earned the right to a good cry. Something about his manner suggested he never properly grieved for his loss, and his pain had become cancerous. “Not at all, Mr. Wesker.”

His tone sharpened a bit. “Not going to tell me to man up and get over it?”

“I’m not a therapist making a housecall, but I still know better than to impugn your masculinity. It would just put you on the defensive, which is counterproductive.”

“You certainly sound like a shrink.”

“It’s the training. I’m Adversary Naomi Bradleigh, and I’ve got a warrant authorizing me to enter Ernest Yoder’s apartment to verify the occupant’s safety or disappearance. I’m not going to give you false hope by promising anything, but I will see what I can learn about Ms. Yount’s disappearance. First, I must check in on Mr. Yoder.”

Wesker nodded. “Is this one of those new-fangled digital warrants where you give me a code to download?”

Looks like he would have preferred paper. “It is. I apologize for the inconvenience. Can you provide a network address?”

“Yeah.” Once I had it, I sent Thomas the warrant ID. “Just give me a minute, Adversary. Yoder’s late paying his rent anyway.”

“Is he habitually late with his rent?”

Thomas shook his head. “No. That’s what I don’t get. He’s usually a model tenant. But he’s late with his rent, and there’s this smell coming from upstairs. I should have gone up there sooner, but Ernest isn’t quite right because of what happened with his parents.” He cocked his head and studied me a moment. “You know what happened to him?”

He was right. There was a stink of rot in the air here, but I couldn’t pinpoint its source. “Enough to realize that if he’s still alive, he isn’t going to appreciate my presence in what had previously been his one refuge from the half of the human race that scares him as much as he scares himself.”

Thomas glanced at me, a smirk curving his thin lips. “You sure you’re not a shrink?”

“Quite.” Stopping at the third-floor landing, I began taking shallow breaths through my mouth. The moist stink of corruption was stronger here than it had been in front of Wesker’s apartment. “Is this Yoder’s place?”

“Yeah.” He cycled through the keys on his ring until he found the right one. “Do you want me to go in first, ma’am? In case it’s bad in there?”

Thomas’ offer was apprehensive, but well-intentioned. His expression suggested that the present situation at least gave him something to focus on beside his own pain. For that reason alone, it was unfortunate that I had to turn him down. “I appreciate it, Mr. Wesker, but you’re a civilian. If it’s as bad as the smell suggests, you might unwittingly compromise the crime scene.”

Inserting the key, he unlocked the door and backed away. “There you go, Adversary. Good luck.”

Good luck, eh? Why did I suspect I’d need it? Steeling myself, I turned the latch.

## Track 38: Alice Cooper — “Halo of Flies”

My eyes watered as I opened the front door to Ernest Yoder’s apartment. A sudden terror sank its talons into my mind, and rationality cowered in a corner left intact as I imagined the overwhelming stench forcing its way into me. The fetid air hung thick and still, a miasma of desperation that invaded my body through every possible entrance, permeating every cell, irreversibly tainting me—as if I would never be clean again.

My stomach clenched, threatening rebellion. It took all of my self-discipline to resist running back outside to wait for Robinson. Instead, I stumbled to the south-facing bay window, my gloved hands fumbling at the latch before I succeeded in yanking it open.. I pressed my face to the screen and drank deep of the fresh air, hoping that any resulting contamination of the crime scene would prove minimal.

Breathed through a handkerchief, the smell reminded me of the forensics lectures I attended at ACS, particularly those that covered estimating times of death. Behind this bedroom door, I would most likely find a week-old corpse, the abdomen bloated from the pressure of gases building up as intestinal flora began feeding on the victim instead of his food. Taking a breath, I opened the door, and immediately slammed it shut again before doubling over and gagging.

The putrefaction of Edward Yoder’s corpse was further advanced than I had estimated. Provided favorable conditions by the heat and humidity within the enclosed bedroom, the bacteria within his body and any opportunistic little beasties in the vicinity had taken advantage of his slight obesity and flourished. His belly, strained beyond capacity, began to split open before my eyes.

The rupture reminded me of a slow-motion video of a balloon bursting when struck by a pin. Yoder’s skin split open. Liquefied flesh spattered with the force of the explosion, and the release of gases pent within added a new layer of charnel stink to what had previously permeated the flat.

Unable to rein in my gorge any longer, I fled to the bathroom. Once inside, I collapsed before the porcelain idol and offered up my breakfast. Bless me toilet, for I have sinned.

Lightheaded and desperate to avoid another assault of nausea, I took a tentative breath. Despite a stomach now as empty as the depths of space, I heaved until my throat was on fire and stars danced across the back of my eyelids. Though I was desperate to get out, my legs lacked the strength to carry me; I had vomited it all up.

Strong arms lifted me to my feet and guided me out into the hallway, where somebody had opened windows and set up fans to draw in more fresh air. Opening my eyes, I found Robinson holding me upright and offering a sweating bottle of ginger ale. “Here. Drink this.”

Straightening, I tried a cautious sip. “Thanks, Sheriff. Did Mr. Wesker call you?”

“Your buddy Malkuth did. Said something about you getting a live horror show.” He glanced at the master bedroom. People in hazmat suits waited outside, ready to open the door, and he led me to an open window before signaling his deputies. They opened the door, filed inside, and got busy. I took another gulp of fresh air. Clearly, they were made of sterner stuff than I was. “I guess we know what happened to Earnest Yoder.”

“No, we don’t.” With an effort, I forced myself to consider the burst-open remains. “I haven’t examined the body yet.”

“Nor should you. A young woman your age shouldn’t have to-”

Sheriff Robinson was trying to protect me, and right now, I wanted to let him. Except regardless of his reasons or my desires, I dared not let him insulate me from the reality in that bedroom. If I settled for secondhand information, I would reach faulty conclusions. Besides, I had already seen him burst apart from internal pressure. My day couldn’t *possibly* get worse, could it? “I shouldn’t have to *what*, Sheriff? Do my bloody job? Yoder could have been murdered in the same manner as Wilson and Foster, and for the same unknown reason. Kaylee told me they were friends. Them and Michael Brubaker, which means *he* might be in danger.”

“Shit.”

“Shit, indeed.” But, why? Had he seen something he shouldn’t have? What did these young men know that was worth killing them to keep it secret? “Sheriff, I need you to find Brubaker and take him into protective custody. I think he’s a witness.”

Naturally enough, Robinson was incredulous. “You think he saw Yoder killed?”

“I wish it was that simple.” If I was to convince him, I had to tie Brubaker to the evidence, however tenuous the connection. My intuition wouldn’t be enough. “We know Wilson and Foster were murdered in the same manner, and I think we’ll find that’s the case with Yoder. They all knew each other. Brubaker knew them. If they’re being targeted by Dusk Patrol as the bite marks on their bodies suggest, then this is connected with Fort Clarion. I think Brubaker knows why this is happening.”

“But Yoder was killed here, wasn’t he?”

“Not if the pattern holds true. We didn’t find the previous victims where they had actually been killed, or there would been more of their blood at the scene. I think he was likely killed elsewhere, and dumped back in his apartment.”

Robinson stared at me for a long moment, as if my conjecture was utterly insane. “We don’t even know how Yoder was killed, yet.”

Then why are we pissing about out here? Let’s pull our fingers out of our arses and check out the dead guy. Not that I said anything of the sort. It wouldn’t be politic. “Then tell the deputies to let me in. I know what to look for, and I’d love to be wrong. Because if I’m not, it means that these killings are unrelated to my presence in Clarion.”

“All right, but wait here a minute.” When he returned, it was with a hazmat suit sized to fit somebody my height. The symbol on the shoulder indicated it was suitable for use against biohazards, but would offer no protection against radiation or chemicals. “Let me help you into this, Adversary. With a separate O2 supply, the smell won’t get to you.”

“Thanks. Is there one for you?” For Robinson’s sake, I hoped so. I doubt he was any fonder of secondhand information than I was, and he deserved to be able to see the body for himself.

He shook his head. “This was supposed to be mine.”

I engaged the air supply and pressurized the suit. It was barely worth being called a hazmat suit, and I doubted it would protect me against to the common cold, and even that was doubtful. Regardless of my misgivings, it shielded me from the smell that had driven me to my knees in Yoder’s bathroom, which was all I needed.

Giving Robinson a thumbs-up to show I was ready, I steeled my nerves and entered Ernest Yoder’s bedroom. The ragged edges of his exploded abdomen were still a repellent sight, especially since it had burst open with sufficient force to spray organic matter in a respectably wide radius around him.

Somebody patched me into a secure relay chat, most likely Crosby. I immediately wished she hadn’t, as a deputy named O’Leary pointed at Yoder’s gut. 「I guess he ordered the extra spicy meal.」

Shaking my head, I refrained from responding, but that didn’t discourage him. Fortunately, Crosby had no such scruples. 「Cram it, kid. Let’s just do the job and get the hell out of here.」

「Sorry, boss, but you gotta wonder. You think Yoder died a virgin?」

「You’re gonna die a virgin real fuckin’ soon if you don’t plug that asshole you keep mistaking for a mouth and finish photographing the goddamned scene. Pardon my French, Adversary.」

「Don’t worry about it, Crosby.」 If my ears somehow remained virginal despite growing up with two older brothers, a few days on the job with Jacqueline for a partner would have fixed that in short order. 「Let’s see what happened to this poor sad bastard.」

The suit was a godsend, since decomposition was sufficiently advanced that flyblown flesh sloughed off the bone when I touched it. Talk about nasty. At least the flies couldn’t bother me in here.

Despite this complication, my suspicions were soon confirmed. Yoder had suffered a concussion, most likely from a blunt arrow; multiple bites from individuals with CPMD; and a stab wound between groin and thigh that finished the kill. But the lack of blood resulting from such a wound suggested he had been killed elsewhere and placed here. 「Sometimes I hate being right.」

Robinson caught that. 「Same method as the others, Adversary?」

「Unfortunately. You know what that means, don’t you?」

「Means our jobs just got more complicated. You done in there?」

「I wish. This doesn’t make sense. Yoder isn’t a small man, and he’d be dead weight, so I find it difficult to believe that not only did a single man bring him back home, but managed to do it without drawing attention to themselves.」

「I was thinking the same thing, and checked with Wesker. He’s got a security camera over the front door.」

Of course, he does.「Any useful footage?」

「No such luck.」

「Then I’ve got more work to do.」Closing my eyes, I counted down from a hundred. When I opened them again, I hoped to see something I had missed earlier. Though I’d settle for some of what I had seen before not being there when I opened my eyes again. Like that body.

Yoder’s body was still on the bed when I gave the place another onceover. There was something else, though. When he burst, the contents of his body spread across a certain distance that did not extend all the way to the window. Yet there seemed to be the tiniest, almost imperceptible drop of blood right by the wall. Pushing aside the curtains, I found a little more blood smeared on the wall. Scraped paint and splinters around the latch suggested that somebody had used a knife to undo it from outside.

My answer was just outside. 「They got in using the fire escape. 」

「All right. Let us take it from here. You want to wait at the station?」

「Have a deputy bring all of Yoder’s computer equipment. I can get started with that while you continue investigating this scene.」 With Yoder dead, I had probable cause to get into his computer and go poking through his data. Hopefully, his preference for online interaction will lead me to where Clarion’s youth hangs out on the network, and what connected these young men to Fort Clarion. Otherwise, I will have to resort to other means of persuasion.

## Track 39: Perturbator — “Perturbator’s Theme”

The Sheriff’s department was still busy when I left, but the cleaners had already arrived and stood by their van, smoking. No doubt Mr. Wesker saw to that. If  not for the circumstances of Ernest Yoder’s murder and the ongoing investigation, I daresay he would have put out a “for rent” sign already. Despite my sympathy for his losing his wife a few years ago, he was still a landlord, and landlords tend to be money-grubbing arseholes.

My stomach twinged, and I couldn’t tell if I was still nauseous from disgust, or queasy because I had lost my breakfast. I would have to eat again before I did anything else, or I wouldn’t be able to focus. By then, Robinson might be back at the station, and I could ask him about Wesker’s wife as I had promised to.

After a second shower, and a second breakfast, I walked over to the Sheriff’s office and checked with the duty officer. “Hi. I’m here to check out Ernest Yoder’s computer.”

“It’s not here. Sheriff Robinson said you should head over to Town Hall. He’ll tell you more there.”

That’s odd. Was the computer at Town Hall? Since it was accessible to civilians, it didn’t make sense to keep evidence there. Hoping Robinson would have a reasonable explanation, I got directions from Cat and headed down into the basement. “So, Sheriff, where’s Yoder’s computer? The duty officer told me to see you.”

Robinson nodded. “I figured that since we had photos of the original crime scene, we could just pull everything out of that bedroom and use a room down here to recreate it.”

“Show me.”

Robinson complied, and held the door for me as I stepped into a brightly lit basement room with all of Yoder’s furniture arranged as it had been in the apartment. The bed, fortunately, had been stripped and the mattress removed. Before doing anything else, I used my implant to photograph the room and sent the images to Malkuth with a note explaining what Sheriff Robinson had done and asking him to run his own comparison with the original.

The nightstand drawer held nothing of direct relevance to my investigation, though the contents shed light on the solitary existence of Ernest Yoder. The container of skin cream for men was a high-end brand full of exotic ingredients, and not available in shops. Despite the shelves crammed with non-fiction and complex novels, his taste in magazines suggested he didn’t read them for the articles. I found a lone issue of *Tomcat* beneath the well-worn issues of girlie mags like *Madonna to Whore* and *Harsh Mistress*.

The cover was a familiar one. It was the same pre-Nationfall issue I had found in the Fort Clarion post exchange, with the blue-eyed snow-blonde who might well be an ancestor of mine. How had Ernest Yoder gotten this, and when? It must have been a recent acquisition, judging from the lack of difficulty I experienced in flipping through the contents. “Sheriff, I need an evidence baggie and a marker.”

“Find something?”

“A girlie mag that I suspect Yoder took from Fort Clarion.”

“Going to show me?”

Not bloody likely, though courtesy demanded the Society share evidence with local authorities since we were both investigating the same crime. Still, there was a protocol for that sort of thing. “File a discovery request, Sheriff.”

With the porno mag sorted, Yoder’s bedroom beckoned. It didn’t look like a standard machine that offers just enough local capacity to connect to an AI megaframe and store a user’s current work should the network connection fail. Instead, this was a full-featured personal machine, and most likely custom-built. A cursory look in one of his desk’s drawers showed he had the tools for the job. The damned thing was water-cooled. Neon lights inside the transparent case flared to life as I plugged in HermitCrab and fired it up.

Glancing at the attached graphic tablet and stylus, I added this to my mental picture of Yoder while waiting for the machine to boot and for HermitCrab to read the computer’s internal storage. He was a loner with an active, albeit solitary, sexual life—and possibly a digital artist as well.

Regardless, I hoped that Yoder wasn’t all that savvy about security. If he’d been as careless as most young people, he would probably have left a trail of digital evidence even the most incompetent amateur sleuth might have followed. Since I was a professional, and fairly competent, I anticipated little difficulty. All I needed was a starting point.

First, I tried accessing the ‘secret’ forum Malkuth found, whose software listened on transmission control protocol port 65535 instead of the standard TCP port for hypertext transfer protocol. Yoder had an account there, as gynophobic\_hikikomori — one who fears women and has withdrawn from society. The name was so apt, I suspected he chose it himself to throw in the faces of those who might mock him for his psychological issues.

A sneaking suspicion grew in the back of my mind as I explored the forum. The people posting here seemed to have been putting on an elaborate charade or chronicling a rich collective fantasy life and so competed to post the most lurid descriptions of the illicit activities in which their parents feared they might engage. While many of Clarion’s youth probably did sneak into the woods to drink moonshine and smoke weed, I doubted they all did so. Nor did I believe they’d all gather for a moonlit orgy where they engaged in acts and configurations I suspected even Jacqueline had never heard of, let alone tried.

I did nothing of the sort, but I maintained a practice journal that suggested I was my instructors’ idea of a diligent musician. Instead, I developed my vocal and piano technique with forty-five minutes each of deliberate practice per day. With six hours a day at ACS, one of which was devoted to physical training, I had enough time for self-care on Monday through Friday—and I bloody well took the weekends off.

As far as my instructors at Juilliard were concerned, all those long hours of devoted practice paid off with rapid growth. If those schmucks ever found out, it was after I had gotten my degrees and an offer to join the Metropolitan Opera of New York once my time of service as an Adversary was complete. What was that about cheaters never winning?

With the ‘secret’ forum a bust, I tried a system-wide text search for other instances where he used the name ‘gynophobic\_hikikomori’, and found it associated with every account that didn’t support authentication via Secure Shell. It was even his primary login on this machine.

I suppose the poor bastard’s issues were central to his identity as he understood it. Were he still alive, I daresay many a Phoenix Society psychologist would have found him an utterly fascinating case. His choice of passwords wasn’t nearly as interesting, however. He used the same password everywhere, ‘4evr@l0n3’, and if that meant ‘forever alone’, it was no doubt another consequence of too much time spent pitying himself with his dick in hand.

Yoder even used this username/password combination with his credit union, which allowed me an intimate look at his finances. He was indeed a graphic designer and artist, and his balance suggested that not only was he good enough do it for a living, he was good enough to earn an a better living than I did if you compared his monthly income with my Adversary’s salary. Not that I begrudged him; with his problems it would have been all too easy to end up in poverty once whatever assets he inherited ran out.

The sites Yoder accessed as gynophobic\_hikikomori were similar to the hoax forum in that they offered no real insight into his character other than that he wanted to overcome his fear of women, but was afraid that if he did and brought female companionship into his life, he’d only turn out to be as abusive as his father. It was the terror of proving his father’s son that kept him locked away, only to come out at night.

Out of curiosity, I tried his secure shell login. It demanded a user name and password, so I tried what Yoder used everywhere else. It worked, which for some reason didn’t surprise me all that much. Once I was in, the rest of his network history was laid bare. One location stood out in its access frequency, an Internet Relay Chat site on the same IP address as both the official town forum and the ‘secret’ bulletin board.

It made sense. A sufficiently paranoid system administrator could disable server-side logging, making anything said on an IRC channel ephemeral unless the users enabled logging on their end. If the youth of Clarion were paranoid enough to maintain a decoy forum and use IRC via SSH, I doubted they logged anything.

My reception as I logged in was immediate, and enthusiastic. Nice to know Yoder had some friends.

> RangerMike: Hey, GH! Where you been, man?  

> Doctor\_Feelgood: Yo, GH, you got a woman over there? That what’s been keeping you busy?  

> D3M0N01D: GH, that Cecilia Harvey poster you did looks great. Too bad she isn’t that hot in the official art.  

> Doctor\_Feelgood: Yo, Demonoid, did you see that snow-blonde Adversary around? Dead ringer for Cecilia, bro. Maybe that’s who GH is shacked up with. Lucky bastard.  

> RangerMike: Guys, I know the lady. GH is a good guy, but he’d have a stroke if he met Adversary Bradleigh. Let’s leave her out of this.  

Sweet of Brubaker to stick up for me. Too bad I can’t thank him without blowing my cover.

> Godfather: RangerMike is right. For all we know, she’s spying on us. I caught her trying to crack the public forum database last night using that clunker my wife asked me to lend her.  

> Doctor\_Feelgood: You lent her a machine? Dude, you’re fucking whipped.  

> [Doctor\_Feelgood: kicked from channel #clarion\_underground](#)  

> [Doctor\_Feelgood: banned from channel #clarion\_underground for 24 hours](#)  

Wow, Cat’s husband doesn’t take any shit. He’d probably come after me with some kind of blunt instrument if he knew I was lurking in the #clarion\_underground channel using a dead man’s handle.

> RangerMike: So, GH, WTF happened, bro? MrSnotty and Clusterfuck are dead, and we haven’t heard from you since you showed us how to get into the basement  a couple weeks ago.  

Jackpot! Man, I could kiss Mike Brubaker right now, but that would probably embarrass the poor kid. But how should I tell him that I know what happened now? If I send him a private message, he’ll know I’m not Ernest Yoder unless I’m extremely careful and extremely lucky. Too bad I didn’t take time to analyze his style so I could imitate it. The slightest variation from familiar usage or grammar might give me away.

> RangerMike: Adversary Bradleigh isn’t stupid, guys. We should probably come clean before she figures it out on her own, or before more of us get whacked.  

> Godfather: You kids were idiots for going in there in the first place. RangerMike, you know the lady. If you don’t talk to her, I will. Unless you’re already lurking, Naomi. Come out and pay your respects.  

Made again? Who is this guy, and why is he pissing about in an overgrown village like Clarion when he could get tech companies in New York and London embroiled in a bidding war for his expertise? Has he no ambition?

Staring at the screen in frustration, I pounded the desk. Some hand-painted wargaming miniatures jumped at my blow, but that was all I managed to accomplish. Logging out, I immediately returned to the IRC server hosting the #clarion\_underground channel under a different name: CeciliaHarvey. It’s silly, but showing these kids I can take a joke might help them open up.

> CeciliaHarvey: Fun time’s over, lads. I want everybody who has been inside Fort Clarion to meet me at The Lonely Mountain in one hour. Pack a bag. GH was murdered by the same people who did MrSnotty and Clusterfuck, and it would be lovely if I could stop these arseholes before they kill more of you. Godfather, I trust you’ll pass the word along to Doctor\_Feelgood.  

## Track 40: Perturbator — “She is Young, She is Beautiful, She is Next”

The #clarion\_underground channel erupted in virtual tumult at my order, and I had neither the time nor the inclination to deal with the shit storm. Nor was I about to justify myself to these kids. Instead, I pulled still images from my feed, one for each of the victims thus far, and posted them with a simple message: “Do as I say if you want to live.”

Rather than stick around for the reaction, I disconnected and shut down Yoder’s machine. My pace was swift as I left his simulated home and began my walk back to The Lonely Mountain. On the way, I used my implant to evaluate transportation and lodging options. I needed those kids away from here and in a safe location. If that location remained secret, so much the better, though invoking the Phoenix Society’s aegis would probably serve to deter any notion of betrayal on the part of those I must perforce trust to carry these kids off to safety.

「Malkuth, I need evac for at least four witnesses, and a safe house, in which to keep them. What can you do for me?」

「You figure the killers are limited to the vicinity?」

「Those kids are dead if I’m wrong about that.」 How far away was far enough? Pittsburgh was definitely too close, but was New York far enough to be safe? London would be better, but could I justify it? 「Yoder, Wilson, and Foster had all been under Fort Clarion, but other kids have been down there, too.」

「I just dispatched a bus from Pittsburgh to pick up your witnesses at The Lonely Mountain and transport them to the New York Chapter. We can put them up at the hotel across the street, and detail some senior ACS cadets to stand guard. It isn’t exactly discreet, but the alternative was a helicopter that wouldn’t be available until tomorrow」

「The bus is armored, right?」 I’d love to see the look on Petersen’s face as the chopper lifted off, taking my witnesses away. However, Malkuth’s right. A bus would do just as well as long as Dusk Patrol doesn’t waylay it.

「Come on, Nims. Give me a little credit. I even arranged a two fireteam escort with the Fallen Angels MC. We’re gonna whisk those kids away in style like badass rock stars trying to avoid paparazzi.」

Knowing that Malkuth had hired Fallen Angels to escort the bus helped me breathe a little easier. The bikers were reliable mercenaries, and I wouldn’t be the first Adversary to take advantage of their services. They were bloody expensive, however, which was why I didn’t consider hiring them to tear apart Fort Clarion. Furthermore, bringing a few dozen Angels to Clarion would do little to endear me to the locals, or to local authorities. Even ten Fallen Angels seemed a bit excessive. If the Phoenix Society sent as many Adversaries on the same mission, they’d probably be dismantling an interplanetary corporation.「Thanks, Malkuth. I suppose this is coming out of my salary.」

「Let’s just say you’re going to have a bit of explaining to do next time you report your expenses.」

Shit. Facing an auditor over this would be no less an ordeal than this job has been, but I couldn’t afford to dwell on it now. Got some lives to save. More than I expected, it turns out. Four young men and three young women awaited my arrival at The Lonely Mountain. They sat at one of the biggest tables in the common room, their bags piled up in the corner behind them.

After double-checking the bags, I cleared my throat to get their attention. “We seem to be a bag short. Who mistook this for a day trip?”

Brubaker looked up from cleaning his shotgun. “I’m not going anywhere, Adversary.” His explanation was evidently for my eyes only, since it came via secure talk. 「You need somebody who knows the woods. And I can watch your back.」

One of the girls began to pout. “If Mike gets to stay, why should the rest of us go?”

Tempting as it was, telling the girl she had to go because I bloody well said so didn’t seem likely to persuade any of the youths sitting before me. Instead, I sat down with them. “Who saw the photos I posted to IRC? Raise your hands.”

The girl who complained kept her hand down. Likewise for the brunette sitting beside her. “Adversary, I didn’t see the photos. David got an email from Mr. Tricklebank and told me we had to leave.”

That left the complainer. She narrowed her eyes at me. “Fine. I saw the pics, but they don’t explain anything. Why should we be inconvenienced because of a few dead people?”

Brubaker shook his head. “Kelly, stop acting like a bitch. We don’t have time for your shit right now. You were under Fort Clarion with the rest of us. For fuck’s sake, Scott was your cousin. Do you want the same thing to happen to one of us? Adversary Bradleigh probably thinks we’re next.”

“But why would they kill any of us? We didn’t do anything wrong. The place was abandoned.”

“It isn’t.” That got everybody’s attention, even Kelly’s. “Fort Clarion was never abandoned. Some of the people stationed there are still alive, and haven’t forgotten their duty.”

“But wouldn’t they be really old?” One of the other youths had a dubious look on his face.

“Do you want to see the photos again? I fought two of them a few nights ago. They are most certainly not old, and had no need to fight at a distance. Were I a bit slower, I might have been gutted.”

I had their attention now. “These aren’t ordinary soldiers. They’ve been changed as a result of a pre-Nationall experimental program called Project Harker. Its intended result was to turn people with CPMD into vampires. The subjects were hardened soldiers before the Commonwealth Army’s scientists got at them, and are all the deadlier now.”

One of the young men started at my use of the name Harker, but Kelly gave a disgusted snort before I could question him. “Vampire soldiers? Under Fort Clarion? Do you have any idea how ridiculous you sound, Adversary? And we thought Yoder was fucked in the head.”

“Is it really so ridiculous, Kelly?” The brunette next to her spoke up. “David, isn’t there a guy who has a monthly appointment to stop at your parents’ grocery store after midnight?”

The young man who had recognized the name earlier nodded. “I wanted to say something before, Jill, but didn’t want to interrupt.” He turned toward me. “Adversary, there’s a standing order at my parents’ shop that has been active since before they purchased the store. I’m pretty sure the name on the order is Harker. A man always comes to the shop on the first of the month after midnight to pick it up. He wears an old army uniform, and I think I saw the name Renfield on it. Somebody named Petersen always picks up the tab. I think it’s the doc, but I’m not sure.”

So, that’s why Fort Clarion’s pantry had fresh groceries. That son of a bitch Renfield is making monthly midnight shopping trips. But why would he make them midnight if he can get around in the day or dusk? Does he come alone, or brings men with him? And how does he cart his groceries back to the bloody base? It isn’t exactly a stroll. “David, this is important. Did Renfield ever show up with anybody else?”

David’s eyes narrowed, and he looked past my shoulder. Turning around, I caught a glimpse of Sheriff Robinson shouldering his way through the patrons. A few of them objected, but Robinson calmed them down by flashing his badge.

He seemed neither surprised nor pleased to find me here. “I suppose I have you to thank for the panicked parents screaming at me because their kids packed a bag and bugged out without a word of explanation. Not to mention the fucking Mayor up my ass. Care to tell me what’s going on, Adversary?”

He’s got the Mayor up his ass? Poor baby. “I know why Yoder, Wilson, and Foster were murdered. A couple of weeks ago, they got into Fort Clarion and poked around underground. These seven were with them. Thinking of their safety first, I arranged for the Phoenix Society to place them in protective custody.”

“Where?”

Why would Robinson care about that? Shouldn’t he be grateful that the Society is looking out for these kids? “My superiors didn’t tell me that.”

“Figures.” Robinson’s chuckle held a bitter note. “OPSEC, need-to-know, and all that spook shit.”

“Which doesn’t make your job easier, does it? You’ve still got all those scared parents. What will you tell them?”

Robinson shrugged. “Not my problem any longer. I told ‘em to take it up with the Phoenix Society.”

Thanks for nothing, but I suppose it was the sensible thing for him to do. It’s not like having the kids spirited away was his idea. The revving of motorcycles outside kept me from telling Robinson I understood his passing the buck. “I think that’s our ride.”

Three Fallen Angels walked in, and that was not a joke. They more closely resembled soldiers than bikers; their jeans and leather had the neatness of uniforms, and their postures and habit of scanning the common room as they approached suggested rigorous training. The one in the middle even had stripes similar to those identifying Renfield sewn into the sleeve of his jacket, and he saluted with his fist over his chest like he was one of ours. “Adversary Bradleigh? I’m Sergeant Jackson from the Fallen Angels. Mind if I transmit the ID for my orders?”

As I returned his salute, I found his IP address and opened a secure talk session. “Ready.”

A long string of random text came through, and I passed it to Malkuth. He confirmed its authenticity and relayed to me the orders passed to the Fallen Angels: take my witnesses into custody and escort them to a secure location. The location was redacted, naturally. I didn’t need that information, and the Society didn’t need me blabbing if captured. “Thank you, Sergeant Jackson. You’ll be escorting six tonight.”

Turning to the kids, I introduced Jackson as Mike Brubaker left them and joined me. “Sergeant Jackson and his squad will escort you to a secure location away from Clarion, where the Phoenix Society will keep you in protective custody until I’ve resolved the situation here. Follow his instructions, please.”

“Thanks, Adversary Bradleigh. If you folks will just grab your bags and follow me, we’ll get you situated. You’ll be traveling in style, but don’t count on the minibar being stocked.” Sergeant Jackson led the motley crew out of the Lonely Mountain, ignoring the boys’s disappointed groans. I guess they were looking forward to free liquor.

Robinson was still there, watching them leave. “They weren’t the only reason I came looking for you. There’s been another murder.”

## Chapter 41: Makeup and Vanity Set — “Search the Night”

Another murder?” Damn it, who had I missed? What was the chance of this person being attacked now that I had seven of the kids that had ventured into the depths of Fort Clarion with me for the last hour? Had I not warned everybody? Was there another who ignored my warning, and had paid for doing so with their life? Or was this something different, and worse? “Who was it? Was it the same method?”

Robinson shook his head. “I think you should see this in person.”

Something about his tone told me it was going to be bad. “Better lead the way, then.”

Without any solid information to chew on, it was tempting to speculate on who had been murdered, when, how, and why. Such temptations were best resisted, lest I arrive at the scene prejudiced. Even suspecting that this murder had anything in common with the others was a mistake, as now I’d have to guard against the impulse to disregard evidence that doesn’t support my theory.

Trying to clear my mind so I could view this kill with fresh eyes, I followed Robinson down Main Street. He led me to a shop called Gibson Hacker Supply, right across the street from Kaylee’s Shiny Hobbies. We found Cat from City Hall sitting inside amid racks of a near-infinite variety of electronic components and tools. She blindly leafed through a book, which promised to teach the reader how to build their own Enigma machine.

Before I could approach the shell-shocked receptionist, Robinson stopped me with a hand on my shoulder. “This way.”

He led me behind the counter, into the back room. It had been ransacked, with solid-state drives scattered hither and yon as if somebody had been searching for a particular drive. In one corner, a small mainframe hummed, heedless of the violence and death that had visited the room. In another, a corpse slumped against the wall with his legs splayed before him, his head lolling over one shoulder.

In life, he had been a short, stout, long-haired bear of a man. In death, he was battered, his arms and legs bent at profoundly wrong angles. Somebody had driven a heavy-duty screwdriver into his chest. One of his temples was dented by what surely had been a mortal blow. He still clutched a crowbar in his left hand, its end bloodied from the blows he struck against his assailants.

It hadn’t been enough. There must have been more than one intruder, and they had overwhelmed him. One of them had sliced his throat open and used his blood to leave a message on the wall. “WE MISS YOU, NAOMI. COME BACK, NAOMI. STAY WITH US FOREVER, NAOMI.”

My heart kicked into overdrive, and my vision narrowed. My voice was a snarl through chattering teeth. “Whoever did this knows my name. This is a direct challenge.”

Robinson nodded. “That’s what I thought. Mind putting your sword away?”

“What?” My training must have taken over, for I had no idea I had it drawn. One of the things we learn as Adversary candidates is to meet fear on the battlefield with anger. While it’s true that anger leads to hate and hate leads to suffering, fear can paralyze you. Worse, it sends you fleeing when your companions need you to stand firm. Rage, on the other hand, gives you strength and courage with which to fight, survive, and prevail.

Breathing deep despite the stink of blood and pain, I sheathed my blade. Even if it didn’t make Robinson nervous, I can’t afford to fence with shadows right now. “Did you get an ID on the victim?”

Robinson nodded. “Matt Tricklebank. Cat’s husband. Everybody in Clarion uses that mainframe of his. We just rent capacity on Tetragrammaton.”

Oh, shit. Though I suspected the victim had been him from the state Cat was in, I hoped to be wrong. Between being a major HermitCrab contributor and running that mainframe, it’s little wonder he called himself the Godfather. He had a hell of a lot of information and power at his fingertips. What was his part in this mess, aside from giving Clarion’s youth a virtual speakeasy? Had he also been to Fort Clarion? “Tetragrammaton?”

Robinson shrugged and gave me a sheepish look. “That’s what he called that mainframe. Said it was a Unix thing.”

Fortunately, I could sit in front of the console without disturbing the crime scene. The keys responded to my fingers with a meaty click as I woke the screen for a shell prompt.

> GENERAL ATOMIC MODEL GA-65536  
> MULTICS VERSION 20481031.23.17  
> UPTIME 10 YEARS, 320 DAYS, 20 HOURS, 15 MINUTES, AND 33.3 SECONDS

> tetragrammaton login:

A quick network search suggested that not only was this most likely the last working Multics installation on the planet, but that this installation ran on a machine last produced prior to Nationfall. Had Tricklebank found this while settling in Clarion? Or was this a relic of the Commonwealth Army’s presence in town? In the chaos of this room, it sat untouched, which suggests it wasn’t why his assailants had come. “Ten years of uptime on a computer this old? Tricklebank must have been some kind of wizard.”

“Sounds about right. We resettled the town about twelve years ago. Tricklebank and his wife found that machine when they bought this building and set up shop.”

“Speaking of which, has anybody gotten a statement from Cat?”

Robinson shrugged. “Have you had a good look at her? Figured she was too traumatized to tell us anything useful.”

“I’ll try talking to her.” Might as well, since there wasn’t much I could do with Tetragrammaton right now. Even if it had the right port for my device, I couldn’t count on HermitCrab being able to talk to it. If it couldn’t, I’d need an account and a manual. “Cat?”

She stared up at me, eyes narrow with grief and hatred. “Why did you have to come here?”

Rather than look down on her, I knelt before her and took her hands in mine. “People were disappearing, and nobody else cared to intervene. I’m sorry about your loss, and I would have protected your husband if I had been aware of his peril. There are six young people who are going to live because I got them out of here. You and Mr. Tricklebank could have been among them.”

“Why would you have us leave here?” Without anger to lend her voice texture, Cat spoke in a flat monotone.

“Do you know Ernest Yoder? He was murdered a couple of weeks ago. Scott Wilson and Charles Foster died the same way. Whatever’s happening here started before I came. If you know anything, please tell me.”

Cat looked around, searching for the Sheriff or his deputies. “Not here.”

She glanced around the shop again before producing a key and opening a door to a staircase leading down into the cellar. She descended without turning on the light, and I followed. Absolute darkness enveloped us once I closed the door behind me, and my implant flashed a “network connection lost” message at me.

Unsure if the cellar was also soundproof, I whispered. “Your husband built a Faraday cage in the basement?”

“No. It was here when we bought the building. It was prewar construction, and Matt left it in place in case he needed to work in a secure location.” Cat flicked a switch, and a soft red glow pushed back some of the gloom. She turned on a display, and the shop above us came into focus via closed-circuit television. “Nobody can hear us down here.”

“How do you know?” The sudden flush in Cat’s cheeks was answer enough. “Never mind. Given the CCTV, it doesn’t really matter since we can see if anybody comes in upstairs. So, what can you tell me? Why are you afraid of Sheriff Robinson?”

Cat didn’t immediately answer. Instead, she stared at the screen for a while with the frown of a person gathering their thoughts and deciding how much was safe to tell me. “Matthew told me that Robinson was here a couple weeks ago with the Mayor and Dr. Petersen. They wanted him to release logs from an IRC server he runs for the kids so they have somewhere safe to talk. He told them to produce a warrant or fuck off. They came back last week and made the same demands, but still didn’t have a warrant. They said that if he didn’t see reason soon, he’d suffer for it.”

“Why didn’t he go to the Phoenix Society?”

“They told him that if he tried to expose them, they’d trump up charges that would blacken both our names.”

“What were they going to do, find some toddlers and coach them to make accusations of Satanic ritual abuse?”

That got a small chuckle out of Cat. “That’s what Matt said. But he didn’t go to you guys because Robinson wore civilian clothes each visit. I was at work, and Matt told me everything down here, so I don’t think Robinson realizes I know.”

They threatened the guy because he wouldn’t produce IRC logs without a warrant? All three of them? And now he was dead. That made no sense whatsofuckingever. Not unless those kids were talking about something they saw at Fort Clarion that would utterly compromise Collins, Robinson, and Petersen. Something like Project Harker?

If Cat had given me a specific time and date, it would be much easier to check the CCTV footage, or see if they were on the job at the time and thus had Witness Protocol running. With the information I currently had, any such effort would be a fishing expedition. “Cat, I need you to think carefully. Do you have any recordings or other evidence of these threats? I can arrest those bastards tonight if I have some proof of their involvement in your husband’s murder. Just give me something I can use.”

It wasn’t much to ask, despite Cat’s recent bereavement. Was it? Even fifteen seconds of video would be enough, if it captured a threat to the deceased. I just needed something more substantial than “my husband told me afterward”.

Cat eventually shook her head. “Matt gave me an account on Tetragrammaton and tried to teach me how to admin the machine, but I never got into it like he did. He was like a big kid with the ultimate model railroad.”

“I know it’s bad practice, but would you be willing to either share your username and password with me?”

She gave me a dubious look. “I could just give you an account of your own with admin rights.”

“Wouldn’t a new account with sysadmin access be noticed? Robinson isn’t an idiot, and neither is Petersen.”

Cat sighed, and found a pen and a scrap of paper. “Here.”

“Thanks.” I saved the credentials, and found a shredder. It wouldn’t do to leave a root password to Tetragrammaton lying around for just anyone to find. “Do you have people outside Clarion? You really shouldn’t be alone right now.”

Cat took a moment to answer. “Some of my cousins are visiting Manhattan. I could join them and follow them back to Melbourne, but what about…” She pushed back a sob. “Matt’s funeral?”

“Manhattan should be far enough for you to be safe. I just think you should get out of town for a bit. The arrangements can wait.”

## Track 42: Makeup and Vanity Set — “I Am Become Death”

With Cat Tricklebank safely in her truck and headed east, it was time to deal with her husband’s mainframe. Or better yet, have Malkuth do it. 「Mal, I need you. Again.」

「Be still, my heart. Is this just to evac another witness?」

With secure talk being a plain-text medium, it was an unfortunate impossibility to tell whether Malkuth was being his usual flirty self, or had taken a sarcastic turn. 「Nope. How would you like to have a go at cracking the last working Multics installation on Earth?」

「Do tell.」 Was that curiosity?

「It’s a pre-Nationfall General Atomic mainframe. Model GA-65535. Matt Tricklebank has been renting out space on it since he took it over during resettlement of Clarion twelve years ago. Even the local government use it, so I wouldn’t be surprised if Dr. Petersen had an account. There should be lots of little treats for you. Surely the sysadmin’s murder is probable cause for a bit of snooping.」

「What’s your basis for treating the admin’s death as a murder?」

Malkuth should damn well know why, considering what Cat said about her husband refusing to hand over log files without a warrant. 「I guess you stopped monitoring my feed. Tricklebank refused to hand over IRC logs, and his wife says Robinson, Collins, and Petersen threatened him. There’s got to be something on that machine they want buried.」

「And now you’re going to dig it up. Dumb motherfuckers should have just bribed the guy.」

No way I could argue with that, though bribery brings its own risks. Individuals who are sufficiently unscrupulous to accept bribes but honest enough to stay bought are a rare commodity. I’ve never met one. 「So, can you help me get into Tetragrammaton?」

「You’ve completed your inventory of Fort Clarion’s armaments. We can’t justify your presence in Clarion any longer.」

「After everything I’ve turned up in the process?」

「You’ve gotten pretty far on probable cause and circumstantial evidence, Naomi, but without an official complaint -」

「Never mind all that.」 What does he want, forms in triplicate filled out by hand? 「Review my Witness Protocol feed. His wife contends they came to their place of business in civilian clothes to make their threats.」

「Not for nothing, Nims, but you’re pushing the limits. You’re walking on lines you dare not cross.」

Lines I dare not cross? Seriously? 「This isn’t a police procedural drama, Mal. Does somebody further up want me to back off?」

A long pause before Malkuth replied. 「All I can say is that what you think you’re discovering is already well-known. Things are as they are in Clarion for reasons I cannot explain because the people who want it this way didn’t explain themselves to me.」

It was hard to believe what Malkuth had just said. To think that somebody highly placed within the Phoenix Society would be aware of the situation, and do nothing about it, was intolerable. That’s not the Society’s purpose. That isn’t my purpose as an Adversary. 「If I have to face a court martial when it’s over, I will, but these murders and disappearances need to stop. I can’t turn my back.」

「I suspected you’d say that, and I’m not unsympathatic.」 What was Malkuth risking by helping me? Could he be put on trial? How would we go about doing so, when his hardware probably makes Tetragrammaton look like a handheld? 「I’ll do what I can, but for now you have more pressing concerns. The Fallen Angels escorting your witnesses have dropped off the network.」

I stopped being miffed about not being able to get access to Tetragrammaton immediately. This took priority. 「Could they just be maintaining radio silence?」

「That’s not standard practice. I’m transmitting their last known location. Find out what happened, Adversary Bradleigh.」 The connection cut out after Malkuth sent the Fallen Angels’ location as a set latitude and longitude values.

I plugged them into my GPS app, and got a location: about 20km east of here. I’d need my motorcycle. Sprinting back to The Lonely Mountain, I found Mike Brubaker where I had left him. “I need you to come to my room with me.”

“But-”

“Not now.” Fortunately, he didn’t make me drag him up to my room by his collar. Nor did he ask questions as I opened the closet and yanked out the case of goodies Nakajima sent me care of Eddie Cohen. Opening it, I pulled out the armor and found the bodysuit I was supposed to wear beneath it along with an owner’s manual. “Something happened to the escort guarding your friends. I need to check it out. I need you to stay here.”

“I should come with you.”

“Not happening.” I ducked into the bathroom to change, since I wouldn’t be able to wear the armor over my regular clothes. When I came out a few minutes later, Mike had most of the pieces arranged on the bed.

He pointed at the boots on the floor. “Step into those first. Then spread out your arms.”

I did as instructed, and let him attach the rest. “Have you done this sort of thing before?”

“My big brother played hockey. He was a goalie.” He didn’t elaborate on why he used the past tense, but continued to attach each piece until all that remained was my helmet. Without asking permission, he found one of my elastic hair bands and used it to tie my hair into a bun before handing me my helmet. “Now we just need to strap on your weapons. Did this come with some kind of harness?”

“In the case.”

Mike soon had me rigged, and I handed him my sidesword. “If anybody other than me or the Halfords come through this door, stab them. You don’t have to reload a sword.”

He drew the sword and tried a thrust; his form was terrible, but at least he knew that holding the sharp end is the other guy’s job. “Got it.”

Locking the door behind me, I found my motorcycle and fired it up. Seconds later, I left Clarion behind. The engine beneath me purred as I cut through the night and followed the map to the Fallen Angels’ last known coordinates. The road was empty, and the cycle’s lights were the only relief from the cloudy gloom.

With half a kilometer left to my destination, I pulled off the road and hid my motorcycle. If the bus and its Fallen Angels escort had been attacked, I didn’t want to ride into an ambush. Choosing every step with care to avoid detection, I crept forward and found my worst fears confirmed.

The bus was a smoking, ruined hulk of twisted metal. It lay on what had once been its roof. The damage to the nearby road pointed at landmines, or perhaps an improvised explosive device. The Fallen Angels fared no better, but they died with empty guns and broken swords. Whoever killed them had hacked Sergeant Jackson’s head from his body and mounted it on a spike. As if this desecration were insufficient, they also stuck a rolled up note in his mouth.

It read, “If you’re as smart as you think you are, Ms. Bradleigh, you’ll get back on your motorcycle and forget this place even exists. The kids who invaded Fort Clarion are ours now. You cannot save them.”

“The bloody hell I can’t!”

“Naomi? Is that you?”

Despite the voice sounding like Renfield’s, I drew my swords. Even if it wasn’t an imposter, there’s no guarantee he’s trustworthy. I hadn’t heard from him since the last night we met. “Who’s asking?”

A figure stepped out of the trees, showing empty hands. His brooding expression quickened my pulse, for he had worn it before I took my pleasure with him. He appeared untouched by the violence around me, his uniform spotless and freshly pressed. “Christ, Naomi. It’s me, Chris Renfield. Remember that night in the the old basement, when I started to seduce you and you turned the tables?”

Oh, I remembered. And that made me vulnerable. “Not taking any chances. You’re going to have to convince me.”

He was on me before I could draw one of my unfamiliar swords, pinning me against a tree. He tore my helmet off, and I couldn’t see where it rolled as he kissed me breathless. Desire, no, lust threatened to overwhelm me. Never mind that there were kids who needed my help, Renfield had set me ablaze and he was the only one who could quench the flames. From the hard heat pressing against me, he felt the same, and I shivered with the memory of feeling that part of him and taking possession.

As if that weren’t enough, there were the teeth. His fangs, gently scraping the tender skin of my throat where he had bitten me last time. This time, I wanted him to bite me. I wanted his cock in me, and my blood in his mouth. How sick was that? “You know, way down deep, that I’m the real deal.”

He was the real deal all right. He was all wrong for me, and I bloody well knew it, but at the same time he already had me at the point where the slightest touch might set me off. Who is this bastard, that he could manipulate me like this? Pushing him away, I drew both my swords to keep him off me. “What the hell are you doing here, Renfield? Why should I believe you didn’t have a hand in this massacre?”

He shook his head, unable to meet my eyes. “Naomi, I’ll swear by anything you call holy I wasn’t involved. One of the men, Corporal Seward, rallied the other men to his side. He’s been stirring them up, saying it’s time we stopped living in hiding and took Clarion for our own. I tried to tell them it would only get us killed, but they won’t listen to me.”

“He’s insane! Right now, he’s just dealing with a single Adversary. A lone sword. If he moves openly against Clarion, he’ll bring the full force of the Phoenix Society down on Dusk Patrol. They might not care about me, but they’ll protect Clarion just to retain legitimacy.”

“You know that. I know that.” Renfield spread his hands in a gesture meant to calm me down. It only pissed me off. “Seward still thinks that if this turns into a clusterfuck, they can just retreat underground.”

“If Seward attacks Clarion, your men will indeed be going back underground. If they’re lucky, the Society might even bother to mark their graves.”

“Seward said something about how the Colonel was still looking out for us, and had root on something called Gungnir. I don’t know what he means, but -”

“I do, and it’s bad.” If Dr. Petersen has access to the GUNGNIR platform, then we’re in deep shit. How the hell do you fight against tungsten lances falling from orbit at terminal velocity? 「Malkuth, are you paying attention? If this isn’t probable cause to hit Tetragrammaton, I don’t know what is.」

Instead of waiting for Malkuth’s reply, I returned immediately to Renfield. “I need your help to get inside Fort Clarion’s underground. If those kids are still alive, I have to rescue them.”

Renfield nodded. “I know a shortcut to the fort from here. I can show you a back door, but for fuck’s sake, put your swords away. Having all that naked steel behind me makes me nervous.”

I did as he requested, but unslung my rifle instead. “If you have any objection to me shooting anybody who gets in my way, you’d better tell me now. I won’t have time to pretend I care once we’re under fire.”

Renfield didn’t say anything. Instead, I felt a pinprick on the back of my neck, in the gap between my armor and my hairline. I tried to whirl on him, to gun him down for his betrayal, but whatever he injected me with took effect too swiftly. My legs collapsed beneath me, leaving me an insensate heap.

## Track 43: Guns ’n Roses — “Pretty Tied Up”

When I regained consciousness, I thought the world had inverted itself around me. It took a second for the fog dulling my reason to clear enough for me to realize that I was upside down. Somebody had bound my hands behind my back with what felt like a cable tie, bound my ankles with nylon rope, and hung me head-down by my ankles from a hook in a meat locker. However, it wasn’t the meat locker we found in Fort Clarion’s mess hall. This one was bigger, and reeked of fresh blood.

People had died here. They had died tonight, and if I didn’t get the hell out of here, I was most likely next on the menu. Fired by this knowledge, I took further stock of my situation. My captors had stripped me of my weapons and armor, leaving me in only my undersuit. With no weapons concealed on my body, simply cutting away my bindings was out of the question. That left me with only one option; I had to break free before somebody came for me.

It would be easy enough to wear out my ankle restraints by swinging from the hook and letting friction do the work for me, but I was half a meter from a hard concrete floor. Once I was off the hook, I would most likely crack my skull. I didn’t have enough room to twist my body to land on something I could more easily afford to break.

Hands first, then. It didn’t take long, but my wrists were bruised and bloody by the time I had freed them, and my shoulders ached from the effort. Despite time being my enemy, I needed a rest before soldiering on.

My fingertips brushed against concrete, and I pulled my hands back for a moment. Though the blood-stained floor repulsed me, I had an idea for getting my ankles freed. As long as I remained hanging, breaking my ankle bonds was most likely a great way to smear my brains across the floor.

But what if I was hung low enough that doing a handstand would get me off the hook? Taking a deep breath to settle my nerves, I tried it. I couldn’t muster the strength to lift myself on the first go, but a perverse corner of my imagination supplied me with a little clip of how I’d die if I didn’t get off that damn hook. It involved a knife, a trough, and a platoon of thirsty vampire soldiers. Quite emphatically averse to going that way, I screamed through the pain and lifted myself until the rope binding my ankles cleared the hook.

Retreating to a corner got me away from the worst of the blood, and gave me the reassurance that can only come from having your back against a solid wall. Whatever came next, it wouldn’t be able to come at me from behind.

“Shit!” Sudden pain flared in my left hand. Seeing that I was bleeding from a superficial cut, I looked around and found a broken knife blade beside me. It only had a single edge, but using it to cut my ankles loose would still be dangerous. Handling it as gingerly as possible, I began to saw away at the last of my bonds. Despite it being broken, the knife-blade still made fairly short work of what I was using it for.

Now that I was free, it was time to reconsider my situation. I was still unarmed and practically naked. My undersuit covered me neck-to-toe, and held in enough body heat to keep me from freezing in this meat locker. Aside from myself, I had no weapons save the broken blade. Worse, I was stuck in here until somebody opened the door; it was locked and didn’t open from inside. Speaking of the door: it opened outward, which precluded use as a blunt instrument.

Though it might just be the meat locker, my implant showed I was offline. If it wasn’t just the room, then I had finally gotten into Fort Clarion’s underground. Man, they were going to regret bringing me home for dinner.

However, I had one small advantage; my eyes had adapted to the gloom. Anyone coming in would need a second or two to adjust. That second or two was my best hope of getting out of here. I had to strike swiftly, incapacitate the first man to enter, and use whatever weapons he carried to take out any companions he had behind him. Crouching in the shadows of the corner nearest the door, I waited and listened while counting my breaths.

Fortunately, my captors didn’t make me wait too long. Minutes after I had taken up my position, I heard approaching footsteps. I tensed as they stopped and a key scraped metal. There was a thunk of metal striking concrete, as if the soldier had simply dropped whatever padlock secured the door’s outer latch. The door was silent on well-oiled hinges as it opened to admit the light from the kitchen outside. The shadows preceding the man suggested he was coming in alone, but I didn’t relax yet. His buddies might still be outside, but standing where their shadows wouldn’t give them away.

“Where the fuck is she?” The soldier stepped inside, blinking at the dark, and I put aside all caution because the son of a bitch had my sword. Not one of the blades Nakajima lent me, but my sidesword, the one I carried throughout my career. I lent Brubaker that blade so he could protect himself in my absence, and if Dusk Patrol had the weapon, they most likely had the kid as well. Whoever this bastard is, he’s going to be the first to pay.

The knife-blade I hurled at his face struck his brow and bit deeply, pouring blood across one of his eyes as I leaped upon him with a feral snarl. He grappled with me, but for some reason, refused to take the offensive. Was he reluctant to strike a woman? Too bad for him if he was, because I was done pissing about. Driving him to his knees with a knee to the balls, I kicked him in the face and bounced the back of his head off the wall behind him. “Where’s Brubaker?”

“Please, stop.” Instead of fighting, he tried to protect himself, curling into a fetal position. “I’m not here to fight you. Please stop hurting me.”

Disgusted with him and myself, I gave him one last kick in the guts before taking my sword off him. “You call yourself a soldier? You’re pathetic. How can you just let me beat the shit out of you like this?”

“R-Renfield sent me.” His breath hitched as he lay shivering on the floor. It took me a moment to realize he was crying. “H-He told me to give you back your sword and bring you to him. He’s got the Brubaker k-kid. He’s safe.”

Brubaker’s safe? With the same Renfield who jabbed me with a sedative and let his buddies hang me by my ankles in a godforsaken meat locker? Not bloody likely. Taking a breath, I drew my sword and tapped him with the tip. “On your feet, soldier, and give me a name so I don’t have to think of you as Private Crybaby.”

He wiped his eyes with the back of his hand. “It’s Private Fowler, ma’am.”

“Adversary Naomi Bradleigh.” Seemed only fair to return the favor. “You said Renfield sent you?”

Fowler nodded. “He told me you’d be miffed about the hypo, but…”

Miffed? Man, Renfield has *no* idea. If he doesn’t have a good explanation, and if Brubaker isn’t safe, Private Fowler here is going to think I went easy on him. “Take me to him. Shall I belabor the consequences of leading me into a trap?”

“No, ma’am. That won’t be necessary.” Judging from the glum expression, he had already given the matter some thought. That suited me just fine; I doubted I was sufficiently sadistic to describe crueler tortures than whatever Fowler might imagine.

Fort Clarion’s underground was a maze of narrow hallways full of doors marked with evocative labels like ‘Isolation Chamber X7’ and ‘Secure Containment’. Fowler ignored them, and led me to the NCO Barracks. Renfield waited inside, and seemed to be doing his best to ignore the fact that Brubaker had my rifle aimed at his head, his finger on the trigger poised to fire. Since I had not sheathed my blade the entire time Fowler escorted me, I pressed my tip into Renfield’s throat. “Start talking. Make it good, and for the sake of those kids your fellow soldiers took, make it quick.”

Renfield shook his head. “Those kids are already dead. There’s nothing you can do about it. I wasn’t going to deny the men their blood.”

I leaned into my sword a little, and let the tip taste blood. “Remember what I said about making it good?”

Renfield spat his defiance. “Look, Naomi, you could shove that sword up my ass and it wouldn’t change the fact that those kids had it coming. That reclusive motherfucker Yoder had it coming. He led the others down here, and didn’t stop them from murdering several of our number in their sleep.”

The kids I tried to protect are murderers? That certainly puts this situation in a different light, but how can I be sure Renfield is telling the truth? “Mike, do you know anything about this?”

Mike Brubaker wouldn’t look at me, and wouldn’t answer until Renfield shook his head. “Tell the lady, kid. This must be the first she’s heard of what’s really been happening around here lately.”

“Renfield’s right.” Mike still wouldn’t look at me, but now I understood why. The kid was ashamed. “I had showed Yoder the entrance once. He led the others down here a month ago, and said something about getting payback for all the people who disappeared in the woods.”

Great. I had walked right into the middle of a vicious circle. Locals and visitors sometimes disappear in the woods. Yoder leads some kids he met on the town’s secret youth forum to the fort. They murder some Dusk Patrol soldiers in their sleep, without even knowing if they’re responsible for the disappearances. Now Dusk Patrol is hunting these people down and making examples of them. And here I am, armed to the teeth and ready to make matters even worse. No wonder Renfield sedated me.

I was still annoyed about being tied up and hung from a hook in a meat locker, but that could wait. Why didn’t Brubaker say something? “You must have had a reason for not telling me, but did you at least speak to the Sheriff when you realized what Yoder did?”

“I did. He told me to mind my own business.” Brubaker shook his head. “I went to him again after Wilson turned up dead, and told him he had to tell you what was going on. He said he would, but…”

Renfield nodded, and handed me a photograph of two officers and a military policeman. “Instead, he told us. He was sergeant major in charge of the MPs at Fort Clarion back in the day. Mayor Collins was a second lieutenant just out of West Point. We know them, and they look out for us.”

And they covered up every disappearance since the resettlement of Fort Clarion. If what Renfield and Brubaker are telling me is true, and those kids are dead, then Robinson and Collins are accessories. But why? And does the reason even matter at this juncture? “What about Dr. Petersen? Where does he fit in?”

“You mean Colonel Petersen? He’s still looking out for us, too.” Renfield sighed. “He wanted me to shoot you and Brubaker. Him because he might have told you everything, and you because you would expose us. It’s what Adversaries like you do. I couldn’t do it, or let the others do it either. Brubaker spoke up for us, and you? You’re a soldier doing your job, just like us. But when you sided with people who murdered our brothers, you changed everything.”

Had I known I’d spend my vacation hacking through years of murderous bullshit, I’d have packed a machete. Next time, I’m damn well bringing one - and keeping a knife up my sleeve in case some asshole ties me up without buying me dinner and arranging a safe-word first.

## Track 44: Nemesea — “Caught in the Middle”

You think I sided with those kids against you and your men?” It was hard to believe Renfield could misunderstand my purpose in coming to Clarion. I was never for or against Dusk Patrol. How could I have been, when I had remained unaware of their existence until after my arrival? “I came here to find out why people visiting Clarion disappear, not to take sides in your little guerrilla war against the local yokels.

“Hey!”

Ignoring Brubaker’s protest, I pressed on. “Even if I had known they might be guilty of murdering your fellow soldiers, I would still have tried to protect them. But instead of witnesses, they would have been prisoners awaiting trial for murder.”

“What do you think you’d accomplish by putting these kids on trial? What jurisdiction would recognize us as persons under the law?”

“The Phoenix Society would recognize you. You think Fort Clarion is the first isolated pocket of humanity we’ve found and helped rejoin civilization? You aren’t even the first isolated bunch of soldiers we’ve had to help understand that the war’s been over for decades, and it’s time to lay down your arms and come back to the world.”

Renfield and Fowler’s bitter laughter at my words stung, but Renfield’s reply cut deeper. “You’re cute, Bradleigh. You want us to come back to the world and trust your justice? You saw what the world did to us. Those kids killed our brothers in cold blood. We’ll make our own justice.”

Brubaker shook his head. “Make your own justice? All you’re doing is taking revenge, just like my idiot friends were doing.”

“I am the only one standing between you and the same fate your friends met.” He turned to Fowler. “Tell the others it’s finished.”

“Yes, sir.” Fowler saluted, and left.

Once the door closed behind him, I returned my attention to Renfield. “Care to tell me why Brubaker would deserve to join his friends in the grave?”

He shook his head. “The kid didn’t do anything to stop his gang. That makes him an accessory.”

“I wasn’t even there, dammit.”

A door thunked shut outside. Instead of saying anything, Renfield held a finger to his lips. Breathing as softly as I could, I listened by the door. There were voices, and slowly approaching footsteps. We waited, breathing as shallowly as we could, until all sounds from outside faded into silence. Looking over my shoulder, I kept my voice at a whisper just in case. “Why aren’t they looking for us? They don’t seem to realize a couple of outsiders are down here.”

A long pause before Renfield answered. “They trust me.” He gestured toward some filing cabinets. “Give me a hand with these, kid.”

The room filled with the grinding of metal on concrete as Brubaker and Renfield pushed aside some filing cabinets to reveal a door. Renfield unlocked and opened it, revealing a stairwell. I tensed, sure the noise would bring the others down on us, but no one came.

Nobody needed to tell me what to do. Since I was first up the stairs, I listened by the door a moment before opening it to reveal Fort Clarion in the cool, faint light of an early autumn morning. The men followed me, and Renfield locked the door behind him before leading us to the Post Exchange. “We’ll be safer aboveground, at least during the day, but we can’t linger.”

Brubaker looked askance at Renfield. “You’re coming with us?”

Renfield shrugged. “Might as well. The others will figure out soon enough that I didn’t kill you two, and they’ll be none too thrilled with me.”

“What are you going to do, then?”

Another shrug from Renfield. “Maybe I can help. I don’t really know anything about Project Harker other than what I told you, and maybe it’s time I stopped living in the dark.”

Brubaker snorted. “Gonna make your own justice?”

“I don’t know.” Renfield fell silent for a long moment, and I was about to prod him when he spoke again. “I thought I was leading the men by believing in the last mission we were given. I clung to that belief, perhaps for too long. It might be too late for me to lead them by showing them a way back to the world, but Naomi, what you said about the Phoenix Society helping others in this situation gives me hope. Do you really think you can help?”

This was the Renfield I remembered from before he tranked me and left me tied up in a meat locker, but was he being honest with me this time? Did he really do it to save my life and Brubaker’s? I wasn’t willing to trust him, but if he wants rope, I’ll give it to him. If he hangs himself, that’s his problem. “I’m willing to try, but I need you to answer my questions as honestly as you can. No more bullshit.”

Brubaker glanced at Renfield a moment before leaning close to me. 「Are you sure this is a good idea?」

Not that he needed to be close to me if he was using secure talk, but it wasn’t worth addressing right now. 「No, but I think Petersen, Collins, and Robinson are using Dusk Patrol. They believe they’re protecting themselves by preserving their former superiors’ secrets, and we’re not going to change that until we expose the truth.」

「Fine. Then ask Renfield about Tetragrammaton. Even I know a computer like that wouldn’t just be sitting in an abandoned town like Clarion if it was still in working condition.」

Now there was a good point, and it had been nagging at me for a while. Why did Clarion have a working General Atomic mainframe in a random building’s basement?

“You two done with your telepathy, or however the fuck you’re talking to each other in secret?” Renfield’s tone held a note of disapproval, which was fair enough. We probably were being rude by having a private conversation right in front of the man.

“You know anything about that mainframe we’ve got in town, Tetragrammaton?” Brubaker blurted the question before I could regain control of the situation.

“Fuck off, kid. I saw you leaning close to Naomi. Is that what you were discussing with her?”

“Actually, Mike was questioning the wisdom of trusting you.” And under the maglev he goes. It wasn’t personal, of course, but I can’t have him making enemies on my behalf. Not when I can do a perfectly adequate job of it on my own. “We aren’t necessarily at cross purposes, Sergeant. You want what’s best for your men. I’m looking out for the people of Clarion, which includes your men. Leaving things as they are doesn’t serve anybody but Petersen, Collins, and Robinson—and I say bollocks to the lot of ‘em.”

Renfield did not speak, but the changes in his expression were an eloquent description of his struggle. Petersen and Collins were commissioned officers, part of Renfield’s chain of command. As such, they had a responsibility toward the men under them. Robinson was a MP, with duties similar to my own. Facing the fact that they didn’t have his best interests at heart, or those of his men, couldn’t possibly be easy.

We sat together, waiting for Renfield to decide whether and how to help us as the sun rose and shortened the shadows. Without a word, Mike reached into his pack and offered us a breakfast of protein bars, jerky, and bottled water. I whispered my thanks while accepting my share, but Renfield shook his head. I was halfway through the packet of jerky when he finally spoke. “I can help you, but it will be risky. We have to go back down.”

“Back underground?” If Dusk Patrol were smart, they wouldn’t all sleep at the same time. Instead, they’d sleep in shifts and keep watch. If they patrolled the underground, instead of simply guarding the barracks, they might find us and wake the rest. “If we’re not careful, we’re likely to be discovered down there.”

“Only if we’re noisy. I’ll be taking you to a location well away from the barracks. None of us go there, because of the memories. I’d rather not venture down there, but you might be right. Maybe it is time to show the world what our country did to us, even if that country no longer exists. But if the Phoenix Society already has this information, won’t they find a way to bury it?”

Good point, and not one for which I had a particularly good answer. But did I actually need one right now? “Let’s worry about that later. We should find the truth before we worry about getting it out into the world.”

That got a wry smile from Mike. “Keeping your priorities straight?”

“Come on. We don’t want to be there long.” Renfield spoke with the impatience of a man who had committed himself to an unpleasant task and wanted to see it completed as soon as possible. Without a word, he led us to the infirmary. We followed him into a doctor’s private office. A dead General Atomic terminal sat forlorn on a desktop cluttered with issues of medical and biological journals like The Haemostat and Organelle. I remembered the latter term from my studies, and assumed the journal dealt with cellular biology.

Renfield stopped at a door bearing a placard warning that the room beyond contained confidential medical information that should only be accessed by authorized personnel. Whatever was buried down here, it was buried even deeper than the rest of the fort’s subterrane. We picked our way down twelve flights of stairs lit only by faint red LEDs, double the flights of stairs we had climbed to reach the surface earlier this morning. At the bottom of the stairs, flanked by empty guard stations, we found a second door bearing the following sign:

> Commonwealth Advanced Research Agency  
> **Project Harker**  
> Authorized Personnel Only  
> All personnel must submit to search on entry and exit.
 
Without a word, Renfield opened the door, and stepped back with an ashen face and trembling hands. “I should keep watch outside. There are too many memories in there for me.”

## Track 45: The Rolling Stones — “You Can’t Always Get What You Want”

Tempted as I was to offer to hold Renfield’s hand, I refrained from doing so lest he think I was mocking his experiences. Besides, I didn’t have time to comfort him, nor was I sure we had the sort of relationship that would let me do so. Instead, I turned to Brubaker. “Mike, can you keep watch outside with Christopher? He might appreciate the company.”

His snarl put the lie to his words. “I’m fine.”

“Look, if you want to wait outside by yourself, that’s fine. However, I can use you beside me. It will take me longer to figure this place out on my own.”

Renfield remained pale, but his hands steadied as he took a deep breath and mastered himself. “I’ll stay behind you as long as I can, but I won’t go in there first.”

Unwilling to make a big deal of the situation, I shrugged and reached inside the doorway to feel around for light switches. I soon found them, and flipped them all at once, banishing the gloom with the cold white glow of flickering florescent lights.

Based on Renfield’s initial refusal to cross the threshold, I half-expected a pseudoscientific torture chamber with blood smeared on floors and walls, rusty, crude medical instruments encrusted with old gore, and machinery whose function I dared not guess for my sanity’s sake. Instead, the lab was clean aside from a bit of dust covering examination tables and workstations. Computers quietly hummed, their screens prompting me for passwords I did not possess and lacked the time to crack.

Halfway across the lab on the left was a door marked “Isolation Cells”. Opposite to it, the pharmacy door stood slightly ajar. At the back loomed a door marked “Records/Computer”. Hopefully it was the one to lead me to the answers, because I doubted I would find them in the isolation cells or the pharmacy.

“At first, I’d just wake up down here after laying down in my rack for the night.” Renfield’s voice was small and quiet. “That wasn’t so bad, though nobody would ever tell me why they kept taking my blood, or what the hell they were injecting into me. It got worse when I started waking up in one of those goddamn isolation cells.”

Though beginning to shudder, Renfield threw open the door to the isolation cells. He reached inside and turned on the lights. “And once they were sure they had made me into something new, they started cutting on me to see how my body reacted. This whole section’s soundproofed so nobody in the rest of the lab could hear me scream.”

It stood to reason that Project Harker involved the vivisection of test subjects after they had been altered, presumably to test for enhanced healing and regenerative capabilities. However, I felt as though I should be shocked. I should be outraged, because this right here is why Adversaries like me exist. Violations of this nature were what I swore to oppose. “What about your men?”

“I was the first.”

“It can’t have been that bad.” Brubaker still waited by the entryway, rifle at the ready. Though he had muttered the words, they sliced through the silent room and bit deep into Renfield. “Since you let them do it to the others.”

Glaring at Brubaker for a moment, I then turned my back on him to focus on the man behind me. “Are you all right?”

Renfield shrugged, and let me lead him to the pharmacy before speaking again. “He’s right. I should have spoken out, and I would have. I think the bastards knew it, because I spent a month under sedation in one of the isolation cells while they performed the Renfield Protocol on the others.”

“They named the process after you?” Talk about profoundly fucked up. They secured Renfield’s continued cooperation and ensured he remained dedicated to his men in one simple move. How could he leave them when he felt responsible for their situation. The threat of an information leak all the brass needed to undermine him. There’s no way they’d follow Renfield if they thought he had sold them out, is there? “And how is it you took this better than the men?”

“Yeah. Said it was some kind of tribute to my having the balls to be the first volunteer.” Renfield sighed as he opened the door to the pharmacy so I could examine its contents. “You need to understand, Naomi. We all volunteered. We thought it would make us better soldiers, so we were happy to do it. We just didn’t know what it would involve.”

Leaning close to me so that only might hear him, he added, “And I’m not handling it well at all. I just tell myself that my men need me more than I need to deal with my feelings.”

I had no words that would ease Renfield’s pain, and the memory of how he had seemingly betrayed me to save me from his men still burned too hot for me to willingly offer the touch that might have substituted for words. Instead, I considered the drugs before me. Of the chemicals I recognized, most seemed to be antipsychotics. Did the so-called Renfield Process induce psychosis in its subjects? “Renfield, how many of the volunteers were fit for duty after the process was completed? What was the casualty rate?”

“They never gave us exact figures, or told us what happened to them afterward, but at least a third of us didn’t make it. As far as I know, they never even got decent funerals.”

“Would you like to find out what happened to them?”

Renfield’s eyes narrowed as he stared at me. “You think you can get that information?”

I didn’t want to promise anything, but it was likely the Project Harker scientists would have kept records of who suffered such adverse actions to the Renfield Process that they either died, or had to be put down. No doubt the researchers would have justified these murders by invoking the need to maintain secrecy. “I’m willing to try, Sergeant. Let’s have a look at the records, shall we?”

Getting past the door was easier said than done, since none of Renfield’s keys fit. “I thought you had access to everything in the base.”

“I thought I did, too.” Renfield flipped through his keyring before tossing them to me. “You try it. Maybe I missed one.”

Fair enough. Starting with the first key, I tried each in turn until an unlabeled key turned all the tumblers and disengaged the lock. Turning the latch, I gave the door a gentle push and stepped back as it slowly, silently turned on well-oiled hinges. “Better mark it for next time.”

“I’d rather there never was a next time.” Renfield clipped the keys to his belt, reached inside, and flipped on the lights.

Empty. The bloody room was practically empty. All the bookshelves were bare. There was nary a filing cabinet to its name. The worktables didn’t even have dust on them. All that remained was a mainframe humming softly in one corner, so I approached it for a better look. A second bay sat empty, as if it once housed another mainframe.

Finding the system console, I sat down in front of it and tried waking it up. The keys responded in a manner that reminded me of the basement under Gibson Hacking Supply, and the screen flared to life and displayed a familiar prompt.
 
> GENERAL ATOMIC MODEL GA-65536  
> MULTICS VERSION 20481031.23.17  
> UPTIME 10 YEARS, 321 DAYS, 5 HOURS, 9 MINUTES, AND 6.9 SECONDS
 
> tetragrammaton login:
 
“Son of a bitch.” What were the odds of there being two of the same model of mainframe, running the same version of the same operating system, with similar uptimes, with the same system name? It was a question I’d have to dump on Malkuth, because the best answer I could come up with is not bloody likely. Regardless of probability, it seemed worthwhile to at least try the credentials Cat gave me. If I got in, we had mirrored systems.

Typing them from my photo of Cat’s scrap of paper Cat, I hit the enter key and waited for a response. All I got was a shell prompt.
 
> ctricklebank@tetragrammaton $
 
Now I knew for sure. “This machine’s a mirror of the one in Clarion. How is that possible?”

Renfield nodded, and pointed at an empty bay that must have housed a second General Atomic mainframe. “Just before everything went to shit, we got orders to take apart one of the mainframes and transport it into town. Some of the brass must have decided it would be a good idea to keep the backup system in a location that wasn’t likely to get bombed because it wasn’t a legitimate military target. So we lugged the machine over in pieces, set it up, and hooked it to some kind of monster fiberoptic cable that some other people must have run out to the town, and fired it up.”

This could be the break I need. If Petersen was keeping the records for his continued experiments on Tetragrammaton, including that weird breeding program Brubaker insisted was being run on the townspeople, and the two instances of Tetragrammaton remained in sync, I should be able to access it here. First, however, I still needed admin privileges since Cat had given me her credentials.

I tried the usual command, but didn’t get the usual response. I got this, instead.
 
Cat, don’t forget that if you need to do admin stuff, you can’t just use the ‘sudo’ command like you would on Unix. I rigged a ‘su’ alias to the actual privilege escalation command, but you need the admin password, not your own.
 
Nice of the guy to leave that little note for Cat, but it didn’t help me much. “Shit.”

“You got the other password?”

“No. Maybe Cat didn’t know about this, or had forgotten.” I tried reaching Mike, but without wifi, my implant was useless. “Do me a favor and take over for Brubaker. I need his help.”

“Right.” I never heard Renfield leave.

Mike soon showed up, but without the rifle. “Need me to log in?”

Standing, I backed away from the machine so he could sit down. “It’s worth a try, but I doubt you have the admin password.”

“We might not need it.” Mike shrugged. “A couple weeks ago, Tricklebank gave me a shell script he wrote, and asked me to run it and help him test some security patches.”

Typing a command, he sat back and waited for the result. It wasn’t what either of us hoped for. “I guess those patches worked. Too bad the poor bastard never got to find out for sure.”

While one shouldn’t speak ill of the dead, it was a bloody shame Matt Tricklebank was so good at his job. “Never mind him. Can you get to the network?”

## Track 46: The Sisters of Mercy — “A Rock and a Hard Place”

「You’re under Fort Clarion? Are you serious?」 Malkuth’s reply came less than a second after I figured out how to open a secure talk session with him from Tetragrammaton. 「Your IP address says you’re at Gibson Hacker Supply in Clarion.」

「When they paired the mainframes, they must have set up some kind of spoofing to make all network activity appear to come from the town instead of the base.」

「Never mind all that. What’s up? I’ve been trying to reach you. The Fallen Angels escort got wasted a few kilometers out of Clarion.」

「Haven’t you been paying attention to my Witness Protocol feed? I saw what happened to them. Then I got tranked, tied up, and had to houdini my way out of a bloody meat locker.」

That must have made an impression on Malkuth, because a minute elapsed before his next response. 「Any idea what happened to the kids the Angels were supposed to escort?」

「The bastards living under Fort Clarion got ‘em. Unfortunately, I’ve uncovered new evidence suggesting that they might not have been innocent victims. I have Brubaker with me, and he confirmed Sergeant Renfield’s allegation that the local kids got in and killed several soldiers in their sleep.」

「He’s the kid you thought might make a good Adversary. You’re keeping him safe, right?」

「Of course I’m keeping him safe, but look, I’m down here surrounded by decades of profoundly fucked up history with Renfield and Brubaker. We’ve gone unnoticed thus far, but my luck won’t hold out forever, and I’m using our best shot at figuring out what’s really happening here.」

「What do you think is happening here?」

That it was the obvious question didn’t stop me from dreading it. 「I suspect Dr. Petersen, Mayor Collins, and Sheriff Robinson of conspiring to commit multiple murders to conceal the continued existence of survivors from Dusk Patrol.」

Damn AI kept me waiting several minutes this time, which suggested he wasn’t just thinking it over, but talking with the sort of people who give me my orders. 「Your reports and Witness Protocol data show a reasonable basis for such inferences. However, we need evidence.」

「I think the evidence is here on Tetragrammaton, but I need a search warrant and help escalating my privileges so I can get at the data. Oh, and a quick rundown on the Multics shell.」 Cat’s husband might have provided POSIX compatibility for users who wanted to stick with Unix-style commands rather than learn a new system, but I wouldn’t be surprised if he considered it a crutch and refused to use it himself. 「The fact that somebody bothered to maintain a constant connection between these mainframes suggests that Tetragrammaton isn’t just some clever boffin’s salvage project.」

「Agreed. Take down this warrant ID for reference.」 Malkuth followed with the usual hexadecimal string, which I filed away using my implant. No way I’d remember it without augmented memory. 「Be careful down there. If you’re right about the two machines being linked, you might want to consider retreating to town. By the time you get back, I’ll have sent you code for the privilege escalation you need. In the meantime I will see what information I can copy over should anything go wrong.」

「OK. Disconnecting now.」 Once I had cleaned up after myself and logged out, I turned to Brubaker and Renfield. “Change of plans, guys. We’re going to get the hell out of here, and search Tetragrammaton in town.”

“Not likely.” A vaguely familiar voice spoke behind us as men clicked off their rifles’ safeties.

Turning to face them, I recognized their leader. He was the soldier I had left alive, but severely wounded, the night of Scott Wilson’s murder. “Hello again.”

My greeting didn’t amuse him. “Renfield, I told you to kill your fuck toy once you were done with her.”

“And when did a sergeant ever take orders from a corporal, Seward?” Renfield advanced upon the other men, heedless of the soldiers’ rifles trained on him. “Adversary Bradleigh is trying to help us.”

“She’s a British spy.”

“There is no United Queensreich of Great Britain anymore, you moron.” Renfield swept a hand through the air as if to brush off an irrelevant past. “Just like there’s no longer a North American Commonwealth. I keep telling you people, but you just don’t get it. You’re too busy listening to Sergeant Major Robinson’s bullshit.”

Is Renfield referring to the Sheriff? I’ll have to ask him later if I get the chance. “Gentlemen, Sergeant Renfield is right. I am here to help, though matters have grown rather complicated. It seems some of you have been killing residents of Clarion, presumably because they’ve trespassed on Fort Clarion and -”

“They killed Jones. And Casey!” One of the soldiers interrupted me.

Another soldier joined in. “They snuck down here while we were sleeping, drove wooden stakes into their hearts, and cut off their goddamn heads. Those superstitious idiots think we’re vampires.”

“Look at where we are. I am aware about the experiments. Renfield told me about Project Harker and the psychological warfare that resulted from it. I’ve read the D Corps novels, and I understand some of you took inspiration from the books into battle.”

Seward glared at Renfield. “What else did you tell her?”

“Dammit, Seward, she’s on our side. She says the Phoenix Society can help us go back to the world. We don’t have to live like this any longer.”

“And you believe her?” Seward sneered before remembering my presence. “What else did you tell Renfield while you two were fucking? Did you tell him that there was a cure for the process that made us what we are? Did you promise him that nobody would try to figure out what made us what we are and try to use that knowledge to create more like us?”

“Naomi didn’t-”

Though I appreciated Renfield speaking in my defense, I cut him off. These were charges I had to answer myself if I wanted any credibility. “Corporal Seward, I made no claims concerning the possibility of reversing the Renfield Process. I am not aware of how it actually works, since I have not yet accessed the Project Harker archives stored on the computer behind me. The process may in fact be irreversible. Even if that were not the case, I could not in good conscience promise a cure, because I’m not a scientist.”

“No, you aren’t.” Seward glanced at the sword on my hip. “I doubt any scientist would fight as you did that night. You realize we expect you to answer for killing one of us, don’t you?”

“Is the right to self-defense exclusively yours, Corporal Seward?”

“Maybe not, but even if you didn’t have our blood on your hands, you tried to protect others who killed our brothers. You’re protecting one of them now.”

Brubaker tightened his grip on my rifle. “I spoke out. I told them to leave you alone. They wouldn’t listen.”

“Talk’s cheap, kid. You were there, and didn’t do anything to stop them. You didn’t even report them to civilian authorities, did you?” Seward’s words dripped venom, but I said nothing. If this was true, then Brubaker deceived me as well. If he had witnessed the murders of several Dusk Patrol soldiers in their sleep and didn’t report them, then he was an accessory, and I would, perforce, arrest him.

Brubaker reddened at the accusation. “The Hell I didn’t! I went straight to Sheriff Robinson. I told him everything, and he did nothing but tell me to keep quiet if I knew what was good for me.”

Before anybody could say anything, I wrested my rifle from Brubaker’s grasp and smacked him upside the head. “When were you planning to mention that he did nothing about it, but told you to keep quiet and used threats to secure your silence? I could have arrested that son of a bitch already on a textbook abuse-of-power charge.”

Brubaker looked away, his voice barely audible. “I didn’t think you could protect me. And I was right, wasn’t I? You couldn’t even protect my friends.”

“See?” Seward spread his hands. “He doesn’t care about anybody but himself and his gang. No way Corporal Robinson would tell a witness to stay quiet. We know the guy. He looks out for us.”

“He turned you into mushrooms.” How can Seward and the rest of Dawn Patrol not realize that the people they trusted are screwing them over? “Robinson, Petersen, and Collins keep you in the dark and feed you bullshit. They use your fear of Project Harker’s secrets getting out into the world to keep you here, where every once in a while some rebellious kids nobody really cares about might get lucky and take a few of your heads.”

Seward shrugged. “Maybe you’re right, but why should we trust you?”

“I don’t give a toss if you idiots trust me or not. You tried to kill me, remember, and threatened to do worse because I had the nerve to fight back. Regardless, you should know that the secret is already out. The Phoenix Society already knows everything about Project Harker. They just won’t tell me.”

One of the soldiers spat on the floor. “So we should trust you, some pale bitch nobody knows? Why should we? Because you look like a Tomcat Treat from way back when?”

Is that what Tomcat called their models? Doesn’t matter. If I’m going to get these people to trust me long enough for us to get out of here without a fight, there’s only one way. If I let them have Brubaker, they’d kill him. If I didn’t make a show of authority against Brubaker, they’d come after both of us. Sure, I’m throwing him under a maglev again, but it might be the safest place for him right now. “Michael Brubaker, you are under arrest on the charge of abetting abuses of power on the part of one Sheriff Robinson of Clarion. You have the right to remain silent. Anything you say may be used against you at trial. You have the right to consult an attorney and have them present with you during questioning. If you cannot afford an attorney, the Phoenix Society will engage one on your behalf. You may exercise these rights at any time, with no repercussions. In addition, you have the right to humane treatment while in custody. Do you understand your rights as a person accused of a crime, Mr. Brubaker?”

He wouldn’t look at me, and wouldn’t answer at first. I was about to repeat my recital when he finally spoke up. “I understand.”

Renfield was kind enough to offer me a zip tie, which allowed me to secure Brubaker’s hands behind his back. Once I had him bound, I turned to Seward and the rest of Dusk Patrol. “Are you satisfied? Michael Brubaker will receive the due process of law. There is no need for further violence.”

Seward shook his head. “You think you can make a show of arresting him in front of us, and then set him free as soon as you’re out of our sight.”

“That’s not happening. The arrest is on record. I cannot dismiss the charges on my own, so Mr. Brubaker’s fate is now up to a jury to decide.”

With a sign from Corporal Seward, the remnants of Dusk Patrol surrounded me, Brubaker, and Renfield. Seward himself took a place beside me. “And we’re going to make sure he faces that jury. If he doesn’t, you’re going to face us.”

## Track 47: Elvis Presley — “Jailhouse Rock”

We must have made a hell of a sight, marching back into Clarion with a sizable Dusk Patrol contingent escorting us. Fortunately, none of them knew I had an implant that allowed me to send texts over an encrypted connection.

With Robinson a suspect, I couldn’t ask him to help me turn the tables on our uniformed friends. Since I lacked the authority to muster them myself, I couldn’t count on militia support. That left Deputy Colby, and maybe Kaylee. Between the two of them, I might manage to gather sufficient forces to subdue my escort without the Sheriff’s interference.「Deputy Colby, I could use your help. Same with you, Kaylee.」

「What’s up?」

Kaylee’s reply wasn’t nearly as guarded. 「Naomi? Last anybody saw of you, you had taken Mike up to your room. So, how was he?」

「Kaylee! I’d never take advantage like that. Deputy Colby, can you and Kaylee round up some deputies and a small militia detachment without tipping off Sheriff Robinson or Mayor Collins? I need some  backup while making an arrest. I’d rather this didn’t become a lynching.」

「Who the hell are you arresting?」

「A bunch of soldiers living under Fort Clarion, for the murders of Yoder, Wilson, Foster, and a bunch of other kids. They’re escorting me and Mike Brubaker back to town right now. I’ve also got Brubaker in custody.」

「For murder?」

If I told Colby I meant to nail Robinson on a tyranny charge, would she still be willing to help me? Or would she feel she owes Robinson? Better not take chances by telling her too much. 「Protective. He’s a material witness.」

「Can’t you get support from the Phoenix Society?」

As if I had time to say pretty-please and deal with the all bureaucratic bollocks the Society puts in the way of Adversaries who could use a bit of backup. Not that I’d tell Colby or Kaylee anything of the sort.「HQ prefers we attempt to cooperate with local authorities first.」

「OK.」 Seeing that response was a relief, but a short-lived one. 「Just tell me one thing: is the Sheriff involved in this, too? He’s been getting this shifty-eyed look lately whenever that old army base comes up.」

「Pfft. Mayor Collins always looks like that.」 Thanks, Kaylee. Glad I’m not the only one who thinks so.

「I’m not sure how much I could divulge without compromising the investigation. Let’s just say he’s on my radar.」

「Fair enough. I’ll round up as many as I can. We’ll be waiting when you get here.」

“Who were you talking to?” One of the soldiers barked at me. He pressed his fingertips to his ear, a gesture used to indicate to others around us we were using our implants. “I saw you doing this. It means you’re on the phone with somebody, right?”

Damn. It’s always the little things that trip you up, like an ingrained habit of pressing a fingertip to your ear so people don’t think you’re ignoring them. “You’re right. I was on the phone. My mother called.”

Another soldier snickered. “Yeah. Sure. What did dear old mum want? Grandkittens?”

If a CPMD- person had said anything of the sort, I’d take it as a slur. Coming from another with CPMD, it was merely rude. An old, worn sign placed by the Commonwealth gave me the perfect lie. “She thinks I’m on vacation. I told her I’m out hiking and met some interesting men, but none of them were really my type.”

“Hear that, guys? Princess thinks we’re not her type.”

“Well, you did ruin it for the rest of us.” Another soldier shrugged. “What’s the big deal? If her mom brings backup, we’ll just slit this bitch’s throat and the kid’s.”

It was just as well that they announced his intent. Knowing they’d kill me and Brubaker if I had called in support meant I wouldn’t have to feel guilty about going all-out if it came to violence. Not killing more of Dusk Patrol would be nice, but I had a witness to protect and dreams to pursue once I had served my time as an Adversary.

Nobody said anything of substance the rest of the march back to Clarion. My unease grew as we approached the town, for there was no sign of the Sheriff’s department or town militia. Instead, the streets were full of residents going about their normal business and visitors beginning to stream in for the annual Clarion Rocks music festival. They recoiled at our approach, though I suspect the tendency of some of the soldiers to leer at young women had something to do with that.

It wasn’t until we reached the Clarion police department headquarters that Deputy Colby sprang her trap. As we approached, there was a sudden rumble of boots on pavement around us as Sheriff’s deputies and civilians armed with shotguns, farm implements, and a sledgehammer encircled us. Colby had barely stepped up when the soldiers of Dusk Patrol put their hands up. I suppose they realized they had little chance of taking out a force of a hundred when they numbered less than twenty.

One of the men slowly stepped forward, his empty hands held high. “Can you guarantee our safety if we surrender?”

Colby nodded. “I can guarantee that nobody will harm you and yours under my command, but I must place you and your men under arrest for murder.”

Indignant shouts rose from the men. “But they murdered our friends first.” “This is bullshit!”

I expected Renfield to say something, to make some attempt at persuading the men who once followed him, and now seemed to follow Corporal Seward. Instead, he remained silent. He approached Deputy Colby a step at a time, making no sudden movements that a deputy or irregular with shaky nerves and an itchy trigger finger might mistake for aggression. When he was three steps away from Colby, he turned to face his men, and held his hands behind his back for the handcuffs.

The other Dusk Patrol survivors followed him into the jail, escorted by Colby’s deputies. Most of the civilian volunteers dispersed. Kaylee was last to leave, giving a parting wink and a text admonition. 「Enjoy him while you can.」

I tried to ignore her as she sashayed past Mike and slapped his ass on the way. Soon I was alone with him. He glanced at the jail. “You aren’t going to put me in there with them, are you?”

“I’m not sure there’s room. So it looks like you’re my prisoner.”

He studied me, as if not sure if I was joking. “You really think I aided and abetted Sheriff Robinson’s tyranny by keeping quiet?”

Shrugging, I led Brubaker away from the Sheriff’s Department. A smart kid like him should have figured out that it was a bullshit charge by now. If I tried to put him in front of a jury, any green lawyer could make a case for witness intimidation on Robinson’s part. “It kept those soldiers from lynching you, and gives me an excuse to keep you close that Robinson can’t overrule.”

That got the wheels turning in his head. “So, if he tries to take me from you, you can nail him for interfering with your investigation?”

“I knew you’d figure it out.” Unfortunately, that still left Renfield where Robinson could get at him, but I was more concerned about Brubaker. At his age, Renfield ought to be perfectly capable of taking care of himself.

“What’s next, then? Back to The Lonely Mountain? You still haven’t gotten any info out of Tetragrammaton.”

Quite true. With a suddenly full jail, I doubted the Sheriff’s department would get underfoot, which provided a golden opportunity to do some data mining. Besides, I also had an incoming message notification. Looks like Malkuth not only came through on the search warrant, but cracked Tetragrammaton and gave me the keys.

Regardless, a return to The Lonely Mountain seemed an excellent idea. I needed to eat, and a hot shower would be nice. Then again, duty demanded I get back to work immediately. Amid conflicting demands, reason asserted herself and forced a compromise: eat first, then head back to Gibson Hacker Supply.

Fate, or at least Dr. Petersen, had other plans. As soon as we had taken a table in The Lonely Mountain’s common room, the doctor took a chair right across from me and Brubaker. “I hear you had a rough night at Fort Clarion. Were you injured?”

“I’m fine, but the same can’t be said for some of your younger patients.”

The lack of emotion in Petersen’s expression and voice meant something. “A tragic and unfortunate loss. They were witnesses under your protection, were they not?”

They were, you son of a bitch, and if I find the slightest scrap of evidence of your involvement in their deaths, I will bloody well crucify you. Not that I said anything of the sort. “A smart Adversary learns when to delegate. Fortunately, I already saw to it that the culprits are in custody.”

Petersen steepled his fingers before him, and regarded me in silence a moment. “Yes, you did. You managed to persuade Dusk Patrol to surrender. That makes you a most intriguing young lady. I wonder, what will you do next?”

He wonders, does he? I smiled at him and the approaching Bruce Halford. I hoped the good doc could handle disappointment, because right this moment my plans consisted entirely of an outrageously large breakfast. “Hi, Bruce. The usual for me and Mr. Brubaker, please.”

“No problem. What about the doctor?”

*Physician, feed thyself.* I would get to Petersen soon enough. Once I had enough dirt to bury the son of a bitch.

## Track 48: Ozzy Osbourne — “Zombie Stomp”

It’s amazing how far a good breakfast and a couple of mugs of hot coffee can go toward substituting for a good night’s sleep. By the time I finished my second cup, I actually felt capable of rational thought. Which was a good thing, since I still had work to do at Gibson Hacker Supply.

My use of the Clarion Sheriff’s Department to ambush Dusk Patrol and arrest them en masse had a useful side effect: there wasn’t anybody guarding Tricklebank’s shop. All I had to do was open the door, and walk right in. One small problem, though. “Bloody door is locked. Figures.”

Mike nodded toward the alley. “Probably a good idea to use a back door, anyway.”

We ducked down the alley to the narrow street behind Gibson Hacker Supply, and gave the doorknob a quick turn. That one was also locked, but Mike seemed unperturbed.

Checking his surroundings first, he crouched by the back door and produced a small case. He seemed to know something about the locksmith’s trade, for he took a moment to study the lock before picking specific tools for the job. “When I was six, my grandpa caught me fiddling with these little darlings. He taught me how to use ‘em, and put me to work. Guess he thought it better than having me learning how to use them on my own out on the street.”

“Could be a handy skill for an Adversary.” Though problematic as hell from an evidentiary standpoint. Would any information I gathered as a result of this bit of breaking and entering be admissible in court?

“Yeah, but I was the one who got the others inside Fort Clarion. If I didn’t know how to pick locks, they’d all be alive.”

“Are you sure you’re not blaming yourself so you can make sense of what’s been happening?”

His hands stopped as he looked over his shoulder at me. “The hell does that mean?”

“It means it won’t bring any of them back. Your friends are dead. They might have died without your involvement. Let’s focus on making sure nobody else dies.”

Mike shook his head, tried the latch, and opened the door a crack. He put away his tools in silence before holding the door for me. “Has anybody ever mentioned you sometimes sound like a stuck-up bitch?”

“Maybe it’s just the posh accent.”

“Whatever.” Mike closed the door behind us and flipped on the lights. “Once we saw the soldiers sleeping, I got out of there. I told the others to do the same, but they didn’t listen. Renfield said that my friends tried slitting one guy’s throat. When that didn’t work, they hacked his head off.”

Unable to keep from imagining the scene, I shuddered. It was the sort of thing that made the news every now and then. Some junkie with purist beliefs catching someone like me alone and unarmed in a dark street. But Clarion was an integrated community. There was no reason to suspect an anti-CPMD sentiment. It was simply that the level of hatred driving the crime had been similar. “You don’t want to believe your friends were capable of such brutality.”

“Would you?” Mike spat the words in disgust. “Goddammit, I shouldn’t have said anything in the first place. It isn’t your problem.”

He was right, but I wasn’t going to say so. Doing so would indeed make me a bitch. Instead, I had work to do. “Get behind cover and watch the street. Warn me if company comes.”

Opening the door to the backroom, I froze in the doorway at the sight of Mayor Collins wearing safety glasses and holding a sledgehammer aloft. He brought the hammer down on the keyboard, scattering shattered plastic everywhere. The ring of my sword clearing the scabbard caught Collins’ attention as he raised his hammer to strike another blow. “Mayor Collins, you are under arrest for destroying evidence pertaining to a Phoenix Society investigation and obstruction of justice. Drop your weapon and put your hands behind your head.”

Mayor Collins turned toward me, still grasping his hammer. Realizing he had no intention of complying, I steeled myself for his assault.

He rushed me, holding his hammer before him as if he meant to drive its head through my chest. If he was smart enough to do that instead of taking a swing that would leave him open, then he was smart enough to be dangerous.

Darting aside, I slashed at his forearm, the tip of my blade parting wool and silk before biting into flesh. I had drawn first blood, but my cut wasn’t deep enough to weaken his grip. For all I knew, his clothing might even have protected him a bit.

Worse, he was fast. Pivoting, he jabbed at me with his weapon and caught my shoulder with a glancing blow. “You couldn’t just have your little vacation and leave, could you? Well, Adversary, you’re going to become a statistic.”

Opening his forearm again, I followed with a cut across his cheek. “It’ll be a cold day in Hell before I die at the hands of a flunky like you.”

That got a laugh out of Collins. “Well, pretty kitty, I hear they’re playing hockey on the Styx right now.”

“That was actually a halfway decent rejoinder.” But not good enough to keep me from cutting him again. “You really think Renfield will let you get away with killing me? Hell, you think the Phoenix Society will let my death go?”

“Renfield’ll mourn, if only because you’re the best poontang he’s had in decades, but he’ll get over it.” Collins shrugged, deflecting my thrust with the haft of his sledgehammer. “In the meantime, he’ll do his job and keep the men in line. He’ll be reliable again, just like he was before you showed up.”

“Not bloody likely, given that I persuaded him and a couple dozen of his men to surrender. They’re in jail right now.” Collins hesitated, and I took the opportunity to run him through. He fell to his knees, holding a hand to his chest as blood leaked from his mouth. “And when they get out, do you really think they’ll go back to being your pet killers? I think it’s more likely they’ll turn on you like wild animals kept too long in a small cage.”

“Too bad you won’t live to see it happen.” Wait a minute. Collins’ voice was loud and clear. He didn’t sound anything like a man who had just had a foot of steel driven though his lung. And what happened to the cuts on his face? He rose, and took a swing at me that I barely dodged. “There are arrangements in place.”

“Naomi! Down!” That was Mike’s voice, and the metallic clunk characteristic of a pump-action shotgun. Damn right I was getting down.

Mayor Collins staggered backward with each blast of buckshot Mike unloaded into his chest. Yet he not only remained standing, but smiled as he tore the tattered remains of his jacket and shirt from his shoulders and threw them aside. The ruined flesh of his torso knit together, and his figure grew slimmer, as Mike dropped shells in his frantic haste to reload.

No human should be able to do that, but I didn’t have time to fuck around. Without a better idea, I grasped the dull base of my blade to stabilize it. Charging the Mayor, I drove the point through his throat so that it pierced his spine and pinned him to the wall. “Mike!”

He tossed me the pump action, and I squeezed off the first shot as soon as I had the muzzle pressed between Collins’ eyebrows. His body began to slacken in death, but rather than take chances, I kept firing until I had pulped his skull. In case you were wondering, it was as disgusting as it sounds.

Mike dropped the shotgun as soon as I handed it to him. “Naomi, what the fuck just happened?”

Good question. Dusk Patrol had been enhanced as part of Project Harker. Among other advantages, they now had regenerative capability. But they were an all CPMD+ unit. Collins was CPMD-, and had not been a victim of Project Harker. If Collins was able to regenerate, and this fast, who else was equipped with this ability? Robinson? The good doctor himself? All I knew was that like those Dusk Patrol soldiers I fought in the woods, you had to attack the brain to kill them. Welcome to my zombie apocalypse. “I think Dr. Petersen continued Project Harker’s work in secret. He managed to apply its treatments to Mayor Collins.”

“Is that even possible?”

I pointed at what was left of the man’s face. “I’d suggest asking him, but you’d have to find a competent necromancer first. Pick up the used shells. We have to get out of here before we’re caught. I doubt we’ll get a chance to plead self-defense.”

Mike nodded, and tossed me the thumb drive containing my HermitCrab environment as I began wiping down every surface we had touched. This would look terrible at my all-but-inevitable court martial, but right now I was more concerned about delaying untrustworthy local authorities. “If they knew about this machine, they probably know about the other. How about you download the information, and we can use my basement in the woods to review it? I can still get a signal down there.”

Cleaning my blade on a scrap of Collins’ tattered shirt, I found I couldn’t blame Mike for not wanting to stick around. I didn’t want to, either. However, the basement wouldn’t do. It wasn’t even close to being defensible, since we’d be trapped down there with our enemies controlling the only exit. “The basement’s no good from a tactical standpoint, and if Petersen and Robinson are watching Tetragrammaton, they’re probably watching for external access. We have to try the other mainframe under Fort Clarion, and upload whatever data we can find from there.”

## Track 49: AC/DC — “Dirty Deeds Done Dirt Cheap”

When I first paired my Conquest with my implant, it was so the bike could send information like current speed, charge remaining, engine temperature, and tire pressure directly to my implant's visual overlay instead of making me glance at little dials. It was a standard feature, and I thought nothing of it until my implant displayed a notification telling me that my bike was five kilometers away, and asking if I wanted to call it. As a lark, I chose the "call bike" option. A few minutes later, my Conquest trundled riderless up Main Street and stopped in front of me.

Mike stared at my ride, no doubt as surprised as I. "Naomi, did you know your ride had autopilot?"

"No. That's what I get for not reading the manual." And if Jacob Spinoza had told me this bike could steer itself, I might not have haggled so hard.

Not that reading the documentation stopped us from going off-road, with Mike clinging to my waist for dear life. He pressed his face into my shoulder, which in other circumstances might have been cute. No doubt a collaboration between marketing and legal came up with the warning that Conquest motorcycles weren't designed for such use, for the bike performed almost as well off-road as it did on the highway. It just didn't look as nice spattered with mud. Nor did I, for that matter.

Of course, we left a nice clear trail for anybody who wanted to follow us. It had started to rain while we were inside Gibson Hacker Supply, and the ground quickly softened enough that a bike with two riders left tracks screaming *Naomi went this way*. However, getting there on foot in this weather would have taken much longer. 

I'll admit it: my plan's sanity was questionable. Here I was, returning to Dusk Patrol's home base, where several of their number no doubt still slept. The only upside was that at Fort Clarion I might manage to avoid further civilian casualties. 

Further civilian casualties other than Mike, that is, who seemed capable of fighting beside me. At least, he kept himself together when we faced Mayor Collins, and seemed to know his away around the designated marksman rifle he filched from the armory. My superiors would bitch about the discrepancy between my report and what their arms control crew actually found. 

Fuck 'em. If we're going to make a stand here, we might as well be properly equipped. I was about to emulate Mike's example and grab a carbine with an grenade launcher mounted under the barrel when I saw him pick up an object marked 'Front Toward Enemy'. "Is this what I think it is? And why are you grinning like that?"

Why? Because I just had a deliciously evil idea. If I really wanted to fuck with Dusk Patrol, what better way than to use their own weapons to deny them access to their arsenal? The Network of Things had infiltrated the military, resulting in mines a soldier could detonate with a smartphone, or rig to detonate should anybody else approach. 

The last thing anybody dumb enough to get too close would here before the bang would be a keening scream. "I had forgotten they had Mandrakes here. Let's set some by the entrance. And see if there's any Semtex."

"You're going to rig the place to blow if somebody else comes in here?"

"Oh yeah. So grab plenty of ammo; we won't be able to come back for more." While Mike scurried off, I set my traps. With a bunch of shriekers slaved to each other, the first schmuck to set foot in here would get his legs blown out from under him and blow up every bit of ammo and explosives the building. I was sure to catch hell from the Phoenix Society for this bit of dirty pool, but right now I had more pressing concerns.

Besides, all's fair in love and war.

Since I had a couple of shriekers left, I took them with me. I could rig them in front of the entrance to the watchtower. They'd hurt somebody, and give Mike and me warning we were about to have company. Speaking of Mike, I should probably see if he's actually any good with that rifle. "How accurate are you with that weapon?"

Mike shrugged, and pointed skyward, where a flock of Canada geese called out to one another with harsh cries as they flew south. A lone straggler trailed behind. "See that goose flying alone?"

Oh, no. "Must you?"

"Any idiot can nail a stationary target. And this one is probably old or sick. See how erratically he flies?" As Mike pointed out the goose's flight, it plummeted from the sky as if dying in mid-flight. Mike led the bird for a moment, and fired. The report echoed through the fort with a sharp crack, and a second later I saw the poor goose burst into a spray of gore and feathers. Mike lowered the rifle. "Any questions?"

"You realize I'm not happy you killed a bird just to prove yourself to me? Even if he was going to die anyway."

Mike nodded. "Yeah, but try visiting Clarion in the spring when they're underfoot, crapping everywhere, and attacking people who get too close. You'll be ready to bag your limit within a week."

"So, you hunt them?"

Mike shrugged. I hefted the backpack full of supplies I looted from the mess hall. "Come on. Do we need anything else before we hole up? I really don't want to push my luck any further without cause".

"Don't think so. We're armed, we've got some food and water, and you grabbed a laptop from Gibson Hacker. Right?"

Recalling the banknotes worth 250mg of gold I left on the counter, I nodded. "Yeah. Nothing fancy, but it'll run HermitCrab and let me talk to Tetragrammaton over secure shell unless somebody cuts the power to both machines."

"So, where do we make our stand?" 

"The western tower." I turned and began walking toward it, and Mike had my back. At least in the tower we'd have the advantage of elevation. Even if they surrounded us, we would see them coming from all sides and snipe at them as they approached. 

The stair leading up was also defensible. Even if I hadn't placed my last two Mandrakes at the entrance, it was a tight space. Between some flashbangs appropriated from the armory and the late Matt Tricklebank's shotgun, we should be well placed to hold our own.

The top of the watchtower had a Tesla point. At least I wouldn't have to worry about the battery running out. Not that I expected to need ten hours, but sometimes it's the little things that make the difference. While HermitCrab ran though its startup scripts, I took in Fort Clarion from above. 

While the immediate vicinity of the towers themselves was quite open, the barracks and other buildings provided more cover than I would have liked for attackers. There was no way we could secure the individual buildings, let alone ensure they remained so. Hopefully Dusk Patrol didn't have high-power rifles stashed underground, or something ridiculous like a recoilless rifle or a rocket launcher.

With that cheery thought, I picked up my laptop and connected to Tetragrammaton via secure shell. Fortunately, I got a strong signal high up above the base, and the damage Mayor Collins had done to Tetragrammaton's console and chassis hadn't brought the computer down. The credentials Malkuth provided earlier gave me the keys to the kingdom. "Mike, I just found your porn stash. You naughty boy."

"That's just for show, in case my parents ever got into my account and went poking around. Didn't you ever have a fake diary for your parents to find?"

"I didn't have the sort of relationship with my parents that made such subterfuge necessary. Also, I left home to study in New York when I was fourteen."

"Damn." Mike shook his head, and scanned the base through his scope. "I wish I had had the nerve to do that."

"You had the nerve to fight beside me."

"Thanks." Narrowing his eyes, he placed his finger on the trigger, only to remove it a second later. "Just a deer. You find anything yet?"

"I'm in the directory for Petersen Family Medicine right now. If I don't find anything here, I can also hit his personal account and see if he has anything more interesting than porn there." Hoping that the grep tool would prove that sometimes old tricks were the best kind, I searched for 'project harker' from the command line.

And I got nothing. At least, nothing in the account for Petersen Family Medicine. Same with 'Dusk Patrol', 'Harker', and 'Renfield'. Just for chuckles, I tried my own name. Nothing but my records for when I got patched up. Nothing terribly interesting there, either.

Time to hit his personal account. Running the same commands I used earlier, I hit paydirt. The output just kept scrolling until I aborted the search. A couple seconds of work, and I tailored and tailored my command to just list the directories containing files that mentioned Project Harker. There was only one, named "harker".

That's where I hit the jackpot. It was all there: requirements, specifications, protocols, security recommendations, and gigabytes of email. Now to find out what role Petersen really played in all this. Was he Frankenstein, or the unwilling bystander and advocate his men believed him to be? Searching in the email directory for 'proposal' and filtering the output by a search for 'mutiny', I found the following: 

> from: Col. Henrik Petersen  
> to: Dr. Ian Malkin  
> date: 6 May 2046  
> subject: Risk Management
>  
> Dr. Malkin:
>  
> I have just finished reading your proposal. 
>   
> While I agree that the men and women of Dusk Patrol would make excellent candidates for your experiments, we must tread carefully. They have rights under the law, and many of them are aware of this fact. They must be carefully managed lest they mutiny, or speak to the media.
>  
> Therefore, it is my suggestion that the authorization for this project come not from me, but from a general in my chain of command. This will allow me to credibly act the part of advocate for my men, protesting on their behalf against illegal human experimentation.  
> 
> If I am overruled, but continue to play the part of advocate, the soldiers will grudgingly allow themselves to be subjected to your experiments. Lt. Collins will maintain discipline among the men and punish dissent.
>   
> ---  
> Col. Henrik Petersen
> North American Commonwealth Army
> Fort Clarion, Mid-Atlantic Province

The reply was succinct, and if Project Harker hadn't been Ian Malkin's baby, I might even have liked the man:

> from: Dr. Ian Malkin  
> to: Col. Henrik Petersen  
> date: 6 May 2046  
> subject: RE: Risk Management
>  
> Your logic is sound, albeit repugnant even to me. 
>  
> ---
> Dr. Ian Malkin, CEO
> AsgarTech Corporation

I looked up in time to see Mike stand up and stretch. He glanced at me. "Find anything?"

"Only stuff I don't dare share with Renfield or the others if I want to get Petersen and Robinson in front of a jury. But they're not the only people who need a bit of due process. Ever hear of somebody named Ian Malkin?"

"Nope." Mike shrugged. "You're rummaging in Petersen's directory, right? Is there anything about breeding experiments or genetics?"

"Let's have a look." 'Breeding' or 'genetic' might be too specific a term, though, so I started with 'population'. A smart move on my part, because this came up.

> from: Col. Henrik Petersen  
> to: Dr. Ian Malkin  
> date: 3 June 2049  
> subject: Further Research
>  
> Dr. Malkin:
>  
> It is clear that the North American Commonwealth will soon collapse, especially since I faced no official reprisals for my use of the GUNGNIR platform against civilian protesters gathering outside the base. Everybody has more pressing concerns than a bunch of whining hippies who think we're trying to create a battalion of super-soldiers.
>  
> However, I think it's too soon to conclude Project Harker. The research to date has turned up several interesting questions requiring further investigation.
> 
> 1. We don't know what long-term effects the Renfield Protocol will have on those it didn't kill outright. A longitudinal study is indicated.
> 2. We have not attempted to replicate the research with other populations. Therefore, we don't know why the Renfield Protocol works on some CPMD+ individuals but not others.
> 3. We don't have a control group. The most likely candidates would be a population of CPMD- individuals.
> 
> You seem to know people involved with this new NGO, the Phoenix Society. Would any of them prove sufficiently sympathetic with our mutual aims to help repopulate the town of Clarion? With sympathetic allies in local government, I can pose as a local physician and continue your work in secret. 
> 
> In a chaotic world, the Society may need to rule rather than serve. It cannot do so without soldiers capable of enforcing its dictates. By continuing the work begun with Project Harker, we can *provide* these soldiers.
>   
> ---  
> Col. Henrik Petersen
> North American Commonwealth Army
> Fort Clarion, Mid-Atlantic Province

By the time I had finished reading this, I was shaking. Not only were the suspicions Mike shared with me true, but the truth carried implications that affected *me*. Did the Phoenix Society use technology developed by Project Harker on Adversaries like me? Had *I* been subjected to the Renfield Protocol? And who was Ian Malkin?

That last question I might be able to answer on my own. Running a search on the name, I found a hit in the metadata for an image file. As I opened it, the screen filled with a high-resolution color photograph of a handsome blue-eyed man wearing a white double-breasted suit with a blue cravat. He was snow-blonde, like me, and the expression of secretive amusement with the world that the photographer captured was one I remembered from my sparring sessions with Maestro. Looks like we might cross swords again.

## Track 50: Judas Priest — “Night Comes Down”

It’ll be dark soon. Are you sure we should stay here?” Mike’s unease was evident in his voice, and he was right. We couldn’t afford to still be here once night fell. Though I had initially assessed the watchtower as a defensible location, I had done so from the viewpoint of an Adversary fighting human opponents. But we weren’t facing regular soldiers, even regular soldiers outfitted with night-vision gear. We were facing what remained of Dusk Patrol.

Dusk Patrol’s enhanced abilities come from what Dr. Petersen’s research called the “asura potential” in CPMD+ individuals, which the Renfield Protocol forcibly activated. Combined with decades of training in low-light combat, they were the one unit justified in claiming ownership of the night.

But if we left, where would we go? Returning to town would put civilians at risk. Not to mention that word of Collins’ death has most likely circulated by now, which would surely make us persona non grata. If we ran fast enough, would we escape the Fallen Angels’ fate? Now that I had access to the Project Harker data, could we afford to retreat and wait for backup?

Speaking of backup, where the bloody fuck was the Phoenix Society’s arms control unit? They should have been here already. Time to check with Malkuth, but I’d let Mike listen in so I wouldn’t have to explain everything afterward. 「You paying attention, Mal?」

「Had I known you’d be so high-maintenance, I wouldn’t have asked you for a date.」

「If I had known you weren’t man enough to handle me, I wouldn’t have accepted. Where’s that arms control unit the Society was supposed to send after I completed my survey of Fort Clarion? They’d be really handy right now.」

「How come? You’ve gotten most of Dusk Patrol to surrender. I doubt they’re going to break out of jail overnight. Or do you think Robinson will let them out and sic them on you?」

That was exactly what I suspected Robinson might do, especially if he’s found out that Mike and I killed Mayor Collins. While I could prove self-defense, I doubt the Sheriff would let me live long enough to stand trial. 「Dusk Patrol has a non-commissioned officer, Corporal Seward, who appears to be more loyal to Robinson than Renfield. Even if Reinfield were able to keep the men already in custody from joining the fray, Robinson could still make my life unnecessarily complicated. So, where’s my backup?」

「They’ll arrive at 0600 tomorrow morning. Think you can hold out until then?」

Talk about life-or-death decisions. I can either hold out until morning, or be Dusk Patrol’s latest snack. Considering the weapons we gathered, and Mike’s steady gaze through his rifle’s scope, I shrugged. There were worse places to make a stand, but I had grown convinced that this wasn’t the best place for me to make mine. 「What kind of tactical support can you provide?」

「Gevurah can get a satellite over Fort Clarion in forty-five minutes, giving you an eye in the sky. But if you leave the fort, you are not to take any catalogued equipment off-base. That ordnance is Phoenix Society property under the Arms Control and Containment Treaty of 1955.」

Great. We could be moments away from fighting crazy super-soldiers, and Mal’s blithering about a century-old treaty whose signatory nations are all history? Fuck him and his treaty sideways. 「Given that I booby-trapped the arsenal, that treaty’s worth fuck-all if Dusk Patrol gets anywhere near it. Furthermore, as an officer of the Phoenix Society, I possess authority under the terms of the ACCT to arm myself and lend Society property to civilian militia.」

Disconnecting from Malkuth, I gave Mike an apologetic smile. “Sorry, kid. You’re in the militia now.”

“Least I’ll get paid that way. What’s the plan?”

“Still working on it.” Which I was, but an idea came to mind. Since I’m not going to download the Project Harker data to this laptop, why should I stick around to babysit the transfer? Cracking open an editor, I whipped up a shell script that would create an archive of Petersen’s home directory and upload it to Malkuth. For good measure, I added a line to upload another copy to one of the virtual lockers Port Royal provides as a public service. Paranoid, of course, but I wanted a copy of the evidence that the Phoenix Society couldn’t touch. Just in case.

It didn’t take long to do a dry run. I could have run it myself, using a terminal multiplexer in detached mode, but I had a better idea. Saving my script, I made a copy of the scheduler’s configuration file, and added two lines. One would run my script. The other would clean up after me by moving the original configuration file back into place and deleting my script. Once it was done, I shut down my laptop. “We’re done here. Let’s bugger off while we’ve still got some daylight.”

Mike had already packed his gear. In what seemed a token effort at honoring Malkuth’s strictures, he took apart the designated marksman’s rifle. Grinning, he bent the firing pin before reassembling the rifle. Without a new one, the weapon would be useless. “What about the data? And where are we going to go?”

“It’s uploading now.” Though I knew where to go, I hesitated. Without a warrant, the legality of my next move was questionable despite the probable cause the evidence gave me. “We’re going to arrest Dr. Petersen.”

“You’re going to hold him hostage.” Mike’s tone was flat as he thrust the accusation home.

Unable to deny the nature of my tactic, I held Mike’s stare until he turned away. “If you can suggest a way to survive the night that won’t stain your conscience, I’d love to hear it. Otherwise, I’m going to do my bloody job and hold the good doctor accountable for his crimes. Hopefully, having him in hand will give Robinson and Dusk Patrol cause to reconsider attacking us.”

“Would the Society approve?”

“That’s my concern. No doubt I will face a court martial once this is over, but I’d willingly stand trial if it means exposing Project Harker and bringing Petersen and Robinson to justice.” Fortunately, my voice didn’t quaver and betray just how shaky a foundation my resolve rested upon.

Despite knowing it was pointless to second-guess myself, I spent the ride back to Clarion in internal debate. I was still at it when we parked in front of Dr. Petersen’s house. The setting sun threw long shadows across the street. With a knock on the door, I committed myself.

The door opened silently on well-oiled hinges at my touch, and I drew my sword. There was no way Dr. Petersen would leave his door like that, even in a quiet little town like Clarion. “Stay close, and keep the shotgun ready.”

“Right.”

Working one room at a time, we cleared the house. Perhaps I should have ordered Mike to wait outside, but he was safer with me and this would be good training for him. It wasn’t until we opened the stairwell leading to the attic that the rusted iron stink of spilled blood hit us.

At the far end of the attic, we spied Dr. Petersen sprawled on the floor. A Dusk Patrol soldier on his knees hunched over him, and a soft lapping told me everything. That soldier was feeding, and if the doctor’s blood still flowed, he might not be beyond help. With a piercing cry, I threw myself forward, driving all my weight and strength behind the point of the sword I held with one hand on the hilt while carefully grasping the base of the blade with the other.

As I hoped, the soldier stood and faced me, impaling himself on my sword. It bit deep into his belly, and drove him to his knees as the blade glanced off his spine. The arterial red welled at his mouth as his entire blood supply poured into his abdominal cavity from the artery I had sliced open. He wasn’t likely to cause further trouble.

Regardless, it never hurt to make sure. Kicking him onto his side, I retrieved my sword before taking his knife. An extra blade might come in handy.

There was a soft click as Mike thumbed his rifle’s safety off. “Worry about the doctor. If this asshole moves, I’ll blow his fucking head off.”

Glad the kid stepped up, I turned to Dr. Petersen. “Doctor, can you hear me?”

“Oh, Natalie. I’m glad you’re here. Or were you Nancy?” Petersen slurred the words, which combined with his confusion over my name suggested he had suffered a concussion before being bitten.

Checking him over, I found no other visible injuries. “Mike, let’s have some light.”

“Sure.” He kept his rifle trained on the soldier as he found a switch.

Observing Dr. Petersen’s eyes as the room brightened, I relaxed a little as both his pupils contacted to the same size in response to the glare of the ceiling-mounted light. Still, it was best to be careful, so I flipped Petersen the bird. “How many fingers am I holding up?”

“One. And that’s a rather rude gesture for a young lady, Adversary Bradford.”

“It’s Bradleigh, doctor. Naomi Bradleigh. And ask your subordinate if I’m a lady.”

Petersen turned his head, his eyes widening. “I’m surprised you didn’t kill him.”

Not about to admit that I couldn’t bring myself to finish the job, I grinned at the doctor while taking the first-aid kit from my belt. “Like you, he’s more useful to me as a prisoner than a carcass.”

I glanced at Mike, who held his shotgun at the ready in case anybody came up the stairs, before patching Petersen’s shoulder. He glanced back at me, and shook his head. “He’s a doctor, ain’t he? He can go heal himself.”

Ignoring Mike, I finished the job and packed my kit back up. When I was done, I patted the doctor’s leathery cheek. “Think you can manage to make it downstairs with us? I mean to have an intimate chat with you, and I’d rather not do it here. Besides, we need to secure your house.”

“You said something about me being a prisoner. Why?”

That got a chuckle from Mike. “I’m terribly sorry, Dr. Petersen. I must have neglected to inform you that you are under arrest for crimes against humanity.”

## Track 51: Metallica — “Disposable Heroes”

"Crimes against humanity?” Dr. Petersen still slurred his words, though not as badly as before, and reduced what would have been a cry of protest to a bleat. “Adversary Bradleigh, I have no idea how you could possibly suspect me of any such -”

“Project Harker.” The words knocked the wind from Petersen as surely as if I had punched him in the gut. But before I could continue, Dr. Petersen needed to be informed of his rights. “Colonel Henrik Petersen, you are under arrest for crimes against humanity not limited to unethical experimentation upon human beings without informed consent. You have the right to remain silent-”

“We had the Miranda Warning in the Commonwealth, Adversary. I understand my rights.”

Favoring Dr. Petersen with my sweetest smile, I patted his uninjured shoulder. “I’m sure you understand your rights under the old regime, doctor, but we’ve updated the classic warning. You have additional rights, of which you might not be aware, and because you have suffered a concussion, I absolutely must notify you of them and confirm your understanding.”

“Yes, yes. Representation by an attorney, network access for preparation of my defense, access to all evidence against me, and humane treatment while in custody.” Petersen was done slurring his words, and now spoke in a clipped, impatient tone. “I have a concussion, not Alzheimer’s disease.”

“Excellent. I shall recall that should you attempt to evade a question by claiming a gap in your memory. Now, do you think you can walk?”

Petersen was a bit wobbly on his feet at first, but waved away the hand I offered to help him. He soon steadied himself, and reached the stairs before I did. “Do you know a safe place where we can talk, Adversary?” He glanced at the soldier struggling against the knives pinning him in place. “My house is not defensible, and it should be obvious that I’m of no use to you as a hostage.”

The Lonely Mountain was tempting, but instead of using one human shield, I’d be using dozens or hundreds. I couldn’t arrest them all, and I was already right up against the ethical line as it was. Fortunately, it didn’t seem as though Mayor Collins had discovered the secret under Gibson Hacker Supply. “I know a place.”

The stocky, dark-haired deputy guarding the entrance favored me with a suspicious glare, and rested a sun-browned and heavily callused hand on the hilt of her service gladius. Her badge identified her as Alvarez. “Sorry, Adversary, but there was another murder on the premises today. Mayor Collins is dead.”

Hearing the man was actually dead was a relief. The last thing I needed was an enraged and technologically augmented public official who had survived having a shotgun emptied into him at close range, and now had a legitimate reason to dislike me. Regardless, it simply wouldn’t do to tell Deputy Alvarez I knew anything. It was easier to feign ignorance than to pretend I wasn’t glad the vicious son of a bitch was dead. “That’s terrible! Does he have somebody who can take over until the election and maintain order?”

“Yeah. Gets worse, though. Whoever did the job used a shotgun, and went for the overkill, but cleaned up after themselves. We couldn’t find a single shell casing.” Deputy Alvarez shook her head. “Damnedest thing. And somebody took a hammer to that big-ass computer in there, too. But no sign of theft.”

“Sounds like a professional hit. Mind if I take a look inside? I might spot something.”

Alvarez shook her head. “Sorry, Adversary, but I can’t let you inside without notifying the Sheriff.”

I should have expected this. Fortunately, my implant provided a function that let me get the IP addresses of others around me. Once I had the deputy’s, I passed it to Malkuth. 「Can you spoof Sheriff Robinson’s IP address and tell Deputy Alvarez to let me and my prisoners through?」

「Sure.」

“Who are you talking to, Adversary?”

Alvarez wasn’t quite ready to draw, but it wouldn’t take much. If I didn’t defuse her suspicion, I’d have a fight on my hands. “Sorry, Deputy Alvarez. I just contacted the Sheriff myself. You should hear from him directly.”

She took her hand from her sword’s hilt and pressed two fingertips to her ear, just as I did to indicate an incoming call. Alvarez shrugged and stepped aside. “Just got word from the Sheriff, Adversary. You and Dr. Petersen are clear.”

“Thank you. What about Mr. Brubaker?”

Alvarez glanced at him. “Why do you need the kid?”

“He’s my prisoner. I have to keep him with me until the Society sends somebody to pick him up.”

“But he could tamper with the-”

“If he tampers with anything, I’ll kick his arse so hard, he lands in London. Do you have any other questions, Deputy?” Alvarez might be doing her job, but the sun was setting, and I hadn’t fully eliminated Dusk Patrol. The ones I hadn’t arrested could still surround me on the street.

Her grip tightened on the hilt of her gladius. “What if I refused to let any of you through?”

“Drawing your blade first will be your last mistake.” My hands were already on my sword, ready to take first blood as soon as the blade cleared the sheath. I was taller, and both my arm and blade were longer. Whatever strength Alvarez possessed would be useless if she couldn’t reach me. If she rushed me, I’d run her through. “Think it over.”

Evidently she did, for after a moment in which she glanced at Brubaker and his shotgun, she let go of her sword’s hilt and shrugged. “I don’t get paid enough for this shit.”

Neither do I, but there was nothing for it but to finish the job. “Thank you, Deputy. We won’t be long.”

Alvarez nodded as she stepped aside to let us into the shop. Once we had locked the door behind us, Petersen surprised me. Striding directly to the wall that concealed the hidden basement entrance, he pushed aside the framed poster of an angry-looking African man wielding a pistol and saying, “POSIX, motherfucker! Do you implement it,” and pressed the button Cat Tricklebank used to open the door I’d thought a secret.

He glanced over his shoulder. “You kids coming?”

Once we were safely downstairs with the door shut, I asked the obvious question. “How did you know about this entrance?”

Petersen shrugged. “I had it built during Nationfall. Normally we’d use the engineer corps for this sort of work, but the boys in Dusk Patrol proved admirably capable. They dug the tunnel leading here from Fort Clarion to transport Tetragrammaton Zero in secret.”

“Why are you telling me this? You realize everything you say will be used to prove your guilt in court, right?” That was assuming his attorney didn’t convince the judge that despite my efforts, Petersen wasn’t competent to claim he understood his rights because of his injuries.

Brubaker stared at me and texted. 「What the fuck are you doing? If he wants to hang himself, just give him more rope!」

“It doesn’t matter.” A note of weariness entered Petersen’s voice as he sat down. “I am guilty, but I probably won’t live to stand trial. The Phoenix Society will soon send an arms control unit despite my contact’s efforts to delay their arrival, and once they do, they’ll finish the work you began, Adversary Bradleigh. And before that happens, Sheriff Robinson will kill me so that I cannot dispute his account of events.”

Before I could say anything, Brubaker spoke up. “What about the Mayor?”

“Robinson would eventually have done the same with Mayor Collins. You did his work for him.”

The conclusion seemed pretty obvious. “With you and Collins unable to speak up, Robinson would be able to pin everything on you two.”

“And escape justice for his own crimes in the process.” A weary smile flashed across Petersen’s weathered face. “In fact, you already thwarted his first attempt on my life.”

Robinson sent that Dusk Patrol soldier? It certainly fit. A significant number of them seem to have thrown their lot in with him via Corporal Seward instead of remaining loyal to Sergeant Renfield and Dr. Petersen. And who did I have arrested? Renfield and many of his loyalists. That bastard Sheriff was probably having a fine laugh at my expense. “He’s been manipulating me from the beginning, hasn’t he?”

“I don’t think Robinson has it in him to consciously manipulate people. But he’s a cop, and good cops are like good officers. They know better than to interrupt an enemy while they’re making mistakes.”

Small comfort, that. Looks like I’ve exhausted any margin for error I might have had. “I think you had better start from the beginning. Project Harker and Ian Malkin from the AsgarTech Corporation sound like a good spot.”

Petersen startled at Malkin’s name, as if surprised I knew it. Or was he afraid the devil might pop up at the mere mention? “So, you managed to get into my files after all. How?”

“Friends in high places.” No need for further explanation if Petersen wasn’t already aware of the ten AIs who served the Phoenix Society. “I know all your nasty little secrets, doctor.”

Brubaker scratched his head, the friction of fingernails against scalp loud in the basement. “Naomi, what do you think Robinson will do if he learns you have access to that data? Who’s going to protect those computers?”

Questions like that are what I get for not telling Mike everything. He doesn’t know that Tetragrammaton is already uploading compressed archives under the system account. A quick check with my implant showed that both transfers were more than halfway done. A couple hours more, and the data would be forever beyond Robinson’s control. “The computers themselves don’t matter. It’s the data that counts, and I’ve already sorted that out.”

“You arranged for the Society to grab the data already?” Petersen studied me a moment. “A wise choice.”

“The Society isn’t grabbing the data. I’m sending it to them.” Not to mention Port Royal, but I kept that bit of information up my sleeve for now. It was my ace in the hole, which I would only play if the Society tried to bury the evidence and cover up what happened here. Likewise, if they tried to make me an unperson.

Instead of turning defeated by this revelation, Petersen’s expression brightened. He sat straighter, as if an unseen weight fell from his shoulders. His smile was that of a man who no longer had to fear the worst-case scenario because it had already happened, and he was still alive. “If you already have the data, then Robinson can no longer buy my cooperation with his silence. I might as well tell you everything.”

## Track 52: Megadeth — “Almost Honest”

Stunned by Petersen’s words, I couldn’t help but stare at him a moment. “Are you absolutely sure you want to confess? If you had an attorney present, and they were at all competent, they’d be yelling at you to shut your bloody gob right now. The Society might use your words as evidence against Robinson if you implicate him, but they won’t seek a more lenient sentence for you because you did their work for them.”

Mike gave me an exasperated look. “He doesn’t have a fuckin’ lawyer, so why are you giving him advice? Don’t you want him to prove his own guilt?”

Before I could answer, Mike got up and took a position by the stairs. If somebody came down, he’d be at risk, but it was also a good place to ambush intruders.

Petersen chuckled. “It seems, Adversary, that you don’t trust your own superiors to do right any more than I trusted mine at the end.”

The doctor was right. My superiors do appear to have mistaken me for some exotic species of mushroom, given their recent tendency to keep me in the dark and feed me bullshit. And if Petersen had contacts in the Society who have made it their business to hinder me, I had all the more cause for distrust. Unfortunately, Mike’s objection wasn’t wholly unreasonable. Duty demanded that I prove Petersen’s guilt by whatever legal means at my disposal. In the meantime, Petersen was innocent until proven guilty in a court of law, and I was obligated to honor his rights in the interim.

My position was hardly enviable, but there was nothing for it but to do my job. “Dr. Petersen, are you sure? Mike’s right, but I’d be remiss if I didn’t offer a final warning. Everything you tell me will be recorded and used against you in court.”

“And there’s nothing I can say to save myself, except to take the Fifth.” Petersen gave a small, bitter laugh. “Good thing for you I’m not interested in saving my own hide. What I want is a deal. Let me take full responsibility for everything that has happened in Clarion. My men were only following orders.”

“I think that’s the first time I’ve seen somebody invoke the Nuremberg Defense on their subordinates’ behalf.”

Petersen shook his head. “It’s called command responsibility. If I was merely aware of atrocities committed by my men, and did nothing, I would still be criminally liable under the Medina standard. But I ordered my men to survive, conceal their existence, and protect Fort Clarion by any means necessary. Moreover, I may be the most senior officer involved in Project Harker who’s still alive. Somebody has to be held accountable. I’m an old man, so it might as well be me.”

Because I agreed with his reasoning, I refrained from mentioning the fate the Phoenix Society meted out to other war criminals who escaped justice until old age. If Petersen knew he might be condemned to involuntary rejuvenation before serving a life sentence, and face a century in prison instead of the five or ten years he expects to serve before dying, it might break his resolve. Despite my duty to respect the rights of an individual not yet proven guilty, the last thing I wanted was for Petersen to reconsider confession. In fact, I said nothing at all.

Petersen did not speak again for several minutes, and we sat in silence as he composed his thoughts. “When Dr. Ian Malkin of the AsgarTech Corporation first approached me about testing Dusk Patrol for what he called the ‘asura potential’, I was delighted. Dusk Patrol was a joint innovation on my part and that of Sergeant Major Renfield, a unit composed entirely of CPMD-positive soldiers who would train to enhance the capabilities unique to them and develop tactics that would exploit their strengths to achieve decisive victories.

“Despite the unit’s proven effectiveness, my superiors were concerned because the root of their espirit de corps was what separated them from the rest of humanity. The brass was concerned about the potential for separatist sentiment to take root in the Army, beginning with the unit, but couldn’t order it disbanded because news of their successes had already made it to the President. So, they tried to do the next best thing and get the unit’s members killed.”

“You figured Dusk Patrol’s luck would eventually run out, and wanted to improve their chances?” It seemed a reasonable question given what Petersen had told me thus far.

Petersen nodded. “Exactly. When Dr. Malkin showed up and started talking about the men having some kind of ‘asura potential’, I recognized the opportunity before us.”

Recalling Petersen’s suggestion that Malkin approach the upper brass first, so that the idea would seem to come above the colonel’s pay grade, I gave him a gentle prod. “But you must have known that if it all went wrong, the men would turn against you along with the brass. You’d be caught in the middle.”

“Which was why I suggested Malkin pitch it to General Quinn. The men already disliked her, and I thought to use that to further harden them.”

“So, Project Harker happened, but you weren’t part of the research team. Why was that?”

Petersen shrugged. “I began my medical training once Project Harker got rolling, but did not complete it until after Nationfall. By that point, I might argue that the damage was done.”

So he might, only the damage continued afterward. “Why order the men to remain in hiding after Nationfall?”

“Dr. Malkin ultimately viewed Project Harker as a failure, for reasons he never shared with me, and I was afraid the men would not be able to rejoin society unless I could find a way to reverse the changes.”

Unable to help myself, I shook my head in disbelief. “You thought you could undo an experimental treatment designed to evoke some kind of asura potential that you’ve yet to describe or explain? How?”

“I had to start by understanding the potential itself. To do so, I persuaded my contacts in the Phoenix Society to back the resettlement of Clarion. I soon determined by comparing the men of Dusk Patrol with the CPMD-negative majority of the new population that the asura potential is tied to CPMD.”

“Are you telling me that everybody who’s CPMD-positive has this thing?”

“Everybody I’ve tested. Even you. It varies between individuals, however, and follows a normal distribution. It’s a heritable trait, and a child of two high-potential parents will most likely have a high potential themselves. Yours is two standard deviations above the mean, if you care. I was able to test you when you came in to have that wound to your side treated.”

Between this and my red eyes marking me as some kind of half-demonic hybrid, I was really starting to feel like a freak. “I haven’t had time to fully examine your research. What does having a high asura potential mean?”

“Good question. Those with the highest potentials were best suited to the treatments developed by Project Harker. However, they were also prone to certain side effects and exhibited unusual phenomena.”

“Such as?”

Petersen glanced around, as if checking for additional observers, and leaned forward. “One subject broke free of his restraints while repeatedly screaming, ‘laissez-faire’. When orderlies tried to subdue the subject, they couldn’t get within a meter of him. They insisted some kind of barrier kept them from getting closer. The subject died soon afterward of starvation. His body had somehow consumed itself while producing the barrier phenomenon. Furthermore, cellular analysis revealed anomalies in the subject’s mitochondrial DNA.”

So, the test subjects had wonky mitochondria? That didn’t sound good. If I still had my college biology down pat, mitochondria are the little critters inside our cells that convert nutrients into adenosine triphosphate, the basic fuel on which our cells depend. Without them and other features of eukaryotic cells, complex multicellular life probably wouldn’t exist. “What sort of anomalies? Did the subjects suffer from some metabolic disorder?”

Petersen raised an eyebrow at my question, but also smiled. “Excellent question, but the subjects’ medical records contained no indication of metabolic problems. Moreover, the mDNA contained genes for neurotransmitter receptors, which were activated in the mitochondria within the subjects brain and nerve cells.”

“Are you telling me these people had brains capable of direct mitochondrial control?” And what would be the benefit if this were the case? Suppose I could command the mitochondria in my brain cells to double or triple their normal energy production. Would that alone let me think faster? “What good is that?”

“I don’t know, but there was some connection between the neural-mitochondrial link and the psychokinetic phenomena certain Project Harker subjects exhibited.” Petersen fell silent for a long moment, and seemed to study me. “When I tested you, I not only checked for asura potential, but for this other trait. You possess both. Under the proper circumstances, you too could exhibit paranormal abilities.”

That’s a bloody cheerful thought. If pushed hard enough, I might go full Carrie and then die once my body has consumed itself to power whatever ability I end up manifesting. What good is that? “I appreciate the warning, doctor, but we’re off on a tangent again. This isn’t about me. This is about your involvement in Project Harker and subsequent unethical research. I take it you engaged in breeding experiments involving the local CPMD-positive population to further your understanding of the asura potential, and longitudinal studies without obtaining informed consent.”

“I did. I performed the same experiments on the CPMD-negative population as both a control and a cover. Since I did it to everybody, I could sell it as free genetic counseling.”

A snarl and a sudden thud against a table pulled my attention to Mike, who had slammed down the book he had been reading. He pointed an accusing finger at Dr. Petersen. “I knew it. I fucking knew it. Do you have any fucking idea how much misery you caused, you lying sack of shit?”

Despite Mike’s angry display, Dr. Petersen remained calm, his voice drily amused. “Considering that the people of Clarion tend to come to me for mental health referrals, I know full well how much misery I caused. It is one of the reasons I am here, explaining myself to your new friend.”

Thank God he didn’t say ‘new girlfriend’. It probably isn’t easy to maintain control of an interrogation when you’re blushing as deep a red as your eyes.

As if guessing at my thoughts, Petersen flashed a smile at me before continuing. “I should let you know I experimented on regular humans for more than one reason. Sheriff Robinson was blackmailing me, as I mentioned before. In exchange for his silence, I had to find a way to transfer the asura potential to humans and safely activate it. I also needed to ensure that the gene therapy didn’t cross the Weismann barrier and affect the germ-line as well as somatic DNA.” He paused, and pride brightened his expression. “I succeeded, as you no doubt learned from your encounter with the late Mayor Collins.”

If Mike and I hadn’t already faced the augmented Mayor Collins, if not for the crazy shit we had already seen, I would have dismissed Dr. Petersen’s claims as the posturing of an old failure. It was tempting to do it anyway, because of the implications. If Petersen had done it to Collins, then he could have done it to others. He might even have done it to himself.

Before I could question the old doctor further, a boom echoed from the door at the top of the stairs. Mike turned away from the CCTV displays. “Naomi, we got a problem.”

## Track 53: Within Temptation — “And We Run (featuring Xzibit)”

Gently pushing Mike aside, I checked the screens myself. His saying we had a problem was an understatement worthy of Shakespeare. Sheriff Robinson was there, and his broken arm looked like it worked just fine. He waved his hands as if conducting a sledgehammer symphony, directing his deputy to gut the interior of the shop above us.

It wasn’t the subtlest method for finding a hidden passage, but it was effective. Like the man said: when in doubt, use brute force.

If the Sheriff’s presence and that of his deputies wasn’t sufficient cause for alarm, that prick Robinson had also brought along Dusk Patrol. One of them offered him a megaphone. Not that I needed an audio feed to figure out what he meant to say. It was most likely something along the lines of, “If we have to come down there after you, we’ll beat the shit out of you and tell everyone you were resisting arrest.”

Turning off the displays, I favored Dr. Petersen with my hardest stare. “When they find the way down here, they’ll come in shooting. Hell, they’ll probably chuck a grenade or three down the stairs first. I certainly would.”

Petersen nodded. “So, you would have me reveal the underground passage to Fort Clarion.”

Mike glanced upward, wincing at a clang of metal on metal. They must have opened the outer door, and were now trying to break down the inner one. If they fail to get through that 50mm slab of steel with hammers, and couldn’t find welding tools that would let them cut through, I suspected their next step would involve high explosives. Being around for that might prove unpleasant. “The only other way out is to force our way through those assholes upstairs. How valuable a hostage do you think you are, Doc?”

A shrug from Petersen was the only answer Mike got. The doctor rose, winced at another ringing blow to the door upstairs, and pointed at a shelf. “You kids need to lift that out of the way.”

“Take that side. On three.” Mike grabbed the other side and braced himself as I counted. It was heavy, but Mike and I managed to heft it up. The question now was where to put the damn thing, but I had an idea.

With a smile that probably resembled a rictus, I cocked my head in the general direction of the clangs. “Let’s put this in front of the stairs and then move some furniture to prop it up.”

Minutes later, we had the barricade rigged up. In the meantime, Petersen had opened the tunnel. He must have found a cache inside, for he came out bearing three rifles. “Here. The passageway is almost twenty kilometers long, with concrete barricades for cover placed throughout. A fighting retreat is our best option.”

We each took a rifle. Since I had never been down here before, these weapons weren’t part of the ordnance catalog I had compiled for the arms control team coming tomorrow. “Got spare magazines?”

Petersen nodded, and handed them out. “We’ll find more ammo along the way. Rations, too.”

Having worked with Adversaries who once served in pre-Nationfall militaries, I shuddered at the thought. While “Meals Rejected Elsewhere” wasn’t what the acronym meant, it was the one they used. Decades-old MREs would most likely kill us before the enemy could. Mike’s disgusted expression suggested he harbored similar suspicions. “I think we’ll pass on the rations.”

For some reason that amused Dr. Petersen, for I heard a soft chuckle from the old man as he passed me and stepped into the tunnel. Picking up a small case of grenades, he pressed Mike into service. “Hold on to this. It’ll come in handy when they catch up.”

We ran through pitch darkness, or so it seemed until my eyes adjusted enough make out the faint blue-green glow radiating from fungi growing along the tunnel walls. The glow steadily brightened as my eyes adjusted further. A wrathful voice resembling Robinson’s echoed at our tails. “Bradleigh, you bitch! Get your ass back here! You are under arrest.”

Definitely Robinson. Turning back, I cupped my hands around my mouth to amplify my voice as best I could. Rather than shout a crude taunt, I sang in a high clear tone. “Sheriff Robinson thought to catch a white lark. He went home frustrated ‘cause he feared the dark.”

The couplet probably wouldn’t work if written down, but I was more concerned with emotional impact than scansion. It got a laugh out of Mike, and an enraged shout from Robinson. Must have struck a nerve.

Heeding my instinct to duck might well have saved my life, for the burst of gunfire shredded the air above me before I heard the gunshots. Closing my eyes and averting them to avoid having the muzzle flash burned into my retinas, I returned fire.

A howl of pain mingled with rage followed us as we fled further down the tunnel. I must have hit somebody back there, but a headshot was too much to hope for.

“I got thirty men with me, Bradleigh, and they all want a taste of you. What do you think of those odds?”

“Sounds like a target-rich environment to me.” To emphasize my point, I fired another long burst behind me as I kept running. An agonized shriek suggested I had scored another hit. Hopefully I blew Robinson’s balls off.

I took the lead as we approached the first of the barricades, and used near-field comms to link the others in a secure relay chat. 「Barricade ahead on the left.」

Petersen wove around the waist-high concrete barrier and took cover behind it. Mike and I joined him for a breather after vaulting over the top. Some idiot sparked a flashlight, giving me a sense of their distance, and I rewarded their foolishness with a burst from my carbine. Despite the rage in Robinson’s voice, they advanced at a methodical pace, checking every meter as if Mike and I had found time to set traps.

Too bad I didn’t have any shriekers handy. 「Doc, where’s the next cache?」

「Two barricades ahead. You out of ammo already?」

「Got any Mandrakes hidden? If anything’s going to slow them down…」 I’d probably maim a few of them, but that didn’t stop me from setting traps in the Fort Clarion armory. If Dusk Patrol wanted war, they were welcome to it. Likewise, for Robinson’s deputies. They too had a choice between upholding the law and obeying a man who had set himself above the law, and they made the wrong choice.

「No mines down here. Sorry to disappoint you.」

「No worries.」 Indulging in a bit of reconnaissance by fire, I squeezed off another burst. The ensuing shout of enraged pain was closer, and laden with the sort of words not spoken by men who respect women. Were Jacqueline here, she might have dropped some quip about my victim kissing his sister with that mouth. 「You ready to move on, Doc? Your troops are closer.」

「You could leave me behind.」

「We all have dreams. Wake up and move your arse.」 The old bastard let out a weary sigh, but complied with acceptable alacrity. Though in good shape for his age, he was still much older than Mike and me. We were thus forced to hold back and match his pace, which recalled to mind the jokes about how one goes about outrunning various wild animals with a taste for human flesh. You didn’t have to outrun the animal; you needed only to outrun your companion.

We stopped again at the next barricade. Our pursuers had gained ground, and I needed to do something about that. Fortunately, I had an idea. 「Mike, give me one of those stun grenades.」

He pressed two into my hand, and squinted his eyes shut as he covered his ears. Petersen did the same as I pulled the pin and hurled it back the way we had come. The fuse ran long enough for me to hear it skitter across the floor as I took cover. Despite my distance, the blast was still uncomfortably loud, though not as bad as my rifle when I followed up by emptying my magazine in short bursts.

Despite my liberal use of gunfire, nobody on Robinson’s side had fired back. I figured some return fire would be in order as I ducked behind the barrier again to reload, but instead of shots or a grenade hurled my way, all I got was more of the Sheriff’s raving. “This tunnel’s gonna be your grave, Naomi! You hear me? Your fucking grave!”

Promises, promises. If that fuckwit had the ability or the nerve to make good on any of his threats, he would have done so already. But he either didn’t bring firearms with him, or some factor unknown to me prevented him from returning fire thus far. I was about to reward his cheap talk with action when a gunshot rang out from Robinson’s direction. Five more followed. Only a revolver, most likely, but it suggested the Sheriff had a pair after all. I threw the other stun grenade Mike had given me just to show Robinson I still cared. 「Come on. Can you still run, doc, or do I have to carry you piggyback?」

「I can manage another five hundred meters. After that, you won’t have to worry about me.」

Rather than waste time asking Petersen what he meant, I ran and trusted the men to follow. Follow they did, so I was first to behold what awaited us. I couldn’t believe what I saw at first, and dismissed it as a wishful thinking or a hallucination born of eyestrain. Petersen pulled ahead of me as I slowed to a surprised stop, climbed into the jeep, and started it up. Squinting against the sudden radiance of the taillights, I jumped in. 「Why didn’t you tell me there was a bloody jeep down here?」

「Wasn’t sure if still was.」 Petersen gunned the engine as Mike clambered aboard and sat beside me in the back. 「Wasn’t even sure if it had gas after all the round trips hauling parts of Tetragrammaton 0, but looks like we got a quarter tank left.」

The jeep’s engine was loud, but not so loud I couldn’t hear Robinson yelling behind me. Something about how wheels weren’t going to save me from whatever the hell it was he fantasized about doing to me. Rather than bleat over the engine, I turned in my seat and saluted with an upraised middle finger.

## Track 54: Dream Theater — “In the Presence of Enemies, Part 1”

It would have been nice to have ridden the rest of the way to Fort Clarion in comfort, but that’s not how matters worked out. With just over a kilometer left to go if my estimate of the distance had been correct, the jeep ran out of fuel. The engine sputtered, running on the last of the fumes in the tank as Dr. Petersen shifted to neutral. We coasted to a dead stop, rolling farther than I expected.

Straining my ears in the sudden silence, I heard nothing, but that didn’t mean that Sheriff Robinson and his men weren’t still behind us. ⎡Think you can manage the rest of the way on foot, Doc?⎦

⎡Doesn’t look like I have a choice. Got a plan for when we get there?⎦

⎡Aside from keeping us all alive?⎦ It didn’t seem wise to plan in any greater detail than that. Besides, the less I told Petersen, the harder it would be for him to betray me if he had the opportunity and inclination. ⎡Leave the headlights on, and let’s go.⎦

The jeep lit our path as we resumed our journey on foot. The white radiance of its headlights slowly attenuated with each step, allowing us to gently re-acclimate to the dark. It was better than being plunged into darkness and having to waste time standing around blinking like grotesque flightless owls. With any luck, the jeep would also slow down Robinson and the others by causing them to suspect an ambush where none existed. Why should I be the only one looking over my shoulder?

We soon reached the other end, which was what I assumed the cold steel shutters guarded. I crouched to grasp the handle, so I could try lifting them, but they rose quickly of their own accord, and lights from the other side blinded me for a moment. Training took over, and I raised my rifle.

“Damn, Naomi. It’s just me.”

I knew that voice, but what the hell was he doing here? “Renfield?”

“Sorry about the sudden light. Let’s see if this helps.”

Before I could say anything, his lips were on mine. He certainly kissed like Renfield, but this was neither the time nor the place. I took a step back, and raised my rifle again. “How the hell did you get down here ahead of us? Why aren’t you with Robinson?”

“I broke out of jail. Saw the commotion in town, but didn’t see you, and figured you found our tunnel. Nice of you to look out for the old man, by the way.” He saluted Dr. Petersen. “I was against having you eliminated, Colonel, but Robinson insisted, and most of the guys would rather follow him and Corporal Seward. They’re tired of living underground.”

Petersen returned the salute. “Thanks, Sergeant.” He looked backward, into the dark distance whence we came. “Hard to blame the others. I should have helped you come back to the world sooner, but I -”

He was still talking, or at least trying to, but no words came out. Instead, blood poured from between his fingers as he clawed at his own throat. The whipcrack of a gunshot soon followed. Shielding Mike with my own body, I yelled at Renfield. “Move your fucking arse!”

He did, but carried Petersen’s corpse with him. Not that I could fault him. I wouldn’t leave a fallen Adversary behind, even if getting them off the battlefield stood to cost me my life.

The shutter rolled back down as Renfield pounded a button set into the wall. Despite his burden, he led us up the stairs and into Fort Clarion’s underground. Encouraging Mike to stay close to Renfield, I trailed behind to guard their backs. At each corner we turned, I stayed behind a moment to ambush any pursuit, but none followed.

Had the shutter stopped them? How long would it remain an obstacle to Robinson and his merry band? Whatever the answer, it wouldn’t be long enough.

When I caught up with the guys, they had placed Petersen’s body in the meat locker where Renfield had left me hanging no more than a couple of nights ago. Somewhere along the way they found a body bag and stuffed him inside. Mike waited next to it, but Renfield was nowhere to be found. “How long have you been alone?”

Mike shrugged. “Only a couple of minutes. Renfield said something about finding some stuff he took from you.”

Renfield soon returned with a bag over his shoulder and two swords stuck in his belt. Lowering the bag to the floor, he pulled the long sword from his belt and held it out to me. It was one of the blades Nakajima lent me. “I found the gear I took off you before. If you’re going to fight Dusk Patrol, I figured you’d want it back.”

“Not going to stop me? I might have to kill some of them.”

Renfield shook his head. “Fuck ‘em. This is war, and when they killed Petersen, they killed our best shot at going back to the world. Did you know he was going to confess, and take the fall for everything?”

Though I could have told Renfield that Project Harker had practically been Petersen’s idea from the start, and that Dusk Patrol’s subsequent isolation had been a further betrayal on their leader’s part, I couldn’t see the point in doing so. What would Renfield have done with the knowledge, since his former commanding officer was beyond all confrontation? It seemed better to let him believe the colonel had been looking out for his men to the end. “He did confess, and claimed command responsibility.”

Renfield nodded, and prodded the bag. “Got your armor in here.”

“Thanks for keeping it safe.” Not sure what good it would do me. Though the turtleneck I wore under my jacket was thin, the gauntlet I tried on wouldn’t lock into place. The catches wouldn’t engage. “Shit. I probably need that damn undersuit Nakajima included.”

Mike tried on the gauntlet himself; it wasn’t a perfect fit because his hand was a bit too small, but it wouldn’t snap shut for him, either. “Must be a security feature.”

“The hell with it.” Packing the useless armor away, I tried one of the Japanese swords. The draw was as smooth as my usual sword, so I slung my sidesword across my back and belted the katana at my hip along with its shorter companion sword. I’d be at a disadvantage because I didn’t regularly train with these blades, but if we could find a safer place, I might be able to get in a few minutes of practice. It would be better than nothing. “Renfield, what’s the most defensible location on the base?”

Seeing him take time to think it over made me nervous. A soldier like him should be ready to answer this question before I was finished asking it. Shouldn’t he? After another moment, he shrugged. “Your best bet is one of the watchtowers. It won’t give you much of an advantage, but you’ll have elevation, a wide view, and the main approach is a narrow stairwell.”

Seriously? We were up there only a few hours ago. But if we had stayed, I wouldn’t have gotten Dr. Petersen’s confession.

Regardless, I might have acquiesced for lack of a better idea if Mike had not voiced his own objection. “Wouldn’t they expect us to use conventional tactics?”

Renfield glared at him. “You got a better idea, kid?”

“Yeah. We’re outnumbered, and can’t afford to face Robinson and your buddies head-on. If we stay in one place, they’ll surround us. We have to stay mobile.”

“How?” Mike had a good point, but even if I called my motorcycle back to me, I wasn’t trained for mounted combat, and my swords probably weren’t intended for use in such conditions.

Renfield scratched his head a moment. “Shit. We don’t have fuel for any of the vehicles.”

Nor did we have time to piss about. If Robinson and his men hadn’t gotten past that shutter in the tunnel, they would soon. “Mike, I want you in that tower with a rifle and plenty of ammo. You can cover me while I get in Dusk Patrol’s faces and keep the bastards busy. Aim for center mass. We’re just trying to hold out until the morning.”

Mike nodded. “What about Renfield?”

Good question. If I pitted him directly against his brothers, would he fight, or would brotherhood outweigh his anger over Dr. Petersen’s death? Better to avoid the question. “Sergeant, I won’t ask you to fight Dusk Patrol beside me. Instead, can I count on you to watch Mike’s back?”

Renfield gave me a long, calculating look. “You realize you’re leaving your witness alone with me, right? I could take him out, and then shoot you from the tower the way Petersen wanted from the beginning.”

He could, but I don’t think his pride as a soldier would permit it. Right now, we share a common mission. Renfield won’t do anything to fuck it up, but all bets are off afterward. “You won’t do anything of the sort until after we’ve nailed Robinson. It would be unprofessional.”

While I waited for him to think it over, I followed Mike’s lead and checked my rifle. Since the magazine only had a few rounds left, I swapped it out for a fresh one. Last thing I needed was to run out of ammo right after the shooting starts. It would be embarrassing. “Well, sergeant? Can Mike and I depend on you?”

Renfield nodded. “Yeah, but when this is over, we’re going to talk.”

## Track 55: Dream Theater — “The Dark Eternal Night”

Renfield must have heard our pursuers as well, for his expression hardened. He glanced at Mike, who was checking his rifle with a frown that suggested deep concentration. Grabbing Mike’s shoulder, he gave the younger man a hard shake. “Hey, kid. We gotta move.”

“I’m ready.” Mike stood and slung his rifle over his shoulder. “Naomi, you’d better take point so those shriekers don’t go off in our faces.”

“Right.”

“You deployed Mandrake mines in the tower?” Renfield trotted beside me as we headed for a stairway to the surface. The way he winced as he mentioned the devices suggested a bad experience with them in the past. “Christ, I hate those fucking things.”

“Trouble deactivating one without the phone used to prime them?”

“Yeah. Lost my legs when the fucker went off.”

“Now you’re just taking the piss.” He had to be having a bit of fun at my expense, given that I had gotten my hands all over his naked body. If his legs were artificial, then they’re a bloody work of art. “You honestly expect me to believe you had your legs blown off?”

Renfield shrugged mid-stride. “One of the few upsides to Project Harker. I grew ‘em back. Took me most of a year, and you wouldn’t believe how bad the itching was. Nurses had to cover ‘em in gauze to keep me from clawing the new skin to ribbons.”

My turn to wince. “That must have been rough.”

“That wasn’t the worst. By the time it was over, I had a hell of a Demerol jones. Not much else to do when your locked in infirmary like a goddamn science project.”

Though I should have saved my breath for running, I couldn’t resist a final question. “How did you get over the Demerol?”

Because he ran beside me, I couldn’t tell if he had shrugged. “Once I was judged fit for duty, it was easier. Life wasn’t just about getting my next fix anymore, and after a while I stopped needing that shit.”

That got me wondering as we reached the surface, and carefully closed the door behind us to avoid unnecessary noise. How may other addicts and obsessives ended up where they were because they had nothing else in their lives that mattered to them? Such questions were the province of scientists, so I put it aside and loaded the app I used to control the Mandrake mines. A quick status check showed that all remained active. 「Stay behind me. My implant will be broadcasting to the mines I armed, telling them not to go off but —」

「But if we get ahead of you — boom.」 Renfield chuckled, and glanced at Mike. 「Better watch your ass, kid. You get your legs blown off, they’re gone forever.」

A soldier on patrol spotted us halfway to the watchtower. Raising his rifle, he narrowed his eyes but did not immediately open fire. “That you, Renfield?”

Renfield stepped forward. “You can stand down, Specialist Catherman. They’re friendlies.”

“You sure about the broad? Didn’t she take out —”

“Yeah, but it was self-defense. Jackson and Munoz went after her without orders.”

“Our standing orders —”

“Our standing orders do not apply when a ranking officer is in the field, Specialist. Remember?”

Specialist Catherman lowered his weapon. “So, what’s the plan?”

Renfield glanced at me. “Sergeant Robinson or one of the men following him killed Colonel Petersen. We’re fighting them. If you don’t want to fight beside us, I understand, but if you get in the way we’ll take you down. Tell anybody else who hasn’t picked a side.”

Catherman nodded, pulled a walkie-talkie from his belt, and spoke a rapid stream of Navajo. Too bad I could only recognize the language, and not understand it. Guess I should have gone for that code-breaking training after all.

He waited a moment for a response, before meeting Renfield’s waiting gaze. “We ain’t gonna fight for your girlfriend, boss, but if any of Robinson’s butt buddies come after us, we’ll make ‘em pay.”

Stepping forward, I offered Specialist Catherman my hand. “Thank you. In exchange, I would like to promise that I won’t strike killing blows against anybody but Robinson.”

That surprised him. “Going right after the boss, huh? That’s how we prefer to do things. Why dick around with pawns —”

“When you can go straight for the king?” I finished the question, which I remembered from training. No doubt somebody in the Training Corps had once served in the Commonwealth military’s special forces. Drawing my sword to show I meant business, I let Catherman take a good look at the blade. “I do intend to go directly after the Sheriff, but anybody who gets in the way is going to get hurt. How badly depends on how badly they piss me off.”

Catherman nodded, his eyes fixed on the gleaming edge. The wicked smile of a man looking forward to payback bared his teeth. “Sarge, I’ll be in the East Tower enjoying the show.”

Renfield pointed toward the tower, and spoke in a near-whisper. “We could make a straight run, but that might not be a good idea.”

His meaning was obvious. The buildings we would pass on the way could conceal several ambushes. Despite the risk of giving Robinson’s men a chance to catch up, the safest way forward was to approach each corner and check. Though I wanted to run to the next intersection, I advanced a slow, silent step at a time while drawing my pistol and stopped short of the corner to listen. Nothing. 「I’ll peek around the corner once you guys join me.」

They crossed the block at the same careful pace I used, and crouched behind me. Leading with my pistol, I leaned out to get a look down the street intersecting our road. Nothing, but it didn’t pay to underestimate them. 「Let me cross first, and see if I can draw them out.」

This time I made no effort at stealth. Instead, I dashed across the street heedless of my footsteps. If they were there, I wanted to draw them out. Stopping at the other side, I peered around the other corner. Still nothing. 「Clear.」

Mike looked impatient as he texted. 「Don’t you think you’re being a bit paranoid, Naomi?」

「Yeah, but there’s plenty of time to yell, “Come get some,” after I’ve gotten you and Renfield up that tower.」

Renfield smiled, and gave Mike a gentle punch in the shoulder. “You could do worse than that for a girlfriend, kid.”

「Did I miss something?」

Mike blushed. 「Nothing.」

Nice to see the guy still has a sense of humor. 「Come on.」

We continued our advance, for all it felt as though the entire night was slipping away in the silence of our slow, stealthy progress. It was almost nine when we finally reached the tower. Another nine hours to go before my backup was scheduled to show, and I was already tired. I was going to need a vacation from my vacation.

The Mandrake mines settled into dormancy as I used my implant to disarm them. Holding open the door, I beckoned them inside. 「Up you go, gentlemen. You’re going to cover me, and I’ll keep their attention where it belongs.」

Renfield raised an eyebrow. 「On your tight ass?」

No way in hell I’d dignify that with a response, though I daresay Jacqueline would have had fun with such a line. 「Just focus, Sergeant. Up you go. We’ve a long night ahead of us.」

Sauce for the goose being sauce for the gander, I made sure to ogle him as he followed Mike up the stairs, and waited a decent interval before re-arming the shriekers. 「Tower entrance secure. You should hear some fireworks if anybody tries to come up after you.」

Mike texted a couple minutes later. 「We’re in position. Not seeing any hostiles, though.」

Shit. Where were those bastards? Robinson and his followers dogged our heels as we ran down the tunnel from Clarion, but now they’re nowhere to be found? That doesn’t make any sense. 「Gevurah, are you there?」

The AI named after a node of the cabalistic Tree of Life representing severity was nowhere near as friendly as Malkuth. 「State your business, Adversary Bradleigh.」

「Malkuth said you could put a satellite over Fort Clarion and feed me info on enemy movements. How long would that take?」

「There is already a satellite overhead. I should be able to access its targeting systems.」

Targeting systems? That was ominous. 「Do I even want to know what sort of military satellite is overhead right now?」

Malkuth would have indulged in a bit of sarcasm and told me that I’d sleep better if I didn’t know. Binah would have gone literary and called my question one of those Lovecraftian situations where ignorance is sanity. Gevurah gave it to me straight. 「The GUNGNIR platform is currently in a geosynchronous orbit over Fort Clarion and the vicinity.」

「GUNGNIR? Are you shitting me? Whose bright idea was that?」

「You are not cleared for that information.」

My reply hit the network before I could think twice and stop myself from sending it. 「Am I cleared to put a boot up your arse?」

「What exactly does Malkuth see in you? It can’t be your winning personality.」

Oh, I’ll give him winning personality once I’m done here. 「Sorry, Gevurah, but I’ve been hearing that a lot lately and it’s getting a bit tiresome. Next time I ask a question, could you find some other way to deny me?」

「No.」

Well, it was worth a try. 「Fine. Are you getting any intel from GUNGNIR?」

「Its sensors report no humanoid life other than yourself, your companions, and an individual in the east tower. You would be well-advised to find a secure location and rest. Get some sleep, if you can. I will wake you when hostile forces approach Fort Clarion.」

Oh, sure. Like I’m going to curl up somewhere cozy and have a catnap while there’s a sodding weapon of mass destruction overhead. 「Thanks, Gevurah. Keep me posted.」

## Track 56: Iron Maiden — “Flash of the Blade”

Despite the weariness, I couldn’t just ignore my misgivings concerning the presence of GUNGNIR overhead, take Gevurah’s advice, and curl up for a catnap. Not when I had people looking out for me. Not when they were counting on me to look out for them. Instead, I sheathed my sword and climbed onto the roof of the PX.

「Are you trying to get a better view?」 No doubt Gevurah objected. 「That’s unnecessary. I’ve activated the base’s CCTV network, and am forwarding the audiovisual feeds to you.」

Sure enough, I had an overlay in one corner of my vision showing the output of cameras around the base. Which was well and good, except it wasn’t the view I wanted, but the visibility. Let Robinson and the others see me. Let them come for me. Let them see the treats I have in store for them.

Though such thoughts sounded like madness, I embraced them as a means of psyching myself up. After all, one against thirty… Yeah, I was officially nuts.

No doubt Gevurah reached a similar conclusion. 「If you survive this, you should undergo psychiatric evaluation. Taking on thirty enhanced soldiers with only a sword and a sniper for support places you firmly in what Binah would call “too stupid to live” territory.」

「And fuck you, too, Gev.」

「Do you not realize you’re making a target of yourself? This is borderline suicidal.」 Wow. You’d think Gevurah actually cared about me, insofar as the AI who dedicated himself to the Phoenix Society’s security gave a toss about anybody.

Besides, the odds weren’t quite as bad as Gevurah made them out to be. I had swords. I had a rifle. My jacket wasn’t full armor, but it would offer some protection. It wasn’t like I meant to fight them in nothing but my knickers—though that might distract them a bit. 「If I can keep their attention on me, they won’t go after my witness.」

「Fine. It’s your funeral. And a moot point at the moment, since I’m not picking up anything.」

Where the hell are they? I’m starting to feel like the ugly girl in a teen drama. You know, the one who gets asked out by the incredibly hot, popular, athletic bloke—only to get stood up in favor of the Head Girl or the Homecoming Queen, depending on where the movie is set?

「About bloody time they showed up.」 I wondered if a little song and dance might hurry them up. Perhaps Handel’s “Come Get Some” chorus in D Major? Or maybe “You’re My Bitch (And You Might as Well Accept It)” by Doomed Space Marines?

「They’re pairing off. Looks like they mean to cover the base in two-man teams. First team is through the fence.」

And here we go. I shivered a little. It felt a bit like stage nerves. Not quite fright, but just my body’s acknowledgement that it was showtime. And right on cue, there was one of the pairs now, slowly advancing.

More followed, none of them approaching my position. Surely they could see me. Couldn’t they? Didn’t they have the advantage of numbers and superior training in night fighting?

They did. They knew it. More importantly, they were fully aware that I knew it. No doubt they reasoned that I wouldn’t face them here, at night, unless I possessed the means to neutralize their advantage. Or did they know I was bluffing, and waited until I had gotten complacent and sloppy to call me on it?

Fuck this. I’m going after them. Once I had reached the ground, I headed straight for them since they didn’t carry rifles. They didn’t even have swords. Just knives, which were only dangerous up close.

There was no need to run and wear myself out. A slow walk was fine. Besides, it was traditional. Two opponents approach one another at a measured pace, each sizing up the other. Normally, the fight was a straight duel, but I’m easy. I don’t mind taking on two at a time. Those odds were almost fair, to them.

Besides, I had done it once before and only needed stitches. One of them knew it. He smiled as he drew his knife. “Ready to die this time? Those guys you offed last time were barely good enough to join the Patrol.”

“Whether you’re better than the last idiots to face me remains to be seen.” If he wanted to taunt, I’d play his game. Letting him get a look at my blade, I favored him with my most sadistic smile. “But I bought a new sword, just for you. Be a dear and give me an excuse to cut you.”

His companion spoke up. “You think you’re some kinda samurai? How about I shove that sword up your ass instead? ”

“I’m an Adversary,” I readied my blade. “And I’d love to see you try.”

The one who wanted to bugger me with my own sword reached me a split second before my old friend, and paid for it as I sliced open his belly and spilled his guts across the tops of his boots. Leaping back as he collapsed saved me from the other man’s edge.

“I had a feeling you’d fuck him up. He always did rush into things.” The soldier I had faced before picked up his partner’s knife, and wielded one in each hand. “The name’s John Atherton. Thought you deserved to know the name of the man who’s going to kill you.”

“Naomi Bradleigh.” Bloody hell; he even looked a bit like my asshole ex. “Drop your weapons and surrender, and you’ll be spared. I’m here for Robinson. You’re just following orders.”

“Generous of you, but I’ll take my chances.” Atherton tried a thrust, and sliced nothing but air.

No doubt he hoped to draw me out, but I wasn’t about to strike just yet. While his fallen companion had tried to rush me and impale me on his knife’s tip, Atherton was more careful, and his technique suggested greater proficiency.

He kept attacking with the knife in his right hand, as if he wanted me to focus on that blade to the exclusion of the one in his left. If I fell for it, he’d have me.

His beetled brow and gritted teeth suggested he realized I saw through his tactics. “Stand still and fight me, woman. You’ve got a fucking sword, for shit’s sake.”

Talking was a mistake. Though I made to strike for his right hand, it was a trick. He fell for it, and I lopped his left off at the wrist. “Better put that on ice, John.”

“Go fuck yourself.” He ground out as he let rage and pain overcome his training and rushed me.

Though I had intended only to slash open Atherton’s throat, I must have underestimated my own strength, his momentum, or the blade itself. It sliced clean through his throat and spine, and his head rolled off. Oops. Hopefully Atherton wasn’t a good friend of Renfield’s.

After wiping my blade on his uniform, I sheathed it and fled the area. Fortunately, the man I had gutted had passed out during my danse macabre with Atherton. He wouldn’t be able to tell his companions which way I ran.

Though I had the CCTV feeds, they were disorienting—especially when the system rotated to a camera aimed at me, so that I watched myself in real time. Instead, I sought a rooftop for a better view. Two pairs of men linked up, and began patrolling together despite initial orders. A shot rang out as they happened upon my first victims, and one of the, fell to one knee.

“Where is that bitch?” The stricken soldier’s shout was as clear as if I was right behind him. If they had seen a muzzle flash from Mike’s rifle, they would’ve probably headed for the tower. Instead, they’re still searching. I unslung my rifle and fired a shot of my own. Might as well help them along. My rifle didn’t have a flash suppressor, so I was nice and visible.

“There she is!”

“She wants us to come after her. Stick to the plan. Stay visible, keep her on edge, but do not engage. We can move in for the kill once’s she’s worn out and frazzled.”

Don’t these stupid gits realize I can hear them? 「Oi, Malkuth! Want to help me take the piss out of some soldiers?」

「What have you got in mind, Nims?」

This was why I liked Malkuth best. 「Does Fort Clarion have a PA? Can you patch me into it?」

「Direct audio feed? You’d need a handheld.」

Too bad I don’t have one of those. 「Can you run my texts through a speech synthesis algorithm?」

「And make it sound like you?」

Hmm… Now there was an interesting question. Sure, I could make it sound like me, but surely the Sephiroth possessed sufficient processing capacity between all ten AIs to synthesize other voices recorded via Witness Protocol. Couldn’t they? 「Any chance you could spoof Sheriff Robinson’s voice?」

「Yeah, I think I can manage. You ready?」

Oh, yeah! I sent the message, and seconds later, Sheriff Robinson’s voice boomed across the base. “All troops, stand down! The Bradleigh bitch has agreed to settle the matter by single combat.”

Of course, Mike had no idea what I had in mind. 「What the fuck, Naomi? Didn’t you hear Dr. Petersen? Robinson’s augmented. He’ll kill you.」

Mike was right. Robinson might kill me, but I didn’t intend to let him. Besides, I told Renfield I’d try to avoid killing more of his men. Going directly after their leader was the best way to keep that promise.

Perhaps Robinson retained some shred of decency after all, because here he was at the gate. Better go down and meet him. Wait, was that a rifle? Was he aiming that at *me*?

## Track 57: Bob Marley and the Wailers — “I Shot the Sheriff”

When consciousness returned, I was sprawled on the ground with a gunshot wound roughly below where one of my kidneys should be. It didn’t look bad. Hell, it looked partially healed, which shouldn’t be possible. Sure the exit wound would be far worse, I tried to reach around to check, but my right arm didn’t quite work right. And was that bone sticking out of the flesh?

After consciousness came sensation, which consisted mainly of pain unmatched by any prior experience. This was the first time I had ever suffered serious injury, and sweet unholy mother of bloody fuck it hurt. Despite my training, I was a mewling wreck sprawled across the pavement.

Never mind nailing Robinson to a wall. Never mind exposing Project Harker and Ian Malkin. Never mind protecting Mike and helping Renfield and the survivors of Dusk Patrol rejoin society. All I wanted was for my suffering to stop. If Robinson showed up and offered to finish me off…

Wait. Robinson was the son of a bitch who shot me. What the hell did that arsehole use that had enough punch to knock me off the roof, a goddamn anti-tank rifle? Whatever it was, he’s probably laughing his sick fucking arse off while I’m here feeling sorry for myself instead of tending to my injuries.

First, the gunshot wound. Though it didn’t look as bad as it should, if I didn’t do something about that soon, I would be a dead woman. Opening my jacket and blouse one-handed, I lifted my camisole and steeled myself to assess the damage.

Because I had seen such wounds before, I knew what to expect. What I actually saw was impossible. The hole Robinson’s shot had punched through my belly was filling itself in. It was closing. It was healing. Sure, Petersen had claimed that I possessed asura potential, but the regenerative capabilities displayed by Dusk Patrol required additional treatments that I had not received.

Pulling on my broken arm, I gritted my teeth and did my best to set it back into place. Once I had done so, the gash where the fractured bone had torn through closed itself. My sword arm was usable again in a few minutes, though I’d want to see a doctor afterward. So were my belly and back. I didn’t even have the scar from my knife wound any longer.

Taking out my compact, I looked for the scar along my jawline where Maestro had cut me for the first and last time, at the beginning of my training with him. It too was gone, my skin as pristine as when I first agreed to become an Adversary.

The implications of what was happening hit me almost as hard as my hunger. No, I wasn’t merely hungry. That was too polite a word. I was ravening—for fresh meat and for revenge. Sheriff Robinson no longer had a future, and his funeral would be a closed casket affair. Mourners please omit flowers.

First things first, though. Better ping my witness. 「How are you holding out, Mike?」

「Getting a bit low on ammo. I’ve been keeping these donkey-raping shit eaters at a distance. I wanted to come to you, but Renfield decked me when I tried to get past him. He said you couldn’t possibly survive. He told me—」

「Looks like I’m down to eight lives. How long was I out?」

「Almost five in the morning.」

That long? Shit. How did I not bleed to death? Was it that healing ability I shouldn’t possess because I hadn’t been subjected to the same treatments as Renfield’s band? Forcing myself to my feet, I rearranged my clothes and put myself back together. 「Any word from the Phoenix Society? We were waiting for backup, remember?」

「Nothing.」

Great. 「Malkuth, where’s my cavalry?」

「Nims! Holy shit, I didn’t think you were gonna make it. Didn’t they get to you and patch you up?」

Something’s wrong with this picture. My backup hasn’t arrived, but Malkuth thinks they got here and provided first aid. He must have told them to get here on the double. 「What was their updated ETA, Mal?」

「They were supposed to get there ASAP. What happened?」

Looks like I can’t just take Robinson’s head and drop-kick it across the continent. Not until I’ve beaten some answers out of him, anyway. 「I’m going to find out.」

“There she—”

A series of explosions coming from the direction of the armory interrupted the lucky soldier. Looks like somebody finally found my little surprises. Taking advantage of my enemy’s shock, I drew my sword and opened his thigh to the bone with a quick slash.

Leaving the fallen soldier, I took advantage of the fact that the remnants of Dusk Patrol seemed to be rushing to put out the arsenal fire. Unfortunately, I got no farther than the barracks before becoming light-headed as hunger pangs threatened to double me over. Should have searched that guy for rations.

The barracks wasn’t locked, but that wasn’t necessarily a good sign. What if it was a trap? No way I’m going in there blind. Instead, I leaned against the wall, taking deep breaths until my wooziness departed. 「You watching, Gevurah?」

「Only through the base CCTV array. GUNGNIR has been moved. Don’t bother asking who moved it or why. You aren’t cleared.」

「Is there anybody inside the barracks? I could use a snack.」

The reply came back a few seconds later. 「It’s clear. I checked the recordings as well. Nobody has been inside all night.」

「Awesome.」 Raiding the pantry, I grabbed a couple packets of beef jerky and filled a canteen from the tap. Not the healthiest of breakfasts, but it was easy protein. I ate while checking over my rifle; it didn’t appear to have been damaged in my fall, which was good because every round in its magazine had Robinson’s name on it. 「You still holding out, Mike?」

「Are all Adversaries’ missions this fucked up?」

Now there was a thought. Were there Adversaries who had it worse than I did right now? I bet Edmund Cohen could tell a hair-raising story or three. 「I’m sure some are worse.」

「Great. Just fired my last round for the DMR. Gonna borrow Renfield’s M16. Where the hell are you?」

「On my way to finish this.」

“Hey! What the—”

Always when I’m eating! I squeezed off a burst from the rifle I held in just one hand before ducking behind the counter. The other was busy stuffing jerky into my mouth.

“Hey, guys! The bitch is in here eating our chow!”

「Malkuth! Barracks floor plan! Show me a back door now!」 A flashbang rolled under the counter while I sent the message, and I threw it back just in time; men screamed as the grenade went off in their faces.

「This way.」 A floor plan appeared in one corner of my vision, with a bouncy little arrow pointing the way just like in a video game. Not funny, Malkuth!

Soldiers pursued me as I ran, but I didn’t dare stop to deter them. Nor did they seem keen on actually catching up to me. Instead, as more of their fellows joined up, they seemed concerned mainly with herding me. 「Malkuth, can you guide me to Robinson?」

「Yeah. This way.」

“Out of my way, arseholes!” Following the bouncing arrow, I surprised a small pack of soldiers with a sustained burst from my rifle. Before they could recover, I had bulled my way past them.

Minutes later, I passed the front gates and stopped short. Robinson leaned against the guardhouse with a pistol in hand. Kneeling beside him, with their hands bound behind their backs, were Mike and Renfield. The son of a bitch grinned at me as if he hadn’t shot me, and wasn’t holding two people hostage. “So, Doc Petersen wasn’t full of shit after all.”

“Never mind that. Where’s my backup, and what the fuck are you doing with Mike and Renfield?”

“Backup? You silly bitch, did you really fall for that? Here’s a clue for you—the Phoenix Society knows all about what’s been happening here, and now you’re part of the experiment.” Robinson’s smile broadened. “I’ve wanted an excuse to kill Renfield for years, and the Brubaker brat has seen too much and is too idealistic. I should’ve offed them already, but I thought it would be fun to make you fight for their lives.”

“Was it really your idea, or are you still taking orders from whatever rogue element in the Society that’s orchestrating this shit?” My question was a stall, meant to keep him busy while I thumbed the fire select switch to single shot. My finger tightened around the trigger, but did not exert enough pressure to fire. Not yet.

“Doesn’t matter.” Robinson leveled his pistol at Mike’s head. “Drop the rifle or—”

Rather than let Robinson finish his threat, I put a shot between his eyes. He swayed a bit, his mouth opening and closing like a fish’s out of water as he tried to turn his gun on me. He collapsed before he could manage it, and sprawled face-down in the dirt.

“Holy shit.” Mike was almost comically wide-eyed. “You just killed the Sheriff.”

Given that his hand still twitched a bit, I wasn’t so sure. Approaching Robinson’s fallen body, I shot him through the head a second time. A control shot to confirm the kill was a bit of Phoenix Society doctrine I wouldn’t discard. “Now I did.”

Taking Renfield’s knife from its sheath, I cut his bonds before attending to Renfield’s. “Sergeant, can you get your men to stand down now that Robinson’s out of the picture?”

He nodded. “Probably. How many of ‘em did you end up killing?”

Should have known that question was coming. That guy I gutted is probably still alive, but I bet he’s righteously pissed off. Same with those guys I shot at while getting out of the barracks. That left one for sure. “Was John Atherton a close friend?”

Renfield shrugged. “Nah. Hell, I saw you take that prick’s head off. You saved me the trouble of kicking his ass myself for suckering the other guys into following Robinson.”

“Awesome.” Mike drew out the word while stretching. “So, can we get the fuck out of here? I mean, it’s all over now. Isn’t it?”

There’s no way things would be that simple. Renfield seemed to hold a similar opinion, for he cocked an eyebrow at Mike. “Kid, it’s never over. Something always comes up.”

Mike remained insistent. “Collins, Petersen, and Robinson are dead, and Naomi’s got evidence to prove that they were ultimately responsible for all the weird shit. What else could possibly happen?”

## Track 58: Krypteria — “I Can’t Breathe”

If there’s a God, and it possesses any control over the events of our lives, then asking a question like *What else could possibly happen?* is tantamount to teasing a bored cat with a laser pointer. As soon as somebody does so, it’s inevitable that causality, fate, or a malicious deity with a juvenile sense of humor will answer.

In our case, the answer came with our return to Clarion after a hike through a forest made almost unnavigable by dense fog. Though we could have waited for the cool morning sun to rise higher in the sky and burn off the fog, we were all hungry, tired, and eager to get the hell away from the fort. Unfortunately, we weren’t the only ones to return.

“Good morning, Adversary Bradleigh.” Dr. Petersen met us midway down Main Street, smack in middle of the thoroughfare. He had an old-fashioned gentleman’s walking stick tucked under one arm, which I had never seen him use before. “Shall I treat you and your companions to breakfast?”

Immediately turning on secure relay chat, I asked Mike and Renfield the obvious question. 「Have I gone round the bend, or is there a dead man standing in front of us and offering to buy breakfast?」

「So what? Let the dead guy buy us breakfast if he wants.」

Mike’s advice was sensible, even if it didn’t allay my concerns regarding my mental state. Rather than confront Petersen, if it was indeed him, I nodded. “Thanks. I could certainly use a good meal.”

It wasn’t until we were cozily tucked away in a far corner of The Lonely Mountain’s common room with our breakfasts that I said anything. “I saw you die, Dr. Petersen. One of those soldiers shot you in the throat.”

Petersen pulled down his shirt collar to display a wattled but otherwise unblemished neck. “Are you sure? You didn’t even think to examine me, let alone confirm brain death.”

“Considering the slug ripped out your jugular, Doc, I think Naomi is right. You can’t be Dr. Petersen.” Renfield paused long enough to take a bit of his four-egg meat lovers’ omelet. “So, who the hell are you?”

Mike stared at Renfield. “Who else could this be?”

“Who indeed?” I pointed at Petersen with my fork. “You might as well spill.”

“Come now. You’ve stabbed men through the heart and seen their wounds heal. You yourself got shot with a fifty-caliber rifle, and yet you look perfectly healthy, if a bit underfed. Did you honestly think I would refrain from taking advantage of the technology I tested on Collins and Robinson? The very technology, with which I treated you when you came to get that knife wound stitched up?”

Renfield was on his feet in an instant. “You bastard! You administered the Process on Naomi?”

Petersen sipped his coffee with the insouciance of a man with nothing to fear. “Yes, I did. A far more refined version than what Project Harker inflicted upon you, and strictly temporary.”

Hearing that whatever allowed me to survive the previous night was temporary left me torn. I was glad it was, because I was different enough from others already, but the treatment wasn’t without its advantages. “How long will whatever you did to me remain in effect?”

“Another two weeks. Assuming you live that long.”

Mike glared at Petersen. “What the fuck is that supposed to mean?”

“I suggest you focus on enjoying your breakfast, young man.” Petersen’s tone oozed condescension.

He took his own advice, but looked up from his bacon and eggs as I rose, drew the side-sword I kept after discarding my other swords in my room, and let Petersen take a good long look at the tip. “If you want to live long enough to enjoy your breakfast, doctor, I suggest you elaborate. Why wouldn’t I live long enough for this treatment to wear off?”

“It’s quite simple, young lady. None of us are going to live that long. I have activated the GUNGNIR system and programmed it to drop its remaining armament on the town and Fort Clarion.” He checked the old-fashioned titanium wristwatch that I had never really noticed him wearing before. “Assuming this old thing is still accurate, I’d say we have about twenty-nine minutes. So dig in. Your last breakfast is getting cold.”

Mike dropped his fork. “Have you lost your goddamn mind? Do you have any idea what that weapon can do?”

Petersen nodded. “Of course. I activated it once before, just before everything went to hell, when a bunch of tree-humping peacenik rabble-rousers incited the town to march on Fort Clarion to protest the unethical experiments we were carrying out so that we could better protect their right to be ignorant and lazy while whining about how corrupt and immoral the Commonwealth’s government was.”

「Malkuth, are you getting this?」

「Yes, Naomi. Petersen is telling the truth. He is in fact guilty of the atrocity for which he just claimed credit.」

Glaring down at Petersen, I knocked the fork from his hand with my sword. “Why? Tell me why, damn you. Why would you use that weapon to destroy the town you rebuilt, and murder thousands of innocent people?”

“It’s really quite simple. The data archive you took from Tetragrammaton didn’t contain my later research. There’s nothing in there that the Phoenix Society doesn’t already possess.” Petersen let that sink in for a moment. He gave me a moment, damn him, to realize my time and effort had been in vain. “What do you think the Society would do if they found out that I had found a way to safely and temporarily activate a CPMD+ individual’s asura potential? What do you think they’d do if they learned that I had also found a way to give CPMD- individuals the asura potential, and activate it?”

Mike had gone pale, and his voice trembled as he forced out the words. “So, what is this? You’re going to sacrifice the town to protect the world?”

Petersen snorted. “No. It’s simpler than that, boy. Ian Malkin exiled me here, and condemned me to live out my best years in this shit-hole on pain of exposure. So I will deprive him of the breakthrough that eluded him and his pet scientists.” He turned a gimlet eye toward me. “And while I’m at it, I’ll deprive him of his daughter.”

Daughter? Though I wanted to demand further details, a choked sob from Renfield distracted me. Though he must surely have done and seen far worse as a soldier, learning his former commanding officer’s intentions must have left him aghast. “Colonel, you’re going to murder Dusk Patrol as well. Don’t you care?”

Petersen smiled. “Christopher, it’s time you and the rest of the boys were put out of your misery. There’s no way you can go back to the world as you are. My only regret is that you won’t be buried at Arlington with the honors your service to the Commonwealth would merit.”

At that, Renfield bared his teeth and lunged for Dr. Petersen. Mike and I had to combine our strength to keep the enraged sergeant from ripping out his former commanding officer’s throat with his bare hands, and I had to turn my sword on him yet again. “Kill him now, and he wins.”

Petersen shrugged. “I wouldn’t have explained your fate or my motives if the slightest possibility of you stopping me existed.”

Mike spat in his face, which should have been beneath him. “Fuck you, old man.”

Petersen wiped his mug with his sleeve and checked the time. “Twenty minutes. You might want to say your last goodbyes.”

My entire body had begun to tremble, and all I could hear was my heart in my throat as tunnel vision set in. The response was a familiar one, and one my training normally tempered, but this time everything I had learned about remaining calm under pressure deserted me. I was ready to fight, but what threatened me wasn’t an enemy I could face with my sword. I was ready to flee, but where would I go to escape the knowledge that I had left thousands to die? 「Malkuth, please tell me you can do something about this. The Phoenix Society was supposed to have control of GUNGNIR, wasn’t it?」

「Yes.」

「So, GUNGNIR shouldn’t have been activated without the Society’s permission, right?」

「Correct.」

Oh God, I was even more afraid of where this line of inquiry was leading than I was of imminent orbital bombardment. 「Who in the Phoenix Society would have the authority to activate GUNGNIR?」

「Naomi, I’m really sorry, but I can’t tell you.」

「God damn you, Mal, I don’t have time for your clearance bullshit right now. Tell me who authorized this, because Petersen sure as shit couldn’t have done it himself, give me the override codes, or direct me to somebody who can! Thousands of human lives are at stake here. Human lives we are sworn to protect. If you don’t, then you’ll be complicit in the biggest violation of individual rights since Nationfall.」

Every second felt like a day as our personal countdown to extinction ticked down. Five minutes passed before Malkuth finally replied. 「Speak to Edmund Cohen. He’s on the Executive Council. I’m sorry I can’t do more, Nims.」

The shaking only worsened, and the Lonely Mountain’s walls started to close in on me. Running outside, I tried to connect to Eddie. Please be there, you lecherous old stoner. Please. 「Nims? You’re OK?」

「No, I’m not OK. Listen: I need you to help me stop GUNGNIR from bombarding Clarion. Malkuth insists he can’t, and told me to speak to you because you’re on the XC.」

「GUNGNIR? Great. Just fucking wonderful.」

Mike ran out, his eyes wide and staring. He glanced skyward as he rushed toward me. “Goddamnit, Naomi, what are you doing?”

“Back off.” Glaring at Mike, I managed to keep from turning my sword on him, but my voice was still a scared, angry hiss. “I know you’re as scared as I am, but this isn’t the fucking time.”

Mike backed away, and I returned my attention to Eddie while staring up into space. What would the tungsten lances look like as they rained death and destruction on Clarion? A perverse corner of my mind was obsessed with the question. 「Help me, Eddie. We’ve only got ten minutes.」

「Naomi, I’ve been trying to help you. I’m going to send you the override code and the satellite’s IP address now. I already tried using it, but GUNGNIR wouldn’t let me connect. Maybe you’ll have better luck.」

Eddie’s message came through as promised, and I immediately attempted to connect. Come on…

> Oppenheimer-Teller Aerospace Corp.  
> OpenBSD 6.9  
> Property of NACAF

> GUNGNIR login:

Bloody hell. They really have put Unix on everything. At least Eddie thought to provide credentials and instructions. I followed them, and got a prompt for the override code, which I sent. The response came seconds later.

> Unauthorized override attempt detected.  
> Terminating remote session.  
> Have a nice day.

Oh, no you don’t. Connecting to Tetragrammaton as the sysadmin, I switched to Petersen’s directory and checked to see if the old man had been dumb enough to put his credentials in a file I could read. No such luck, which meant I had to crack root.

Figuring the late Matt Tricklebank might have useful tools, I switched to his account and poked around. His copy of the HermitCrab source had a directory called “dbfi-experimental”, so I accessed it and opened the README file. Turns out DBFI stand for “distributed brute force intrusion”, and this app would attempt to leverage the entire network to crack root on a target machine.

Mike kept glancing skyward, but didn’t speak to me. Had I frightened him? Renfield, however, was not so reticent. “Naomi, if you’ve got an ace in the hole, now would be a really good time to play it.”

“Working on it.” I ground out as I figured out how to run the DBFI tool. Turns out Tricklebank had a test script that would spawn a few thousand virtual machines, all of them running a DBFI client targeting the machine of my choice. Aiming the script at GUNGNIR, I ran it and soon had a hundred thousand processes trying passwords against the root account.

A hundred thousand wouldn’t be enough, but I was already pushing Tetragrammaton to its limit. I needed more power. 「Malkuth, I’ve got an idea, but I need your help.」

「Shoot.」

Before explaining, I sent the code. 「Matt Tricklebank has a distributed brute force intrusion tool with a script that can spawn thousands of virtual machines. I want you and the rest of the Sephiroth to run this script on GUNGNIR. If one of you manages to crack root, the tool will automatically give me control.」

Five minutes left, and Malkuth had gone quiet. No doubt he and the others were deliberating, but neither I nor the people of Clarion had time for a bunch of AIs to piss about with a discussion.

Four minutes and thirty seconds remained on the clock when the DBFI control panel reported the addition of additional clients. I had jumped from a hundred thousand processes attacking GUNGNIR to a hundred billion. More processes came online, until I had just over a trillion little virtual machines pounding the satellite. Maybe I’d end up frying the onboard computer instead of cracking it. Would that prevent it from deploying its payload?

The clock was down to three minutes when the control panel overlaying my vision disappeared. A terminal connected to GUNGNIR with a control menu appeared in its place. Seeing that one of the options available was “Cancel Current Deployment”, I chose it and waited.

Time seemed to stretch as the countdown I ran based on Petersen’s estimate ticked down to the point where only seconds remained. Each second felt like an hour, and cold sweat soaked through my clothes as I waited and hoped that any second now a response would come back down the pipe.

## Track 59: Bruce Dickinson — “Road to Hell”

Ten seconds left, and I had received no response from GUNGNIR. There was no sign that the satellite had accepted or rejected my command to cancel Dr. Petersen’s previous request to drop its remaining payload on Clarion. There was nothing more I could do. Nothing but wait, and hope.

My hopes waned by the second. Only one of which remained, then nine hundred and ninety-nine milliseconds as my implant unhelpfully switched to smaller units. With only ten milliseconds left on the clock, a response finally came down from GUNGNIR.

> Launch aborted.

Unable to believe the stone cold crazy luck, I blinked even though my implant’s display was superimposed over the raw perceptual data coming from my eyes. The message remained. The launch had been aborted. And I was back to the control menu, which now helpfully offered a self-destruct option.

Because of the time of day, there was no way to see GUNGNIR’s destruction from the ground. The sky was simply too bright. But I imagined the satellite using the thrusters that allowed it to change its position to reenter Earth’s atmosphere and either burn up on entry, or splash down somewhere in the Atlantic Ocean.

Either way, a tsunami of elation crashed over me, leaving me feeling invincible. Though I wanted nothing more but to jump around and cheer like a loon, I simply smiled at Mike and Renfield. “Guys, you can breathe now. I aborted the launch and dropped GUNGNIR into the ocean.”

Grabbing Renfield, I stole a searing kiss just to drive my point home. I was about to treat Mike in similar fashion when almost a meter’s worth of sharpened steel erupted from his chest before disappearing just as swiftly. Mike fell to his knees, bloody froth bubbling from the mouth I was about to taste mere moments ago. “Renfield! First aid! Get a fucking paramedic!”

It was as if somebody else had shouted the orders using my voice, but I forced myself back into reality as Dr. Petersen backed away, the sword he had concealed in his walking stick dripping red. Drawing my own sword, I bared my teeth. “Are you truly so desperate to die, Dr. Petersen?”

“I told you, Adversary Bradleigh. I will not permit the knowledge in my head to fall into the Phoenix Society’s hands. Since you’ve stopped GUNGNIR, I must resort to other methods.”

We circled one another, sizing each other up, neither of us ready to strike the first blow. Neither of us wanting to be the first to reveal our style, to give the other insight into our strategy. “If you wanted to commit suicide, there are easier ways. You could have done the job with a pistol instead of attempting to murder one of my witnesses.”

“That was not an option for me.” Petersen seemed almost regretful. “Unlike yours, the treatment I administered to myself was the first version, based on modifications to the Patch. It includes inhibitors intended to prevent certain kinds of ideation. I can’t even use related euphemisms without blacking out.”

So, he could attempt suicide by Adversary as long as he didn’t say or think the words? That didn’t make any sense, since I said the word, he heard it and understood it, and managed to remain standing. “I think you’re bullshitting me, old man. I think you could fall on your sword or eat the gun if you really wanted to. You’re still playing some kind of game. Did Ian Malkin put you up to this?”

“No.” The first genuine, unaffected smile I had seen on Dr. Petersen’s face lit his features, casting him in psychopathic relief. “The Devil’s honest truth is that the only way to properly test the effect of my treatment on you is to push you to your limit—and I don’t think you’ve reached it yet.”

This is definitely some kind of bullshit game, and I’m sick and tired of playing. “Trust me, old chap, I’ve hit my limit. Now shut your gob and fight me.”

Leading by example, and running counter to years of training, I let my anger at Petersen’s cowardice drive me to strike the first blow. Steel clashed as he parried my thrust with his blade while swinging the cane that had concealed it at my head. The price of my overconfidence was a ringing blow that left me reeling and vulnerable for a moment.

Recovering before Petersen could follow up, I parried a cut from his sword. This time I knew his cane was coming, so I stepped inside his guard and struck him with my off-hand. His nose crumpled beneath the palm of my hand, and I licked my lips as blood poured forth.

Unperturbed by the damage, he knocked me to the ground with a shoulder strike, forcing me to scramble to get back on my feet before he could pin me to the pavement. It was obvious that I hadn’t been taking Petersen seriously. Despite his apparent age, the damn experimental treatment must have restored his youthful vigor.

“I expected better from you, Adversary Bradleigh.” Petersen’s voice still carried the nasality of a man with a broken nose. It must have healed as is, just as my broken arm had last night.

Circling him, waiting for him to strike and expose himself, I gave him the finger. “You’re not the first man I’ve disappointed, doctor.”

A cruel smile curved Petersen’s features. “No doubt you disappoint every man who first sees you from behind before getting a look at your face.”

Was that pathetic excuse for a butter-face joke supposed to goad me into dropping my guard? Not bloody likely. “Just keep digging that grave.”

“If you could have killed me, you already would have.” Petersen lunged, but my initial anger at his stabbing Mike had cooled and my training had reasserted itself. His thrust was barely worth the effort it took to sidestep it. While he was off-balance, I punished his failure with a slash across his wrist.

We continued our dance as the ambulance arrived and paramedics attended to Mike. A crowd had gathered, not understanding why Petersen and I dueled, but cheering the barbaric spectacle. I was caught up in the flow now, and all but untouchable. Every time Petersen attacked, his blade sliced air as mine tasted his blood. His suit was soon tattered, his leathery skin showing through dozens of rents in the cloth.

A normal man would have given up by now. He would have thrown down his sword and surrendered. However, Petersen fought with the determination of an enraged bull, all finesse lost. Whether he retained his intent to test me as part of whatever deranged experiment he had concocted, or was now fighting to avenge wounded pride, he would not stop.

However, he had slowed a little. The little cuts I inflicted as punishment had begun to take a toll on his body. Without food, the only way his enhanced flesh could repair itself was by reconfiguring existing tissue in a catabolic process. He would eventually eat himself alive, unless he submitted. “I can do this all day, Adversary. Even if you can keep my blade from tasting your flesh, your endurance isn’t—”

Stopping to monologue in the middle of a duel is a bad idea. Sticking my sword in his lung seemed the best way to illustrate this fact. Besides, I had gotten bored with smacking Petersen around like a mouse. But before I could press my advantage and disarm the doctor, strong hands caught my arm. The voice behind me was one of implacable command. “Stand down, Adversary Bradleigh.”

Wrenching free of the interloper, I spun and was about to turn my sword on him when another rammed the stock of her Kalashnikov into my ribs before reversing her rifle and pressing its muzzle into my temple. “Adversary Naomi Bradleigh, you are under arrest. The charges are misuse of Phoenix Society property, destruction of Society property, inhumane treatment of a prisoner in custody, and abuse of authority.”

“Inhumane treatment?” The charges stunned me, even though I had been aware the whole time I was right up against the line. “Who did I treat inhumanely?”

“We saw you torturing Dr. Henrik Petersen, whom you had previously arrested.” Spoke the Adversary who had caught my arm and warned me to stand down. Something in his tone reminded him of my foster father as he swept an arm to indicate the crowd around us. “So did a few hundred locals.”

None of this made sense. None of it was in any way fair. But I remained silent as my fellow Adversaries recited my rights as one accused of committing crimes with the solemnity of a priest administering the last rites. I had even withdrawn into myself a bit, so that the Adversary not holding a gun to my head crouched in front of me and snapped his fingers. “Adversary, I repeat. Do you understand your rights?”

“Yes, I understand my rights. Now tell your girlfriend to get that AK out of my face. And tell me what happened to Mike Brubaker. He was under my protection. What’s his condition?”

Instead of answering, the Adversary nodded at his partner. She reversed her rifle, so that the last thing I saw before unconsciousness claimed me was the stock of that damned thing rushing toward me and a flare of crimson pain spreading across my vision. No good deed goes unpunished.

## Track 60: Wolfgang Amadeus Mozart — “Queen of the Night (The Magic Flute)”

The last time I woke up with a headache like this, there was a foul taste best left undefined in my mouth and a certainty in my mind that Jacqueline was somehow to blame. This time, I wasn’t quite sure what had happened. There were hazy memories of a duel of attrition, a wounded young man I should have done a better job of protecting, and the almost overwhelming conviction that my death and that of thousands of others would rain from beyond the sky.

Had I taken a blow to the head? It scared me that I couldn’t quite remember, nor understand why I was here—wherever here was.

A soft white light flared to life above me, and showed that my world consisted of gleaming black. The walls, ceiling, furnishings, fixtures, even the bedding were an unremitting ebony. To add insult to injury, somebody had even taken my clothes and weapons, and left me clad in black pajamas.

At least the floor wasn’t too cold against my bare feet; whoever had thought to provide pajamas did not prove equally conscientious about socks or slippers. Furthermore, the cell wasn’t as cramped as it could be at five by five meters, with a ceiling three meters from the floor. There was room for me to exercise, so that was how I started. I still wasn’t clear on how I’d escape, but I knew I needed to be strong if I hoped to make the attempt.

Once I had finished my PT, it was time to take further stock of my surroundings. Despite my implant, I couldn’t reach the network. There wasn’t even a GPS reading that would give me a clue as to my location—save that I was either underground or in a Faraday cell. Quite possibly both.

All I knew was that several days had passed since my arrest. Was Mike Brubaker safe? What about Christopher Renfield and the remnants of Dusk Patrol? Were they here with me, in other cells? What did the Phoenix Society gain from keeping me here? All I had were questions, solitude, and entirely too much time on my hands. Here goes nothing.

“Hello? Is anybody out there?” It would have reassured me to hear a guard outside, even if their sole response was a barked order to shut the fuck up.

Instead, there was only silence. Even my own voice seemed strangely muted, as if my cell’s acoustics had been designed to dampen all sound. If I screamed, would anybody outside hear me?

Not realizing it, I had begun to pace as if trying to outrun my thoughts. That wouldn’t do at all. PT was one thing, but wearing myself out would be counterproductive.

The question was, what should I do? My captors had not even thought to provide a selection of books I might read to while away the time. So much for humane treatment while in custody. Oh, wait. That had been one of the charges against me: that I had subjected Dr. Petersen to inhumane treatment.

Never mind that he wasn’t my prisoner at the time, or that he had come within a fine hair of wiping Clarion of the map. Because I had placed him under arrest, I was responsible for the bastard. I should have just blown his head off, and then worried about stopping GUNGNIR. At least then I wouldn’t have had to worry about him being outside, free, and laughing his sick fucking arse off at me.

Just thinking about it frustrated me. Without realizing it, I hit the wall with the heel of my hand. The impact sent waves radiating outward through the material from where I had struck my blow.

Now, that was strange. I’ve never heard of a solid material doing something like this. With a shout, I tried the wall again, lashing out with a kick. Same effect, only the waves spread out farther this time.

Sitting in the black chair that constituted one of the three pieces of furniture my cell contained—the third being a table—I tried to blank my mind, breathe, and just be. It was a technique we Adversaries were taught, to stop trying to direct one’s reason and just let it join intuition and go where it would. It sometimes led in directions one might not consciously consider because they seemed too ridiculous.

And what I was thinking now is whatever property of the material lining the walls and ceiling of my cell that allowed it to visibly react to the force of my blows also gave it some kind acoustic damping ability. Sound was force, after all, force expressed as vibration conveyed through a medium.

Taking a breath, I stood and let my voice fill the cell at concert pitch. The walls and ceiling became a rippling gray they absorbed the power I projected into a clear steady tone, with which one could tune just about any instrument. Letting the note fade, I watched the walls settle back into their original gleaming black.

Speaking hadn’t produced the same effect as singing a sustained concert A, which was strange. Stranger still was the idea born of this result. If I worked my way through my entire range, might I find a note capable of shattering the walls? A singer sustaining the right pitch with sufficient power could shatter glass, so why not this material?

Lacking anything more interesting to do, I ventured the experiment, starting with the B just below middle C and working my way up in semitones as if practicing a chromatic scale. The effect I had seen before become more pronounced as I progressed toward the upper limit of my performance range, until I was sure that my cell would crumble around me as I hit the highest note I had ever managed while performing.

But as I let my voice fade into silence, I remained a prisoner. It was obvious that it would take a note higher than any I had sung before. If I wanted out, I would have to break free of my own limitations first.

Beginning to sing again, I catapulted myself to the top of my range as if trying to ram a locked door with my shoulder to break it down. Every time I failed, anger grew hotter within me as I readied myself for another attempt. My rage fed upon itself, a wrathful chain reaction that drove me to push despite my voice growing hoarse from overuse.

Even as it threatened to crack I continued to try. It was insane, I was insane, but something about this cell felt so profoundly wrong that I couldn’t bear to spend a single night here. It was bad enough I had woken up here after God alone knows how long. My need to get out had eclipsed reason, and would consume me if it went unmet any longer.

Frightened that I might gain my freedom only by losing my voice, I forced myself to stop. Running the tap, I tasted the barest sip of cold water from a cupped hand and waited.

If there was something in the water—a sedative, perhaps—I hoped my precaution would result in me getting too small a dose to put me out. When nothing happened after fifteen minutes, I began drinking from the tap in greedy double handfuls that cooled my parched throat.

Finally full, I settled back into my chair to think. Perhaps with training and hours of practice I might extend my range by a few notes, but I wasn’t back at Juilliard and didn’t have the time. It wasn’t rational, but I felt like my very life depended on me getting out of here as soon as possible.

For lack of anything better to do, I tried my voice. Singing softly, without projecting, I settled into one of my favorite arias from *The Witchflute*. Though it wasn’t necessarily appropriate to the setting, I loved the Queen of the Night’s role, which sounded all the more aggressive in German. 

> The vengeance of Hell burns my heart  
> Death and despair blaze before me!  
> If Sarastro feels not at your hands  
> The agony of death  
> Then you shall be my daughter nevermore  
> Disowned may you be forever  
> Abandoned may you be forever  
> Destroyed be forever  
> All the bonds of nature  
> If not by your sword  
> Sarastro breathes his last!  
> Hear me, Gods of Vengeance  
> Hear this mother’s oath!

The walls surrounding me had one use. The rippling they produced as they dampened my voice showed that it wasn’t just my imagination. They proved I still had it. Perhaps, if I could put enough power into it, I might be audible outside. Surely a guard would eventually pass by and hear me.

Resolved to reach through my prison and touch somebody, I sang from the depths of my soul. Working my way through every aria I ever memorized, I poured myself into a succession of roles. I was Titania, Queen Elizabeth, Cleopatra, Lady Macbeth, Lucia Lammermoor, and dozens of other operatic heroines.

Lost in song, I lost all awareness of anything but the music and my desperation to be heard. To reach just one person, to pierce the armor of reason and habit and social convention, and strike directly at their emotions—that was all any artist wanted. But unlike many artists, my life depended on it. That much was certain.

## Track 61: Joe Satriani — “Friends”

When I returned to my senses, the black cell was gone. Instead of black pajamas, which had been comfortable and a rather stylish by comparison, I had been downgraded to a hospital gown. The prison cot was now a hospital bed. A cool breeze through open windows made the cut flowers in a vase on my bedside table sway a little.

Though I could have rung for a nurse, I left the call button untouched. It was enough to lie here, listening as the pigeons strutted and cooed along the ledge outside the windows. Someone would be along eventually.

Someone with a familiar voice had other ideas. "Oi, Naomi. I saw you open your eyes. Wake up already."

"Dammit, Jacqueline." Turning away from my friend, I pulled the blanket over my head. "Lemme sleep. It's Saturday."

"You've had plenty of sleep." She pulled them off me, exposing my bare arse to the cool air. "C'mon, Nims. Visiting hours are up in fifteen minutes. You can go back to sleep then." 

"Fine. You win." Sitting up, I tied the gown closed behind me. "Where the hell am I?"

"Nightingale Memorial Hospital in Philadelphia. What the hell did you get yourself into in Clarion? I had to come all the way from London to get you out of a black site."

Not that I minded a rescue from Jackie, but it was weird. "Why you?"

A small, sidelong smile suggested it would be an interesting story, but Jackie kept it short. "Malkuth called me. Said something about how he didn't want to miss out on that date."

Looks like Mal is never going to let that promise go, that incorrigible flirt. But if his experience of humanity comes from people like me and Jacqueline, I suppose it's only to be expected. Jackie was a bad influence on me; she was probably even worse for an innocent, naive AI like Malkuth. Not that he seemed to mind being corrupted. "Was that all he told you?

A shrug from Jacqueline. "Pretty much, though he said the tip came from a guard who had a crisis of conscience after seeing that a pretty girl had been locked up alone in a Commonwealth black site under Philadelphia to sing her heart out until she collapsed. He requested anonymity out of fear of reprisals from whoever put you there."

Whoever my arse. It had been the Society that put me there. "Jackie, two Adversaries showed up to arrest me. One of them must have decided I was resisting, 'cause she hit me upside the head.

At least she had the decency to give a sympathetic wince. "Damn. And then you woke up in that cell? That couldn't have been much fun."

"It wasn't. So, when's my court martial?"

"I don't know anything about that. Maybe ask Mal?"

"Good idea." I pressed my fingertips to my ear. 「Malkuth?」

No response. No network connection. Dammit. Pressing the call button, I gave Jackie a sidelong glance. "Looks like I'm off the network. Somebody better have a—"

A nurse stuck her head in. "Is something wrong, Ms. Bradleigh?"

"Care to explain why I'm denied network access?" I had snapped the question before getting a good look at her. She wasn't a proper nurse, but a candy-striper. Her ID card marked her as one Jen Simmons. If she was a day over fifteen, I'd eat these flowers. "Sorry, Ms. Simmons. That was rude of me. Would you please find my attending physician and ask him to stop by?"

Simmons gave a hurried nod. "Of course, Ms. Bradleigh."

The doctor arrived twenty minutes later, which gave Jackie and I time to catch up a bit and share news. She was just telling me about some of *her* adventures when a woman softly cleared her throat. "Adversary Bradleigh? She approached the bed as Jackie pushed her chair back, and offered a slim hand. "I'm Doctor Tranh, your attending. Ms. Simmons told me you were concerned about your lack of network access."

"That's right. If I'm a prisoner awaiting trial, I'm still entitled by law to —"

Dr. Tranh nodded. "I understand, and I apologize. I ordered your implant disabled. It is a standard preventative measure for individuals who have suffered recent head injuries."

"I suggest you have my friend's implant re-enabled, as a prophylactic against an acute case of boot-in-arse syndrome."

Dammit. "Thanks, Jackie, but I don't think you're helping."

Fortunately, Dr. Tranh found Jackie's threat amusing. Or was it my embarrassment put that little smile on her face? "I can have her removed, if she's bothering you. I don't see her listed as next-of-kin."

"It's all right. She's my partner." As Dr. Tranh raised a questioning eyebrow, I clarified the relationship. Not that it was any of *her* business. "We're both Adversaries, and usually work together."

"Nice save, Nims."

Dr. Tranh looked down on Jackie. "Partner or not, visiting hours ended about ten minutes ago. You can have five more minutes, but I must insist upon you leaving afterward. My patient needs her rest."

"I just woke up."

"And I can bring have a nurse bring you dinner and a tablet so you can read a book or catch up on the news, if you like. But I want you rested for tomorrow. If the tests all go well, I can discharge you then." With that, Dr. Tranh left in a swirl of white coat and inky black hair.

Once the door was closed, Jackie was back on her feet and reaching into a bag. She produced three thick volumes of manga. One bore the title *Shotgun Exorcist*, and featured a cigar-chomping nun wielding a crucifix and a double-barreled sawed-off—presumably for situations where the power of Christ proved insufficiently compelling. "Those cheap old tablets hospitals lend out will just give you a fuckin' headache. You can read these instead. Seems Claire already had copies."

I flipped through *Shotgun Exorcist* first. It was obvious the artist was a guy; no woman would draw such outrageously proportioned female figures. "Are you talking about—"

Jackie nodded. "You met her on the maglev to New York. Seems my sister-in-law was taking her to live with her parent to get her away from me. Says I'm a bad influence."

"I think the damage is already well and truly done. Did Claire know what her mother was doing?"

A chuckle from Jackie. "No, but she raised all nine circles of hell when she figured it out. And now her mother's miffed that her little girl would rather live with me. Though Claire wasn't exactly diplomatic in making her preferences known. You should have been there, but it's a good thing I thought to record it."

Jackie showed me a handheld, and tapped a button on the screen. The video began playing, and I recognized the voice of the girl who had sat next to me instead of her mother despite the tearful anger and intermittent sniffles. "Fuck you, mum. I'm never gonna be the demure little Stepford daughter you really want, so why not just let me live with Jackie? At least she loves me the way I am."

"Claire, you stop right this instant. What would your dad say?"

"I don't have a father, you lying slag. You got drunk one night, shagged some stranger, ended up with me, and couldn't admit the only difference between you and Aunt Jackie is you were too bloody *stupid* to use birth control. So you fucking lied to me, and let me hope I'd someday get to meet a bloke who actually has no idea I even exist."

Unable to believe any eight-year-old girl would speak so harshly, or with such bitterness, I stared at Jackie. "Is Claire going to be all right?"

Jacqueline sighed. "I don't know. I should have stepped in sooner, but I thought Claire had a right to have her say. But now I don't think she and my sister Charlotte will ever reconcile. But she's currently in my hotel room, so I should go back to her."

That had to be a rough situation for Claire *and* Jackie. It was tempting to talk to my parents, since they had already raised one tomboy, but Claire would be a much harder case than I had been. She already realized that the adults in her life were all too human. "I'd offer to help, but my own future's kinda shaky right now."

"It's fine, Nims." Jackie showed me a modest ring on her left hand. "You know that vicar? We're getting married soon, and he dotes on Claire, so she'll have an uncle of sorts. Can I count on you to stand up with me?"

"Of course."

"Thanks." She drew me into an awkward hug. "And if there *is* a court martial, you can count on me. I'll testify on your behalf."

Before I could express my appreciation, the door opened. Dr. Tranh cleared her throat, and stared daggers at Jacqueline. "Better go, Jackie. We'll talk again tomorrow."

"Right." Jackie blew the doctor a kiss. "Don't get your knickers twisted, Doc. I'm leaving now. You can have your patient all to yourself now. But don't do anything *I* wouldn't do."

Dr. Tranh's expression softened as she examined me. "I sent her away as much for her own good as for yours. She's been at your side most of your stay, with only short breaks to check up on her niece and make sure she had something to eat."

"Jackie's a good friend. So, how's the prognosis? Will I live to stand trial?"

"Most likely, but there's something odd about you. I had a look at your arm, since it's only the slightest bit crooked, and I discovered that you recently broke it. Yet it's somehow already healed. Would you like to tell me what happened? As your attending physician I can offer complete confidentiality."

Rather than answer immediately, which would have resulted in me turning down the offer, I took a moment to consider it. After a while, I nodded. "It's going to be a long, somewhat complicated story. Can we have dinner brought in, first?"

## Track 62: Queensrÿche — “Eyes of a Stranger”

To my surprise, telling my tale to Dr. Tranh didn't take nearly as long as I expected. She was an excellent listener, and saved her questions until I had finished. If she had reached any conclusions, or had any opinions, she didn't share them with me. She did, however, arrange for me to have my right arm broken again and reset so it would heal straight lest I suffer nerve or muscle damage later on.

She got no objections from me. That was my sword arm, and with one good hand you're limited to playing either the melody on a piano or the bass line, but not both. 

This time, however, my arm didn't heal instantly. Whatever sneaky treatment Dr. Petersen had given me had run its course. Instead, I now had an arm encased in an old-fashioned plaster cast.

"I know it's low-tech," Dr. Tranh offered by way of explanation, "but it's dirt cheap, and we've found collecting signatures helps patient morale—which hastens recovery."

Dr. Tranh was first to sign. Jackie and Claire were next, and I made a point of thanking Claire for the manga. More Adversaries came to visit, and they all signed my cast before Edmund Cohen wheeled in a robotic arm while smoking a fat blunt while ignoring my doctor's protests.

"What the hell is *that* for?"

Eddie chuckled and blew a haze of smoke laden with second-hand tetrahydrocannabinol from his nostrils as he signed my cast. He took his time, letting his eyes linger a little too long over my chest and puffing like some species of dragon. *Dracolech erectus*, perhaps? "Malkuth wants to sign your cast, too, but he doesn't have any hands of his own. So I agreed to bring in this waldo."

"Fine. Malkuth, you be careful, all right?"

"Of course." He was as good as his word. His signature was surprisingly florid, and even came with a little Tree of Life. He laid down the pen when he was done, and took my hand. "Get better soon, Nims. We have a date, remember?"

Eddie chuckled, and tapped ashes into my latest vase of flowers. "How come I don't get a date?"

"I didn't want to get between you and your girlfriend Mary Jane." Coughing, I started waving away the smoke. "Could somebody please crack a window?"

"I've a better idea." Dr. Tranh plucked the blunt from Eddie's fingertips. Handling it as if it were radioactive, she dropped it into the toilet and flushed several times. By the time she had finished, the room's ventilation system had dealt with the smoke. "Light up another one of those here, Adversary, and I'll schedule you for a colonoscopy."

Eddie's horror-struck expression was priceless. "Not another one of those. I just had one."

"Not at this hospital you haven't." Tranh seemed to like the idea of having Eddie sedated so she could go spelunking with an endoscope. "You look like the sort of man who leads a shockingly unhealthy lifestyle, so I'm sure I'd find all sorts of interesting things. For science, of course."

Eww… That just sounded kinky. And Jackie looked like she was ready to suggest possibilities. Knowing her, it would start with a gerbil graveyard and get worse from there. "Can you two please get a room?"

Dr. Tranh stared at me a moment while Eddie gave her a once-over. "You think we're flirting?"

"I certainly well hope so."

Eddie chuckled. "Same here, Nims. So, Doc, when does your shift end?"

"I still have to finish my rounds. Think you can wheel this machinery out of here on your own?"

"I think I'll manage." And to Eddie's credit, he didn't seem to put in any effort. Maybe the old bastard's in much better shape than he looks.

More Adversaries filed in to sign my cast, which confused me. If I stood accused of abusing my authority, none of them would want anything to do with me even if I *was* innocent until proven guilty. Regardless, I wasn't going to begrudge the attention. What really shocked me, however, was the sight of Mike Brubaker coming to visit with Christopher Renfield in tow.

Mike looked pretty good for somebody who had gotten a sword through the lung. And Renfield wasn't bad, either. Just bad news. Best to put him behind me. 

Careful not to hit Mike with my cast, I gave him a one-armed hug. "Should you be up and about with your wound?"

"I'm fine. The exercise will do me some good. Is it true you were arrested?"

"Yeah, but you wouldn't think so given how nice other Adversaries have been to me. Normally we shun an Adversary who got busted for the slightest infraction as if they were radioactive child molesting bubonic plague carriers."

"Nice." Renfield shook his head. "What happens if the trial happens and the accused is acquitted. Does everybody pretend they didn't shun the poor bastard?"

"To be honest, I don't recall an Adversary accused of a crime and subsequently acquitted ever returning to duty." Not that I ever thought about why. Now that I faced the prospect of a court martial, I understood a little better. To be accused by your fellow Adversaries of being what you swore to oppose stings like betrayal.

Rather than dwell on it, I offered Brubaker a pen. "Want to sign my cast? There should be a clear space somewhere."

"Sure." His grasp was gentle as he added his signature to those preceding his. When he was done, he stood aside for Renfield.

Rather than sign, the old soldier leaned over me and stole a lingering kiss that left me wanting more. "You'll remember that after the cast comes off."

He was right. Before I could demand more, he placed a heavy package wrapped in pastel paper in my lap. "The Society crunched the numbers and decided we were owed back pay for guarding Fort Clarion even though the Commonwealth is long gone. Smart way for them to give us some working capital to get set up without it feeling like charity, if you me. The guys decided to chip in and put together a gift to thank you."

Rather than open the gift, I pushed it away. "I killed some of you, and wounded others."

Renfield shrugged and put the package back in my hands. "We're getting a chance to make new lives for ourselves because of you. So open the goddamn present."

"If you insist." Whenever I unwrapped gifts, I made a game of doing so with a little damage to the paper as possible. My foster mother does it as well. When I asked her why, as a little girl sitting amid a pile of shredded wrapping paper one Winter Solstice, she told me it let her re-use the paper to wrap other presents.

My fastidiousness seemed to amuse Renfield, judging by his smile, but it wasn't long before I had the brown cardboard box unwrapped. Opening it, I found books of sheet music containing a repertoire worthy of a serious pianist that spanned five hundred years of Western music. Taking the topmost from the box, I checked the index. I had played only a third of the pieces contained within. A third I had heard of, but hadn't attempted. The rest were obscure to me, but wouldn't remain so for long. "Thank you. This is a wonderful gift. I'd love to play some selections for you and the others."

"I'd like that. I think some of the other guys would, too. Drop me a line when you're ready, all right?" Rising from his seat, Renfield bent and kissed me again. This time it tasted like farewell, which was probably for the best. "Mike had something he wanted to tell you privately, so I'll wait for him outside."

Mike wouldn't sit down, but remained standing Renfield closed the door behind him. Rather than remark on it, I got to the point. "So, what's the secret?"

"I've decided against becoming an Adversary. Something about the Phoenix Society just doesn't feel right, and I think I can do more to help working outside their system." He didn't look at me until he had finished. "You should get out, too. The way you fought Dr. Petersen—it was like you were *playing* with him. I don't know if you had a mean streak before you took the oath, or if you got it on the job, but I don't want to be the sort of person you were a few days ago."

"I appreciate the advice." Which was a flat-out lie. I could understand him having doubts about the Society since I had my own. But to accuse me of torturing that bastard Petersen? Or was I ready to tear into Brubaker because he was right about me? Maybe I *did* like hurting people when I was sure they had it coming. "Anything else?"

"I might have said too much. You looked like you were ready to throw down." Mike shook his head. "Actually, I should thank you. I might have made two mistakes if you hadn't shown me what you're like on the job. I might have become an Adversary myself, and I might have fallen in love with you."

"Get out." I didn't scream, shout, or even sharpen my tone. However, something in my voice put him to flight despite his wound.

The rage that reddened my vision and made a sword of my voice didn't make sense at first. Sure, Mike was cute and I had respected his willingness to fight beside me in Clarion despite being an all-but-untrained civilian, but I wasn't interested in him. I wouldn't have wanted him to be interested in me. Regardless, his cold, calm rejection was a thrust from a far sharper knife than the ones I faced only a few days ago. The truth cuts deepest of all.

Renfield and Brubaker were my last visitors that day, and while a nurse brought me dinner I was otherwise left alone with my self-image in ruins around me. All I could think about was the introduction to moral philosophy all prospective Adversaries had to pass before they could face their final trials. The first day, we were confronted with Nietzsche's warning to those who fight monsters and stare too long into abysses.

Was that me? Was I becoming what I despised? Would I even recognize myself in a mirror, or would a stranger's eyes meet mine?

"I would say that your ability to consider the possibility proves that you haven't become the monster you set out to fight, but that would be a lie."

Reaching for a weapon as I opened my eyes, I scrambled out of bed to get away from the intruder. 

A snow-blonde CPMD+ man in a white suit with a navy blue cravat sat at the foot of my bed, regarding me with cobalt eyes. He favored me with the insouciant smile I recognized from a hundred sparring matches—and from photographs I had taken from Dr. Petersen's account on Clarion's town computer. "Dr. Ian Malkin, I presume. How did you get in here at midnight, without my noticing?"

His smile broadened. "I liked it better when you called me Maestro."

"I liked you better when I knew you as Maestro. And you still haven't answered my question."

## Track 63: Dream Theater — “In the Presence of Enemies, Part 2”

My uninvited guest—Ian Malkin, Maestro, or the Devil himself for all I knew—favored me with an indulgent smile. "Call it sufficiently advanced technology. Or magic, if you prefer. It doesn't matter to me either way."

"How dare you claim me as your daughter?" Though I had heard everything he said after that, it seemed irrelevant in the face of his outrageous claim.

"I gain nothing by lying when the truth is just a couple of cheek swabs and a lab test away. Shall we?"

"No, that won't be necessary." Not that I wasn't curious, but this was one situation where I preferred to cope with the doubt over living with the certainty. Besides, it didn't matter. This asshole didn't raise me. He didn't love me despite my turning out to be someone other than the person he hoped I'd become. "Even if you did fuck my mother, neither you nor she could be bothered to actually do the hard part and be my parents."

My accusation actually seemed to wound Malkin, for he fell silent and studied me. The years seemed to weigh more heavily on him despite a visage that suggested he was far too young to have been involved in Nationfall. When he spoke again, he seemed pensive and almost weary. "Had I chosen a different path in life, nothing would have pleased me more than to have been your father. But long before I met your mother I devoted myself to a cause that grants me only rare moments of respite. Had I kept you, I would have relegated you to a succession of nannies and tutors, and you still would have grown up not knowing me. You would have lived under guard, knowing all the while you were a potential bargaining chip to be used against me. I could not in good conscience have condemned you to such an upbringing."

Good conscience? Is this bastard serious? "Malkin, I don't believe for a moment that you ever had a conscience."

"Perhaps not, but consider this: your mother left you to die in a dumpster." Malkin spat the words, and locked his eyes on mine. "She didn't want a demifiend's daughter. It would have been easy enough for me to leave you where I found you, or perhaps even finish the job."

Hadn't they been blue before? Now they were as red as mine. "What happened to your color contacts?"

"I have other ways to hide my true colors." It was impossible to do anything else. Christopher Renfield now lounged in the chair Malkin had occupied only a moment ago, wearing a dress uniform I had never seen the actual Renfield wear. A moment later, I stared across the bed at Colonel Petersen. He saluted, and then shifted back to the snow-blonde dandy in white I had called Maestro. Only now there was two of them. Both spoke together. "When I called myself a demifiend, I was not entirely accurate. My father was a demon, and I was born to an asura mother as you were. I went on to become a demon myself, for reasons that need not concern you."

"A demon? It's the end of the twenty-first century, and you expect me to believe in *demons*?"

Looks like Malkin got tired of the theatrics. There was only one of him, and his eyes were blue again. Thank goodness. I was starting to think I had gone round the bend. Either that, or the painkillers had me tripping balls. "Whether you believe or not is immaterial. Project Harker would have happened without the involvement of Henrik Petersen and Dusk Patrol. The fact that this colonel had created an all-CPMD+ special forces unit was merely an irresistible opportunity."

"An opportunity for *what*? Why would you want to turn CPMD+ people into psionic super-soldiers?"

"You would not believe the truth, since you don't believe that I am a demon, but I will tell you anyway for my own amusement. I intend to create soldiers capable of killing the demon who pretends to be God."

A demon who pretends to be God? And Ian Malkin wants to create a what? A *deicide* squad? "You're right. I *don't* believe you. But since you admitted to conspiring with Dr. Henrik Petersen and conducting unethical scientific experiments on human subject who did not give informed consent, I—"

"Choose your words with exacting care, Naomi. Your idealism has proved amusing thus far, but do not try my patience. You do not yet realize your peril." The menace in his voice sliced the air and in that moment I believed. Ian Malkin was a demon. "I am aware of everything that has happened at Clarion since the inception of Project Harker. Most of my fellow members of the Phoenix Society's executive council is as well, with the exception of a couple of junior members we keep close to ensure their complicity."

My fellow members? Oh, fuck me and marry me young. Ian Malkin's on the bloody XC. "So you've had the authority to block my investigation at every turn. You're the reason Malkuth wouldn't tell me anything. You could have stopped Petersen from activating GUNGNIR, but you were prepared to let him massacre twenty thousand people."

"Twenty thousand is trivial compare to the gigadeaths for which I am already responsible." The matter-of-fact tone with which he made this claim precluded any hope of this being more than his bravado. "Once you were in too deep, I decided that rather than persuading you to leave Clarion alone I would draw you in deeper and see just what you were made of. I wanted to see you in action, to see if you were the one I hoped to create."

The one he hoped to create? This shit just keeps getting deeper, doesn't it. "Who was I supposed to be?"

"That is irrelevant. You are not the one I hoped to create." Magnin began to pace, but kept his eyes on me as if he expected me to strike at him. "I had hoped that like some of the Project Harker subjects, you might prove to be a flowseeker, that you might manifest your psychoenergetic talent under sufficiently extreme duress. I expected GUNGNIR to be the trigger. After activating the system and setting its target, I had locked it down to prevent you from simply jacking in and aborting the launch."

"But I did it anyway, thanks to the tools I found on Tetragrammaton." Damn, I would have to tell Cat Tricklebank that her husband helped save the whole goddamn town. "What did you think I would do, shatter those tungsten carbide rods in midair with my voice?"

The corner of Malkin's mouth crooked as if he wanted to smile, but wouldn't let himself. "That was one possibility. Instead, you found another way to carry out your duties, and played the nightingale in one of the North American Commonwealth's black sites. I could not permit you to finish your duel with Dr. Petersen and bring him to trial, and had you not reached out and touched a guard with your song, you would be there waiting for a court martial that would never come."

It would have been my own personal Chateau d'If. The very notion left me shuddering. "So having me arrested was your idea? Who were the Adversaries you sent to do the job?"

Now Magnin smiled. "Adversaries? More like actors. Call them Rosencrantz and Guildenstern if you must associate names with faces. It is my fond hope you will ever see them again."

"Why is that? Don't want to have them assassinate me?"

Just a second ago, I had been standing in a Philadelphia hospital room interrogating a madman who claimed to be one of the hands pulling the Phoenix Society's strings. Now I had no idea where the *hell* I was. All was darkness around me. The only light was a distant star, so faint that it barely lit the huge dirty snowball tumbling beside me. 

Wait. That's what comets are supposed to be. Dirty snowballs. Something bumped into me. Something that could have been a man, his mouth frozen in a rictus. Had I seen him before somewhere?

Before I could scream into the void, I was back in the hospital. Malkin's smile was thoroughly malicious as he regarded me. "If I wanted you dead, Naomi, I would have left you in the middle of the Oort Cloud. Nobody would ever find you out there. You'd just be another meat popsicle."

"What kind of fucked up magic do you do that lets you teleport people into deep space and bring them back alive?" I blurted the question, desperate to grasp at some kind of sense despite being strapped in for a ride on the crazy train. "If you're a demon who can do everything I've seen you do, why don't you rule the world?"

"What makes you doubt that I already do?" Malkin's voice held the quiet confidence of an attorney who had finished delivering an unassailable argument. Sometimes the facts speak for themselves when laid out properly, with no need to give a jury the hard sell. "I don't need to announce myself to the people. In fact, doing so would be counterproductive."

"Everything I've seen and heard has been recorded via Witness Protocol. I can expose you."

"And who controls Witness Protocol? Who has root on the Sephiroth?" Magnin paused, letting his questions strike with the impact of depth charges. "Even if I consigned you to the cold ever-night of interstellar space, you have friends who would demand answers. If I made *them* disappear in turn, I'd only turn more people against me. So, here is how it will be. Your debt is paid. You need no longer serve as an Adversary. The Phoenix Society will celebrate you as a heroine who exposed an old conspiracy, solved multiple murders and disappearances, and saved a town from orbital bombardment—all while on vacation. With the bonus you'll receive, you will have no trouble striking out on your own, though a patron can also be arranged if you wish it."

"What's the price?" There had to be a catch. There's no way this bastard would show he was capable of leaving me in deep space and then offer my fondest desire.

"All you have to do is shut up and play the modest, humble heroine mouthing bromides like the privilege of service. That was Henrik Petersen you bumped into, by the way, and it was time Ian Malkin died as well. A sudden stroke, perhaps. Just walk away and keep your pretty mouth shut."

The hell of it was, I could do it. It would be easy, now that Malkin had proven that the Phoenix Society was rotten. I might owe my fellow Adversaries, but at the same time I had no right to shatter *their* faith. I'd be like a newly minted atheist trying to convince his still-devout neighbors that there were no gods. "What if I refuse, and try to expose you?"

Malkin sighed, as if he expected me to insist upon seeing the stick as well as the carrot. "If you attempt to bring me or Dr. Petersen to trial, I will arrange for the record to reflect that you were in fact arrested for abuse of authority. A court martial will find you guilty on all counts, and condemn you to the guillotine. You will then be made an unperson, your name erased from existence in every way that matters."

## Track 64: Blue Öyster Cult — “Out of the Darkness”

The choice was simple enough. If I made myself complicit in Ian Malkin's conspiracy, I would get a shot at the life I had always wanted. If I kept my oath and held true to my ideals, my only reward would be the ignominious death of a traitor.

It would have been reasonable to consider the possibility that even if I was convicted, enough people might still believe me and believe *in* me, and hold true to the same ideals. They might work and struggle to expose the truth, exonerate me, and expose the rot at the Phoenix Society's blackened heart.

But I wouldn't be alive to see it. I'd just be a martyr, a holy name with everything that made me a person hidden under as many coats of whitewash as those rallying around my image needed. What would that accomplish, besides bringing down the organization that rebuilt the post-Nationfall world?

Whatever would rise from the Phoenix Society's ashes wasn't likely to be a better deal for the people than the current regime. A return to the old regime of warring nation-states where the strong exploited the weak and called it "sound economics" wasn't even the worst scenario I could imagine. The real nightmare scenario was the snow-blond dandy before me taking off the kid gloves, declaring himself openly, and demanding absolute obedience on pain of death. 

So that was how I sold my soul. There was no hesitation, and no doubt in my mind as I took a breath and gave my answer. "I don't get paid enough for this shit."

Malkin chuckled at my reply. "No, you most certainly don't. But you'll find in time that you made the right decision."

Before I could say anything else, he disappeared in front of me. Now you see him. Now you don't. Nothing for it but to go back to bed, since it was long after midnight. Not that the night had been young when Malkin showed up.

Though I had expected to spend the remaining hours until breakfast awake and staring at the ceiling, my eyes slipped shut and sleep claimed me as soon as I had the blankets wrapped around me. Dr. Tranh discharged me the next morning, and Jacqueline and Claire were there to meet me at the entrance.

"Oi, Nims!"

"Hi, Jackie." Letting her hug me, I patted her shoulder with my good hand. "Hello, Claire. I understand you're going back to London with your aunt?"

"Yep. Did you know Jackie's getting married?" Claire didn't look too keen on the idea. "She wants me in her party. I gotta wear a dress, dammit."

"Actually, Nims, I meant to ask earlier. I could use a maid of honor."

"I'd love to." It was as easy to say yes to Jackie as it had been to Ian Malkin. Easier, perhaps. I'd want Jackie on my side if I ever found a man I wanted to marry. "But before we head back to London and start preparations, can we get breakfast? I'm bloody famished."

"I got maglev tickets with breakfast service. Let's go."

It was the first time I had eaten something out of the maglev's dining car, and breakfast was shockingly good. The only difference between our breakfast and what we might get at a pub that served breakfast was that there wasn't a screen showing a local football match. We passed the rest of our ride in companionable silence, and I refrained from noting Jacqueline's pensive expression.

Sure, Claire seemed to be absorbed in some kind of tactical game that had her defending various locations against war pigs from outer space. However, I suspected she didn't have the volume cranked up that high. Her headphones would be no guarantee of her not being to hear us.

Perhaps I was only being meddlesome, but it seemed easier to focus on my friend's problems than the clusterfuck I had just made of my own life. In the cold light of day, it was clear now that I had agreed to spend the rest of my life, which could be long indeed, living a lie. Regardless, something was bugging Jackie. 

Though I couldn't let it go, I knew better than to be direct. She was as bad  as some men when it came to emotional talk. Then again, I probably wasn't much better. Once we had returned to her house and put the baggage aside to unpack later, I let Jackie press a gin and tonic into my hands. "Thanks for coming to meet me at the hospital."

She mixed one for herself, sipped it, and put the gin away. "No worries. We were in town, and planning to head back anyway. How's the arm?"

"Itches like hell." At least that meant it was healing. "I love that so many people wanted to sign my cast, but the sooner I can swap this for something that'll let me scratch the happier I'll be."

Before Jackie could say anything, little Claire burst into the kitchen and hugged Jackie with such force that the poor girl ended up wearing half her aunt's drink. "Aunt Jackie, I *love* my new room. It's so huge. Thank you! Can I show Aunt Nims?" 

Her face screwed itself into a distasteful expression as she sniffed, and ran a hand through her damp auburn curls. "Bugger me with a rusty bazooka. Did you just spill your bloody drink on me?"

Chagrined, Jackie put aside her glass and reached for a towel. "Sorry."

"Claire, you *did* pounce on your aunt out of nowhere. You should give people warning."

"Sorry." She actually looked chastised for half a second before grabbing my hands. "C'mon, Aunt Nims. You gotta see my room. It's awesome."

Accepting the inevitable, I let Claire lead me from the kitchen as I looked over my shoulder at her aunt. "I'll be back in a tick, already? You can finish my drink if you want."

That set to Claire to giggling. "Ooh, an indirect kiss. Aren't you two a bit old for that?"

Rather than protest, I decided discretion was the better part of valor and kept my mouth shut as she led me upstairs. It wasn't until we had reached the attic that I said anything. "Claire, there's nothing here. It's just a big empty space."

"Yeah, but think of the possibilities!" She ran from one end of the attic to the other, opening all the windows and practically bouncing with delight. "Sure, the only furniture we'll be able to get up here will probably come from IKEA, but who gives a toss? I'm just a fuckin' kid; I don't need nice furniture that I'll pass down through the generations."

"That's a fair point." A cool breeze circulated through the attic with all the windows open, which felt delightful. "Lots of room for toys, too."

"And privacy, too. If I stake my claim now I'll have the whole floor to myself. If Aunt Jackie and Reverend Ronnie have kids like they want to, they'll have to make do with rooms downstairs. I'll be a teenager by the time they're old enough to be a pain in my arse, but since I can pull the ladder up and lock the entrance from inside, they won't be able to invade my privacy."

Damn, she's thinking ahead. Not that I was an expert in child development, but that kind of long-term thinking didn't seem common in children Claire's age. She was definitely going to be a handful for Jackie and her husband. "I'd love to see the place again once you've got it all set up."

"Cool." She closed each of the windows, though she had to stand on tiptoe to do so. What kind of life had she had with her mother that drove her to become so independent so soon?

Jacqueline was still in the kitchen when I returned, putting together a beef roast to go in the crock pot. "So, is Claire really set on having the attic?"

"Yeah. She figures it'll keep her cousins out from underfoot when she's older. Is it me, or is Claire unusually precocious?"

Jackie shook her head. "It's not you. She's fine now, but when she was younger she had gelastic epilepsy. She had to have a tumor removed."

"Poor kid." Checking the network, I looked up the condition she mentioned. "She isn't going through puberty already, is she?"

"Christ, I hope not. She's still a bit young." She chuckled as she set the slow cooker's timer. "Though she's looking forward to it. She's in a hurry to grow up. Damned if I know why, though."

And here was my cue to return to the conversation I meant to have before Claire had burst in. "Has something been bothering you? You were quiet the whole ride back."

She shrugged. "I submitted my resignation. You're going to need a new partner once you're back on the job."

What the hell? I thought Jackie loved being an Adversary. "Wasn't being an Adversary your dream as a kid? You told me it was as close as you could get to being a hero."

"Yeah, but I've put in a couple of years and haven't done much good. Sure, we've saved some lives and ended some abuses, but it's like being a janitor. As soon as you clean a mess, somebody makes a new one. This shit never ends. You know what I mean?"

Yeah, I did. Hearing that Jacqueline wanted out made it easier for me to open up. I gave her a one-armed hug. "I know exactly how you feel. I'm going to tender my resignation as well. Being an Adversary was never *my* dream, but I'll never make my dream real if I don't put away my sword. I don't blame you for wanting out."

"Bloody hell, that went better than expected." Now, *that* was the Jacqueline I recognized. "You want to stay for dinner tonight? We gotta talk wedding plans, and I was kinda hoping you'd be my maid of honor.”

## Track 65: Thank You Scientist — “My Famed Disappearing Act”

It had been almost nine months since Jacqueline invited me to stay for dinner. I ended up moving in with her at her insistence. It was a huge house which she owned outright, so as long as I kicked in some dosh for food and other bills and pitched in with the housework I was welcome to stay. Trust me, it was a godsend, and didn't carry the stigma I might have felt if I had moved back in with Howell and Sophie on the farm.

The time simply flew by. Jacqueline and I tendered our resignations amid news that Ian Malkin had committed seppuku and left a note admitting to abusing his position on the Phoenix Society's executive council to cover up what had been happening in Clarion. It seemed unbelievable, given what I had seen the night I agreed to back off, but Jackie and I saw the body. He was dead with his belly sliced open. 

His will specified that his entire fortune was to be spent providing restitution for those who had suffered as a result of his crimes. For some reason, that included me. It would be foolish of me to name the sum, but the money is safely invested and paying a modest dividend. 

Though it felt like blood money, I didn't quibble. Not when I could finally afford to focus on my music, which I've been doing when not helping Jacqueline plan her dream wedding. 

Claire wasn't too keen on being part of the bride's party until her aunt revealed that it was going to be a gender-bending wedding. The women would wear tuxedos, and the men would wear the dresses for a change. Not that getting Claire fitted for a tux was any easier than having her fitted for a dress, despite her being much more cooperative. She kept growing, which buggered the measurements on a regular basis.

Kaylee had been kind enough to bring my Conquest back, and we've stayed in touch since she decided she liked London better. She opened a new Shiny Hobbies shop on Piccadilly Circus of all places, outrageous rents be damned, and actually managed to make a go of it so far. Claire helps out on weekends. So do I; it turned out I rock an awesome Cecilia Harvey cosplay even if the original character didn't have a badass red motorcycle.

Cat Tricklebank had settled in with her family back in southeast Australia. It took me a while and some help from Claire, who was turning to quite the little hacker herself, to track her down. She was alone when she met me atop the Rialto in Melbourne, dressed in mourning black, and fixed an accusing glare on me. She wasn't aiming it at me yet, but she had a semiautomatic pistol in her hand. "You let my husband die."

"Yes, I did. Had I suspected for a moment that he was in danger, I would have done everything I could to persuade him to take you and get the hell out of Clarion. I'm sorry, for what little that's worth."

"Did you come here to just to apologize? If so, you've said your piece. Leave now, while you still can."

At least she had put the gun away. But there was more she needed to hear. "I came to do more than apologize." 

Cat had turned away, to gaze at the city, but eventually shrugged and faced me again. "What is it? Going to tell me he was involved, too?"

"No. He created an experimental tool that I used to stop the GUNGNIR satellite from bombarding Clarion. Without his help, I might not have been able to save anybody. I don't know if you encouraged him in his hacking, or just stayed out of his way, but thank you."

Cat's reaction wasn't what I expected. Collapsing onto a bench, she buried her face in her hands and began to sob. Sitting beside her, I put an arm around her and let her get it out of her system. When she had composed herself again, she spoke softly while gazing out over the city. "He wanted to be an Adversary, but didn't make the cut. He had the brains, but he was too nearsighted and too stocky. So he settled for making software Adversaries could use. The Phoenix Society paid him well for his work, but he would have loved to have heard you say that his code helped you save lives."

"I wish I could have told him in person. Is there anything I can do to help?"

Cat shook her head. "I have a job, his life insurance payout, and there was a bequest from that bastard Ian Malkin. I won't have to worry about money."

Knowing Cat Tricklebank would at least have financial security was a weight off my shoulders. Bringing her a measure of peace made my return to London easier; it was one of the few pieces of unfinished business I had remaining. 

Another greeted me in New York while I waited at Grand Central Terminal for my maglev to London at a little cafe off the main concourse. Mike Brubaker had taken to wearing severely cut suits that looked good on him. "I didn't expect to see you in the city, Adversary Bradleigh."

Inviting him to join me with a gesture, I waved over the attendant. "Please just call me Naomi. What you said got me thinking, and I decided that I didn't really like the person I was with a sword in my hand and pins in my lapels any more than you did. And how are *you* doing?"

"Wait." Mike stared at me a moment. "You quit the Adversary corps? Because of me?"

"Don't take all the credit. You just got me thinking. I found other reasons quickly enough. Did you hear about Ian Malkin?"

A satisfied smile curved Mike's lips. "Yeah. Can't help but think he was just the fall guy, though. It's too bad he couldn't have been brought to trial so that everything could be exposed."

Time to change the subject lest my own complicity in the coverup become apparent. "So, what have you been doing with yourself?"

"I'm in finance getting my hands dirty. Once I've got fuck you money I'm going to go into law. I want to see if I can advocate for individual rights without taking up the sword."

Kissing him was a sweet mistake. Letting him kiss me back was sweeter still. Leaving the concourse with him to get a hotel room was sweetest of all. The only words spoken were his, a single sentence as I undressed him and pushed him down on the bed: "I haven't been able to get you out of my head."

When we were done, I tucked him in and left a note that became a long letter—or possibly a confession. He said he loved me before drifting off, but that wasn't what I needed. It wasn't what *he* needed. He needed to continue to work toward his dream, to achieve a sense of financial security that would let him live up to his ideals without compromise.

As for me? I had dreams of my own to chase. Besides, I had to be back in London for Jackie's wedding. That thought was foremost in my mind as I walked the darkened streets of Manhattan in the cold gray hours just before dawn. 

However, the night wasn't mine alone. Someone pursued me, someone who took care to match their pace with mine so that our footsteps blended together.

Halting halfway to Grand Central Terminal, I turned to face my stalker, and found myself meeting the golden eyes of a raven-haired CPMD+ woman as tall as I was draped in a black fur coat months out of season. Though she didn't appear to be armed, something about her bearing unnerved me. "Who are you?"

The other woman greeted me with the slightest nod, but for some reason that small gesture felt as if she had bowed to me. "Good evening, Ms. Bradleigh. It was not my intention to frighten you. You may call me Theresa Garibaldi. Your father told me you had blossomed into a pale rose before his unfortunate passing."

Something about Garibaldi rubbed me the wrong way. "Are you the bitch who left me to die in a dumpster?"

Electricity seemed to gather around me, and I leaped backward. Good thing I did, too, for lightning struck the sidewalk where I had stood moments before. "No, Naomi. I'm the one who incinerated that bitch because your father Imaginos was too soft-hearted to do so. I am the ensof Thagirion."

Wonderful. Another demon. Just what I needed. "I apologize for my rudeness, but why contact me?"

"Accepted." Thagirion's burgundy-tinted lips curved in a slight smile. "Your father told me you're a dramatic coloratura soprano in need of advanced training."

Now, what could a demoness capable of calling down lightning teach me about music? "What sort of training?"

"The esoteric kind." She glided toward me, and patted my cheek with a gloved hand. "What if I told you I could teach you to sing with a siren's voice and captivate your audience? While imprisoned, you manifested a rare talent. I am the only one who can teach you to master it."

"What's *your* price?"

"There is no price. I want to see what you do with your voice once you've learned to use it to its fullest effect." She withdrew, and her smile turned ironic. "You still look doubtful. What might I say to convince you that I shall never hold you to any debt or obligation toward me? Shall I swear it on the Styx like a goddess of old?"

Nice try, but I'm not a Hellenist. Besides, I had just uploaded a clip of the last few minutes to Port Royal. "There's no need for oaths. Just understand that I can expose you if I wish. My death likewise means your exposure."

Her laughter was the melodious peal of church bells. I glanced around, sure she would draw attention, but we still had the dark street to ourselves. "Oh, you truly are your father's daughter. Now, when shall we begin?"

"Is there a way I can contact you? I was supposed to be back in London hours ago."

"I understand." She handed me a business card. "Call me when you're ready to begin."

Before I could examine the card, or say anything, there was a disorienting sense of *discontinuity*. It was like I had become unstuck in space and time. A moment later I was outside Jackie's house in London. Blinking against the sudden afternoon glare of sunlight, I slipped Thagirion's card into my pocket and opened the door. Thank goodness I was only going to make Jackie late for her hen night! "Sorry! I'll be dressed in a tick!"

## Bonus Track: Blue Öyster Cult - "Astronomy"

NEW SLEEPING SUN VOCALIST CAPTIVATES BUDOKAN IN WINTER SOLSTICE DEBUT
*Dark Eternal* (https://earth.com.news.music.darketernal)  
by Jethro Cooper  
22 December 2096

**Sleeping Sun** might be one of the first classical heavy metal revival acts to rise from the ashes of Nationfall, but of late their star has faded thanks to a succession of lackluster vocalists. These ladies, who shall go unnamed, had the chops but not the presence necessary to bring Sleeping Sun to the level of its inspiration Nightwish. 

Readers of *Dark Eternal* might argue that is unfair to hold vocalists in a revival band to the standard set by Tarja Turunen, let alone her successors Annette Olzon and Floor Jansen. This is a fair point, which I can only rebut by saying that the whole point of a revival band is to offer modern audiences an experience as close to the original band as possible.

Newcomer Naomi Bradleigh of London seems to understand this. Despite my initial reluctance to attend Sleeping Sun's Winter Solstice show at the Budokan in Tokyo, I was curious. Was Sleeping Sun really that desperate that they'd hand the mic to an unknown? Who the hell was Naomi Bradleigh?

It was my job to find out, but it soon proved to be my pleasure. An equipment failure crippled the band, and would normally have delayed the show while the band's sound crew figured out what had gone wrong. This tends to annoy the fans, especially if the venue's management can't get on the PA and explain that the band has run into technical difficulties, as was the case last night.

Instead of leaving the fans waiting, Ms. Bradleigh took the stage alone. I don't know how she did it, but when she spoke, everybody could hear her despite her not having a working microphone. Moreover, she bowed and addressed the crowd in Japanese.

Being the ignorant gaijin that I am, I had to ask somebody to translate. This was what she said. Apparently she was as fluent as a native speaker.

> Good evening, Nippon Budokan. I'm Naomi Bradleigh, and this is supposed to be my first performance with Sleeping Sun. Unfortunately, we've run into some trouble with the equipment which we expect to resolve shortly. We appreciate your patience, and humbly apologize for keeping you waiting. In the meantime, will you permit me to entertain you?

I don't know how she did it, but I haven't never heard a crowd go that wild before. I was going ape myself, and all she was doing was apologizing on behalf of the band and venue, but it felt as though she were humbling herself for me personally.

Once the venue was quiet, she began. I hadn't noticed it, but some stagehands had wheeled out an old baby grand piano for her, and she had sat down to play. Again, the whole venue shouldn't have been to hear her play, but every note was as clear as if I were standing right beside her. 

But she was smart. Instead of singing, she only played at first. I only recognized a couple of selections from Beethoven and Chopin at first, but once she shifted into the piano intro to "Astronomy" by the Blue Oyster Cult I *knew* something was up.

Right on cue, the lights came up and the rest of the band joined in as Ms. Bradleigh sung the opening lines. And here's the really fucking *weird* part, the part you won't believe unless you were there that night. Even though everybody else played amplified instruments, and even though Ms. Bradleigh didn't have a mic, I could still hear her sing, and I could still hear her piano, *and it was if she were singing and playing just for me.*

You read it first at Dark Eternal, folks. Naomi Bradleigh won't last long with Sleeping Sun. That band is too small a stage for a pianist and vocalist of her caliber. I don't if she's a goddess, some kind of demon, or just an preternaturally skilled young woman—but if you get a chance to see her perform, you owe it to yourself to take it.

*fin*

Matthew Graybosch  
Harrisburg, PA  
15 June 2014—30 August 2015

## Glossary

#### Advanced Catacombs & Chimeras

* The premier tabletop game for geeks who can't be bothered with fantasy football.

#### Adversary Candidate School

* A four-year military academy dedicated to training young men and women to serve the Phoenix Society as Adversaries. Also provides training in law, medicine, psychology, computer science, and forensic accounting.

#### Adversary

* An officer of the Phoenix Society tasked with the investigation of allegations of abuses of power in organized religion, politics, and business. They have the authority to hold any individual accountable, but this near-absolute authority is balanced with overwhelming responsibility: abuse of power by an Adversary is the one crime punishable by death. The method of execution depends on the severity of the Adversary's breach of the public trust. The guillotine is the easy way, but one ex-Adversary was not only impaled, but had his name erased from all records.

#### Agni Burger

* An upscale fast food chain specializing in its eponymous take on the hamburger, spiced ground lamb wrapped in fresh tandoori naan and served with a variety of traditional Indian dishes on the side. Reputedly founded and operated by Agni, the Hindu deva presiding over fire. If you can eat their entire Kali Yuga Special three times, you'll never have to buy another meal at Agni Burger again. You'll also never have any privacy again.

#### AsgarTech Corporation

* A technology firm located in the Antarctic domed city of Asgard, on Ross Island near Mt. Erebus. It deals in both hardware and software, and produces a wide variety of devices and applications.

#### Borgia Pizza

* A chain of pizza parlors named for Rodrigo Borgia and his infamous children, Cesare and Lucrezia. They won't punish customers who complain about poor service. Those guilty of *providing* poor service, on the other hand...

#### Cecilia Harvey

* Female protagonist of *Last Reverie IV*, whose plot is essentially the same as *Final Fantasy IV*. (The real game's protagonist is named Cecil.)

#### Charn

1. A progressive rock band from Clarion. Their closest real-world equivalent would be UK neo-prog outfit [Jadis](http://www.jadis-net.co.uk/).
2. In the *Narnia* books by C. S. Lewis, Charn was the world in which the white witch Jadis was imprisoned prior to Narnia's creation. Charn was a barren wasteland after a war for supremacy Jadis claimed to have won by speaking the Deplorable Word (Which I doubt was "Megidolaon").

#### Clarion

* A small town in Western Pennsylvania, in the Allegheny Mountains. The author took outrageous geographic liberties.

#### Conquest Motorcycles

* You can get any color motorcycle you want from Conquest, as long as you like black.

#### CPMD

* Congenital pseudofeline morphological disorder, a fictitious genetic condition first "discovered" by researchers at Ohrmazd Medical Group. CPMD+ individuals resemble humans from a distance, but can display colorations differing from the human norm as well as feline characteristics like slit-pupiled eyes. CPMD+ individuals cannot have children with CPMD- individuals, which proves that CPMD+ people are not the same species as *homo sapiens*.

#### Damascus steel

* A type of high-quality steel used in Indian and Middle Eastern swordsmithing whose original production method is lost. Certain modern types of steel have proven superior to Damascus steel, but the original was extraordinary for its time due to its chemical composition.

#### Dante

1. Italian poet, author of *The Comedy* And *La Vita Nuova*.
2. A little black kitten with one white whisker who lives at The Lonely Mountain with his brother Virgil. They get scraps from patrons by means of purr-begging.

#### Demifiend

1. A word describing a person whose ancestry is half-demonic. Similar to 'demigod'.
2. The silent protagonist of the 2003 ATLUS RPG *Shin Megami Tensei: Nocturne*

#### Doomed Space Marines

A thrash metal band from Brooklyn. Their name comes from an Easter Egg hidden inside an old computer game.

#### Dusk Patrol

* An elite special forces unit composed entirely of CPMD+ soldiers, nominally attached to the North American Commonwealth Army's Third Infantry division. Personally commanded by Colonel Henrik Petersen, these soldiers soon gained a fearsome reputation for successful night-time missions despite near-impossible conditions.

#### Eight Immortals Buffet

* A Chinese buffet named after the Eight Immortals of Taoist tradition.

#### Ensof

* From *ein sof*, a Hebrew term for the divine origin of all created existence. The Ensof are post-biological intelligences who live inside stars, feeding off their thermonuclear reactions, and possess capabilities most easily described as "godlike". They cannot be killed by normal weapons, though their avatars can be destroyed.

#### Executive Council

* The ultimate authority in the Phoenix Society. Nobody knows who sits on the XC, which makes them unaccountable.

#### Fireteam

* [A small infantry sub-unit consisting of four or five soldiers in various roles.](https://en.wikipedia.org/wiki/Fireteam) Fireteams often work together, covering each other as they advance through hostile territory. In the real world, the fireteam is the primary unit upon which the US, British, Canadian, and Australian militaries' infantry is organized.

#### Frigga's Loom

* Named for Odin' wife in Norse mythology. Possibly owned and operated by the goddess herself.

#### Gilgamesh

* Sumerian hero-king, ruler of Uruk, who set out to find the secret to immortality in grief over the loss of his friend Enkidu.

#### Global thermonuclear war

1. If this happens, we're all fucked.
2. Are you sure you wouldn't rather play a nice game of chess?

#### Gnostic metal

* A subgenre of heavy metal characterized by lush, complex, highly technical music and lyrics influenced by Gnostic spirituality, alchemy, and occasionally Freemasonry. Inspired by real-world Swedish band Therion's albums *Vovin*, *Deggial*, *Secret of the Runes*, *Lemuria/Sirius B*, and *Gothic Kabbalah*.

#### GAEBOLG

1. In the Ulster Cycle of Irish Mythology, the [Gáe Bolg](https://en.wikipedia.org/wiki/G%C3%A1e_Bulg) was the spear wielded by the hero [Cú Chulainn](https://en.wikipedia.org/wiki/C%C3%BA_Chulainn). Once thrown, it had to be cut out of its victim before it could be used again.
2. GAEBOLG is the codename of an [orbital kinetic strike](https://en.wikipedia.org/wiki/Kinetic_bombardment) platform developed by the United Queensreach of Great Britain in response to intelligence suggesting the North American Commonwealth was hard at work on a similar system codenamed GUNGNIR. It was still in orbit as of the events of *Silent Clarion*, and under the Phoenix Society's control.

#### gaslighting

* A form of psychological abuse in which true information is spun, twisted, edited to favor the abuser, or false information is used, to make the victim doubt their own perception, memory, and rationality. An abuser may simply deny that previous abusive incidents ever occurred, or even go as far as staging bizarre events to screw with the victim's head. It is common in relationships where one party has power over the other. The term comes from the classic British play *Gas Light*.

#### GUNGNIR

1. A spear carried by Odin, king of the Aesir. Loki obtained it from the dwarves while making restitution for his giving the goddess Sif an unwanted haircut.
2. The world's first successful orbital kinetic strike platform, developed by the North American Commonwealth, and the only one ever used. During Nationfall, GUNGNIR was moved over the town of Clarion, and dropped three lances into a crowd of unarmed protesters marching toward the base.

#### Hell's Kitchen

* A neighborhood in New York City, reputedly home to a blind lawyer who dresses up at night and beats the fear of Hell into slumlords.

#### Hellfire Club

* Kinda like the Hard Rock Cafe, but with better food, and complimentary hookers and blow for high rollers. What happens here, *stays* here. Kinda like Las Vegas, Lothlorien, and the TARDIS.
* A set of exclusive clubs established in eighteenth century Britain and Ireland.

#### House of Lords

1. The unelected upper house of the United Kingdom's Parliament, filled mainly with members of the British Peerage and high-ranking clerics of the Church of England, the House of Lords acts as a check on the House of Commons While they cannot outright veto acts by the lower house, they can greatly inconvenience the Commons should they choose.
2. An American hard rock/glam metal band from Los Angeles debuting in 1988 with a self-titled LP.

#### Ian Malkin

* A civilian consultant involved with Project Harker. After Nationfall, he became an authority in medicine and biotechnology.

#### Implant

* A highly miniaturized biocomputer that can be implanted into an individual's head by hollowing out the mastoid process. Implants run a Unix-based operating system, are controlled by neural impulses generated by conscious thought, are powered by blood sugar, and provide a variety of useful computing functions, including text communications over secure links, detailed diagnostic information, and targeting assistance. They are also used to implement Witness Protocol.Implants are customized to minimize the probability of rejection, which can have serious adverse consequences.

#### Jeffersonian

* Pertaining to Thomas Jefferson, author of the Declaration of Independence. Though he wrote that all men are created equal, he forgot to include women, and also forgot to free his slaves. A case of lofty ideals not realized, to put it charitably.

#### Juilliard Conservatory

* Located in New York City near Lincoln Center.

#### Kaylee's Shiny Games, Hobbies, and Crafts

* A shop in Clarion owned by the first person in town to befriend Naomi. I was watching *Firefly* when I wrote that bit. Sue me.

#### Keep Firing, Assholes

* A band from Clarion. Their first album was called *Who Made That Man a Gunner?!*. Every member is an asshole, and so are their road crew.
* A famous line spoken by [Rick Moranis as Dark Helmet in Mel Brooks' 1987 comedy, *Spaceballs*](https://www.youtube.com/watch?v=PNcDI_uBGUo).

#### Lady Frostmane

* A distant cousin to Lady Snowblood, protagonist of a few classic samurai movies that helped inspire a Tarantino epic called *Kill Bill*. Also, a reference to an older version of Naomi Bradleigh, who used to wield a katana.

#### Last Reverie

* The developer thought it would be their last title. Instead, it was the foundation of an empire.

#### Liquid Swords

* A highly-regarded 1997 hip-hop album by Wu-Tang Clan artist GZA.

#### LONGINUS

1. Codename for an orbital kinetic strike system developed by the European Union in response to the space arms race between the North American Commonwealth and the United Queensreach of Britannia.
2. A name given to the Roman soldier alleged to have pierced the side of Jesus of Nazareth during his crucifixion in the Gospel of John, but not the Gospels of Matthew, Mark, and Luke. The spear he used became a relic known as the Spear of Destiny.

#### Lucifer Invictus

* Pseudo-latin for "Unconquered Lightbringer": a Gnostic metal band Naomi listens to.

#### Maestro

* A man in white with blue eyes and snow-blonde hair who teaches Naomi fencing. Readers of *Without Bloodshed* may recognize him...

#### Maimonides' Deli

* A Jewish delicatessen in Manhattan named after philospher Moses Maimonides. Owned and operated by Mr. Spinoza, named for another Jewish philosopher, Baruch Spinoza.

#### Malkuth

1. An AI employed by the Phoenix Society who is more comfortable with people than his fellows.
2. One of the ten Sephiroth. In Kabbalah, it means "Kingdom" and is associated with the material realm.

#### Mastoid Process

* A conical protrusion of the temporal bone in the human skull that provides an attachment point for facial muscles. It is filled with air cells that get aerated during the first year of life. It can sometimes get infected if an ear infection spreads, and surgery to remove it is called a mastoidectomy.

#### MEPOL

* The Metropolitan Police of London. The real-world Metropolitan Police Service is often referred to as The Met, and its officers do not carry swords.

#### Moirai

* The collective name for the three Fates from Greek myth: Clotho, who spins the thread of life; Lachesis, who measures it; and Atropos, who severs the thread at its appointed end.

#### Mommylookit

* The characteristic cry of children afflicted with the mommylookits, a common childhood mental disorder that most children eventually outgrow as modern parenting and public education beat all traces of curiosity out of their little heads.

#### NACS *Thomas Paine*, the

* A nuclear submarine in service to the North American Commonwealth Navy.

#### Nationfall

* A near-total collapse of civilization triggered by widespread adoption of The Patch. Named for the survivors' refusal to permit the existence of any political entity larger than a city-state.

#### North American Commonwealth

A country founded in 1812 after an incursion by US forces into Canada was punished by a full-scale invasion leading resulting in Canadian troops burning Washington DC to the ground before the British could do it. They subsequently annexed the US, declared independence from Britain, and proclaimed the North American Commonwealth. 

One of the new government's first acts was to free black people held as slaves by taking them from their owners via eminent domain, and immediately manumitting them. Though this provoked rebellious sentiment among Southern plantation owners face with the prospect of having to pay for labor, any talk of rebellion was soon quelled by means of carefully targeted assassinations.

Just prior to Nationfall, the Commonwealth covered the whole of North America, from the Isthmus of Panama in the south to the northern shores of Alaska and Greenland.

#### Ohrmazd Medical Group

* A corporation involved in healthcare provision, medical research, pharmaceuticals, and biotechnology. Named after Ohrmazd, also known as Ahura Mazda, the creative spirit venerated in Zoroastrianism.

#### Patch, the

* psychiatric nanotech allegedly developed by the Ohrmazd Medical Group. It was variously touted as a productivity booster for workers, an aid to social cohesion, and a way to be closer to God. It only worked on those who accepted it willingly, which meant it could not be applied under any kind of duress.

#### Phoenix Society

* A non-governmental organization (NGO) that arose during Nationfall to help those who survived the near-total collapse of civilization. As armed biker gangs and soldiers without countries gathered around, the Society forged them into an army dedicated to the ideals of liberty, justice, and equality under law for everybody.

#### POSIX

* An international standard (current version: IEEE Std 1003.1, 2013 edition) describing application programming interfaces (APIs), command-line shells, and utilities required to maintain software compatibility between various implementations of Unix and other computer operating systems.

#### Project Harker

* A series of medical experiment's conducted in the North American Commonwealth armed forces Army Medical Corps. Intended to enhance the abilities of CPMD+ soldiers, Project Harker's primary subjects were the members of the Third Infantry Divison's all-CPMD unit, *Dusk Patrol*.

#### Programmer Cat

* A cuddly blue-eyed kitty who can hack Unix. He gets into all kinds of trouble, and has even piloted giant robots to fight kaiju like Godzilla and Cthulhu. Inspired by Mark Rogers *Samurai Cat* books, and used as a handle by the author on FARK.

#### Pulsecannon

1. A heavy metal band from Seattle.
2. A weapon the author's wife made him take out because it was too conducive to dick jokes.

#### purr-begging

* An advanced begging technique used by cats. It relies on being unbearably cute.

#### Pussycat

* A slur describing an individual with CPMD. As offensive as a certain six-letter word beginning with 'n'. Often used to provoke the target into demanding a duel.

#### Pyrrhic victory

* A victory whose cost is so devastating that the winner might as well have been defeated. Named for King Pyrrhus of Epirus, whose army suffered irreplaceable losses in war against Rome in 280-279BCE.

#### "safe, sane, and consensual"

* Everything kink should be.

#### Seiten Taisei

1. Japanese equivalent of the Chinese title Qítiān Dàshèng (齊天大聖), which means "Great Sage that Equals Heaven", that the Monkey King Sun Wukong claimed for himself in the Chinese novel *Journey to the West*. The first word can be mispronounced like the name of Saddam Hussein's boyfriend in *South Park: Bigger, Longer, and Uncut*, but has nothing to do with him.
2. another Gnostic metal band Naomi likes.

#### Separatist

1. One who advocates of a state of cultural, ethnic, tribal, religious, racial, governmental or gender separation from the larger group.
2. An individual with CPMD who views himself as part of a species distinct from *homo sapiens* which should govern itself and treat its interests as distinct from, if not opposed to, those of humanity.

#### Service gladius

1. A short sword carried by police officers, and formerly used by Roman soldiers. It is the only weapon permitted them by the Phoenix Society.

#### Snow-blonde

* A hair color occurring in individuals with CPMD. The term is used to distinguish from white hair resulting from varying degrees of albinism.

#### Sophia

* Greek word for "wisdom": a major figure in Gnostic tradition, Sophia is the lowest Aeon, or anthropic expression of the emanation of the light of God.

#### That Scottish Play

* A euphemism for Shakespeare's *Macbeth*. Actors believe the play is cursed, and refuse to use its name when performing it.

#### The Lonely Mountain

1. A nod to Tolkien, but a dragon wouldn't fit in this one.
2. an inn in Clarion Bruce and Dick Halford purchased and restored when they got married ten years before the events of this novel.

#### The Tube

* London's subway.

#### True Goddess Metempsychosis

* A literal translation of the name of a long-running Japanese RPG franchise better known as *Shin Megami Tensei*. In Japanese "shin" can also mean "new" as well as "true".

#### Universal Declaration of Individual Rights

* An expanded version of the United Nations' Universal Declaration of Human Rights rigorously enforced by the Phoenix Society's corps of Adversaries.

#### Valkyrie Gym

* A chain of gyms and dojos which cater primarily to women, but tolerate male customers who behave themselves.

#### Vestal

1. A manufacturer of stylish motor scooters marketed mainly to urban women
2. A reference to the Vestal Virgins of ancient Rome.

#### Vlad Tepes

* A Wallachian warlord notorious for repaying the Turkish hospitality he enjoyed as a young man by shoving sharp pointy things up invading Turkish soldiers' asses. He was equally harsh with criminals, spammers, and people who talk at the theater.

#### Winston

* A cat who visits Naomi. Possibly named after Winston Churchill.

#### Witness Protocol

* A software package which runs on the implants of all individuals entrusted with positions of authority, no matter how minor. It translates data gathered from the optic and auditory nerves into video, compresses it, and transmits it to the Phoenix Society. It can exonerate an individual accused of a crime as often as it condemns them. Many young people, still new to the idea of having a computer in their heads, use Witness Protocol for more prurient purposes.

#### Xanadu House

* A chain of pleasure houses staffed by highly trained courtesans capable of catering to almost any adult desire. Similar to the Hellfire Club, only they provide the hookers and blow in-house rather than expecting patrons to bring their own.

## Acknowledgements

Writing for a specific audience from a single young woman's viewpoint is a bit of a departure from my first book, and I'd like to thank some of the people who helped make *Silent Clarion* possible. 

Let's start with all the Starbreaker fans who have supported me from the beginning. You know who you are, and you're the fucking best. It's a privilege to have you as readers. If this is your first Starbreaker story, I hope you enjoyed it and that you'll stick around. There's lot's more to come.

Now for some more specific shout-outs.

 * For my wife, Catherine: thanks for reading every chapter, even if it meant staying up all night.
 * For Alisa at Curiosity Quills: thanks for encouraging me to go off on this little tangent into Naomi's past.
 * For Nikki at CQ: thanks for all the work you do on promo. I'll do a Claire story next, just for you. Well, and Catherine.
 * For Clare at CQ: thanks for being patient when I cut things a little too close.
 * For my parents: thanks for reading my stuff. I know it isn't your preferred fare.
 * To D. Solberg, R. J. Blain, J. Karaganis, S. Hart, L. J. Cohen, C. Kravetz, L. Williams, M. Dunn, R. Cota, R. Porter, G. Minoli, M. Zeman, J. Jones, A. Wiggins, K. Glatfelter, T. Ebl, D. Swensen, J. Dahiya, B. Tomlinson, R. Toxopeus, R. Mattison, H. Bunda, J. Mitchell, D. Reimer, A. Perez, A. V. Flox, K. Huxtable, B. Calder, N. Smith, S. Miles, C. Paluch, E. Irving, D. Higgins, A. Meadows, S. Bergen and all my other fans on Google+ and Facebook: I really should be better about keeping up with you all, but even if I'm not around I haven't forgotten you. I just want to write more kick-ass science fantasy for you all.
 * To the folks on Reddit, particularly those frequenting r/fantasy, who read my stuff and recommend it. You know who you are, and you're the fuckin' best. m/
 * To everybody who criticized my last book: You might not see this, but I *did* Take your reviews into consideration.

## About the Author

According to official records maintained by the state of New York, Matthew Graybosch was born on Long Island in 1978. Urban legends from New York suggest he might be Rosemary's Baby, the result of top-secret DOD attempts to continue Nazi experiments combining human technology and black magic, or that he sprang fully grown from his father's forehead with a sledgehammer in one hand and a copy of *Bulfinch's Mythology* in the other — and has given the poor man headaches ever since.

The truth is more prosaic. Matthew Graybosch is a novelist from New York who lives in central Pennsylvania with his wife and cats. He is also an avid reader, a long-haired metalhead, and an unrepentant nerd. *Without Bloodshed* was his first published novel, and *Silent Clarion* is his most recent work. He's currently hard at work on more badass Starbreaker novels like *Nightmare Sequencer*, *Edge of Thorns*, and *Blackened Phoenix* while working Deloitte as a software engineer.

You can follow him on Google+, Twitter, and Facebook; or visit his website at [http://www.matthewgraybosch.com](http://www.matthewgraybosch.com). Please help him quit his day job by telling leaving reviews and telling your friends. Or buy copies to inflict on your enemies.
