#+TITLE: Pistol at Dawn
#+DATE: <2015-02-12 Thu>
#+AUTHOR: Matthew Graybosch
#+EMAIL: matthew@starbreakerseries.com
#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t c:nil creator:comment d:(not "LOGBOOK") date:t
#+OPTIONS: e:t email:nil f:t inline:t num:t p:nil pri:nil stat:t
#+OPTIONS: tags:t tasks:t tex:t timestamp:t toc:t todo:t |:t
#+CREATOR: Emacs 24.4.1 (Org mode 8.2.10)
#+DESCRIPTION: Before Starbreaker
#+EXCLUDE_TAGS: noexport
#+KEYWORDS:
#+LANGUAGE: en
#+SELECT_TAGS: export
* Design :noexport:
** Synopsis
** Cast
** Plot
** Scenes
* Outtakes                 :noexport: