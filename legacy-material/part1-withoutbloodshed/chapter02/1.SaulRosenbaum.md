“How did Boston become *our* problem again?” Saul Rosenbaum looked at his two fellow Directors, Iris Deschat and Karen Del Rio. The three of them formed a triumvirate executing the Phoenix Society’s policy of enforcing walls of separation between church, state, and commerce in the city of New York and its vicinity. Boston was far enough from New York, and sufficiently populous after three decades of resettlement, to merit its own chapter of the Phoenix Society with Adversaries recruited from the local populace. Saul had celebrated the new chapter’s opening by taking a vacation, his first in three years. It was his fifth since joining the Society at its inception sixty years ago, in 2052. He allowed himself a smile as he did the math. A vacation every twelve years was hardcore.

“You don’t know about Liebenthal?” Iris Deschat was the first to reply. She was Saul’s age, and they had survived Nationfall together aboard the *NACS Thomas Paine*. Their last order as officers of the North American Commonwealth Navy had been to defend the submarine and its nuclear armament against all enemies. Despite her age, she retained her vitality. Life on half rations with no hope of a clean uniform aboard a submarine flying the colors of a dead nation had been gentler to her looks than to his; he retained the haggard aspect he acquired as her first officer, while hers was the face of an athletic grandmother.

“Saul’s been on vacation all month.” It was one of Karen Del Rio’s usual snide remarks, a barb she threw to cover her own shortcomings. Born long after Nationfall, Karen had neither the experience nor the refinement of Iris. Saul suspected she had a taste for amphetamines, but had never seen her indulge on the job. He wished she would. Coming down from a high could be a bitch, just like Karen. “He was on vacation, and out of touch.”

“The whole point of a vacation is to be out of touch.”

“I hope you refrain from expressing such attitudes in front of the Adversaries. You set a poor enough example.”

Saul could think of better ways to set a poor example, such as giving command responsibility to somebody who had never served in the Phoenix Society’s Department of Individual Rights Defense. The DIRD officers began to call themselves ‘Adversaries’ after an edict from Pope Magdalene named them enemies of the Roman Catholic Church and all good Christians. They had terms for people like Karen in the Navy, unflattering ones like ‘martinet’ and ‘rear-echelon motherfucker’. Some of them found use among the Adversaries. He intended to celebrate his retirement by using them to her face. “Worry about your own example, Del Rio, and tell me what’s going on with Liebenthal.”

“I’ll summarize the situation.” Iris held up a finger. “At seventeen-hundred hours yesterday, Alexander Liebenthal, Munakata Tetsuo, and the members of the Fireclowns Motorcycle Club stormed City Hall in Boston. They rounded up the entire government and put them on the first maglev to Asgard.” A second finger joined the first. “At zero four-hundred hours this morning, three combat-qualified Adversaries from the Boston chapter stormed the Liebenthal Produce Distribution office in an attempt to arrest Liebenthal. Munakata was waiting for them, and killed them all.” She held up a third finger. “At zero eight-hundred hours this morning, half an hour ago, Liebenthal held a press conference.”

He stared at Iris; the situation was more serious than he had expected. Although the first thing to spring to his mind was flippant, it needed to be said. “Aren’t you supposed to issue your demands before you start killing people?” He ran a quick search of the Phoenix Society’s records. “And didn’t Morgan Stormrider kill Munakata Tetsuo after the Shenzhen labor riots three years ago?”

Karen shook her head. “Liebenthal did not appear before the press to issue demands, and Morgan must have fucked up.”

“How do you fuck up a headshot at point-blank range?”

“I’m sure Morgan found a way. I keep telling you not to count on him.” Impatient fingers scrabbled at her terminal’s keyboard as Saul resisted the urge to remind Karen of her own tendency to depend on Stormrider. “What the hell is that AI doing? Malkuth! Pay attention!”

A screen mounted in the wall revealed a Spartan office. A tall, slim man dressed in an Adversary’s formal dress uniform sat at attention behind a bare desk. The Roman numeral ‘X’ glowed in the center of his forehead, and black-lensed sunglasses hid his eyes. “Say please, bitch. I’m not one of your Adversaries.”

“Malkuth, please play Alexander Liebenthal’s speech from this morning.” Iris used a more conciliatory tone as Karen glared at the AI’s avatar.

“Of course, Overseer Deschat.”

A cadaverous man in a suit tailored to flatter a stouter figure took his place behind a podium erected outside Boston’s City Hall and raised his hands for silence. “Some of you know me. I’m Alexander Liebenthal, local produce distributor and, unfortunately, temporary dictator of Boston. I feel nothing if not regret for the deaths of three Adversaries slain by my retainer this morning. These deaths were a tragic necessity. I attempted to enlist the aid of the Phoenix Society when I first discovered the corruption of Boston’s government, but my petition went unheard. I took arms against the government of Boston for the good of her people. The city council and the Phoenix Society made peaceful revolution impossible and violent revolution inevitable.”

“Did he just paraphrase John F. Kennedy to justify a coup?”

Iris nodded to Saul. “Watch the rest.”

“The revolution is not yet complete!” Liebenthal leaned over the podium. He thrust an accusing finger across the street, where the Phoenix Society’s Boston chapter stood as a counterpoint to City Hall. “The tyranny and corruption of Boston’s city council are trivial compared to the organization which makes their rule possible. The Phoenix Society itself poses the greatest threat to the individual rights we trust them to defend! They ignore the demands of the people! They rule without the consent of the people! Their executive council remains hidden. None of us know their names or can hold them accountable! They send assassins when challenged!”

He straightened and adjusted his tie before continuing in a softer tone. “Ladies and gentlemen, while I regret the deaths of the Phoenix Society’s assassins, we are fortunate the Phoenix Society did not send their deadliest weapon. Had Morgan Stormrider come for me, I daresay I would not be here now. I would not be able to tear aside their veil of propaganda and speak truth to power!”

Saul rolled his eyes. “This guy has quite a bit of nerve for a produce distributor. How long is this tirade?”

Malkuth halted playback. “He’s still talking. A live-updating transcript of Liebenthal’s speech is in the file, along with the results from previous investigations into his business dealings. I’ve also included cross-references to Tetsuo Munakata and the Fireclowns MC.”

Saul made coffee for himself and the women, and passed out the mugs. *So much for cutting my intake.* “Malkuth, do our previous investigations into Liebenthal have any bearing on the current situation?”

Karen gave Saul a dirty look for forgetting to sweeten her coffee, which Saul ignored. It’s the twenty-second century, and Karen should have gotten her own damn coffee. “Two months ago, Doctor Ronald Paulson led Boston’s city council in petitioning us to investigate a charge that Liebenthal’s wholesale produce business was a front for arms trafficking.”

Saul nodded and took a tentative sip of his coffee, which remained too hot. It was unusual for a city government to *request* the Phoenix Society’s attention. Officials usually did their best to avoid giving an Adversary reason to visit, lest they find themselves staring down the barrel of a gun. Iris added cream and sugar to her coffee, and sipped it as she checked her terminal. “Sarah Kohlrynn was available at the time, but too inexperienced for solo fieldwork. Fortunately, we had Catherine Gatto on loan from the Melbourne chapter.”

“Here’s the report for their investigation. Riders from the Fireclowns MC had escorted Liebenthal Produce trucks to a series of towns in the Berkshire Mountains, according to Gatto and Kohlrynn. In each of these towns, members of the Repentant in Christ bought weapons.”

Saul considered Malkuth’s summary. “What sort of weapons?”

“Adversary Kohlrynn included video of a demonstration. She described it as an ‘experimental electromagnetic mass driver’. Her implant detected localized bursts of electromagnetic radiation every time it was fired.”

Iris asked the question on the tip of Saul’s tongue. “Was the weapon effective?”

“The video shows the weapon in use against a pig carcass. It appeared to be effective, though prone to overheating. Based on their reports, Gevurah thinks somebody has developed a miniaturized railgun system suitable for infantry use.”

“Am I the only one who doesn’t understand why the Repentant, who are supposed to be pacifists, would buy experimental railguns?” She had stolen Saul’s next question, and the notion of trying Iris with a deck of Zener cards intrigued him for a moment. However, since the Zener deck consisted of circles, squares, triangles, waves, and stars, Iris would always have a one in five chance of guessing which card he held. It was no way to prove telepathy on her part.

A tone of annoyance entered Karen’s voice. “The Repentant have purchased firearms in mass quantities before, along with swords. They recycle the materials and use them for more peaceful purposes.”

“That’s probably why the Society hasn’t worried too much about the Repentant buying large quantities of weapons in the past.” Saul stared into his coffee. “This is different. If they were just buying these experimental weapons so they could recycle the materials, why would they want to see the weapons in use? You don’t ask to see a firearm demonstrated on a carcass unless you mean to use it against live targets.”

“I asked Gatto and Kohlrynn the same question. Neither was willing to conjecture.”

Saul shook his head. “Of course not. If you pressed them, they’d insist that they have no evidence, and they have no evidence because we’ve been content to write them off as devout, but harmless. In fact, they’ve been reasonably helpful, but we never had any guarantee that they would stay that way. I think they mean to use these experimental weapons. I think we need to find out why.”

“Why the Repentant want these weapons is irrelevant!” Karen stopped short of grabbing him by his collar to shake him. “Saul, you’re welcome to hate Christians because of the shit fundies did in Nationfall. Blame them for the near-extinction of the human race if you must, but we don’t have the authority to do anything about this sect until its members actually use the weapons they’re buying to violate the rights of others.”

Saul reddened, and glared at Karen for moment before turning to Iris. “I hate it when she’s right. Are my feelings about monotheists so obvious?”

“Your feelings aren’t the issue.” She gave Saul’s shoulder an affectionate squeeze. “I know you consider the Repentant in Christ’s purchase of experimental weapons to be a potential threat. We can gather the evidence we need to move against them *after* we’ve dealt with Liebenthal, Munakata, and the Fireclowns.”

“It’s not just the Repentant. Somebody is making experimental weapons. Somebody is buying these weapons and selling them to the Repentant. Who else might be buying these weapons? Why are they being made? Just how powerful are they?” He took a breath. “We haven’t even captured one for study.”

“Our mission as Directors of the Phoenix Society is to preserve and protect individual rights.”

“Our mission makes us vulnerable. We can’t prevent another Nationfall. We can only react to events after the fact.”

Karen chuckled. “Nobody expects another Nationfall but you, Saul.”

“Nobody expected the Spanish Inquisition, but it happened all the same.” Malkuth paused, and Saul considered asking him if he intended to joke. “Overseer Rosenbaum’s reasoning is sound. I will make discreet inquiries.”

“Thank you.” Iris spoke before Saul could. “I share Saul’s concerns, but we have more pressing responsibilities.”

He took a breath to calm himself. “Do the Fireclowns have these weapons?”

Karen checked her terminal before answering. “Gatto and Kohlrynn are still in Boston, and Miria Deschat has not yet succeeded in getting them or the remnants of the Boston chapter out of there. All three report that the Fireclowns are still using standard Kalashnikov-style rifles with either smallswords or semiautomatic pistols as sidearms.”

Iris glared at Karen. “You assigned my granddaughter to a solo extraction mission, knowing Tetsuo Munakata already cut down three Adversaries?”

“Her orders, like Gatto’s, are not to engage the enemy, but to observe and report.” She stood to meet Iris’ eyes. Seeing that her mug was empty, she filled it and drank half before continuing. “I would have preferred to send Morgan Stormrider, since he *is* the best killer we’ve got. However, he was unavailable, and now his little bitch of an AI is stonewalling me. Since your granddaughter has the same combat qualifications as Stormrider, and Stormrider has won duels with Munakata in the past, I judged it likely she would hold her own if forced to defend herself.”

Saul did not like the ice in Iris’ voice. He had last heard it when he tried to deal with a conspiracy to start a mutiny aboard the *Thomas Paine* without involving her. “You judged it likely, knowing that neither Gatto, Kohlrynn, nor my granddaughter have ever fought under anything but controlled conditions. Did you also judge it likely that the Adversaries Munakata already killed would be able to take him?”

“Don’t blame me for the Boston chapter’s decision.”

Malkuth’s voice chilled the room. “You approved it when they asked for advice. According to our records for Adversaries Gabriel, Rutherford, and Collins, none of them had more than basic close-combat qualification. None of them had their weapons in hand when Munakata attacked, even though they knew that Munakata tends to use techniques which allow him to kill an enemy while drawing his sword. They were diplomats, not soldiers, and you threw their lives away in order to appear decisive.”

“Then get Morgan Stormrider.” Karen’s expression indicated that she had no intention of admitting that she had screwed up and gotten people killed. It was an expression Saul wanted to slap off of her face. “Oh, wait. You can’t. You look human, but you’re just a ghost in a machine, and the machine’s stuck in the basement. You and those other nine AIs who call themselves the Sephiroth are useless without us, so remember your place.”

Saul glared at Karen, his hands itching to wring her scrawny neck. Before he could tell her what he really thought of her, Malkuth sighed and shook his head. “Give me a better body than yours and we can trade places. I’ll let you take abuse from MEPOL as you tell them that the Witness Protocol data on file shows that they lack probable cause to extradite Morgan Stormrider to London in connection with the Crowley murder.”

\###

