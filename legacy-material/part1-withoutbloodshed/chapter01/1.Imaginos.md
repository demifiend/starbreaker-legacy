Snow whirled around Imaginos, and hid him from sleepless eyes condemned to an insomniac vigil. He slipped out of the limousine parked before a row house on a quiet street in London’s Crouch End. Though the sidewalks and the street were heated to prevent the accumulation of snow and ice, thirty centimeters’ worth already covered unheated surfaces like front gardens and old rooftops. The fallen snow possessed a bloodstained aspect lent by streetlights tinted almost infrared to avoid drowning the stars above in light pollution. Flakes brushed his face, melting against his skin until he seemed to cry for the unconscious woman he lifted from the limousine. He cradled her in his embrace, and closed the limousine door with a soft thump, which the snow diffused into silence. He gathered her arms to keep them from dangling as he padded along the sidewalk and up a short flight of steps to the front door of the house at the corner. Its owner had painted the house blue, with white accents; snowflakes melted in her chestnut hair as Imaginos waited for the AI to recognize her and admit them.

The front door of the adjacent house opened a crack. The house reflected its owner, mostly white, with crimson accents. Scarlet eyes with feline pupils blinked from behind the red door as the house’s mistress peeked out. She drew the collar of her robe tight around herself to ward off the chill, and sleep still tinged her voice. “Doctor? Why are you here so late? Is Christabel all right?”

Imaginos reached out with preternatural senses to visualize the neural activity of Christabel Crowley’s onetime friend and band mate, Naomi Bradleigh. A second’s effort to draw power and apply the pattern was all he needed to ensure Naomi would not remember meeting him at her door tonight. “Somebody slipped a drug into her drink. I’m going to put her to bed, put a blanket over her, and leave a note so she doesn’t panic.”

“Let me get the door for you.” Naomi frowned for a second, as if remembering a bit of information too rarely used. “I might not be able to, now that I think of it. Christabel and I aren’t close any longer. Just a minute.”

The door unlocked, and opened a crack. “Here you go. Will you lock up behind you?”

“You need not worry, Ms. Bradleigh. I hope your Winter Solstice was a happy one.”

Her voice was wistful to Imaginos’ ears, as if she missed somebody. “I hope the same for you, Doctor.”

The red door snapped shut. Imaginos backed into Christabel’s house while taking care to avoid bumping the head of the woman he held against the doorframe. The snow diffused the streetlights; they barely lit his way as the scarlet glow refracted into the house. The lights died behind him as he stepped beyond the range of the motion detectors installed to avoid  empty streets. He had the night to himself as he carried his victim upstairs.

He needed no password to command the house’s AI to run the lights at five percent of their maximum illumination. It was enough for him to see as a human might, but not bright enough for neighbors to wonder why Christabel was up so late – assuming any of them cared enough about her to wonder. He stroked her hair as he laid her across her bed, knowing it would be his last chance to feel the rough chestnut silk beneath his fingertips. *You were never good at making friends, and not much better at keeping them.* 

He translated himself into the basement, and considered the machinery running the household AI which acted as Christabel’s servant and companion. He rejected the notion of obtaining root privileges on the AI and commanding it not to record his actions, or even to suppress the memory of what he might do here; both seemed too subtle. *I may need to frame Morgan Stormrider for the murder. Would he crack root? No. He’d simply cut the power to the AI, knowing that it would hibernate to conserve its backup power supply.* He did as he thought Morgan would do, disconnecting the heavy cable from the wall socket that connected the AI to the electrical grid because it was too old to use a tesla point for wireless power transmission. Since the basement lacked recording devices, and he made a habit of wearing gloves, he would leave no evidence of his tampering. The console immediately flared to life, showing a system-wide alert that the AI would hibernate in thirty seconds.

He drank every detail of the house until he believed himself able reproduce it from memory down to the dust bunnies hiding from Christabel’s obsession with cleanliness beneath the furthest corner of the antique couch dominating the living room. Setting a crime scene was a demanding art; its first requirement was an intimate knowledge of the environment serving as his canvas.

He chose the converted attic in which Christabel practiced her violin for the site of his crime. A music stand holding the score for a concerto by Vivaldi toppled and clattered against the polished oak floor as he laid the body of the woman he brought in on her side, arranging her left arm and leg to keep her from lying prone. He drew her hair from around her face with tender hands, to make it spill across the floor. It would shimmer beneath the skylights, which were heated to prevent snow from accumulating atop them, if the sky were clear enough for the moon to bathe London in silver. 

His preparations were complete, and if he committed himself he could never return to Crouch End. Such an elementary error as revisiting the site of his atrocities was beneath him. He remained poised on the point of decision; the crime remained a possibility, and a last resort. He could gather his victim into his arms, carry her back down to the limousine, and instruct the driver to take them away. It would be a trivial matter, if he could only find another way to accomplish the purpose that brought him to the suburb that the depopulating effects of Nationfall had made the edge of London.

An errant draft fluttered a page of the concerto so that it brushed against her outstretched hand. One of her eyelids twitched, but her grey eyes did not open. He picked up the music stand, and retrieved the pages with the silent care he would use to avoid waking his victim from a natural sleep.

Another woman’s fingers caressed his shoulder as he arranged the sheet music on the stand in the corner. He recognized the intruder before she spoke. “Are you sure you want to do this?”

“No.” He faced Ashtoreth, and refrained from drawing her into his embrace as he once had. The time for such gestures was long past. Nobody else had her amber eyes, or the inclination to dress for conspiracy and murder in the same evening gown with which she turned heads at the premiere of *Casablanca*. The sight of her all in black inspired his lust, but she had more to offer than the promise of carnal fulfillment. “I waited for you in case you found a better way.”

She smoothed the lapels of his jacket, and straightened his cravat for him. “I can offer no alternative, despite my doubts about your plan. Why should Morgan avenge Christabel? He ended the relationship. There’s nothing between them now.”

*Christabel threw away every chance she had to make a happy life for herself because she thought it would please me. Now she wants me to help her escape.* “Heartless words cannot erase a decade of history. Morgan will demand answers.”

Her ebony hair brushed against the white wool of his jacket as she turned away. “She gave him ample cause to hate her.”

“I built Morgan. His life follows my design. If love proves insufficient, duty will bring him to me.”

“Duty?” She held the word in her mouth as she might an unfamiliar wine. “What of the game you play with the traitor Adramelech? He is reliable enough, unless you forget his thirty pieces of silver.”

“The game proceeds as expected, despite your distaste for the piety with which he plays the priest.”

“You would do better to draw Morgan down that path. He already doubts his mission as an Adversary. Make him see that the Phoenix Society represents the very tyranny he swore to oppose, and your brother will have no trouble persuading him to wield the Starbreaker against you. Then you need only place our enemy in his way.”

Imaginos said nothing for several minutes, for Ashtoreth’s suggestion required careful thought on the psychology of the man he sought to manipulate. *Would outrage at the Society’s corruption be sufficient provocation to unleash his suppressed abilities? No. Such anger is too cerebral. I require rage unchecked by reason.* “I had your idea in mind, but it will not be sufficient. Between the indignation your plan would provoke, and the personal wrongs I have done him, his hatred will drive him to massacre entire pantheons for a chance to kill me.”

Ashtoreth turned away, and began reading the score he had replaced on the stand. “Have you given a moment’s thought to how others suffer for your cause?”

“Our cause.” He advanced upon her, gripping her bare shoulder. “I need you to trust me.”

“You doubt me?”

He remembered the vehemence with which she spoke when he first discussed using Morgan. “You despise my methods.”

“Can you not reason with him? He would fight beside us --”

“We have no time for idealism.” He indicated the painting from the first Crowley’s Thoth album, *Prometheus Unbound*. “I need Morgan unleashed, rather than chained by his conscience. He’ll not free himself without cause.”

“Does impatience force your hand?”

He considered lying to her for a moment. “I am afraid. The bindings we placed on our enemy are decaying. Within a year he will be free again to attempt genocide against our people. “

She shook her head. “Is there nothing I can do to help you?”

“Be my nemesis. Guide Morgan. Seduce him if you must. Make him believe that I corrupted the Phoenix Society, and made it an instrument of fascist oppression.”

She stared at him, aghast at his request. “Are you bereft of reason? Provoke such hatred in him, arm him with the Starbreaker and his full asuric powers, and you stand defenseless before him.”

“So will our enemy. I know the risks inherent in provoking Morgan Stormrider, and judge them manageable.”

“Underestimating him will cost you your life.”

“It would be a delightful irony to die, not for my involvement in Nationfall, but because I misjudged an Asura Emulator.” He laughed at his own bitterness and self-loathing. “You’re about to tell me that dying will accomplish nothing of value.”

She took his hand instead. “I did not come to belabor the obvious, but if you insist, then I’ll remind you of our burden as Disciples of the Watch. We do the unforgivable for the greater good.”

“Then you understand my purpose here.” He turned her hand over in his, and pressed a kiss into the palm. He closed her fingers into a loose fist and released her. “Please go. I do not want you to be an accessory to this crime.”

“I understand.” Ashtoreth disappeared from sight, and the air rushed in to fill the space she had occupied. He burned the room into his memory, lest he lie to himself by denying the depravity of his actions. He knelt by the woman on the floor, and caressed her shoulder. Her flesh was warm and pliant, but she did not stir or make a sound. He began to concentrate, and reached out with preternatural senses for an external power source. He found a tesla point embedded in the wall, which suited his purpose. He drew energy as if he were an appliance or a lamp, and conducted it into the woman through his own body. He let the current cook her from the inside out, until all that distinguished her from a pile of burnt meat was her hair.

He heard a woman’s voice call him from the stairs. “Are you still here? Why does it smell like burnt meat in here?”

If she had come a few minutes earlier, she would be a twin to his victim. Her grey eyes flared, and her entire body began to tremble as he rushed to her. Her breath hitched, but Imaginos drew her into his arms before she could scream. He stroked her chestnut hair as she settled against him, her body relaxing into sleep as he whispered in her ear. “I told you not to look.”
