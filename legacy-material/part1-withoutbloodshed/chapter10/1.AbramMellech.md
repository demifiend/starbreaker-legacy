Abram Mellech chafed at his collar, taking a hand off the wheel of his retrofitted black Cadillac to pull at it so he could breathe. Everything had seemed so simple. Of all the Disciples of the Watch, he was the one most reluctant to indulge in the pleasures of the flesh. His repeated dalliances with Elisabeth Bathory had been brief affairs, which he ended out of disgust with himself for still needing a woman’s touch. He never ate, and never drank. He never slept, and had taken on an aspect at once ascetic and cadaverous. As such, he was the Disciple of the Watch best suited to his mission: become Sabaoth’s right hand, gather and equip for the ensof trapped beneath the Antarctic ice an army, and make Sabaoth believe that they would fight and die *for* him. When the time came, however, the Repentant in Christ would fight against the demon who dared call himself God.

He looked forward to being Adramelech again, and reclaiming his true self. No longer a priest, but a Disciple of the Watch, a deva who had sought qliphotic power and become an emanation of the tree of death. First, he would seek a flow state, rip this damnable collar from his neck, and wash its ashes from his hands. He already missed Bathory’s taste, and the heat of her kiss, despite having had her less than a week ago, so he would seek her next and -- should she permit it -- take his fill of her.

A motorcyclist cut Abram off, forcing him to hit his brakes to avoid ramming the biker. He caught a glimpse of what appeared to be a Fireclowns MC patch on the rider’s jacket before realizing the patch had the wrong colors, and therefore identified a Sun Jesters Motorcycle Club member. All he had to do was concentrate a bit and draw some power, and he could make the restored Harley-Davidson explode beneath its rider, blowing him apart in turn. Some of the Fireclowns might even thank him for getting rid of one of their rivals. He shook his head and pulled at his collar again. “Grant me patience, Lord.” *Now I pray to one of my equals, and an enemy besides. The things I do to stay in character.* A panel truck pulled into his lane without signaling, and Abram barely had time to brake and avoid a collision. He shook his fist once the truck had pulled far enough ahead for the driver to be able to see Abram in his mirrors. “But first, give *this* idiot a case of crabs.”

He managed to park underneath Boston’s City Hall without further incident, and retrieved an attache case containing payment for a final shipment of weapons from the trunk. One of Liebenthal’s mercenaries, a heavily tattooed man with a thick red beard which covered his chest, greeted him and verified his identity before escorting him upstairs. He was muscular enough to handle one of the huge vintage motorcycles he and his fellow Fireclowns insisted upon riding, despite the age the grey in his hair and beard; Abram had no doubt that he would handle the Kalashnikov slung across his back or the knives in his belt with similar ease. He shot Abram a sidelong glance as the elevator doors closed. “You want to watch out for Yojimbo.”

Abram nodded. All of the Fireclowns called Munakata Tetsuo ‘Yojimbo’ after a screening of the Kurosawa film; they knew him for a sellsword, and suspected he had his own agenda. They also had a name for Alexander Liebenthal, who had hired both Munakata and the Fireclowns. They called him ‘the client’. “I usually do. Is there any particular reason for concern?”

“Yeah. Couple of Adversaries stormed the Phoenix Society’s Boston chapter, and caught Yojimbo and a few of us by surprise. We knew a couple of Adversaries and support staff were still there, so the client wanted us to take ‘em hostage.”

Abram nodded. “What happened?”

“This big black badass with dreadlocks went sword to sword with Yojimbo. I mean, the guy was over two meters tall, must have massed at least one twenty kilos, and he was fucking *ripped*. Just my type, you know?”

“You know homosexuality is a sin in the Lord’s eyes.”

“Tell your Lord to pluck out his eyes if they offend him, Rev. Anyway, he took on Yojimbo with a rapier and one of those little short rapiers --“

*Let the blasphemy slide. I’m not pretending to be that sort of priest. At least this man has a spine.* “A main gauche?”

“Yeah. Never could get the pronunciation right. Anyway, the big dude kept Yojimbo busy, so we figured we’d take out the old fart who was with him. He just had this little bullpup carbine with a scope, you know? He chucked a flashbang and a tearjerker at us, and got his people out the back door before we could get our shit together.”

*No doubt this turn of events would displease Munakata.* “Did Munakata win his duel against the younger Adversary?”

“I’d call it a draw. The old fart came back and kneecapped Yojimbo from up the other end of the hallway. When he tried to keep fighting, the bastard took out his other knee. We tried to catch ‘em, but the wily old bastard rigged some flashbangs and tearjerkers to some kind of proximity detector. Soon as we got close to the door they used, *bang!*” The biker slammed his fist into the palm of his other hand for emphasis. “We’re back on the floor, blind, crying, and coughing up our fuckin’ lungs. Meanwhile, Yojimbo must be the fuckin’ Wolverine’s bastard kid, ‘cos he’s back on his feet and cussin’ in Japanese.”

The elevator stopped, and opened its doors. As Abram stepped out, the biker who had escorted him caught his shoulder. “Hey, Rev. I’ve got a couple of minutes before I have to resume my post downstairs. You want to duck into one of these offices for a couple minutes?”

*How many types do you have, anyway?* Abram gently brushed off the man’s hand. Though he had crossed swords with Sathariel before, it had always been while they made Ashtoreth the center of their attention. What this biker offered was something different. “Sorry, but I took a vow of chastity. Christ’s peace be upon you.”

The staccato of rapid footsteps preceded the metallic shing of a sword clearing its scabbard. The blade rent air with an almost inaudible whoosh, and then hissed as its owner sheathed it. The repetition of this pattern led Abram to a long conference room in which all of the furniture had been stacked against one wall. Munakata’s head snapped towards Abram to reveal a visage which which would have been indistinguishable from Morgan Stormrider’s if Munakata had not affected a hairstyle which went out of style during the Meiji Restoration; he waxed the front of his head, and let the rest of his hair grow long, binding it into a knot at the back of his skull. It exposed his furred ears, which were equilateral triangles with rounded points pointing directly behind him. He wore a black suit with a forest green shirt, black tie, and black waistcoat; the double-breasted jacket hung from a coat rack near the pile of furniture. Abram examined the jacket; his eyes widened as he read the label. “Caderousse & Sons of Marseilles? I’m surprised you care enough about clothes.”

Sheathing his katana, Munakata found a chair for Liebenthal before straddling the chair whose back he had used to hang his coat. “I’m a sellsword, not a barbarian.” His jade eyes fixed on a crow perched outside the windows. “Are you here for the last shipment?”

“Among other things. What happened last night?”

“Liebenthal thought there might be remnants of the Phoenix Society’s presence in Boston: support staff, Adversaries who had just returned from assignments in the vicinity, and the like. He had me take a dozen Fireclowns to the Boston Chapter and do a sweep. We found a couple of Morgan Stormrider’s friends, Sid Schneider and Edmund Cohen.” Munakata spat the names as if they tasted bitter on his tongue. “They drew their weapons first. I came within millimeters of tearing out Schneider’s throat; he managed to jump backward as I drew. The old bastard, Cohen, kept his distance and kept the Fireclowns busy with non-lethal grenades.”

Munakata rose, and began pacing in front of Mellech. His right hand clenched around his katana’s sheath, and he held it against his side as if he wished to draw it again. *His story aligns with what the Fireclown in the elevator told me before propositioning me. Unfortunately for him, I know they're both lying in order to hide the truth from Liebenthal. Thirty-six Fireclowns taken out with rifles loaded with tranquilizer rounds from cover, and Munakata disarmed twice by a bodybuilder. Jesus wept.* “I heard you had been shot.”

“I got over it.” Munakata’s left hand closed about the hilt, and his blade sprang forth, its steel rippling blue and grey beneath the ceiling lights. He danced a set of techniques at a pace which impressed Mellech, who disdained weapons himself. What use was a sword to one who could strike down enemies with their own nightmares given the semblance of reality? “I don’t understand Cohen’s game. He knows about us asura emulators. He knows how to put me down permanently. Why did he hold back?”

Here was a question Mellech could answer; despite playing the traitor, he still had access to the Sephiroth. They answered to Imaginos, and were in on the plot. “Stormrider’s orders specified no fatalities.”

“His orders usally do.” Munakata’s smile held a cynical edge. “If I were naive, I’d be shocked that almost all of his targets died while resisting arrest. However, given that the crimes of which Morgan’s targets were accused went unnoticed until the targets ceased to be of use to Imaginos, you will pardon my lack of surprise. The choice Morgan’s targets face is simple. Die by the sword, meeting your killer’s eyes, or die with a shank in the back and no idea who struck you down.”

“That’s the choice you think you will have to make once Imaginos has no further use for you?” Abram had spent a hours cultivating his tone in order to soothe those burdened by guilt so that he could ease their consciences for a reasonable fee. “It is written that those who take the sword die by the sword.”

“Better the sword than the cross.” Munakata sheathed his blade. “Where’s Stormrider?”

“He has only just arrived in Boston, and has brought a companion.”

“Who?”

“Naomi Bradleigh. Imaginos expects you to leave his daughter alone.”

Munakata turned his back on Abram, who followed his gaze out the window. A pack of bikers riding two abreast approached City Hall. “Imaginos’ expectations are no concern of mine. I no longer serve him. He betrayed me three years ago, and I mean to expose him. If I refrain from drawing my blade against his daughter, it will be because she did not first threaten me.”

“You had no such scruples when you cut down Rutherford, Collins, and Gabriel. Your technique left them with no time to draw weapons.”

Munakata looked away, as if to hide his shame. “And their blood will stain my karma. However, the Phoenix Society would not have sent Stormrider if I had let those Adversaries live, or offered them a semblance of a fair fight.”

“Speaking of whom, Imaginos expects you to tell Morgan the truth as you understand it. He won’t believe a word you say, but Imaginos expects you to rouse his curiosity.”

“As I said: Imaginos’ expectations are no concern of mine. I will speak to him if he does not draw his sword upon meeting me. I will attempt to persuade him that we share a common cause with Mr. Liebenthal. I doubt I will succeed. That is not your concern.”

“The souls of all the Christ’s children are my concern. I could ease the weight of sin you bear.”

“My sins made me what I am. I will keep them, and you can keep your Christ.” Munakata rose, and grabbed his jacket. He tucked it and his katana under his right arm. “I think I should continue to practice in the open air. Mr. Liebenthal is in his office. He’s expecting you.”

\###

