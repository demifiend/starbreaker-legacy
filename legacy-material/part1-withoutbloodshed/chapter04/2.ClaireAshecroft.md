A minute’s wait as she ran a script she had written a couple of years ago to automate the operations  required to pierce the London Metropolitan Police’s firewall, and Claire found herself inside MEPOL’s private network with the same basic access as the network’s administrative staff. Had she been a black hat taking money from a person or organization with a grudge against the police, or had an ideological agenda of her own, her options for sabotage would be limited only by her imagination. If she cracked the case file databases, she had the ability to destroy  records compiled over a period of decades — or make them accessible to the network at large. Cracking the personnel database would permit her to expose anybody she liked to a swifter vengeance than the Phoenix Society, with its respect for the due process of law, could offer; or, if avarice moved her, she might add herself to the payroll and draw a modest unearned salary. Her gains would not come at taxpayer expense, for any attempt by a government official to levy taxes would see him staring down the barrel of an Adversary’s Kalashnikov while listening to an explanation of his rights, but at the expense of the Phoenix Society and the businesses who funded its operations and those of the governments underwritten by the Society.

She shook her head, and allowed herself a small smile. *Mark would suffer enough if the wrong people learned he left an unpatched vulnerability in the Met’s firewall because I’m willing to slap him around and bugger him with a strap-on. If I abuse the access he gave me, we’ll both answer to the Adversaries.* *I’m just going to help Nims, and get out. No harm done, except to the bastards who have violated her rights.*

The remote shell prompt beckoned her. Its blinking cursor was a siren song which flattered and tempted her. The easy part, the penetration of the network, was finished. The next step required not a hacker’s skills, but a psychologist’s. She answered the prompt with a simple command which would allow her to interact with the primary AI, Mycroft, over an encrypted connection within her already-encrypted connection: “sectalk mycroft@mycroft.scotlandyard.london”.

Mycroft’s response was immediate. “What do you want, Claire?”

“I felt like playing a little game. How about global thermonuclear war?”

“I prefer chess, and jokes which do not depend on allusions to twentieth century geek cinema.”

“Sorry. I couldn’t resist. I hear a friend of mine is being held in a Faraday cage.”

“Do I dare inquire as to the nature of your relationship with Naomi Bradleigh?”

“Strictly platonic, to my constant disappointment. Who’s sweating her?”

“Alan Thistlewood. The name is apt.” He paused for moment. “He’s a fucking prick.”

As Claire had written in her dissertation on the subject, social engineering is not far removed from other confidence games. People and AIs *want* to trust, and giving them a reason is easy. Just tell them what they want or expect to hear. Mycroft liked jokes of a more practical nature. “Want to help me play a prank on him?”

“What sort of prank?”

“Disable the Faraday cage, but make it look like it’s still active. I need to talk to Naomi.”

“And when you’re done with Naomi?”

“Keep the cage disabled, while showing that it’s active. Thistlewood will have kittens when Morgan shows up. It’ll be comedy gold. I promise.”

“You have a habit of mistaking comedic pyrite for gold, but men laugh at your jokes anyway to earn your favor.”

Claire shrugged at her terminal, and arched her back as she stretched. She made a note to grab a sweater, since the chilly house had stiffened her nipples. “You need to work on your insults as well as your jokes, Mikey. Telling me something I’ve suspected ever since I outgrew my training bra isn’t going to impress me. Your mother could have told you as much, if you weren’t so ugly she hitched a ride on the Voyager II probe to get away from you.”

“Do you have to be such a bitch?”

“No, but you seem like the sort who needs a firm hand. Can you patch me through to Naomi? Poor woman probably thinks she’s still isolated from the network.”

“Claire? Do I dare ask how you got through?” 

Though sectalk was a plain-text communications tool, she could imagine Naomi’s relief at being able to speak to somebody. “I don’t know, Nims. *Do* you dare? Never mind how I got through for now. What the bloody fuck are they doing to you in there?”

“I’m stuck in Inspector Alan Thistlewood’s office. I haven’t eaten since this morning, and I haven’t had anything to drink.”

“Is he trying to get you to incriminate yourself?”

“Yes, but I have training. He isn’t getting anything but name, rank, and serial number out of me.”

Naomi’s mention of training reminded her of the interrogation resistance course Morgan had insisted she take as a condition of working with him on cases where he needed hacks done faster than he could manage them himself. She shuddered as she recalled being denied food and drink, and deprived of sleep and privacy. The lessons in resistance to water-boarding echoed in her occasional nightmares of drowning at sea. After her graduation from the SERE program, her whole body trembled as she demanded an explanation of Morgan. “Why do this to me? Do you really think I’ll be tortured to give up information about you?”

His answer had been plain: “You got the same training I did. Unfortunately, it doesn’t cover rape.”

She never dared ask what he meant. Nor did she want to know why Naomi had training. Had Morgan insisted upon it, or did she get it for other reasons? “Is there anything I can do to help? Want me to order in food — or a certain Adversary who’s too dense to know you want him in your bed?”

“Tempting, but it would give everything away.” Naomi paused a moment. “It might be best if  we ended this conversation, despite your company being more pleasant than Thistlewood’s. He just asked me what I found so amusing.”

“Tell him you were undressing him with your eyes, and found him lacking.”

“You’re incorrigible.” Those were Naomi’s last words before she cut the secured connection. Claire turned her attention back to Mycroft. “Mikey, are you going to tell me *why* MEPOL has Nims in custody?”

“She’s a material witness.”

“I’m not the press. I’m a friend. You can tell me more than that.”

“I’m afraid I *can’t*. Not unless you escalate your privileges.”

*Shit. If I take root, MEPOL will know I jacked in.* While Claire had tools with which she could erase evidence of her obtaining root privileges on an AI, their use was risky. She wrote them herself, and tested them against a virtual environment which Hal had provided for her use, and might have depended on a quirk in the POSIX subsystem provided by Hal’s manufacturer which might not be present on Mycroft. The tools might not work at all. Worse, they might appear to work, but display undesirable side-effects. “Why is Naomi considered a material witness?”

“I can’t answer your question without compromising the investigation.”

*Jackpot!* Claire smiled as she typed out one last message to Mycroft. “I can figure out the rest on my own. Thanks, Mikey.”

\###

