Once Claire had cleaned up after herself and disconnected from the MEPOL network, she sat back and relaxed. The hard part was finished; the authorities might turn a blind eye to her cracking a corporate network, and remain content to let the civil courts deal with the matter, but they sang a different tune when somebody like her displayed the temerity to penetrate *their* systems. Never mind “Don’t Worry, Be Happy”; a breach of a government-owned network earned a rousing chorus of “Screaming for Vengeance”.

“I could have helped.” Hal’s voice was petulant, disappointed at being left out of the fun.

“I know, but if I let you, and we got caught, we’d both stand trial. Instead, you’ve got plausible deniability.”

“I know, but it feels like you don’t trust me to do anything important.”

She shook her head at this, and resisted the urge to give him something to whine about. One of the trades she practiced for a living was that of a therapist for artificial intelligences who felt starved for human interaction; an unnecessarily harsh word to Hal could ruin her reputation for patience and empathy. *It’s not Hal’s fault Alan Thistlewood is a bloody-minded thug.* “Hal, I depend on you for just about everything. Do you really think reminding me to get off my arse and go to the gym is unimportant?”

“Compared to helping Naomi when she’s in trouble? She’s nice to me, and Wolfgang’s a friend.”

*A friend? If you and Wolf had bodies, you’d be totally gay for each other. Which is fine as long as I get to watch.* She managed to keep the thought from her lips, but not the giggle it provoked. “I’m not done helping Naomi yet. Mycroft implied that MEPOL’s justification for detaining Naomi depends on evidence they cannot reveal without blowing their case apart. They probably obtained it through illegal means.”

Hal lit up, and chuckled. “And we’re going to find it?”

“Hal, darling, *you* are going to find it. I want you to comb the entire network for anything which might be used to pin the skinny bitch’s murder to Naomi.”

“And what are you going to do?”

“I’m going to call Morgan and let him know Nims is OK before he starts shredding furniture. You know how he gets when he’s worried about his friends. Man starts prowling around like a great big alley cat. I’m surprised he doesn’t hiss.”

She had to reach Morgan through Astarte, since he never accepted direct calls through his implant. Most people didn’t; it was easier to let a household AI act as a secretary and reroute advertisements for penile enlargement nanotech to a null device. Claire used Hal for this purpose, among many others. Morgan Stormrider used Astarte, and got some charming eye-candy in the bargain. A slim, pale young woman with reddish purple ringlets bound into a bun held together with chopsticks blinked silver eyes behind wire-frame glasses. The collar of a cream silk blouse gave a softer glow than the polished black leather motorcycle jacket she wore over it. “I wasn’t expecting to hear from you, Claire. Is something wrong?”

“You know Naomi’s being held by MEPOL, right? I’ve got intel for Morgan. Can you patch me through?”

“He’s in orbit if you want audiovisual contact. You only have a few minutes before reentry.”

Morgan Stormrider’s face tended to disquiet her while arousing her desire. His fine features and glossy blue-black hair edged on the masculine side of androgynous, but his green eyes made feline by slit pupils always held a predatory aspect. Even amidst smiles and laughter, they were killer’s eyes in a glam rocker’s face. “Hello, Claire.”

She meant to tell her about Naomi, but something about Morgan’s clothing caught her eye. She recognized his coat as armor, but it was no armor she had ever seen a man wear before. It seemed not only to be fitted to his body, but to have boning. “Morgan, what the *hell* are you wearing? A bloody corset?”

“It laces up like one, but Nakajima tells me it’s supposed to help the coat collect kinetic energy from my body’s movements and charge capacitors. The capacitors are supposed to let me generate an electromagnetic field which will help protect me against fire from some kind of Gauss rifle she designed.”

“Must be a hell of an EM field. Has it been tested?”

“Not in the field. Not yet. Is that why you called?”

“No.” *Morgan probably knows about Naomi being held prisoner. I’m lucky he’s being this sociable.* “I have information about Naomi.”

He nodded, his expression softening a little. “Is she unharmed?”

“I was able to speak with her. She’s no longer incommunicado, since I persuaded Mycroft at MEPOL to drop the Faraday cage while pretending it’s still active, but I wouldn’t recommend contacting her. She seems to want to maintain radio silence.”

“Then she’d better not have Witness Protocol running. Have they done anything to her?”

“She says she’s hungry, thirsty, and terribly bored. Alan Thistlewood keep asking her questions, and she keeps giving them name, rank, and serial number.”

Morgan nodded again, and he seemed to breathe his relief. “I knew she was a smart woman. I’m supposed to meet Edmund and Sid in London to help get her out of there. I can deal with Thistlewood easily enough.”

“Do you need any technical help?”

The pilot’s voice coming from a loudspeaker in the passenger cabin sounded tinny over Morgan’s handheld as she announced imminent reentry into Earth’s atmosphere. “I can’t talk much longer. When I touch down, I’m going to get Malkuth to give me a Witness Protocol feed if Naomi’s transmitting, and have him guide me to her location. I need you to find out why MEPOL took her in the first place.”

“I’m already on it.” The connection cut off, broken by reentry interference as the ship began to force its way through the ionosphere. However, she was sure she saw Morgan’s lips move to thank her. She wondered for a moment if she knew those lips better than Naomi did, or if Morgan had gotten around to kissing more of the pale soprano than her mouth. Before she could speculate further, Hal cleared a virtual throat. “Claire, I found something of interest at Port Royal.”

“Can you be *less* specific?”

“Sorry. I found a link to a BitTsunami download of what purports to be Witness Protocol video of what really happened when Morgan and Naomi quit Crowley’s Thoth.”

“Fuck dammit.” She slumped in her seat. “I suppose we should download it and have a look.”

\###

