Claire Ashecroft could think of worse ways to start a cold winter’s morning than feeling a man’s morning wood pressing into the cleft of her bottom as he spooned with her. His muscular arm, tattooed at the forearm with the image of an undead cowboy and a “Killer Saints” banner, would have held her close to him if they had the bed to themselves. Instead, his arm draped over her body and over the chest of his boyfriend, who had the same tattoo on his calf. She had not expected to meet a couple of acquaintances from her gamer tribe at a nearby gay bar, and had been content to leave them alone she saw them kissing. However, when one of them broke the kiss and invited her over with a gesture, she knew better than to resist. She caressed the chest of the man with whom she was spooning, and tweaked his nipple as he stirred. “Isn’t it still early, Claire?”

She checked the time. Noon was hardly early, unless one had been up until dawn being the center of attention for a pair of friends turned lovers who never pursued a woman unwilling to have them both. “Don’t worry, Nigel. You and Basil don’t have to get up yet, but Hal says there’s a problem.”

“All right, then.” Nigel sat up, and shifted so that Claire could wriggle out of bed. As she put on last night’s jeans and t-shirt over a fresh workout bra and panties, she smiled at her guests while  they snuggled together under the covers. “Stick around. I might have a use for the two of you when I’m done.”

Basil stuck his tattooed arm out from under the covers to give a thumbs up, causing Claire to shake her head as she closed the door behind her. *It’s obvious they prefer each other’s company, and women like me are just a bit of variety. I can’t complain, though; I had to beg them to touch each other in front of me.* She padded into the kitchen and began to rummage through the refrigerator. She examined plastic containers filled with assorted take-out leftovers in various stages of evolution towards consciousness. She threw the rejected containers over her shoulder and into the trash bin marked with a biohazard symbol. “Come on. I know I brought home leftovers last night.”

“Try the bottom shelf.”

She pulled out a container of beef chow mein from a place called Lee Ho Fuk’s, and looked over her shoulder at the kitchen screen to meet the unblinking green eye of her household AI, Hal. “How did I not remember where I put this? It’s huge.”

“I think you meant to share it with your guests. Speaking of whom, they’re quite busy in your shower. Should I record them?”

“They didn’t want me to record them using my implant. I’m not going to record them without permission when they think they’re alone. I thought you knew me better.” She shook her head as the microwave dinged, and pulled out her irradiated beef chow mein.

“Fine.” He paused as Claire found a pair of chopsticks. “Are you sure you should be eating so much?”

“I’ll work it off in the simulator after I’ve dealt with the reason you made me get out from between two yummy bi men. I want to try out Mindcrime Interactive’s new expansion for Ultraviolent Radiation. I meant to check it out last night with Josefine, but she never showed up.” She shook her head, still annoyed by her old university friend’s tendency to let her obsession with work rule her life.

“Have you considered a form of exercise that doesn’t involve electronics?”

“I tried aerobics once. It was the most horrible twenty minutes of my life.”

“I wasn’t joking. Hrist from Valkyrie Gym sent a message this morning. They don’t want to keep taking your money if you’re not going to show up and lift.”

The chopsticks fell from her fingers and made a soft bamboo clatter against the kitchen tiles. “*What?* I missed a week. A bloody *week!*”

“Hrist says it was a month. Should I remind you to go tonight?”

“Yeah. And don’t let me run any software or access the net if I ignore you. And whatever you do, don’t offer to resume our Shin Megami Tensei campaign.” She considered the calluses on her hands, one of which had cracked and begun to bleed a month ago. *The callus healed weeks ago. I’ve just been too chickenshit to go back. Lots of lifters get split calluses; I just have to be better about taking care of my hands.* “Also, please remind Hrist to adjust my training regimen to account for my absence.”

“Done.”

“Thanks.” She chewed a piece of beef. “Has anything interesting happened in the real world?”

“There’s a media release from AsgarTech concerning Project Æsir. Little Josefine’s been busy. Also, the Phoenix Society is talking about quelling a coup d’état in Boston by sending Morgan. Did you want celebrity news today, as well?”

“Only if I fucked the celebrities in question,” said Claire as she finished her coffee. “And the closest I’ve come to fucking a celebrity lately is snogging Christabel under the mistletoe last Winter Solstice.” She paused for a minute, considering a spoonful of leftovers. “Now that I think of it, she started acting different after that party.”

“Christabel’s dead. MEPOL says she was murdered last night in her house at Crouch End.”

“Holy shit.” She had never liked Christabel, and maintained to Morgan for years that he should be dating Naomi instead, but she never wanted to see her dead. “Do Morgan and Naomi know?”

“I just got this intel, so don’t yell at me. MEPOL has Naomi.” Hal’s green eye turned red. “The official line is she’s a material witness. But nobody keeps a material witness in a Faraday cage to keep them from accessing the network. I can’t get through her. Neither can her own AI, Wolfgang. As soon as they brought her to one of the inspectors’ offices, she dropped off the network.”

“Not without a bloody warrant, and not unless they’ve got a cracker in custody. Somebody like me, if I wore a black hat instead of white.” She dumped her chow mein in the trash, and added the bowl and chopsticks to the effusion of used dishes clogging both of her sinks. Cracking her knuckles, she padded down the hallway past her bedroom. She stopped for a moment, and could hear them watching one of her vintage porno movies. From the sound, they were probably watching a *Take It Like a Man* movie, or *Dirty Gear Solid 3: Muff Diver*. Though they were her favorites, she had more pressing matters before her than walking in on her guests, pretending to scold them for being perverts, and making them enjoy their punishment at her hands. “Hal, tell Nigel and Basil that they can help themselves to some of that beef chow mein and a couple of beers if they want to make themselves useful and clean up around here.”

“You’re shameless. When you bring lovers home, you’re supposed to make them breakfast, not put them to work.”

“If some arsehole hadn’t murdered Christabel, and some *other* arsehole hadn’t decided to hold Nims incommunicado, they’d be having *me* for breakfast right now.” She opened the door to her workshop and flicked on the light. A vinyl sculpture of Hiro Protagonist saluted her from atop a bookcase crammed with parts for vintage computers, his katana raised overhead. A partially assembled Silicon Graphics workstation from the late 1990s sat atop the workbench; a shell prompt blinked from its widescreen display, which was famous both for being ahead of its time and having a nonstandard connector. Next to it sat a classic Macintosh, and between both lay a partially field-stripped Thompson submachine gun. Over the worktable hung a framed digital painting by one of her occasional lovers, which depicted her as she appeared as fellow players of a competitive combat simulation called *Heartless Souls*: an iron circlet bound her auburn curls, black studded leather encased her generous figure, and her pale green eyes and piratical grin enticed challengers to dare her rapier and buckler. 

None of this held any interest for her now. Instead, she yanked the rolling chair out from under the desk opposite the door, and logged into a terminal connected directly to Hal. Her fingers tapped out a command which would connect her to MEPOL’s AI, Mycroft, using an exploit in the London Metropolitan Police’s firewall. The exploit remained unpatched for a simple reason: the system administrator was obsessed with letting Claire humiliate him, and would do nothing which might jeopardize his chances of getting to kneel and abase himself before her. Though many of her fellow hackers disagreed, to Claire this was just another form of social engineering. 

Using this exploit would, however, leave Claire liable for damages in a lawsuit. Hal would also be liable, if the City of London proved his involvement. “I’m going to do this myself.”

\###

