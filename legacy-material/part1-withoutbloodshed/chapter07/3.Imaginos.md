Imaginos considered the invitation for the tenth time since receiving it from a bike messenger this afternoon. It bore no return address, but was addressed to “Dr. Isaac Magnin”. Both the envelope and it’s contents were made of a soft cream-colored paper, which felt like vellum in his hands. The paper held the scent of camellias, and the invitation consisted of a single sentence written in a feminine hand which would have been considered old-fashioned in Shakepeare’s time: “I would have the pleasure of your company at dinner tonight.”

It was not the sort of invitation one refused, not if one recognized its source, and Imaginos knew what this invitation portended. Thagirion, the eldest of the Disciples of the Watch, had begun to pay attention to wider concerns than those of the New York Philharmonic and the needs of her rooftop garden atop a luxury tower overlooking Central Park from Fifth Avenue called Hanging Gardens. Her concern had lapsed for a couple of decades while she lived a human life as a heiress who devoted her time to horticulture and classical music, but Imaginos knew it would not last. *She’s done being Tamara Gellion for now, and means to collar me again,* he thought, betraying himself with a rueful smile as he stepped out of the limousine. *I could oppose her, and attempt to force her to acknowledge me as her equal, but at the price of revealing myself.*

He recognized the doorman, who stood at military attention as he approached. Though the doorman wore livery instead of an armored coat, concealed his pistol in a shoulder holster under his jacket instead of wearing it openly, and wore a smallsword on his hip instead of a longsword slung over his shoulder, his face marked him as kin to Morgan Stormrider: a Model 101 asura emulator. Imaginos knew them all by name and number alike; the doorman, who stepped forward to meet him, was unit 512, Eric Ulrich. He lived in Queens with his wife, two sons, and a cat whose full name was His Holiness the Second Coming of Frank Zappa. The name was his wife’s idea. Ulrich raised a white-gloved hand to halt Imaginos. “Your business, sir?”

Imaginos offered the invitation, which he had slipped back into its envelope. “I am here at Ms. Gellion’s request.”

Ulrich held the tips of his first two fingers against his ear, a gesture intended to show that he was using his implant to communicate with somebody else. He soon lowered his hand, and opened the door. “Ms. Gellion is expecting you, Dr. Magnin. Please come in.”

“Thank you.” Imaginos offered the doorman a business card. “The AsgarTech Corporation can always use good people for its security staff. Please consider us.”

Ulrich nodded, and pocketed the card. “I’ll consider it. Thanks.”

The lobby of Hanging Gardens reminded Imaginos of the temperate rainforests near the island of Vancouver across the continent and further north. Unlike most artificially heated dwellings, the air was moist, and carried a mélange of scents: rich, moist earth; mulch, fruit fallen from the trees lining the lobby with the precision of columns, honey from a beehive nestled somewhere out of sight, and flowers he never thought to learn to identify by scent. A bumblebee buzzed past, doing a close flyby of Imaginos’ head on its way to an azalea. *I suppose the rooftop was no longer enough for Thagirion. She must have purchased the entire building and renamed it.*

A framed article hanging from the wall behind the building manager’s desk confirmed his suspicion. Her purchase and renovation of the building had drawn the attention of the venerable *New Yorker*. The manager, one Frank Kissel if the nameplate on his desk was to be believed, noted Imaginos’ attention. “It’s the damnedest thing. I came in one morning, and Ms. Gellion is sitting in my office holding my paycheck. Have you ever seen her?”

Imaginos nodded. “She’s an old colleague of mine. I trust Ms. Gellion still has her penthouse?”

“The whole damn floor is hers now. I’d better come with you. You need a code.”

The penthouse differed only from the lobby in that it had access to natural sunlight. The roof was gone, and replaced in its entirety with glass to make of the top floor a greenhouse. The rustling of leaves created by an artificial breeze mixed with the hum of insects and the tangle of birds’ calls to one another, which was sometimes punctuated by flapping when some of them took flight. Under all of these sounds, Imaginos heard a cello. Its strings sang a tenor rendition of a piece whose melodic complexity suggested an origin as a part written for an instrument more often used for solos than the cello.

Thagirion was intent upon her music when Imaginos found her. The low-backed chair on which she sat allowed him an unobstructed view of a ebony cascade of hair spilling over shoulders bearing the slightest tint of honey. He spent years suspecting that she wore backless dresses whenever the customs of the time permitted it because she was fully aware of the effect the contrast of her hair over her skin had on men. In that respect, she was like her sister Ashtoreth. 

“Please sit down.” The command in Thagirion’s voice was clear despite its soft tone. She indicated a chair with her bow while turning to the next page of her music with her left. Her eyes were the same amber as Ashtoreth’s, and Imaginos fancied for a moment that she regarded him over the top of her score while playing. To distract himself from idle speculation as to Thagirion’s reasons for summoning him, he focused on the music, and tried to identify it.

“I don’t recognize the piece.”

Thagirion favored him with a knowing smile as she put away her cello. The pages whispered as she gathered up the music and slipped it back into its envelope. “It’s not part of any standard repertoire. I was asked to join a small ensemble to record the score for a new documentary film which profiles several former Adversaries and allows them to tell their stories.”

“Have I heard of this film?” Imaginos doubted it, but knew his ignorance was his own fault, since he gave minimal attention to cultural news and events.

“It’s called ‘Disposable Heroes’. I was tempted to recommend that the director interview *you* as an expert on the matter, to offer a different perspective.”

Imaginos followed her in silence, and considered his reply. *After knowing the lady for ten thousand years, I should be able to figure out when she’s deadpanning.* She led him through one garden room after another, identifying plants of which she was particularly proud when he stopped to examine an unfamiliar bloom. By the time they had reached what appeared to be the dining room, for it was clear of foliage and contained a table set for two, he knew what to say. “I appreciate your restraint.”

“Did you think I would betray our cause by exposing you? Satheriel is directing the film, using his Samuel Terell alias.” Thagirion shook her head at the notion as Imaginos drew back her chair for her. Once they were both seated, a servant strode from the kitchen bearing a fresh-carved roast. The bison steamed, and smelled of wine and spices as the servant lifted a slice thick enough to be considered a steak onto Imaginos’ plate. He then served his mistress as another server came, bearing a bottle of Callo Merlose cabernet sauvignon. This servant showed the bottle to Thagirion, and waited for her approval before opening it and pouring. With the first course served, the servants withdrew. Thagirion raised her glass once the door had closed behind them. “I am not your enemy. If I meant you ill, I would challenge you on neutral ground, rather than spoil the peace of home.”

Imaginos nodded, and raised his own glass. “In that case, madam, I thank you for your hospitality.” He sipped the wine as Thagirion sipped hers, and tried the roast before him. The meat dripped clear and reddish, its juices mixed with the wine in which it had been roasted. It melted in his mouth before he could chew it, and left a soft echo of spice on his palate when he swallowed. “You have a new chef. He’s excellent.”

“I’ve retained Monsieur Baptistin for a decade now.” Thagirion smiled at him over the rim of her glass. “But I last had you over to dinner before I found him. He’s eccentric, of course. ‘Monsieur Baptistin’ was the manner in which he introduced himself to me, and I have no other name for him. You would recognize him, as you recognized my doorman.”

“You employ the asura emulators I created to act as possible bearers for the Starbreaker?”

“I employ the ones you ignore as defective, the ones you write off as failures. They are not the fighters you need to be, and many of them are convinced of their psychosis before I find them; they believe they hear God speaking directly to them, commanding them to kill.”

Imaginos put aside his knife and fork; he no longer wished to eat. “What would you have me do? I do not ignore these men. I watch over them daily, and even the least of them is known to me. However, I cannot use them. Of the six hundred and sixty-six asura emulators I created, only a hundred entered ACS and faced the trials by which we forge Adversaries and weed out those unsuited to wield the Starbreaker. Of those hundred, only two proved suitable. I cannot use Munakata Tetsuo, for he is sworn to expose me.”

“I would have you explain yourself. You have asked my sister and me to trust you, yet you foist a guest upon her without explanation, and continue to associate with the traitor Adramelech despite having denounced him yourself.”

*You are too like your sister. You ask the same questions, and with lips and voice so similar I might mistake you for Ashtoreth and attempt to comfort you as I would her. No doubt my kiss would enrage instead of soothe.* “If Morgan destroys only Sabaoth, and is destroyed in turn so the Starbreaker can be bound once more, we will have eliminated but one threat to this world.”

Thagirion made no immediate reply, but instead returned her attention to her dinner. The meat, which had cooled, began to steam again, and Imaginos knew that she had used an energistic pattern to warm their dinner. He returned to his own meal, lest he offend his hostess by staring at her, and found himself caught off guard when she spoke again. “What other threat would the world face in Sabaoth’s absence?”

*You are as blind as your sister if I must put words to the truth.* “Any ensof who tampers with evolution is a threat to intelligent life. Have I not tampered with humanity’s biological and cultural evolution in my repeated attempts to purge from the species the tendency of individuals to stop thinking and obey when given orders by anybody they consider a legitimate authority?”

“Are you bereft of reason? You would have Morgan Stormrider turn the Starbreaker upon *you* once he strikes down Sabaoth?”

Imaginos shook his head in disappointment. “Such is the reaction I expect from Ashtoreth, which is why I have held my silence and asked her to trust me. I did not think you cared enough for me to ignore the truth.”

“I’ve had ten thousand years to learn to fear you when you speak of truth.”

“The truth is simple. I set out to fight a monster, and became one myself.” He remembered their last dinner together. Ashtoreth had been there, along with Sathariel, who had been Thagirion’s lover at the time instead of Ashtoreth’s. The ladies had warned him of the consequences of his methods. But with the stench of Dachau overshadowing everything else, the only response he could muster was the creed they shared: “Our sins shall save the world.”

“I defenestrate people for paraphrasing Nietzsche at the dinner table.”

*Deadpanning again?* “Before you show me out, would you answer a question?”

She waved an elegant hand as if to prompt him. He meant to ask her why she cared so much. A different question sprang to his lips. “Why all the gardens?”

Her expression grew pensive, and she fell silent for long minutes. She stared at her wineglass, which still held a mouthful, before finally draining it. “This isn’t home. It never was. It can never be. It’s just the only home I will ever have.”

The sight of a tear caught in her eyelashes, ready to spill down her cheek, stripped Imaginos of any ability to dissemble. “I don’t understand.”

“I don’t expect you to understand. This is *your* home, your birthworld. I was born on a different world, beneath different suns. And though the plants and insects and animals aren’t those I remember, they have to be close enough.”

The sight of her suspended tear was intolerable. He rose, his napkin in his fist, and caught it before it could finally fall. “Your birth star is lost in the redshift, and I cannot reclaim your true home for you. But I swear, Thagirion, by everything you ever called holy, that you will never have to give up *this* home. You don’t have to believe in me. Just trust your sister, who believes in me.”

“And everything I’ve seen thus far, even the coup in Boston, is part of  your plans?”

“I did not expect Alexander Liebenthal to take the money I paid him to deliver weapons to the Repentant in Christ and buy himself a conscience, but his coup is part of my plans *now*. The manner in which Stormrider quells this rebellion will tell us much about him, and whether he and his can be trusted.”

Thagirion nodded, and took a breath to compose herself. “I owe you an apology. After I spoke with Ashtoreth, I was prepared to demand the return of the Starbreaker. I would have bound you here until it was returned.”

“Why do you think I have it?” Imaginos suspected he knew the answer, but wanted to hear it from Thagirion’s lips. He did not possess the Starbreaker himself, nor did he keep the weapon in a location to which he had direct access. Instead, he entrusted it to his brother, Desdinova, whom he had instructed to give the weapon to Morgan Stormrider should Imaginos judge him suited to bear it. However, he doubted Thagirion knew any of this. Desdinova was too firm a believer in his father’s faith that *containing* the ensof was enough. It made him a suitable guardian for the one weapon capable of destroying an ensof, instead of merely shattering its avatar, since he was content to let their enemy remain bound beneath the Antarctic ice.

“Sabaoth speaks to me at times, when I am weary and my defenses are down.” She shook her head, and began to massage her brow as if trying to ward off a headache. “I did not expect this when I agreed to take over the duty of maintaining the bindings.”

“I apologize, Thagirion. He never spoke to me, or I would have warned you.”

A smile bloomed from her lips. “He *hates* you. He never forgot the manner in which you humiliated him when he first manifested in Egypt. All you had was a dev’astra, and you destroyed his avatar before his human followers.”

“I was an impetuous youth.”

“You still are, compared to Ash and me. I never told you why I was prepared to hold you hostage until the Starbreaker was returned to my keeping.”

Imaginos shook his head, and returned to his seat after refilling her glass. He refilled his own, and considered the roast for a moment. Being ensof themselves, neither he nor Thagirion needed to eat, but it was pleasant to do so. “You never had to. You wanted to see for yourself that I remain in command of events.”
