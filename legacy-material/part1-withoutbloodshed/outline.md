# Outline for _Without Bloodshed_ #
## Summary ##
To prove that the Phoenix Society, a non-government organization dedicated to ensuring a balance between law and liberty, does not simply assassinate its critics, Morgan Stormrider of the Society's Adversary corps must depose a dictator backed by an ex-Adversary and a heavily armed biker gang without killing anybody. His mission is complicated by the murder of his ex-girlfriend Christabel Crowley, and by his own desire to lay down his arms and begin a new life with his longtime band mate, Naomi Bradleigh.
##Plot Outline##
1. The Unforgiven
	1. Imaginos (AKA Isaac Magnin) and Ashtoreth (AKA Elisabeth Bathory) debate the best means of recruiting Morgan Stormrider in Christabel Crowley's home in Crouch End. 
	2. Imaginos murders Christabel, but did he really?
2. No Refuge but in Audacity
	1. It's Saul Rosenbaum's first day back on the job at the New York office of the Phoenix Society after a month-long vacation, and everything's gone to hell thanks to Alexander Liebenthal pulling a coup d'état in Boston and accusing the Phoenix Society of using assassins. (Of course, the Phoenix Society is just the ACLU with a global reach and a tendency to use assassins instead of attorneys, but they don't like to admit the fact.)
	2. Saul, Iris Deschat, and Karen Del Rio want to send Morgan Stormrider to Boston, but can't reach him. His AI, Astarte, won't tell them where he is or take messages.
	3. One of the Phoenix Society's AIs, Malkuth, reports that Morgan Stormrider's IP address just showed up in Tokyo, and that Scotland Yard is holding Naomi Bradleigh in apparent connection with the murder of Christabel Crowley of the prog metal band Crowley's Thoth; the connection is only apparent because Naomi hasn't been charged and is being held in a Faraday cage so that she can't access the network.
	4. Saul decides to call Astarte again, telling her that Christabel is dead, Naomi is in trouble and that Morgan needs to know.
3. Even Gods May Stand Defenseless
	1. Morgan arrives at the Tokyo headquarters of the Nakajima Armaments Company of Nippon to meet with its president, Chihiro Nakajima.
	2. Ms. Nakajima asks Morgan to help her investigate an incident of industrial espionage involving a design for a weapon she chose not to bring to production because it was so powerful that no human, however well-armored, could survive being shot with it. Nakajima says of the weapon, an electromagnetic mass driver that fires charged iron flechettes, "Before this weapon even gods may stand defenseless."
	3. According to the evidence she gives Morgan, an ex-Adversary named Tetsuo Munakata appears to be involved, and weapons based on her designs are being sold underground to religious groups like the Repentant in Christ. 
	4. Before Morgan can decide, Astarte calls him to report the news Saul Rosenbaum passed to her at the end of Chapter 2. When Morgan explains the interruption, Nakajima offers to arrange a suborbital flight back to London for him, and offers to arm him, since he came to Tokyo without weapons or armor.
4. Social Engineering
	1. Claire Ashecroft has several ideas for a perfect morning, and waking up sandwiched between two bi men she picked up at a LGBT nightclub the night before is one of them. She leaves them sleeping, has leftover takeout for breakfast, and reads the news.
	2. Claire's news mentions a press conference given by the AsgarTech Corporation to discuss the project of their Asura Project, with which Claire's best friend Dr. Josefine Malmgren has been involved for the last couple of years.
	3. When she's done with the article, her AI (Hal, inspired by HAL-9000 from 2001: A Space Odyssey) tells her that she might want to check the bootleg media site Port Royal, as a torrent purporting to contain Witness Protocol video showing Morgan Stormrider dumping Christabel Crowley is getting a shitload of downloads.
	4. Claire examines the file, and finds out that it’s not only authentic, but heavily encrypted. She has Hal get her hooked up to Malkuth, who gives her a decryption key in exchange for a promise to help the Sephiroth (the Phoenix Society's ten AIs, of which Malkuth is one) figure out who cracked them, and how they managed to access Witness Protocol data without leaving any evidence of their activities. The video doesn't just prove a security breach; it's being used by Scotland Yard to justify holding Naomi Bradleigh in custody.
	5. Since the Witness Protocol Data appeared to come from Edmund Cohen, Claire decides to start with him. She also chews out Hal for not mentioning that Christabel had been murdered right away. Hal's explanation? "I didn't think you'd care, since you thought she was a stuck-up bitch."
5. Conduct Unbecoming
	1. Edmund Cohen is hung over after a wild night of sex, drugs, and rock 'n roll, and he doesn't remember what the hell happened the night before. His AI, Savannah, is being flooded by calls from Saul Rosenbaum and Morgan Stormrider about Naomi being held in a Faraday cage by Scotland Yard.
	2. He gets a call from Claire Ashecroft, who isn't at all happy with him because of the file he or one of his guests uploaded to Port Royal the night before, which Scotland Yard is using to justify holding Naomi.
	3. Edmund gets Claire to help him figure out what happened last night. It turns out that Elisabeth Bathory of the Phoenix Society's executive council seduced him, plied with wine, and got the root password to his AI so that she could upload the file from his house. As a result, Scotland Yard thinks Naomi Bradleigh either killed Christabel Crowley, or knows something about the murder.
	4. Edmund, aghast at his stupidity, resolves to repay Claire's help after setting things right with Morgan and Naomi.
6. My Own Savior
	1. Naomi Bradleigh finds herself in Scotland Yard's custody. She's hungry, tired, and unable to connect to the network.
	2. As Inspector Alan Thistlewood attempts to intimidate her into incriminating herself, Naomi bides her time and tries to create an opportunity get at his gun and turn the tables.
	3. Once she has Thistlewood's gun, she holds him at bay as Morgan, Edmund, and Sid Schneider burst in to rescue her.
	4. As Morgan and the others lead Naomi out of Scotland Yard, they're challenged by Thistlewood's superior, who is honestly unaware of Thistlewood's abuses.
		1. Chief Inspector Gregory Windsor insists on resolving the matter, and he and the others end up watching the video that was used to justify taking Naomi into custody. 
		2. The video shows Morgan dumping Christabel, who starts a tirade about how Morgan never cared about the band but only used Crowley's Thoth to be around Naomi Bradleigh. 
		3. Morgan offers to leave the band at the end of the tour if Christabel doubts his commitment or his professionalism.
		4. Christabel says, as Naomi hears the commotion and enters the hotel room: "You and Naomi will have to kill me before I let you go."
		5. Inspector Windsor concludes that this video wasn't enough evidence to establish any sort of motive, especially since the preliminary results from the post-mortem show that Naomi's katana had last been unsheathed a year ago so that it could be cleaned and oiled, and the cause of death didn't involve a sword.
		6. Windsor agrees to let Naomi go, and restores to her property taken as evidence. The property includes a ruby tennis bracelet that Morgan had given Naomi last Winter Solstice (the same bracelet that had provoked Christabel's tirade) -- and her katana, a Nakajima blade.
7. In Camera
	1. After a short scene in which Adramelech, still disguised as Rev. Abram Mellech, reports to an unseen voice that his faithful are fully armed with weapons no armor can block and fully prepared to die for their God, we see Ashtoreth spying on him as Elisabeth Bathory. She attempts to figure out who he's betraying.
	2. Cut to an in camera meeting of the Phoenix Society's executive council as Ashtoreth presses Adramelech back against the altar and kneels before him. No need to actually depict the blowjob.
	3. Imaginos, Thagirion, and Desdinova (who doesn't use an alias) have gathered under cover of a Phoenix Society executive council meeting. Thagirion is concerned about Adramelech's actions, but Imaginos eases her concerns.
	4. Imaginos then reports that it's time to cut Alexander Liebenthal loose, since he has served his purpose. Though Morgan's official orders will be to achieve a nonviolent resolution, Imaginos will instruct his agent Tetsuo Munakata to attempt to make it impossible for Morgan to depose Liebenthal without bloodshed. This is a test for Morgan: if he fails this mission, the Phoenix Society can simply discredit him as being emotionally compromised by Christabel's murder, and unfit to have taken on a mission of such importance.
	5. Thagirion is concerned that Edmund Cohen might interfere, since he acted on his own by giving Morgan a warrant to go to Scotland Yard and rescue Naomi. She questions the wisdom of keeping the man around, since he does not wholly sympathize with their cause in that he questions their methods.
	6. Imaginos explains that he expected Cohen to attempt to atone for his role in Naomi Bradleigh's illegal incarceration, and when Morgan finds out that Edmund is responsible for Naomi's predicament, they will have an opportunity to test the bond between the two men. If Morgan still trusts Edmund despite his betraying Morgan's love, then Desdinova can use their common friendship to ingratiate himself with Morgan. If that doesn't work, then Ashtoreth can attempt to connect with Morgan herself.
8. Duet
	1. Morgan is at Naomi's house after a brunch date with her. He's talking with Saul Rosenbaum, who wants him on a private express maglev with Edmund and Sid at 7:30am London time. The maglev will take them to Boston, and Elisabeth Bathory will brief them en route. Arrival time in Boston should be 9:00am local time.
	2. After Saul lets him go, he hears Naomi upstairs playing the piano. He sneaks upstairs and watches her for play for a bit. She's wearing her reading glasses, since she's playing from sheet music, and Morgan loves the way they look.
	3. When she's done with her piece, she shifts over on the stool and invites him to sit down, saying that she needs an extra set of hands for the next piece.
	4. Their hands keep touching as they play, and when it's over, Naomi kisses him. Afterward, she tells him that she's wanted to do that for years, but behaved herself after he broke up with Christabel for the band's sake. She kisses him again, but Morgan stops her before they can go further and insists that today isn't the right time, since Christabel hasn't been buried yet. 
		1. He also doesn't want to take advantage, since Thistlewood all but molested her the day before.
		2. However, he doesn't mention this to Naomi.
	5. The next morning, Naomi wakes up alone at 6:00am even though she persuaded Morgan to sleep with her without making love. She misses the warmth of his body pressed against her back, spooning her and holding her close, and wonders if he got uncomfortable and retreated to the couch.
	6. She finds him in the kitchen, making breakfast in uniform. He serves her, and explains that he would have left breakfast for her in the refrigerator with a note if she had stayed asleep.
9. Devil in a Black Dress
	1. Ashtoreth meets Morgan, Eddie, and Sid on the private express maglev from London to Boston. She finds his sudden shyness around her amusing, and notes Sid's physique, but saves most of her attention for Morgan.
	2. She explains the mission in Boston to them, and answers any questions they might have.
	3. After the briefing, she encourages them to sleep and get a bit of rest. She watches as Sid and Edmund sleep under the influence of the gentle bit of magic she used to massage their brain chemistry into a sleepy state.
	4. Morgan, however, is still awake. Rather than succumb to the urge to sleep, he's using a terminal provided by the maglev to request access to Witness Protocol feeds recorded just before their deaths by the Adversaries killed by Tetsuo Munakata during the coup.
	5. Elisabeth leans in close, looking over his shoulder, and asks him what he's doing. Morgan explains that since Munakata killed three Adversaries before they could draw their weapons and defend themselves, he thought it was to see how Munakata did it so that he could. It turns out that Munakata's been working on his sword technique, and was able to behead the first Adversary while drawing his sword. The others were too shocked to effectively defend themselves, and Munakata cut them down without effort.
	6. When Elisabeth asks him if he'll be able to counter Munakata's opening, Morgan tells her that all he has to do is duck -- but simply ducking isn't very stylish.
10. No Prayer for the Dying
	1. Abram Mellech (Adramelech) has returned to Boston's City Hall, where Alexander Liebenthal has taken up residence after the coup. He first finds Tetsuo Munakata practicing with his sword.
		1. Mellech cautions Munakata against relying too much on his new iaijutsu (attacking while drawing the sword) techniques, since Morgan Stormrider is a 100 series Asura Emulator like him, and thus his equal.
		2. Munakata refuses to acknowledge Morgan as his equal, and insists that Morgan doesn't use technique when he draws his sword, but brute force and ignorance.
	2. Leaving Munakata to his training, Abram Mellech then finds Michael Riordan of the Fireclowns Motorcycle Club.
		1. Mellech informs Riordan that he won't be needing the Fireclowns MC to escort the last shipment.
		2. Riordan tells him to fuck off, since his contract doesn't oblige him to take orders from Liebenthal's customers, but only from Liebenthal.
	3. Liebenthal is waiting for Mellech in his new office, and greets the priest with contempt. He has the last shipment ready, and is waiting for Abram Mellech to pay him. He asks Mellech if he gets the money to pay him by bilking old ladies out of their savings, but Mellech insists that he gets his money by providing spiritual advice and reassurance to people like Liebenthal: rich people in need of a clear conscience.
	4. Liebenthal tells him that his services won't be needed in that case, since he has a clear conscience. He knows what he's done, and is prepared to die to expose the Phoenix Society.
11. Three Adversaries Walk into a Bar
	1. Morgan, Edmund, and Sid arrive at the Four Winds Bar in South Boston that night, after spending the day using indirect means to gather more information, such cracks Liebenthal's AIs. Thanks to help from Claire, they're able to get Liebenthal's records from his AIs, but they're encrypted, and the encryption will take time to crack.
	2. Morgan lays a check issued by his credit union on the bar and instructs the bartender to ensure that every Fireclown gets his drinks courtesy of him. The money should be enough to ensure free beer for the Fireclowns for at least a month, or good whiskey for a week.
	3. Michael Riordan confronts Morgan, and asks him what he's trying to accomplish.
	4. Morgan explains, while Edmund and Sid stare down the other Fireclowns, that he wanted to get Riordan's attention by proving that he has the financial means to buy out the Fireclowns' contract with Liebenthal.
	5. Riordan questions Morgan's motives. Morgan explains that he has orders to resolve the situation without bloodshed. Furthermore, this is to be his last job, and he'd like to do it right.
	6. Riordan insists on buying Morgan and his friends a drink at his table. As they drink, he explains that he signed the contract with Liebenthal on behalf of the Fireclowns because Liebenthal told them he just wanted escorts for his trucks. He didn't sign on to stage a coup, or to lead his men to die fighting Adversaries and city militia, many of whom are the Fireclowns' friends, neighbors, or family. However, there's no need for Morgan to buy out the contract. The contract is in force only until the Fireclowns MC experiences a change in leadership
	7. Morgan quickly grasps the implications: if Riordan is incapacitated and can no longer act as president of the Fireclowns MC, their obligation to Liebenthal ends. He challenges Riordan.
12. Surgical Strike
	1. The Fireclowns gather around the parking lot of the Four Winds Bar, creating a circle around Morgan Stormrider and John Riordan.
	2. Riordan calls for his sword, a hand and a half job that is passed to the club's president from his predecessor.
	3. Morgan does not draw his sword, but instead puts on a pair of sap gloves.
	4. When Riordan questions him, Morgan reminds him of his orders: resolve the situation without bloodshed. He then invites Riordan to strike the first blow.
	5. Riordan reluctantly attacks, and Morgan disarms him before delivering a thorough beating that leaves Riordan with a dislocated shoulder and a couple of broken ribs.
	6. We cut to Tetsuo Munakata at this point, who arrived in time to see the end of the fight. Morgan and his friends leave before he can get to them, and the Fireclowns keep their rifles on him as he watches paramedics get him into a stretcher and into an ambulance.
	7. As the ambulance doors close, Riordan spots Munakata and gives him the finger. Another Fireclown tells him to go back to Liebenthal and tell him that he's going to need new mercenaries.
13. Unintended Consequences
	1. Cut to a short scene in which Liebenthal has just finished calling in some old favors.
	2. Morgan and Claire have had a late night going through decrypts of data taken from Liebenthal's AIs.
	3. Claire can't believe that Liebenthal depended solely on an obsolete whole-disk encryption scheme that had been cracked five years ago to protect his data.
	4. Morgan agrees. He thinks it's almost too easy, as if somebody on Liebenthal's side wanted him to get caught. Before he can voice his suspicion that Liebenthal is a patsy, he gets a call from Karen Del Rio.
	5. Del Rio rips Morgan a new asshole because Liebenthal reacted to the loss of the Fireclowns by mobilizing two regiments (make sure this is the right term) of city militia. By trying to simplify the situation, Morgan has made matters worse.
	6. Cut to Imaginos, standing behind Del Rio as he gives her a drug by the name of World Without End. He watches with contempt as she uses it immediately, and then demands more. While she's still under the influence, he tells her she must wait for the appropriate moment, and then give Morgan a change in orders: he is to kill Liebenthal and Munakata.
14. The Judicious Application of High Explosives
	1. Before Morgan can tell Edmund and Sid what has happened, all three get panicked messages from Claire telling them to get the hell out of the safe house.
	2. Edmund sees a militia squad outside through the window, and notes that they brought RPGs and heavy machine guns. 
	3. Claire's advice proves sensible indeed; as soon as Morgan and the others have gotten out, the squad opens up and blows the safe house off its foundation.
	4. Morgan decides to distract the squad to give Edmund and Sid a chance to escape. He storms through the burning house and throws himself at the squad while they're reloading their weapons.
	5. Morgan has the advantage, despite using his empty hands; he is alone, and if any militiaman uses his weapon, he risks cutting down his buddies along with Morgan.
	6. The leader of the squad waits until Morgan has beaten the others before drawing his pistol. He empties the seven-shot magazine into Morgan's chest, but his armor protects him from everything but the concussive force of the rounds, and he heals from that in seconds.
	7. Before the squad leader can reload, Morgan has his sword at the man's throat, and forces his surrender as Edmund and Sid return with several militia squads to arrest the squad whose asses Morgan just kicked.
	8. Morgan reflects that they must be doing something right, despite Del Rio's opinion, since Liebenthal now considers their involvement the sort of personal problem that can be solved with the judicious application of high explosives. Either Edmund or Sid (it doesn't matter which), look at the ruins of the safehouse and say, "You call that a judicious application?"
15. A Ronin's Pride
	1. Alexander Liebenthal listens as Tetsuo Munakata reports that the militia squad he sent to kill Morgan Stormrider and the others not only failed, but were all taken alive, arrested, and are in all probability giving evidence against Liebenthal.
	2. Abram Mellech, who also listened, offers to help Liebenthal escape Boston and find sanctuary, but Liebenthal insists on making his stand here, and names Mellech as part of the conspiracy that calls itself the Phoenix Society's executive council.
	3. Mellech acknowledges that he is on the XC, and insists that the Society is willing to protect him in exchange for the services he has performed on their behalf.
	4. Liebenthal, however, remains unmoved. He is determined to make his stand and expose the Society's corruption. He orders Munakata to escort Mellech out, at swordpoint if necessary.
	5. Mellech somehow darkens the room, and tells Liebenthal that he has not only rejected the executive council's help, but that of the Disciples of the Watch, and that since Imaginos has no further use for him, the Disciples have no reason to forgive his arrogance.
	6. Mellech disappears, leaving Liebenthal alone with Munakata.
	7. Liebenthal asks Munakata if he means to remain at his side, or if he will leave Liebenthal to face Morgan Stormrider alone.
	8. Munakata insists that he will remain with Liebenthal until the end. He too is sworn to expose the Phoenix Society, and that since they share a noble purpose, honor demands that he not abandon his comrade.
		1. Liebenthal nods his approval, and shows Munakata one of the electromagnetic mass drivers he held back from the last shipment. He means to turn the weapon on Morgan if all else fails.
16. The Violence Inherent in the System
	1. Morgan arrives at City Hall with Sid, Edmund, and Boston's two remaining Adversaries, Catherine Gatto and Sarah Kohlrynn. All but Morgan have taken command of a militia regiment in order to keep Liebenthal's militia pinned down. 
	2. Though Liebenthal's militia has not surrendered, they've stated that they will not fight against the numbers arrayed against them.
	3. Gatto remarks that it is a tragedy that matters came to this pass, with citizen pitted against citizen and the threat of civil war in the air.
	4. Morgan agrees with her, but reminds her that an Adversary bears the responsibility of defending liberty against the encroachment of law by both diplomacy and force of arms.
	5. Edmund asks Gatto if she knows what the Adversaries really are, and when Kohlrynn gives the answer provided by Phoenix Society propaganda, Edmund cuts them off and says, "We're the threat the Phoenix Society uses to impose order as defined by Society policy. We're the violence inherent in the system."
	6. Sid stares at Edmund, shocked by his words, but it's Morgan who speaks against him by saying, "I'm sick of being a necessary evil. If I am just the sword with which the Society strikes down its opposition, then let the Society be damned."
	7. Before Edmund can answer him, Morgan walks past him and says, "This is my resignation, effective the minute I've delivered Liebenthal into custody. I'm done."
	8. Morgan then parts the soldiers before him and walks into City Hall through the front door.
17. The Necessity of Enmity
	1. Tetsuo Munakata is waiting for Morgan in the lobby of City Hall as Morgan walks in. He has his katana ready to draw, but has not drawn the blade yet. He notes that Morgan is not wearing his pins, and is therefore out of uniform.
	2. Morgan stops in the center of the lobby, and tells Munakata that he just resigned his post, effective the second Liebenthal is in Phoenix Society custody.
	3. Munakata asks Morgan if he resigned for the same reason Munakata did: the realization that every word of the Phoenix Society's propaganda is a damned lie.
	4. Morgan admits that this is the case, but that he is determined to finish his mission and depose Alexander Liebenthal.
	5. Munakata suggests that Morgan should abandon the mission, and instead work with him and Liebenthal to expose the Phoenix Society's corruption and tyranny. However, Morgan rejects the notion out of hand, saying, "Even if we shared common cause, your methods are repugnant to me. If I expose the Phoenix Society, it will be without violating the rights of others." He then tells Munakata, "I hear you've been practicing, but I am not here to duel with you. I came for Liebenthal. My orders said nothing about you, so sheathe your blade and stand aside."
	6. Munakata refuses, saying, "You are not the only one bound by his pride. Defend yourself."
	7. Munakata rushes past Morgan while drawing his sword, and uses his momentum to try to behead Morgan from behind as he did one of the Adversaries sent to stop Liebenthal a few days ago. However, Morgan has a counter: he's wearing his sword on his back, and draws the blade partway to block Munakata's weapon.
	8. Munakata draws back as Morgan turns to face him and asks, "Who are you trying to impress?"
		1. They face each other, katana against Renaissance-style bastard sword, and duel in earnest. However, while Munakata is trying to spill Morgan's blood, Morgan wields his sword one-handed, using it to defend against the katana while striking with his fist. He eventually disarms Munakata, breaking the dishonored Adversary's blade beneath his boot while holding the tip of his sword to his throat.
	9. As soon as Munakata yields, Morgan turns his back on him and heads upstairs, to Liebenthal's office.
18. Non Serviam
	1. Morgan finds Liebenthal sitting alone in his office. There's a weapon on the desk in front of him that resembles a M4 carbine, but appears to lack moving parts.
	2. Before Morgan can speak and begin the process of arresting Liebenthal, his wall-screen flares to show Karen Del Rio.
	3. Del Rio announces a change in Morgan's orders: he is to execute Alexander Liebenthal and return with proof of the kill.
	4. Morgan refuses the order. It has not come through proper channels or in the proper form, but even that is irrelevant in light of his resignation. When Karen insists, Morgan says, "Non serviam. My service ends here."
	5. Liebenthal uses his weapon to destroy the screen, and asks him how he could bear to take orders from that 'hatchet-faced bitch'. Morgan ignores the question, and announces the charges against Liebenthal.
	6. Before Morgan can notify Liebenthal of his rights, Liebenthal opens up with the weapon in an attempt to shred Morgan where he stands. Extrapolating the probable trajectory of the rounds based on the position of the weapon's muzzle, Morgan gets out the way quickly enough that Liebenthal wonders if he can dodge bullets.
	7. While Liebenthal tries to aim the weapon while firing it, Morgan gets behind him and wrests it from his hands before ramming the stock into his belly. Morgan then finishes the process of arresting Liebenthal, and hands him over to Edmund Cohen, along with the pins that marked him as an Adversary.
19. The Useful Lifetime of One's Tools
	1. Desdinova meets Imaginos at his office in the penthouse of the AsgarTech Building in Asgard on Ross Island, Antarctica.
	2. After the social pleasantries are concluded, Desdinova asks Imaginos about Karen Del Rio's actions in the Liebenthal case.
	3. Imaginos shrugs, and suggests that since Del Rio can no longer exert authority over Morgan, it may soon be time to cut her loose. 
	4. Imaginos says something about the importance of understanding the useful lifetime of one's tools, and Desdinova asks him if that means he'll be getting rid of Liebenthal before he says something that Sathariel's propaganda machine can't dismiss as the ranting of a deposed dictator.
	5. Imaginos says neither yes nor no, but instead counsels his brother to wait, be patient, and let the machinery of government do its work. Liebenthal will get his due, in due course.
20. Once an Adversary
	1. En route to Tokyo via a maglev that stops in Sydney, Morgan is working with his AI, Astarte, to figure out the truth behind the electromagnetic mass driver (EMD) he took from Alexander Liebenthal in Boston.
		1. He starts by querying Masamune, Nakajima's business AI. Masamune refuses to answer his questions. 
		2. Morgan then speaks to Malkuth, but Malkuth isn't willing to intercede with Masamune on Morgan's behalf, and warns him against involving Claire.
	2. Before Morgan can continue his search, the maglev stops in Sydney, and Edmund Cohen boards.
		1. Edmund's been chasing Morgan across the world via suborbital flight, and finally caught up with him.
		2. He gives Morgan back his pins, saying that Morgan served with distinction, and thus earned the right to keep them.
		3. He then tells Morgan that he's still acting like an Adversary, even if he did renounce the position. "Once an Adversary, always an Adversary."
	3. Morgan accepts the pins, and thanks Edmund for understanding. He then asks him if there was something else Edmund wanted.
		1. Edmund shows him a prescription for a drug called Alcemetic, which makes the user violently sick if he ingests any sort of alcoholic beverage.
		2. He explains that he quit drinking a week ago, when Morgan came to London to rescue Naomi, because it was his fault Naomi was taken into custody.
		3. Morgan demands an explanation, and Edmund tells him about his night with Elisabeth Bathory. He then points at the EMD sitting on the seat with Morgan and tells him directly that Chihiro Nakajima didn't design that weapon. He isn't willing to say more than that, but instead insists that he owes somebody called 'Desdinova' too much to say more.
21. A Rock and a Hard Place
	1. Upon arriving in Tokyo, Morgan immediately makes his way to Nakajima Armaments, where Chihiro Nakajima awaits him.
	2. Rather than accuse her directly, as an Adversary might, Morgan apologizes for barging in without an appointment, and asks Nakajima's forgiveness because of the delicate business with which she entrusted him.
	3. Once Nakajima has him in her private office, Morgan unwraps the EMD and asks her for the truth. He first tells her that he knows that a woman named Elisabeth Bathory has been tampering with people close to him, and asks Nakajima if Bathory somehow got to her as well.
	4. Nakajima doesn't recognize Elisabeth Bathory, but tells a different story:
		1. Isaac Magnin of the AsgarTech Corporation asked her to implement a prototype EMD based on his design.
		2. She fabricated the prototype, and was horrified by its destructive power.
		3. She refused Magnin's offer of a license to mass-produce the weapons in exchange for Magnin relinquishing his 25% share in her company.
	5. Morgan, shocked that Nakajima would rather give up a chance to reclaim a quarter of her company than mass produce weapons that would make wholesale slaughter easier than ever before, offers a heartfelt apology. He had come prepared to accuse her of being involved in the Liebenthal affair as a supplier of the weapons Liebenthal trafficked up and down the Atlantic coast of North America.
	6. Nakajima accepts Morgan's apology, and offers her own. She had been afraid to accuse a man who owned so much of her company, and had hoped that Morgan might find the truth on his own.
	7. Morgan smiles, and says that he had -- but wanted to hear it from Nakajima's own lips. He says that the Phoenix Society's executive council, with the sole exception of Edmund Cohen, appears to have been behind Liebenthal and several others. He knows this, but cannot prove it in a court of law yet.
22. The Dead and Their Secrets
	1. Two weeks later, Morgan's with Naomi again. They've just attended Christabel Crowley's funeral in Dover, which was a closed-casket affair because of the manner of her death.
	2. Naomi asks him if he has to go back to New York right away, and Morgan explains that while he had to testify at Liebenthal's trial, he's already been convicted and sentenced to life imprisonment.
	3. She leads him to her restored Aston-Martin coupe, a red Volante, which had been retrofitted with an electrical engine and an onboard computer that includes autopilot functionality, and asks him if he wants to drive.
	4. Morgan declines. A question had just occurred to him: did Alexander Liebenthal know Isaac Magnin? He tells Naomi that he wants to follow this up by arranging a visit with Liebenthal in prison, and says also that he loves to watch her drive.
	5. As Naomi drives through the English countryside, Morgan gets in touch with the warden of the prison to which Liebenthal was sentenced. According to the warden, Liebenthal already put in a request for euthanasia. The request was granted, and carried out a few hours ago. 
	6. When Morgan asks for the name of the person who countersigned the euthanasia request, the warden is surprised by his question, but tells him that the approval came straight from the executive council, since Isaac Magnin countersigned it.
	7. Naomi, in the meantime, hears Morgan speak the name Isaac Magnin and wonders for a moment if she should tell Morgan that she had seen Christabel with Magnin after Morgan had broken up with her last year. She decides that the dead and their secrets will keep equally well, and tells Morgan that she knows an excellent bed and breakfast up ahead where they can stop for the night, if he wants.
	8. To Naomi's delight, Morgan engages the autopilot and kisses her before offering to make reservations for a week. He explains himself by reminding Naomi that in his ten years as an Adversary and a member of Crowley’s Thoth, he has never had so much as a day to himself, so he’s damn well going to take a week off.