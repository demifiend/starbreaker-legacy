The safe house reminded Morgan of his own brownstone on West 96th street in the Upper West Side of Manhattan. It was situated in a similar neighborhood, Exeter Street in Boston's Back Bay. The exterior must have gotten a fresh coat of red paint before the onset of winter, and the holly bushes fronting the building remained full of berries. He sniffed a hint of woodsmoke from the air; a thin white plume streamed skyward from the chimney as he led Naomi up the stoop. He held the door for her while scanning the street; all was quiet. "I'm surprised the Society still holds on to this property as a safe house; Claire tells me Alexander Liebenthal lives a few doors down."

"That might be why. Would Liebenthal dare start a fight so close to home?" Her lips brushed Morgan's as she stepped inside. "I think you're still paranoid from dealing with that Bathory woman."

"Bathory has nothing to do with it." Morgan had almost closed the door when a calico cat pulled herself out from under the holly bushes and bounded up the stoop to slip inside. She rubbed against his shins with a "mrrr?" and her tail held straight up before slipping into the house behind him. Once the front door was locked and deadbolted, he took Naomi's coat. "I'm always like this on the job."

Naomi turned to Sid, who had come to the hallway to greet them with a half-eaten plate of steak and scrambled eggs in hand. "Is he really?"

"What? Paranoid?" Sid scooped up a forkful of scrambled eggs and shoveled it into his mouth. "Nah. I've dealt with paranoid people. Morgan makes 'em look reasonable by comparison."

Morgan clapped Sid's shoulder. "Thanks. Have you had a look around the house?"

"Yeah. The Fireclowns shouldn't be able to give us any trouble in here unless they bring artillery. All of the doors and windows have steel shutters on the inside. One word to the AI and they all come down, right, Malkuth?"

"Exactly." Morgan had been on the job too long to be surprised that one of the Sephiroth controlled the building's systems. A local AI's hardware could be compromised, and the building's security logic had a failsafe; if the remote AI was somehow prevented from interfacing, the entire building would automatically lock down. "I had a week's worth of fresh food delivered, as well as clean clothes; I assumed you would be traveling light. Adversary Bradleigh, I apologize in advance if what I selected for you doesn't fit perfectly; I used your measurements on file from fifteen years ago."

Naomi shrugged. "They should be fine. If not, I can steal some of Morgan's. Is it just the three of us and Edmund?"

"No. Sarah Kohlrynn is here as well, from the Boston Chapter."

"Is she part of the mission?"

"No, Adversary Stormrider, but I'm sticking around anyway." A slender strawberry blonde in jeans and a "Hot Beef Injection" concert t-shirt bore a plate of scrambled eggs and toast from the kitchen. Her feet remained bare, and Morgan suspected she was also braless. "Deschat and Gatto are here as well, but don't bother 'em. Deschat's running interference with the New York chapter and handling any bureaucratic bullshit coming out way. She said she'd keep that Del Rio bitch off our asses. Gatto's working with local law enforcement to keep everything from falling apart. They're still on the job because Liebenthal's covering the city payroll. Once you take him down, Boston won't have a functioning government. The Phoenix Society will need an Adversary on the scene to supervise the elections. I've handled such situations before, and I can hold my own in a fight if need be. Besides, I'm sure I could help at least one of you keep your morale up."

"Take that metaphorically." Eddie made to swat Sarah's ass as he rose from the basement. Sweat slicked his grey hair and a day's worth of stubble. "How many Fireclowns did you take out by yourself last night, Sarah? Half a dozen in thirty seconds?"

"I took you out faster, old man. There's still some scrambled eggs in the pan. Go get something to eat." Sarah turned to Morgan. "Aren't you the guitarist from Crowley's Thoth? Don't tell me you're also a neurosurgeon."

"I had considered taking up the trade after I retire." If Sarah was to be another Claire, making obscure allusions to ancient movies, Morgan suspected the most sensible way to interact with her would be to play along. He pulled up Sarah's service record, noting that while her service record was perfectly respectable, she had scored a nine on the Milgram Battery; the score suggested almost abject submissiveness on her part, and was outside the acceptable range for Adversary candidates. *Then again, I scored a big fat zero, and they still let me take the oath.*

Naomi requested a secure talk session. "Sarah just sent me a rather lewd request to borrow you for half an hour."

"At least she asked first. I'm tempted to introduce her to Claire. They'd get along famously."

"Dear God, are you mad? They're as likely to spur each other on as they are to wear each other out." She terminated the session as Morgan offered his hand to Sarah. Professional conduct, he reasoned, might gently remind her to focus on business. "I just pulled your file, Adversary Kohlrynn. I'm sure you'll be an asset to the mission. Is there enough food ready for Naomi and me to get a plate?"

"Not really. I found some roast beef and a wedge of Eire cheddar, though. A sandwich sounds about right, don't you think?" Naomi called from the kitchen, elbowing shut the refrigerator door.

Though it was mid-morning in Boston, it felt like lunch was long-overdue to Morgan. No doubt Naomi's stomach had sent her a similar message. "Do you want me to make my own?"

"You made breakfast and saw to the dishes before we left. I'll sort out our lunch. Talk to Eddie and ask about that Bathory woman."

Eddie's face fell as soon as Naomi mentioned Elisabeth Bathory, and Morgan forgot every harsh word he meant to inflict upon the man; Cohen knew damned well that he had betrayed his friends, and condemned himself for it in far crueler terms than Morgan could manage. He shot a glance at Sarah. "Find Sid, and go do something which will give Eddie, Naomi, and me privacy. Keep in mind that Sid loves his wife."

Sarah nodded. "Sid's working out downstairs. I'll be in the study."

Once the three of them were alone together, Eddie spoke, sounding as broken as he looked. "I know sorry doesn't count for shit, but I *am* sorry. I always wanted Elisabeth Bathory, but I knew better, and I was always sober around her."

"Claire told me everything." Naomi laid a hand on Eddie's shoulder. "She caught you when you were drunk, and plied you with more wine, and hashish, and then lured you into bed. She knew all of your weaknesses, and exploited them."

"Shit, Nims, don't tell me you've forgiven me."

"I have. Morgan, I think you should let go of your own anger over what happened to me at MEPOL. Remember what Bathory said: we're being tested. Not just you, but us, and the bonds connecting us. I suspect that if we let those bonds fray, we'll have failed."

*My anger?* Morgan glanced at the full-length mirror hanging on the wall beside him. His fists clenched at his sides. He forced them open, and saw that the palms of his hands bled from being pierced by his claws. The wounds healed before he could get to the sink to wash them, but the blood needed still remained to be washed away. *She's right. I'm furious, but not with Edmund. How dare Bathory do all of this?* He turned back with clean hands, monitoring himself with greater care as he led Naomi to a sofa and sat down with her. "I'll save my wrath for a more deserving target. Eddie, you were just being used."

Eddie flopped into an armchair, and the cat that had come inside with Morgan rose from her place before the fire merrily crackling in the hearth, stretched, and curled up in his lap. "I let myself be used."

"Put that aside for now. Bathory mentioned somebody called 'Imaginos', and said he probably killed Christabel. She went on to suggest that this 'Imaginos' character had a hand in Liebenthal's coup." Eddie paled as Morgan mentioned Imaginos, his posture stiffening. His sudden tension disturbed the cat, who left his lap for a safer place to nap. "You're both executive council. Do you know anything?"

"I know I picked the wrong fucking time to swear off booze." Eddie began to pace, casting frightened glances out the windows. "What does the name mean to the two of you?"

"I used to have a restored white Jaguar with a number plate bearing that name. I bought it at a charity auction; Isaac Magnin had put it up for sale. Christabel had come along, but you were on a mission and had Astarte donate on your behalf." Naomi patted Morgan's hand as if to say it was his turn. "The only 'Imaginos' I ever heard of was a 1988 concept album by the Blue Öyster Cult which dropped out of print as soon as it dropped off the charts. I have a copy. Supposedly, he was also called Desdinova." He stopped for a moment; he knew damned well that he had heard the name before. "Could Imaginos actually be Michael Desdinova of the Ohrmazd Medical Group?"

Eddie had paled further at the mention of Desdinova's name, and ceased his pacing. "Why are you asking these questions, Morgan? I'm not sure how this pertains to the mission at hand."

Morgan shared a glance with Naomi before answering the question. "I think there's more to this mission than I've been told. Elisabeth Bathory claims that I'm being tested. I checked the timelines on the way to Boston after she left us, and Christabel's estimated time of death corresponds with the time Liebenthal staged his coup when you factor in the time difference. They both happened within an hour of one another if you use Universal Coordinated Time, as did your seduction at Bathory's hands so she could upload that Witness Protocol video you recorded. MEPOL got their hands on it, took in Naomi ostensibly to get her statement, and mistreated her in a manner which obligated me as her friend to intervene, which kept me from going directly to Boston to stop Liebenthal. I think somebody wanted to give Liebenthal a little more time, and had the means to go to outrageous lengths to do so."

"Do you have any evidence to support your reasoning?" Eddie returned to his armchair, scratching his head with such speed that he resembled a dog desperate to dislodge a flea. "Aside from the timing of events, which I admit is suggestive."

Naomi shook her head. "The timeline is all we have. I'm not sure how we might get more evidence."

"I know somebody."

"No. Hell no. Shit no. *Fuck no.*" Eddie shot to his feet, his hands making wild gestures of denial. "Morgan, as a member of the Phoenix Society's executive council I absofuckinlutely *forbid* you to drag Claire into this."

"I helped you and Sid last night, Edmund Cohen, and this is how you treat me?" Claire appeared onscreen, her auburn curls restrained by a hairband with black plush cat's ears. She wore a faded navy blue Worshyp t-shirt; Morgan wondered if she had purchased it when Crowley's Thoth did a few dates with The Worshyp in Toronto before releasing *Prometheus Unbound*. She would have been a teenager at the time, had that been the case. She stretched, straining her jaws with her yawn. "You ingrate. Didn't I keep the Fireclowns out of the building so you could take them all out without getting hurt?"

"This is different. It's bad enough Morgan and Naomi are involved in this, but they're trained Adversaries. If something happens to you --"

"Cram it, Lancelot." Claire looked directly at Morgan. "If something happens to me, Port Royal will hear about it. In fact, I'll be coming to Boston to explain to them why this situation is their problem, since it *will* affect the crew."

Morgan nodded. Port Royal was the world's most successful distributed republic, and they looked after their own. "That may be why Eddie would rather you not get further involved. However, you've been involved in this mess from the beginning. You helped me get Naomi away from MEPOL headquarters. What I have in mind should not place you in harm's way." He turned to Eddie. "I need more information. I could get it myself, but Claire can get it faster and leave me free to do other things. That has been my rationale for bringing her on board for years. Why do you object now?"

Eddie held the tips of his first two fingers to his ear to indicate that he was using secure talk. Within a minute, Sid ascended from the basement and found an armchair for himself. He greeted Morgan and Naomi with a gesture halfway between a wave and a salute. "You guys missed some fun last night. I had Munakata in a half nelson with a knife at his throat, and I was going to bring him in." He glared at Eddie. "Now that we're all here, let's hear about Elisabeth Bathory."

"I can't tell you anything yet."

Morgan's hands clenched into fists before he could stop himself. He rose, pulling away from Naomi to glare at Edmund from the center of the room. "Eddie, Saul Rosenbaum told me everything he was authorized to tell. However, this situation is far more complex, and you know things which could help us make sense of the situation. This isn't just a pathetic attempt at a coup d'etat. The efforts made to delay my coming to Boston suggest as much."

Eddie shook his head. "I know it's unfair to all of you, but I can't divulge further information about this situation. It touches upon people and events I'm under standing orders to conceal."

"Morgan, didn't you say Eddie was part of the executive council?" Naomi turned a bemused expression upon Eddie, whose lap had been reclaimed by the cat. "Who could give you orders."

"Aside from the rest of the executive council?" Eddie gave the calico a grudging scratch behind the ears; the man disliked cats, which was why he had given Morgan a kitten he had found on his doorstep ten years ago. *Considering Mordred's size and intelligence, no doubt Eddie feels he dodged a bullet. I need to check with Astarte and see how the cat's doing.*

Sid laughed as the cat vacated Eddie's lap and curled up at his feet. "Are you really XC, man? Or do you just say you are to impress women?"

"I'm more like the junior partner." Eddie's shoulders slumped at the admission. "I saw some crazy shit during Nationfall, while working for one of the men who founded the Phoenix Society. The position and its attendant pay and privileges are an ongoing bribe. In exchange, I keep quiet and do what the man tells me."

"Are you telling us everything you can?" Morgan doubted it, and suspected that he might be facing another test with an unseen observer.

Eddie nodded. "I can say one thing more. I don't work for the individual Elisabeth Bathory identified as 'Imaginos'."

Morgan suspected that his expression amused Naomi; she stared at him and began giggling behind a hand covering her mouth. She stifled her mirth and turned to Eddie. "I don't think that was *quite* what Morgan meant."

"Maybe not, but I wouldn't mind hearing a bit more." Claire gave Eddie a meaningful look, but when no further revelations proved forthcoming, she shrugged. "Be that way, grandpa. So, Morgan, what do you need me to do?"

"Gather every available megabyte of data generated by or pertaining to Alexander Liebenthal. Send everything to Malkuth as evidence, but make sure Astarte gets a copy as well. In the meantime, we carry out our mission as originally instructed."

The other Adversaries nodded, and Sid cracked his knuckles. "Sorry I wasn't able to take out Munakata, but I noticed something strange. He didn't care when he thought I would just cut his throat, but he got real quiet after I threatened to cut off his head."

"He got over my putting a bullet through his skull. Maybe there's a limit to whatever regenerative technology he possesses."

"Maybe. Or maybe you guys are both immortals, vying for supremacy." Claire pretended to hold a sword two-handed. "There can be only one!"

Morgan rolled his eyes at the allusion before sending Sarah a text asking her to join the others. "There's somebody I'd like you to meet, Claire. I think you'd like her. She hit me with a Buckaroo Banzai reference the second we were introduced."

Sarah stared at the screen. "Hey, when did you see The Worshyp?”

Claire shrugged. "Oh, back when Crowley's Thoth did a couple of shows with 'em before releasing Prometheus Unbound. They were at a festival in Toronto with Keep Firing Assholes, Sweater Kittens, Doomed Space Marines, and The Ten Who Were Taken. I went back to London with them figuring at least one of them would proposition me, but they were total gentlemen. Too bad; the singer was really cute despite being older than Eddie.”

"Wasn't that ten years ago?"

Morgan felt Naomi's lips brush his ear. "That went well." She stood, commanding the room's attention. "Has anybody given any thought to our next steps? Sid and Eddie, you were able to rescue the remnants of the Boston chapter, but we still have to take down Liebenthal."

Morgan rose, and caught Naomi's arm. "Sid, if you had succeeded in arresting Munakata, you would have deprived Liebenthal of his right-hand man. We can do the next best thing, and deprive Liebenthal of the Fireclowns."

Naomi blanched, her eyes widening as she stared at Morgan. "You're going to kill five hundred men?"

"Who said anything about killing them?" Morgan paused a moment, and remembered his reputation. Killing anybody who drew a sword on him, a fellow Adversary, or an innocent was the means by which he made his name synonymous with mortal terror in the minds of all who held authority. Saul, Iris, and Karen counted upon his reputation to defeat Liebenthal before they faced one another. "All of you, listen to me. Alexander Liebenthal has publicly accused the Phoenix Society of using me to silence their critics. If I cannot take him down without fatalities, the Society will throw me under a maglev to salvage its own reputation. If I'm lucky, they'll drag me in front of a kangaroo court before putting a couple of rounds in the back of my head."

Eddie nodded. "What have you got in mind?"

Claire turned away from Sarah a moment. "The Fireclowns use a dive called the Four Winds Bar as their headquarters. I can send you the location."

"Thanks." Morgan turned to Naomi and Sid. "We can't take Eddie with us, since he swore off booze and it would be unfair to tempt him, but the three of us can go have a little chat with the president of the Fireclowns Motorcycle Club. I suspect he no more keen on attending their funerals than I am on arranging them."

\###

