Michael Riordan knew things would get weird as soon as Liebenthal called him into his office and explained that he'd be staging a coup d'etat, and needed the Fireclowns MC to act as his provisional army. Riordan had already bound the club's membership to Liebenthal's service, but their job had been to ride with Liebenthal's trucks as escort in the event that bikers from a rival gang dared appear to threaten a driver or his cargo. It was a profitable and delicate racket which required cooperation and careful coordination between disparate motorcycle clubs. The leadership of outfits like the Sun Jesters or the Transmaniacon MC had to be persuaded to refrain from the more enterprising approach of staging independent highway robberies in exchange for a safe, steady profit obtained by making a show of force and accepting a cut of the fee earned by more 'respectable' motorcycle clubs like the Fireclowns or the venerable Hells Angels in exchange for providing 'protection'. The latter had cleaned up their act during Nationfall by abandoning their ties to the drug trade and other organized crime in favor of the security business. *They'd corner the fucking market if everybody had forgotten about Altamont.*

Despite his rivalry with the more established gang of mercenary bikers, Riordan regretted not letting the Angels have the Liebenthal contract, especially after that fucking wannabe ronin took a sword to three Adversaries. Worse still, Liebenthal had taken a fucking podium in front of City Hall and accused the Phoenix Society of fascism. He had cornered the client in his office afterword. "Are you trying to get my men killed? We're not trained to take on Adversaries, and you and your bodyguard did your absolute *best* to bring Morgan fucking Stormrider down on us! If we face him, it's going to be a goddamned *massacre*!"

*Here comes the massacre now.* The thought sprang to the forefront of Riordan's mind as the front doors to The Four Winds Bar opened to admit Morgan Stormrider and his companions. He immediately broadcasted a general order over the secure relay chat the Fireclowns kept open as a matter of habit; it was easier than shouting over engine and road noise, or even bar chatter. "Everybody keep cool. They're not armed."

The three split. While Stormrider angled for a seat at the bar like anybody in need of a cold beer after a ride, the big African guy seemed to make a point of looking for people who had been at the Phoenix Society's Boston chapter the night before, or their relations. "What does the big guy want?"

Though secure relay chat couldn't convey emotional context, Riordan suspected that the replies came from confused men. "He's asking after us, as if we didn't try to storm a building last night and kidnap his comrades. He says Stormrider is trying to deal with the client without killing anybody."

*Morgan Stormrider's doing a no fatality job? Not likely.* The notion contradicted everything Riordan knew about the man, which the same most people probably knew: if you killed other Adversaries, the Phoenix Society stopped caring about whether or not you lived to stand trial, and sent an Adversary who had proven himself willing and able to cut down anybody who drew a weapon against him. *But the man came unarmed to my bar, which is full of my people. We could stomp him and his friends flat, even though they might hurt some of us.* Before Riordan could ask the Fireclowns who had gathered around him to scatter and give Stormrider room at the bar, somebody started playing the upright piano that the barkeep's son pounded on weekend nights so that the Fireclowns could have a bit of dancing. He rapped his knuckles on the bar, motioning the bartender over. For some reason, the man resembled Stormrider to some extent. *Jones just has CPMD. Lots of people do.* "Did your son come early?"

"No way, Mike. That's Naomi Bradleigh. You know, the snow-haired lady who came in with Stormrider?"

Riordan strained his ears to identify the music. "She plays jazz?"

"Yeah. Sounds like that guy my son's instructor told him to check out. I think the name was Monk. Too bad he's not here to see this." Jones left Riordan to get a Fireclown's order as Stormrider settled into the stool beside his. He laid a battered leather saddlebag upon the bar, and began counting out bullion. He formed two stacks of coins stamped "1g Au, 99.999% purity verified by the Metallurgical Society of New York", and pushed one stack of ten coins towards the bartender. "Good evening, Mr. Jones. Kindly see to it that everybody here eats and drinks their fill with my compliments." Stormrider pushed the other stack towards the man, whose sudden gape threatened to dislocate his jaw. "And here is your tip, sir."

Jones managed to get his jaw unlocked before Riordan could get his head around the notion that this Adversary offered to pay for the food and drink of every man in the bar with a month's worth of profits, and gave the same to the bartender as a tip. "What will *you* be having, Adversary?"

Stormrider laid a smaller coin on the bar. Though it claimed the same purity, it was half the size, and bore the face of Thomas Jefferson in profile instead of the Statue of Liberty. "Your best single malt, on the rocks. Pour one for the gentleman beside me as well, and keep the change."

*Like he needs it.* Riordan kept replaying the exchange, unable to believe that Stormrider possessed such wealth, and was willing to simply lay it on the bar. The Adversary was utterly nonchalant as he sipped his Scotch, and Riordan felt he had no choice but to do the same. He could not taste it, but he retained sufficient wit to realize his inability to appreciate the glass of Scotch before him was a tragic waste. Words flowed past him, slipping through his grasp. "Sorry. I didn't hear you."

Stormrider nodded. "I asked, 'How's your Scotch?'."

"I'm a little too shocked to taste it."

"I have your attention, do I not?"

Riordan produced a cigarette case from an inner pocket and selected a joint. "You've had that since you and your friends walked in. I've spent the last quarter hour wanting to know what brings you here, unarmed." He tried to light it, only to find his lighter nonfunctional.

"I assume you are aware of my mission in Boston." Stormrider held a stainless steel lighter of his own, and flicked it alight for Riordan. One side appeared to have "Fuck Authority!" engraved upon it.

"Thanks." Riordan exhaled, letting the smoke calm him as the Adversary snicked his lighter shut and slipped it into his pocket. "You're here to take down my client."

Stormrider watched the ice in his drink melt for a bit. "Your client is an arms trafficker, has staged a coup d'etat, and is responsible for the deaths of three Adversaries. Under normal circumstances, I would simply locate him, storm the premises, and kill every Fireclown who drew a weapon on Liebenthal's behalf. Once I got to him, I would make a good-faith effort to bring him back alive. However, my instructions for this mission are explicit. Because of the charges Liebenthal laid at the Society's feet, I am to carry out my mission without bloodshed."

Riordan nodded, and tapped ashes from his joint into the ashtray before him. *If Stormrider's serious about taking down Liebenthal without killing anybody, don't I owe it to my men to cooperate with the man? If we turn our backs on Liebenthal, though, will anybody else be willing to hire us? Honest mercenaries stay bought, damn it. "It sounds to me like you want us to abandon our contract with Liebenthal."

"Is that objectionable?"

"It would make our lives harder in the long run. Who would hire us? What's to stop Liebenthal from hiring the Boston Hells Angels and sending them after us?"

Morgan smiled as he withdrew three ingots of gold from the saddlebag. The stamp burned itself into Riordan's mind as the metal flashed under the lights, and read "1kg Au, 99.999% purity verified by the Metallurgical Society of New York". Riordan tried to crunch the numbers in his head. Three kilograms of gold was three million milligrams; a milligram of gold could buy a man a cup of coffee and a gram was a respectable week's wage for a middle-class worker. Three thousands grams was fuck you money by any reasonable standard. "This is a year's pay for you and your men. I am prepared to give you this money up front, in exchange for your service."

"You want the Fireclowns to work for you, for a year? What would we do? You don't need protection."

"Neither does anybody else, at least not for the next year. I want you to stay out of my way for now, and do nothing which would embarrass me. The city's other motorcycle clubs are my concern, not yours."

Stormrider's requirements were far from onerous, but the thought of betraying Liebenthal stuck in Riordan's throat. *Never mind that Liebenthal threw himself into a meat grinder and expects us to jump in after him. A contract's a contract.* "It's a tempting offer, Adversary, but --"

"You dislike the notion of betraying your client. Your arrangement depends on his being able to pay you on time, does it not?"

"Yeah. If he stiffs us, we're out."

Stormrider nodded, and rapped on the bar to get Jones' attention. He laid down a twenty-milligram piece. "Another Scotch on the rocks, please. Do you have a tablet I might borrow?"

Jones placed the tablet next to Stormrider's drink. Rather than taste the Scotch, he turned his attention to the tablet and pulled up what appeared to be the 2112 budget for the City of Boston: five hundred kilograms to be spent on the police, fire department, roads, parks, schools, and every other service a city provided its citizens. "The Phoenix Society normally refrains from imposing unfunded mandates on city governments. The city of Boston is expected to provide certain services, and the Society defrays the expense so that the city need not trample individual rights by levying taxes."

"The Society cut off the money when Liebenthal took over."

Morgan nodded. "Cutting off funding is standard procedure in situations like this. The sudden cessation of services normally makes it impossible for people like Liebenthal to obtain popular support. He's held off the stoppage by funding the government himself, but he cannot do so much longer. He will be broke tomorrow. If you continue to work for him, it will be as his tax collectors."

Riordan pushed the three kilogram ingots back towards Stormrider. "I need to speak to Liebenthal before I make a decision. I have to hear from the man himself that he won't be able to pay us."

Stormrider nodded as he returned the gold to his saddlebag. "That would be best. I'd recommend bringing armed men with you, lest Munakata take exception to your departure."
