It had been years since Naomi last rode a motorcycle, but the rented   Triumph responded to her commands with the effortless ease of her own legs. The motor purred beneath her, powered by a miniaturized thorium-powered nuclear reactor which comprised half of the bike's mass due to shielding requirements. She opened the throttle, pulling alongside Morgan, who sat his chopper with a proud ease worthy of a paladin in the medieval romances they had read while recording the Crowley's Thoth album *Le Morte d'Arthur*. His hair remained unbound beneath his helmet; rather than wear head protection tailored for bikers, he settled for an armored helmet issued to Adversaries and militia, and protected his eyes with sunglasses. Sid had done the same. Naomi had paid extra to rent a proper helmet with her cycle, and regretted it; having the visor down left her head completely enclosed, and evoked a sense of claustrophobia. She opened a secure talk session with Morgan. {You were right about the helmet. I feel like I can't breathe in this thing, or hear properly.}

{Do you want to stop and switch? My gear should fit you.}

Naomi used her implant to pull up the map; only a kilometer and a half remained before they reached their destination. {I can put up with it a bit longer. It was knightly of you to offer.}

{Knightly?}

{Sorry; it's the way you look while riding.} She slowed to keep pace with Morgan, and because a message from trafficnet to her implant advised her that the maximum safe speed was now a quarter of what it had been due to heavy truck traffic. They had entered South Boston, and had begun passing warehouses and small factories from which trucks bearing goods bound for delivery to buyers in the city or transport by maglev to buyers outside Boston lumbered forth, occasionally forcing Naomi and the others to stop and wait. Their pace was so reduced, she felt a temptation to tuck her helmet into a saddlebag. *Morgan might get away with that; Claire thinks he's an irreligious Rasputin. I'm only human.*

The Four Winds Bar lay just past the warehouses and factories, in a district which seemed to have been allocated to bars, nightclubs, strip joints, and brothels. Motorcycles crowded the parking lots in front of several establishments, despite the plenitude of bus stops for the use of revelers too drunk to pilot a vehicle. Most of the stops bore public service posters displaying men and women passed out from drunkenness and slogans like "Too drunk to drive = Too drunk to fuck" and "Only losers take advantage". Others bore recruitment posters for the Adversaries, some of which had been defaced by some angry hand so that they read, "We have control. We keep you safe. We are your hope." She used her implant to photograph one such poster while passing. *I bet Claire would like this. She's a Protomen fan.*

{How do we know we'll find the Fireclowns here?} She followed Morgan inside after they parked their motorcycles and dismounted. {Claire's information is usually accurate, right?}

Morgan nodded. {If she's wrong, it's because the situation changed too fast for her to keep up. That's rare.}

Sid kept pace behind Morgan with solid strides, but did not join the secure relay chat. He was connected, but had nothing to say. *He's done this before, and I almost always worked alone during my tenure.* {I usually work alone, so do you just need me to watch your back?}

{Watch Sid's as well, and we'll both look out for you. Otherwise, I trust your judgment. We want the Fireclowns to respect us, and we want them to understand that we don't have a beef with them.}

{All right.} She followed the men inside, and smiled as she took in the interior. Despite what Claire had said, the Four Winds Bar was no dive. Every booth and table had plush leather seats or chairs, and the woodwork was unscarred, and gleamed with fresh polish. Screens displayed various sporting events currently in progress, among them a fencing tourney, a soccer match, a Formula One race, and a baseball game. While almost all of the patrons wore the colors of the Fireclowns MC, most of them seemed too absorbed in their own conversations to notice her. "--I told my wife I didn't like this job any more than she did, but did she listen? Nah--" 

"--Can you believe this shit? It's 2112 and my grandparents still think my being bi is just a phase. --" 

"Hey, isn't that the babe from Crowley's Thoth?"

She stopped and turned towards the booth containing the patron who had recognized her. He seemed terribly young to be riding with a gang of mercenary bikers, and his reddish brown hair kept falling over his face. His companions were equally young. *I guess it's a family business for some of them.* "I didn't think anybody would recognize me in uniform. I'm actually helping Morgan; we'd like to resolve things with Liebenthal without anybody getting hurt."

One of the other youths nodded. "My dad isn't here. The doctor told him not to drink for a week, but he couldn't believe you guys didn't just kill them all last night."

"Morgan and I only got to Boston this morning, but I'm glad for our friends' restraint." She pointed to Sid. "Adversary Schneider was there at the Boston Chapter. I'm sure he'd like to hear from you."

The biker whose father could not come reddened as he brushed past Naomi to get out of the booth. She smiled as he passed; his blush had even set the back of his neck aflame. By the time he caught up with Sid, he was too far away to be audible over the hum of conversation, but the sight of him shaking the giant's hand encouraged her. *That's at least one man with no desire to fight.*

Morgan had not yet reached the bar; all of the stools had been taken, and he had told her that he wanted a particular stool: one adjacent to the one Michael Riordan occupied. However, there was no getting near the man; Fireclowns intent on a mixed martial arts match in progress on a screen over Riordan's head crowded him. She spied an upright piano sitting forlorn against the wall in a section of the bar kept clear, and caught the bartender's attention. "Do you think your patrons might fancy some live entertainment? I assume your piano's in tune."

"I keep it in tune for my son; he practices in the mornings after helping me set up for the afternoon. I gotta tell you, though, these guys aren't much for the classics."

That suited Naomi; she wasn't in the mood for Beethoven, or Mozart, or Rachmaninoff, or Liszt, or Chopin, or Grieg. This wasn't a situation which could be played by a score, or rehearsed. Tonight was a night for improvisation. She shrugged off her armored coat and slung it over her shoulder, holding the collar with a crooked finger. "I also play jazz."

She draped her coat over the top of the upright and sat down to uncover the keys as she connected to the network and began Thelonius Monk's "Straight, No Chaser" from memory. She played it often enough at Crowley's Thoth shows while Christabel had ducked backstage for various reasons that she needed no score. Since she didn't have Sid to play the bass or Morgan to accompany her on guitar, she had no choice but to improvise. The youths with which she had spoken gathered nearby to watch. She had stopped playing Monk; her own invention crystallized around the seed his own work provided. She played without rest, letting themes and various develop on their own accord as she slipped into a flow state.

At some point her music wriggled free of the strictures of jazz. The tropes inherent to the style resumed their proper place as tools and techniques which Naomi might call upon as needed to reflect her own mind and emotions. Her peripheral vision suggested the presence of a vast crowd, but she dared not turn her head to verify the size of her congregation; experience proved an unforgiving tutor in teaching her of the fragility of concentration in a flow state, once achieved. Once she permitted some ancillary concern to shatter her focus, she had no guarantee concerning its restoration.

Feeling the need for an additional instrument to provide counterpoint to her piano, she let her voice take wordless flight. Without the need to convey lyrics, it was a pure instrument; she trained in the use of her own throat, lips, and tongue with same dedication with which she trained her hands at the keyboard. After a time, however, her voice stilled and her hands had settled into a melody familiar to her; it was an art rock song from the 1970s concerning a trip to a fair, which she often performed with Morgan for the same reasons she might perform Thelonius Monk: because Christabel was backstage and thus unable to object.

When she was done, she rose from her seat before the piano and bowed before the crowd of Fireclowns gathered before her as she spied Morgan seated besides Michael Riordan. "I'm sorry if I bothered anybody. I haven't had a chance to practice in a few days, and I couldn't resist. Would anybody mind terribly if I continued after I've had a drink?"

One of the Fireclowns raised a bottle of beer. "You can have a drink on me, lady."

She smiled at the biker. He closely resembled Michael Riordan; both had similar wiry builds and square-jawed faces with pale blue eyes and reddish blond hair. "Generous of you, but I think everybody's drinks and meals come courtesy of Morgan Stormrider tonight."

The bottle slipped from his grasp, and would have shattered upon the floor were she a second later in catching it. He stared at her for a moment before turning to the bar, where Morgan continued what appeared to be an animated conversation with the president of the Fireclowns MC, Michael Riordan. "Why is the boss talking to Stormrider, anyway?"

*No doubt Riordan will explain things to the Fireclowns if he agrees to Morgan's proposal, but what harm can I do by helping?* Naomi doubted that she'd do any harm by telling the truth. "We're here to depose and arrest Alexander Liebenthal. We would have no argument with you if you were not working for Liebenthal, so we'd like you to step aside."

"Look, lady, Liebenthal isn't paying us enough to cross swords with your man. If he'd rather not kill us all, that's *fine* with me." He offered Naomi his hand. "I'm Roger Riordan, by the way. Mike's little brother, and the closest thing the Fireclowns MC has to a vice president. Why not come with me to the bar, so we can try talking sense into my big brother?"

\###

