“Adversary Stormrider, would you be so kind as to restrain your girlfriend?” Elisabeth Bathory’s voice was smoky, and would have been persuasive on its own. However, her words came with a sudden *wrenching*, which felt to Morgan as if somebody had grabbed his emotions by the scruff of the neck and tried to point them in a direction perpendicular to their original trajectory. A sudden urge to rise to Bathory’s defense clawed as his consciousness, which Morgan understood as a psychological invasion of some sort. *No. This is not one of my thoughts or desires. Why would I want to protect Elisabeth Bathory? She’s no friend of mine. If anything, she’s caused harm to friends of mine and is therefore my enemy.*

Instead of calling Naomi off, Morgan drew his new pistol, which Nakajima had insisted upon giving him at cost. He held it in a loose grip, and let it rest on his thigh while aiming at Bathory’s midsection, and rested his finger along the trigger guard. “I think Adversary Bradleigh is entitled to an answer.”

Bathory blinked, her eyes widening as if she fully expected Morgan to step between her and Naomi. “Adversary Bradleigh, would you please step back a bit? My shot isn’t as clear as I’d like.”

“You would shoot me?” Rather than the fear Morgan expected to find in Bathory’s eyes, for staring down the barrel of a pistol tended to make people nervous, he imagined he saw amusement. “Excellent. It appears no further psychological testing is necessary.”

“Morgan, what is Bathory going on about?”

“He won’t be able to answer you, Adversary Bradleigh.” Bathory rose from her seat and slipped past Naomi. She perched on the arm of Morgan’s seat, leaning over him so that her eyes became his world. She was like him, a CPMD carrier, and her amber eyes shimmered, her pupils widening as if she lusted after him. “Do you think this is just about some idiot taking over a city with a biker gang for an army and a failed Adversary as his enforcer?”

“Of course not.” He pressed the muzzle of his pistol into her belly as she leaned over him, to make her back off. She recoiled, and he felt the warm fog clouding his reason and demanding he trust Bathory tear itself to shreds. “Christabel was murdered just after the coup, while you plied my friend Edmund with wine and hashish in order to get access to his AI. You uploaded Witness Protocol data which could be used to implicate Naomi and me in Christabel’s murder. Rather than go immediately to Boston to deal with Liebenthal and Munakata, I came to London to help Naomi get free of MEPOL, who held her without charging her with an offence, and stuck her inside a Faraday cage with Alan Thistlewood, who tried to sexually assault her.”

“I’m sorry to hear that.” Morgan wanted to believe her; her tone had become subdued. However, he recognized her downcast look. Edmund had warned him and other young men at ACS about when he was younger. The old misogynist gathered them in darkened auditoriums and played old movies in which Lauren Bacall tucked in her chin, lowered luminous eyes, and stared at men to melt their defenses. He stood, and took a two-handed grip on his pistol as Naomi clipped her sword’s scabbard to her belt, and drew her own. Now Elisabeth Bathory had two guns covering her. “No doubt you’ll be skeptical, but I have always been a fan of Crowley’s Thoth. I even have the real first album, *Magick in Theory and Practice*. I never wanted any harm to come to any of you.”

“Nevertheless, harm has come.” Naomi’s voice took on a tremble Morgan had never heard from her before. “Christabel is dead. MEPOL would have accused Morgan and me of the crime for lack of a better suspect, using video you made available to the world. I think you know the murderer, Ms. Bathory.”

“For whom are you working, Ms. Bathory?” Morgan cut her with the question before Bathory could recover her balance. He half-expected Naomi to accuse him of tipping their hand too early using secure talk, but she only nodded to him. “Are you working to give Alexander Liebenthal time to consolidate his hold on Boston? Or are you working for Munakata Tetsuo? I killed him three years ago, or so I thought. No doubt the man holds a grudge.”

Bathory’s expression grew thoughtful as she retreated to her seat. “You place me in different company from what I tend to choose for myself, but your analysis is closer to the truth than you realize.”

An impulse to arrest Bathory pounced upon Morgan’s consciousness, only to have him grab it by the throat and throw it to the floor. *If I arrest her, I put control of the situation back into her hands. Once I’ve bound her, she’s entitled to the due process of law and has the absolute right to refuse to answer my questions.* “I suspect, Ms. Bathory, that you want me to arrest you.”

“It would place me in your power, would it not?”

Morgan holstered his pistol, ignoring Naomi’s questioning look. He responded to the more explicit question she shot him over secure talk by texting back, “We’re both pointing pistols at her, and she isn’t impressed. I don’t think we can force her.” He settled back into his seat; he crossed his legs as he locked his eyes on Bathory’s. “If I use my authority against you, I lose. You will just exercise your right to remain silent, leaving me with nothing but my suspicions. Instead, I am going to ask you one last time. Would you *please* tell me what you can?”

A perfect black eyebrow arched. “What I can, Adversary?”

“You’re here for reasons which aren’t entirely congruent with the Society’s mission, aren’t you?” Naomi had finally put away her own pistol, and rejoined Morgan by sitting down. “It seems we both think you’re working for somebody else.”

“There is little I can tell you at this stage. I suspect that Christabel Crowley was murdered by somebody I know only as ‘Imaginos’, and that he not only murdered Crowley, but arranged the coup in Boston as a means of testing you, Adversary Stormrider.”

“For what purpose is Imaginos testing Morgan? Why him, anyway?”

Bathory offered Naomi an apologetic glance. “I cannot answer that. However, I can say that Imaginos is not only testing Morgan. He is also testing those closest to him, or rather, your bonds with him. He expected that you would turn your backs on Edmund Cohen after I induced him to betray you.”

“What other tests has this Imaginos run on me?”

“Adversary Bradleigh at MEPOL was the next test situation. The first test was of your resourcefulness, as well as that of your friend, Claire Ashecroft, in effecting Bradleigh’s release. The second test was of your bond with her, Adversary Stormrider. To put it simply: the question was whether or not you were man enough to accept Naomi Bradleigh as your equal, if not your superior.”

Morgan nodded as he considered the last couple of days. He smiled at Naomi’s text to him as she took his hand: “I was also testing you. I had been afraid ever since Claire told me you were coming for me that you wouldn’t be able to see me as an equal capable of standing beside you regardless of the situation.”

Bathory continued before Morgan could offer Naomi a suitable reply. “I should mention that I had no objection to Imaginos’ test in this case. The impulse to shield a woman from the world is like any other quality common to men: too much is as useless as too little.”

*Have I stumbled into an unknown attempt by Jane Austen to pioneer the thriller genre?* Morgan shook his head at the thought, and considered Bathory’s words. However, when he considered them in context of what he had read of Aristotle’s notion of a golden mean, they made sense of a sort. A lack of courage was cowardice, but excessive courage became foolhardiness. Likewise, a lack of willingness to protect Naomi would have left her with no reason to trust him, but he would stifle her if he tried to protect her from everything. *Either way, Naomi would feel as if I did not value her as she is.* “Assume for the moment that I passed that test, Ms. Bathory. Does what you’ve been doing here on this maglev also constitute a test of some sort? You said earlier, after you failed to persuade Naomi and me to put our weapons away, that no further psychological testing was necessary. Would you please explain that?”

Bathory held up her tablet, without showing the screen. “You have implants which conform to particular specifications mandated by the Phoenix Society. One of those specifications describes a subsystem intended to prevent you from being debilitated by battlefield stress by manipulating the balance of neurotransmitters inside your brains. It is not a system we’ve used, due to ethical concerns, but I have the means to monitor your immediate mental state by capturing faint electromagnetic radiation emitted by your brains using a process similar to Van Eck phreaking.”

Morgan turned towards Naomi; her narrowed eyes suggested she was thinking along similar lines, but he texted her to be sure. “Do you think she’s bullshitting us?”

“I think she’s been bullshitting us the entire time.” Naomi rose from her seat, her hand on the pistol riding her right hip, and opened the door to their compartment. “Ms. Bathory, we’re sorry to have wasted your time, but I don’t think you can assist us with our mission to Boston.”

Elisabeth Bathory turned a look both appraising and appealing towards Morgan, but did not rise from her seat. “I’m sorry. I wanted to explain what I was doing to you and Adversary Bradleigh in terms of technology with which you might already be familiar. If I explained that I have preternatural abilities which I can use to persuade, seduce, or outright dominate people, you might think I was raving.”

“Actually, I’d be tempted to believe it.” Naomi closed the door and returned to her seat. She reddened as she looked at Morgan. “You see, I’ve loved Morgan for years, but every time you talk to me, I feel --“

“I know. I tried to make you lust after me, despite your inclination and habit. I did the same to Morgan. I was asked, not by Imaginos, to test the strength of Morgan’s ego, his mental resistance.” She glided towards him, and her hand felt almost like Naomi’s as she caressed his face. “He loves *you*, Ms. Bradleigh, but if I could get him to transfer those feelings towards me --“

*She’s still tampering with my emotions. Hasn’t she tested me enough?* Morgan made his voice a low snarl, a final warning before baring steel. “Get your hand off me.”

Bathory immediately withdrew her hand, and backed away until she was out of his sword’s reach, which suited Morgan. “As you can see, Morgan is quite capable of resisting my attentions. I think it’s related to the training you’ve both received as Adversaries, in particular, the preventative cognitive/behavioral therapy you both underwent during training, and after every mission.

“So, Morgan and I can resist attempts to bewitch us. Why does this matter?”

“I can’t explain that at the moment.” Bathory opened the door to the compartment herself. “However, if you successfully carry out your mission to Boston, you may find a means of obtaining the answers you seek. In the meantime, Malkuth has footage of Munakata Tetsuo in combat in addition to your instructions. It seems he’s been practicing.”

The compartment door slipped shut behind Elisabeth Bathory as she turned her back on them. Morgan waited in silence for several minutes, feeling the hum of the maglev beneath his feet as it tore through the miles. He needed the right words if he was going to explain himself to Naomi, and get her to understand exactly what he had felt. “It wasn’t that I wanted sex with Elisabeth Bathory. Sex was part of it, but when she turned on the charm I felt a need to submit to her in every respect, and obey her every command.”

Naomi blinked, her eyes widening as if in recognition. “That’s what you felt as well? Claire insists that everybody has an exception to their usual preferences, and I was afraid that Bathory had somehow *made* herself my exception. But it felt more like how you described it. Sex would have just been an outward sign of my submission. She felt like a devil in a black dress, trying to con us into selling ourselves for a song.”

Morgan took Naomi’s hand, and found it cold. He caressed it, chafing warmth back into the skin as he locked his eyes on hers. “We’re going to bring Liebenthal back alive, and see the bastard stand trial. You’re going to retire again, and I’m going to retire with you. We’re not submitting to anybody but each other.”

“And if she tries to seduce you again?”

“Shoot me. Then shoot her. You know I’ll get over it.”

Naomi's hand remained cold in his; in her sudden anger, her voice held all of her body's heat, and the threat of ignition. “If you value our relationship, never joke about getting shot in front of me again. I hate hearing you speak so casually of your own safety. Would it kill you to value yourself as highly as I value you?”

\###

