City Hall Plaza felt like ground zero of a thunderstorm possessed of sufficient energy to spawn tornadoes worthy of use in disaster movies as thousands of protesters responded to being shot at from City Hall by raising their own rifles to rain gunfire upon the building in hope of either suppressing the shooter, or killing him. Morgan shouldered his way through the crowd. Sarah's implant broadcasted a distress signal which his own implant triangulated and superimposed on a street map of the area, allowing him to find her. 

EMTs examined her as Sid kept the remains of her legs elevated; enough remained below the knees to wrap tourniquets around. Morgan took her hand as she opened her eyes. "I told you this wouldn't work out, Stormrider. You shoulda just gone in and whacked the bastard."

He caught Sid's attention with a text, since the gunfire from the protesters was too close to permit speech. "How bad is it?"

"How bad does it *look*? She's lucky I was there with a first aid kit, and that the rounds hit as low as they did, but the surgeons aren't going to be able to reattach her feet. Some of the other poor bastards tried to cover me."

Morgan glanced about himself, only to see more fallen. Paramedics attended to some, stabilizing them for transport to a hospital and further treatment. Others had blankets draped over them, to be collected later; the dead were unable to object to the wait. Each confirmed Sarah's opinion. She moaned, and tried to pull her hand free as Sid gave Morgan's shoulder a gentle punch. "You're crushing her hand, man."

Morgan released the wounded Adversary's hand, and placed it on her belly. "I'm sorry."

"For my hand?" Sarah raised the hand Morgan had held to examine it. "Least of my problems. Have you seen my boots? My feet are cold."

"I--" Morgan searched the area without rising before finding the rest of Sarah's legs. The feet and ankles were intact, but the rest was mangled meat and gristle. Blood congealed on the tops of her boots. He took a deep breath before turning back to Sarah. "Try to bear with it. I'll find you some fluffy socks when we get you to the hospital."

A weak smile curved her lips. "Dude, I can see my fuckin' stumps. Sid's holding 'em up to keep me from bleeding out. I guess you don't know me well enough to know when I'm joking."

"I suppose I'll have to knit some special socks for you, then."

An EMT bearing a folding chair and some bungie cords crouched by them. "Adversary, can you please lift the patient's bottom up so I can get the back of the chair under her?"

Sarah winked at Morgan as he used her belt to lift her without taking liberties. "I wanted your hands on my ass, but this wasn't how I wanted to go about arranging it."

"Naomi would kill me."

"Naomi can join in."

"Too bad you had to get shot. Claire's in town; you two would get along." Morgan held Sarah's thighs in place as Sid got out of the EMT's way. The technician lashed her legs to the chair's seat with bungie cords before using a forearm to wipe sweat from her forehead. "OK. This should keep Adversary Kohlrynn's legs elevated until we get her to the hospital." She glanced around in search of help. "Fuck. Can you gentlemen lift her into the ambulance?"

Morgan nodded, but forced himself to retrieve Sarah's feet first. Sid was probably right about the surgeons being unable to rebuild her legs, but to leave part of her in the plaza was unconscionable. He placed them between the chair's legs, out of Sarah's view, before taking his end of the stretcher. "Ready?"

Sid nodded. "On three." They counted down together, lifting the stretcher in unison. Sid backed into the ambulance, and remained inside once Sarah was secured. "I'll go to the hospital with Sarah. Stay strong."

"What do you mean?"

"Sarah's right about it being easier to just go in and whack the bastard. Fewer people would have gotten hurt, but we can't keep killing everybody who opposes us. We're not proving anything that way." The ambulance started up, its electric motor humming. "Don't kill the bastard. You're just letting him off easy."

Morgan nodded, and turned towards City Hall. The thunder of massed gunfire faded as the protesters emptied their magazines and stopped to reload. Shattered glass littered the plaza around the building, but most of the rounds fired appeared to have struck concrete. The building's facade, which had been reviled as ugly and brutal since it was finished the 1960s, bore thousands of fresh scars. The upper windows, shadowed by concrete overhangs and dividers protruding between the panes, were perfect for a sniper; as long as the bastard did not stick the barrel of his rifle out too far, he would be able to kill from cover.

"Is that Sarah I saw on the stretcher?" Naomi had finally caught up with him.  Her voice was barely audible as she glanced around her. "How could he have killed so many?"

Morgan pulled free of Naomi's embrace, shaking his head. "Liebethal is not wholly responsible for these deaths. I should be on trial alongside him. I appealed to the people. I called them out here. I *used* them, assuming all the while that Liebenthal would not dare fire upon those he meant to rule. I was wrong, and these people are dead as a result." *I failed Christabel. I failed in my mission. Why do I still call myself an Adversary?* "Our mission was to remove Liebenthal from power without bloodshed. I failed, Naomi."

"You have not failed yet." Naomi took his hands, drawing him closer to her. "Your memory of your orders is faulty. You were to take Liebenthal alive, by any means necessary. The rest was your decision." Her hands tightened around his. "And it was the right decision. If the people had resisted as they should have, there might still have been deaths. With the Fireclowns working for Liebenthal, *more* people might have died."

"That won't comfort the families of those who died today. Nor will it give Sarah back her legs." He tore himself free of Naomi and sped a text to Eddie, who had stationed himself on a rooftop across the plaza from City Hall. "Can you see Liebenthal?"

"I can't see shit from my current position. I'm too high above, and the angle's all wrong for me to nail somebody inside. If that bastard were firing from the rooftop, I'd have busted a trank in his ass as soon as he shot Sarah." Morgan looked up at the building Eddie had chosen as his vantage point, and found the barrel of his rifle peeping out the window. It disappeared inside, and did not reappear at that window. Instead, a window opened three floors above. "That's better. I can see into some of these windows, but these fucking overhangs and dividers really complicate matters."

Morgan considered the building again, realizing that Eddie was right. Snipers stationed at every window on that floor might cover City Hall, but it was unfair to expect Eddie to cover the entire front. Nor could Catherine Gatto and Miria Deschat help much; having them join Eddie would require them to leave their stations and cross the plaza, making them vulnerable. Furthermore, City Hall had three other sides which Eddie had no hope of covering without entering another building. "I'm going to have to go in there."

"Sorry I can't help you. I'd love to nail him after what he did to Sarah. She was the best I'd had in years, you know?" Morgan refrained from chiding the old soldier. At least he was using secure talk, and not secure relay chat; he subjected nobody else to his casual misogyny. Furthermore, for him, his involvement with Sarah was a step up from his usual dalliances with courtesans. She had claimed to have enjoyed Eddie's company, which implied that their affair was consensual. *Maybe Eddie actually gives a shit about Adversary Kohlrynn.*

Feedback shrieked across the plaza as somebody activated a public address system, and gave way to a wheezing cough which did its best to imitate thunder. "I see you, Stormrider. I've got you dead in my sights. Who's that standing beside you? A new whore to replace Naomi Bradleigh? Have you no decency, man?"

Naomi shot a glance at him. "Why does Liebenthal think Sarah is me?"

"Never mind that." He found Claire and added her to the Adversaries' secure relay chat. "Claire, can you trace the PA signal?"

"Not directly, but the wiring schematics suggest that Liebenthal's in the middle of the third floor. He's bluffing about having you in his sights unless Munakata's doing the shooting." 

The thought of Munakata being the one responsible for these deaths and Kohlrynn's injury, rather than Liebenthal, offered Morgan no comfort. *Not that it matters. Munakata works for Liebenthal, making the latter responsible.* "I need their IP addresses superimposed on a floor plan, Claire. I have to get in and find these bastards before they do further harm."

"I'm on it." Claire signed off as Liebenthal continued. "Tell me, Stormrider, how it feels to have the blood of civilians on your hands. How does it feel to know that the dead around you are *victims* of your need to preserve the Phoenix Society's reputation? Do you blame yourself? Or will you add murder to the list of charges against me?"

A police officer ran to Naomi carrying a megaphone, which she handed to Morgan. "Don't let him speak unopposed."

Morgan nodded as he accepted the bullhorn, and placed it before his mouth. "What do you want, Liebenthal? We can end this without further bloodshed."

"Only if I let you prove me a liar. I killed Abram Mellech for suggesting it. I mean to keep killing until you come in here and stop me."

"Why are you doing this?"

"The people have the right, and the responsibility, to know the truth. The Society tells us that they uphold individual rights, and that our society is governed by principles of liberty, justice, and equality. It's time you showed the good people of Boston the *truth*. It's time you showed us the violence inherent in the system!"

A message from Claire followed. "I just shut down the PA. I'm not going to listen to that arsehole paraphrase Monty Python."

"Thanks, Claire." The reply came thrice, sent simultaneously by Morgan, Naomi, and Eddie. Morgan turned off the megaphone and returned it to Naomi. *The threat is plain. More innocent people will die if I continue this waiting game.* "I'm going in there. Eddie, if you see Liebenthal and have a clear shot, take it. Naomi --"

"I'll go with you. You need a sword at your back."

Morgan took Naomi's hands, and turned with her so that his body would shield her if Liebenthal attempted a shot. Rather than use his implant, he resorted to his voice, to impress upon her the importance of his request. "Your sword will be at my back, but I can't let you follow me inside. I need you to get these people out of here."

"To deprive Liebenthal of targets?" Naomi's expression settled into firm resolve. "I understand. I'll get the cadets from the New York Adversary Candidate School to help me." She stole a bare brush of a kiss before backing away. "Be careful in there. Whatever you did with the Godhead Riders earlier, you look tired."

*I am tired. I'm tired of dealing with filth like Alexander Liebenthal. However, somebody has to do it, so why shouldn't it be me?* The bitterness of his thoughts tasted of bile and acid at the back of his throat. He castigated himself, reminding himself that it was a privilege to serve as an Adversary as he turned towards City Hall and undid the strap keeping his sword in its scabbard. He used secure relay chat to answer Naomi, because what he meant to say concerned the others as well. "You're right, Naomi. I *am* tired. When reasonable Adversaries like you fail, the Society turns to unreasonable Adversaries like me. When diplomats fail, the Society speaks of necessary evils and resorts to killers. I'm tired of being a necessary evil."

"You can't go in there thinking like this." 

Eddie had more to say, which Morgan did not bother to read. He already knew what he meant to say to his friend. "Eddie, I spent the last year working through channels and following proper procedure in order to resign my commission. For my pains, Karen Del Rio kept using me as the Society's hit man. No more. Once Liebenthal has had his due process, I quit. Do you understand? I've had enough, and I want *out*." He disconnected as he approached the entrance to City Hall. The interior was shadowed; the emergency lighting would do more harm than good, for it would be insufficient to see normally, but too bright for Morgan's vision to switch to infrared. Before he could mount the steps leading up the the front door, they opened before Munakata Tetsuo as he strode to the top of the staircase with his katana bared.