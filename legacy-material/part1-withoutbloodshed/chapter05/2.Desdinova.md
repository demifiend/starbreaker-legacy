Desdinova dismissed his secretary with a nod after she served tea to go with his morning correspondence. His brother, Isaac Magnin, often chided him for employing people for work AIs could handle, but Ohrmazd Medical Group's motto — "The healing touch is a human touch." — often entailed the rejection of automation in favor of employing human workers. *Not that my brother should care, as long as the Phoenix Society gets their cut of the profits.* He sipped his tea as his terminal displayed an incoming call alert from Malkuth. He put aside the latest issue of *The Hemostat*, in which an article he peer-reviewed six months ago had finally been published, and clicked the notification with his trackball to accept the call. "What's wrong?"

Malkuth did not bother to render his avatar in his office. Instead, Desdinova's screen displayed the cabalistic Tree of Life, with Malkuth's node at the root of the tree highlighted, and the words ‘audio only' in the center of the screen. "While drunk, Edmund Cohen downloaded video recorded with Witness Protocol, and allowed it to be uploaded without encryption to the network."

"Have you removed it?"

"No. In fact, the video just went viral. Before that happened, however, MEPOL was using it to justify holding Naomi Bradleigh as a material witness."

"How did it go viral?"

"I think it was Claire Ashecroft's doing."

"One of Stormrider's friends? The grey-hat hacker?"

"The same. Her reasoning was that if the leak was brought to public attention, MEPOL would be unable to use it for lack of proof of legitimate access. She knows Edmund Cohen is executive council, by the way."

*Does Imaginos know about Ms. Ashecroft? Or is she a friend of Morgan's who has thus far escaped my brother's notice?* "What else does Ms. Ashecroft know?"

"More than you'd prefer, I'm sure. Do you want to see the video in question?"

"You might as well send it."

The footage arrived a minute later, and Desdinova adjusted the playback settings to reroute the audio to his implant. Though the video displayed small compression artifacts, he could tell it had not been recorded by Morgan Stormrider, Christabel Crowley, or Naomi Bradleigh. Instead, he watched the implosion of Crowley's Thoth through another's eyes.

"I suppose you'll be replacing me now." Christabel stalked about the dressing room, holding her black high-heeled shoes in one hand. She kept passing in front of a dish of blue candies which, according to Edmund, was a vestige of a tradition dating back to the 1980s. "As if it was *my* fault the dress I wanted to wear tonight was three sizes too big."

"When my dress turns out to be too big, I manage with a few safety pins."

Christabel brandished a shoe at Naomi. "You never gave a damn about fashion, you freak."

"I think we're getting side-tracked." Morgan placed himself between the women. He had loosened his tie, but was otherwise dressed for an encore in a black suit tailored for his lithe frame. "Christabel, how long were we supposed to keep the fans waiting?"

"As long as it bloody well took!"

Naomi shook her head. Her unbound hair was an avalanche over creamy shoulders left bare by a black gown with an empire waist. "This is beneath you, Christabel. You're trying to avoid the fact that you let us down. We needed you, and you weren't there. You didn't just leave us for fifteen minutes to fix your makeup. You left us on that stage for an *hour*, and offered no indication of when you'd be ready to *start the show*."

"And so you *replaced* me with some bloated sow from the orchestra? How dare you put some pregnant cow on stage and have her play my part?"

"It was easy, Christabel." The breath Morgan took, and the set of his shoulders, suggested to Desdinova that Morgan approached the outer limits of his forbearance. His fists clenched and loosened in time with his breath, and he clasped his hands behind his back to present a non-threatening appearance. "You're a good musician, but not irreplaceable, and I was tired of you and your prima donna attitude long before I broke up with you."

"Morgan's right. You play well, but there's no real passion in your music. You're just a technician."

"Just a *technician*? I *founded* this band, Naomi. *You* were covering "Ma Vie en Rose" in some fucking *dive* on Broadway. Morgan was just some killer with a badge pretending to be Yngwie Malmsteen. You two would have been *nothing* without me."

"I would have been an Adversary, though I might not have had Naomi's friendship."

"I was happy at Mick's Lounge, Christabel, and I sang with Sleeping Sun before you faced your first audition at Juilliard."

"Do you think Mick will hire you again, Naomi? Morgan, are you content to just be an Adversary? You're both *nothing* without me, and you bloody well know it."

"I know nothing of the kind, Christabel. I told you I would probably quit at tour's end. The tour's over, and I'm done with you."

"Don't think I'll keep Naomi around if you go."

"You need not concern yourself on my account. I too am leaving. Your behavior ever since you and Morgan broke up has barely been tolerable, and tonight you've been positively insufferable."

"*My* behavior?" Christabel threw her shoes to the floor, not noticing as one of the heels snapped off. "Do you know what it's like to know to know the man you love loves somebody else?"

"I bore it longer than you, and with more grace."

"I see how he acts around you, you ghostly bitch. How long as he been fucking you behind my back?"

"He has yet to kiss me."

"Liar!" Christabel sprang towards Naomi, only to be caught from behind by Morgan, who restrained her and attempted to reason with her by whispering in her ear. Rather than calm herself, she twisted in Morgan's arms and raked lacquered fingernails across his face. Blood welled from the scratches as she began to pummel Morgan, her thin arms pumping as she drove her fists into his belly. She soon tired, and crumpled in tears to the floor. "I suppose you're going to kill me now, like you do everybody else who raises a hand to you."

"I had it coming. Once you began to cool towards me, I let my regard for Naomi grow. My failure to hide my affection for her only gave you further reason to withdraw from me. If our relationship suffered a long, slow death it is in all probability my fault. I was a coward, who feared to end the relationship properly lest I tear the band apart."

He turned to Naomi as blood trickling down his face began to stain the white collar of his shirt. "I think Christabel needs some time alone."

Desdinova stopped playback, and regretted watching as much as he had. *It might have been less sordid if the video had shown Morgan and Christabel having sex together, rather than showing the three of them at what must surely be a low point in all their lives.* He wiped the video from local storage, and called his secretary. "Ms. Ives? I would like to speak with Edmund Cohen."

"Connecting now, sir."

The man looked almost respectable behind the wheel of his car. "I had a feeling you'd call, doc."

"Then you know why I am displeased with you?"

"I could make an educated guess."

"Then I need not remind you of the responsibility with which I entrusted you."

"No sir."

"Care to explain yourself?"

"I know why you kept telling me to stay the fuck away from Elisabeth Bathory, sir."

*No wonder my brother thinks Cohen's a liability. I gave him the simplest possible mission: befriend Morgan Stormrider, and watch his back. I wanted one person in Stormrider's circle of whom I could be sure, one person who owed my brother nothing. Did it have to be this idiot?* "Edmund, you've had better-looking women than Ms. Bathory."

"And they were *easy*. All I needed was money. Bathory was a challenge."

"You weren't even worthy of her contempt. Did you not think it strange when, after decades of nothing but courtesy from her, she would act as if she found you attractive?"

"I was almost too drunk to fuck, doc, let alone *think*. She did something to me while I was drunk and vulnerable."

*That makes sense. Ashtoreth specializes in manipulating sensation and emotion. Edmund has wanted her for years, not for lust's sake, but as a matter of pride. The man can't bear to admit the existence of a woman for whom he holds no appeal. The fool even makes passes at militant lesbians if left unsupervised. Then again, he also makes passes at handsome men.* "I suppose you'll explain yourself to Morgan. Do *not* say anything to reveal Elisabeth Bathory's true nature to him."

"I was just going to tell him not to trust the bitch. Also, I'm going to tell him what I'm going to tell you since I've got your attention: no more bottles of scotch at Winter Solstice."

Desdinova's eyebrows rose, and he leaned forward. He had spent years complaining to Edmund about his vices. His whoring made him vulnerable to blackmail. His drinking left him vulnerable to poor judgment, leading him to do idiotic things like let Ashtoreth seduce him — or press Morgan up against the wall and try to kiss him at Winter Solstice. *I should be grateful he refrains from using cocaine often, or in large quantities.* "You're giving up alcohol?"

"Yeah, before I do something *really* stupid. If you see Elisabeth before I do, tell her I said thanks for giving me a reason to give up drinking. If I have trouble sleeping, I'll just smoke some hashish. She left me a really nice hookah."

"She won't appreciate you calling her by her first name."

"Then she shouldn't have let me fuck her."
