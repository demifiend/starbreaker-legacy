"How much longer are we supposed to wait?" It was the first thing Karen Del Rio had bothered to say to the driver of the limousine in which she had waited for the past hour; a mere driver, like most men, was beneath her notice unless she wanted something of him. The limo was comfortable enough that she had had no need to make demands of him; the interior was neither too warm, nor too cold, and the driver had treated her with every courtesy, reminding her that he and the food and drink with which the limo was stocked were at her disposal. Moreover, the vehicle's onboard systems permitted her to monitor the situation. *I can't believe Sarah's in the hospital because of Stormrider. How dare that son of a bitch make it necessary for me to find a new slut?*

The drug called to her. Hours had passed since she had last breathed in World Without End. The need to do so and be liberated from the limitations of her body to experience release beyond human experience swelled within her, growing from the center of her belly and radiating outward to suffuse her not with bliss, but its absence. She disciplined herself, and tried to ignore the jones. When Isaac Magnin finally left her, after watching Morgan Stormrider's unauthorized press conference, he had warned her against using the drug in Boston. "I will know, and I will be displeased should you ignore my instructions."

The gunshots and screams, at least, had subsided. All of the limo's entertainment options had been disabled, forcing her to attend to the events outside. Upon her return to New York, she meant to demand that Stormrider be brought before a tribunal for inciting this riot; it seemed the man was no longer content to kill with his own hands, but had resorted to feeding his bloodlust by goading others into the line of fire. The limo started again, advancing towards City Hall as a message from Isaac Magnin reached her implant: "Deliver the orders. Take Munakata Tetsuo from Stormrider's custody for final disposal."

She puzzled over the second instruction. If Stormrider had Munakata in custody, did that mean he had already taken Alexander Liebenthal? What did Magnin mean by 'final disposal'? *Munakata's supposed to be dead. Stormrider shot him in the head three years ago. We can't have people knowing that we didn't actually kill that bastard when we said we did. It would undermine the Society.*

The limo stopped, obstructing Stormrider's path. The Adversary stopped, and stood at attention as the driver opened the door for Del Rio and helped her out. She offered no acknowledgement for merely doing his job, but instead strode towards Stormrider with the new orders extended before her. "I have new orders for you."

Stormrider accepted the envelope, but did not open it. Instead, he examined it as if suspecting fraud. "Were these orders authenticated by Malkuth?"

"Why would I care? Isaac Magnin delivered them to me, with instructions to see them in your hands." Del Rio frowned as she answered, unable to understand Stormrider's skepticism. "He's a member of the executive council. That should be good enough for you."

Stormrider's voice was cold formality. "Your pardon, director. This is just extremely unusual. I normally get updated orders directly from Malkuth." He tore open the envelope and shook out the sheet of flimsy it contained. "'Resolve the situation in Boston using whatever force deemed necessary.'"

"Is there a problem with that order, Adversary?" Del Rio was unable to imagine a reason for him to balk at such an order. It was free rein for Stormrider to do what he no doubt wanted to do from the beginning of this mission: storm City Hall and slash open Alexander Liebenthal's throat with a single swing of his sword.

Stormrider read the order aloud a second time. "This is unforgivably vague. What force is necessary, and according to whom?" He slipped the flimsy back into the torn envelope, and slipped the envelope inside his armored coat. "This is either some kind of test, or an attempt at throwing me under a maglev. I've neither the time nor the inclination for this in either case, Director."

Del Rio's fists clenched from a desire to strike the Adversary before her. She had never trusted him; he was nothing to her but a killer seeking society's imprimatur. When he told the psychologists interviewing him that killing was all he was good for, so he might as well kill those who harm others and do some good in the process, Del Rio had been content to take him at his word. His performance during the Milgram Battery, in which he had not only defied authority in every scenario placed before him but recognized the scenarios as externally-induced lucid dreams, revealed his utter inability to accept any form of external discipline. The man had never been able to take an order without questioning it. "Why be so cynical? You finally have what you want: authority to kill Alexander Liebenthal if you deem it necessary."

The slow shake of his head, as if Stormrider had been called upon to explain a simple concept to a defective child, further exasperated her. "What of my responsibility to uphold the Universal Declaration of Individual Rights without exception?"

"Don't you dare suggest Liebenthal's rights outweigh those of his victims."

"Nor do everybody else's rights outweigh Liebenthal's." Another slow, condescending head-shake punctuated Stormrider's reply. "The Society has been happy to take advantage of the people's reluctance to consider people like Liebenthal innocent until proven guilty. You were happy to look the other way while I murdered those accused of murdering other Adversaries. Whatever his crimes, Liebenthal is entitled to the due process of law. He has not yet been proven guilty."

"God damn you, that's what you *wanted*. You said you wanted to kill." Del Rio pressed her hand into Stormrider's chest and gave the man a shove, only to find him unmoved save for the hand grasping her wrist. "You insubordinate *bastard!* Why the fuck can't you just follow orders? And what's this shit about presumption of innocence? You *saw* what he did to Sarah Kohlrynn."

"Munakata told me he meant to do it to Naomi." Del Rio blinked at that, and at the resolve in his voice. *Liebenthal meant to kill his new girlfriend, but he still wants to see the bastard stand trial. He fucking means it.* 

Morgan continued, forcing Del Rio out of her own thoughts. "I was happy to follow orders for as long as I believed the orders were compatible with my oath to uphold liberty and equality under law. However, I realized that I've allowed myself to be used to assassinate the Society's enemies for no better reason than that their  temerity exceeds that of common criminals." He released her wrist, and Del Rio withdrew several steps while rubbing it. "Nor did I ever say I wanted to kill. I told the psychologist that I was good for nothing but violence, and so I wanted to use that capacity for violence to do good."

"You wanted to be a soldier." Del Rio sneered. She recalled instructions Magnin had given her as she waited in the limo. "All right, *soldier*. Where's that prisoner you captured? The one you said you whacked in Shenzhen three years ago?"

"Munakata Tetsuo has agreed to give evidence against Alexander Liebenthal. He is in protective custody for now. Once he has testified, he too will receive the due process of law."

"Where?"

Stormrider turned his back on her. "So you can have somebody else do a more thorough job of making him disappear than I had done three years ago?"

"I will see you stand trial for your defiance." It was a threat Del Rio had held in reserve, knowing it might only prove effective once. If it proved an empty threat, or if Stormrider were vindicated by his judges, she would have no power over him whatsoever. "I will see you convicted, stripped of your authority, and placed before a firing squad. I will *personally* ensure that your friends are given the task of executing you. This isn't some Milgram Battery scenario. In the real world, disobedience has *penalties*!"

Del Rio froze, her mouth hanging open as she realized the nature of the words which had just escaped her mouth in her sudden rage at Stormrider's insubordination. To utter such a threat under normal circumstances was unprofessional, at the bare minimum, but to hurl them at a man she *knew* would react to genuine threats with lethal violence had to be hubris. Unable to withdraw herself or her words, she waited for the hammer to fall. Rather than take weapon in hand, Morgan Stormrider laughed at her as if she had told him an unexpectedly hilarious joke. However, she did not fully relax. Despite his apparent mirth, his eyes remained hard. His laughter cut deeper than the chill wind from the east, which drew a gray curtain across the sky in the wake of the setting sun.

"Keep practicing, Karen, and you might someday manage poetry instead of melodrama." His tone was utterly flat, as if Karen were beneath contempt. Nor had he ever presumed to address her by her first name. She opened her mouth to apologize, and make some attempt at salvaging the situation, but Stormrider had already turned his back and begun to walk away. The orders she had given him lay forgotten on the brick pavement behind him. Snowflakes from a flurry melted into the paper, blurring the ink. A hand clasped her shoulder, and she spun to face its owner as a scream threatened to escape her throat. 

It was the driver. "It's time to go, Director." Del Rio nodded, and allowed him to escort her back to the limousine. As the door enclosed her in the back of the car, she poured herself a stiff shot of vodka. The onboard computer alerted her to an incoming message from the Phoenix Society. Though she expected it to come from Isaac Magnin, the sender was Stormrider, and the message two sentences: "I hereby resign my commission as Adversary, effective immediately after the successful capture of Alexander Liebenthal. This organization has become inimical to the ideals it was founded to uphold, and I cannot in good conscience act under its authority." Nor was Del Rio the sole recipient. Saul Rosenbaum and Iris Deschat were on the list, as was Isaac Magnin. *No. This can't be my fault. We've been trashing his resignation letters ever since the Shenzhen job, which was a request from the Shanghai chapter.*

\###


