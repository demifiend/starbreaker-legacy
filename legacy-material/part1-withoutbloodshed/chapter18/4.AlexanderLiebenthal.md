The sunset glared crimson through the tall, wide windows through which one might survey the portion of Boston west of City Hall. Its last light thrust at Alexander Liebenthal's weary eyes along a street leading to the plaza between two high-rises whose plate glass and steel facades reflected and amplified every beam which strayed from the direct path between star and observer. The sky was ablaze with the day's end, and held back for now the storm-clouds encroaching upon the land with far greater success than the ocean from whence it came ever managed. Snow began to fall from this gray blanket, a flurry which soon steadied into flakes tinted by bloody sunlight from the southwestern sky, which remained cloudless.

Liebenthal turned from the window, unwilling to gaze any longer upon the skyline lest an impulse towards poetry seize his mind and distract him from the matter at hand. He considered the rifle laying upon his desk, which had come to represent both his rise and imminent fall. He had made himself rich by taking money from Isaac Magnin via Munakata Tetsuo to collect these rifles from the Murdoch Defense Industries factory in the dead of night. His trucks had to drive all the way down to the edge of the city of New York, where Queens gave way to the independent communities of Long Island. Once laden, his drivers had to get the rifles to their destinations north and west of Boston, to mountain communities whose citizens dedicated themselves to the cause of the Repentant in Christ, whatever that cause might be. Liebenthal had never concerned himself overmuch; Magnin paid him to get the rifles, and Abram Mellech paid him a matching sum to deliver them.  What the Repentant did with those rifles was not his concern.

It had been a good racket while it lasted, and Liebenthal regretted not doing more to enjoy his fortune while he had it. Rich meals, fine wines and spirits, nightly entertainments of music and theater in the company of beautiful women well-paid to go beyond the girlfriend experience and worship him as their god made flesh, travel to any location on earth accessible via maglev or suborbital flight, legitimate political influence -- all of these and more had been within Liebenthal's grasp, had he only thought to reach out and claim it. *I was a dragon, hoarding my riches. Now I am but a wolf at bay, my pack killed or driven from my side.*

He used his implant to query the rifle's built-in electronics, and received the same answer as an hour ago. The weapon had sufficient power stored in its batteries to fire a single shot at full power. One shot would either kill Morgan Stormrider, or give him just cause to kill Liebenthal. *It doesn't matter if their favorite assassin dies by my hand, or he kills me instead of bringing me back alive. Either way, I defeat the Phoenix Society.*

"You seem pensive, Mr. Liebenthal." The insouciant, almost mocking voice startled Liebenthal, and he looked up to find Isaac Magnin seated before him, a glass of scotch in hand. "Has the time come to make your last stand?"

Liebenthal sat down, glaring at the other man from across the desk. "What do you want now?"

Magnin sipped his drink. "I thought to enjoy another glass of this excellent whisky, which you continue to deny yourself. Also..." He snapped his fingers, and the lights overhead began to glow. The rifle's software sent a notification to Liebenthal's implant indicating that it had detected several available tesla points, and was currently charging. "It seems rather unfair to make you fight Morgan Stormrider in the dark."

"Unfair?" Liebenthal spoke the word with a cynical chuckle, despite being grateful that the power was back on. "A lady in black told me that all of this is some kind of experiment for you."

"I told you as much at my last visit. It seems I was not the only one who underestimated the resourcefulness of Stormrider and his companions. Even now Claire Ashecroft works to shatter the defenses I placed in her way so that she can cut off the power again. However, she vies against all of the AsgarTech Corporation's artificial intelligences, who actively maintain the barrier."

"Who the fuck is Claire Ashecroft?" Liebenthal spat the name, enraged that the Society's assassin would have such friends working on his behalf while he, in his extremity, stood alone. It was a monumental injustice.

"A friend of Stormrider's, and of one of my employees. I had hoped to hire both women, but Ms. Ashecroft called me a rent-seeking parasite and told me to bugger myself with a secondhand Soviet ICBM, as if any of those were still lying about." Magnin's eyes twinkled amusement as he recalled this anecdote. "I wonder to this day if she had any notion of who I really am."

"Would it have mattered?" It meant nothing to Liebenthal, save that he was grateful to have more than a single shot to fire if this was indeed to be his last stand.

"I hope not." Magnin chuckled and sipped his drink again. "Such audacity is rare. Despite my efforts, humans retain an inborn tendency to defer to those whose status appears higher than their own."

Liebenthal remained silent. Despite the other man having hurled him across the room without touching him, he did not believe any of Magnin's talk of experiments or of efforts to improve humanity. Such speech struck him as pretentious; it was as if Magnin claimed to be an immortal of some kind, capable of tampering with human evolution and willing to do so for ends he would not bother to explain to mere mortals. Even his snapping the fingers just before the lights came on was nothing but showmanship; he must have known to the second when whatever measures this Claire Ashecroft woman had taken would be nullified, and timed his act accordingly. Surely Imaginos was a proficient charlatan, and nothing more.

They sat in silence for time. When the glass was empty, Magnin placed it upon the desk and rose from his seat. "What will you do, Mr. Liebenthal? If you surrender, I can arrange for you to appear to have died in custody. In truth, your health will be restored, your life will be extended, and you will be given a new identity and the means to begin a new life of anonymous opulence far from here."

Liebenthal snorted. "How Faustian of you."

"You do me a disservice. You may also stand against Stormrider. If he takes you alive, I am prepared to treat you as if you had voluntarily surrendered. However, I may yet be wrong about him."

*He's talking about his experiments again.* "Doesn't that asshole have orders to take me alive at any cost?"

Magnin smiled, and adjusted his cravat before buttoning his greatcoat. "Those were his original orders. He has since received new orders. The manner in which he acts upon them is the final test. He has the authority now to kill you, if he deems it necessary. His choice will provide valuable insight into his character."

Liebenthal bridled at the nonchalance with which Magnin spoke of the possibility that Morgan Stormrider might kill him. He raised his rifle, and sighted upon the man through the scope, which automatically adjusted its resolution to provide just enough magnification to compensate for the short distance between them. "Don't you realize that if Stormrider kills me, he'll prove the truth of everything I've said about the Phoenix Society?"

Laughter erupted from Isaac Magnin. "You poor, foolish human. People have questioned the Phoenix Society's legitimacy since its inception, but those who question are invariably shouted down by their fellows."

"Because they believe your propaganda!" Liebenthal squeezed the trigger, not realizing the safety remained engaged.

"Results are their own propaganda. The vast majority of the people are free, prosperous, and safe under the Phoenix Society's rule. They do not care if the likes of you are put to the sword."

With the safety disengaged, Liebenthal's lips stretched into a rictus grin as he adjusted his aim. "Get the fuck out of my office."

All signs of mirth and amusement fled Magnin's face. "You presume to command me?"

"I thought you *admired* audacity in humans. " Liebenthal squeezed the trigger, involuntarily squeezing his eyelids shut as he did so. When he opened them again, Magnin stood untouched with three slivers of gleaming metal suspended before him. The man had made no move, spoken no word of command. His hands remained at his side, but the shots Liebenthal had fired did not touch him.

Magnin raised his hand and plucked one of the slivers from the air. A smirk twisted his thin lips as he flicked it aside. The others fell to the floor. "Did you think I would design such a weapon, allow it to be mass-produced by humans, and allow the likes of you to wield it if I had no means of defense against it? Save your defiance and your ammunition for Morgan Stormrider." Before Liebenthal found the presence of mind to fire another burst, or to flip the fire selector to full automatic and cut loose, Isaac Magnin disappeared from sight.

\###

