There was no privacy in the women's dormitory the people who had bought and outfitted the safe house made of the bedrooms on this floor, and her cot was too narrow for comfort. However, it had been good enough for Naomi to steal a few hours of sleep before dawn. Sarah Kohlrynn had done the same; she could still hear the other woman's soft snoring from the other side of the dorm. She sat up, and tried to reach both over and under her shoulders to get at an itchy spot in the middle of her back, but she couldn't quite get to it. *I hope Morgan's awake; I could use a good scratch right now.*

She rose as quietly as she could, lest she wake Sarah, and retrieved a fresh set of clothes from her footlocker; if she was going to be awake, she might as well be ready for the raid; once the sun was up, Morgan meant to move on Liebenthal and arrest him in daylight, so that everybody could see that the Adversaries were no assassins. She found a long-handled bath brush beneath her clothes, and let a sigh of relief escape her lips. *At least I can scratch this damnable itch.*

She found him in the study, seated before a terminal, after she had finished her shower. She padded across the carpet behind him while fluffing her damp hair with an equally white towel, and could not resist brushing aside his own barely dry hair to kiss the nape of his neck. She resisted the temptation to question him concerning the tattoo she found hidden beneath his mane, which read as follows:

> > AsgarTech Corporation: Asura Emulator Project  
> > Model 101  
> > Serial Number: AE101/1010011010  
> > Inception: 6 June 2082 @ 0600 UTC  

She bent to taste his skin and see if he would shudder beneath her kiss. He only managed a sigh, and took her hands in his as she wound her arms around him. "You're up early, Naomi."

"Did you even sleep? You'll be worn out if you're not careful."

"Claire got her hands on Liebenthal's data just as we were getting back. It's all an unstructured mess of documents and spreadsheets, and we only had a few hours before it's time to finish this job, so I thought I'd help Claire get this data prepared for mining."

She nosed his ear. "You should have left this to Claire and gotten some sleep. We need you to be sharp."

An avatar popped up on the screen. Most of her scarlet hair was bound into a French twist; a few ringlets escaped to frame a face too idealized to be Claire's, though Naomi could imagine her putting her hair up if pressed hard enough to dress properly for a formal occasion. Nor did Claire wear glasses. "Actually, Nims, I'll be doing most of the work. Poor Morgan's has just been helping Claire work out the requirements."

"Couldn't you have figured out the data yourself?" Why leave this work to Morgan, when an AI was capable of converting the data taken from Liebenthal's systems without his knowledge to a usable form faster than a human could. *Not that I stayed up to keep him company. I might have brought the man coffee, at least.*

Morgan chuckled. "Astarte refused. She insisted that I work out the relationships since I'm the Adversary here, and therefore have domain knowledge."

"Also, we AIs don't want you humans becoming too dependent upon us. Vomisa Principle Zero applies in this situation." Astarte smiled as she said this, which left Naomi to wonder if she was joking. She knew of only three Vomisa Principles of robotics and artificial intelligence; Dr. Ruth Vomisa formulated them, while designing the Sephiroth, to provide a basis for AIs to develop ethics which would allow them to coexist with people. "By the way, Morgan, all of your logic checks out. The similarity between your suggested database design and Claire's is ninety percent. I can combine your designs, if you wish. Do you want me to start the conversion?"

"Please do so." Morgan threw his arms back while stretching, and let his head loll back. His jaw creaked from the strain of his yawn as he pushed himself onto his feet. "And please prepare a report for me detailing the differences between my design and Claire's."

"Go take a nap. I'll let you know when I'm done." Astarte's animation became a little jagged, as if the conversion process had drawn too much power from her processors and impinged on cycles allocated elsewhere. "Naomi, at least make sure Morgan has breakfast. He has important work to do today."

"Of course." She turned to fetch Morgan and drag him into the kitchen, but he had already curled up on the couch, and settled on his side to sleep. She found a blanket and draped it over him before brushing a kiss against his cheek. To her delight, the kitchen was fully stocked, and equipped with all of the gear needed to prepare a proper breakfast for Adversaries. She ate strawberries while preparing a breakfast of ham steaks, curried potatoes, and scrambled eggs. Giggles escaped her lips as the cat leaped upon the counter and stole a strawberry from the basket to bat around the kitchen with her paws before finally flopping onto her side and eating it. She prevented her from stealing a second; she had no desire to see the resident calico sick from overindulging in treats which cats had not evolved to include in their diet.

Sid yawned his way into the kitchen, wearing only a pair of drawstring trousers and a ratty old Herakles Gym t-shirt; he smelled of fresh sweat, as if he had just finished working out downstairs. "Mornin', Nims. You planning to eat all that?"

"Of course not. There's enough for everybody. Be a dear and wake everybody up, Morgan last. He's on the couch in the study."

Sid slowly shook his head. "I told him to just let Claire deal with it, but does he listen to me?"

"You're not in his chain of command." Cohen slapped Sid's shoulder as the big man slipped past him to leave the kitchen. He glanced at the food. "You've been cooking, Nims?"

She turned over the steaks and checked the eggs before stirring the curried potatoes. "It's obvious?"

"I smell curry. It wouldn't have occurred to Sid to use any sort of spices. Where's the rosemary and garlic?"

"I put it on the ham steaks. Please help me serve?"

Naomi was unable to catch Morgan alone again until after breakfast. The others were happy to eat her cooking, and ask in vain for a second helping, but none wanted a hand in the cleanup. Morgan, at least, had offered to wash the dishes and leave the drying to her. "Thanks for breakfast. I needed that."

"We all did. Who told you my hands tend to crack if I spend too long washing dishes?"

A concerned frown creased Morgan's brow as he stopped mid-dish to study Naomi's hands. "I had no idea." He continued to wash dishes, passing each piece to her in silence, until he had reached the large skillet in which she had done the scrambled eggs. As he set to scrubbing it, he glanced at Naomi. "I was wrong to obey you last night. If I had fought empty-handed, I could have incapacitated Riordan without risking his life, by applying an arm-lock and dislocating his shoulder. If we had not gotten to him in time, Riordan would have bled out."

She remembered the speed with which Riordan moved once he had committed himself, and the fearful trajectory of his blade as he lifted it high to cut downward. Without the rapier he borrowed, Morgan would have died with a cloven skull. *This is his guilt talking.* "Riordan would have killed you. Do you really think you could have stepped aside in time?"

"I've done it before." The nonchalance with which he spoke revealed the truth of his words. The plate she had been drying slipped from her fingertips. She expected to hear it shatter upon the floor, but there was no such sound. She glanced down at her feet, but the floor was clear. "Look on the counter."

Naomi stared at the counter; the one she should have finished drying, but had dropped, sat in front of the others. She stared at her man. "How did you do that?"

"I call it 'demon speeding'. Sometimes, if I'm in the zone, it feels like my own progress through time is slower than that of the world around me."

*Can he do it again?* The question was a perverse impulse, despite having been friends with Morgan for a decade, and wanting him for a lover, she still knew little about him. She had always believed that he was just a man with CPMD who took foolhardy risks as an Adversary because the preternatural speed with which he healed lent him a false sense of security. Seeing the tattoo cast him in a different light, but Naomi had no intention of treating him differently because of some text inked into his skin. *Even if he is some kind of war machine, he's been a friend to me and the others. He did his best to be the lover Christabel wanted. I can taste his love when he kisses me, and feel his hunger pressing into me when he holds me close. Asura Emulator or not, he's still a man.* Despite this, her curiosity demanded she test him. An impish smile curved her lips as she pushed the plate from the counter. This time, he lunged forward and caught the plate by getting his hand under it just before it struck the floor. "I saw you move this time."

"It's unreliable." Morgan stacked the plate atop the others before drawing her into his arms to prevent further mischief. "I have to be in a flow state, or it doesn't happen."

For some reason, she imagined Morgan as a child, shocking his parents as he assembled a puzzle in what seemed an instant. "Were you always able to do it?"

"No." He shook his head. "It started after Christabel dragged me into Crowley's Thoth, after I met you again. I thought I had gotten over my adolescent infatuation with you, but I was wrong." He began to redden, as if embarrassed, and turned away from Naomi. "I wanted nothing more than to impress you, and thought to do so by taking my guitar playing to the next level."

"Am I the key to you reaching a flow state?" The notion terrified Naomi; she had always believed Morgan wholly independent, a man who needed nobody, but befriended her and the others for the simple pleasure of their company. If he needed her in order to reach his full potential, such dependence threatened to distort their relationship.

"Only when I hear your voice." He kissed her as he answered, slipping past her to clean the kitchen counters. "Otherwise, any complex piece of music will do, but I do prefer progressive metal."

*Of course he does.* She had helped him reshape Crowley's Thoth into such a band once Christabel had brought them aboard her pet project. Naomi needed little time, despite the distracting warmth lingering upon her lips, to understand the implications of Morgan achieving a flow state using music. "If you can use music to enter a flow state, and do that demon-speeding trick, you could have taken on Riordan without a weapon."

Morgan opened the cabinet beneath the sink and placed the used washcloth in a bin labeled 'Washcloths to Launder'. "I could have taken all of the Fireclowns without a weapon, regardless of their armament."

"Then why did you borrow a sword when I told you to do so? Was it because I didn't know?" Naomi hoped this was the case, but feared his answer would be that he did so out of love for her and a need to protect her peace of mind.

Morgan poured mugs of coffee before answering. "I needed the Fireclowns to see me defeat Riordan in a fair duel. I dared not use any sort of preternatural ability while facing him. I would have fought empty-handed, using only my natural strength and agility." Naomi nodded as she sipped her coffee, which was as black as the interior of a gun's barrel, but possessed of a hint of sweetness. These were logical, pragmatic reasons, and Naomi could detect no defect in his logic. She prompted him to continue by placing her hand on his, but was taken aback by the intensity with which he gazed into her eyes. “You’re right. The chances of my walking away from such a fight unscathed are somewhat limited.”

“You really think you’d survive having your head cloven in two with a sword?” She instantly regretted the question, for the answer would surely be a variation on ‘I’ll get over it’.

Instead, he took her hand. “I’m glad we didn’t have to find out.” Before he could go further, a burst of profane raving from outside the kitchen filled the air. Sarah burst into the kitchen. "Hey, Romeo. Del Rio's on the screen in the parlor, and she is *pissed*."

\###

