Somebody had muted the screen from which Karen Del Rio, one of the directors of the New York Chapter's Adversaries, raved and pontificated. Instead of listening to her tiresome abuse while waiting for her to arrive by the most circuitous possible route at her point, Morgan had the luxury of being able to settle onto the couch and watch the real-time transcription of her blithering scroll across the screen. He had the room to himself; according to Sarah, Karen Del Rio wanted to speak exclusively to him. *That's probably just as well,* he thought as his flint-eyed superior dragged Naomi into the matter by accusing him of bringing her along as a morale booster. *Morale booster? That's almost delicate.*

He turned on the screen's speakers. "-- you arrogant long-haired bastard! Did you think you were being *cute* by taking the Fireclowns away from Liebenthal? While you were having yourself a leisurely fucking *breakfast*, he got himself another army! Instead of five hundred Fireclowns, he's got a couple thousand men from every biker gang in Boston but the local Hells Angels! What in the name of hell's asshole have you got to say for yourself?"

Morgan shrugged, but did not rise from the couch. There was little point, since he too far away to slap Karen Del Rio's petulant face, and had never yielded to the temptation to do so when she had offended him in person. Though he had no love for his foster parents, they taught him better than that. "This is why I usually just kill everybody who gets in my way, and kill the suspect if he offers any resistance. However, you wanted me to do the job without killing anybody. Rather than tear the throat out of every Fireclown who stood between me and the target, I bribed them to abandon him. I fought a duel to first blood with their former leader which allowed them to get out of their contract with Liebenthal while still abiding by it. I thought, based on the psychological profile provided with my instructions, that the sudden loss of his army would cripple Liebenthal and allow me to take him out this morning."

"You should have taken him last night." Morgan loved this quality in Karen; she was ever ready to tell him what he should have done, despite having never worn an Adversary's pins herself. He had tried to take plenty of suspects at night. They all ended up dead by his hand. He forced his fists to loosen, and kept his voice cool. "Last night, Del Rio? Don't tell me you haven't heard Liebenthal's speech. The bastard stopped just short of calling the Phoenix Society a totalitarian regime and comparing the Adversaries to such a regime's secret police."

"That's just *rhetoric*, you idiot. Nobody takes him seriously!"

"Nobody at all?" Morgan recalled the defaced recruitment poster he and the others passed on their ride to The Four Winds Bar. It was hardly the first of its kind he had seen. The posters themselves seemed designed to appeal to primitive emotions: a desire on the part of the prospective recruit to protect his fellows against a faceless other bent on oppression, and a desire on the part of the recruit's family and friends to look up to those who shouldered this martial burden. His foster parents had taken him to the Museum of Modern Art several times as a child, but most of the exhibits bore long names suggesting a target demographic of scholars, rather than ordinary people. He had seen one before leaving home which was different; the title was a single word, 'Propaganda', and considered entirely of military recruitment posters from every country which had existed in the last century or so. The centerpiece of the exhibit, however, had been a series the Phoenix Society printed not to recruit men and woman into its ranks, but to persuade people embittered by Nationfall. Each bore a stylized portrait of a different Adversary extending an open hand, people of every color and gender, and bore the same message: "To serve is my privilege". The word "LIAR" had been spray-painted across each Adversary's face.

The media had likewise become more insistent when questioning the Phoenix Society, to the point where Adversaries given standing orders to refer all questions to the PR department began to define PR not as an acronym for 'public relations', but as an abbreviation for  'propagandists'. He shook his head and pushed such thoughts from his mind. If Del Rio wished to blind herself to the truth concerning public perception of the Phoenix Society, there was little Morgan could do about it. "Even if you're right, and people don't doubt the Phoenix Society, do we _really_ want to sow the seeds of doubt by acting in a manner consistent with his allegations?"

"You sound as though you believe it yourself." Del Rio pursed her already thin lips until they disappeared into a flat line. "I think you're trying to distract me from the fact of your incompetence. You should have taken Liebenthal when you had the chance. You had all night."

"And look like secret police serving a totalitarian regime? Why do you continually force me to repeat myself?

"I told Saul you weren't man enough for this mission. I knew it as soon as you passed orders to Eddie and Sid to rescue the Boston Chapter's remaining staff without killing any of the Fireclowns." A hard, triumphant smile cracked Del Rio's features as her flinty eyes glinted. She leaned forward, and lowered her voice. "I was wrong, Morgan. It wasn't incompetence on your part, but *cowardice*. You're *afraid* to draw your sword around Naomi. You're *scared shitless* she'll hate you once she sees your joy at the taste of blood, just like Christabel did."

Morgan nodded. She had struck close to the truth without realizing it. "If I fear anything, it is the consequences of leading others into a situation I don't fully understand. At risk of repeating myself yet again, the information I was provided suggested Liebenthal was just a gun-running produce wholesaler who had managed to hire a revenant ex-Adversary and a gang of mercenary bikers. He should not have been able to stage a coup, nor have had the nerve to call a press conference in which he quoted Kennedy and accused us of fascism. He should have run out of money trying to keep the city running. What the hell is *really* going on, Karen? Who are this guy's backers? Does Munakata have him on strings?" 

"All I know is that Liebenthal got more money from a backer we cannot identify." Del Rio began darting glances around her, as if looking for a way out. He narrowed his eyes as a question intruded upon his consciousness. *Why is it just Del Rio chewing me out?* "Where are Saul and Iris? Are they aware of the situation?"

Del Rio's shifting eyes hardened from flint to diamond. "They don't need to know about this. I'm doing you a favor here."

"Not likely." He disconnected, and used secure relay chat to call a meeting. Naomi was first to arrive; her armored figure had yet to pall, and a sensation resembling a static charge began to build in his nerves as she took the other side of the loveseat. Instead of speaking, she sidled close to him and took his hand in hers while letting her sword lean against the cushion. Sid arrived next, his face made fearsome by the leather thong binding his dreadlocks so that they flowed down his back. Rather than a sword, he wore half a dozen knives; he was less likely to break them than he would a sword if he struck with the full extent of the prodigious strength his bodybuilder's physique lent him. However, Morgan had never seen him go all out except when lifting. Eddie and Sarah came together; the old soldier was incorrigible in his taste for women young enough to be his granddaughters, or even great-granddaughters. Sarah fit the bill, being the sort of tall, leggy blonde with which Morgan usually saw Eddie, but he had seen her in action via Witness Protocol; it was probably the closest thing Eddie had ever had to a relationship between equals. Now that everybody was here, Morgan saw no reason to dance around the subject. "I owe you all an apology. I had assumed, based on what the Society knows about Liebenthal, that once we got the Fireclowns out of the way we could walk into City Hall, arrest Munakata, and then arrest Liebenthal. I was sure we could have this done in time for dinner. I was wrong."

Naomi tightened her grip on Morgan's hand. "What happened?"

"According to Del Rio, Liebenthal has a backer. Instead of being all but broke, he has enough money to keep running the city, and to hire and equip the rest of the city's biker gangs. Instead of five hundred men, he has over two thousand."

"Bribing their leaders is probably out."

Eddie nodded, and jerked a thumb towards Sid. "The big guy's right, Morgan. You got away with extending an open hand because the Fireclowns expected a fist. It won't work a second time."

"We need to be able to match Liebenthal's forces. Could we start by hiring the Fireclowns?"

"I think it's worth the attempt." Naomi squeezed Morgan's hand. "Have you already considered the notion?"

Morgan nodded. He had considered an attempt to hire the Fireclowns, before setting foot in The Four Winds Bar, and rejected it out of hand. If the Fireclowns were willing to take a bribe from Morgan to repudiate Liebenthal, what was to stop them from taking a bigger bribe to resume their old loyalties? Furthermore, while Michael Riordan had understood the unsustainable nature of the relationship between Liebenthal and the Fireclowns MC, his younger brother Roger seemed more grasping, and had been ambitious enough to arrange a duel between Morgan and his brother which would give the Fireclowns a way out of their contract which adhered to the terms, while giving him a shot at leadership until the next election. "I'm not convinced the Fireclowns can be relied upon."

"Call out the militia." Everybody stared at Eddie. Every man and woman of military age was expected to train with small arms and answer a call to arms should the city government request militia support to bolster the strength of police officers, who were forbidden by law and by the Phoenix Society to carry weapons. "Liebenthal probably guessed that you'd hesitate to ask us to follow you against two thousand men when we're under orders to refrain from killing. Why not bring the entire city to bear against his two thousand?"

Morgan considered the numbers. The city of Boston had a population of over a million, of which at least four hundred thousand were of military age. "I don't have the authority to issue a call to arms."

"You don't need a call to arms." Naomi's voice was soft, and her eyes luminous as she spoke. "Would any of Liebenthal's men dare fire upon a mass of thousands of unarmed people gathered to demand Liebenthal's surrender? What's to stop you from organizing a non-violent demonstration against the new regime?"

Sid's fist thudded into his palm. "Naomi's right. Besides, why the fuck is it *our* job to take back Boston? The people of Boston should help take back their own city."

Sarah stared at Morgan for a second, before shaking her head. "Have you lost your mind? I've read your dossier. You're no diplomat. Your strengths lie in unconventional warfare."

"You're out of line." Naomi stood, raising her voice in Morgan's defense, but stopped when he held up his hand. He texted her: “She’s inexperienced. Let me explain things to her.”

As Naomi resumed her seat, Morgan met Sarah's eyes. She stared directly at him, her arms crossed as if expecting him to raise his own voice. He instead lowered it. "Adversary Kohlrynn, it is not constructive to view the situation in terms of warfare. Liebenthal is guilty of multiple individual rights violations, and must be removed from power and brought to New York to stand trial. The people with whom he has surrounded himself are, with the exception of Munakata Tetsuo, guilty of nothing more than being in the way. Viewing the situation in martial terms will lead only to bloodshed, which serves no one. With enough people behind us, we can create a situation where Liebenthal’s men would be insane to do anything but lay down their arms and walk away.”

Sarah's expression hardened as she shook her head. "I still think you’re nuts." She grasped Eddie's shoulder to get his attention. "This wasn't what you had in mind, was it?"

"No, but I like it." Eddie stood, shrugging off Sarah's hand. "You're going to need to hold a press conference."

Morgan nodded. "I know. How about the Boston Chapter?"

"Sounds good. Sid and I will make the arrangements. Have you given any thought to what you'll say?"

"I have a speech roughed out." To persuade the people of Boston to rise up and demand Liebenthal's surrender, he need the full extent of his eloquence, which he had rarely used once he was graduated from ACS and accepted into service. The last argument of kings had been the only argument he needed to offer for the last ten years; the oratory of violence had its own rhetoric, one of swords clashing and pistol reports echoing into the void to which he consigned his targets' minds. He would have to present the facts, and make the people understand, before suggesting the appropriate course of action. He rose, and offered his hand to Naomi. "Could you help me refine it?"

\###

