## Chapter Forty-Four

Sergeant D'Amato did not answer me straight away. Instead, he favored me with a long, slow appraisal. His eyes lingered on me until my skin began to crawl. An urge to launch myself at him, drawing my sword mid-leap, rose within me. "So, you're Naomi. I left you for Renfield so he could enjoy you one last time before we made vulture bait of you. Guess he doesn't understand who's in charge here."

Renfield bristled at that, and would have attacked had I not raised my arm to forestall him. "It'll be a cold day in Hell before a Sergeant Major takes orders from a Staff Sergeant in *this* man's Army."

D'Amato's contemptuous snort told me this had been a long-standing rivalry between the two men. "It hasn't been this man's Army for a long fuckin' time, Chris, and you goddamn well know it. We're not Commonwealth Army any longer. There's no Commonwealth, remember? We're Dusk Patrol now, and it's us against everybody else. They made monsters of us, and then abandoned us, remember?"

"Naomi didn't make vampires of us and then abandon us to survive in a world we no longer understood. Brubaker didn't, either. Even those kids who broke in on a lark and murdered some of us in our sleep aren't guilty of *that* crime."

"Vampires!" D'Amato glanced at his men. "You hear that? Renfield still thinks we're some kind of mythological bogeyman."

Not that I didn't know all along that the men of Dusk Patrol weren't actually vampires, but I still had no bloody idea what the hell they actually *were*. Maybe it was time I got a word in edgewise. "If you aren't vampires, Sergeant D'Amato, then what are you? Renfield mentioned something about Project Harker. My superiors are also aware, but refuse to tell *me* anything. Whatever it is, it seems to only involve people with CPMD."

"You really don't know?" D'Amato chuckled. "We're *asuras*, little soldier girl. We're gods among men, every one of us! It took Project Harker to unleash our full potential. Let me show you."

D'Amato struck, drawing one of his stolen swords and advancing upon me with a swiftness I had previously only seen while training with Maestro. That training took over. I stepped aside to let D'Amato pass me, and my sword bit deep as I pierced his heart from behind. "Was that all, Sergeant?"

D'Amato wheezed, and blood so deep a red it was almost black leaked from the corner of his mouth. He straightened, took a deep breath, and licked the blood from his lips. "So, that was how you got Reeves. Maybe you too were awakened without realizing it. I daresay the Colonel would love to have *you* for breeding stock. Just think of the asuras we might spawn."

"You may think about it if you like. In my opinion the subject barely merits the consideration necessary to reject it out of hand."

My rejoinder must have amused some of D'Amato's men despite themselves, for fleeting grins flashed across some of their faces. You'd think they'd do more than just listen to me exchange my poor excuse for witty repartee with their leader. "I'm surprised your men haven't leaped to your defense and beaten me into the ground, Sergeant. Did you want me all to yourself?"

Brubaker coughed, and gently tapped the 40mm grenade launcher mounted under the barrel of the Nakajima carbine Renfield must have given him. "First of you fuckers to take a step forward gets to meet my good friend Willie Pete."

D'Amato snorted. "You ain't got the balls, kid."

Without a word, Brubaker proved him wrong. He squeezed the launcher's trigger with the deliberate care of a trained Adversary. The grenade shrieked across the room, and struck one of the men with a blinding flare of white flame and a sudden heat that drove me back as the other men battered one another in their blind desperation to retreat. The soldier Brubaker shot roasted from the inside out, unable to even scream. He must have seen the round coming straight for him so quickly he had no hope of escape, must have opened his mouth to scream his despair, and ended up with a white phosphorous round lodged in his mouth.

Steel clattered against the concrete floor as D'Amato dropped his stolen katana and stared at the charred remains of what had once been one of his brothers in arms. "Jesus Christ, kid, he didn't even make a move." His voice trembled as he collapsed to his knees, overcome by horror and a grief I didn't think him capable of feeling. Not that I blamed him; I was too busy forcing my gorge back down. "Chuck McMillan and I came from the same town, and joined the Army together straight outta high school. We went to boot camp together. We got our first tattoos together. Hell, we even picked up our first girls together. That was my best friend, and you snuffed him just to prove you were hard enough to take us on."

Brubaker's response ended any hope he might have that I'd recommend him as an Adversary. He calmly loaded a fresh round into his launcher, and leveled it at D'Amato. "You want to join him? Just open your mouth again, and I'll do you like I did your jerkoff buddy."

Before I could do it, Renfield slugged the kid and wrested the carbine from him. "Enough, kid. What the fuck happened to you? Last time you were down here, you were speaking against violence. Now you're talking like somebody who's seen too many shitty action movies."

"Oh, and we're not living out a shitty movie right now?" Brubaker rubbed his face where Renfield's punch had connected. "You fuckers have been killing people ever since we resettled Clarion. And not just killing them, but fuckin' torturing them like you did Scott and Charlie. Those were *my* friends, you bastards!"

"And where the bloody hell does it end?" They looked at me like I had grown an extra pair of tits, but I pressed on. "Maybe Dusk Patrol has been killing hikers and other unfortunates who wandered too close to Fort Clarion, Mike, and maybe not. We don't know for sure right now. So your friends are gonna get payback by killing some soldiers in their sleep?"

Leaving him to chew on that, I pointed at D'Amato with my sword. "And you, Sergeant. Brubaker's friends killed some of yours, and now you killed them. You were bloody well ready to kill *me*, and all I've been doing is trying to make sense of all this so I can help. What good did any of that do? Seems like it only got your childhood friend killed extra crispy. You think he'd say it was worth it?"

"No." Sergeant D'Amato stared at the floor. "But what the fuck were we supposed to do? We've been at war for decades. We didn't have anybody to tell us the war was over and we could go back to being citizens again. We ain't even got a country to be citizens *of* any longer. We're as dead as the Commonwealth."

"Oh, come off it." Look, I know he just saw his best friend burned to the ground, but the self-pity was getting a bit much. "The Phoenix Society has been finding isolated towns and villages for decades, and helping them rejoin human society. We've even found soldiers like you, old men and women who held their posts decades after they last received orders. We helped them reintegrate into society and adjust so they could live out their lives in peace. We could have helped you. We still can."

My words didn't do much for D'Amato, but hope flared in Renfield. "Naomi, are you serious? We could have gone back to the world decades ago? But what about Colonel Petersen? All this time he's told us we had to maintain our post and stay hidden, that the world couldn't know we existed. Why would he lie to us like this? He always looked out for us, goddammit."

That was the hard part.
