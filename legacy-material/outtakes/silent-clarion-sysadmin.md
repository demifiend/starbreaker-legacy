Matt Tricklebank's shop was empty, but the man himself was behind the counter wearing a jeweler's loupe while taking a very small screwdriver to an old smartphone. Since it looked like a delicate operation, I waited until he had finished turning the last screw. "Excuse me."

He straightened, which made the slogan on his t-shirt visible. It read, *Warrant or GTFO*. "What's up, Adversary?"

I texted him the access code for my warrant. "Nice shirt. Were you expecting me?"

"It was the first shirt I found in the closet this morning. I figured you'd have to wait until Monday to get your warrant. Did you have one of the Sephiroth crawling the town forum a few minutes ago?"

Bloody hell, how does he find out so quickly? Is he running the town server off of the implant in his head? "Malkuth did that on his own. You're aware that a warrant is not required to view publicly available data on a network site, no doubt."

"That's why I didn't boot him the way I did you." Matt's fingers moved with slow deliberation as he took apart the old smartphone. "But the server is rigged to send me alerts when system load spikes, or when somebody attempts to brute-force a database."

He gave me a pointed look that I didn't need as he finished his explanation. I never did understand how he detected my attempt to crack his database. "HermitCrab's penetration tools are supposed to be undetectable. How did you know I was breaking in?"

Matt shook his head in mock exasperation. "I'm one of the developers, remember? I know how those tools work, and I was able to devise countermeasures. To be honest, I should thank you. It's one thing to test those countermeasures in a controlled environment, but getting real-world results is a bit harder."

"Well, I'm glad I helped you with your experiment, but your explanation raises another question."

"No, I didn't make my countermeasures available to the general public. Access to the source code is limited to individuals whose trustworthiness I personally verified. They aren't fully tested yet, and releasing them as free software would make them accessible to criminals."

Does he look at his wife with such contempt when she asks a question he considers moronicly obvious? For her sake, I hope he's more sensible than that. Fortunately, I'm not his wife. "Then why create them?"

"You want the Devil's honest truth?" His bright smile and sudden lighthearted tone combined with the manual dexterity I witnessed earlier helped me understand why Cat tolerated his presence in her life. "You guys would be fucked if a criminal got their hands on HermitCrab and used it against you. I figured I'd sell you guys a defense against your own tools and extort every milligram I could in exchange."

There was a motive I could understand and respect. "At least you're forthright about your greed. So, are you going to give me access to the forum?"

"I can do a bit better than that if you give me twenty minutes. Have a look around the shop if you like. You can tinker with some of the machinery, but if you break it you bought it."

"Does your wife know you're a mercenary little shit?"

"I warned her about all of my physical and moral shortcomings in great detail before I proposed to her. I wanted her to understand she was agreeing to marry a money-grubbing asshole with a needle dick before she committed herself."

What kind of man admits he's less than generously endowed? Is he taking the piss out of me? He probably is, the smarmy little bastard. "And she married you anyway? Was she crazy?"

"It was a short list." He ducked into the back, leaving me to explore.