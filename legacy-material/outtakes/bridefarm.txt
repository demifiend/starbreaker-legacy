﻿"One of my missions involved a bride farm, where captive women are impregnated, and then subjected to abortions if the fetus turns out to be male. The baby girls are then taken from their mothers, and inspected for defects. If they pass inspection, they grow up indoctrinated to be obedient wives eager to please the husbands to whom they're sold as soon as they turn eighteen."

Nobody asked what happened to the girls who failed inspection. Naomi suspected their fate was obvious from her expression and tone of voice, because she witnessed what befell one such unfortunate herself. "I left, and came back with a dozen other Adversaries, all women. I thought we'd be able to restrain our anger if we worked together. Instead, after we liberated the captives, we put everybody working there to the sword. We burned the bride farm to the ground after salvaging the records and documentation. We hunted down the customers, arrested them, and tried them. None were acquitted. I hear most died in prison, at the hands of their fellow inmates."

Naomi fell silent and stared at her hands, unable to fathom why she let this story escape her lips. She had kept it to herself since her post-mission debriefing, and the subsequent trial by court martial. Judges left pale and trembling by the evidence recorded through Witness Protocol acquitted her and her fellow Adversaries. "I'm sorry. At the time, my only coherent thought was that what I saw was unforgivable."

Morgan's hand grasped hers. "I would have done the same in your position, with pleasure."

She found Morgan's expression on everybody else's faces, as well. "You all agree with Morgan?"

Sid was first to show his assent. "Come on, Nims. You think I'd want something like that to happen to Elly, or my girls?"

Astarte's voice was small, and quiet. "What happened to the women?"

"There were a lot of suicides, despite the Phoenix Society's efforts to give everybody the care they needed to heal." Naomi stared at the floor, recalling the names of those she proved unable to save. "Sometimes I get letters from the others.