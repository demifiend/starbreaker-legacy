# Meeting the Sheriff's Mother

The following is an outtake from chapters 35 and 36 of _Silent Clarion_. Naomi Bradleigh and Sheriff Robinson are discussing the recent burglary of his house, during with his prized hunting bow and (blunt-tipped) small-game arrows were stolen, along with other items like a Solaroid camera. Robinson still doesn't have a suspect, until Naomi asks how the burglar got in.

With no signs of forced entry, the perp was most likely somebody with access to the home. Robinson admits that he entrusted twelve people with keys to his house, because his aged mother has an incurably advanced case of Alzheimer's disease, and sometimes forgets her keys when she goes walkabout.

Naturally, that gives Naomi twelve suspects. But to narrow it down, Sheriff Robinson suggests that Naomi question his mother. It doesn't go down the way either of them expected...

---

"Crap. You're right." Robinson checked his watch. Talk about nostalgia. "When did you agree to meet Chambers and Brubaker?"

"An hour ago, but they don't mind waiting. No doubt Kaylee's already run up a nice big tab for me at the bar." Hopefully Michael had the sense to get dinner instead of just shooting pool. "What have you got in mind?"

"I think it's time you met my mother."

"Why, Sheriff Robinson!" Sure, he just wanted me to help ask her some questions, but I honestly couldn't resist. "What will she _think_? Aren't I a bit young for you?"

He's no fun. He just shook his head. "Come on, Adversary."

We found Mrs. Robinson at home in her rocking chair, cursing over her knitting and pulling out a stitch gone awry. She looked up as we came in, her eyes bright, and made to spring from her chair. "Did you get leave, Robert? I wish you had told me so I could have made a nice dinner for you."

Before he could say anything, Mrs. Robinson sighted me beside him. "And who's your new girlfriend?"

"Ma, I'm not in the army any longer." Robinson helped her mother put her knitting aside, but she got out of her chair and hugged him tight without his assistance. "I'm Clarion's sheriff now."

Confusion showed in her eyes for a moment. "Oh, that's right. I remember now. But aren't you going to introduce your new girlfriend? This _is_ why you brought her over, isn't it? Are you two serious yet?"

"It's not like that, Ma. Agent Bradleigh is here on business."

"That's right." I offered Mrs. Robinson my hand. "I'm Naomi Bradleigh, from the Commonwealth Bureau of Investigation. I was here on vacation for the music festival, but Sheriff Robinson somehow found out what I do for a living and roped me into a consulting gig."

Her memory might be fucked, but she was pretty sharp otherwise. "You don't sound like an American. When did the CBI start hiring Brits?"

"Oh, bloody hell. I was supposed to keep this to myself, but I'm part of an inter-agency exchange. CBI sent one of their agents to London, and MI-5 sent me in exchange." Why compound one lie with another? It's simple. If somebody sees through your bullshit, you didn't lay it on thick enough. Just slap on another coat.

Or two, since Mrs. Robinson was now interested in my sword. "It's an experimental initiative. CBI has caught some flak about excessive force lately. Seems suspects don't find our service pistols sufficiently intimidating, so we have to shoot too many of them. They're not used to having a cop put a sword to their throat while reading them their rights, so they think twice about starting shit."

"Is that the real reason, Adversary?" There was a crafty glint in her eye as she subtly emphasized my rank. A glance at the Sheriff suggested he had missed it, or was too used to thinking of his mother as all but helpless because of her condition.

Fine. I'll play her little game. I'll tell the old bat what she wants to hear, and deal with the inevitable complaints about the impossibility of grandchildren. "I'm sorry I lied, Mrs. Robinson. We didn't want to admit we were having an affair, because Robert knows how much you want grandchildren."

"Then maybe he should date a proper woman who can give him children." She glared at her son. "Or are you sticking to catgirls because you can go bareback without having to worry about knocking them up?"

"I suppose that's one advantage to being old and senile." I couldn't draw my sword on this decrepit harpy, but if she wants to show me the edge of her tongue I'll bloody well return the favor. "It lets you say the first idiotic thing to spring to what's left of your mind while everybody around you just swallows their embarrassment because you somehow can't help it."

"Naomi -" Robinson reached for me, as if he meant to usher me out of the room.

Grabbing my sword as if I meant to draw, I met Robinson's glare. "You stupid git. She's been playing you. If she has Alzheimer's, then I'm the fucking Queen of Britannia." 

Turning back to his mother, I took a step forward. She backed away, and ended up in her chair again. Conveniently enough, that let me look down on her. "As for _you_. I am going to ask you some questions."

"Why should I answer?" 

"Your son is trying to stop a murderer, and _I_ am trying to help him. The sooner you answer my questions, the sooner you and your son can have a long-overdue talk."

"And what if I can't remember?"

"I think you _can_, which is why I will march you to the jail at swordpoint on an obstruction charge if you give me the slightest cause to think you're bullshitting me." I still didn't want to draw on this old woman, but I still needed to intimidate her a little more. "Everything I see and hear is recorded and transmitted to the Phoenix Society in real time. I've got ten AIs listening in, ready to analyze your every word and tell me when you're lying."

Sheriff Robinson held his silence and his rising temper until I was done grilling his lying bitch of a mother. Once we had left his house, however, his voice was all the warning I needed. "Adversary? We need to talk."

Turning toward him, I stepped back so that the sucker punch he hoped to land caught nothing but air. "What's wrong, Sheriff? Going to use me as a punching bag because your lying bitch of mother conned you all this time, and you haven't got the backbone to throw her arse out?"

"What the fuck is wrong with you?" He all but shouted the question, heedless of the fact we were out in public. "Why do you have to dig into everything? Why can't you just leave well enough alone?"

"Were I the type to leave well enough alone, I'd be worthless as an Adversary." My voice held a note of compassion I didn't currently feel for Robinson, not after he took a swing at me. "You knew all along, didn't you?"

"Of course I did, you stupid little girl!" He didn't swing at me again, but he _did_ get in my face. His spittle sprayed my cheek as I turned away. "Did it ever occur to you I might have had a reason for playing along with her game?"

Since I didn't really care, I shrugged. "I figured you had a reason. I just didn't care, since you couldn't be bothered to tell me ahead of time." That was what really got on my tits. I've done everything I could to refrain from undermining this arsehole, and he can't even be straight with me? Before I could stop myself, I had grabbed him by the collar and pressed him against the trunk of an gnarled old oak that shaded half the front yard. "Listen to me, you stupid son of a bitch. If you want me to work with you, instead of working around you, then I have to be able to trust you. I can't do that if I have to analyze everything you tell me and wonder what you _aren't_ telling me."

"Get your hands off me."

I complied, took half a step backward, and then slapped him across the face. The impact was a whipcrack in the cool evening air, and it seemed to stun him. "I don't think you understand your position, Sheriff. Your mother has spent the last twenty years or so defrauding the people of Clarion, and you abetted her. I could nail you along with your mother, but not only are you two not worth my trouble right now, but you might still be of use to me if I let you keep your badge. So I'm going to make sure you know your place by explaining it in the simplest possible terms."

Pausing for effect, I jabbed his chest with my fingertip. "You're my bitch until I finish with this godforsaken town. I bloody well _own_ you. Not Dr. Petersen. Not Mayor Collins. Not your mother. _Me._ So when I say 'jump', the only thing I expect to hear from you is, 'How high, Adversary?'" 

Robinson nodded, and I couldn't see any of his former anger in his expression. "Yes, ma'am. I'm sorry I took a shot at you, ma'am."

"All's forgiven." Forgiven, but not forgotten. Still, we had work to do. "Any questions, Sheriff?"

That smirk was a bad sign. "Did you pay for your schooling by working as a dominatrix, by any chance?"

No, but that didn't stop a perverse corner of my imagination from supplying an image of me encased in black leather with spike-heeled boots and a riding crop tucked under my arm. "Sorry to disappoint you, but that slap across the chops was a one-off."