With the discovery of newly-manufactured Gauss rifles and evidence of rampant worker exploitation, and Morgan's success in breaking Murdoch's electronic security from within, all that remained was to arrest the executives. Though this was familiar work, Naomi dreaded it for a reason that descended to meet her and Morgan.

She stared into Murdoch's only working elevator as its tarnished maw ground open to admit her and Morgan. The mirrors lining its interior were milky with age and neglect. She set a foot inside, and her bootheel adhered to the floor with a faint squelch. Worst of all was the Muzak emanating from a tinny old speaker within. The Crowley's Thoth anthem sanitized for a corporate setting was an insult.

Naomi withdrew, and glanced at Morgan. "You can ride that death trap alone if you like. I'll take the stairs."

Morgan shook his head. "We'll be safer if we take the stairs together."

They found the stairwell, and recoiled at the stench as they opened the door. A cloud of fruit flies rushed toward them, eager to escape, and Morgan slammed the door shut to contain them. Naomi suppressed her nausea with an effort. "They must have been dumping their garbage in there for weeks. It's a fire hazard and a biohazard combined."

"I don't like this at all." Morgan punched the elevator's call button. "If we possessed evidence tying Imaginos to Murdoch Defense Industries, I'd say he instructed Murdoch to make working conditions as egregious as possible to keep us busy."

She recalled the information Sid passed up from the factory floor as they boarded the elevator. Despite the reluctance to blame everything on Imaginos she shared with Morgan, she could imagine only one other explanation. "Maybe sanitation and humane treatment of workers just isn't profitable enough?" 

"You're right. This isn't the first sweatshop we've had to shut down." Morgan pressed the button for the top floor, and the elevator lurched upward.

She clung to Morgan for support, glad that he drew her closer instead of questioning her. The elevator protested throughout its creaking, jerking progress up to the fifth floor, until it finally stopped with a barely audible chime that seemed to say, _"No more, please. Have I not suffered enough?"_

The doors shuddered open, revealing a top floor as shabbily decorated as the rest of the headquarters. The furniture predated Nationfall, and many of desks bore scars that suggested they had been salvaged from a landfill, cleaned, and disinfected before installation. The cubicle partitions' cloth linings were faded and threadbare. Naomi scarcely recognized the workers ensconced within them as human; their silent, plodding movements as they carried out their duties made them closer kin to poorly made animatronic puppets.

Naomi's head began throbbing in time with the flickering of the overhead florescent lights as a squad of sword-bearing security guards blocked their path through the cubicle farm to the executive suite. "Stand aside, gentlemen. We brought a warrant, and have found sufficient evidence of malfeasance to arrest Victoria Murdoch."

One of the guards made to draw his sword. His hand stopped, and hung suspended in midair as his sword appeared in Morgan's hand. "How the --"

"Magic." Morgan smiled as he withdrew the warrant from his pocket and displayed it for the guards' benefit. "Trust me when I say Murdoch doesn't pay you and your friends enough to oppose us."

The guard lowered his hand. "You're just going to let us go?"

"Yes, we are." Naomi took the guard's sword from Morgan, and offered it to the guard hilt first. "You're not the focus of our mission."

The guard accepted his sword, and sheathed it. With a gesture, he led the others to the elevator, which opened to let Sid and Eddie out. The former glared at the guards for a moment, silently daring them to defy him. They parted, and let him and Eddie join Naomi and Morgan before filing into the elevator.

Sid stalked toward Naomi with his fists clenched at his sides. His glower was an expression she had never seen on his face before. Though they were all gathered in the office, Naomi used secure relay chat for privacy. [Sid, do you want to be the one who arrests Victoria Murdoch?]

Morgan glanced at Sid, and nodded. [That means Sid's the one Murdoch and her lawyers will face in court.]

A wide, sunny grin cut through the anger clouding Sid's face. [That suits me just fine, Morgan. I'll take this organization apart, and pass along any evidence I find that might implicate Imaginos.]

Eddie shook his head. [Let's not count our chickens just yet. We still have to bust the bitch.]
