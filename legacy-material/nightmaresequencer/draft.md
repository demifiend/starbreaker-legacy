# Nightmare Sequencer

a Starbreaker novel

by Matthew Graybosch

## Also by Matthew Graybosch

**Starbreaker**

* *Without Bloodshed* (2013)
* *Silent Clarion* (2015)

**Serials**

* *Silent Clarion*, Episode 1: "The Geographic Cure"
* *Silent Clarion*, Episode 2: "Always the Quiet Ones"
* *Silent Clarion*, Episode 3: "Life is Short and Love is Over"
* *Silent Clarion*, Episode 4: "Death in a Northern Town"

## Short Works

* "The Milgram Battery" (in *Curiosity Quills: Primetime*)  
* "Limited Liability" (in *Curiosity Quills: Chronology*)

## Disclaimer

The following is a work of fiction. Any views or opinions expressed by the characters in this work are strictly their own, and do not necessarily reflect the views of the author or the publisher. Any resemblance or similarities between the characters in this novel to living or dead persons in this world or any parallel world within the known multiverse is either a coincidence; an allusion to real, alternate, invented, or occult history; or a parody. If you find any allegory or applicability, please consult a qualified professional for psychiatric evaluation and treatment.

## Author's Note

*Nightmare Sequencer* contains not only spoken dialogue, but dialogue transmitted over text message. To distinguish the two, I use corner bracket marks commonly used as quotes in East Asian languages like Japanese and Chinese for the latter instead of standard quotation marks, so that a text message will look like this,「This is text dialogue.」

Other authors facing similar issues in their own work are welcome to use the same method. The Unicode representations for these characters are U+300C and U+301C.

## Dedication

## Chapter One

Claire's face was everywhere, reflected back at her a billion times from the floors, walls, pillars, and ceilings of the cathedral as she advanced toward the altar. Her footsteps echoed, precluding any hope of a stealthy approach, but that did not concern her. Here in this temple sacred to a long-dead goddess, her presence was long expected.

She had tried and failed to reach the entrance a thousand times, and each failed attempt ended with her death. But she was beyond death's grasp, waking at a candlelit shrine at the base of the long, twisting cavern leading to the Heart of the Earth-Mother every time she failed. 

With each failure, Claire's knowledge of the path before her deepened and her resolved hardened. The reasons she had failed before were legion. A companion might have abandoned her at a critical moment—or worse, betrayed her. Perhaps she lacked the proper equipment. Maybe simply did not understand the forces arrayed against her well enough to counter them.

Whatever had stopped Claire before was no longer an obstacle. She had finally reached the end of her journey. The last and greatest of the Plaguebringers, Anthracis, awaited her, and she would be the first to strike him down. She wore the grin of a pirate staring into a chest full of booty as she raised her sword. "Yo, Anthy! You ready to get your arse pounded?"

"You are the first fool to reach my sanctum, little Claire, but it isn't too late to retreat. You have not faced the last or greatest of my guardians."

Undeterred, Claire strode into the nave. A form began to coalesce into reality before her as she approached. "This better be fun, Anthracis. Otherwise I'll just kick your arse harder for boring me."

At first, it resembled a massive disembodied cancerous growth several times Claire's height. It soon began to differentiate, as four legs grew to support its bulk, and a long tail extended behind it. Bony spikes pierced the beast's tail, spilling not blood but some kind of yellow-white ichor that more closely resembled pus.