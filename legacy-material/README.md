# README #

This is a private repository for all works pertaining to [*Starbreaker*](http://www.starbreakerseries.com) by [Matthew Graybosch](http://www.matthewgraybosch.com). It is not an application, but since the Atlassian TOS didn't say anything about novelists not being permitted to use BitBucket, here we are. :)

### What is this repository for? ###

All drafts and revised text for *Starbreaker* will live here.

### How do I get set up? ###

If you have access to this repository, you're either my wife, my publisher, or you managed to crack my account. All you have to do is clone the repository. All drafts are in plain text, formatted with Markdown. Make sure you've got pandoc, rake, and LibreOffice installed, and you should be all set.

### Who do I talk to? ###

Please direct all questions/concerns to [public@starbreakerseries.com](mailto:public@starbreakerseries.com).